<?php
namespace DB;
final class MySQLi {
	private $connection;

	public function __construct($hostname, $username, $password, $database, $port = '3306') {
		$this->connection = new \mysqli($hostname, $username, $password, $database, $port);

		if ($this->connection->connect_error) {
			throw new \Exception('Error: ' . mysql_error($this->connection) . '<br />Error No: ' . mysql_errno($this->connection) . '<br /> Error in: <b>' . $trace[1]['file'] . '</b> line <b>' . $trace[1]['line'] . '</b><br />' . $sql);
		}

		$this->connection->set_charset("utf8");
		$this->connection->query("SET SQL_MODE = ''");
		// $this->connection->query("SET character_set_results=utf8'");
		// $this->connection->query("SET names=utf8");
		// $this->connection->query("SET character_set_client=utf8");
		// $this->connection->query("SET character_set_connection=utf8");
		// $this->connection->query("SET collation_connection=utf8_general_ci");
	}

	public function query($sql) {
		$this->logQuery($sql);
		$query = $this->connection->query($sql);

		if (!$this->connection->errno) {
			if ($query instanceof \mysqli_result) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new \stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				$query->close();

				return $result;
			} else {
				return true;
			}
		} else {
			throw new \Exception('Error: ' . $this->connection->error  . '<br />Error No: ' . $this->connection->errno . '<br />' . $sql);
		}
	}

	private function logQuery($sql){
		date_default_timezone_set("Asia/Kolkata");
		if(isset($_GET['route'])){
			$page_name = $_GET['route'];
		} else {
			$page_name = '';
		}
		// '/^(UPDATE|INSERT|DELETE|TRUNCATE)\s{1,}(FROM|TABLE|INTO|[a-zA-Z_]{1,})\s{1,}([a-zA-Z_]{1,})\s.*$/i'
		$sql = str_replace(array(";", "`", "\r\n", "\n", "\r"), '', $sql);
		$sql = $sql.' ';
		preg_match_all('/^(UPDATE|INSERT|DELETE|TRUNCATE)\s{1,}(FROM|TABLE|INTO|[a-zA-Z_]{1,})\s{1,}([a-zA-Z_]{1,})\s.*$/i', $sql, $results);
		$table_name = '';
		$type  = '';
		$in = 0;
		if(isset($results[1][0]) && strtolower($results[1][0]) == 'update'){
			$in = 1;
			$table_name = strtoupper($results[2][0]);
			$type  = strtoupper($results[1][0]);
		} elseif(isset($results[1][0]) && strtolower($results[1][0]) == 'insert') {
			$in = 1;
			$table_name = strtoupper($results[3][0]);
			$type  = strtoupper($results[1][0]);
		} elseif(isset($results[1][0]) && strtolower($results[1][0]) == 'delete') {
			$in = 1;
			$tables = strtoupper($results[3][0]);
			$table_name = str_replace("WHERE", "", $tables);
			$type  = strtoupper($results[1][0]);
		}

		if($in == 1){
			$valid_array = array('OC_ITEM', 'OC_BRAND', 'OC_BRAND_TYPE', 'OC_CAPTAIN', 'OC_WAITER', 'OC_CATEGORY', 'OC_SUBCATEGORY', 'OC_LOCATION', 'OC_KOTGROUP', 'OC_TAX', 'OC_DEPARTMENT', 'OC_CUSTOMER', 'OC_CUSTOMERINFO', 'OC_MODIFIER', 'OC_TEMPLATE', 'OC_EXPENSE_ACCOUNT', 'OC_SUPPLIER', 'OC_ORDER_INFO', 'OC_ORDER_ITEMS', 'OC_ORDER_INFO_MODIFY', 'OC_ORDER_ITEMS_MODIFY', 'OC_RUNNING_TABLE', 'OC_USER_GROUP', 'SETTINGS_ADOR');
			if(in_array($table_name, $valid_array)){
				//$user_id = $_SESSION['user_id'];
				$user_id = 0;

				$ip_address = $_SERVER['REMOTE_ADDR'];
				$sql = base64_encode($sql);
				$sql1 = "INSERT INTO `oc_log` SET `type` = '".$type."', `page_name` = '".$page_name."', `table_name` = '".$table_name."', `query` = '".$sql."', `log_date` = '".date('Y-m-d')."', `log_time` = '".date('H:i:s')."', `user_id` = '".$user_id."', `ip_address` = '".$ip_address."' ";
				//echo $sql1;exit;
				$this->connection->query($sql1);
			}
		}
	}

	public function escape($value) {
		return $this->connection->real_escape_string($value);
	}
	
	public function countAffected() {
		return $this->connection->affected_rows;
	}

	public function getLastId() {
		return $this->connection->insert_id;
	}
	
	public function connected() {
		return $this->connection->connected();
	}
	
	public function __destruct() {
		$this->connection->close();
	}
}