<?php
class Template {
	private $adaptor;

  	public function __construct($adaptor) {
	    $class = 'Template\\' . $adaptor;

		if (class_exists($class)) {
			$this->adaptor = new $class();
		} else {
			throw new \Exception('Error: Could not load template adaptor ' . $adaptor . '!');
		}
	}

	public function set($key, $value) {
		$this->adaptor->set($key, $value);
	}

	public function render($template) {
		return $this->adaptor->render($template);
	}
}

// class Template {
// 	public $data = array();

// 	public function fetch($filename) {
// 		$file = DIR_TEMPLATE . $filename;

// 		if (file_exists($file)) {
// 			extract($this->data);

// 			ob_start();

// 			include($file);

// 			$content = ob_get_clean();

// 			return $content;
// 		} else {
// 			trigger_error('Error: Could not load template ' . $file . '!');
// 			exit();				
// 		}
// 	}
// }
