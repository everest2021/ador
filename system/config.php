<?php
// HTTP
define('HTTP_SERVER', 'http://localhost:8012/hotel/zeel1/admin/');
define('TEST', 'http://27.0.58.133/hotel/hotelserver/');
define('HTTP_CATALOG', 'http://localhost:8012/hotel/zeel1/');
define('HTTP_HOME', 'http://localhost:8012/hotel/admin/index.php?route=common/login&user_id=1');

// HTTPS
define('HTTPS_SERVER', 'http://localhost:8012/hotel/zeel1/admin/');
define('HTTPS_CATALOG', 'http://localhost:8012/hotel/zeel1/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/hotel/zeel1/admin/');
define('DIR', 'C:/xampp/htdocs/hotel/zeel1/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/hotel/zeel1/system/');
define('DIR_IMAGE', 'C:/xampp/htdocs/hotel/zeel1/image/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/hotel/zeel1/admin/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/hotel/zeel1/admin/view/template/');
define('DIR_CONFIG', 'C:/xampp/htdocs/hotel/zeel1/system/config/');
define('DIR_CACHE', 'C:/xampp/htdocs/hotel/zeel1/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/hotel/zeel1/system/storage/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/hotel/zeel1/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/xampp/htdocs/hotel/zeel1/system/storage/modification/');
define('DIR_UPLOAD', 'C:/xampp/htdocs/hotel/zeel1/system/storage/upload/');
define('DIR_CATALOG', 'C:/xampp/htdocs/hotel/zeel1/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'db_ador');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
define('type', '1');

//DB 27
define('DB_HOSTNAME1', 'funshalla.in');
define('DB_USERNAME1', 'funshalla2018');
define('DB_PASSWORD1', 'funshalla2018');
define('DB_DATABASE1', 'db_hotel_fun');

define('FASTFOOD', '2');
define('WAITER','0');
define('CAPTAIN','0');
define('PERSONS','0');
define('SETTLEMENT','1');
define('SETTLEMENT_ON','0');
define('SERVICE_CHARGE_FOOD', '0');
define('SERVICE_CHARGE_LIQ', '0');
define('INCLUSIVE','1'); // 1 = Inclusive , 0 = Exclusive
define('NOCATEGORY','0'); // 1 = direct subcategory, 0 = category -> subcategory
define('CATEGORY','1'); // If direct subcategory is set to '1' this should also be set 

//For bill printing
define('PRINTER_NAME','192.168.1.23'); // for network 'ip address' & for windows 'name'
define('PRINTER_TYPE','1'); // 1 for Network & 2 for Windows

define('BARCODE_PRINTER','XP-58C');

// Hotel Name and Address
define('HOTEL_NAME', 'ENATS');
define('HOTEL_ADD', "Shop No 5,Mansorovar\nOpp. Narayan E-School\nSwami Satyanand Marg\nBhayandar-W");
define('GST_NO', '123456');
define('TEXT1', '');
define('TEXT2', '');
define('TEXT3', 'Thank you visit again');

//sms link
define('SMS_LINK_1', "http://bulksms.gfxbandits.com/api/sendmsg.php?user=funshalla&pass=xyz123@abc&sender=FUNSLA&priority=ndnd&stype=normal");
define('SMS_LINK_2', "http://bulksms.gfxbandits.com/api/sendmsg.php?user=funshalla1&pass=xyz123@abc&sender=FUNSLA&priority=ndnd&stype=normal");
define('SMS_LINK_3', "http://bulksms.gfxbandits.com/api/sendmsg.php?user=funshalla2&pass=xyz123@abc&sender=FUNSLA&priority=ndnd&stype=normal");
//define('BAR_NAME', 'YARRI');
//define('BAR_ADD', "Shop No 5,Mansorovar\nOpp. Narayan E-School\nSwami Satyanand Marg\nBhayandar-W");

//define('CONTACT_NUMBER','7977580214');
define('CONTACT_NUMBER','8850446788');

define('BAR_NAME', '');
define('BAR_ADD', "");
define('ADVANCE_NOTE',"Booking amount once paid\n will not be refunded");