/*$order_data['orders'][] = array(
			'order_id' 			=> '1',   //wera orderid
			'restaurant_id' 	=> '1',	// wera outlet id
			'restaurant_name' 	=> 'Mukesh Hotel',
			'external_order_id' => '01',     // swigy zomato id
			'order_from' 		=> 'Swiggy', // order from swiggy,zomato,foodpanda
			'order_date_time'	=> '2019-10-09T18:26:59.75', //unix date time formate
			'enable_delivery'	=> '1',   // if 0 then source fulfilled oder. if 1 then restaurant
			'net_amount'		=> '500', //sum of total cost of all the items
			'gross_amount'		=> '560', // net_amt + order_tax +other_charge - discount
			'payment_mode'  	=> 'ONLINE', //cash/online
			'order_type' 		=> 'DELIVERY',	//TAKEAWAY /DELIVERY/DINEIN
			'order_instructions'=> 'extra chees',
			'cgst'				=> '9',
			'sgst'				=> '9',
			'order_packaging'	=> '10',  //Packaging Charge on Order Level
			'packaging'			=> '11',   //order_packaging + item_level_packaging
			'packaging_cgst'	=> '10', 		//cgst on Total Packaging Charge
			'packaging_sgst'	=> '10', 		//sgst on Total Packaging Charge
			'discount'			=> '0', 	//total discount on order
			'delivery_charge'	=> '10',

		);

		

		

		

		$order_data['order_items'][] = array(
			'wera_item_id'   		=> '01',   //wera item id
			'item_id'				=> '01',  // (as sent in menu upload)
			'item_name'				=> 'Manchurian Pizzza',
			'item_unit_price'		=> '400', //unit price of item
			'subtotal'				=> '450', //Price of item with addons and variants
			'instructions'			=> 'Chees Barabar se Dalna Extra Tikha', 
			'discount'				=> '2', //discount on item
			'item_quantity'			=> '1',  //qty of item ordered
			'cgst' 					=> '2',
			'sgst'					=> '2',
			'cgst_percent'			=> '9',
			'sgst_percent'			=> '9',
			'packaging'				=> '2'
		);

		


		$order_data['variants'][] = array(
			'size_id' 		=> '1',
			'size_name'		=> 'New Size',
			'price' 		=> '50'

		);


		$order_data['addons'][] = array(
			'addon_id' 		=> '1',
			'name'			=> 'Chees',
			'price'			=> '22',
			'cgst'			=> '2',
			'sgst' 			=> '2',
			'cgst_percent'  => '9',
			'sgst_percent'  => '9'
		);

		foreach ($order_data['orders'] as $okey => $ovalue) {
			$this->db->query("INSERT INTO `oc_werafood_orders` SET 
						`order_id` = '".$ovalue['order_id']."',
						`restaurant_id` = '".$ovalue['restaurant_id']."',
						`restaurant_name` = '".$ovalue['restaurant_name']."',
						`external_order_id` = '".$ovalue['external_order_id']."',
						`order_from` = '".$ovalue['order_from']."',
						`order_date_time` = '".$ovalue['order_date_time']."',
						`enable_delivery` = '".$ovalue['enable_delivery']."',
						`net_amount` = '".$ovalue['net_amount']."',
						`gross_amount` = '".$ovalue['gross_amount']."',
						`payment_mode` = '".$ovalue['payment_mode']."',
						`order_type` = '".$ovalue['order_type']."',
						`order_instructions` = '".$ovalue['order_instructions']."',
						`cgst` = '".$ovalue['cgst']."',
						`sgst` = '".$ovalue['sgst']."',
						`order_packaging` = '".$ovalue['order_packaging']."',
						`packaging` = '".$ovalue['packaging']."',
						`packaging_cgst` = '".$ovalue['packaging_cgst']."',
						`packaging_sgst` = '".$ovalue['packaging_sgst']."',
						`discount` = '".$ovalue['discount']."',
						`delivery_charge` = '".$ovalue['delivery_charge']."'
			");
		}
		$order_id = $this->db->getLastId();


		foreach ($order_data['order_items'] as $ikey => $ivalue) {
			$this->db->query("INSERT INTO `oc_werafood_order_items` SET 
						`order_id` = '".$order_id."',
						`wera_item_id` = '".$ivalue['wera_item_id']."',
						`item_id` = '".$ivalue['item_id']."',
						`item_name` = '".$ivalue['item_name']."',
						`item_unit_price` = '".$ivalue['item_unit_price']."',
						`subtotal` = '".$ivalue['subtotal']."',
						`instructions` = '".$ivalue['instructions']."',
						`discount` = '".$ivalue['discount']."',
						`item_quantity` = '".$ivalue['item_quantity']."',
						`cgst` = '".$ivalue['cgst']."',
						`sgst` = '".$ivalue['sgst']."',
						`cgst_percent` = '".$ivalue['cgst_percent']."',
						`sgst_percent` = '".$ivalue['sgst_percent']."',
						`packaging` = '".$ivalue['packaging']."'
			");
		}
		$item_id = $this->db->getLastId();
		
		$this->db->query("INSERT INTO `oc_werafood_order_customer` SET
					`order_id` = '".$order_id."',
					`name` = '".$order_data['customer_details']['name']."',
					`phone_number` = '".$order_data['customer_details']['phone_number']."',
					`email` = '".$order_data['customer_details']['email']."',
					`address` = '".$order_data['customer_details']['address']."',
					`delivery_area` = '".$order_data['customer_details']['delivery_area']."',
					`address_instructions` = '".$order_data['customer_details']['address_instructions']."'
		");

		foreach ($order_data['variants'] as $vkey => $vvalue) {
			$this->db->query("INSERT INTO `oc_werafood_order_variants` SET
						`order_id` = '".$order_id."',
						`order_item_id` = '".$item_id."',
						`size_id` = '".$vvalue['size_id']."',
						`size_name` = '".$vvalue['size_name']."',
						`price` = '".$vvalue['price']."'
			");
		}

		foreach ($order_data['addons'] as $akey => $avalue) {
			$this->db->query("INSERT INTO `oc_werafood_order_addons` SET
						`order_id` = '".$order_id."',
						`order_item_id` = '".$item_id."',
						`addon_id` = '".$avalue['addon_id']."',
						`name` = '".$avalue['name']."',
						`price` = '".$avalue['price']."',
						`cgst` = '".$avalue['cgst']."',
						`sgst` = '".$avalue['sgst']."',
						`cgst_percent` = '".$avalue['cgst_percent']."',
						`sgst_percent` = '".$avalue['sgst_percent']."'

			");
		}*/