<?php echo $header; ?>

<?php

$notimp = DIR_ACTIVATE;
$localfile = file_get_contents($notimp);
$localdata = explode('|', $localfile);

$getdates = date('Y-m-d', strtotime($localdata[2] .' -10 day'));
$date1 = date_create($localdata[2]);
$currentdate = date("Y-m-d");
$date2 = date_create($currentdate);

$diff=date_diff($date1,$date2);
// echo $diff->format("%a days");exit;

?>


<link rel="stylesheet" type="text/css" href="view/stylesheet/style.css">
<script src="https://kit.fontawesome.com/a81368914c.js"></script>
<div id="content" >
    <div class="container-fluid"><br /><br />
        <div class="row">
            <div class="panel">
                <div class="panel-body">
                    <?php if ($currentdate >= $getdates) { ?>
                         <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo 'Your data will be corrupted in '.$diff->format("%a days") .' please contact on this number :- 8329381148'; ?>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                          </div>
                    <?php } ?>
                    <?php if ($success) { ?>
                        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>
                    <?php } ?>
                    <?php if ($error_warning) { ?>
                        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>
                    <?php } ?>
                    <?php if ($warning) { ?>
                        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $warning; ?>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>
                    <?php } ?>
                    <div class="col-sm-6">
                        <div class="img">
                            <img src="view/image/chef.svg">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="login-container">
                            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                                <img class="avatar" src="view/image/cooking.svg"><br><br><br><br>
                                <h3>Welcome to <?php echo $hotel_name ?>  </h3><br>
                                <div class="input-div one">
                                    <div class="i">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <div>
                                        <h5>Username</h5>
                                        <input type="text" name="username" class="input" autocomplete="off" placeholder="Username">
                                    </div>
                                </div>

                                <div class="input-div two">
                                    <div class="i">
                                        <i class="fa fa-lock"></i>
                                    </div>
                                    <div>
                                        <h5>Password</h5>
                                        <input type="password" name="password" class="input" placeholder="Password">
                                    </div>
                                </div>
                                <!-- <a href="#">Forgot Password?</a> -->
                                <!-- <input type="submit" name="login" class="btn"> -->
                                <button type="submit" name="login" class="btn btn-primary"><i class="fa fa-key"></i> <?php echo $button_login; ?></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

   
    
<div id="content" style="display: none;">
  <div class="container-fluid"><br />
    <br />
    <div class="row">
      <div class="col-sm-offset-4 col-sm-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h1 class="panel-title"><i class="fa fa-lock"></i> <?php echo $text_login; ?></h1>
          </div>
          <div class="panel-body">
            <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
              <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
              <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <?php if ($warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $warning; ?>
              <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="input-username"><?php echo $entry_username; ?></label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input type="text" name="username" value="<?php echo $username; ?>" placeholder="<?php echo $entry_username; ?>" id="input-username" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label for="input-password"><?php echo $entry_password; ?></label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
                  <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                </div>
                
              </div>
              <div class="text-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-key"></i> <?php echo $button_login; ?></button>
              </div>
              <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- <script type="text/javascript" src="js/main.js"></script> -->
<?php echo $footer; ?>