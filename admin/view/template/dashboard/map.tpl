<div class="tile" style="background-color: #FF00FF;">
  <div class="tile-heading" style="display: none;"><?php echo $heading_title; ?></div>
  <div class="tile-body" style="min-height: 92px;line-height: 31px !important;">
    <h2 class="pull-left" style="">
      <div style="font-size: 15px;"><?php echo 'एकूण गेम / Total Games'; ?> : <a href="<?php echo $total_link; ?>"><?php echo $total_sports; ?></a></div>
    </h2>
    <br />
    <h2 class="pull-left" style="">
      <div style="font-size: 15px;"><?php echo 'कला गेम / Kala Games'; ?> : <a href="<?php echo $kala_link; ?>"><?php echo $kala_sports; ?></a></div>
    </h2>
    <br />
    <h2 class="pull-left" style="">
      <div style="font-size: 15px;"><?php echo 'क्रिडा गेम / Krida Games'; ?> : <a href="<?php echo $krida_link; ?>"><?php echo $krida_sports; ?></a></div>
    </h2>
  </div>
  <div class="tile-footer" style="background-color: #FF00FF;">&nbsp;</div>
</div>