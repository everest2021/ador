<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
	</head>
	<body style="height: 100%;margin: 0;padding: 0;font-family: 'Open Sans', sans-serif !important;font-size: 12px;color: #666666; text-rendering: optimizeLegibility;">
		<div class="container" style="width: 725px;padding-right: 0px;padding-left: 0px;margin-right: auto;margin-left: auto;">
			<h5 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h5>
		  		<?php date_default_timezone_set("Asia/Kolkata");?>
		  		<h5 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h5>
		  		<center><h5><b>Customer Wise Sale Report</b></h5></center>
		  		<h5>From : <?php echo $startdate; ?></h5>
		  		<h5 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h5><br>
				<table border="1" class="table" style="margin-top: 0px;border: 0px solid black;width: 100%;max-width: 100%;margin-bottom: 5px;background-color: transparent;border-collapse: collapse;padding: 0px !important;font-size: 14px;">
					<tr>
						<th style="text-align: left;">Date</th>
						<th style="text-align: left;">Customer Name</th>
						<th style="text-align: left;">Contact No</th>
						<th style="text-align: left;">Bill No</th>
						<th style="text-align: right;">Grand Total</th>
					</tr>
					<?php $grandtotal=0;?>
					<?php foreach ($showdata as $key => $value) {?>
				  		<tr>
					  		<td style="text-align: left;"><?php echo $value['bill_date']; ?></td>
					  		<td style="text-align: left;"><?php echo $value['cust_name']; ?></td>
					  		<td style="text-align: left;"><?php echo $value['cust_contact'] ?></td>
							<td style="text-align: left;"><?php echo $value['billno'] ?></td>

					  		<td style="text-align: right;"><?php echo $value['grand_total']; ?></td>
					  	</tr>
					  	<?php $grandtotal = $grandtotal + $value['grand_total']; ?>
					<?php } ?>
					<tr>
					  	<td style="text-align: right;" colspan="4"><?php echo "Grand Total" ?></td>

					  	<td style="text-align: right;"><?php echo $grandtotal; ?></td>
					</tr> 
				</table>
				<?php echo $footer; ?>
		</div>
</html>