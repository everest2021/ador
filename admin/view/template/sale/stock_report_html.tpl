<!DOCTYPE html>
	<html dir="<?php echo ""; ?>" lang="<?php echo ""; ?>">
		<head>
			<meta charset="UTF-8" />
		</head>
		<body style="height: 100%;margin: 0;padding: 0;font-family: 'Open Sans', sans-serif !important;font-size: 12px;color: #666666; text-rendering: optimizeLegibility;">
			<div class="container" style="width: 725px;padding-right: 0px;padding-left: 0px;margin-right: auto;margin-left: auto;">
				<h5 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h5>
		  		<?php date_default_timezone_set("Asia/Kolkata");?>
		  		<h5 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h5>
		  		<center><h2><b>Stock Report</b></h2></center>
		  		<h3>Date : <?php echo $filter_enddate; ?></h3>
				<table border="1" class="table" style="margin-top: 0px;border: 0px solid black;width: 100%;max-width: 100%;margin-bottom: 5px;background-color: transparent;border-collapse: collapse;padding: 0px !important;font-size: 14px;">
					<?php foreach($final_data as $fkey =>$fvalue) { ?>
						<tr>
							<th style="text-align: center;">Item Name</th>
							<th style="text-align: center;">Opening Qty</th>
							<th style="text-align: center;">Purchase Qty</th>
							<th style="text-align: center;">Return</th>
							<th style="text-align: center;">Manufacture</th>
							<th style="text-align: center;">Trans. From Qty</th>
							<th style="text-align: center;">Trans. In Qty</th>
							<th style="text-align: center;">Wastage Qty</th>
							<th style="text-align: center;">Sales</th>
							<th style="text-align: center;">Closing Qty</th>
							<th style="text-align: center;">Sale Price</th>
							<th style="text-align: center;">Value</th>
						</tr>
						<tr>
							<td colspan="12"><b><?php echo $fvalue['store_name'] ?></b></td>
						</tr>
						<?php foreach($fvalue['all_data'] as $key =>$value) { //echo "<pre>"; print_r($fvalue['all_data'])?>
							<tr>
								<td><?php echo $value['item_name'] ?></td>
								<td><?php echo $value['open_bal'] ?></td>
								<td><?php echo $value['purchase_order'] ?></td>
								<td><?php echo "0" ?></td>
								<td><?php echo $value['manufac_amt'] ?></td>
								<td><?php echo $value['stock_transfer_from'] ?></td>
								<td><?php echo $value['stock_transfer_to'] ?></td>
								<td><?php echo $value['wastage_amt'] ?></td>
								<td><?php echo $value['sale_qty'] ?></td>
								<td><?php echo $value['avail_quantity'] ?></td>
								<td><?php echo $value['stock_rate'] ?></td>
								<td><?php echo $value['final_amt'] ?></td>
							</tr>
						<?php } ?>
						<tr>
							<td><b><?php echo "Total" ?></b></td>
							<td><?php echo $fvalue['sub_total_open_bal'] ?></td>
							<td><?php echo $fvalue['sub_total_purchase_order'] ?></td>
							<td><?php echo "0" ?></td>
							<td><?php echo $fvalue['sub_total_manufacture_order'] ?></td>
							<td><?php echo $fvalue['sub_total_stock_transfer_from'] ?></td>
							<td><?php echo $fvalue['sub_total_stock_transfer_to'] ?></td>
							<td><?php echo $fvalue['sub_total_wastage_amt'] ?></td>
							<td><?php echo $fvalue['sub_total_sale_qty'] ?></td>
							<td><?php echo $fvalue['sub_total_avail_quantity'] ?></td>
							<td><?php echo $fvalue['sub_total_stock_rate'] ?></td>
							<td><?php echo $fvalue['sub_total_final_amt'] ?></td>
						</tr>
						<tr>
							<td colspan="12"></td>
						</tr>
					<?php } ?>	
						<tr>
							<td><b><?php echo "Grand Total" ?></b></td>
							<td><?php echo $grand_total_open_bal ?></td>
							<td><?php echo $grand_total_purchase_order ?></td>
							<td><?php echo "0" ?></td>
							<td><?php echo $grand_total_manufacture_order ?></td>
							<td><?php echo $grand_total_stock_transfer_from ?></td>
							<td><?php echo $grand_total_stock_transfer_to ?></td>
							<td><?php echo $grand_total_wastage_amt ?></td>
							<td><?php echo $grand_total_sale_qty ?></td>
							<td><?php echo $grand_total_avail_quantity ?></td>
							<td><?php echo $grand_total_stock_rate ?></td>
							<td><?php echo $grand_total_final_amt ?></td>
						</tr>
					</table>
				<?php echo $footer; ?>
   			</div>
		</body>
	</html>
