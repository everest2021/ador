<!DOCTYPE html>
<html dir="<?php echo ""; ?>" lang="<?php echo ""; ?>">
	<head>
		<meta charset="UTF-8" />
	</head>
	<body style="height: 100%;margin: 0;padding: 0;font-family: 'Open Sans', sans-serif !important;font-size: 12px;color: #666666; text-rendering: optimizeLegibility;">
		<div class="container" style="width: 725px;padding-right: 0px;padding-left: 0px;margin-right: auto;margin-left: auto;">
			<h5 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h5>
	  		<?php date_default_timezone_set("Asia/Kolkata");?>
	  		<h5 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h5>
	  		<center><h5><b>Customer Wise Item Report</b></h5></center>
	  		<h5>From : <?php echo $startdate; ?></h5>
	  		<h5 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h5><br>
			<table  class="table" style="margin-top: 0px;width: 100%;max-width: 100%;margin-bottom: 5px;background-color: transparent;border-collapse: collapse;padding: 0px !important;font-size: 14px;">
				<?php $grandtotal=0;  ?>
				<tr>
							<th style="text-align: center;">Bill No</th>
							<th style="text-align: center;">Date</th>
							<th style="text-align: center;" >Customer Name</th>
							<th style="text-align: center;" >Customer Address</th>

							<th style="text-align: center;">Contact No</th>
							<th style="text-align: right;">Grand Total</th>
						</tr>
				<tr>
					<th style="text-align: center;">Sr No</th>
					<th style="text-align: center;">Kot No</th>
					<th style="text-align: center;" >Name</th>
					<th style="text-align: center;">rate</th>
					<th style="text-align: right;">Qty</th>
					<th style="text-align: right;">Amt</th>
							</tr>
						  	<?php foreach ($final_datass as $key => $value) { //echo'<pre>';print_r($value); exit;?>
						<tr >
							<th style="text-align: center;border-right: none;border-left: none;" colspan="8"></th>
						</tr>
						
					  		<tr>
						  		<td><?php echo $value['billno'] ?></td>
						  		<td><?php echo $value['bill_date']; ?></td>
						  		<td><?php echo $value['cust_name']; ?></td>
						  		<td ><?php echo $value['cust_address']; ?></td>

						  		<td><?php echo $value['cust_contact'] ?></td>
						  		<td style="text-align: right;"><?php echo $value['grand_total']; ?></td>
						  	</tr> 
				  			<?php $grandtotal = $grandtotal + $value['grand_total']; ?>
				  			
					  			<?php foreach ($value['sub_data'] as $skey => $svalue) { //echo'<pre>';print_r($svalue); exit;?>
					  			<tr>
							  		<td><?php echo $svalue['sr_no']; ?></td>
							  		<td><?php echo $svalue['kot_no']; ?></td>
							  		<td ><?php echo $svalue['name']; ?></td>
							  		<td><?php echo $svalue['rate']; ?></td>
							  		<td style="text-align: right;"><?php echo $svalue['qty'] ?></td>
							  		<td style="text-align: right;"><?php echo $svalue['amt'] ?></td>
							  	</tr>
							  	<?php } ?>
						  	<?php } ?>
						  	<tr>
							  	<td style="text-align: right;" colspan="7"><?php echo "Grand Total" ?></td>

							  	<td style="text-align: right;"><?php echo $grandtotal; ?></td>
							</tr> 
			</table>
			<?php echo $footer; ?>
   		</div>
	</body>
</html>