<!DOCTYPE html>
	<html dir="<?php echo ""; ?>" lang="<?php echo ""; ?>">
		<head>
			<meta charset="UTF-8" />
			<!-- <title><?php echo $heading_title; ?></title> -->
		</head>
		<body style="height: 100%;margin: 0;padding: 0;font-family: 'Open Sans', sans-serif !important;font-size: 12px;color: #666666; text-rendering: optimizeLegibility;">
			<div class="container" style="padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;">
  
			  <!-- <div class="col-sm-6 col-sm-offset-3">
						<center>
						  <?php echo HOTEL_NAME ?>
						  <?php echo HOTEL_ADD ?>
						</center> -->

				<h5 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h5>
		  		<?php date_default_timezone_set("Asia/Kolkata");?>
		  		<h5 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h5>
		  		<center><h5><b>Billwise Sales Report</b></h5></center>
		  		<h5>From : <?php echo $startdate; ?></h5>
		  		<h5 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h5><br>
				<table border= '1' style='width:100%; text-align: center; padding: 1px; border-collapse:collapse;margin-right:100px;' class="table table-bordered table-hover" >
					<tr>
						<th style="text-align: center;">Item Name</th>
						<th style="text-align: center;" colspan="13">Opening Stock</th>
						<th style="text-align: center;" colspan="13">Purches Stock</th>
						<th style="text-align: center;" colspan="13">Sale Stock</th>
						<th style="text-align: center;" colspan="13">Closing Qty</th>
					</tr>
					<?php if($liquor_type){ //echo "<pre>";print_r($liquor_type);exit; ?>
						<?php foreach($liquor_type as $lkey => $lvalue) { ?>
							<tr>
								<th style="text-align: left;" colspan="55"><?php echo $lvalue['type']; ?></th>
							</tr>
							<tr>
								<th style="text-align: center;">Brand Name</th>
								<?php foreach($size[$lvalue['type']] as $szkey => $szvalue){ ?>
									<td><?php echo $szvalue['size'];?></td>
								<?php } ?>
								<?php foreach($size[$lvalue['type']] as $szkey => $szvalue){ //echo "<pre>";print_r($size[$lvalue['type']]);exit; ?>
									<td><?php echo $szvalue['size'];?></td>
								<?php } ?>
								<?php foreach($size[$lvalue['type']] as $szkey => $szvalue){ ?>
									<td><?php echo $szvalue['size'];?></td>
								<?php } ?>
								<?php foreach($size[$lvalue['type']] as $szkey => $szvalue){ ?>
									<td><?php echo $szvalue['size'];?></td>
								<?php } ?>
							</tr>
							<?php foreach($liquor_name[$lvalue['type']] as $lnkey => $lnvalue){ //echo "<pre>";print_r($liquor_name);exit;?>
								<tr>
									<td><?php echo $lnvalue['brand'] ?></td>
									<?php foreach($brand as $bkey => $bvalue) {  ?>
										<?php foreach ($bvalue['opening_qtys'] as $okey => $ovalue) { //echo "<pre>";print_r($brand);exit; ?>
											<?php if($bkey == $lnvalue['brand']){ ?>
												<td><?php echo $ovalue['closing_balance'] ?></td>
											<?php } ?>
										<?php }?>
									<?php } ?>
									<?php foreach($brand as $bkey => $bvalue) {  ?>
										<?php foreach ($bvalue['pqty'] as $pkey => $pvalue) { //echo "<pre>";print_r($brand);exit; ?>
											<?php if($bkey == $lnvalue['brand']){ ?>
												<?php if($pvalue['sqty'] !=''){ ?>
													<td><?php echo $pvalue['sqty'] ?></td>
												<?php } else { ?>
													<td><?php echo "0.00";?></td>
												<?php } ?>																														
											<?php } ?>
										<?php }?>
									<?php } ?>
									<?php foreach($brand as $bkey => $bvalue) {  ?>
										<?php foreach ($bvalue['sale_qtys'] as $skey => $svalue) { //echo "<pre>";print_r($svalue);exit; ?>
											<?php if($bkey == $lnvalue['brand']){ ?>
												<?php if($svalue['sale_qty'] !=''){ ?>
													<td><?php echo $svalue['sale_qty'] ?></td>
												<?php } else { ?>	
													<td><?php echo "0.00";?></td>
												<?php } ?>															
											<?php } ?>
										<?php }?>
									<?php } ?>
									<?php foreach($brand as $bkey => $bvalue) {  ?>
										<?php foreach ($bvalue['closing_qtys'] as $ckey => $cvalue) { //echo "<pre>";print_r($cvalue);exit; ?>
											<?php if($bkey == $lnvalue['brand']){ ?>
												<?php if($cvalue['closing_bal'] !=''){ ?>
													<td><?php echo $cvalue['closing_bal'] ?></td>
												<?php } else { ?>	
													<td><?php echo "0.00";?></td>
												<?php } ?>															
											<?php } ?>
										<?php }?>
									<?php } ?>
								</tr>
							<!-- <?php //return false; ?> -->
							<?php } ?>	
						<?php } ?>	
					<?php } ?>

				</table>
			<?php echo $footer; ?>
   		</div>
	</body>
</html>
