<!DOCTYPE html>
	<html dir="<?php echo ""; ?>" lang="<?php echo ""; ?>">
		<head>
			<meta charset="UTF-8" />
			<!-- <title><?php echo $heading_title; ?></title> -->
		</head>
		<body style="height: 100%;margin: 0;padding: 0;font-family: 'Open Sans', sans-serif !important;font-size: 12px;color: #666666; text-rendering: optimizeLegibility;">
			<div class="container" style="width: 725px;padding-right: 0px;padding-left: 0px;margin-right: auto;margin-left: auto;">
				<h5 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h5>
		  		<?php date_default_timezone_set("Asia/Kolkata");?>
		  		<h5 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h5>
		  		<center><h5><b>Cashier Report</b></h5></center>
		  		<h5>From : <?php echo $startdate; ?></h5>
		  		<h5 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h5><br>
				<table border="1" class="table" style="margin-top: 0px;border: 0px solid black;width: 100%;max-width: 100%;margin-bottom: 5px;background-color: transparent;border-collapse: collapse;padding: 0px !important;font-size: 14px;">
					<?php $discount = 0; $grandtotal = 0; $vat = 0; $gst = 0; $discountvalue = 0; $afterdiscount = 0; $stax = 0; $roundoff = 0; $total = 0; $advance = 0;?>
				  		<?php $usertotal = 0; ?>
						<tr>
							<td><b>Payment Summary</b></td>
							
							<td><b>Total</b></td>
						</tr>
						<tr><td colspan="2" height="35"></td></tr>
						<tr>
							<td>Cash</td>
							
							<td><?php echo $orderpaymentamt['totalcash']?></td>
						</tr>

						<tr>
							<td>Credit Card</td>
							
							<td><?php echo $orderpaymentamt['totalcard']?></td>
						</tr>
						<tr>
							<td>Online</td>
							
							<td><?php echo $orderpaymentamt['totalpayonline']?></td>
						</tr>

						<tr>
							<td>Used Points Amt</td>
							
							<td><?php echo $orderpaymentamt['points_amt']?></td>
						</tr>

						<tr>
							<td><b> Used Credit Amt</b></td>
							
							<td><b><?php echo $orderpaymentamt['credit_amt']?></b></td>
						</tr>
						
						<tr>
							<td><b>Net Amt</b></td>
							
							<td><b><?php echo $orderpaymentamt['net_amt']?></b></td>
						</tr>

						<tr>
							<td>Sale Return Amt</td>
							
							<td><?php echo $orderpaymentamt['salereturn_amt']?></td>
						</tr>

						<tr>
							<td>Total Amt:</td>
							
							<td><?php echo $orderpaymentamt['total_amt']?></td>
						</tr>

						<tr>
							
							<td colspan="2"><?php echo "Todays Cash Total" ?></td>
						</tr>
						<tr>
							<td>Total Cash Amt:</td>
							
							<td><?php echo $cash_pay; ?></td>
						</tr>

						
						<tr>
							<td>Return Cash Amt:</td>
							
							<td><?php echo $cash_return; ?></td>
						</tr>

						<tr>
							<td>Total Cash Amt:</td>
							
							<td><?php echo $toady_cash_total; ?></td>
						</tr>
				</table>
			</div>
		</body>
	
		<style>
			 td,th {
				  font-size: 20px;
				  color: black;
				}

			h3,h4 {
				color: black;
			}
		</style>

</html>