<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
	</head>
	<body style="height: 100%;margin: 0;padding: 0;font-family: 'Open Sans', sans-serif !important;font-size: 12px;color: #666666; text-rendering: optimizeLegibility;">
		<div class="container" style="width: 725px;padding-right: 0px;padding-left: 0px;margin-right: auto;margin-left: auto;">
			<h5 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h5>
		  		<?php date_default_timezone_set("Asia/Kolkata");?>
		  		<h5 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h5>
		  		<center><h5><b>Shift Close Report</b></h5></center>
		  		<h5>From : <?php echo $startdate; ?></h5>
		  		<h5 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h5><br>
				<table border="1" class="table" style="margin-top: 0px;border: 0px solid black;width: 100%;max-width: 100%;margin-bottom: 5px;background-color: transparent;border-collapse: collapse;padding: 0px !important;font-size: 14px;">
					<tr>
						<th style="text-align: center;">Shift Id</th>
						<th style="text-align: center;">Closing Time</th>
						<th style="text-align: center;">Total Bill</th>
						<th style="text-align: center;">Total Sale</th>
						<th style="text-align: center;">Total Cash</th>
						<th style="text-align: center;">Total OnAcc</th>
						<th style="text-align: center;">Total Card</th>
					</tr>
					<?php $grandtotal=0;?>
						  	<?php foreach ($showdata as $key => $value) {?>
						  		<tr>
						  			<td colspan="7"><b><?php echo $key; ?><b></td>
						  		</tr>  
							  	<?php foreach ($value as $akey =>$data1) {?>
							  		<tr>
								  		<td><?php echo $data1['shift_id']; ?></td>
								  		<td><?php echo $data1['shift_time'] ?></td>
								  		<td><?php echo $data1['total']; ?></td>
								  		<td><?php echo $data1['totalbill']; ?></td>
								  		<td><?php echo $data1['cash']; ?></td>	
								  		<td><?php echo $data1['onacc']; ?></td>	
								  		<td><?php echo $data1['card']; ?></td>
								  	</tr> 
								<?php $grandtotal += $data1['totalbill']; }  ?> 
						  	<?php } ?>
						  	<tr>
						  		<td colspan="6"><b>Grand Total :</b></td>
						  		<td  style="text-align: left;"><b><?php echo $grandtotal ?></b></td>
						  	</tr>
				</table>
				<?php echo $footer; ?>
		</div>
</html>