<!DOCTYPE html>
<html dir="<?php echo ""; ?>" lang="<?php echo ""; ?>">
<head>
<meta charset="UTF-8" />
<!-- <title><?php echo $heading_title; ?></title> -->
</head>
<body style="height: 100%;margin: 0;padding: 0;font-family: 'Open Sans', sans-serif !important;font-size: 12px;color: #666666; text-rendering: optimizeLegibility;">
<div class="container" style="width: 725px;padding-right: 0px;padding-left: 0px;margin-right: auto;margin-left: auto;">
  <!-- <div class="col-sm-6 col-sm-offset-3">
            <center>
              <?php echo HOTEL_NAME ?>
              <?php echo HOTEL_ADD ?>
            </center> -->
                <h5 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h5>
                <?php date_default_timezone_set("Asia/Kolkata");?>
                <h5 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h5>
                <center><h5><b>Item wise Sales Report</b></h5></center>
                <h5>From : <?php echo $startdate; ?></h5>
                <h5 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h5><br>
                <table border="1" class="table" style="margin-top: 0px;border: 0px solid black;width: 100%;max-width: 100%;margin-bottom: 5px;background-color: transparent;border-collapse: collapse;padding: 0px !important;font-size: 14px;">
                    <tr>
                        <th style="text-align: center;">Product Name</th>
                        <th style="text-align: center;">Quantity</th>
                        <th style="text-align: center;">Amount</th>
                    </tr>
                    <?php 
                        $qtytotal = 0; 
                        $amttotal = 0;
                        $qtytotalm = 0; 
                        $amttotalm = 0; 
                        $vat = 0; 
                        $gst = 0; 
                        $stax = 0; 
                        $grandtotal = 0; 
                        $discount = 0; 
                        $cancelamt = 0; 
                        $roundoff = 0;
                        $advance = 0;
                        $packaging = 0;
                        $packaging_cgst = 0;
                        $packaging_sgst = 0;
                    ?>
                    <?php foreach($departments as $dept => $value ) {?>
                        <?php $departmentqty = 0; $departmentamt = 0;?>
                        <?php if($value != array()) {?>
                            <tr>
                                <td colspan="3" style="text-align: left;padding-left: 80px;"><?php echo $dept ?></td>
                            </tr>
                        <?php } ?>
                        <?php foreach($value as $key) {?>
                        <tr>
                        <?php if($key['cancel_bill'] == 1){ ?>
                            <td><?php echo $key['name'] = '' ?></td>
                            <td><?php echo $key['quantity'] = 0 ?></td>
                            <td><?php echo $key['amount'] = 0 ?></td>
                        <?php } else {?>
                            <td><?php echo $key['name'] ?></td>
                            <td><?php echo $key['quantity'] ?></td>
                            <td><?php echo $key['amount'] ?></td>
                        <?php } ?>
                            <?php $qtytotal = $qtytotal + $key['quantity']; $amttotal = $amttotal + $key['amount'];?>
                            <?php $departmentqty = $departmentqty + $key['quantity']; $departmentamt = $departmentamt + $key['amount'];?>
                        </tr>
                        <?php }?>
                        <?php if($value != array()) {?>
                            <tr>
                                <td>Total</td>
                                <td><?php echo $departmentqty ?></td>
                                <td><?php echo $departmentamt ?></td>
                            </tr>
                        <?php } ?>
                    <?php }?>
                    <?php /*<tr>
                        <td colspan="4"><b>Modifiers</b></td>
                    </tr>
                    <?php foreach($departmentsm as $dept => $value ) {?>
                        <?php $departmentqty = 0; $departmentamt = 0;?>
                        <?php if($value != array()) {?>
                            <tr>
                                <td colspan="3" style="text-align: left;padding-left: 80px;"><?php echo $dept ?></td>
                            </tr>
                        <?php } ?>
                        <?php foreach($value as $key) {?>
                        <tr>
                        <?php if($key['cancel_bill'] == 1){ ?>
                            <td><?php echo $key['name'] = '' ?></td>
                            <td><?php echo $key['quantity'] = 0 ?></td>
                            <td><?php echo $key['amount'] = 0 ?></td>
                        <?php } else {?>
                            <td><?php echo $key['name'] ?></td>
                            <td><?php echo $key['quantity'] ?></td>
                            <td><?php echo $key['amount'] ?></td>
                        <?php } ?>
                            <?php $qtytotalm = $qtytotalm + $key['quantity']; $amttotalm = $amttotalm + $key['amount'];?>
                            <?php $departmentqty = $departmentqty + $key['quantity']; $departmentamt = $departmentamt + $key['amount'];?>
                        </tr>
                        <?php }?>
                        <?php if($value != array()) {?>
                            <tr>
                                <td>Total</td>
                                <td><?php echo $departmentqty ?></td>
                                <td><?php echo $departmentamt ?></td>
                            </tr>
                        <?php } ?>
                    <?php }?>
                    <tr>
                        <td colspan="4"><b>Modifiers</b></td>
                    </tr> */?>
                    <?php if($modifiers != []) { ?>
                        <tr>
                            <td colspan="4"><b>Modifiers Start</b></td>
                        </tr>
                        <?php $qtytotalm = 0; $amttotalm = 0;?>
                        <?php foreach($modifiers as $modifier) { ?>
                        <tr>
                            <td><?php echo $modifier['name'] ?></td>
                            <td><?php echo $modifier['quantity'] ?></td>
                            <td><?php echo $modifier['amount'] ?></td>
                            <?php 
                                $qtytotalm = $qtytotalm + $modifier['quantity'];
                                $amttotalm = $amttotalm + $modifier['amount'];
                             ?>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td>Total</td>
                            <td><?php echo $qtytotalm ?></td>
                            <td><?php echo $amttotalm ?></td>
                        </tr>
                        <tr>
                            <td colspan="4"><b>Modifiers End</b></td>
                        </tr>
                    <?php } ?>
                    <?php if($qtytotal != '0' && $amttotal != '0' ) { ?>
                        <tr>
                            <td><b>Grand Total</b></td>
                            <td><b><?php echo $qtytotal ?></b></td>
                            <td><b><?php echo $amttotal ?></b></td>
                        </tr>
                        <tr>
                            <td colspan="2"><b>Gross Total : </b></td>
                            <td><b><?php echo $amttotal ?></b></td>
                        </tr>
                        <?php if($departmentvalue == '' && $category == '' && $subcategory == '' && $type == '' && $tablegroup == '') { ?> 
                            <tr>
                                <td colspan="2">P- Chrg Amt (+) : </td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td colspan="2">O- Chrg Amt (+) : </td>
                                <td>0</td>
                            </tr>
                            <?php foreach($totals as $total) {
                                if($total['liq_cancel'] == 1){
                                    $vat = 0;
                                }else{
                                    $vat = $vat + $total['vat'];
                                }
                                if($total['food_cancel'] == 1){
                                    $gst = 0;
                                } else{
                                    $gst = $gst + $total['gst'];
                                }
                                $packaging = $packaging + $total['packaging'];
                                $packaging_cgst = $packaging_cgst + $total['packaging_cgst'];
                                $packaging_sgst = $packaging_sgst + $total['packaging_sgst'];

                                $stax = $stax + $total['stax'];
                                if($total['food_cancel'] == 1){
                                    $total['ftotalvalue'] = 0;
                                }
                                if($total['liq_cancel'] == 1){
                                    $total['ltotalvalue'] = 0;
                                }
                                $discount = $discount + $total['ftotalvalue'] + $total['ltotalvalue'];
                                $grandtotal = $grandtotal + $total['grand_total'];
                                if($total['liq_cancel'] == 1 || $total['food_cancel'] == 1){
                                    $total['roundtotal'] = 0;
                                }
                                $roundoff = $roundoff + $total['roundtotal'];
                                $advance = $advance + $total['advance_amount'];
                            } ?>
                            <tr>
                                <td colspan="2">S- Chrg Amt (+) : </td>
                                <td><?php echo $stax ?></td>
                            </tr>
                            <tr>
                                <td colspan="2">Vat Amt (+) :</td>
                                <td><?php echo $vat ?></td>
                            </tr>
                            <tr>
                                <td colspan="2">GST Amt (+) :</td>
                                <td><?php echo $gst ?></td>
                            </tr>

                            <tr>
                                <td colspan="2">Packaging AMT (+) :</td>
                                <td><?php echo $packaging ?></td>
                            </tr>

                            <tr>
                                <td colspan="2">Packaging CGST (+) :</td>
                                <td><?php echo $packaging_cgst ?></td>
                            </tr>

                            <tr>
                                <td colspan="2">Packaging SGST (+) :</td>
                                <td><?php echo $packaging_sgst ?></td>
                            </tr>
                            <tr>
                                <td colspan="2">R-Off Amt (+) :</td>
                                <td><?php echo $roundoff / 2?></td>
                            </tr>
                            <tr>
                                <td colspan="2">Discount Amt (-) :</td>
                                <td><?php echo $discount ?></td>
                            </tr>
                            <tr>
                                <td colspan="2">P- Discount Amt (-) :</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td colspan="2">Cancel Bill Amt (-) :</td>
                                <td><?php echo $cancelamount ?></td>
                            </tr>
                            <tr>
                                <td colspan="2">Comp. Amt (-) :</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td colspan="2">Advance. Amt (-):</td>
                                <td><?php echo $advance ?></td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>Net Amount :</b></td>
                                <?php if($INCLUSIVE == 1){ ?>
                                    <td><b><?php echo ($amttotal + $stax + $roundoff) - $discount//$grandtotal ?></b></td>
                                <?php } else { ?>
                                    <td><b><?php echo ($amttotal + $stax + $vat + $gst + $roundoff + $packaging + $packaging_cgst + $packaging_sgst) - $discount//$grandtotal ?></b></td>
                                <?php } ?>
                            </tr>
                            <tr>
                                <td colspan="2"><b>Advance Amt (+) :</b></td>
                                <td><?php echo $advancetotal ?></td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>Grand Total (+) :</b></td>
                                <?php if($INCLUSIVE == 1){ ?>
                                    <td><b><?php echo ($amttotal + $stax + $roundoff) - $discount//$grandtotal ?></b></td>
                                <?php } else { ?>
                                    <td><b><?php echo ($amttotal + $stax + $vat + $gst + $roundoff + $packaging + $packaging_cgst + $packaging_sgst) - $discount//$grandtotal ?></b></td>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    <?php }?>
                </table>
            
        </div>          

<?php echo $footer; ?>


</div></body></html>


