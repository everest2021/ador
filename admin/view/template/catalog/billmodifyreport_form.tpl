<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-4">
						       		<div class="form-row">
									    <div class="col-sm-3">
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
				 	<div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 		<button id="export" type="button" class="btn btn-primary">Export</button>
				 	</div>
					<div class="col-sm-10 col-sm-offset-1">
					<center style = "display:none">
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Billwise Sales Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					<?php foreach ($final_data as $key => $value) {?>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;">Ref No</th>
							<th style="text-align: center;">T.No</th>
							<th style="text-align: center;">Waiter No.</th>
							<th style="text-align: center;">Bill Date</th>
							<th style="text-align: center;">Item</th>
							<th style="text-align: center;">Qty</th>
							<th style="text-align: center;">Rate</th>
							<th style="text-align: center;">Discount</th>
							<th style="text-align: center;">Total Payment</th>
							<th style="text-align: center;">Pay By</th>
							<th style="text-align: center;">Reason</th>
						</tr>
						<?php if(isset($_POST['submit'])){ ?>
							<?php foreach ($value['pre_itemdatas'] as $skey => $svalue) {?>
						  		<tr>
							  		<td><?php echo $svalue['ref_no'] ?></td>
							  		<td><?php echo $svalue['t_name'] ?></td>
							  		<td><?php echo $svalue['waiter_code'] ?></td>
							  		<td><?php echo $svalue['bill_date'] ?></td>
							  		<td><?php echo $svalue['item_name'] ?></td>
							  		<td><?php echo $svalue['qty'] ?></td>
							  		<td><?php echo $svalue['rate'] ?></td>
							  		<td><?php echo $svalue['discount_value'] ?></td>
							  		<td><?php echo $svalue['total_payment'] ?></td>
							  		<!-- <td><?php echo $svalue['ftotalvalue'] + $svalue['ltotalvalue'];?></td> -->
							  		<?php if($svalue['pay_cash'] != '0') { ?>
							  			<td>Cash</td>
							  		<?php } else if($svalue['pay_card'] != '0') { ?>
							  			<td>Card</td>
							  		<?php } else if($svalue['pay_online'] != '0') { ?>
							  			<td>Online</td>
							  		<?php } else if($svalue['pay_card'] != '0' && $svalue['pay_cash'] != '0') { ?>
							  			<td>Cash/Card</td>
							  		<?php } else if($svalue['pay_online'] != '0' && $svalue['pay_cash'] != '0') { ?>
							  			<td>Online/Cash</td>
							  		<?php } else if($svalue['pay_card'] != '0' && $svalue['pay_online'] != '0') { ?>
							  			<td>Card/Online</td>
							  		<?php } else { ?>
							  			<td>-</td>
							  		<?php } ?>
							  		<td><?php echo $svalue['modify_remark'] ?></td>
							  		 <?php } ?>
						  		</tr>
							  	
					  	<?php } ?>
					  </table>
					  <?php } ?>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/billmodifyreport/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});


		$('#export').on('click', function() {
		  var url = 'index.php?route=catalog/billmodifyreport/export&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>