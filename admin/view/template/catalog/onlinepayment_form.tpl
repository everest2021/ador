<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">

	<div class="page-header">

		<div class="container-fluid">

			<div class="pull-right">

			   <button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>

			   <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>

			   <h1><?php echo $heading_title; ?></h1>

			   <ul class="breadcrumb">

				   <?php foreach ($breadcrumbs as $breadcrumb) { ?>

					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

					<?php } ?>

			   </ul>

		</div>

  </div>

  <div class="container-fluid">

	   <?php if ($error_warning) { ?>

	   <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>

		   <button type="button" class="close" data-dismiss="alert">&times;</button>

	   </div>

		<?php } ?>

	   <div class="panel panel-default">

		   <div class="panel-heading">

			   <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>

			</div>

	   <div class="panel-body">

		   <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">

				<div class="form-group required">

					<label class="col-sm-3 control-label"><?php echo 'Payment Method '; ?></label>

					<div class="col-sm-3">

						<input type="text" name="payment_type" value="<?php echo $payment_type; ?>" placeholder="" class="form-control" />

						<?php if ($error_payment_type ) { ?>

						<div class="text-danger"><?php echo $error_payment_type ; ?></div>

						<?php } ?>

					</div>

				</div> 

				<div class="form-group required" style="display:none;">

					<label class="col-sm-3 control-label"><?php echo 'Rate Adult'; ?></label>

					<div class="col-sm-3">

						<input type="text" name="rate" value="<?php //echo $rate; ?>" placeholder="<?php //echo $entry_rate; ?>" class="form-control" />

						<?php if ($error_rate ) { ?>

						<div class="text-danger"><?php //echo $error_rate ; ?></div>

						<?php } ?>
 
					</div>

				</div>
			<!-- 	<div class="form-group required" style="display:none;">
					<label class="col-sm-3 control-label"><?php // echo 'Rate Child'; ?></label>
					<div class="col-sm-3">
						<input type="text" name="rate_child" value="<?php  //echo $rate_child; ?>" placeholder="<?php // echo 'Rate Child'; ?>" class="form-control" />
					</div>
				</div>

				<div class="form-group required" ; style ="display:none">

					<label class="col-sm-3 control-label"><?php //echo 'Total Amount'; ?></label>

					<div class="col-sm-3" ;style ="display:none">

						<input type="text" name="total_amount" value="<?php// echo $total_amount; ?>" placeholder="" class="form-control" /> -->

						<?php if ($error_capacity ) { ?>

						<div class="text-danger"><?php// echo $error_capacity; ?></div>

						<?php } ?>

					</div>

				</div>  

		  	</form>

		</div>

	  </div>

	</div>

  </div>

  <script>

  $('#colorSelector').ColorPicker({

	color: '#0000ff',

	onShow: function (colpkr) {

		$(colpkr).fadeIn(500);

		return false;

	},

	onHide: function (colpkr) {

		$(colpkr).fadeOut(500);

		return false;

	},

	onChange: function (hsb, hex, rgb) {

		$('#colorSelector').css('backgroundColor', '#' + hex);

		$('#colorSelector').val('#' + hex);

	},

	onSubmit: function (hsb, hex, rgb) {

		$('#colorSelector').css('backgroundColor', '#' + hex);

		$('#colorSelector').val('#' + hex);

	}

});

</script>

</div> 

<?php echo $footer; ?>