<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right" style = "display:none;"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
				<button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
			</div>
			<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
			<select class="pull-right" name="booking_status" id="input-booking_status" style="padding: 8px;padding-right: 15px;">
				<option  value="999" selected = "selected"><?php echo "All" ?></option>
				<?php foreach($statuss as $skey => $svalue){ ?>
				  <?php if($skey == $booking_status){ ?>
				  <option value="<?php echo $skey ?>" selected = "selected"><?php echo $svalue; ?></option>
				  <?php } else { ?>
				  <option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
				  <?php } ?>
				<?php } ?>
			</select>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
			</div>
			<div class="panel-body">
				<div class="row" >
					<?php foreach($bookingdata as $key => $value) { ?>
						<div class="col-md-3" style="border: 1px #000 solid">
							
							<div style="display: none">
								<label style="width: 100px" ><?php echo 'Males'; ?></label><span class="orderappmargin"><?php echo $value['males'] ?></span>
							</div>
							<div style="display: none;">
								<label style="width: 100px" ><?php echo 'Females'; ?></label><span class="orderappmargin"><?php echo $value['females'] ?></span>
							</div>
							<?
								if ($value['display'] == 1 ) {
									$display = 'Funshalla';
								}elseif($value['display'] == 2 ){
									$display = 'Turf';
								}else{
									$display = '';
								}
								
								// if ($test['event_type'] == 1) {
								// 	$type = 'Form 1';
								// }
								// elseif($test['event_type'] == 2){
								// 	$type = 'Form 2';
								// }else{
								// 	$type = '';
								// }
							?>
							<div>
								<label style="width: 100px" ></label><span class="orderappmargin"><b><?php echo ucfirst($value['display']['event_name']); ?></b></span>
							</div>
							<div>
								<label style="width: 100px" ><?php echo 'Name'; ?></label><span class="orderappmargin"><?php echo $value['customername'] ?></span>
							</div>
							<div>
								<label style="width: 100px" ><?php echo 'Contact'; ?></label><span class="orderappmargin"><?php echo $value['customercontact'] ?></span>
							</div>
							
							<div>
								<label style="width: 100px" ><?php echo 'Number of Person'; ?></label><span class="orderappmargin"><?php echo $value['adults'] ?></span>
							</div>
							<div>
								<label style="width: 100px" ><?php echo 'Narration'; ?></label><span class="orderappmargin"><?php echo $value['narration'] ?></span>
							</div>
							<?php foreach ($value['timedatas'] as $values => $keys) {  ?>
									<div>
										<label style="width: 100px" ><?php echo 'Date'; ?></label><span class="orderappmargin"><?php echo $values ?></span>
									</div>
									<?php foreach ($keys as $keyss => $valuess) { ?>
										<div>
											<label style="width: 100px" ><?php echo 'Time'; ?></label><span class="orderappmargin">
											<?php echo $valuess['time']; ?></span>									
										</div>
									<?php } ?>
							<?php } ?>
							<div>
								<label style="width: 100px"><?php echo 'Status'; ?></label><span class="orderappmargin"><?php echo $value['status']?></span>
							</div>
							<div style="padding-bottom: 15px">
							<?php if($value['status'] != '1'){ ?>
								<a href="<?php echo $value['except']; ?>"  class="btn btn-primary">Accept</a>
							<?php } ?>
								<a href="<?php echo $value['cancle']; ?>"  class="btn btn-primary">Cancel</a>
							<?php if($value['status'] != '2'){ ?>
								<button id="<?php echo $value['turf_id'] ?>" onclick="view(this.id)" class="btn btn-primary">View</button>
							<?php } ?>
							</div>
							<div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div id="myModal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<form method="POST" id="bookingform" class="form-horizontal popup_booking">
							<input type="hidden" id="viewid"/>

							<div class="form-group">
								<label class="col-sm-3 control-label" ><?php echo 'Customer Name'; ?></label>
								<div class="col-sm-9" style="">
									<input type="text" readonly="readonly" id="customername" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" style=""><?php echo 'Customer Contact'; ?></label>
								<div class="col-sm-9" style="">
									<input type="text" readonly="readonly" id="customercontact" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<input type="hidden" name="bookingid" id="bookingid" class="form-control" />
								<label class="col-sm-3 control-label adultlabel" style=""><?php echo 'Adults'; ?></label>
								<div class="col-sm-9" style="">
									<input type="text" name="adults" id="adults" class="form-control" />
								</div>
							</div>
							<div class="form-group narration">
								<label class="col-sm-3 control-label" style=""><?php echo 'Narration'; ?></label>
								<div class="col-sm-9" style="">
									<input type="text" name="narration" id="narration" class="form-control" />
								</div>
							</div>
							<div class="form-group kidsdiv">
								<label class="col-sm-3 control-label" style=""><?php echo 'Kids'; ?></label>
								<div class="col-sm-9" style="">
									<input type="text" name="kids" id="kids" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" style=""><?php echo 'Type'; ?></label>
								<div class="col-sm-9" style="">
									<input readonly="readonly" type="text" name="type"  id="type" class="form-control" />
								</div>
							</div>
							<div class="form-group booking_class">
								<label class="col-sm-3 control-label" ><?php echo 'Venue'; ?></label>
								<div class="col-sm-9" >
									<select name="venue_id" id="venue_id" class="form-control" style="">
									  	<?php foreach($venue_data as $gkey => $gvalue) { ?>
											<option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
										<?php } ?>
									</select>
									<input type="hidden" name="venue" id="venue" value="" size="50" />
								</div>
							</div>
							<div class="form-group booking_class">
							   <label class="col-sm-3 control-label" ><?php echo 'Meal Type'; ?></label>
							   <div class="col-sm-9">
									<select name="meal_id" id="meal_id" class="form-control" style="">
									</select>
									<input type="hidden" name="meal" id="meal" value="" size="50" />
								</div>
							</div>
							<div id="mealitems" style="text-align:center;">
							</div>
							<div id="category" style="text-align:center;">
							</div>

							
							
							<div class="form-group" id ="outerhtml">
								
							</div>
							
							<div class="form-group">
								<label class="col-sm-3"></label>
								<div class="col-sm-9" style="">
									<span class="error error_booking_done_local" style="color: red;"></span>
								</div>
							</div>
						  	<div class="modal-footer">
								<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
								<button style = "display:none;" type="button" class="btn btn-primary" id="savedata">Save</button>
						  	</div>
						</form>
			  		</div>
			 	</div>
			</div>
		</div>
		<script type="text/javascript">	
			$('#date_from').datepicker();
			$('#button-filter').on('click', function() {
				var url = 'index.php?route=catalog/turfbooking&token=<?php echo $token; ?>';
				var booking_status = $('select[name=\'booking_status\']').val();
				if (booking_status) {
					url += '&booking_status=' + encodeURIComponent(booking_status);
				}
				location = url;
			});

			function view(id){
				$('#meal_id').html('');
				$('#viewid').val(id);
				//alert(id);
				$.ajax({
					url:'index.php?route=catalog/turfbooking/view&token=<?php echo $token; ?>&id='+id,
					Type:"POST",
					dataType:"json",
					success: function(json) { 
						$('#adults').val(json.adults);
						$('#narration').val(json.narration);
						$('#kids').val(json.kids);
						$('#customername').val(json.customername);
						$('#customercontact').val(json.customercontact);
						$('#type').val(json.type);
						if(json.is_hidden == 0){
							$('.booking_class').show();
							$('.kidsdiv').show();
							$('.adultlabel').html('Adults');
							$('.narration').hide();
							$('#venue_id').val(json.venue_id);
							$('#venue').val(json.venue);
							$('#mealitems').show();
							if(json.meal_exist == 1){
								$.each(json.meal_datas, function (i, item) {
				  					$('#meal_id').append($('<option>', { 
					  					value: i,
					  					text : item 
				  					}));
								});
								$('#meal_id').val(json.meal_id);
								$('#meal_id option[value="'+json.meal_id+'"]').attr("selected", "selected");
								$('#meal').val(json.venue);
								activity_array = json.activity_id;
								for(i=0; i < activity_array.length; i++){
								  	$("#activity option[value='" + activity_array[i] + "']").attr("selected", 1);
								  	$("#activity").multiselect("refresh");
								}
								$('#mealitems').html('');
				               	$('#mealitems').append(json.selected_meal_items);
							}
						} else {
							$('.booking_class').hide();
							$('.kidsdiv').hide();
							$('.adultlabel').html('Number of person');
							$('.narration').show();
							$('#mealitems').hide();
						}
						$('#date_from').val(json.date_from);
						$('#time_from').val(json.time_from);
						$('#time_to').val(json.time_to);
						$('#bookingid').val(id);
						meal_name = $('#meal_id option:selected').text();
						$('#meal').val(meal_name);
						$('#outerhtml').html(json.html);
						$('#category').html(json.html_1);
						$('#myModal').modal({show:true});
					}
				});
				$('#bookingform').attr('action','index.php?route=catalog/turfbooking/updatedata&token=<?php echo $token; ?>&id='+id);
			}

			$('#savedata').click(function(){
				action = 'http://funshalla.in/hotelform1/hotel_api/calculate_price_api_web.php';
				var status = 0;
            	$.post(action, $('#bookingform').serialize()).done(function(data) {
                	var json = JSON.parse(data);
                    $('.error_meal_items').html('');
                    if(json.error_meal_items){
                    	status = 1;
                    	for (var key in json.error_meal_items) {
                            if (json.error_meal_items.hasOwnProperty(key)) {
                                var val = json.error_meal_items[key];
                                $('.error_meal_items').each(function( index ) {
                                	idss = $(this).attr('id');
									s_id = idss.split('_');
									if(key == s_id[3]){
										$('#error_meal_items_'+s_id[3]).html(val);
									}
                                });
                            }
                    	}
                    }

                    $('.error_meal_items1').html('');
                    if(json.error_meal_items1 == 1){
                    	status = 1;
                    	$('.error_meal_items1').html("Please Select Meal Items");
                    }

                    $('.error_booking_done_local').html('');
                    if(json.error_booking_done_local == 1){
                    	status = 1;
                        $('.error_booking_done_local').html('Booking For this Date and Time Already Done');
                    }

                    if(status == 0){
                    	$('#bookingform').submit();
                	}
            	});
			});
		   
			$('#activity').multiselect({
				includeSelectAllOption: true,
				enableCaseInsensitiveFiltering : true,
				maxHeight: 235,
			});
			
			jQuery.browser = {};
			(function () {
				jQuery.browser.msie = false;
				jQuery.browser.version = 0;
				if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
					jQuery.browser.msie = true;
					jQuery.browser.version = RegExp.$1;
				}
			})();

			$('#meal_id').on('change',function(){
				meal_name = $('#meal_id option:selected').text();
				$('#meal').val(meal_name);
				id = $('#meal_id').val();
 				viewid = $('#viewid').val();
                $('#mealitems').html('');
                $.ajax({
                    url:'index.php?route=catalog/bookingapp/selected_meal_items_ajax&token=<?php echo $token; ?>&meal_id='+id+'&id='+viewid,
                    type:"POST",
                    dataType:'json',
                    success:function(json){
                    	$('#mealitems').append(json['html']);
                    }
                });
			});
		
	  		/*$('#venue_id').on('change', function() {
	  			venue_name = $('#venue_id option:selected').text();
  				$('#venue').val(venue_name);
	  			venue_id = $('#venue_id').val();
	  			$.ajax({
					url: 'index.php?route=catalog/bookingapp/meal&token=<?php echo $token; ?>&venue_id=' +  encodeURIComponent(venue_id),
					dataType: 'json',
					success: function(json) {   
		  				$('#meal_id').find('option').remove();
		  				if(json){
							$.each(json, function (i, item) {
			  					$('#meal_id').append($('<option>', { 
				  					value: item.id,
				  					text : item.name 
			  					}));
							});
		  				}
					}
	  			});
			});*/
		</script>
	</div>		
<?php echo $footer; ?>