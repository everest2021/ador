<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" id="form" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Data</b></h4></center><br>
					       		<div class="form-row">
								    <div class="col-sm-2 col-sm-offset-2" style="margin-left: 41.5%";>
								    	<label>Date</label>
								     	<input type="text" name='filter_startdate' value="<?php echo $filter_startdate?>" class="form-control form_datetime" placeholder="Start Date">
								    </div>

								   <!--  <div class="col-sm-2">
									    <label>Start Time</label>	
									    <input type="time" name="filter_starttime" value="<?php echo $filter_starttime?>" class="form-control" placeholder="Start Time">
									</div>

									<div class="col-sm-2">
									    <label>End Time</label>	
									    <input type="time" name="filter_endtime" value="<?php echo $filter_endtime?>" class="form-control" placeholder="End Time">
									</div> -->
								  
									<div class="col-sm-12">
										<br>
										<center><a id ="filter" class="btn btn-primary" value="Show">Submit</a></center>
									</div>
								</div>
							</div>
					    </div>
					</div>
				 	<!-- <div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 	</div> -->
					<div class="col-sm-6 col-sm-offset-3">
						<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
						<?php date_default_timezone_set("Asia/Kolkata");?>
						<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
						<center><h3><b>Hourly Report</b></h3></center>
						<h3>From : <?php echo $filter_startdate; ?></h3>
						<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $filter_enddate; ?></h3><br>
						<?php if ($all_cap) {?>
						  	<table class="table table-bordered table-hover" style="text-align: center;">
								<thead>
									
									<tr>
										<td style="text-align: center;"><?php echo 'Date'; ?></td>
										
										<td style="text-align: center;"><?php echo 'Total Bill'; ?></td>		            
										<td style="text-align: center;"><?php echo 'Total'; ?></td>
									</tr>
									<tr>
										<!-- <td colspan="3" style="text-align: center;"><?php echo '1:00 AM To 2:00 AM'; ?></td> -->
									</tr>
								</thead>
								<tbody>
										<?php foreach($cap_datas as $fkeys => $fvalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $fvalues['date'];?></td></b>
												</tr>

												<tr>	
													<td style="text-align: center;" class="left">
														<?php echo '1:00 AM To 2:00 AM'; ?>
													</td>		          
													<td style="text-align: center;" class="left">
														<?php echo $fvalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $fvalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>
								
								<tbody>
										<?php foreach($cap_datas1 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '2:00 AM To 3:00 AM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>

								<tbody>
										<?php foreach($cap_datas2 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '3:00 AM To 4:00 AM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>
						
								<tbody>
										<?php foreach($cap_datas3 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '4:00 AM To 5:00 AM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>
							
								<tbody>
										<?php foreach($cap_datas4 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '5:00 AM To 6:00 AM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>
							
								<tbody>
										<?php foreach($cap_datas5 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '6:00 AM To 7:00 AM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>
							
								<tbody>
										<?php foreach($cap_datas6 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '7:00 AM To 8:00 AM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>
							
								<tbody>
										<?php foreach($cap_datas7 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '8:00 AM To 9:00 AM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>
							
								<tbody>
										<?php foreach($cap_datas8 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>	

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '9:00 AM To 10:00 AM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>
							
								<tbody>
										<?php foreach($cap_datas9 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>	

												<tr>			          
													<td style="text-align: center;" class="left"><?php echo '10:00 AM To 11:00 AM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>
							
								<thead>
									
									
								</thead>
								<tbody>
										<?php foreach($cap_datas10 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>	

												<tr>			          
													<td style="text-align: center;"><?php echo '11:00 AM To 12:00 AM'; ?>
													</td>
													
													<td style="text-align: center;" >
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" >
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>
							
	
								<tbody>
										<?php foreach($cap_datas11 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>	

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '12:00 PM To 1:00 PM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>

							
								<tbody>
										<?php foreach($cap_datas12 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>	

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '1:00 PM To 2:00 PM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>

								
								<tbody>
										<?php foreach($cap_datas13 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>	

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '2:00 PM To 3:00 PM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>

								<tbody>
										<?php foreach($cap_datas14 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>	
												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '3:00 PM To 4:00 PM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>

								<tbody>
										<?php foreach($cap_datas15 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>	

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '4:00 PM To 5:00 PM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>

							
								<tbody>
										<?php foreach($cap_datas16 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>	

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '5:00 PM To 6:00 PM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>

								<tbody>
										<?php foreach($cap_datas17 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>	

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '6:00 PM To 7:00 PM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>

					
								<tbody>
										<?php foreach($cap_datas18 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>	

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '7:00 PM To 8:00 PM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>

								<tbody>
										<?php foreach($cap_datas19 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>	

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '8:00 PM To 9:00 PM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>

							
								<tbody>
										<?php foreach($cap_datas20 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>	

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '9:00 PM To 10:00 PM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>

								<thead>
									<tr>
										<td colspan="3" style="text-align: center;"></td>
									</tr>	

								</thead>
								<tbody>
										<?php foreach($cap_datas21 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '10:00 PM To 11:00 PM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>

								<tbody>
										<?php foreach($cap_datas22 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>

												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '11:00 PM To 12:00 AM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>

								<tbody>
										<?php foreach($cap_datas23 as $ekeys => $evalues){ ?>
												<tr><b>
													<td colspan="3" style="text-align: center;font-weight: bold;">
														<?php echo $evalues['date'];?></td></b>
												</tr>
												<tr>			          
													<td style="text-align: center;" class="left">
														<?php echo '12:00 AM To 1:00 AM'; ?>
													</td>
													
													<td style="text-align: center;" class="left">
														<?php echo $evalues['order_id']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $evalues['grand_total']; ?>
													</td>
												</tr>
										<?php } ?>
				
								</tbody>
								<thead>
									
										<tr>
											<td style="text-align: center;"><?php echo 'TOTAL BILL'; ?></td>
											<td colspan="3" style="text-align: center;"><?php echo $billno ; ?></td>
										</tr>	
						
										<tr>
											<td style="text-align: center;"><?php echo 'GRAND TOTAL'; ?></td>
											<td colspan="3" style="text-align: center;"><?php echo $total; ?></td>	
										</tr>
								</thead>
							</table><br>
						<?php }  ?>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker({
			dateFormat: 'dd-mm-yy',			
	  	});

		$('#filter').on('click', function() {
		  	var url = 'index.php?route=catalog/hourly_report&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
			var filter_starttime = $('input[name=\'filter_starttime\']').val();
			var filter_endtime = $('input[name=\'filter_endtime\']').val();

			if (filter_startdate) {
				url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}
			if (filter_starttime) {
				url += '&filter_starttime=' + encodeURIComponent(filter_starttime);
		  	}
		  	if (filter_endtime) {
				url += '&filter_endtime=' + encodeURIComponent(filter_endtime);
		  	}
		  	location = url;
		});

		function close_fun_1(){
			window.location.reload();
		}

		
		$('#type').on('change', function() {
			$('#form').submit();
		});

		$('#filter_name').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
		  		filter_type = $('#filter_type').val();
				$.ajax({
			  		url: 'index.php?route=catalog/captain_and_waiter_sale/name&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_type=' +  encodeURIComponent(filter_type),
			  		dataType: 'json',
			  		success: function(json) {   
						response($.map(json, function(item) {
				  			return {
								label: item.name,
								value: item.id,
				  			}
						}));
			  		}
				});
		  	}, 
		  	select: function(event, ui) {
		  		$('#filter_id').val(ui.item.value);
		  		$('#filter_name').val(ui.item.label);
		  		return false;
		  	}
		});
	</script>

	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>