<html>
	<head>
		<title>Barcode</title>
		<style>
			@media print {
			    footer {page-break-after: always;}
			}
		</style>
	</head>
	<body>
		<table style="">
			<tr>
				<td style="font-size:14px;">
					<b>FUNSHALLA TEST</b>
				</td>
			</tr>
			<tr>
				<td style="font-size:12px;">
					<b>PASSPORT NO</b>
				</td>
			</tr>
			<tr>
				<td>
					<img style="height:60px; width:150px;" src='https://barcode.tec-it.com/barcode.ashx?data=10000001&code=Code128&multiplebarcodes=true&translate-esc=false&unit=Fit&dpi=96&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0' alt='Barcode Generator TEC-IT'/>
				</td>
			</tr>
		</table>
		<footer></footer>
		
		<?php for($i=$start; $i<=$end; $i++) { ?>
			<table style="padding-top:5px;">
				<tr>
					<td style="font-size:14px;">
						<b>FUNSHALLA</b>
					</td>
				</tr>
				<tr>
					<td style="font-size:12px;">
						<b>PASSPORT NO</b>
					</td>
				</tr>
				<tr>
					<td>
						<img style="height:60px; width:150px;" src='https://barcode.tec-it.com/barcode.ashx?data=<?php echo $i; ?>&code=Code128&multiplebarcodes=true&translate-esc=false&unit=Fit&dpi=96&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0' alt='Barcode Generator TEC-IT'/>
					</td>
				</tr>
			</table>
			<footer></footer>
		<?php } ?>

	</body>
</html>