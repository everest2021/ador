<?php echo $header; ?>
<div id="content" class="sucess" style="overflow-y: hidden;">
  <div class="page-header">
  </div>
  <div class="container-fluid">
	<?php $tab_index = 1; ?>
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-order" class="form-horizontal">
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 style= control-label">Table No :</label>
					<div class="col-sm-3">
				  		<input autocomplete="off" type="text" name="transfer_table" id="transfer_table" value="" placeholder="<?php echo 'Table'; ?>" class="form-control inputs" />
			  		</div>
			  		<div class="col-sm-2" id="save_btn">
		  				<a id="form-order"  onclick="check()" title="Update" class="btn btn-primary" style="font-size:12px;">Check</a>
		  				<a id="form-order"  onclick="save()" title="Update" class="btn btn-primary" style="font-size:12px;margin-left: 72px;margin-top: -49px;">Clear</a>
		  				<button  id="print" type="button" class="btn btn-primary" style="font-size:12px;margin-left: 140px;margin-top: -82px;">Print</button>
			  		</div>
		  		</div>
			</div>
	    </form>
	</div>
	<div id="dialog-form_select_qty" title="Select Quantity">
		<div class="col-md-4">
						<table id="data" class="table table-bordered" style=" width: 70%;">
							<tr>
								<th>Item Name</th>
								<th>Qty</th>
								<th>Rate</th>
							</tr>
						</table>
					</div>
	</div>
</div>

<script type="text/javascript">
	$('#transfer_table').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});
</script>

<script type="text/javascript">

function save(){
	var trans_table = $('input[name=\'transfer_table\']').val();
  	
	$.ajax({
		type: "POST",
		url: 'index.php?route=catalog/cleartable/cleartbls&token=<?php echo $token; ?>&tbl=' +trans_table,
		dataType: 'json',
		data: {"data":"check"},
		success: function(data){
			alert('Table Data deleted Successfully');
		}
	});
}

function check(){
	var trans_table = $('input[name=\'transfer_table\']').val();
  	
	$.ajax({
		type: "POST",
		url: 'index.php?route=catalog/cleartable/checktbls&token=<?php echo $token; ?>&tbl=' +trans_table,
		dataType: 'json',
		data: {"data":"check"},
		success: function(json){
			console.log(json);
			//alert('Table Data deleted Successfully');
			$.each(json,function(i,fooditem){
	   				var $tr = $('<tr>').append(
	   								$('<td>').text(fooditem.name),
	   								$('<td>').text(fooditem.qty),
	   								$('<td>').text(fooditem.rate),
	   							);
	   					$('#data').append($tr);
	   			});
		}
	});
}

$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/cleartable/prints&token=<?php echo $token; ?>';

		  var trans_table = $('input[name=\'transfer_table\']').val();
		  //console.log(trans_table);

		  if (transfer_table) {
			  url += '&tbl=' + encodeURIComponent(trans_table);
		  }
		    location = url;
		});

</script>


<?php echo $footer; ?>