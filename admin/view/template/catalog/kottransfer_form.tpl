<?php echo $header; ?>
<div id="content" class="sucess" style="overflow-y: hidden;">
  <div class="page-header">
  </div>
  <div class="container-fluid">
	<?php $tab_index = 1; ?>
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-order" class="form-horizontal">
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 style= control-label">Table No :</label>
					<div class="col-sm-2">
				  		<input autocomplete="off" type="text" name="table" value="<?php echo $table ?>" placeholder="<?php echo 'Table'; ?>" id="input-t_number" class="form-control inputs" />
				  		<?php $tab_index ++; ?>
				  		<input type="hidden" name="table_id" value="<?php echo $table_id ?>" id="input-t_number_id" class="form-control inputs" />
			  		</div>
			  		<label class="col-sm-3 style= control-label">To Transfter Table No :</label>
					<div class="col-sm-3">
				  		<input autocomplete="off" type="text" name="transfer_table" value="<?php echo $table ?>" placeholder="<?php echo 'Table'; ?>" id="input-t_number" class="form-control inputs" />
				  		<?php $tab_index ++; ?>
				  		<input type="hidden" name="transfer_table_id" value="<?php echo $table_id ?>" id="input-transfer_t_number_id" class="form-control inputs" />
			  		</div>
			  		<div class="col-sm-2" id="save_btn">
		  				<a id="form-order"  onclick="save()" title="Update" class="btn btn-primary" style="font-size:12px;">Update</a>
			  		</div>
		  		</div>
			</div>
			<div class="panel-body" style="padding-bottom: 0%;">
				<div style="display:inline-block; width:55%;float:left;">
			  		<div style="width:100%;height:20%;">
						<div class="col-sm-3">
					  		<input type="text" readonly="readonly" name="location" value="<?php echo $loc ?>" placeholder="<?php echo 'location'; ?>" id="input-location" class="form-control inputs list" style="display: none"/>
					  		<input type="hidden" name="location_id" value="<?php echo $locid ?>" id="input-location_id" class="form-control" />
						</div>
			  		</div>
			  		<div style="width:100%; float:left;padding-top: 1%;padding-bottom: 5%;overflow-y: auto;height:500px;">
						<table style="" class="table table-bordered table-hover ordertab">
					  		<tr>
								<td style="padding-top: 2px;padding-bottom: 2px;">No.</td>
								 <td style="width:0%;padding-top: 2px;padding-bottom: 2px;"></td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;">Code</td>
								<td style="width:0%;padding-top: 2px;padding-bottom: 2px;">Name</td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;">Qty</td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;">Rate</td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;">Amt</td>
								<td style="width:20%;padding-top: 2px;padding-bottom: 2px;">KOT</td>
								<td style="width:20%;padding-top: 2px;padding-bottom: 2px;">Reason</td>
								<td style="display: none;">Delete</td>
					  		</tr>
						</table>
			  		</div>
				</div>
				<div style="display:inline-block; width:45%;float:left;padding-left: 10px;">
			  		<div style="width:100%;height:20%;">
						<div class="col-sm-3">
					  		<input type="text" readonly="readonly" name="transfer_location" value="<?php echo $loc ?>" placeholder="<?php echo 'location'; ?>" class="form-control inputs list" style="display: none"/>
					  		<input type="hidden" name="transfer_location_id" id="input-transfer_location_id" value="<?php echo $locid ?>" class="form-control" />
						</div>
			  		</div>
			  		<div style="width:100%; float:left;padding-top: 1%;padding-bottom: 5%;overflow-y: auto;height:500px;">
						<table style="" class="table table-bordered table-hover ordertabtransfer">
					  		<tr>
								<td style="padding-top: 2px;padding-bottom: 2px;">No.</td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;">Code</td>
								<td style="width:0%;padding-top: 2px;padding-bottom: 2px;">Name</td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;">Qty</td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;">Rate</td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;">Amt</td>
								<td style="width:20%;padding-top: 2px;padding-bottom: 2px;">KOT</td>
								<td style="display: none;">Delete</td>
					  		</tr>
						</table>
			  		</div>
				</div>
			</div>
	    </form>
	</div>
	<div id="dialog-form_select_qty" title="Select Quantity">
	</div>
</div>

<script type="text/javascript">
var tab_index = '<?php echo $tab_index; ?>';

$('.readonly').attr('readonly',"readonly");

</script>
<script type="text/javascript">
function findtable(){
	var filter_table = $('input[name=\'table\']').val();
	if(filter_table != '' && filter_table != undefined){
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/kottransfer/findtable&token=<?php echo $token; ?>&filter_tname=' +filter_table,
			dataType: 'json',
			data: {"data":"check"},
			success: function(data){
				//alert(data.name);
				if(data.status == 1){
					$('input[name=\'table\']').val(data.name);
					$('input[name=\'table_id\']').val(data.table_id);
					$('input[name=\'location\']').val(data.loc_name);
					$('input[name=\'location_id\']').val(data.loc_id);
					$('input[name=\'rate_id\']').val(data.rate_id);
					tablein();
					$('#input-waiter').focus();
				} else {
					alert('Table Does Not Exist');
					window.location.reload();
					return false;
				}
			}
		});
	}
	return false;
}

function findtabletransfer(){
	var filter_table = $('input[name=\'transfer_table\']').val();
	if(filter_table != '' && filter_table != undefined){
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/kottransfer/findtable&token=<?php echo $token; ?>&filter_tname=' +filter_table,
			dataType: 'json',
			data: {"data":"check"},
			success: function(data){
				//alert(data.name);
				if(data.status == 1){
					$('input[name=\'transfer_table\']').val(data.name);
					$('input[name=\'transfer_table_id\']').val(data.table_id);
					$('input[name=\'transfer_location\']').val(data.loc_name);
					$('input[name=\'transfer_location_id\']').val(data.loc_id);
					$('input[name=\'rate_id\']').val(data.rate_id);
					transfertablein();
					$('#input-waiter').focus();
				} else {
					alert('Table Does Not Exist');
					window.location.reload();
					return false;
				}
			}
		});
	}
	return false;
}

function tablein(){
    var location_id = $('input[name=\'location_id\']').val();
    var table_id = $('input[name=\'table_id\']').val();
    $.ajax({
		type: "POST",
		url: 'index.php?route=catalog/kottransfer/tableinfo&token=<?php echo $token; ?>&lid='+location_id+'&tid='+table_id+'&tab_index='+tab_index,
		dataType: 'json',
		data: {"data":"check"},
		success: function(data){
			var html =data.html;
			ur = '';
			if(data.status == 1){
				if(html != '') {
			  		$('.ordertab').html('');
			  		$('.ordertab').append(data.html);
			  		$('.readonly').attr('readonly',"readonly");
				} else {
					html1 = '<tr>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 5%;">No.</td>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 5%;">Code</td>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 30%;">Name</td>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 7%;">Qty</td>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 10%;">Rate</td>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 8%;">Amt</td>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 10%;">KOT</td>';
					html1 += '</tr>';
				 	html1 += '<tr id="re_1">';
						html1 += '<td style="padding-top: 2px;padding-bottom: 2px;font-size: 16px;" class="r_1" >1';
						html1 += '</td>';
						html1 += '<td class="r_1" style="padding-top: 2px;padding-bottom: 2px;">';
							html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="number" class="inputs code form-control" readonly="readonly" name="po_datas[1][code]" id="code_1" />';
							tab_index ++;
						html += '</td>';
						html1 += '<td style="padding-top: 2px;padding-bottom: 2px;" class="r_1">';
							html1 += '<input tabindex = "'+tab_index+'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs names form-control" name="po_datas[1][name]" id="name_1" />';
							tab_index ++;
						html1 += '</td>';
						html1 += '<td style="padding-top: 2px;padding-bottom: 2px;width:8%;" class="r_1">';
							<?php if($display_type == '1') { ?>
								html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" readonly="readonly" type="number" class="inputs qty form-control" name="po_datas[1][qty]" id="qty_1" /></td>';
							<?php }  else { ?>
								html1 += '<input tabindex = "'+tab_index+'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" onclick="select_qty(1)" class="inputs qty form-control screenquantity" name="po_datas[1][qty]" id="qty_1" /></td>';	
							<?php } ?>
							tab_index ++;
						html1 += '</td>';
						html1 += '<td style="padding-top: 2px;padding-bottom: 2px;" class="r_1">';
						<?php if($display_type == '1') { ?>
							html1 += '<input tabindex = "'+tab_index+'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" type="number" class="inputs rate form-control" name="po_datas[1][rate]" id="rate_1" />';
						<?php }  else { ?>
							html1 += '<input tabindex = "'+tab_index+'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" onclick="select_rate(1)"  class="inputs rate form-control screenrate" name="po_datas[1][rate]" id="rate_1" />';
						<?php } ?>
							tab_index ++;
						html1 += '</td>';
						html1 += '<td style="padding-top: 2px;padding-bottom: 2px;width: 8%" class="r_1">';
							html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;background-color: #f2f2f2;font-size: 16px;" type="text" class="inputs form-control" readonly="readonly" name="po_datas[1][amt]" id="amt_1" />';
						html1 += '</td>';		
						html1 += '<td class="r_1" style="padding-top: 2px;padding-bottom: 2px;width: 25%">';
							html1 += '<input tabindex = "'+tab_index+'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs lst form-control" name="po_datas[1][message]" id="message_1" /><input style="display:none" type="text" name="po_datas[1][is_liq]" value = "0" id="is_liq_1" />';
							tab_index ++;
						html1 += '</td>';
			  		html1 += '</tr>';
			  		$('.ordertab').html('');
			  		$('.ordertab').append(html1);
				}
			} else {
				alert('Table Does Not Exist / Already Billed');
				return false;
			}
		}
  	});
}

function transfertablein(){
	//alert('in');
    var location_id = $('input[name=\'transfer_location_id\']').val();
    var table_id = $('input[name=\'transfer_table_id\']').val();
    $.ajax({
		type: "POST",
		url: 'index.php?route=catalog/kottransfer/tableinfotransfer&token=<?php echo $token; ?>&lid='+location_id+'&tid='+table_id+'&tab_index='+tab_index,
		dataType: 'json',
		data: {"data":"check"},
		success: function(data){
			var html =data.html;
			//alert(html)
			ur = '';
			if(data.status == 1){
				if(html != '') {
			  		$('.ordertabtransfer').html('');
			  		$('.ordertabtransfer').append(data.html);
			  		$('.readonly').attr('readonly',"readonly");
				} else {
						html1 = '<tr>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 5%;">No.</td>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 5%;">Code</td>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 30%;">Name</td>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 7%;">Qty</td>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 10%;">Rate</td>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 8%;">Amt</td>';
						html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 10%;">KOT</td>';
					html1 += '</tr>';
				 	html1 += '<tr id="re_1">';
						html1 += '<td style="padding-top: 2px;padding-bottom: 2px;font-size: 16px;" class="r_1" >1';
						html1 += '</td>';
						html1 += '<td class="r_1" style="padding-top: 2px;padding-bottom: 2px;">';
							html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="number" class="inputs code form-control" readonly="readonly" name="po_datas[1][code]" id="code_1" />';
							tab_index ++;
						html += '</td>';
						html1 += '<td style="padding-top: 2px;padding-bottom: 2px;" class="r_1">';
							html1 += '<input tabindex = "'+tab_index+'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs names form-control" name="po_datas[1][name]" id="name_1" />';
							tab_index ++;
						html1 += '</td>';
						html1 += '<td style="padding-top: 2px;padding-bottom: 2px;width:8%;" class="r_1">';
							<?php if($display_type == '1') { ?>
								html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" readonly="readonly" type="number" class="inputs qty form-control" name="po_datas[1][qty]" id="qty_1" /></td>';
							<?php }  else { ?>
								html1 += '<input tabindex = "'+tab_index+'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" onclick="select_qty(1)" class="inputs qty form-control screenquantity" name="po_datas[1][qty]" id="qty_1" /></td>';	
							<?php } ?>
							tab_index ++;
						html1 += '</td>';
						html1 += '<td style="padding-top: 2px;padding-bottom: 2px;" class="r_1">';
						<?php if($display_type == '1') { ?>
							html1 += '<input tabindex = "'+tab_index+'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" type="number" class="inputs rate form-control" name="po_datas[1][rate]" id="rate_1" />';
						<?php }  else { ?>
							html1 += '<input tabindex = "'+tab_index+'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" onclick="select_rate(1)"  class="inputs rate form-control screenrate" name="po_datas[1][rate]" id="rate_1" />';
						<?php } ?>
							tab_index ++;
						html1 += '</td>';
						html1 += '<td style="padding-top: 2px;padding-bottom: 2px;width: 8%" class="r_1">';
							html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;background-color: #f2f2f2;font-size: 16px;" type="text" class="inputs form-control" readonly="readonly" name="po_datas[1][amt]" id="amt_1" />';
						html1 += '</td>';		
						html1 += '<td class="r_1" style="padding-top: 2px;padding-bottom: 2px;width: 25%">';
							html1 += '<input tabindex = "'+tab_index+'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs lst form-control" name="po_datas[1][message]" id="message_1" /><input style="display:none" type="text" name="po_datas[1][is_liq]" value = "0" id="is_liq_1" />';
							tab_index ++;
						html1 += '</td>';
						html1 += '<input type="hidden" name="orderidtransfer" value=""  id="orderidtransfer" />';
			  		html1 += '</tr>';
			  		$('.ordertabtransfer').html('');
			  		$('.ordertabtransfer').append(html1);
				}
			} else if(data.status == 2){
				alert('Table Is Allready Running...!!!')
				return false;
			} else {
				// alert('Table Does Not Exist / Already Billed');
				// return false;
			}
		}
  	});
}

function close_fun_1(){
	window.location.reload();
}
</script>

<script type="text/javascript">

$(document).ready(function() {
	$("#save_btn").hide();
  $('#input-t_number').focus();
});
</script>

<script type="text/javascript"><!--
dialog = $("#dialog-form").dialog({
	autoOpen: false,
	height: 750,
	width: 750,
	modal: true,
	buttons: {
		Cancel: function() {
			dialog.dialog( "close" );
		}
	},
});

dialog_select_qty = $("#dialog-form_select_qty").dialog({
	autoOpen: false,
	height: 300,
	width: 200,
	modal: true,
	buttons: {
		Done: function() {
			//$(this).dialog("close");
			var increment = $("#autoincrementqty").val();
			$('#qty_'+increment).val($("#selectquantity").val());
			//$('.screenquantity').val($("#selectquantity").val());
			$('.qty').trigger('change');
			dialog_select_qty.dialog("close");
			//html = '<a  data-toggle="tooltip" onclick="check()" title="check" class="btn btn-primary check" style="margin-top: 10%">Check</a>';
			//$('.custcheck').append(html);
		}
	},
});

function close_fun(){
  window.location.reload();
}

function select_qty(id){
	//html = '<h4 style="font-weight: bold;">More Option</h4>';
	html = '<table class="table-hover" style="text-align: center;">';
		html += '<tr>';
			html += '<td><h1 onclick="increase_qty()">+</h1></td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td>';
			html += '<input style="padding: 0px 1px;" value="0" id="selectquantity" class="inputs form-control"/>';
			html += '<input type="hidden" style="padding: 0px 1px;" value="'+id+'" id="autoincrementqty"/>';
			html += '</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td><h1 onclick="decrease_qty()">-</h1></td>';
		html += '</tr>';
	html += '</table>';
	dialog_select_qty.dialog("open");
	$('#dialog-form_select_qty').html(html)
}

function increase_qty(){
	var counter = $('#selectquantity').val();
	var existingqty = $('#qty'+counter).val();
	counter++;
	if(counter > existingqty){
		alert(existingqty);
		alert("Quantity cannot be increase");
		$('#selectquantity').val(existingqty);
	}
	else{
		$('#selectquantity').val(counter);
	}
}

function decrease_qty(){
	var counter = $('#selectquantity').val();
	counter--;
	if(counter < 0){
		alert("Value cananot be negative");
	}
	else{
		$('#selectquantity').val(counter);
	}
}

</script>

<script type="text/javascript">
	
$(document).on('keydown', '.inputs', function(e) {
  	var code = (e.keyCode ? e.keyCode : e.which);
	if (code == 13) {
  		var name = $(this).attr('name'); 
  		var class_name = $(this).attr('class'); 
  		var id = $(this).attr('id'); 
  		event.preventDefault();
  		if(name == 'table'){
	  		findtable();
  		}
  		//alert(name);
  		if(name == 'transfer_table'){
  			$("#save_btn").show();
  			findtabletransfer();
  		}
	}
});


$(document).on('change', '.qty', function(e) {
	console.log("changed");
  	idss = $(this).attr('id');
  	s_id = idss.split('_');
  	is_liq1 =$('#is_liq_'+s_id[1] ).val();

  	var existingqty = $('#pre_qty'+s_id[1]).val();
  	var currentqty = parseFloat($('#qty_'+s_id[1]).val());

  	if(currentqty > existingqty && existingqty != '0'){
  		alert("You cannot change");
  		parseFloat($('#qty_'+s_id[1]).val(existingqty));
  	}

  	if(currentqty < 0 ){
  		alert("Quantity cannot be negative");
  		parseFloat($('#qty_'+s_id[1]).val(1));
  	}

  	if(is_liq1 == 0){
  		rate = parseFloat($('#rate_'+s_id[1] ).val());
  		qty = parseFloat($('#qty_'+s_id[1] ).val());
  		amt = rate * qty ;
  		$('#amt_'+s_id[1] ).val(amt);
	} else {
  		rate = parseFloat($('#rate_'+s_id[1] ).val());
  		qty = parseFloat($('#qty_'+s_id[1] ).val());
  		amt = rate * qty ;
  		$('#amt_'+s_id[1] ).val(amt);
	}
});

function save(){
  	table_id = $("#input-t_number_id").val();
  	transfer_table_id = $("#input-transfer_t_number_id").val();
	var trans_table = $('input[name=\'transfer_table\']').val();
  	//alert(trans_table);
  	format = /[ !@#$%^&*()_+\-=\[\]{};'`:"\\|,.<>\/?]/;
	filter_code =  format.test(trans_table);
	//alert(filter_code);
	if(filter_code == false){
	  	if(table_id != '' && table_id != '0'){
	  		$.ajax({
				type: "POST",
				url: 'index.php?route=catalog/kottransfer/orderdatas&token=<?php echo $token; ?>&tbl=' +trans_table,
				dataType: 'json',
				data: {"data":"check"},
				success: function(data){
					//alert(data);
					if(data.status == 1){
						alert("Table is Already Billed...!!! / Table Is Running");
					 	return false;
					} else {
						action = '<?php echo $action; ?>';
					  	action = action.replace("&amp;", "&");
					  	$.post(action, $('#form-order').serialize()).done(function(data) {
					    	var parsed = JSON.parse(data);
					     	if(parsed.info == 1){
					     		$('.sucess').html('');
					     		$('.sucess').append(parsed.done);
					     	} else if(parsed.info == 0){
					     		alert("Nothing is changed");
					     		return false;
					     	} else if(parsed.info == 2){
					     		alert("Deselect Modifier Item");
					     		return false;
					     	}
						});
					}
				}
			});
		  	
		} else {
			alert("Please Select Both the Tables");
	 		return false;
		}
	} else {
		alert("Please Remove Special Character");
	 		return false;
	}

}

</script>
<?php echo $footer; ?>