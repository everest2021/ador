<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;">
  	<div class="page-header">
		<div class="container-fluid">		
	  		<div class="pull-right sav">
				<!-- <button type="submit" form="form-sport" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button> -->
				<a data-toggle="tooltip" onclick="save()" id="save" title="<?php echo $button_save; ?>"  class="btn btn-primary" ><i class="fa fa-save"></i></a>
				<a href="<?php echo $cancel; ?>" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
	  		<h1 class="testtitle"><?php echo $heading_title; ?></h1>
	  		<ul class="breadcrumb bread">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
	  		</ul>
		</div>
  	</div>
  	<div class="container-fluid" >
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default" style="display: none;">
			<div class="well">
				<div class="row">
				</div>
			</div>
		</div>
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		  	</div>
		  	<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-department" class="form-horizontal">
			  		<div class="form-group required">
						<label class="col-sm-3 control-label"><?php echo 'Sale Promotion Name'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="name"  id="promotion_name" value="<?php echo $name; ?>" placeholder="<?php echo 'Name'; ?>" class="form-control" />
						   <!--  <?php if ($error_name) { ?>
                             <div class="text-danger"><?php echo $error_name; ?></div>
                            <?php } ?> -->
						</div>
			   		</div>
			   		<div class="form-group required">
			   			<div class="col-sm-8 ">
			   				<div style="padding-top: 15px;padding-bottom: 15px;margin-bottom: 0;">
								<label class="col-sm-3 control-label"><?php echo 'From date'; ?></label>
							    <div class="col-sm-3">
							     	<input type="text" id="from_date"  name='from_date' value="<?php echo $from_date;?>" class="form-control form_datetime" data-date-format="YYYY-MM-DD" placeholder="From date">
							    </div>
							    <label class="col-sm-3 control-label"><?php echo 'To date'; ?></label>
							    <div class="col-sm-3">
							     	<input type="text" id="to_date"  name='to_date' value="<?php echo $to_date;?>" class="form-control form_datetime" data-date-format="YYYY-MM-DD" placeholder="To date">
							    </div>
							</div>
							<div style="padding-top: 15px;padding-bottom: 15px;margin-bottom: 0;margin-top: 59px">
								<label class="col-sm-3 control-label"><?php echo 'From Time'; ?></label>
								<div class="col-sm-3">
									<input name="from_time"  value="<?php echo $from_time; ?>" placeholder="From Time" type="time" class="form-control" required="required">
								</div>
								<label class="col-sm-3 control-label"><?php echo 'To Time'; ?></label>
								<div class="col-sm-3">
									<input name="to_time"  value="<?php echo $to_time; ?>" placeholder="To Time" type="time" class="form-control" required="required">
								</div>
							</div>
						</div>
				   		<div class="col-sm-4 "> 
				            <label class="col-sm-3 control-label"><?php echo 'Day to Active Promotion'; ?></label>
				            <div class="col-sm-9">
				              	<div class="well well-sm" style="height: 150px; overflow: auto;display: block !important;">
				                	<?php foreach($days_array as $key => $value) {  ?>
					                	<div class="checkbox">
							                <label>
							                    <?php if (in_array($key, $days)) { ?>
							                    	<input type="checkbox" name="days[]" value="<?php echo $key; ?>"  checked="checked" />&nbsp;<?php echo $value; ?>
							                    <?php } else { ?>
							                    	<input type="checkbox" name="days[]" value="<?php echo $key; ?>" />&nbsp;<?php echo $value; ?>
							                   <?php } ?>
							                </label>
						                </div>
						            <?php } ?>
				              	</div>
				            </div>
				   		</div>
			   		 </div>
			   		<div class="form-group">
						<label class="col-sm-3 control-label" ><?php echo 'Sub Category Name'; ?></label>
						<div class="col-sm-9" >
							<select name="subcategory" id="subcategory" class="form-control" style="">
								<option value="0"><?php echo 'Please Select'; ?></option>
							  	<?php foreach($subcategory_data as $subkey => $subvalue) { 
									if($subcategory == $subvalue['category_id']){ ?>
										<option value="<?php echo $subvalue['category_id']; ?>" selected="selected"><?php echo $subvalue['name']; ?></option>
							  		<?php } else {  ?>
										<option value="<?php echo $subvalue['category_id']; ?>"><?php echo $subvalue['name']; ?></option>
									<?php } 
								} ?>
							</select>
							<!-- <input type="hidden" name="subcategory_id" id="subcategory_id" value="" size="50" /> -->
						</div>
			   		</div>

			   		<div class="form-group menuitem_div" style="display: none;">
						<label class="col-sm-3 control-label" ><?php echo 'Menu Item Name'; ?></label>
						<div class="col-sm-9">
				              <div class="well well-sm" style="height: 200px; overflow: auto;display: block !important;">
								<div class="checkbox" id="menuitem">
					               	<!--  <label>
					                   	<input type="checkbox" name="itme_list[]" value="<?php echo $key; ?>" />&nbsp;<?php echo $value; ?>
					                </label> -->
				                </div>
				            </div>
						</div>
			   		</div>
			   		<div class="form-group">
						<label class="col-sm-3 control-label" ><?php echo 'Free Sub Category Name'; ?></label>
						<div class="col-sm-9" >
							<select name="free_subcategory" id="free_subcategory" class="form-control" style="">
								<option value="0"><?php echo 'Please Select'; ?></option>
							  	<?php foreach($subcategory_data as $subkey => $subvalue) { 
									if($free_subcategory == $subvalue['category_id']){ ?>
										<option value="<?php echo $subvalue['category_id']; ?>" selected="selected"><?php echo $subvalue['name']; ?></option>
							  		<?php } else {  ?>
										<option value="<?php echo $subvalue['category_id']; ?>"><?php echo $subvalue['name']; ?></option>
									<?php } 
								} ?>

							</select>
							<!-- <input type="hidden" name="free_subcategory_id" id="free_subcategory_id" value="" size="50" /> -->
						</div>
			   		</div>

			   		<div class="form-group free_menuitem_div" style="display: none;">
						<label class="col-sm-3 control-label" ><?php echo 'Free Menu Item Name'; ?></label>
						<div class="col-sm-9">
				            <div class="well well-sm" style="height: 200px; overflow: auto;display: block !important;">
								<div class="checkbox" id="free_menuitem">
					               <!--  <label>
					                   	<input type="checkbox" name="free_itme_list[]" value="<?php echo $key; ?>" />&nbsp;<?php echo $value; ?>
					                </label> -->
				                </div>
				            </div>
						</div>
			   		</div>
			   		

			   		<div class="form-group ">
						<label class="col-sm-3 control-label"><?php echo 'Discount'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="discount_percentage" id="discount_percentage" value="<?php echo $discount_percentage; ?>" placeholder="<?php echo 'Discount(%)'; ?>" class="form-control" />
						</div>
						<div class="col-sm-3">
				  			<input type="text" name="discount_rupees" id="discount_rupees" value="<?php echo $discount_rupees; ?>" placeholder="<?php echo 'Discount(₹)'; ?>" class="form-control" />
						</div>
			   		</div>

			   		<div class="form-group ">
			   			<label class="col-sm-3 control-label"><?php echo 'Royalty Point'; ?></label>
			   			<div class="col-sm-9 " style="margin-bottom: 56px;">
			   			</div>
						<div class="form-group col-sm-6">
							<label class="col-sm-4 control-label"><?php echo 'Amount'; ?></label>
							<div class="col-sm-3">
					  			<input type="text" name="amount" id="amount" value="<?php echo $amount; ?>" placeholder="<?php echo 'Amount'; ?>" class="form-control" />
							</div>
							<label class="col-sm-2 control-label"><?php echo 'Point'; ?></label>
							<div class="col-sm-3">
					  			<input type="text" name="point" id="point" value="<?php echo $point; ?>" placeholder="<?php echo 'Point'; ?>" class="form-control" />
							</div>

			   			</div>
			   			<div class="form-group col-sm-6" style="border-top: 0px solid">
							<label class="col-sm-3 control-label"><?php echo 'Point'; ?></label>
							<div class="col-sm-3">
					  			<input type="text" name="per_point" id="per_point" value="<?php echo $per_point; ?>" placeholder="<?php echo 'Point'; ?>" class="form-control" />
							</div>
							<label class="col-sm-3 control-label"><?php echo 'Rupees'; ?></label>
							<div class="col-sm-3">
					  			<input type="text" name="point_rupees" id="point_rupees" value="<?php echo $point_rupees; ?>" placeholder="<?php echo 'Rupees'; ?>" class="form-control" />
							</div>
						</div>
			   		</div>
				</form>
		  	</div>
		</div>
  	</div>
</div> 
<script type="text/javascript">
	function save(){
		$('.text-danger').remove();
		var name = $('#promotion_name').val();
		if(name == ''){
			var element = $('#promotion_name');
	  		element.after('<div class="text-danger">Please Enter Name</div>');
	  		console.log('pppppppp');
		} else {
			$('#form-department').submit();
		}
	}

	$('#subcategory').change(function(){
		var subcategory_id = $('#subcategory').val();
		var sale_promotion_id = '<?php echo $sale_promotion_id;  ?>';
		$.ajax({
	        url:'index.php?route=catalog/sale_promotion/items&token=<?php echo $token; ?>&subcategory_id=' + encodeURIComponent(subcategory_id)+'&sale_promotion_id='+sale_promotion_id+'&free_sub_cat=0',
	        dataType:'json',
	        method: 'post',
	        success: function(json) {
	        	$('#menuitem').html('');
	    		if (json['status'] == 1) {
	    			$('.menuitem_div').show();
					$.each(json['items'], function (i, item) {
						var is_checked = '';
						if(item.is_selected == 1){
							is_checked = 'checked="checked"';
						}
						$('#menuitem').append('<div style="display:flex;padding-left: 20px;margin-bottom: 1%;"><div class="col-sm-4" ><input type="checkbox" name="item_list[]" '+is_checked+' value="'+item.item_id+'" />&nbsp;'+item.item_name+'&nbsp;&nbsp;&nbsp;</div><div class="col-sm-8"> <input type="number"  class="form-control"  name="item_quntity['+item.item_id+']" value="'+item.quantity+'" placeholder="Quntity" style="width: 200px;margin-top: -7px;" /></div>');
						//console.log('<label><input type="checkbox" name="item_list[]" value="'+item.item_id+'" />&nbsp;'+item.item_name+'</div></br>');
					});
				} else {
					$('.menuitem_div').hide();
				}
	        }
	    });
	});


	$( document ).ready(function() {
	    var subcategory_id = $('#subcategory').val();
	    var sale_promotion_id = '<?php echo $sale_promotion_id;  ?>';
		$.ajax({
	        url:'index.php?route=catalog/sale_promotion/items&token=<?php echo $token; ?>&subcategory_id=' + encodeURIComponent(subcategory_id)+'&sale_promotion_id='+sale_promotion_id+'&free_sub_cat=0',
	        dataType:'json',
	        method: 'post',
	        success: function(json) {
	        	$('#menuitem').html('');
	    		if (json['status'] == 1) {
	    			$('.menuitem_div').show();
					$.each(json['items'], function (i, item) {
						var is_checked = '';
						if(item.is_selected == 1){
							is_checked = 'checked="checked"';
						}
						$('#menuitem').append('<div style="display:flex;padding-left: 20px;margin-bottom: 1%;"><div class="col-sm-4" ><input type="checkbox" name="item_list[]" '+is_checked+' value="'+item.item_id+'" />&nbsp;'+item.item_name+'&nbsp;&nbsp;&nbsp;</div><div class="col-sm-8"> <input type="number"  class="form-control"  name="item_quntity['+item.item_id+']" value="'+item.quantity+'" placeholder="Quntity" style="width: 200px;margin-top: -7px;" /></div>');
					});
				} else {
					$('.menuitem_div').hide();
				}
	        }
	    });
	});


	$('#free_subcategory').change(function(){
		var subcategory_id = $('#free_subcategory').val();
		var sale_promotion_id = '<?php echo $sale_promotion_id;  ?>';
		$.ajax({
	        url:'index.php?route=catalog/sale_promotion/items&token=<?php echo $token; ?>&subcategory_id=' + encodeURIComponent(subcategory_id)+'&sale_promotion_id='+sale_promotion_id+'&free_sub_cat=1',
	        dataType:'json',
	        method: 'post',
	        success: function(json) {
	        	$('#free_menuitem').html('');
	    		if (json['status'] == 1) {
	    			$('.free_menuitem_div').show();
					$.each(json['items'], function (i, item) {

						var is_checked = '';
						if(item.is_selected == 1){
							is_checked = 'checked="checked"';
						}
						$('#free_menuitem').append('<div style="display:flex;padding-left: 20px;margin-bottom: 1%;"><div class="col-sm-4" ><input type="checkbox" name="free_itme_list[]" '+is_checked+' value="'+item.item_id+'" />&nbsp;'+item.item_name+'&nbsp;&nbsp;&nbsp;</div><div class="col-sm-8"> <input type="number"  class="form-control"  name="free_item_quntity['+item.item_id+']" value="'+item.quantity+'" placeholder="Quntity" style="width: 200px;margin-top: -7px;" /></div>');
					});
				} else {
					$('.free_menuitem_div').hide();
				}
	        }
	    });
	});

	$( document ).ready(function() {
	    var subcategory_id = $('#free_subcategory').val();
	    var sale_promotion_id = '<?php echo $sale_promotion_id;  ?>';
		$.ajax({
	        url:'index.php?route=catalog/sale_promotion/items&token=<?php echo $token; ?>&subcategory_id=' + encodeURIComponent(subcategory_id)+'&sale_promotion_id='+sale_promotion_id+'&free_sub_cat=1',
	        dataType:'json',
	        method: 'post',
	        success: function(json) {
	        	$('#free_menuitem').html('');
	    		if (json['status'] == 1) {
	    			$('.free_menuitem_div').show();
					$.each(json['items'], function (i, item) {
						var is_checked = '';
						if(item.is_selected == 1){
							is_checked = 'checked="checked"';
						}
						$('#free_menuitem').append('<div style="display:flex;padding-left: 20px;margin-bottom: 1%;"><div class="col-sm-4" ><input type="checkbox" name="free_itme_list[]" '+is_checked+' value="'+item.item_id+'" />&nbsp;'+item.item_name+'&nbsp;&nbsp;&nbsp;</div><div class="col-sm-8"> <input type="number"  class="form-control"  name="free_item_quntity['+item.item_id+']" value="'+item.quantity+'" placeholder="Quntity" style="width: 200px;margin-top: -7px;" /></div>');
					});
				} else {
					$('.free_menuitem_div').hide();
				}
	        }
	    });
	});

</script>
<script type="text/javascript">
	 	$(".form_datetime").datepicker();
</script>
<script type="text/javascript">
	 	$("#date_anniversary").datepicker();
</script>
<?php echo $footer; ?>