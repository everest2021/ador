<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" style="background-color: white;">
  	<div class="container-fluid" style="padding-left: 0px !important; padding-right: 0px !important;">
		<?php if ($error_warning) { ?>
	  		<div class="alert alert-danger" style="padding: 0px !important;margin-bottom: 0px !important;"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	      		<button type="button" class="close" data-dismiss="alert">&times;</button>
	  		</div>
	    <?php } ?>
	    <?php if ($success) { ?>
	  		<div class="alert alert-success" style="padding: 0px !important;margin-bottom: 0px !important;"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
	      		<button type="button" class="close" data-dismiss="alert">&times;</button>
	  		</div>
	    <?php } ?>
	  	<div class="panel panel-default"  >
		  	<div class="panel-body" style="padding: 0px; padding-top: 5px !important;">
		  		<div>
		  		<a style="margin-top: 0px;margin-bottom: 2px;margin-left: 12px;font-size: 20px; " class="btn btn-primary" id="back">Back</a>
		  		</div>
		  		<!-- <div class="hover_bkgr_fricc">
				    <span class="helper"></span>
				    <div>
				        <div class="popupCloseButton">X</div>
				        <p>Add any HTML content<br />inside the popup box!</p>
				    </div>
				</div> -->
		  		<!-- <ul class="nav nav-tabs"> -->
				<!-- <li class="active">
		    		<a href="#1" data-toggle="tab">Bill View</a>
				</li> -->
				<!-- <li style="display: none;">
					<a href="#2" data-toggle="tab">Liquor</a>
				</li> -->
				
			<!-- </ul> -->
		  		<div class="table-responsive" style="display:inline-block; width:70%;float:left;border-right: solid black 1px;height:750px;">
					<table class="table table-bordered" style="margin-bottom: 0px !important;border: solid black 4px;">
			  			<thead>
							<tr style="border: solid black 4px;">
					  			<td style="border: solid black 4px; " colspan="1">
									<label style="font-size: 20px;cursor: pointer;"><?php echo "QTY"?></label>
								</td>
								<td style="border: solid black 4px;">
									<label style="font-size: 20px;cursor: pointer;"><?php echo "DESCRIPTION" ?></label>
								</td>
								<td style="border: solid black 4px;">
									<label style="font-size: 20px;cursor: pointer;"><?php echo 'KOT MESSAGE' ?></label>
								</td style="border: solid black 4px;">
								<td colspan="1">
									<label style="font-size: 20px;cursor: pointer;"><?php echo 'TABLE NO' ?></label>
								</td>
								<td style="border: solid black 4px;">
									<label style="font-size: 20px;cursor: pointer;"><?php echo "LOCATION" ?></label>
								</td>
								<td style="border: solid black 4px;" colspan="1">
									<label style="font-size: 20px;cursor: pointer;"><?php echo "KOT NO" ?></label>
								</td>
								<td style="border: solid black 4px;" colspan="4">
									<label style="font-size: 20px;cursor: pointer;"><?php echo "STATUS" ?></label>
									
								</td>
							</tr>
			  			</thead>
			  			<tbody>
							<?php foreach($inprocess_datas as $akey => $avalue){ ?>
								<?php if($avalue['inprocess_datas']) { ?>
									<?php foreach($avalue['inprocess_datas'] as $pkey => $pvalue){ ?>
										<tr style="border: solid black 4px;">
											<td colspan="7" >
												<label style="font-size: 20px;cursor: pointer;"><?php echo $pkey ?></label>
											</td>
										</tr>
										<?php /*echo '<pre>';
										print_r($ivalue);
										exit;*/
										?>
										<?php foreach($pvalue as $ikey => $ivalue){ ?>
											<tr style="border: solid black 4px;">
												<td>
													<label style="font-size: 20px;cursor: pointer;"><?php echo $ivalue['qty'] ?></label>
												</td>
												<td style="border: solid black 4px;">
													<label style="font-size: 20px;cursor: pointer;"><?php echo $ivalue['name'] ?></label>
												</td>
												<td style="border: solid black 4px;">
													<label style="font-size: 20px;cursor: pointer;"><?php echo $ivalue['msg'] ?></label>
												</td>
												<td style="border: solid black 4px;" colspan="1">
													<label style="font-size: 20px;cursor: pointer;"><?php echo $ivalue['table_id'] ?></label>
												</td>
												<td style="border: solid black 4px;">
													<label style="font-size: 20px;cursor: pointer;"><?php echo $ivalue['location'] ?></label>
												</td>
												<td style="border: solid black 4px;" colspan="1">
													<label style="font-size: 20px;cursor: pointer;"><?php echo $ivalue['kot_no'] ?></label>
												</td>
												<td style="border: solid black 4px;" colspan="4">
													<div class="col-sm-offset">
														<?php if($ivalue['kitchen_dis_status'] == '0'){ ?>
												 			<a href="<?php echo $ivalue['href_inprocess'] ?>" class="btn btn-primary inprocess" >In Process</a>
												 		<?php } ?>
												 		<?php if($ivalue['kitchen_dis_status'] == '1'){ ?>
												 			<a href="<?php echo $ivalue['href_complete'] ?>" class="btn btn-success complete" >Complete</a>
												 		<?php } ?>
												 	</div>
												</td>
											</tr>
										<?php } ?>
									<?php } ?>
								<?php } ?>
							<?php } ?>
			  			</tbody>
					</table>
				</div>
				<div class="table-responsive" style="display:inline-block; width:30%;float:left;border-right: solid black 1px;height:750px;">
					<table class="table table-bordered" style="margin-bottom: 0px !important;border: solid black 4px;">
			  			<thead>
							<tr style="border: solid black 4px;">
					  			<td style="border: solid black 4px; " colspan="1">
									<label style="font-size: 20px;cursor: pointer;"><?php echo "QTY"?></label>
								</td>
								<td style="border: solid black 4px;">
									<label style="font-size: 20px;cursor: pointer;"><?php echo "DESCRIPTION" ?></label>
								</td>
								<td style="border: solid black 4px;">
									<label style="font-size: 20px;cursor: pointer;"><?php echo 'KOT MESSAGE' ?></label>
								</td style="border: solid black 4px;">
								
							</tr>
			  			</thead>
			  			<tbody>
							<?php foreach($summary_datas['new_datas'] as $jkey => $jvalue){ ?>
							<?php /*echo '<pre>';
							print_r($summary_datas);
							exit;*/
							?>
								<tr style="border: solid black 4px;">
									<td>
										<label style="font-size: 20px;cursor: pointer;"><?php echo $jvalue['qtyy'] ?></label>
									</td>
									<td style="border: solid black 4px;">
										<label style="font-size: 20px;cursor: pointer;"><?php echo $jvalue['name'] ?></label>
									</td>
									<td style="border: solid black 4px;">
										<label style="font-size: 20px;cursor: pointer;"><?php echo $jvalue['message'] ?></label>
									</td>
								</tr>
							<?php } ?>
			  			</tbody>
					</table>
				</div>
				
				
			</div>
			<br/>
			<div class="col-sm-offset" style="border: solid black 2px;">
					<label style="font-size: 27px;cursor: pointer;">Total Pending Items:<?php echo $pending_items ?></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;					
					<label style="font-size: 27px;cursor: pointer;">Total Items:<?php echo $total_items ?></label>					
								 		
				</div>
		</div>
	</div>
	<style type="text/css">
		.hover_bkgr_fricc{
    background:rgba(0,0,0,.4);
    cursor:pointer;
    display:none;
    height:100%;
    position:fixed;
    text-align:center;
    top:0;
    width:100%;
    z-index:10000;
}
.hover_bkgr_fricc .helper{
    display:inline-block;
    height:100%;
    vertical-align:middle;
}
.hover_bkgr_fricc > div {
    background-color: #fff;
    box-shadow: 10px 10px 60px #555;
    display: inline-block;
    height: auto;
    max-width: 551px;
    min-height: 100px;
    vertical-align: middle;
    width: 60%;
    position: relative;
    border-radius: 8px;
    padding: 15px 5%;
}
.popupCloseButton {
    background-color: #fff;
    border: 3px solid #999;
    border-radius: 50px;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: -20px;
    right: -20px;
    font-size: 25px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
}
.popupCloseButton:hover {
    background-color: #ccc;
}
.trigger_popup_fricc {
    cursor: pointer;
    font-size: 20px;
    margin: 20px;
    display: inline-block;
    font-weight: bold;
}
	</style>
	<script type="text/javascript">

		// window.setTimeout(function(){ 
		// 	document.location.reload(true); 
		// }, 20000);


		// $(".inprocess").click(function(){
		//        $('.hover_bkgr_fricc').show();
		//     });
		//      $(".complete").click(function(){
		//        $('.hover_bkgr_fricc').show();
		//     });
		//     $('.hover_bkgr_fricc').click(function(){
		//         $('.hover_bkgr_fricc').hide();
		//     });
		//     $('.popupCloseButton').click(function(){
		//         $('.hover_bkgr_fricc').hide();
		//     });
		    
		$('#back').click(function(){
			start_page = '<?php echo $STARTUP_PAGE ?>';
			
			if(start_page == '1'){
				$('#back').attr('href','index.php?route=catalog/order&token=<?php echo $token; ?>');
			} else if(start_page == '2'){
				$('#back').attr('href','index.php?route=catalog/orderqwerty&token=<?php echo $token; ?>');
			} else if(start_page == '3'){
				$('#back').attr('href','index.php?route=catalog/orderqwerty&token=<?php echo $token; ?>');
			}else{
				$('#back').attr('href','index.php?route=catalog/ordertab&token=<?php echo $token; ?>');
			}
		});

		$( document ).ready(function() {
		    $('#filter_order_no').focus();

		});
		
	</script>

</div>

<?php echo $footer; ?>