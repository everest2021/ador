<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>" />
  <link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
  <link href="view/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />
  <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
  <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
  <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
  <script src="view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />
  <script src="view/javascript/common.js" type="text/javascript"></script>
</head>
<body>
  <div align="center" class="container"  style="width:50%;">
    <h3 style="text-align:center"><?php echo $title; ?></h3>
    <?php foreach($final_datass as $final ) { ?>
      <table align="center" class="table table_custom no_border" style="width:80%">
        <tr>
          <th style="text-align:left;border-top: 0px;"><?php echo 'Location:'.$final['location']; ?></th>
          <th style="text-align:right;border-top: 0px;"><?php echo 'Table:'.$final['t_name']; ?></th> 
        </tr>
        <tr>
          <th style="text-align:left;border-top: 0px;"><?php echo 'Waiter:'.$final['waiter'];?></td>
          <th style="text-align:right;border-top: 0px;"><?php echo 'Captain:'.$final['captain'];; ?></td>
        </tr>
      </table>
    <?php } ?>
    <span>
      ---------------------------------------------------------------------------------------------
    </span>
    <table align="center" class="table table_custom no_border" style="width:80%">
      <tr>
        <th style="text-align:left;padding: 10px 15px;border-top: 0px;width: 50%;">Name</th>
        <th style="text-align:right;padding: 10px 15px;border-top: 0px;width: 25%;">QTY</th> 
        <th style="text-align:left;padding: 10px 15px;border-top: 0px;width: 25%;">KOT</th> 
      </tr>
      <?php foreach($final_datas as $final_data) { ?>
        <tr>
          <td style="font-size: 15px;padding: 10px 15px;text-align: left;border-top: 0px;"><?php echo $final_data['name']; ?></td>
          <td style="font-size: 15px;padding: 10px 15px;text-align: right;border-top: 0px;"><?php echo $final_data['qty']; ?></td>
          <td style="font-size: 15px;padding: 10px 15px;text-align: left;border-top: 0px;"><?php echo $final_data['message']; ?></td>
        </tr>
      <?php } ?>
    </table>
  </div>
</body>
</html>
<script type="text/javascript">
$(document).ready(function() {
  window.print();
});
</script>