<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right">
		       	<button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" id="save" class="btn btn-primary"><i class="fa fa-save"></i></button>
		       	<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
		    </div>
		    <h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
		    </ul>
	    </div>
	</div>
    <div class="container-fluid">
     	<?php if ($error_warning) { ?>
		    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			    <button type="button" class="close" data-dismiss="alert">&times;</button>
		    </div>
	    <?php } ?>
	    <div class="panel panel-default">
		    <div class="panel-heading">
		        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		    </div>
			<div class="panel-body">
		   		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
				  	<div class="form-group">
						<label class="col-sm-3 control-label"><?php echo 'Date'; ?></label>
					    <div class="col-sm-3">
					     	<input autocomplete="off" readonly="readonly" type="text" name='date' value="<?php echo date('d-m-Y') ?>" class="form-control">
					    </div>
					    <label class="col-sm-1 control-label"><?php echo 'Store'; ?></label>
					    <div class="col-sm-3">
					     	<select name='store' id="store" class="form-control">
						     	<option value="">Please Select</option>
						     	<?php foreach($stores as $key) { ?> 
						     		<?php if($key['outlet_id'] == $store) { ?>
						     			<option value="<?php echo $key['outlet_id'] ?>" selected="selected"><?php echo $key['outlet_name'] ?></option>
						     		<?php } else { ?>
						     			<option value="<?php echo $key['outlet_id'] ?>"><?php echo $key['outlet_name'] ?></option>
						     		<?php } ?>
						     	<?php } ?>
					     	</select>
					    </div>
				    </div>
				    <div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'POs'; ?></label>
						<div class="col-sm-3">
							<select name='pos' id="pos" class="form-control">
								<option value="">Please Select</option>
					     	</select>
						</div>
					</div>
				    <div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Item Name'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="itemname" id="itemname" placeholder="<?php echo 'Item Name'; ?>" class="form-control"/>
						</div>
					</div>
					<table id="data" class="table table-bordered">
						<tr>
							<th>Sr.No</th>
							<th>Item Name</th>
							<th>Quantity</th>
							<th>Notes</th>
							<th>Remove</th>
						</tr>
					</table>
				</form>
	  		</div>
		</div>
  	</div>
  	<script type="text/javascript">
  	var i = 1;
  	$('#itemname').autocomplete({
	  delay: 500,
	  source: function(request, response) {
	    $.ajax({
	      url: 'index.php?route=catalog/purchaseorder/autocompleteitemname&token=<?php echo $token; ?>&filter_item_name=' +  encodeURIComponent(request.term),
	      dataType: 'json',
	      success: function(json) {   
	        response($.map(json, function(item) {
	          return {
	            label: item.item_name,
	            value: item.item_id,
	            code : item.item_code
	          }
	        }));
	      }
	    });
	  }, 
	  select: function(event, ui) {
		html = '<tr id="re_'+i+'">'; 
		  html += '<td>'+i+'</td>';
	      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][item]" value="'+ui.item.label+'"  class="form-control"/></td>';
	      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][qty]" id="qty_'+i+'" value = "1" style="display:inline-block" class="qty form-control"/>';
	      html += '<input type="hidden" name="po_datas[' + i + '][itemid]" value="'+ui.item.value+'"  class="form-control"/>';
	      html += '<input type="hidden" name="po_datas[' + i + '][po_number]" class="form-control"/>';
	      html += '<input type="hidden" name="po_datas[' + i + '][is_new]" value="1"  class="form-control"/>';
	      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][msg]" id="msg_'+i+'" class="msg form-control"/></td>';
	      html += '<input type="hidden" name="po_datas[' + i + '][msgcode]" id="msgcode_'+i+'" class="form-control"/>';
	      html += '<td><a style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove " id="remove_'+i+'" ><i class="fa fa-trash-o"></i></a></td>';
      	html += '</tr>';
      	$('#data').append(html);
		i++;
	    return false;
	  },
	  focus: function(event, ui) {
	  	$('#itemname').val('');
	    return false;
	  }
	});

	function remove_folder(c) {
		$('#re_'+c).remove();
	}

    $('.form_datetime').datetimepicker({
	  pickTime: false,
	  format: 'DD-MM-YYYY'
	});

	$('#store').change(function(){
		store_id = $('#store').val();
		$.ajax({
			url:'index.php?route=catalog/purchaseoutward/getPos&token=<?php echo $token; ?>&store_id='+store_id,
			type:"POST",
			dataType:'json',
			success:function(json){
			  $('#pos').find('option').remove();
		      if(json['po_data']){
		        $.each(json['po_data'], function (i, item) {
		          $('#pos').append($('<option>', { 
		              value: item.id,
		              text : item.po_number
		          }));
		        });
		      }
			}
		});
	});

	$('#pos').change(function(){
		pos_id = $('#pos').val();
		$('#pos option:selected').attr('disabled',true);
		$.ajax({
			url:'index.php?route=catalog/purchaseoutward/getPosData&token=<?php echo $token; ?>&pos_id='+pos_id,
			type:"POST",
			dataType:'json',
			success:function(json){
				for(var podata in json){
					html = '<tr id="re_'+i+'">'; 
					  html += '<td>'+i+'</td>';
				      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][item]" value="'+json[podata].item_name+'"  class="form-control"/></td>';
				      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][qty]" id="qty_'+i+'" value="'+json[podata].quantity+'"style="display:inline-block" class="qty form-control"/>';
				      html += '<input type="hidden" name="po_datas[' + i + '][itemid]" value="'+json[podata].item_id+'"  class="form-control"/>';
				      html += '<input type="hidden" name="po_datas[' + i + '][po_number]" value="'+json[podata].po_number+'"  class="form-control"/>';
				      html += '<input type="hidden" name="po_datas[' + i + '][is_new]" value="0"  class="form-control"/>';
				      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][msg]" id="msg_'+i+'" value="'+json[podata].notes+'" class="msg form-control"/></td>';
				      html += '<input type="hidden" name="po_datas[' + i + '][msgcode]" id="msgcode_'+i+'" value="'+json[podata].notes+'" class="form-control"/>';
				      html += '<td><a style="cursor: pointer;font-size: 16px;" class="button inputs remove " id="remove_'+i+'" >-</td>';
			      	html += '</tr>';
			      	$('#data').append(html);
			      	i++;
		      	}
			}
		});
	});

  	</script>
</div>
<?php echo $footer; ?>