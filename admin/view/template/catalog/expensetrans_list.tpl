<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
   <div class="page-header">
	  <div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
			  	<button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			    <ul class="breadcrumb">
			       <?php foreach ($breadcrumbs as $breadcrumb) { ?>
			       <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			       <?php } ?>
			    </ul>
	   </div>
   </div>
   <div class="container-fluid">
		  <?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			   <button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		   <?php } ?>
		   <?php if ($success) { ?>
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  </div>
		  <?php } ?>
	  <div class="panel panel-default">
		  <div class="panel-heading">
		       <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
	      </div>
	  <div class="panel-body">
		  <div class="well">
		        <div class="row">
			       <div class="col-sm-4">
			          <div class="form-group">
				         <label class="control-label" for="input-name"><?php echo 'Account Name'; ?></label>
				         <input type="text" name="filter_account_name" value="<?php echo $filter_account_name; ?>" placeholder="<?php echo 'Name'; ?>" id="filter_account_name" class="form-control" />
				         <input type="hidden" name="filter_id" value="<?php echo $filter_id; ?>" id="filter_id" class="form-control" />
				         <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
				       </div>
			       </div>
		   </div>
		</div>
		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport">
		  <div class="table-responsive">
			<table class="table table-bordered table-hover">
			  <thead>
				<tr>
				  <td style="width: 1px;display: none;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
				  <td class="text-right">
					<?php if ($sort == 'name') { ?>
					<a href="<?php echo $name; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Account Name"; ?></a>
					<?php } else { ?>
					<a href="<?php echo $name; ?>"><?php echo "Account Name"; ?></a>
					<?php } ?></td>
				  <td class="text-right">
					<?php if ($sort == 'Account Type') { ?>
					<a href="<?php echo $name; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Type"; ?></a>
					<?php } else { ?>
					<a href="<?php echo $name; ?>"><?php echo "Type"; ?></a>
					<?php } ?></td>
				  <td class="text-right">
					<?php if ($sort == 'name') { ?>
					<a href="<?php echo $name; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Payment Type"; ?></a>
					<?php } else { ?>
					<a href="<?php echo $name; ?>"><?php echo "Payment Type"; ?></a>
					<?php } ?></td>
				  <td class="text-right" style="display: none;"><?php echo $column_action; ?></td>
				</tr>
			  </thead>
			  <tbody>
				<?php if ($tables) { ?>
				<?php $i = '1';?>
				<?php foreach ($tables as $table) { ?>
				<tr>
				  <td class="text-center" style="display: none;">
					<?php if (in_array($table['id'], $selected)) { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $table['id']; ?>" checked="checked" />
					<?php } else { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $table['id']; ?>" />
					<?php } ?></td>
				  <td class="text-right"><?php echo $table['account_name']; ?></td>
				  <td class="text-right"><?php echo $table['type']; ?></td>
				  <td class="text-right"><?php echo $table['payment_type']; ?></td>
				  <td class="text-right" style="display: none;">
				  	<a style="display: none;" href="<?php echo $table['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
				  </td>
				</tr>
				<?php $i++?>
				<?php } ?>
				<?php } else { ?>
				<tr>
				  <td class="text-center" colspan="3"><?php echo $text_no_results; ?></td>
				</tr>
				<?php } ?>
			  </tbody>
			</table>
		  </div>
		</form>
		<div class="row">
		  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script type="text/javascript">
$('#button-filter').on('click', function() {
  var url = 'index.php?route=catalog/expensetrans&token=<?php echo $token; ?>';

  var filter_account_name = $('input[name=\'filter_account_name\']').val();
  if (filter_account_name) {
	var filter_id = $('input[name=\'filter_id\']').val();
	if (filter_id) {
	  url += '&filter_id=' + encodeURIComponent(filter_id);
	   }
	  url += '&filter_account_name=' + encodeURIComponent(filter_account_name);
  }

  

  location = url;

});

$('input[name=\'filter_account_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/expensetrans/autocompletecname&token=<?php echo $token; ?>&filter_account_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.account_name,
            value: item.id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_account_name\']').val(ui.item.label);
    $('input[name=\'filter_id\']').val(ui.item.value);
		
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});

</script></div>
<?php echo $footer; ?>