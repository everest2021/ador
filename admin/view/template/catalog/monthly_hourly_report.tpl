<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" id="form" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Data</b></h4></center><br>
					       		<div class="form-row">
								    <div class="col-sm-2 col-sm-offset-2" style="margin-left: 507px;">
								    	<label>Start Date</label>
								     	<input type="text" name='filter_startdate' value="<?php echo $filter_startdate?>" class="form-control form_datetime" placeholder="Start Date">
								    </div>

								   <div class="col-sm-2">
								    	<label>End Date</label>
								     	<input type="text" name='filter_enddate' value="<?php echo $filter_enddate?>" class="form-control form_datetime" placeholder="End Date">
								    </div>

									<!-- <div class="col-sm-2">
									    <label>End Time</label>	
									    <input type="time" name="filter_endtime" value="<?php echo $filter_endtime?>" class="form-control" placeholder="End Time">
									</div> -->
								  
									<div class="col-sm-12">
										<br>
										<center><a id ="filter" class="btn btn-primary" value="Show">Submit</a></center>
									</div>
								</div>
							</div>
					    </div>
					</div>
				 	<!-- <div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 	</div> -->
					<div class="col-sm-6 col-sm-offset-3">
						<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
						<?php date_default_timezone_set("Asia/Kolkata");?>
						<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
						<center><h3><b>Monthly Hourly Report</b></h3></center>
						<h3>From : <?php echo $filter_startdate; ?></h3>
						<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $filter_enddate; ?></h3><br>
						
						  	<table class="table table-bordered table-hover" style="text-align: center;">
								<thead>
									
									<tr>
										<td style="text-align: center;"><?php echo 'TIME'; ?></td>
										<td style="text-align: center;"><?php echo 'Total'; ?></td>
										<td style="text-align: center;"><?php echo 'Total Bill'; ?></td>		  		            
									</tr>
									
								</thead>
								<tbody>
									<?php 
									    $array = array();
									    $array = array(
									    	'1' => '01:00:00 to 01:59:59',
									    	'2' => '02:00:00 to 02:59:59',
									    	'3' => '03:00:00 to 03:59:59',
									    	'4' => '04:00:00 to 04:59:59',
									    	'5' => '05:00:00 to 05:59:59',
									    	'6' => '06:00:00 to 06:59:59',
									    	'7' => '07:00:00 to 07:59:59',
									    	'8' => '08:00:00 to 08:59:59',
									    	'9' => '09:00:00 to 09:59:59',
									    	'10' => '11:00:00 to 11:59:59',
									    	'11' => '12:00:00 to 12:59:59',
									    	'12' => '13:00:00 to 13:59:59',
									    	'13' => '14:00:00 to 14:59:59',
									    	'14' => '15:00:00 to 15:59:59',
									    	'15' => '16:00:00 to 16:59:59',
									    	'16' => '17:00:00 to 17:59:59',
									    	'17' => '18:00:00 to 18:59:59',
									    	'18' => '19:00:00 to 19:59:59',
									    	'19' => '20:00:00 to 20:59:59',
									    	'20' => '21:00:00 to 21:59:59',
									    	'21' => '22:00:00 to 22:59:59',
									    	'22' => '23:00:00 to 23:59:59',
									    	'23' => '00:00:00 to 00:59:59',
									    );
									 ?>
										<?php if ($finala_array) {	 ?>
										<?php foreach($finala_array as $fkeys => $fvalues){ //echo"<pre>";print_r($fkeys);?>
											<tr>
												<td colspan="3" style="text-align: center;font-weight: bold;"><?php echo $fkeys ?></td>
											</tr>
										    <?php foreach($array as $akeys => $avalues){ //echo"<pre>";print_r($avalues);?>
										    	
												<tr>
													<b>
													<td  style="text-align: center;font-weight: bold;"><?php echo $avalues ?></td></b>
													<td style="text-align: center;" class="left">
														<?php echo $fvalues[$avalues]['sum']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $fvalues[$avalues]['count']; ?>
													</td>
												</tr>
										    <?php } ?>
										<?php } //exit;?>
										<thead>
									
										<tr>
											<td style="text-align: center;"><?php echo 'TOTAL BILL'; ?></td>
											<td colspan="3" style="text-align: center;"><?php echo $billno ; ?></td>
										</tr>	
						
										<tr>
											<td style="text-align: center;"><?php echo 'GRAND TOTAL'; ?></td>
											<td colspan="3" style="text-align: center;"><?php echo $total; ?></td>	
										</tr>
								</thead>
								</tbody>
							<?php }  ?>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker({
			dateFormat: 'dd-mm-yy',			
	  	});

		$('#filter').on('click', function() {
		  	var url = 'index.php?route=catalog/monthly_hourly_report&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
			var filter_enddate = $('input[name=\'filter_enddate\']').val();
			var filter_starttime = $('input[name=\'filter_starttime\']').val();
			var filter_endtime = $('input[name=\'filter_endtime\']').val();

			if (filter_startdate) {
				url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}

		  	if (filter_enddate) {
				url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  	}

			if (filter_starttime) {
				url += '&filter_starttime=' + encodeURIComponent(filter_starttime);
		  	}
		  	if (filter_endtime) {
				url += '&filter_endtime=' + encodeURIComponent(filter_endtime);
		  	}
		  	location = url;
		});

		function close_fun_1(){
			window.location.reload();
		}

		
		$('#type').on('change', function() {
			$('#form').submit();
		});

		$('#filter_name').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
		  		filter_type = $('#filter_type').val();
				$.ajax({
			  		url: 'index.php?route=catalog/captain_and_waiter_sale/name&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_type=' +  encodeURIComponent(filter_type),
			  		dataType: 'json',
			  		success: function(json) {   
						response($.map(json, function(item) {
				  			return {
								label: item.name,
								value: item.id,
				  			}
						}));
			  		}
				});
		  	}, 
		  	select: function(event, ui) {
		  		$('#filter_id').val(ui.item.value);
		  		$('#filter_name').val(ui.item.label);
		  		return false;
		  	}
		});
	</script>

	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>