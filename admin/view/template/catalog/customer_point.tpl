<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
	    	<?php if ($success) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				   <button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-4">
						       		<div class="form-row">
									    <div class="col-sm-3">
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									      
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show">
									<button id="print" type="button" class="btn btn-primary">Print</button></center>
								</div>
							</div>
					    </div>
					</div>
					
				 	
					<div class="col-sm-10 col-sm-offset-1">
					<h4>
					<center>
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center><h4>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Customer Points Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;">Bill No</th>
							<th style="text-align: center;">Date</th>
							<th style="text-align: center;">Customer Name</th>
							<th style="text-align: center;">Bill Amt</th>
							<th style="text-align: center;">Point</th>
							
						</tr>
						<?php if(isset($_POST['submit'])){ ?>
							<?php $discount = 0; $dboy_amount = 0; $grandtotal = 0; $vat = 0; $gst = 0; $foodtotal = 0; $fdistotal = 0; $liqtotal = 0;$ldistotal = 0; $discountvalue = 0; $afterdiscount = 0; $stax = 0; $roundoff = 0; $total = 0; $advance = 0; $packaging = 0;$packaging_cgst = 0;$packaging_sgst = 0;?>
							  	<?php foreach ($billdatas as $key => $value) {?>
							  		<tr>
							  			<td colspan="17"><b><?php echo $key ?><b></td>
							  		</tr>
							  		<?php $sdiscount = 0; $sgrandtotal = 0; $svat = 0; $sgst = 0; $sfoodtotal = 0; $sfdistotal = 0; $sliqtotal = 0;$sldistotal = 0; $sdiscountvalue = 0; $safterdiscount = 0; $sstax = 0; $sroundoff = 0; $stotal = 0; $sadvance = 0; $spackaging = 0;$spackaging_cgst = 0;$spackaging_sgst = 0;?>
								  	<?php foreach ($value as $data) { //echo'<pre>';print_r($data);exit;?>
								  		<tr >
									  		<td><?php echo $data['billno'] ?></td>
									  		<td><?php echo $data['bill_date'] ?></td>
									  		<td><?php echo $data['cust_name'] ?></td>
									  		<td><?php echo $data['grand_total'] ?></td>
									  		<td><?php echo $data['cust_point'] ?></td>
									<?php } ?>
									
							  	<?php }  ?>
							  	<tr>
							  		<td colspan="17"><b style="font-size: 25px;">All Total </b></td>

							  	<tr>
							  		<td colspan="10"><b>Total Point :   <?php echo $custp ?></b></td>
							  		
							  	</tr>
							  	</tr>
							  	
					  	<?php } ?>
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	

		function close_fun_1(){
			window.location.reload();
		}


		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<script type="text/javascript">
	$('#print').on('click', function() {

		  var url = 'index.php?route=catalog/customer_point/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  location = url;
		  //setTimeout(close_fun_1, 50);
		});

	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>

</div>
<?php echo $footer; ?>