<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
    	<div class="container-fluid">
	        <div class="pull-right">
	        	<button type="button" id="print" data-toggle="tooltip" title="Print" class="btn btn-primary"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></button>
		       	<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Back" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
		    <div class="container-fluid">
			      <h1><?php echo 'Items Details'; ?></h1>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
		 	<div class="col-md-12">
			    <div class="panel-heading">
				    <table class="table table-bordered">
				    	<tr>
				    		<th style="text-align: center;">Main Item</th>
				    		<th style="text-align: center;">Item Code</th>
				    		<th style="text-align: center;">Rate</th>
				    		<th style="text-align: center;">Notes</th>
				    		<th style="text-align: center">Quantity</th>
				    	</tr>
				    	<tr>
				    		<td style="text-align: center;"><?php echo $itemDetails['item_name']?></td>
				    		<input type="hidden" id="itemid" value="<?php echo $itemDetails['item_id']?>">
				    		<input type="hidden" id="quantity" value="<?php echo $itemDetails['quantity']?>">
				    		<td style="text-align: center;"><?php echo $itemDetails['item_code']?></td>
				    		<td style="text-align: center;"><?php echo $itemDetails['purchase_price']?></td>
				    		<td style="text-align: center;"><?php echo $itemDetails['description']?></td>
				    		<td style="text-align: center;"><?php echo $itemDetails['quantity']?></td>
				    	</tr>
				    </table>
				</div>
		    </div>
		    <div class="col-md-6 col-md-offset-3">
				<h5 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h5>
				<?php date_default_timezone_set("Asia/Kolkata");?>
				<h5 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h5>
		    	<br>
			    <table class="table table-bordered">
		    		<tr>
			    		<th style="text-align: center;">Item</th>
			    		<th style="text-align: center">Quantity</th>
			    		<th style="text-align: center">Unit</th>
			    		<th style="text-align: center">Available quantity</th>
			    	</tr>
				    <?php $i = 0; $quantity = 0; ?>
				    <?php foreach($bomdetails as $key) { ?>
			    	<tr>
			    		<td style="text-align: center;"><?php echo $key['item_name']?></td>
			    		<td style="text-align: center;"><?php echo $key['qty']?></td>
			    		<td style="text-align: center;"><?php echo $key['unit']?></td>
			    		<td style="text-align: center;"><?php echo $key['avq']?></td>
			    	</tr>
			    	<?php $quantity = $quantity + $key['qty']; $i++; ?>
			    	<?php } ?>
					<tr><td colspan="4"></td></tr>
					<tr>
						<td  colspan="2" style="text-align: right;"><b>Total Items : <?php echo $i?></b></td>
						<td colspan="2" style="text-align: right;"><b>Total Quantity : <?php echo $quantity?></b></td>
					</tr>			            
			    </table>
		    </div>
		</div>
	</div>
	<script>
		$('#print').click(function(){
			itemid = $('#itemid').val();
			quantity = $('#quantity').val();
			var url = '<?php echo $print ?>';
			var url1 = url.replace("&amp;", "&");
			url2 = url1+'&filter_item_id='+itemid+'&filter_quantity='+quantity;
			url3 = url2.replace("&amp;",'&');
			url4 = url3.replace("&amp;",'&');
			location = url4;
		});
	</script>
</div>
<?php echo $footer; ?>