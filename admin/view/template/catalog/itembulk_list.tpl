<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
		    <div class="pull-right">
		    </div>
			<h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
		    </ul>
	    </div>
    </div>
  <div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
		    </div>
		<div class="panel-body">
		    <div class="well">
			   <div class="row">
			        <div class="col-sm-3">
				       <div class="form-group">
				          <label class="control-label" for="input-name"><?php echo 'Item Name'; ?></label>
				          <input type="text" name="filter_item_name" value="<?php echo $filter_item_name; ?>" placeholder="<?php echo 'Item Name'; ?>" id="input-filter_item_name" class="form-control" />
				          <input type="hidden" name="filter_item_id" value="<?php echo $filter_item_id; ?>" id="input-filter_item_id" class="form-control" />
				         
				        </div>
			        </div>
			       	<div class="col-sm-3">
					   <div class="form-group">
					       <label class="control-label" for="input-location_name"><?php echo 'Item Code'; ?></label>
					       <input type="text" name="filter_item_code" value="<?php echo $filter_item_code; ?>" placeholder="<?php echo 'Item Code'; ?>" id="input-filter_item_code" class="form-control" />
					    </div>
			    	</div>
			    	<div class="col-sm-3">
					   <div class="form-group">
					    	<label class="control-label" for="input-location_name"><?php echo 'Barcode'; ?></label>
					    	<input type="text" name="filter_barcode" value="<?php echo $filter_barcode; ?>" placeholder="<?php echo 'Barcode'; ?>" id="input-filter_barcode" class="form-control" />
					    </div>
			    	</div>
			    	<div class="col-sm-3">
					   <div class="form-group">
					       	<label class="control-label" for="input-category"><?php echo 'Category'; ?></label>
					       	<select name="filter_category" class="form-control" id="filter_category">
						  		<?php foreach($item_categorys1 as $ckey => $cvalue){ ?>
						  			<?php if($ckey == $filter_category){ ?>
						  				<option value="<?php echo $ckey ?>" selected="selected"><?php echo $cvalue ?></option>
						  			<?php } else { ?>
						  				<option value="<?php echo $ckey ?>"><?php echo $cvalue ?></option>
						  			<?php } ?>
						  		<?php } ?>
							</select>
					    </div>
			    	</div>
			    	<div class="col-sm-3">
					   <div class="form-group">
					       	<label class="control-label" for="input-sub_category"><?php echo 'Sub Category'; ?></label>
					       	<select name="filter_sub_category" class="form-control" id="filter_sub_category">
						  		<?php foreach($item_sub_categorys1 as $ckey => $cvalue){ ?>
						  			<?php if($ckey == $filter_sub_category){ ?>
						  				<option value="<?php echo $ckey ?>" selected="selected"><?php echo $cvalue ?></option>
						  			<?php } else { ?>
						  				<option value="<?php echo $ckey ?>"><?php echo $cvalue ?></option>
						  			<?php } ?>
						  		<?php } ?>
							</select>
					    	<button type="button" data-toggle="tooltip" title="<?php echo 'Update Data'; ?>" class="btn btn-primary pull-right" onclick="$('#form-sport').submit()">Update Data</button>
					    	<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
					    </div>
			    	</div>
			    </div>
		   </div>
	    </div>
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport">
		    <div class="table-responsive">
			  <table class="table table-bordered table-hover">
			   <thead>
				<tr>
				  	<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
				  	<td class="text-right" style="width: 30px;"><?php if ($sort == 'activate_item') { ?>
						<a href="<?php echo $sort_activate_item; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Active Item'; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_activate_item; ?>"><?php echo 'Active item'; ?></a>
						<?php } ?>
					</td>

					<td class="text-right" style="width: 30px;"><?php if ($sort == 'stock_register') { ?>
						<a href="<?php echo $sort_stock_register; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Register Stock'; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_stock_register; ?>"><?php echo 'Register Stock'; ?></a>
						<?php } ?>
					</td>

					<td class="text-right" style="width: 50px;"><?php if ($sort == 'ismodifier') { ?>
						<a href="<?php echo $sort_ismodifier; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Is Modifier'; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_ismodifier; ?>"><?php echo 'Is Modifier'; ?></a>
						<?php } ?>
					</td>

					<td class="text-left" style="width: 50px;"><?php if ($sort == 'item_code') { ?>
						<a href="<?php echo $sort_item_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo " Item Code"; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_item_code; ?>"><?php echo " Item Code"; ?></a>
						<?php } ?>
					</td>

					<td class="text-right" style="width: 150px;"><?php if ($sort == 'item_name') { ?>
						<a href="<?php echo $sort_item_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Item Name'; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_item_name; ?>"><?php echo 'Item Name'; ?></a>
						<?php } ?>
					</td>

				  	<td class="text-right" style="width: 50px;"><?php if ($sort == 'department') { ?>
						<a href="<?php echo $sort_department; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Department'; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_department; ?>"><?php echo 'Department'; ?></a>
						<?php } ?>
					</td>

				  	<td class="text-right" style="width: 120px;display: none;"><?php if ($sort == 'item_category') { ?>
						<a href="<?php echo $sort_item_category; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Category'; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_item_category; ?>"><?php echo 'Category'; ?></a>
						<?php } ?>
					</td>

				  	<td class="text-right" style="width: 120px;"><?php if ($sort == 'item_sub_category') { ?>
						<a href="<?php echo $sort_item_sub_category; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'SubCategory'; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_item_sub_category; ?>"><?php echo 'SubCategory'; ?></a>
						<?php } ?>
					</td>

					<td class="text-right" style="width: 100px;"><?php if ($sort == 'rate_1') { ?>
						<a href="<?php echo $sort_rate_1; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Rate 1'; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_rate_1; ?>"><?php echo 'Rate 1'; ?></a>
						<?php } ?>
					</td>

					<td class="text-right" style="width: 100px;"><?php if ($sort == 'rate_2') { ?>
						<a href="<?php echo $sort_rate_2; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Rate 2'; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_rate_2; ?>"><?php echo 'Rate 2'; ?></a>
						<?php } ?>
					</td>
				  <td class="text-right" style="width: 100px;"><?php if ($sort == 'rate_3') { ?>
					<a href="<?php echo $sort_rate_3; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Rate 3'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_rate_3; ?>"><?php echo 'Rate 3'; ?></a>
					<?php } ?></td>
				  <td class="text-right" style="width: 100px;"><?php if ($sort == 'rate_4') { ?>
					<a href="<?php echo $sort_rate_4; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Rate 4'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_rate_4; ?>"><?php echo 'Rate 4'; ?></a>
					<?php } ?></td>
				  <td class="text-right" style="width: 100px;"><?php if ($sort == 'rate_5') { ?>
					<a href="<?php echo $sort_rate_5; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Rate 5'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_rate_5; ?>"><?php echo 'Rate 5'; ?></a>
					<?php } ?></td>
				  <td class="text-right" style="width: 100px;"><?php if ($sort == 'rate_6') { ?>
					<a href="<?php echo $sort_rate_6; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Rate 6'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_rate_6; ?>"><?php echo 'Rate 6'; ?></a>
					<?php } ?></td>
				   <td class="text-right" style="width: 120px;"><?php if ($sort == 'brand') { ?>
					<a href="<?php echo $sort_brand; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Brand'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_brand; ?>"><?php echo 'Brand'; ?></a>
					<?php } ?></td>
					<td class="text-right" style="width: 100px;"><?php if ($sort == 'quantity') { ?>
					<a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Quantity'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_quantity; ?>"><?php echo 'Quantity'; ?></a>
					<?php } ?>
				</td>
				<td class="text-right" style="width: 100px;"><?php if ($sort == 'vat') { ?>
					<a href="<?php echo $sort_vat; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'GST'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_vat; ?>"><?php echo 'GST'; ?></a>
					<?php } ?>
				</td>
					
					<td class="text-right" style="width: 120px;"><?php if ($sort == 'modifier_group') { ?>
					<a href="<?php echo $sort_modifier_group; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Modifier Group'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_modifier_group; ?>"><?php echo 'Modifier Group'; ?></a>
					<?php } ?></td>
					
					
					<td class="text-right" style="width: 100px;"><?php if ($sort == 's_tax') { ?>
					<a href="<?php echo $sort_s_tax; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'S-Tax'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_s_tax; ?>"><?php echo 'S-Tax'; ?></a>
					<?php } ?></td>
				</tr>
			  </thead>
			  <tbody>
				<?php if ($items) { ?>
					<?php foreach ($items as $ikey => $item) {  ?>
						<tr>
						  	<td class="text-center">
						  		<?php if (in_array($item['item_id'], $selected)) { ?>
									<input type="checkbox" name="selected[]" value="<?php echo $item['item_id']; ?>" checked="checked" />
								<?php } else { ?>
									<input type="checkbox" name="selected[]" value="<?php echo $item['item_id']; ?>" />
								<?php } ?>
							</td>
							<td class="text-center">
						  		<?php if($item['activate_item'] == '1'){ ?>
						 			<input type="checkbox" name="item_datas[<?php echo $ikey ?>][activate_item]" value="1" checked="checked">
						 		<?php } else { ?>
						 			<input type="checkbox" name="item_datas[<?php echo $ikey ?>][activate_item]" value="1">
						 		<?php } ?>
						  	</td>

						  	<td class="text-center">
						  		<?php if($item['stock_register'] == '1'){ ?>
						 			<input type="checkbox" name="item_datas[<?php echo $ikey ?>][stock_register]" value="1" checked="checked">
						 		<?php } else { ?>
						 			<input type="checkbox" name="item_datas[<?php echo $ikey ?>][stock_register]" value="1">
						 		<?php } ?>
						  	</td>

						  	<td class="text-center">
						  		<?php if($item['ismodifier'] == '1'){ ?>
						 			<input type="checkbox" name="item_datas[<?php echo $ikey ?>][ismodifier]" value="1" checked="checked">
						 		<?php } else { ?>
						 			<input type="checkbox" name="item_datas[<?php echo $ikey ?>][ismodifier]" value="1">
						 		<?php } ?>
						  	</td>

						  	<td class="text-left">
						  		<?php echo $item['item_code']; ?>
						  		<input type="hidden" name="item_datas[<?php echo $ikey ?>][item_code]" value="<?php echo $item['item_code']; ?>">
						  		<input type="hidden" name="item_datas[<?php echo $ikey ?>][item_id]" value="<?php echo $item['item_id']; ?>">
						  	</td>
						  	<td class="text-right">
						  		<input type="text" name="item_datas[<?php echo $ikey ?>][item_name]" value="<?php echo $item['item_name']; ?>">
						  	</td>
						  	<td class="text-right">
						  		<select name="item_datas[<?php echo $ikey ?>][department]" class="form-control">
							  		<?php foreach($departments as $dkey => $dvalue){ //echo'<pre>';print_r(($item['department'])); echo'<br>'; echo'<pre>';print_r(($dvalue));  ?>
							  			<?php if($dvalue == $item['department']){ ?>
							  				<option value="<?php echo $dkey ?>" selected="selected"><?php echo $dvalue ?></option>
							  			<?php } else { ?>
							  				<option value="<?php echo $dkey ?>"><?php echo $dvalue ?></option>
							  			<?php } ?>
							  		<?php } ?>
								</select>
						  		<!-- <input type="text" name="item_datas[<?php echo $ikey ?>][department]" size="5" value="<?php echo $item['department']; ?>"> -->
						  	</td>
						  	<td class="text-right" style=";display: none;">
						  		<select name="item_datas[<?php echo $ikey ?>][item_category_id]" class="form-control item_category_select" id="item_category_id-<?php echo $ikey ?>">
							  		<?php foreach($item_categorys as $dkey => $dvalue){ ?>
							  			<?php if($dkey == $item['item_category_id']){ ?>
							  				<option value="<?php echo $dkey ?>" selected="selected"><?php echo $dvalue ?></option>
							  			<?php } else { ?>
							  				<option value="<?php echo $dkey ?>"><?php echo $dvalue ?></option>
							  			<?php } ?>
							  		<?php } ?>
								</select>
								<input type="hidden" id="item_category-<?php echo $ikey ?>" name="item_datas[<?php echo $ikey ?>][item_category]" size="5" value="<?php echo $item['item_category']; ?>">
						  	</td>
						  	<td class="text-right">
						  		<select name="item_datas[<?php echo $ikey ?>][item_sub_category_id]" class="form-control item_sub_category_select" id="item_sub_category_id-<?php echo $ikey ?>">
							  		<?php foreach($item_sub_categorys as $dkey => $dvalue){ ?>
							  			<?php if($dkey == $item['item_sub_category_id']){ ?>
							  				<option value="<?php echo $dkey ?>" selected="selected"><?php echo $dvalue ?></option>
							  			<?php } else { ?>
							  				<option value="<?php echo $dkey ?>"><?php echo $dvalue ?></option>
							  			<?php } ?>
							  		<?php } ?>
								</select>
								<input type="hidden" id="item_sub_category-<?php echo $ikey ?>" name="item_datas[<?php echo $ikey ?>][item_sub_category]" size="5" value="<?php echo $item['item_sub_category']; ?>">
						  	</td>
						  	<td class="text-right">
						  		<input type="text" class="form-control" name="item_datas[<?php echo $ikey ?>][rate_1]" value="<?php echo $item['rate_1']; ?>">
						  	</td>
						  	<td class="text-right">
						  		<input type="text" class="form-control" name="item_datas[<?php echo $ikey ?>][rate_2]" value="<?php echo $item['rate_2']; ?>">
						  	</td>
						  	<td class="text-right">
						  		<input type="text" class="form-control" name="item_datas[<?php echo $ikey ?>][rate_3]" value="<?php echo $item['rate_3']; ?>">
						  	</td>
						  	<td class="text-right">
						  		<input type="text" class="form-control" name="item_datas[<?php echo $ikey ?>][rate_4]" value="<?php echo $item['rate_4']; ?>">
						  	</td>
						  	<td class="text-right">
						  		<input type="text" class="form-control" name="item_datas[<?php echo $ikey ?>][rate_5]" value="<?php echo $item['rate_5']; ?>">
						  	</td>
						  	<td class="text-right">
						  		<input type="text" class="form-control" name="item_datas[<?php echo $ikey ?>][rate_6]" value="<?php echo $item['rate_6']; ?>">
						  	</td>
						  	<td class="text-right">
						  		<select name="item_datas[<?php echo $ikey ?>][brand_id]" class="form-control brand_id_select" id="brand_id-<?php echo $ikey ?>">
								  	<option value="">None</option>
								  	<?php foreach($brands as $dkey => $dvalue){ ?>
								  		<?php if($dvalue['brand_id'] == $item['brand_id']){ ?>
								  			<option value="<?php echo $dvalue['brand_id'] ?>" selected="selected"><?php echo $dvalue['brand'] ?></option>
								  		<?php } else { ?>
								  			<option value="<?php echo $dvalue['brand_id'] ?>"><?php echo $dvalue['brand'] ?></option>
								  		<?php } ?>
								  	<?php } ?>
								</select>
								<input type="hidden" id="brand_name-<?php echo $ikey ?>" name="item_datas[<?php echo $ikey ?>][brand]" value="<?php echo $item['brand']; ?>">
						  	</td>
						  	<td class="text-right">
						  		<input type="text" class="form-control" name="item_datas[<?php echo $ikey ?>][quantity]" value="<?php echo $item['quantity']; ?>">
						  	</td>
						  	<td class="text-right">
						  		<select name="item_datas[<?php echo $ikey ?>][vat]" class="form-control" id="vat-<?php echo $ikey ?>">
						  			<option value="" selected="selected">None</option>
								  	<?php foreach($vats as $dkey => $dvalue){ ?>
								  		<?php if($dvalue['id'] == $item['vat']){ ?>
								  			<option value="<?php echo $dvalue['id'] ?>" selected="selected"><?php echo $dvalue['tax_name'] ?></option>
								  		<?php } else { ?>
								  			<option value="<?php echo $dvalue['id'] ?>"><?php echo $dvalue['tax_name'] ?></option>
								  		<?php } ?>
								  	<?php } ?>
								</select>
								<!-- <input type="hidden" id="vat-<?php echo $ikey ?>" name="item_datas[<?php echo $ikey ?>][vat]" value="<?php echo $item['vat']; ?>"> -->
						  	</td>
						  	
						  	<td class="text-right">
						  		<select name="item_datas[<?php echo $ikey ?>][modifier_group]" class="form-control modifier_group_select" id="modifier_group-<?php echo $ikey ?>">
								  	<option value="">None</option>
								  	<?php foreach($modifier_groups as $dkey => $dvalue){ ?>
								  		<?php if($dvalue['id'] == $item['modifier_group']){ ?>
								  			<option value="<?php echo $dvalue['id'] ?>" selected="selected"><?php echo $dvalue['modifier_name'] ?></option>
								  		<?php } else { ?>
								  			<option value="<?php echo $dvalue['id'] ?>"><?php echo $dvalue['modifier_name'] ?></option>
								  		<?php } ?>
								  	<?php } ?>
								</select>
						  	</td>
						  	
						  	<td class="text-right">
						  		<select name="item_datas[<?php echo $ikey ?>][s_tax]" class="form-control" id="s_tax-<?php echo $ikey ?>">
								  	<?php foreach($s_taxs as $dkey => $dvalue){ ?>
								  		<?php if($dkey == $item['s_tax']){ ?>
								  			<option value="<?php echo $dkey ?>" selected="selected"><?php echo $dvalue ?></option>
								  		<?php } else { ?>
								  			<option value="<?php echo $dkey ?>"><?php echo $dvalue ?></option>
								  		<?php } ?>
								  	<?php } ?>
								</select>
								<!-- <input type="hidden" id="s_tax-<?php echo $ikey ?>" name="item_datas[<?php echo $ikey ?>][s_tax]" value="<?php echo $item['s_tax']; ?>"> -->
						  	</td>
						</tr>
					<?php } ?>
				<?php } else { ?>
				<tr>
				  <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
				</tr>
				<?php } ?>
			  </tbody>
			  </table>
		    </div>
		</form>
		<div class="row">
		    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script type="text/javascript">
$('#button-filter').on('click', function() {
  	var url = 'index.php?route=catalog/itembulk&token=<?php echo $token; ?>';

  	var filter_item_name = $('input[name=\'filter_item_name\']').val();
  	if (filter_item_name) {
		var filter_item_id = $('input[name=\'filter_item_id\']').val();
		if (filter_item_id) {
	  		url += '&filter_item_id=' + encodeURIComponent(filter_item_id);
	  		url += '&filter_item_name=' + encodeURIComponent(filter_item_name);
	  	}
  	}

  	var filter_item_code = $('input[name=\'filter_item_code\']').val();
  	if (filter_item_code) {
		url += '&filter_item_code=' + encodeURIComponent(filter_item_code);
	}

	var filter_barcode = $('input[name=\'filter_barcode\']').val();
	if (filter_barcode) {
		url += '&filter_barcode=' + encodeURIComponent(filter_barcode);
	}

	var filter_category = $('select[name=\'filter_category\']').val();
  	if (filter_category) {
		url += '&filter_category=' + encodeURIComponent(filter_category);
	}

	var filter_sub_category = $('select[name=\'filter_sub_category\']').val();
  	if (filter_sub_category) {
		url += '&filter_sub_category=' + encodeURIComponent(filter_sub_category);
	}
  	
  	location = url;
});

$('input[name=\'filter_item_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/item/autocomplete&token=<?php echo $token; ?>&filter_item_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.item_name,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_item_name\']').val(ui.item.label);
    $('input[name=\'filter_item_id\']').val(ui.item.value);
		
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});

$('input[name=\'filter_item_code\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/item/autocompletecode&token=<?php echo $token; ?>&filter_item_code=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.item_code,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_item_code\']').val(ui.item.label);
		
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});


$('input[name=\'filter_barcode\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/item/autocompletebarcode&token=<?php echo $token; ?>&filter_barcode=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.item_code,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_barcode\']').val(ui.item.label);
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});


$(document).on('change', '.item_category_select', function(e) {
  idss = $(this).attr('id');
  s_id = idss.split('-');
  id = s_id[1];
  category_name = $('#item_category_id-'+id+' option:selected').text();
  $('#item_category-'+id).val(category_name);
});

$(document).on('change', '.item_sub_category_select', function(e) {
  idss = $(this).attr('id');
  s_id = idss.split('-');
  id = s_id[1];
  subcategory_name = $('#item_sub_category_id-'+id+' option:selected').text();
  $('#item_sub_category-'+id).val(subcategory_name);
});

$(document).on('change', '.brand_id_select', function(e) {
  idss = $(this).attr('id');
  s_id = idss.split('-');
  id = s_id[1];
  brand_name = $('#brand_id-'+id+' option:selected').text();
  $('#brand_name-'+id).val(brand_name);
});


</script></div>
<script type="text/javascript">
$('#button-export').on('click', function() {
  var url = 'index.php?route=catalog/item/export&token=<?php echo $token; ?>';
  location = url;
});
</script>
<?php echo $footer; ?>