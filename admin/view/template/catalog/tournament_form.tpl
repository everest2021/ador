<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">

	<div class="page-header">

		<div class="container-fluid">

			<div class="pull-right">

			   <button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>

			   <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>

			   <h1><?php echo $heading_title; ?></h1>

			   <ul class="breadcrumb">

				   <?php foreach ($breadcrumbs as $breadcrumb) { ?>

					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

					<?php } ?>

			   </ul>

		</div>

  </div>

  <div class="container-fluid">

	   <?php if ($error_warning) { ?>

	   <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>

		   <button type="button" class="close" data-dismiss="alert">&times;</button>

	   </div>

		<?php } ?>

	   <div class="panel panel-default">

		   <div class="panel-heading">

			   <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>

			</div>

	   <div class="panel-body">

		   <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">

				<div class="form-group required">

					<label class="col-sm-3 control-label"><?php echo 'Customer Name'; ?></label>

					<div class="col-sm-3">

						<input type="text" name="customer_name" value="<?php echo $customer_name; ?>" placeholder="<?php echo 'Customer Name'; ?>" class="form-control" />

						<?php if ($error_name ) { ?>

						<div class="text-danger"><?php echo $error_name ; ?></div>

						<?php } ?>

					</div>

				</div> 

				<div class="form-group required">

					<label class="col-sm-3 control-label"><?php echo 'Discription'; ?></label>

					<div class="col-sm-3">
						<textarea type="text" name="discription" class= "form-control"> <?php echo $discription; ?> </textarea>
						
					</div>

				</div>
				<div class="form-group">
						<label class="col-sm-3 control-label" ><?php echo 'Image'; ?></label>
					    <div class="col-sm-3">
					  		<input readonly="readonly" type="text" name="photo" value="<?php echo $photo; ?>" placeholder="<?php echo 'Image'; ?>" id="input-photo" class="form-control" />
					  		<input type="hidden" name="photo_source" value="<?php echo $photo_source; ?>" id="input-photo_source" class="form-control" />
					  		<br>
					  		<span class="input-group-btn">
								<button type="button" id="button-photo" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>
					  		</span>
							<?php if($photo_source != ''){ ?>
					  			<br/ ><a target="_blank" style="cursor: pointer;" id="photo_source" class="thumbnail" href="<?php echo $photo_source; ?>">View Image</a>
							<?php } ?>
						</div>
			    	</div>

		  	</form>

		</div>

	  </div>

	</div>

  </div>

  <script>

  	$('#button-photo').on('click', function() {
  $('#form-photo').remove();
  $('body').prepend('<form enctype="multipart/form-data" id="form-photo" style="display: none;"><input type="file" name="file" /></form>');
  $('#form-photo input[name=\'file\']').trigger('click');
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  timer = setInterval(function() {
	if ($('#form-photo input[name=\'file\']').val() != '') {
	  clearInterval(timer); 
	  image_name = 'item';  
	  $.ajax({
		url: 'index.php?route=catalog/testimonal/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-photo')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload').button('loading');
		},
		complete: function() {
		  $('#button-upload').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
			alert(json['error']);
		  }
		  if (json['success']) {
			alert(json['success']);
			console.log(json);
			$('input[name=\'photo\']').attr('value', json['filename']);
			$('input[name=\'photo_source\']').attr('value', json['link_href']);
			d = new Date();
			var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="photo_source" href="'+json['link_href']+'">View Image</a>';
			$('#photo_source').remove();
			$('#button-photo').after(previewHtml);
		  }
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});

  $('#colorSelector').ColorPicker({

	color: '#0000ff',

	onShow: function (colpkr) {

		$(colpkr).fadeIn(500);

		return false;

	},

	onHide: function (colpkr) {

		$(colpkr).fadeOut(500);

		return false;

	},

	onChange: function (hsb, hex, rgb) {

		$('#colorSelector').css('backgroundColor', '#' + hex);

		$('#colorSelector').val('#' + hex);

	},

	onSubmit: function (hsb, hex, rgb) {

		$('#colorSelector').css('backgroundColor', '#' + hex);

		$('#colorSelector').val('#' + hex);

	}

});

</script>

</div> 

<?php echo $footer; ?>