<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
	    	<?php if ($success) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				   <button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
		      <h1><?php echo "GST Report"; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo "GST Report"; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-4">
						       		<div class="form-row">
									    <div class="col-sm-3">
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
					<?php /*<div class="table-responsive col-sm-6 col-sm-offset-3">
					  <table class="table table-bordered table-hover" style="text-align: center;">
						  	<tr>
						  		<th style="text-align: center;">Bill Number</th>
						  		<th style="text-align: center;">Bill Amount</th>
						  	</tr>
						  	<?php $total = 0; ?>
						  	<?php foreach ($billdatas as $data) {?>
						  		<tr>
							  		<td><?php echo $data['order_no'] ?></td>
							  		<td><?php echo $data['grand_total'] ?></td>
							  		<?php $total = $total + $data['grand_total']  ?>
						  		</tr>
						  	<?php } ?>
						  	<?php if($total != '0') { ?>
						  		<tr>
							  		<td><b>Total</b></td>
							  		<td><b><?php echo $total ?></b></td>
						  		</tr>
						  	<?php }?>
					  	</table>
				 	</div> */?>
				 	<div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
						<a id="sendmail" data-toggle="tooltip" title="<?php echo "Send Mail"; ?>" style="width: 70px;height: 34px;" class="btn btn-primary "><i class="fa fa-envelope "></i></a>

				 		<button id="export" type="button" class="btn btn-primary">Export</button>
				 	</div>
					<div class="col-sm-10 col-sm-offset-1">
					<h4>
					<center>
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center><h4>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>GST Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;">Order ID</th>
							<th style="text-align: center;">Bill No</th>
							<th style="text-align: center;">Date</th>
							<th style="text-align: center;">Net-Total</th>
							<th style="text-align: center;">Discount (%) </th>
							<th style="text-align: center;">After D. Total </th>
							<th style="text-align: center;">CGST</th>
							<th style="text-align: center;">SGST</th>
							<th style="text-align: center;">Total-GST</th>
							<th style="text-align: center;">Grand-Total</th>
							
						</tr>
						<?php if(isset($_POST['submit'])){ ?>
							<?php $sgst = 0; $cgst = 0; $gst = 0; $total = 0; $ttotal = 0; $dtotal =0;?>
							  	<?php foreach ($billdatas as $key => $value) {?>
							  		<tr>
							  			<td colspan="10"><b><?php echo $key ?><b></td>
							  		</tr>
							  		
								  	<?php foreach ($value as $data) { //echo'<pre>';print_r($data);exit;
								  	if($data['ftotal'] != 0 && $data['billno'] != 0){
								  	?>
								  		<tr >
									  		<td><?php echo $data['order_no'] ?></td>
									  		<td><?php echo $data['billno'] ?></td>
									  		<td><?php echo $data['bill_date'] ?></td>
									  		<td><?php echo $data['ftotal'] ?></td>
									  		<td><?php echo $data['fdiscountper'] ?></td>
									  		<td><?php echo $data['ftotal_discount'] ?></td>
									  		<td><?php echo ($data['gst']/2); ?></td>
									  		<td><?php echo ($data['gst']/2); ?></td>
									  		<td><?php echo $data['gst'] ?></td>
											<td><?php echo round($data['ftotal_discount'] + $data['gst'])?></td>
									  	</tr>
									  	<?php 
									  	 	   $ttotal = $ttotal + $data['ftotal'];
									  	 	   $dtotal = $dtotal + $data['ftotal_discount'];
									  		   $total += ($data['ftotal_discount'] + $data['gst']);
									  		   $gst = $gst + $data['gst'];
									  		   $cgst = $cgst + $data['gst'] / 2;
									  		   $sgst = $sgst + $data['gst'] / 2;
									  	?>
									<?php } ?>
								<?php } ?>
								<tr >
									  		<td>Grand Total: </td>
									  		<td> - </td>
									  		<td> - </td>
									  		<td><?php echo $ttotal ?></td>
									  		<td> - </td>
									  		<td><?php echo $dtotal ?></td>
									  		<td><?php echo $cgst ?></td>
									  		<td><?php echo $sgst ?></td>
									  		<td><?php echo $gst ?></td>
									  		
											<td><?php echo ($total)?></td>
									  	</tr>
									
							  	<?php }  ?>
							  	
					  	<?php } ?>
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/gstbill/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}


		$('#export').on('click', function() {
		  var url = 'index.php?route=catalog/gstbill/export&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		$('#sendmail').on('click', function() {
		  var url = 'index.php?route=catalog/gstbill/sendmail&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>

</div>
<?php echo $footer; ?>