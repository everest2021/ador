<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       	<div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-5">
						       		<div class="form-row">
									    <div class="col-sm-3">
									    	<label>Start Date</label>
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<label>End Date</label>
									     	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center>
										<input type="submit" name="submit" class="btn btn-primary" value="Show">
									</center>
								</div>
							</div>
					    </div>
					</div>
				 	<div class="col-sm-offset-10" style="display: none;">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 	</div>
				 	<?php if(isset($_POST['submit']) ){ ?>
						<div class="col-sm-6 col-sm-offset-3">
						<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
						<?php date_default_timezone_set("Asia/Kolkata");?>
						<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
						<center><h3><b>Order Process</b></h3></center>
						<h3>Date : <?php echo $startdate; ?></h3>
						
						  <table class="table table-bordered table-hover" style="text-align: center;">
							<tr>
								<th style="text-align: center;">Order Number</th>
								<th style="text-align: center;" >Inprocess Time</th>
								<th style="text-align: center;" >Ready Time</th>
								<th style="text-align: center;" >Total Time</th>
							</tr>
							<?php $i=0; ?>
							<?php if($finaldatas != array()){ ?>
								<?php foreach($finaldatas as $key => $value) { ?>
									<tr>
										<td><?php echo $key;?></td>
										<td><?php echo $value['inproces'];?></td>
										<td><?php echo $value['ready'];?></td>
										<td><?php echo $value['total_time'];?></td>
									</tr>
								  <?php $i++;?>
								<?php }?>
								<tr>
									<td style="text-align: left;" colspan='4'>Total Orders: &nbsp;&nbsp;<?php echo $i;?></td>
								</tr>
							  </table>
							  <br>
						  <?php } ?>
						  <center><h5><b>End of Report</b></h5></center>
					 	</div>
				 	<?php } ?>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

		$('#print').on('click', function() {

			// var forprintarray = JSON.parse('<?php echo $forprintarray ?>');
			// console.log(forprintarray);

		  var url = 'index.php?route=catalog/daysummaryreport/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_category = $('select[name=\'filter_category\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_category) {
			  url += '&filter_category=' + encodeURIComponent(filter_category);
		  }

		  // url += '&datatest=' + encodeURIComponent(btoa(JSON.stringify(forprintarray)));

		  location = url;
		  //setTimeout(close_fun_1, 50);
		});

		// function close_fun_1(){
		// 	window.location.reload();
		// }
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
	
</div>
<?php echo $footer; ?>