<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $heading_title; ?></title>
    <base href="<?php echo $base; ?>" />
        <link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
    <h1 style="text-align:center;font-weight: bold;color: #000;">
        <?php echo $heading_title; ?>
    </h1>
    <table class="table table-bordered product" style="width:100% !important; border: 1px solid;">
        <thead>
                <tr style="border: 1px solid black;">
                    <center>
                        <?php echo HOTEL_NAME ?>
                        <?php echo HOTEL_ADD ?>
                    </center>
                </tr>
                <tr style="border: 1px sol 863421`p;
                ']/ id black;">
                   <h5 style="text-align: left;"><?php echo date('d/m/Y'); ?></h5>
                    <?php date_default_timezone_set("Asia/Kolkata");?>
                    <h5 style="text-align: right;"><?php echo date('h:i:sa'); ?></h5>
                </tr>
                <tr style="border: 1px solid black;">
                   <h5 style="text-align: left;">From : <?php echo $startdate; ?></h5>
                    <?php date_default_timezone_set("Asia/Kolkata");?>
                    <h5 style="text-align: right;">To : <?php echo $enddate; ?></h5>
                </tr>
                <tr>
                    <td class="text-right" style="font-weight: bold;text-align: right;border: 1px solid black;"><?php echo 'Invoice No'; ?></td>
                    <td class="text-right" style="font-weight: bold;text-align: right;border: 1px solid black;"><?php echo 'Date'; ?></td>
                    <td class="text-right" style="font-weight: bold;text-align: right;border: 1px solid black;"><?php echo 'Item Name'; ?></td>
                    <td class="text-right" style="font-weight: bold;text-align: right;border: 1px solid black;"><?php echo 'Supplier Name'; ?></td>
                    <td class="text-right" style="font-weight: bold;text-align: right;border: 1px solid black;"><?php echo 'Store Name'; ?></td>
                    <td class="text-right" style="font-weight: bold;text-align: right;border: 1px solid black;"><?php echo 'Quantity'; ?></td>
                    <td class="text-right" style="font-weight: bold;text-align: right;border: 1px solid black;"><?php echo 'Unit'; ?></td>
                    <td class="text-right" style="font-weight: bold;text-align: right;border: 1px solid black;"><?php echo 'Rate'; ?></td>
                    <td class="text-right" style="font-weight: bold;text-align: right;border: 1px solid black;"><?php echo 'Amount'; ?></td>
                </tr>
        </thead>
    <tbody>
    <?php if ($orderdatas) { ?>
        <?php $total = 0; ?>
        <?php foreach($orderdatas as $orderdata){ ?>
            <tr>
                <td class="text-right" style="border: 1px solid black;" ><?php echo $orderdata['invoice_no'] ?></td>
                <td class="text-right" style="border: 1px solid black;" ><?php echo $orderdata['invoice_date'] ?></td>
                <td class="text-right" style="border: 1px solid black;" ><?php echo $orderdata['description'] ?></td>
                <td class="text-right" style="border: 1px solid black;" ><?php echo $orderdata['supplier_name'] ?></td>
                <td class="text-right" style="border: 1px solid black;" ><?php echo $orderdata['store_name'] ?></td>
                <td class="text-right" style="border: 1px solid black;" ><?php echo $orderdata['qty'] ?></td>
                <td class="text-right" style="border: 1px solid black;" ><?php echo $orderdata['unit'] ?></td>
                <td class="text-right" style="border: 1px solid black;" ><?php echo $orderdata['rate'] ?></td>
                <td class="text-right" style="border: 1px solid black;" ><?php echo $orderdata['amount']?></td>
            </tr>
            <?php $total = $total + $orderdata['amount']; ?>
        <?php } ?>
        <tr>
            <td colspan="8">Total</td>
            <td><?php echo $total ?></td>
        </tr>
    <?php } else { ?>
                <tr>
                    <td class="text-center" colspan="22"><?php echo 'No Record Found'; ?></td>
                </tr>
            <?php } ?>
    </tbody>
  </table>
</div></body></html>