<?php echo $header; ?>
<div id="exTab2" class="container-fluid">	
	<div class="row">
		<div class="col-md-8">
			<ul class="nav nav-tabs">
				<li class="active">
		    		<a href="#1" data-toggle="tab">Customer History</a>
				</li>
					<a href="index.php?route=catalog/customer/add&token=<?php echo $token; ?>&iframe=1" style="margin-top: 2px;" class="btn btn-primary" id="back">Back</a>
			</ul>
			<div class="tab-content">
			  	<div style="overflow:auto; ;height:700px;" class="tab-pane active" id="1">
		  			<table class="table table-bordered" id="tablefood" style="cursor: pointer; width: 20%;">
			  			<tr>
			  				<th>Bill Date</th>
			  				<th>Grand Total</th>
			  				<th Style ="display: none;">Customer id</th>
			  				<th Style ="display: none;">creditcustname</th>
			  			</tr>
			  			<tr><b>Monthly</tr>
			  			<?php foreach($order_datas as $order_data) { ?>
			  				<tr>
			  					<td><?php echo $order_data['bill_date'] ?></td>
			  					<td><?php echo $order_data['grand_total'] ?></td>
			  					<td style="display: none;" id="new1"><?php echo $order_data['onaccust'] ?></td>
			  					<td style="display: none;"><?php echo $order_data['onacname'] ?></td>
			  					<td style="display: none;" id="new2" ><?php echo $order_data['order_id'] ?></td>
			  				</tr>
			  			<?php } ?>
		  			</table>

		  			<tr><b>Daily</tr>
		  			<table class="table table-bordered" id="tablefoodsss" style="cursor: pointer;width: 20%;margin-top: auto;">
			  			<tr>
			  				<th>Bill Date</th>
			  				<th>Grand Total</th>
			  				<th Style ="display: none;">Customer id</th>
			  				<th Style ="display: none;">creditcustname</th>
			  			</tr>
			  			

			  			<?php foreach($order_datasss as $order_datass) {  ?>
			  				<tr>
			  					<td><?php echo $order_datass['bill_date'] ?></td>
			  					<td><?php echo $order_datass['grand_total'] ?></td>
			  					<td style="display: none;" id="new11"><?php echo $order_datass['onaccust'] ?></td>
			  					<td style="display: none;"><?php echo $order_datass['onacname'] ?></td>
			  					<td style="display: none;" id="new22" ><?php echo $order_datass['order_id'] ?></td>
			  				</tr>
			  			<?php }  ?>
		  			</table>

					<div class="col-md-4">
						<table id="data" class="table table-bordered" style=" width: 70%;">
							<tr>
								<th>Item Name</th>
								<th>Date</th>
								<th>Qty</th>
								<th>Rate</th>
								<!-- <th>Grand Total</th> -->
								<!-- <th>Grand Total</th> -->
							</tr>
							<tr><th>Grand Total</th></tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#tablefood tr").click(function(){
	   var bill_date=$(this).find('td:first').html();
	   var onaccust=$(this).find('#new1').html();
	   var orderid=$(this).find('#new2').html();
	   var onacname=$(this).find('td:last').html();
	   $('#data').html('');
	   $.ajax({
	   		type: "POST",
	   		url: 'index.php?route=catalog/customer_history/getdatafood&token=<?php echo $token; ?>&bill_date='+bill_date+'&onaccust='+onaccust+'&onacname='+onacname+'&order_id='+orderid,
	   		dataType: 'json',
	   		success: function(data){
   				var $trm =$('<tr>').append(
   								$('<th>').text("Item Name"),
   								$('<th>').text("Date"),
   								$('<th>').text("Qty"),
   								$('<th>').text("Rate"),
   							);
   				$('#data').append($trm);

	   			$.each(data,function(i,fooditem){
	   				var $tr = $('<tr>').append(
	   								$('<td>').text(fooditem.name),
	   								$('<td>').text(fooditem.bill_date),
	   								$('<td>').text(fooditem.qty),
	   								$('<td>').text(fooditem.rate),
	   							);
	   					$('#data').append($tr);
	   			});

	   		
	   			var $tm =$('<tr>').append(
	   							$('<th colspan = 3>>').text("Grand Total"),
	   							// $('<th>').text(data[i].grand_total)
	   							$('<th>').text(data[0].grand_total)
	   				);

	   			$('#data').append($tm);
	   					
	   		}
	   })
	});

	$("#tablefoodsss tr").click(function(){
	   var bill_date=$(this).find('td:first').html();
	   var onaccust=$(this).find('#new11').html();
	   var orderid=$(this).find('#new22').html();
	   var onacname=$(this).find('td:last').html();
	   $('#data').html('');
	   $.ajax({
	   		type: "POST",
	   		url: 'index.php?route=catalog/customer_history/getdatafoods&token=<?php echo $token; ?>&bill_date='+bill_date+'&onaccust='+onaccust+'&onacname='+onacname+'&order_id='+orderid,
	   		dataType: 'json',
	   		success: function(data){
   				var $trm =$('<tr>').append(
   								$('<th>').text("Item Name"),
   								$('<th>').text("Date"),
   								$('<th>').text("Qty"),
   								$('<th>').text("Rate"),
   							);
   				$('#data').append($trm);

	   			$.each(data,function(i,fooditem){
	   				var $tr = $('<tr>').append(
	   								$('<td>').text(fooditem.name),
	   								$('<td>').text(fooditem.bill_date),
	   								$('<td>').text(fooditem.qty),
	   								$('<td>').text(fooditem.rate),
	   							);
	   					$('#data').append($tr);
	   			});

	   		
	   			var $tm =$('<tr>').append(
	   							$('<th colspan = 3>>').text("Grand Total"),
	   							// $('<th>').text(data[i].grand_total)
	   							$('<th>').text(data[0].grand_total)
	   				);

	   			$('#data').append($tm);
	   					
	   		}
	   })
	});


</script>


<?php echo $footer; ?>