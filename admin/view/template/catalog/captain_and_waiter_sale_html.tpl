<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
	<h1 style="text-align:center;font-weight: bold;color: #000;">
    <?php echo "Captain And Waiter Sale Report"; ?><br />
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
    <?php echo 'From : '. $filter_startdate; ?>
   	<?php echo 'To : '. $filter_enddate; ?><br />
    </span>
    <span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
  	</h1>
		<?php if ($final_datas || $final_datas_1) { ?>
		  	<table class="product" style="width:100% !important;">
				<thead>
					<tr>
						<td style="text-align: center;"><?php echo 'Date'; ?></td>
						<td style="text-align: center;"><?php echo 'No of Bill'; ?></td>
						<td style="text-align: center;"><?php echo 'Grand Total'; ?></td>
						
					</tr>
				</thead>
				<tbody>
				<?php if($final_datas_1){ ?>
					<?php foreach($final_datas_1 as $fkeys => $fvalues){ ?>
						<tr>
							<td colspan="3" style="text-align: left;  "><strong>Captain:-<?php echo $fvalues['name']; ?></strong></td>
						</tr>
						<?php foreach($fvalues['datas'] as $result) { ?>
							<tr>
								<td style="text-align: center;" class="left">
									<?php echo $result['date']; ?>
								</td>			          
								<td style="text-align: center;" class="left">
									<?php echo $result['no_of_bill']; ?>
								</td>
								<td style="text-align: center;" class="left">
									<?php echo $result['grand_total']; ?>
								</td>
								
							</tr>
						<?php } ?>
						<tr>
							<td style="font-weight: bold;" colspan="2">Total</td>
							<td style="font-weight: bold;text-align: center"><?php echo $fvalues['final_grand_total']; ?></td>
						</tr>
					<?php } ?>
				<?php } ?>
				<?php if($final_datas){ ?>
					<?php foreach($final_datas as $fkeys => $fvalues){ ?>
						<tr>
							<td colspan="3" style="text-align: left;  "><strong>Waiter:-<?php echo $fvalues['name']; ?></strong></td>
						</tr>
						<?php foreach($fvalues['datas'] as $result) { ?>
							<tr>			          
								<td style="text-align: center;" class="left">
									<?php echo $result['date']; ?>
								</td>
								<td style="text-align: center;" class="left">
									<?php echo $result['no_of_bill']; ?>
								</td>
								<td style="text-align: center;" class="left">
									<?php echo $result['grand_total']; ?>
								</td>
								
							</tr>
						<?php } ?>
						<tr>
							<td style="font-weight: bold;" colspan="2">Total</td>
							
							<td style="font-weight: bold;text-align: center"><?php echo $fvalues['final_grand_total']; ?></td>
						</tr>
					<?php } ?>
				<?php } ?>
				</tbody>
			</table>
		<?php }  ?>
 	</div>
</body>
</html>
