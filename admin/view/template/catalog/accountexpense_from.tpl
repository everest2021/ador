<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-department" class="form-horizontal">
                    <div class="form-group required">
                        <label class="col-sm-3 control-label"><?php echo 'Account Name'; ?></label>
                        <div class="col-sm-3">
                            <input type="text" name="exp_acc_name" value="<?php echo $exp_acc_name; ?>" placeholder="<?php echo 'Account Name'; ?>" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-3 control-label"><?php echo 'Account Type'; ?></label>
                        <div class="col-sm-3">
                            <select name="exp_acc_type" id="input-exp_acc_type" class="form-control">
                                <option value="0" ><?php echo "Please Select" ?></option>
                                <?php foreach($account_types as $skey => $svalue){ ?>
                                    <?php if($skey == $exp_acc_type){ ?>
                                        <option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-3 control-label"><?php echo 'contact No'; ?></label>
                        <div class="col-sm-3">
                            <input type="text" name="contact_no" value="<?php echo $contact_no; ?>" placeholder="<?php echo 'Contact No'; ?>" class="form-control" />
                            
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-3 control-label"><?php echo 'Address'; ?></label>
                        <div class="col-sm-3">
                            <input type="text" name="address" value="<?php echo $address; ?>" placeholder="<?php echo 'Address'; ?>" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-3 control-label"><?php echo 'GST No.'; ?></label>
                        <div class="col-sm-3">
                            <input type="text" name="gst_no" value="<?php echo $gst_no; ?>" placeholder="<?php echo 'GST No'; ?>" class="form-control" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
<script type="text/javascript">


$('#input-parent_id').on('change', function() {
  loc_name = $('#input-parent_id option:selected').text();
  $('#parent_category').val(loc_name);
 
});

$('#colorSelector').ColorPicker({
    color: '#0000ff',
    onShow: function (colpkr) {
        $(colpkr).fadeIn(500);
        return false;
    },
    onHide: function (colpkr) {
        $(colpkr).fadeOut(500);
        return false;
    },
    onChange: function (hsb, hex, rgb) {
        $('#colorSelector').css('backgroundColor', '#' + hex);
        $('#colorSelector').val('#' + hex);
    },
    onSubmit: function (hsb, hex, rgb) {
        $('#colorSelector').css('backgroundColor', '#' + hex);
        $('#colorSelector').val('#' + hex);
    }
});

$('#button-photo').on('click', function() {
  $('#form-photo').remove();
  $('body').prepend('<form enctype="multipart/form-data" id="form-photo" style="display: none;"><input type="file" name="file" /></form>');
  $('#form-photo input[name=\'file\']').trigger('click');
  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }
  timer = setInterval(function() {
    if ($('#form-photo input[name=\'file\']').val() != '') {
      clearInterval(timer); 
      image_name = 'subcategory';  
      $.ajax({
        url: 'index.php?route=catalog/category/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
        type: 'post',   
        dataType: 'json',
        data: new FormData($('#form-photo')[0]),
        cache: false,
        contentType: false,
        processData: false,   
        beforeSend: function() {
          $('#button-upload').button('loading');
        },
        complete: function() {
          $('#button-upload').button('reset');
        },  
        success: function(json) {
          if (json['error']) {
            alert(json['error']);
          }
          if (json['success']) {
            alert(json['success']);
            console.log(json);
            $('input[name=\'photo\']').attr('value', json['filename']);
            $('input[name=\'photo_source\']').attr('value', json['link_href']);
            d = new Date();
            var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="photo_source" href="'+json['link_href']+'">View Image</a>';
            $('#photo_source').remove();
            $('#button-photo').after(previewHtml);
          }
        },      
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});

</script>
<?php echo $footer; ?>