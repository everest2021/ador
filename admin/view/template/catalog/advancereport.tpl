<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<div class="col-sm-offset-3">
						       		<div class="form-row">
									    <div class="col-sm-3">
									    	<label>Start Date</label>
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<label>End Date</label>
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									    <div class="col-sm-3">
									    	<label>Status</label>
								    	   <select class="form-control" name="filter_status" id="input-filter_status">
								          	<option selected="selected" value="">All</option>
									          <?php foreach($status as $key => $value){?>
									          	<?php if($filter_status == $key && $filter_status != ''){ ?>
									          		<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
									          	<?php } else { ?>
									          		<option value="<?php echo $key ?>"><?php echo $value?></option>
									          	<?php } ?>
									          <?php } ?>
								          </select>
									    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
				 	<div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 	</div>
					<div class="col-sm-6 col-sm-offset-3">
					<center>
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Advance Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;">Receipt No</th>
							<th style="text-align: center;">Customer Name</th>
							<th style="text-align: center;">Booking Date</th>
							<th style="text-align: center;">Delivery Date</th>
							<th style="text-align: center;">Delivery Time</th>
							<th style="text-align: center;">Advance Amount</th>
							<th style="text-align: center;">Amount</th>
						</tr>
						<?php if(isset($_POST['submit'])){ ?>
							<?php foreach($advancedatas as $key) { /*echo "<pre>"; print_r($key)*/?>
								<tr>
									<td><?php echo $key['advancedata']['advance_billno'] ?></td>
									<td><?php echo $key['advancedata']['customer_name'] ?></td>
									<td><?php echo $key['advancedata']['booking_date'] ?></td>
									<td><?php echo $key['advancedata']['delivery_date'] ?></td>
									<td><?php echo $key['advancedata']['delivery_time'] ?></td>
									<td><?php echo $key['advancedata']['advance_amt'] ?></td>
									<td><?php echo $key['advancedata']['balance'] ?></td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td colspan="2"><b>Item name</b></td>
									<td colspan="2"><b>Item qty</b></td>
									<td></td>
								</tr>
								<?php foreach($key['advanceitem'] as $value) { /*echo "<pre>"; print_r($value)*/?>
									<tr>
										<td></td>
										<td></td>
										<td colspan="2"><?php echo $value['item_name'] ?></td>
										<td colspan="2"><?php echo $value['qty'] ?></td>
										<td></td>
									</tr>
								<?php } ?>
							<?php } ?>	  	
					  	<?php } ?>
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/reportbill/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_status = $('select[name=\'filter_status\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_status) {
			  url += '&filter_status=' + encodeURIComponent(filter_status);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}
	</script>

	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>