<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header">
		<div class="container-fluid">
	  		<div class="pull-right">
				<button type="submit" form="form-manufacturer" data-toggle="tooltip" title="Send" class="btn btn-primary"><i></i>Send Sms</button>
				<a href="<?php echo $reset; ?>" data-toggle="tooltip" title="RESET" class="btn btn-primary">RESET</a>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
	  			<h1><?php echo $heading_title; ?></h1>
	  			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
	  		</ul>
		</div>
  	</div>
  	<div class="container-fluid">
	<?php if ($error_warning) { ?>
	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	  	<button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	<?php } ?>
	<?php if ($warning) { ?>
	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $warning; ?>
	  	<button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	<?php } ?>
	<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>
	<div class="panel panel-default">
	  	<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
	  	</div>
	  	<div class="panel-body">
			<form action="<?php echo $actionss; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
				<div class="form-group">
			 		<label class="col-sm-2 control-label" for="input-division"><?php echo 'Template'; ?></label>
			  		<div class="col-sm-3">
				  	<select  name="tem_id" id='input-division' class="form-control" style="cursor:pointer;">
						<?php foreach($templets as $skey => $svalue) { ?>
					  		<?php if($svalue['tem_id'] == $tem_id) { ?>
								<option value="<?php echo $svalue['tem_id'] ?>" selected="selected" style="cursor:pointer;"><?php echo $svalue['tem_name'] ?></option>
					  		<?php } else { ?>
								<option value="<?php echo $svalue['tem_id'] ?>" style="cursor:pointer;"><?php echo $svalue['tem_name'] ?></option>
					  		<?php } ?>
						<?php } ?>
				  	</select>
			  		</div>
		  		</div>
		  	<div style="display: none;" class="form-group">
			<label class="col-sm-2 control-label"><?php echo 'Customers'; ?></label>
				<div class="col-sm-3">
				  	<select  name="id" id='input-customer' class="form-control" style="cursor:pointer;">
					<?php foreach($batch_datas as $ckey => $cvalue) { ?>
					  	<?php if($cvalue['id'] == $id) { ?>
							<option value="<?php echo $cvalue['id'] ?>" selected="selected" style="cursor:pointer;"><?php echo $cvalue['value'] ?></option>
					  	<?php } else { ?>
							<option value="<?php echo $cvalue['id'] ?>" style="cursor:pointer;"><?php echo $cvalue['value'] ?></option>
					  	<?php } ?>
					<?php } ?>
				  </select>
			  	</div>
		  	</div>
		  	<div style="display: none;" class="form-group">
			<label class="col-sm-2 control-label"><?php echo 'Link Account'; ?></label>
				<div class="col-sm-3">
				  	<select  name="link_id" id='input-link' class="form-control" style="cursor:pointer;">
					<?php foreach($link_datas as $ckey => $cvalue) { ?>
					  	<?php if($ckey == $link_id) { ?>
							<option value="<?php echo $ckey ?>" selected="selected" style="cursor:pointer;"><?php echo $cvalue ?></option>
					  	<?php } else { ?>
							<option value="<?php echo $ckey ?>" style="cursor:pointer;"><?php echo $cvalue ?></option>
					  	<?php } ?>
					<?php } ?>
				  </select>
			  	</div>
		  	</div>
		  	<?php /* ?>
			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo 'Customers'; ?></label>
			<div  class="col-sm-5">
				<div id = "studata" class="well well-sm" style="height: 150px; overflow: auto;">
				  <?php foreach ($customers as $skey => $svalue) { ?>
					  <div class="checkbox">
						  <label>
							  <input type="checkbox" name="name[]" value="<?php echo $svalue['customer_id']; ?>"  />
							  <?php echo $svalue['fullname'] ; ?>
						  </label>
					  </div>
				  <?php } ?>
			   </div>
				<a onclick="$(this).parent().find(':checkbox').prop('checked', true);" style="cursor:pointer;"><?php echo 'Select All'; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);" style="cursor:pointer;"><?php echo 'Un Select All'; ?></a>
			</div>
		  </div>
		  <?php */ ?>
		</form>
	  </div>
	</div>
	<div class="pull-right">
		<button type="submit" form="form-manufacturer" data-toggle="tooltip"  class="btn btn-primary"><i class="">Send Sms</i></button>
		<a href="<?php echo $reset; ?>" data-toggle="tooltip" class="btn btn-primary">RESET</a>
		<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
  </div>
</div>
<script type="text/javascript">



$('#input-standard_id,#input-division_id').change(function() {
var standard_id = $('#input-standard_id').val();
var division_id = $('#input-division_id').val();
$.ajax({
	url: 'index.php?route=catalog/sms/getstudents&token=<?php echo $token; ?>&standard_id=' +  encodeURIComponent(standard_id) + '&division_id=' +  encodeURIComponent(division_id),
	dataType: 'json',
	success: function(json) {
	  console.log(json);
	  $('#studata').replaceWith(json.html);
	}
  });
});
// $('#input-standard').on('change', function() {
//   url = 'index.php?route=report/schoolfee&token=<?php echo $token; ?>';
  
//   var filter_month_start = $('select[name=\'standard_id\']').val();
  
//   if (filter_month_start) {
//     url += '&filter_month_start=' + encodeURIComponent(filter_month_start);
//   }

//   var filter_month_end = $('select[name=\'filter_month_end\']').val();
  
//   if (filter_month_end) {
//     url += '&filter_month_end=' + encodeURIComponent(filter_month_end);
//   }
	
//   var filter_year = $('select[name=\'filter_year\']').val();
  
//   if (filter_year) {
//     url += '&filter_year=' + encodeURIComponent(filter_year);
//   }

//   filter_fees_type_selected = $('#input-fees_type').val();
//   fees_name = '';
//   for(i=0;i<filter_fees_type_selected.length; i++){
//     fees_name += filter_fees_type_selected[i]+',';
//   }
//   //var filter_owner = $('input[name=\'filter_owner\']').attr('value');
//   if (fees_name) {
//       url += '&filter_fees_type=' + encodeURIComponent(fees_name);
//   }

//   url += '&once=1';
  
//   location = url;
// });

</script>
<?php echo $footer; ?>