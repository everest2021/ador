<?php echo $header; ?><?php echo $column_left; ?>
<?php
	header('refresh:2; url='.$refresh_url);
?>
<div id="content">
  	<div class="container-fluid" style="padding-left: 0px !important; padding-right: 0px !important;">
	  	<div class="panel panel-default" style="background-color: #000;">
		  	<div class="panel-body" style="padding: 0px !important;">
			  	<div class="table-responsive" style="overflow-x: visible !important;margin-top: 0%;">
					<table class="table table-bordered table-hover">
				  		<thead>
							<tr>
					  			<td style="width: 50%;color: #fff;font-size: 30px;" class="text-center">
					  				<?php echo 'In Process'; ?>
					  			</td>
					  			<td style="width: 50%;color: #fff;font-size: 30px;" class="text-center">
									<?php echo 'Ready'; ?>
								</td>
							</tr>
				  		</thead>
				  		<tbody>
							<tr>
								<td class="text-center" style="border-bottom: 0px !important;padding: 10px;vertical-align: top;">
									<?php foreach($inprocess_datas as $ikey => $ivalue){ ?>
										<span style="font-size: 55px;cursor: default;color: orange;"><?php echo $ivalue['order_no'] ?></span><br />
									<?php } ?>
						  		</td>
						  		<td class="text-center" style="border-bottom: 0px !important;padding: 10px;vertical-align: top;">
									<?php foreach($ready_datas as $ikey => $ivalue){ ?>
										<span style="font-size: 55px;cursor: default;color: green;"><?php echo $ivalue['order_no'] ?></span><br />
									<?php } ?>
						  		</td>
							</tr>
				  		</tbody>
					</table>
				</div>
				<!-- <div class="row">
				    <div class="col-sm-6 text-left"><?php //echo $pagination; ?></div>
				    <div class="col-sm-6 text-right"><?php //echo $results; ?></div>
				</div> -->
			</div>
		</div>
	</div>
	<script type="text/javascript">
	</script>
</div>
<?php echo $footer; ?>