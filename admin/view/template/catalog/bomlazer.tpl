<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" id="form" enctype="multipart/form-data">
				<div class="panel-body"  style="overflow-x: auto;overflow-y: auto">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Data</b></h4></center><br>
					       		<div class="form-row">
								    <div class="col-sm-2 col-sm-offset-3">
								    	<label>Start Date</label>
								     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
								    </div>
								    <div class="col-sm-2">
								    	<label>End Date</label>
								    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
								    </div>
								    <div class="col-sm-2">
								    	<label>Select Store</label>
								    	<select class="form-control" name="filter_storename">
								    		<option value="">Please Select</option>
								    		<?php foreach($stores as $value) { ?>
									    		<?php if($value['outlet_id'] == $storename ) { ?>
									    			<option value="<?php echo $value['outlet_id'] ?>" selected="selected"><?php echo $value['outlet_name']?></option>
									    		<?php } else { ?>
									    			<option value="<?php echo $value['outlet_id'] ?>"><?php echo $value['outlet_name']?></option>
									    		<?php } ?>
								    		<?php } ?>
									    </select>
								    </div>
									<div class="col-sm-12">
										<br>
										<center><input type="submit" class="btn btn-primary" value="Show"></center>
									</div>
								</div>
							</div>
					    </div>
					</div>
					<div class="col-md-12">
						<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
						<?php date_default_timezone_set("Asia/Kolkata");?>
						<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
						<center><h3><b>BOM Lazer</b></h3></center>
						<h3>From : <?php echo $startdate; ?></h3>
						<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  	<table class="table table-bordered table-hover" style="text-align: center;">
							<thead>
								<tr>
									<td style="text-align: center;"><?php echo 'Sr.No'; ?></td>
									<td style="text-align: center;">Menu Items</td>
									<?php foreach($bom_items as $bom_item) { ?>
										<td style="text-align: center;"><?php echo $bom_item['name']; ?></td>
									<?php } ?>
								</tr>
							</thead>
							<?php if ($final_data) {?>
							<tbody>
								<?php foreach($final_data as $fkey =>$fvalue) { ?>
									<tr>
										<td><?php echo $fvalue['i']?></td>
										<td><?php echo $fvalue['item_name']?></td>
										<?php foreach($fvalue['bom_data'] as $bom_data) { ?>
											<td><?php echo $bom_data['quantity'] ?></td>
										<?php } ?> 
									</tr>
								<?php }  ?>
								<tr>
									<td colspan="2">Grand Total</td>
									<?php foreach($bom_items as $key => $value) { ?>
										<td><?php echo $value['total'] ?></td>
									<?php } ?>
								</tr>
							</tbody>
						</table>
						<?php }  ?>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker({
			dateFormat: 'dd-mm-yy',			
	  	});

		$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/reportstock/itemwisereport&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_storename = $('select[name=\'filter_storename\']').val();
		 

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_storename) {
			  url += '&filter_storename=' + encodeURIComponent(filter_storename);
		  }


		  location = url;
		  setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}

		$('#type').on('change', function() {
			$('#form').submit();
		});
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>