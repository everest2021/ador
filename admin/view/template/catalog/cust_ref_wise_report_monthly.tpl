<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-2" >
						       		<div class="form-row">
									    <div class="col-sm-3">
									    	<label>Start Date</label>
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<label>End Date</label>
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									    <div  class="col-sm-3">
								       		<label>Customer Name</label>
									    	<input type="text" id="filter_cust_name" name="filter_cust_name" value="<?php echo $filter_cust_name ?>" class="form-control" placeholder=" Customer Name">
									    	<input type="hidden" id="filter_cust_id" name="filter_cust_id" value="<?php echo $filter_cust_id ?>" class="form-control" placeholder=" Customer Id">
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show">
							 		<button id="print" type="button" class="btn btn-primary">Print</button></center>
								</div>
							</div>
					    </div>
					</div>
					
				 	<div class="col-sm-offset-10">
				 		<button style="display:none" id="export" type="button" class="btn btn-primary">Export</button>
				 	</div>
					<div class="col-sm-8 col-sm-offset-2">
					<!-- <h4>
					<center>
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center><h4> -->
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Monthly Custumer Refrens Wise Report</b></h3></center>
					<h3>From : <?php echo date('d/m/Y',strtotime($startdate)); ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo date('d/m/Y',strtotime($enddate)); ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						
						<tr>
							<th style="text-align: center;">Ref. No</th>
							<th style="text-align: center;">Date</th>
							<th style="text-align: center;" colspan="3">Customer Name</th>
							<th style="text-align: center;">Customer Address</th>
							<th style="text-align: right;">Contact No</th>
							<th style="text-align: right;">Total Amt.</th>
							<th style="text-align: right;">Point</th>
							<th style="text-align: right;">Total Point</th>
						</tr>
							<?php if(!empty($cust_datas)) { //echo "<pre>";print_r($billcnt);exit; ?>
							
							  	<?php foreach ($cust_datas as $keys => $data) {  ?>
						  				<tr>
						  					<td colspan="10"><b><?php echo $keys ?></td>
						  				</tr>

							  			<?php foreach($data as $key => $value) { //echo "<pre>";print_r($cust_po);exit; ?>	
								  			<tr>
										  		<td><?php echo $value['cust_id']; ?></td>
										  		<td><?php echo $value['bill_date']; ?></td>
										  		<td colspan="3"><?php echo $value['cust_name']; ?></td>
										  		<td><?php echo $value['cust_address']; ?></td>
										  		<td style="text-align: right;"><?php echo $value['cust_contact'] ?></td>
										  		<td style="text-align: right;"><?php echo $value['grand_total'] ?></td>
										  		<td style="text-align: right;"><?php echo $value['cust_point'] ?></td>
									  			<td style="text-align: right;"><?php echo $cust_po[$key]['cust_poin'] ?></td>
										  	</tr>
									    <?php } ?>
							  	<?php } ?>
						  	<tr>
							  	<td style="text-align: right;" colspan="7"><b><?php echo "Grand Total" ?></td>
							  	
							  	<td style="text-align: right;" colspan="3"><b><?php echo $gttotal['grandt']; ?></td>
							</tr> 
							<tr>  	
							  	<td style="text-align: right;" colspan="7"><?php echo "Total Bill" ?></td>
							  	
							  	<td style="text-align: right;" colspan="3"><?php echo $billcnt['billcnt']; ?></td>
							</tr>  	
							<?php } ?>							  	
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

		function close_fun_1(){
			window.location.reload();
		}

		$('#export').on('click', function() {
		  var url = 'index.php?route=catalog/customerwise_item_report/export&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_cust_id = $('input[name=\'filter_cust_id\']').val();
		  var filter_cust_name = $('input[name=\'filter_cust_name\']').val();
		  //alert(filter_cust_name);

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_cust_id) {
			  url += '&filter_cust_id=' + encodeURIComponent(filter_cust_id);
		  }

		  //alert(url);
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/cust_ref_wise_report_monthly/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
	      var filter_cust_id = $('input[name=\'filter_cust_id\']').val();
		  var filter_cust_name = $('input[name=\'filter_cust_name\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_cust_id) {
			  url += '&filter_cust_id=' + encodeURIComponent(filter_cust_id);
		  }

		  if (filter_cust_name) {
			  url += '&filter_cust_name=' + encodeURIComponent(filter_cust_name);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}


		$('#filter_cust_name').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
		  		//filter_type = $('#filter_type').val();
				$.ajax({
			  		url: 'index.php?route=catalog/customerwise_item_report/autocomplete_name&token=<?php echo $token; ?>&filter_cust_name=' +  encodeURIComponent(request.term),
			  		dataType: 'json',
			  		success: function(json) {   
						response($.map(json, function(item) {
				  			return {
								label: item.name,
								value: item.id,
				  			}
						}));
			  		}
				});
		  	}, 
		  	select: function(event, ui) {
		  		$('#filter_cust_id').val(ui.item.value);
		  		$('#filter_cust_name').val(ui.item.label);
		  		return false;
		  	}
		});
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>

</div>
<?php echo $footer; ?>