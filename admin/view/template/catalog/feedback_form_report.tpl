<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" >
  	<div class="page-header">
		<div class="container-fluid">
	  		<div class="pull-right">
				
		  		<button style="display: none;" type="submit" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
	  		<h1><?php echo $heading_title; ?></h1>
	  		<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
	  		</ul>
		</div>
  	</div>
  	<div class="container-fluid" >
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	  			<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
	  			<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default" >
	  		<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
	  		</div>
	  		<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
					
					<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-contact_no"><?php echo 'Contact Number' ?></label>
						<div class="col-sm-3">
			  				<input type="text" name="contact_no" value="<?php echo $contact_no; ?>"  id="input-contact_no" class="form-control" readonly/>
							<?php if ($error_contact_no) { ?>
			                	<div class="text-danger"><?php echo $error_contact_no; ?></div>
			                <?php } ?>
						</div>
		  			</div>

					<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-name"><?php echo 'Customer Name' ?></label>
						<div class="col-sm-3">
			  				<input type="text" name="name" value="<?php echo $name; ?>"  id="input-name" class="form-control"  readonly/>
							<?php if ($error_name) { ?>
			                	<div class="text-danger"><?php echo $error_name; ?></div>
			                <?php } ?>
						</div>
		  			</div>

					<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-email"><?php echo 'E-mail Id' ?></label>
						<div class="col-sm-3">
			  				<input type="text" name="email" value="<?php echo $email; ?>"  id="input-email" class="form-control"  readonly/>
							<?php if ($error_email) { ?>
			                	<div class="text-danger"><?php echo $error_email; ?></div>
			                <?php } ?>
						</div>
		  			</div>
		  			

		   			<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-location"><?php echo 'Location' ?></label>
						<div class="col-sm-3">
							<select style="font-size: 14px;" class="form-control" name="location" disabled="disabled">
					    		<?php foreach($location_datas as $key => $value) { ?>
						    		<?php if($key == $location ) { ?>
						    			<option disabled="disabled" value="<?php echo $key ?>" selected="selected" ><?php echo $value ?></option>
						    		<?php } else { ?>
						    			<option disabled="disabled" value="<?php echo $key ?>" ><?php echo $value?></option>
						    		<?php } ?>
					    		<?php } ?>
						    </select>
						</div>
		   			</div>

		   			<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-dob"><?php echo 'Date Of Birth' ?></label>
						<div class="col-sm-1">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-date_dob"><?php echo 'Date' ?></label>
							<select name="date_dob" id="input-date_dob" class="form-control">
		                  		<?php for ($date=1; $date<=31; $date++) { ?>
		                  			<?php if($date == $date_dob){ ?>

		                  				<option selected="selected" value="<?php echo $date ?>"><?php echo $date_dob ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $date ?>"><?php echo $date ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
						<div class="col-sm-1">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-month_dob"><?php echo 'Month' ?></label>
							<select name="month_dob" id="input-month_dob" class="form-control">
		                  		<?php for ($month=1; $month<=12; $month++) { ?>
		                  			<?php if($month == $month_dob){ ?>
		                  				<option selected="selected" value="<?php echo $month ?>"><?php echo $month_dob ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $month ?>"><?php echo $month ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
						<div class="col-sm-2">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-year_dob"><?php echo 'Year' ?></label>
							<select name="year_dob" id="input-year_dob" class="form-control">
		                  		<?php for ($year=1950; $year<=2020; $year++) { ?>
		                  			<?php if($year == $year_dob){ ?>
		                  				<option selected="selected" value="<?php echo $year ?>"><?php echo $year_dob ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $year ?>"><?php echo $year ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
		   			</div>

		   			<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-date_doa"><?php echo 'Date Of Anniversary' ?></label>
						<div class="col-sm-1">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-date_doa"><?php echo 'Date' ?></label>
							<select name="date_doa" id="input-date_doa" class="form-control">
		                  		<?php for ($date=1; $date<=31; $date++) { ?>
		                  			<?php if($date == $date_doa){ ?>
		                  				<option selected="selected" value="<?php echo $date_doa ?>"><?php echo $date_doa ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $date_doa ?>"><?php echo $date ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
						<div class="col-sm-1">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-month_doa"><?php echo 'Month' ?></label>
							<select name="month_doa" id="input-month_doa" class="form-control">
		                  		<?php for ($month=1; $month<=12; $month++) { ?>
		                  			<?php if($month == $month_doa){ ?>
		                  				<option selected="selected" value="<?php echo $month_doa ?>"><?php echo $month_doa ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $month ?>"><?php echo $month ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>

						</div>
						<div class="col-sm-2">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-year_doa"><?php echo 'Year' ?></label>
							<select name="year_doa" id="input-year_doa" class="form-control">
		                  		<?php for ($year=1950; $year<=2020; $year++) { ?>
		                  			<?php if($year == $year_doa){ ?>
		                  				<option selected="selected" value="<?php echo $year_doa ?>"><?php echo $year_doa ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $year_doa ?>"><?php echo $year ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
		   			</div>

		   			<?php $i=0; ?>
		   			<?php foreach ($question_datas as $qkey => $qvalue) { ?>
		   				<?php ++$i; ?>
		   				<?php // echo "<pre>";print_r($question_datas);exit; ?>
			   			<div  class="form-group <?php echo ($i%2) ? 'even' : 'odd' ?> ">
			                <label style="font-size: 22px;" class="col-sm-6 control-label" for="input-food_have"><?php echo $qvalue['question'] ?></label>
			                <div class="col-sm-6">
			                	<?php $array = explode(',', $qvalue['q_ans']); 
			                	$question_id = $qvalue['id']; ?>
								<?php if($ans_datas){ ?>
									<?php foreach ($ans_datas as $akey =>$avalue) { ?>
										<?php if($qvalue['id'] == $avalue['question_id']){ ?>
			                    			<input style="font-size: 20px;" type="radio" name="<?php echo 'ans_datas['."$question_id".']' ?>"  value="<?php echo $avalue['values'] ?>" checked="checked">&nbsp;&nbsp;<span style="font-size: 20px;"><?php echo $avalue['values'] ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<br>
			            				<?php } ?>
			            			<?php } ?>
	            				<?php } ?>
			            	</div>
	           	 		</div>
	           	 	<?php } ?>
	           	 	<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-suggestion"><?php echo 'Suggestion' ?></label>
						<div class="col-sm-3">
			  				<textarea type="text" name="suggestion"  readonly id="input-suggestion" class="form-control"/><?php echo $suggestion; ?>
			  				</textarea>
						</div>
		  			</div>
		  			<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-remark"><?php echo 'Remark' ?></label>
						<div class="col-sm-3">
			  				<textarea type="text" name="remark" readonly value="<?php echo $remark; ?>"  id="input-remark" class="form-control"/>
			  				<?php echo $remark; ?>
			  				</textarea>
						</div>
		  			</div>
				</form>
		  	</div>
	  	</div>
		<div class="form-group">
	  		<div class="col-sm-5">
				<button  style="display: none;" style="margin-top: 0px;margin-bottom: 2px;margin-left: 170px;font-size: 20px; " type="submit" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary col-sm-5"><i >Save</i></button>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
.even{
 background-color:#cccccc;
 
}
.odd{
 background-color:white;
}
</style>
</div>
<script type="text/javascript">
	$(".form_datetime").datepicker({
			dateFormat: 'dd-mm-yy',			
	  	});

// Contact search

$('input[name=\'contact_no\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/feedback/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            value: item.contact_no,
            contact_no: item.contact_no,
            name: item.name,
            location: item.location,
            dob: item.dob,
            doa: item.doa,
            email: item.email,

          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
  	//console.log(ui.item);
    $('input[name=\'name\']').val(ui.item.name);
    $('input[name=\'contact_no\']').val(ui.item.contact_no);
    $('input[name=\'location\']').val(ui.item.location);
    $('input[name=\'dob\']').val(ui.item.dob);
    $('input[name=\'doa\']').val(ui.item.doa);
    $('input[name=\'email\']').val(ui.item.email);

    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});


</script>
<?php echo $footer; ?>