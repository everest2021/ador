<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <?php if($show_item_off == 1) { ?>
            <a  data-toggle="tooltip" title="<?php echo ' Items ON/OFF'; ?>" class="btn btn-primary"> Item ON/OFF Button Disbled Threee Attemt</a>&nbsp;&nbsp;
        <?php } else { ?>
                 <a  href="<?php echo $item_onoff; ?>" data-toggle="tooltip" title="<?php echo ' Items ON/OFF'; ?>" class="btn btn-primary"> Item ON/OFF</a>&nbsp;&nbsp;
        <?php } ?>

        <?php if($store_on_off == 0) { ?>
            <a style="background-color: #e72b0b;border-color:#e72b0b;" onclick="store_onoff()" data-toggle="tooltip" title="<?php echo ' Store ON/OFF'; ?>" class="btn btn-primary"> Store ON/OFF</a>&nbsp;&nbsp;
        <?php } else { ?>
              <a style="background-color: #e72b0b;border-color:#e72b0b;" data-toggle="tooltip" title="<?php echo ' Store ON/OFF'; ?>" class="btn btn-primary"> Store ON/OFF Item ON/OFF Button Disbled Threee Attemt</a>&nbsp;&nbsp;
        <?php } ?>
        <a style="background-color: #e72b0b;border-color:#e72b0b;" onclick="live_platform()" data-toggle="tooltip" class="btn btn-primary"> Live Platform</a>&nbsp;&nbsp;
        <a href="<?php echo $back; ?>" data-toggle="tooltip" style="background-color: #f33333;border-color:#f33333;" class="btn btn-warning">Back</a>
         <!-- &nbsp;&nbsp;<a href="<?php echo $back; ?>"  class="btn btn-warning" style="background-color: #f33333;border-color:#f33333;position: absolute !important;bottom: 2px !important;right: 5px !important;width: 55px !important;  ">Back </a> -->

      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
          <div class="well">
           <div class="row">
               <div class="col-sm-4">
                   <div class="form-group">
                      <label class="control-label" for="input-name"><?php echo 'Item Name'; ?></label>
                      <input type="text" name="filter_item_name" value="<?php echo $filter_item_name; ?>" placeholder="<?php echo 'Item Name'; ?>" id="input-filter_item_name" class="form-control" />
                      <input type="hidden" name="filter_item_id" value="<?php echo $filter_item_id; ?>" id="input-filter_item_id" class="form-control" />
                     
                    </div>
               </div>
               <div class="col-sm-4">
                   <div class="form-group">
                       <label class="control-label" for="input-location_name"><?php echo 'Item Code'; ?></label>
                       <input type="text" name="filter_item_code" value="<?php echo $filter_item_code; ?>" placeholder="<?php echo 'Item Code'; ?>" id="input-filter_item_code" class="form-control" />
                        <button type="button" id="button-filter" class="btn btn-primary pull-right" style="margin-top: 2%;"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
                    </div>
                </div>
                
               
                
            </div>
       </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-product">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                    <td class="text-left">Code</td>
                    <td class="text-left">Title</td>
                    <td class="text-left">Category </td>
                    <td class="text-left">Food Type </td>
                    <td class="text-left">Price </td>
                    <td class="text-left">Included Platform </td>
                    <td class="text-left">Serves </td>
                    <td class="text-left">Stock</td>
                    <td class="text-left">Action</td>
                </tr>
              </thead>
              <tbody>
                <?php if ($products) { ?>
                    <?php foreach ($products as $product) { ?>
                        <tr>
                             <td class="text-left"><?php echo $product['item_id']; ?></td>
                            <td class="text-left"><b style="color:#2196f3"><?php echo $product['title']; ?></b></td>
                            <td class="text-left"><?php echo $product['category_ref_ids']; ?></td>
                            <td class="text-left"><?php echo $product['food_type']; ?></td>
                            <td class="text-left"><?php echo $product['price']; ?></td>
                            <td class="text-left"><?php echo $product['included_platforms']; ?></td>
                             <td class="text-left"><?php echo $product['serves']; ?></td>
                              <td class="text-left"><?php echo $product['current_stock']; ?></td>
                            <td class="text-right">
                                 <?php if($show_item_off == 1) { ?>
                                     <?php echo 'Item ON/OFF Button Disbled Threee Attemt'; ?>
                                 <?php } else { ?>
                                         <?php if($product['item_onoff_status'] == '0' ){ ?>
                                        <a href="<?php echo $product['edit']; ?>" data-toggle="tooltip" title="<?php echo "Item ON/OFF" ?>" class="btn btn-danger"><i class="fa fa-toggle-on"></i></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $product['edit']; ?>" data-toggle="tooltip" title="<?php echo "Item ON/OFF" ?>" class="btn btn-success"><i  class="fa fa-toggle-on"></i></a>
                                        <?php }?>
                                 <?php }?>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                      <td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
                    </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
    <div id="dialog_storeonoff" title="Store ON / OFF">
            
        </div>
  </div>
  <script type="text/javascript"><!--

$('#button-filter').on('click', function() {
    var url = 'index.php?route=catalog/urbanpiperitem_show&token=<?php echo $token; ?>';

    var filter_item_name = $('input[name=\'filter_item_name\']').val();
    if (filter_item_name) {
        var filter_item_id = $('input[name=\'filter_item_id\']').val();
        if (filter_item_id) {
            url += '&filter_item_id=' + encodeURIComponent(filter_item_id);
            url += '&filter_item_name=' + encodeURIComponent(filter_item_name);
        }
    }

    var filter_item_code = $('input[name=\'filter_item_code\']').val();
    if (filter_item_code) {
        url += '&filter_item_code=' + encodeURIComponent(filter_item_code);
    }

    var filter_barcode = $('input[name=\'filter_barcode\']').val();
    if (filter_barcode) {
        url += '&filter_barcode=' + encodeURIComponent(filter_barcode);
    }
    
    location = url;
});

$('input[name=\'filter_item_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/urbanpiperitem_show/autocomplete&token=<?php echo $token; ?>&filter_item_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.item_name,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_item_name\']').val(ui.item.label);
    $('input[name=\'filter_item_id\']').val(ui.item.value);
        
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});

$('input[name=\'filter_item_code\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/urbanpiperitem_show/autocompletecode&token=<?php echo $token; ?>&filter_item_code=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.item_code,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_item_code\']').val(ui.item.label);
        
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});

function live_platform() {
    //load_unseen_notification("closed_re");

    platform_list1 = '<?php echo $platform_list; ?>';
    platform_list1 = platform_list1.replace("&amp;", "&");

    window.location = platform_list1; 
}

function store_onoff(){
    //load_unseen_notification("closed_re");
    htmlc = '<iframe id="itemdata" style="border: 0px;" src="index.php?route=catalog/store_onoff&token=<?php echo $token; ?>&status='+status+'" width="100%" height="100%"></iframe>';
    dialog_storeonoff.dialog("open");
    $('#dialog_storeonoff').html(htmlc);
    //location.reload();
}

dialog_storeonoff = $("#dialog_storeonoff").dialog({
    autoOpen: false,
    height: 600,
    width: 900,
    modal: true,
    dialogClass: 'myTitleClass',
    buttons: {

    },
    close: function(event, ui) {
        $("#overlay").fadeIn(200);　
        location.reload();
    },

});

  function closeIFrame(){
        //load_unseen_notification("closed_re");
        location.replace('index.php?route=catalog/urbanpiperitem_show&token=<?php echo $token; ?>');
        //window.location.reload();     
    }
//--></script></div>
<?php echo $footer; ?>