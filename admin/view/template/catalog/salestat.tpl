<?php echo $header; ?>
<div id="content" class="sucess" style="overflow-y: hidden;">
  <div class="container-fluid">
	<div class="panel-body" style="padding-bottom: 0%;">
		<div class="row">
	  		<div class="col-sm-5">
	  			<h3>All Time Famous Food</h3>
				<table style="text-align: left;" class="table table-bordered table-hover">
					<thead>
						<th>No.Of Sale</th>
						<th>Item Name</th>
					</thead>
					<tbody>
						<?php foreach ($famous_food as $fkey => $fvalue) { ?>
							<tr>
								<td><?php echo $fvalue['total_cnt'] ?></td>
								<td><?php echo $fvalue['name'] ?></td>
							</tr>	
						<?php } ?>
					</tbody>
				</table>
			</div>

			<div class="col-sm-5 col-sm-offset-1">
	  			<h3>All Time Famous Liquor</h3>

				<table style="text-align: left;" class="table table-bordered table-hover">
					<thead>
						<th>No.Of Sale</th>
						<th>Item Name</th>
					</thead>
					<tbody>
						<?php foreach ($famous_liq as $fkey => $fvalue) { ?>
							<tr>
								<td><?php echo $fvalue['total_cnt'] ?></td>
								<td><?php echo $fvalue['name'] ?></td>
							</tr>	
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>


		<div class="row">
	  		<div class="col-sm-5 ">
	  			<h3>Weekly Famous Food</h3>
				<table style="text-align: left;" class="table table-bordered table-hover">
					<thead>
						<th>No.Of Sale</th>
						<th>Item Name</th>
					</thead>
					<tbody>
						<?php foreach ($weekly_famous_food as $fkey => $fvalue) { ?>
							<tr>
								<td><?php echo $fvalue['total_cnt'] ?></td>
								<td><?php echo $fvalue['name'] ?></td>
							</tr>	
						<?php } ?>
					</tbody>
				</table>
			</div>

			<div class="col-sm-5 col-sm-offset-1">
	  			<h3>Weekly Famous Liquor</h3>

				<table style="text-align: left;" class="table table-bordered table-hover">
					<thead>
						<th>No.Of Sale</th>
						<th>Item Name</th>
					</thead>
					<tbody>
						<?php foreach ($weekly_famous_liq as $fkey => $fvalue) { ?>
							<tr>
								<td><?php echo $fvalue['total_cnt'] ?></td>
								<td><?php echo $fvalue['name'] ?></td>
							</tr>	
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		

		<div class="row">
	  		<div class="col-sm-5 ">
	  			<h3>Highest Sale</h3>
				<table style="text-align: left;" class="table table-bordered table-hover">
					<thead>
						<th>Day</th>
						<th>Date</th>
						<th>Amount</th>
					</thead>
					<tbody>
						<?php foreach ($highest_sale as $hkey => $hvalue) { ?>
							<tr>
								<td><?php echo date('l',strtotime($hvalue['bill_date'])) ?></td>
								<td><?php echo date('d-m-Y',strtotime($hvalue['bill_date'])) ?></td>
								<td><?php echo $hvalue['g_total'] ?></td>
							</tr>	
						<?php } ?>
					</tbody>
				</table>
			</div>

			
		</div>
		
		<div class="col-sm-12">
			<table style="" class="table table-bordered table-hover">
		  		
			</table>
		</div>
	</div>
<!-- 	<script type="text/javascript">
		var data = $('#netamount').val();
		var afterdec = data.split('.')[1];
		alert(afterdec);
	</script> -->
</div>
<?php echo $footer; ?>