<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header">
		<div class="container-fluid">
	  		<div class="pull-right">
		  		<button type="submit" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a style="display: none;" href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
	  		<h1><?php echo $heading_title; ?></h1>
	  		<ul class="breadcrumb" style='display:none;'>
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
	  		</ul>
		</div>
  	</div>
  	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	  			<button type="button"  class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
	  			<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
	  		<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
	  		</div>
	  		<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
					<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-name"><?php echo 'Table No' ?></label>
						<div class="col-sm-3">
			  				<input type="text" name="table_no" value=""  id="input-table_no" class="form-control"  />
						</div>
						<div class="col-sm-3">
							<button type="button" id="button-filter" onclick="custdata()" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'GO'; ?></button>
						</div>
		  			</div>
		  			
	           	 	

		  			<div class="form-group">
		  				<label style="font-size: 22px;" >Personal Info:-</label></div>

		  			<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-contact_no"><?php echo 'Contact Number' ?></label>
						<div class="col-sm-3">
			  				<input type="number" name="contact_no" value="<?php echo $contact_no; ?>"  id="input-contact_no" class="form-control" />
							<?php if ($error_contact_no) { ?>
			                	<div class="text-danger"><?php echo $error_contact_no; ?></div>
			                <?php } ?>
						</div>
		  			</div>

					<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-name"><?php echo 'Customer Name' ?></label>
						<div class="col-sm-3">
			  				<input type="text" name="name" value="<?php echo $name; ?>"  id="input-name" class="form-control"  />
							<?php if ($error_name) { ?>
			                	<div class="text-danger"><?php echo $error_name; ?></div>
			                <?php } ?>
						</div>
		  			</div>

					<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-email"><?php echo 'E-mail Id' ?></label>
						<div class="col-sm-3">
			  				<input type="text" name="email" value="<?php echo $email; ?>"  id="input-email" class="form-control"  />
							<?php if ($error_email) { ?>
			                	<div class="text-danger"><?php echo $error_email; ?></div>
			                <?php } ?>
						</div>
		  			</div>

		   			<div style = "display:none" class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-location"><?php echo 'Location' ?></label>
						<div class="col-sm-3">
							<select style="font-size: 14px;" class="form-control" name="location" id="location">
					    		<?php foreach($location_datas as $key => $value) { ?>
						    		<?php if($key == $location ) { ?>
						    			<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
						    		<?php } else { ?>
						    			<option value="<?php echo $key ?>"><?php echo $value?></option>
						    		<?php } ?>
					    		<?php } ?>
						    </select>
						</div>
		   			</div>
		   			<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-dob"><?php echo 'Date Of Birth' ?></label>
					</div>
		  			<div class="form-group">
						<div class="col-sm-4 col-xs-4" >
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-date_dob"><?php echo 'Date' ?></label>
							<select name="date_dob" id="date_dob" class="form-control">
		                  		<?php for ($date=1; $date<=31; $date++) { ?>
		                  			<?php if($date == $date_dob){ ?>
		                  				<option selected="selected" value="<?php echo $date_dob ?>"><?php echo $date ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $date ?>"><?php echo $date ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
						<div class="col-sm-4 col-xs-4">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-month_dob"><?php echo 'Month' ?></label>
							<select name="month_dob" id="month_dob" class="form-control">
		                  		<?php for ($month=1; $month<=12; $month++) { ?>
		                  			<?php if($month == $month_dob){ ?>
		                  				<option selected="selected" value="<?php echo $month_dob ?>"><?php echo $month ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $month ?>"><?php echo $month ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
						<div class="col-sm-4 col-xs-4">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-year_dob"><?php echo 'Year' ?></label>
							<select name="year_dob" id="year_dob" class="form-control">
		                  		<?php for ($year=1950; $year<=2020; $year++) { ?>
		                  			<?php if($year == $year_dob){ ?>
		                  				<option selected="selected" value="<?php echo $year_dob ?>"><?php echo $year ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $year ?>"><?php echo $year ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
		   			</div>
		   			<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-date_doa"><?php echo 'Date Of Anniversary' ?></label>
					</div>
		   			<div class="form-group">
						
						<div class="col-sm-4 col-xs-4">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-date_doa"><?php echo 'Date' ?></label>
							<select name="date_doa" id="date_doa" class="form-control">
		                  		<?php for ($date=1; $date<=31; $date++) { ?>
		                  			<?php if($date == $date_doa){ ?>
		                  				<option selected="selected" value="<?php echo $date_doa ?>"><?php echo $date ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $date ?>"><?php echo $date ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
						<div class="col-sm-4 col-xs-4">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-month_doa"><?php echo 'Month' ?></label>
							<select name="month_doa" id="month_doa" class="form-control">
		                  		<?php for ($month=1; $month<=12; $month++) { ?>
		                  			<?php if($month == $month_doa){ ?>
		                  				<option selected="selected" value="<?php echo $month_doa ?>"><?php echo $month ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $month ?>"><?php echo $month ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
						<div class="col-sm-4 col-xs-4">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-year_doa"><?php echo 'Year' ?></label>
							<select name="year_doa" id="year_doa" class="form-control">
		                  		<?php for ($year=1950; $year<=2020; $year++) { ?>
		                  			<?php if($year == $year_doa){ ?>
		                  				<option selected="selected" value="<?php echo $year_doa ?>"><?php echo $year ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $year ?>"><?php echo $year ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>

						</div>
		   			</div>
		   			<?php $i=0; ?>
		   			<?php foreach ($question_datas as $qkey => $qvalue) { ?>
		   				<?php ++$i; ?>
		   				<?php // echo "<pre>";print_r($ans_datas);exit; ?>
			   			<div  class="form-group <?php echo ($i%2) ? 'even' : 'odd' ?> ">
			                <label style="font-size: 22px;" class="col-sm-6 control-label" for="input-food_have"><?php echo $qvalue['question'] ?></label>
			                <div class="col-sm-6">
			                	<?php $array = explode(',', $qvalue['q_ans']); 
			                	$question_id = $qvalue['id'];
								if($ans_datas){ ?>
									<?php $counter = 0; ?>	
									<?php foreach ($array as $akey =>$avalue) { ?>
										<?php if(isset($ans_datas[$question_id]['values']) && $avalue == $ans_datas[$question_id]['values']){ ?>
			                    			<input checked="checked" style="font-size: 20px;" type="radio" name="ans_datas[<?php echo $question_id ?>][values]"  value="<?php echo $avalue ?>" checked="checked">&nbsp;&nbsp;<span style="font-size: 20px;"><?php echo $avalue ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<br>
			            				<?php } else { ?>
			                    			<input style="font-size: 20px;" type="radio" name="ans_datas[<?php echo $question_id ?>][values]"  value="<?php echo $avalue ?>">&nbsp;&nbsp;<span style="font-size: 20px;"><?php echo $avalue ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<br>
			            				<?php } ?>
			            				<?php $counter ++; ?>
			            			<?php } ?>
	            				<?php } else { ?>
	            					<?php $counter = 0; ?>
	            					<?php foreach ($array as $akey => $item) { ?>
	                    				<?php if($counter == 0){ ?>
	                    					<input checked="checked" style="font-size: 20px;" type="radio" name="ans_datas[<?php echo $question_id ?>][values]"  value="<?php echo $item ?>" >&nbsp;&nbsp;<span style="font-size: 20px;"><?php echo $item ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<br>
	                    				<?php } else { ?>
	                    					<input style="font-size: 20px;" type="radio" name="ans_datas[<?php echo $question_id ?>][values]"  value="<?php echo $item ?>" >&nbsp;&nbsp;<span style="font-size: 20px;"><?php echo $item ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<br>
	                    				<?php } ?>
	            						<?php $counter ++; ?>
	            					<?php } ?>
	            				<?php } ?>
			            	</div>
	           	 		</div>
	           	 	<?php } ?>
	           	 	<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-suggestion"><?php echo 'Suggestion' ?></label>
						<div class="col-sm-3">
			  				<textarea type="text" name="suggestion"   id="input-suggestion" class="form-control"/></textarea>
						</div>
		  			</div>
		  			<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-remark"><?php echo 'Remark' ?></label>
						<div class="col-sm-3">
			  				<textarea type="text" name="remark"  value="<?php echo $remark; ?>"  id="input-remark" class="form-control"/></textarea>
						</div>
		  			</div>
		  			<div class="form-group">
	  					<div class="col-sm-2">
							<button style="margin-top: 0px;margin-bottom: 2px;margin-left: 120px;font-size: 20px;width: 50%; " type="submit" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary col-sm-5"><i >Save</i></button>
						</div>
					</div>
				</form>
		  	</div>
	  	</div>
		
	</div>
</div>
<style type="text/css">
.even{
 background-color:#cccccc;
 
}
.odd{
 background-color:white;
}
</style>
</div>
<script type="text/javascript">
	$(".form_datetime").datepicker({
		dateFormat: 'dd-mm-yy',			
  	});

// Contact search

$('input[name=\'contact_no\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/feedback/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&contact=1',
      dataType: 'json',
      success: function(json) { 
        response($.map(json, function(item) {
        	console.log(item);
          return {
          	label: item.contact_no,
            value: item.table_no,
            contact_no: item.contact_no,
            name: item.name,
            location: item.location,
            email: item.email,
            date_dob: item.date_dob,
            date_doa: item.date_doa,
            month_dob: item.month_dob,
            month_doa: item.month_doa,
            year_dob: item.year_dob,
            year_doa: item.year_doa,
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'name\']').val(ui.item.name);
    $('input[name=\'table_no\']').val(ui.item.table_no);
    $('input[name=\'contact_no\']').val(ui.item.contact_no);
    $('input[select=\'location\']').val(ui.item.location);
    $('input[name=\'dob\']').val(ui.item.dob);
    $('input[name=\'doa\']').val(ui.item.doa);
    $('input[name=\'email\']').val(ui.item.email);
    loc = ui.item.location;
    date_dob = ui.item.date_dob;
    date_doa = ui.item.date_doa;
    month_dob = ui.item.month_dob;
    month_doa = ui.item.month_doa;
    year_dob = ui.item.year_dob;
    year_doa = ui.item.year_doa;
   // alert(year_doa);

    //$("#location option[selected]").removeAttr("selected"); 
    $("#location option[value="+loc+"]").attr("selected","selected");

    $("#date_doa option[value="+date_doa+"]").attr("selected","selected"); 
    $("#date_dob option[value="+date_dob+"]").attr("selected","selected"); 
    $("#month_dob option[value="+month_dob+"]").attr("selected","selected"); 
    $("#month_doa option[value="+month_doa+"]").attr("selected","selected"); 
    $("#year_dob option[value="+year_dob+"]").attr("selected","selected");     
    $("#year_doa option[value="+year_doa+"]").attr("selected","selected");     


    // $('select[name="location"] option:selected').attr("selected",null);
    // $('select[name="location"] option[value="ui.item.location"]').attr("selected","selected");

    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});


function custdata(){
	//alert('in');
	var table_no = $('input[name=\'table_no\']').val();
   	//alert(table_no);
	$.ajax({
      url: 'index.php?route=catalog/feedback/autocomplete&token=<?php echo $token; ?>&filter_name=' + table_no,
      dataType: 'json',
      success: function(json) {   
        console.log(json);
        		$('#input-email').val(json[0].email);
        		$('#input-name').val(json[0].name);
        		$('#input-contact_no').val(json[0].contact_no);

        		$('#date_dob').val(json[0].date_dob);
        		$('#month_dob').val(json[0].month_dob);
        		$('#year_dob').val(json[0].year_dob);

        		$('#date_doa').val(json[0].date_doa);
        		$('#month_doa').val(json[0].month_doa);
        		$('#year_doa').val(json[0].year_doa);

            // value: item.table_no,
            // contact_no: item.contact_no,
            // name: item.name,
            // location: item.location,
            // email: item.email,
            // date_dob: item.date_dob,
            // date_doa: item.date_doa,
            // month_dob: item.month_dob,
            // month_doa: item.month_doa,
            // year_dob: item.year_dob,
            // year_doa: item.year_doa,
      }
     });

}




// $('#button-filter').on('click', function() {
//   var url = 'index.php?route=catalog/expensetrans&token=<?php echo $token; ?>';

//   var filter_account_name = $('input[name=\'filter_account_name\']').val();
//   if (filter_account_name) {
// 	var filter_id = $('input[name=\'filter_id\']').val();
// 	if (filter_id) {
// 	  url += '&filter_id=' + encodeURIComponent(filter_id);
// 	   }
// 	  url += '&filter_account_name=' + encodeURIComponent(filter_account_name);
//   }

  

//   location = url;

// });


</script>
<?php echo $footer; ?>