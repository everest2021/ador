<?php echo $header; ?>
<div id="content" class="sucess">
	<div class="page-header">
		<div id="overlay">
			<div class="cv-spinner">
				<span class="spinner"></span>
			</div>
		</div>
		<form action="<?php echo $action; ?>" id="payform" method="post">
			<div class="container-fluid">
				<table class="table" style="background-color: #fff;width: 96%;margin-left: 2%;margin-top: 3%;font-size: 149%;cursor: default;">
				
				<div class="col-sm-12" id="storesss" style="margin-top: 3%; ">
					<div class="col-sm-4">
			      		<label for="stores" style="color: #000;font-size: 25px;">Stores</label>
			      	</div>
					<div class="col-sm-3" >
			    		<?php foreach($stores as $key => $value) { ?>
		    				<?php  if (in_array($value, $stores_post)) { ?>
		    					<input type="checkbox" value="<?php echo $key ?>" name="store[]" checked="checked"><span style="padding: 15px;font-size: 20px;"><?php echo ucwords($value)?><span></span><br>
		    				<?php } else {?>
		    					<input type="checkbox" value="<?php echo $key ?>" name="store[]" ><span style="padding: 15px;font-size: 20px;"><?php echo $value?><span></span><br>
		    				<?php } ?>
			    		<?php } ?>
			      	</div>
			  	</div>


			  	<label style="padding-left: 20px;color: #000;font-size: 25px;padding-left: 24px;"> Store ON/OFF</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<label class="switch" style="padding-left: 34px;padding: 12px;">
						<?php  if ($store_status == 'enable') { ?>
					  		<input type="checkbox" value="enable" checked="checked" name="store_status" id="store_status">
					  	<?php } else {?>
					  		<input type="checkbox" value="enable"   name="store_status" id="store_status">
					  	<?php } ?>



					  <span class="slider" style="border-radius: 40px;"></span>
					</label>

			  	<!-- <div class="col-sm-12" id="reason_ss" style="margin-top: 3%; ">
					<div class="col-sm-4">
			      		<label for="reason" style="color: #000;font-size: 25px;"> Reason:</label>
			      	</div>
					<div class="col-sm-5" >
			      		<input type="text" name="reason" id="reason" value=""  class="form-control">
			      	</div>
			  	</div> -->

			  	<div class="col-sm-offset-0" id="ok_btns" >
				  	<div class="col-sm-3" style="margin-top: 3%;">
						<a  onclick="ok_btn()" id="ok-input"  class="btn btn-block" style="background: green;color: #fff;font-size: 25px;">OK</a>
					</div>
				</div>
				<br />
				
			</div>
		</form>
	</div>
<style type="text/css">
	.myTitleClass .ui-dialog-titlebar {
          background: #e72b0b;
          color: #fff;
    }

    .ui-button.cancelButton {
	    border: 1px solid #aaaaaa
	    ;
	    color: #FF0000
	    ;
	}

	#overlay{	
		position: fixed;
		top: 0;
		z-index: 100;
		width: 100%;
		height:100%;
		display: none;
		background: rgba(0,0,0,0.6);
	}

	.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 40px;
	height: 40px;
	border: 4px #09287d solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
	0% { 
		transform: rotate(0deg); 
	}
	100% { 
		transform: rotate(359deg); 
	}
}
.is-hide{
	display:none;
}

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}


.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
  border-radius: 40px;
}

input:checked + .slider {
  background-color: #00891d;
}

input:focus + .slider {
  box-shadow: 0 0 1px #00891d;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

</style>	
	<script type="text/javascript">
	$(document).on('keydown', '#cash', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) { 
			save();
		  }
		//console.log(code);
		//alert(code);
	});


	function ok_btn(){
		//reason = $('#reason').val();
		//stores = $('#stores').val();
		// $(document).ajaxSend(function() {
		// 	$("#overlay").fadeIn(200);　
		// });

		var data = $('form').serialize();

		//if(reason != ''){
			$.ajax({
				type: "POST",
			  	url: 'index.php?route=catalog/store_onoff/store_status&token=<?php echo $token; ?>',
			  	data: data,
			  	dataType: 'json',
			  	beforeSend: function() {
		  			$("#overlay").fadeIn(200);　
				},
			  	complete: function() {
		  			$("#overlay").fadeOut(50);
				}, 
			  	success: function(json) {
			  		//alert('sucess');
			  		//console.log(json);
			  		if(json.success){
						//$('.sucess').html('');
						//$('.sucess').append(json.success);
						alert(json.success);
							$('.sucess').html('');
					$('.sucess').append(json.done);
						//location.reload();
					} else if(json.errors) {
						alert(json.errors);
					} else {
						alert("somthing is wrong");
					}
			  	},error: function(xhr, ajaxOptions, thrownError) {
			  		
		  			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		  			window.location.reload(); 
				}
			});
		// }else {
		// 	alert("Please Enter Reason");
		// }
		
		
		
	}

	function closeIFrame(){
      	location.replace('index.php?route=catalog/urbanpiperitem_show&token=<?php echo $token; ?>');
      	//window.location.reload(); 	
	}

	

	
	</script>
</div>
	