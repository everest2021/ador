<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo 'Live Store'; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <style type="text/css">
    .myclasscolor{
        color: #3391ff;
    }
</style>
  <div class="container-fluid">
   
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    <?php } ?>
    <?php if (isset($success) && $success != '') { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo 'Live Store'; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-data" data-toggle="tab"><?php echo $tab_general; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-data">

                <div class="form-group ">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_store_ref_id_lbl; ?>:</label>
                    <div class="col-sm-10">
                    	<label class="control-label myclasscolor" for="input-model"><?php echo $entry_store_ref_id; ?></label>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_store_nme_lbl; ?>:</label>
                    <div class="col-sm-10">
                      <label class="control-label myclasscolor" for="input-model"><?php echo $entry_store_nme; ?></label>
                    </div>
                </div>

                <div class="form-group ">
                    <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_store_add_lbl; ?>:</label>
                    <div class="col-sm-10">
                    	<label class="control-label myclasscolor" for="input-model"><?php echo $entry_store_add; ?></label>
                    </div>
                </div>

                <div class="form-group ">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_store_city_lbl; ?>:</label>
                    <div class="col-sm-10">
                      <label class="control-label myclasscolor" for="input-model"><?php echo $entry_store_city; ?></label>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_store_zipcode_lbl; ?>:</label>
                    <div class="col-sm-10">
                    	<label class="control-label myclasscolor" for="input-model"><?php echo $entry_store_zipcode; ?></label>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_store_contact_phone_lbl; ?>:</label>
                    <div class="col-sm-10">
                    	<label class="control-label myclasscolor" for="input-model"><?php echo $entry_store_contact_phone; ?></label>
                    </div>
                </div>

                <div class="form-group ">
                    <label class="col-sm-2 control-label" ><?php echo $entry_store_notification_no_lbl; ?>:</label>
                    <div class="col-sm-10">
                    	<label class="control-label myclasscolor" for="input-model"><?php echo $entry_store_notification_no; ?></label>
                    </div>
                </div>

                <div class="form-group ">
                    <label class="col-sm-2 control-label" ><?php echo $entry_store_notification_email_lbl; ?>:</label>
                    <div class="col-sm-10">
                    	<label class="control-label myclasscolor" for="input-model"><?php echo $notification_emails; ?></label>
                      	
                    </div>
                </div>

              

                <div class="form-group ">
                    <label class="col-sm-2 control-label" ><?php echo $entry_minimum_pick_time_lbl; ?>:</label>
                    <div class="col-sm-10">
                      <label class="control-label myclasscolor" for="input-model"><?php echo $entry_minimum_pick_time; ?></label>
                    </div>
                </div>

                <div class="form-group ">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_minimum_delivery_time_lbl; ?>:</label>
                    <div class="col-sm-10">
                    	<label class="control-label myclasscolor" for="input-model"><?php echo $entry_minimum_delivery_time; ?></label>
                    </div>
                </div>

                <div class="form-group ">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_minimum_order_value_lbl; ?>:</label>
                    <div class="col-sm-10">
                    	<label class="control-label myclasscolor" for="input-model"><?php echo $entry_minimum_order_value; ?></label>
                    </div>
                </div>


                <div class="form-group ">
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_geo_longitude_lbl; ?>:</label>
                        <div class="col-sm-10">
                        	<label class="control-label myclasscolor" for="input-model"><?php echo $entry_geo_longitude; ?></label>
                        </div>
                    </div>
                    <div class="col-sm-12" style="margin-top: 3%;">
                        <label class="col-sm-2 control-label" for="input-model" ><?php echo $entry_geo_latitude_lbl; ?>:</label>
                        <div class="col-sm-10">
                        	<label class="control-label myclasscolor" for="input-model"><?php echo $entry_geo_latitude; ?></label>
                        </div>
                    </div>
                </div>

                 <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_ordering_enabled_lbl; ?>:</label>
                    <div class="col-sm-10">
                        <?php if ($entry_ordering_enabled == 'true') { ?>
                            <label class="col-sm-2 control-label myclasscolor" for="input-model"><?php echo 'Active'; ?></label>
                        <?php } elseif($entry_ordering_enabled == 'false') { ?>
                            <label class="col-sm-2 control-label myclasscolor" for="input-model"><?php echo 'IN-Active'; ?></label>
                        <?php } ?>
                    </div>
                </div>




                <div class="form-group">
                    <div class="col-sm-12" style="margin-top: 5%;">
                        <table id="tbcharge" class="table table-striped table-bordered table-hover">
                            <thead>
                            	<tr colspan = "3">
                                  <td class="text-left"><?php echo "Timing"; ?></td>
                                </tr>
                                <tr>
                                  <td class="text-center"><?php echo "Days"; ?></td>
                                  <td class="text-center"><?php echo "Start Time"; ?></td>
                                  <td class="text-center"><?php echo "End Time"; ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($store_all) { ?>

                                    <?php foreach($store_all as $store_timingkey => $store_timing) {  ?>
                                        <tr>
                                            <td class="text-center"><?php echo $store_timing['day']; ?></td>
                                            <td class="text-center"><?php echo $store_timing['slots'][0]['start_time']; ?></td>
                                            <td class="text-center"><?php echo $store_timing['slots'][0]['end_time']; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="4">No Result Found</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                 <div class="form-group">
                    <div class="col-sm-12" style="margin-top: 5%;">
                        <table id="tblplatfrom" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                  <td class="text-center"><?php echo "Platform"; ?></td>
                                  <td class="text-center"><?php echo "Registeration Link"; ?></td>
                                  <td class="text-center"><?php echo "Store Id"; ?></td>
                                  
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($platform_datas) { ?>

                                    <?php foreach($platform_datas as $plkey => $plvalue) {  ?>
                                        

                                        <tr>
                                            <td class="text-center"><?php echo $plvalue['name']; ?></td>
                                            <td class="text-center myclasscolor"><?php echo $plvalue['url']; ?></td>
                                            <td class="text-center"><?php echo $plvalue['platform_store_id']; ?></td>
                                           
                                           
                                           
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="4">No Result Found</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
               
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
  <script type="text/javascript"><!--
// Manufacturer
 $('.time').timepicker({timeFormat: 'hh:mm:ss'});

 $.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

    function addtimes(){
        var day_open_store = $('#day_open_store').val() || '';
        var store_intime = $('#store_intime').val() || '';
        var store_outtime = $('#store_outtime').val() || ''; 
         if(day_open_store == '' || store_intime == '' || store_outtime == ''){
            alert('Please fill all data');
            return false;
        }
        $.ajax({
            type: "POST",
            url: 'index.php?route=catalog/urbanpier_store/gettiming&token=<?php echo $token; ?>',
            dataType: 'json',
            data: {
                day_open_store: day_open_store,
                store_intime: store_intime,
                store_outtime: store_outtime,
            },
            success: function(json) {
                if (json.html != '' ) {
                     $('#tbcharge tbody').html('');
                    $('#tbcharge tbody').append(json.html);
                    $('#day_open_store').val('');
                    $('#store_intime').val('');
                     $('#store_outtime').val('');
                    return false;
                } 
                
            }
        });
    }


   function delete_store_day(id){
        if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
            $.ajax({
                url:'index.php?route=catalog/urbanpier_store/deleterecord&token=<?php echo $token; ?>'+'&id='+id,
                method: "POST",
                dataType: 'json',
                success: function(json)
                {   
                     $('#tbcharge tbody').html('');
                    $('#tbcharge tbody').append(json.html);
                     $('#day_open_store').val('');
                    $('#store_intime').val('');
                     $('#store_outtime').val('');
                    return false;
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });
             return false;
        }
    }

    function addplatform(){
        
        var platform_name = $('#platform_name').val() || '';
        var platform_link = $('#platform_link').val() || '';
        var platform_store_id = $('#platform_store_id').val() || ''; 
       /* console.log(platform_name);
        console.log(platform_link);
        console.log(platform_store_id);*/

         if(platform_name == '' || platform_link == '' || platform_store_id == ''){
            alert('Please fill all data');
            return false;
        }
        $.ajax({
            type: "POST",
            url: 'index.php?route=catalog/urbanpier_store/getplatform&token=<?php echo $token; ?>',
            dataType: 'json',
            data: {
                platform_name: platform_name,
                platform_link: platform_link,
                platform_store_id: platform_store_id,
            },
            success: function(json) {
                if (json.html != '' ) {
                     $('#tblplatfrom tbody').html('');
                    $('#tblplatfrom tbody').append(json.html);
                    $('#platform_name').val('');
                    $('#platform_link').val('');
                    $('#platform_store_id').val('');
                    return false;
                } 
                
            }
        });
    }


   function delete_platform(id){
            if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
                $.ajax({
                    url:'index.php?route=catalog/urbanpier_store/deleteplatform&token=<?php echo $token; ?>'+'&id='+id,
                    method: "POST",
                    dataType: 'json',
                    success: function(json)
                    {   
                         $('#tblplatfrom tbody').html('');
                        $('#tblplatfrom tbody').append(json.html);
                         $('#platform_name').val('');
                        $('#platform_link').val('');
                        $('#platform_store_id').val('');
                        return false;
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error deleting data');
                    }
                });
                 return false;
            }
    }

$('input[name=\'manufacturer\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/manufacturer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				json.unshift({
					manufacturer_id: 0,
					name: '<?php echo $text_none; ?>'
				});

				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['manufacturer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'manufacturer\']').val(item['label']);
		$('input[name=\'manufacturer_id\']').val(item['value']);
	}
});

// Category
$('input[name=\'category\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['category_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'category\']').val('');

		$('#product-category' + item['value']).remove();

		$('#product-category').append('<div id="product-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_category[]" value="' + item['value'] + '" /></div>');
	}
});

$('#product-category').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Filter
$('input[name=\'filter\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['filter_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter\']').val('');

		$('#product-filter' + item['value']).remove();

		$('#product-filter').append('<div id="product-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_filter[]" value="' + item['value'] + '" /></div>');
	}
});

$('#product-filter').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Downloads
$('input[name=\'download\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/download/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['download_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'download\']').val('');

		$('#product-download' + item['value']).remove();

		$('#product-download').append('<div id="product-download' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_download[]" value="' + item['value'] + '" /></div>');
	}
});

$('#product-download').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Related
$('input[name=\'related\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'related\']').val('');

		$('#product-related' + item['value']).remove();

		$('#product-related').append('<div id="product-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_related[]" value="' + item['value'] + '" /></div>');
	}
});

$('#product-related').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
//--></script>






  
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
$('#option a:first').tab('show');
//--></script></div>
<?php echo $footer; ?>
