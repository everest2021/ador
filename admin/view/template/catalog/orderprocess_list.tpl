<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header" style="display: none;">
	    <div class="container-fluid">
	        <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" 
	             class="btn btn-primary"><i class="fa fa-plus"></i></a>
		         <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
	        </div>
	         <h1><?php echo $heading_title; ?></h1>
	        <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
	        </ul>
	    </div>
  	</div>
  	<div class="container-fluid" style="padding-left: 0px !important; padding-right: 0px !important;">
		<?php if ($error_warning) { ?>
	  		<div class="alert alert-danger" style="padding: 0px !important;margin-bottom: 0px !important;"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	      		<button type="button" class="close" data-dismiss="alert">&times;</button>
	  		</div>
	    <?php } ?>
	    <?php if ($success) { ?>
	  		<div class="alert alert-success" style="padding: 0px !important;margin-bottom: 0px !important;"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
	      		<button type="button" class="close" data-dismiss="alert">&times;</button>
	  		</div>
	    <?php } ?>
	  	<div class="panel panel-default" style="background-color: #000;">
		  	<div class="panel-heading" style="display: none;">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
		  	</div>
		  	<div class="panel-body" style="padding: 0px !important;">
				<!-- <form action="<?php //echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport"> -->
				  	<div class="form-group" style="padding-top: 5px !important;">
				        <label class="col-sm-3 control-label" style="font-size: 25px;font-weight: bold;color: #fff;"><?php echo 'Order No'; ?></label>
				        <div class="col-sm-3">
				            <input id="filter_order_no" type="text" name="filter_order_no" value="<?php echo $filter_order_no; ?>" style="font-size: 25px;font-weight: bold;color: #000;" placeholder="<?php echo 'Order No'; ?>" class="form-control inputs" />
				        	<?php if ($error_filter_order_no ) { ?>
								<div class="text-danger" style="font-size: 18px;"><?php echo $error_filter_order_no ; ?></div>
							<?php } ?>
				        </div>
			        </div> 
				  	<div class="table-responsive" style="overflow-x: visible !important;margin-top: 1%;">
						<table class="table table-bordered" style="margin-bottom: 0px !important;">
					  		<thead>
								<tr>
						  			<td style="width: 33%;padding: 5px !important;" class="text-center">
						  				<?php if ($sort == 'in_process') { ?>
											<a style="color: #fff;font-size: 30px;font-weight: bold;" href="<?php echo $sort_in_process; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'IN PROCESS'; ?></a>
										<?php } else { ?>
											<a style="color: #fff;font-size: 30px;font-weight: bold;" href="<?php echo $sort_in_process; ?>"><?php echo 'IN PROCESS'; ?></a>
										<?php } ?>
						  			</td>
						  			<td style="width: 33%;padding: 5px !important;" class="text-center">
										<?php if ($sort == 'ready') { ?>
											<a style="color: #fff;font-size: 30px;font-weight: bold;" href="<?php echo $sort_ready; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'READY'; ?></a>
										<?php } else { ?>
											<a style="color: #fff;font-size: 30px;font-weight: bold;" href="<?php echo $sort_ready; ?>"><?php echo 'READY'; ?></a>
										<?php } ?>
									</td>
						  			<?php /* ?>
						  			<td style="width: 33%;padding: 5px !important;" class="text-center">
										<?php if ($sort == 'taken') { ?>
											<a style="color: #fff;font-size: 30px;font-weight: bold;" href="<?php echo $sort_taken; ?>" class="<?php echo strtolower($order); ?>"><?php echo "TAKEN"; ?></a>
										<?php } else { ?>
											<a style="color: #fff;font-size: 30px;font-weight: bold;" href="<?php echo $sort_taken; ?>"><?php echo "TAKEN"; ?></a>
										<?php } ?>
									</td>
									<?php */ ?>
								</tr>
					  		</thead>
					  		<tbody>
								<tr>
									<td class="text-center" style="border-bottom: 0px !important;padding: 10px;vertical-align: top;text-align: left;">
										<div style="display: inline;float:left;margin-right: 10%;">
											<?php foreach($inprocess_datas as $ikey => $ivalue){ ?>
												<?php
													$in = 0;
													if($ikey % 8 == 0){
														$in = 1;
													}
												?>
												<?php if($in == 1){ ?>
													</div>
													<div style="display: inline;float:left;margin-right: 10%;">
														<a href="<?php echo $ivalue['href'] ?>" id="in-process-href-id_<?php echo $ikey; ?>" style="font-size: 55px;cursor: pointer;color: <?php echo $ivalue['color'] ?>;"><?php echo $ivalue['order_no'] ?></a>
														<br />
												<?php } else { ?>
													<a href="<?php echo $ivalue['href'] ?>" id="in-process-href-id_<?php echo $ikey; ?>" style="font-size: 55px;cursor: pointer;color: <?php echo $ivalue['color'] ?>;"><?php echo $ivalue['order_no'] ?></a><br />
												<?php } ?>
												<input type="hidden" name="inprocess_datas[<?php echo $ikey ?>]['order_date']" value = "<?php echo $ivalue['order_date'] ?>"  id="in-process-order-date_<?php echo $ikey ?>" class="in_process_order_date" />
												<input type="hidden" name="inprocess_datas[<?php echo $ikey ?>]['order_time']" value = "<?php echo $ivalue['order_time'] ?>"  id="in-process-order-time_<?php echo $ikey ?>" class="in_process_order_time" />
												<input type="hidden" name="inprocess_datas[<?php echo $ikey ?>]['order_date_time']" value = "<?php echo $ivalue['order_date_time'] ?>"  id="in-process-order-date-time_<?php echo $ikey ?>" class="in_process_order_date_time" />
											<?php } ?>
										</div>
							  		</td>
							  		<td class="text-center" style="border-bottom: 0px !important;padding: 10px;vertical-align: top;">
										<div style="display: inline;float:left;margin-right: 10%;">
											<?php foreach($ready_datas as $ikey => $ivalue){ ?>
												<?php
													$in = 0;
													if($ikey % 8 == 0){
														$in = 1;
													}
												?>
												<?php if($in == 1){ ?>
													</div>
													<div style="display: inline;float:left;margin-right: 10%;">
														<a href="<?php echo $ivalue['href'] ?>" id="ready-href-id_<?php echo $ikey; ?>" style="font-size: 55px;cursor: pointer;color: <?php echo $ivalue['color'] ?>;"><?php echo $ivalue['order_no'] ?></a><br />
														
												<?php } else { ?>
													<a href="<?php echo $ivalue['href'] ?>" id="ready-href-id_<?php echo $ikey; ?>" style="font-size: 55px;cursor: pointer;color: <?php echo $ivalue['color'] ?>;"><?php echo $ivalue['order_no'] ?></a><br />
												<?php } ?>
												<input type="hidden" name="ready_datas[<?php echo $ikey ?>]['order_date']" value = "<?php echo $ivalue['order_date'] ?>"  id="order-ready-order-date_<?php echo $ikey ?>" class="order_ready_order_date" />
												<input type="hidden" name="ready_datas[<?php echo $ikey ?>]['order_time']" value = "<?php echo $ivalue['order_time'] ?>"  id="order-ready-order-time_<?php echo $ikey ?>" class="order_ready_order_time" />
												<input type="hidden" name="ready_datas[<?php echo $ikey ?>]['order_date_time']" value = "<?php echo $ivalue['order_date_time'] ?>"  id="order-ready-order-date-time_<?php echo $ikey ?>" class="order_ready_order_date_time" />
											<?php } ?>
										</div>
							  		</td>
							  		<?php /* ?>
							  		<td class="text-center" style="border-bottom: 0px !important;padding: 10px;vertical-align: top;">
										<?php foreach($taken_datas as $ikey => $ivalue){ ?>
											<a href="<?php echo $ivalue['href'] ?>" style="font-size: 55px;cursor: pointer;color: orange;"><?php echo $ivalue['order_no'] ?></a><br />
										<?php } ?>
							  		</td>
							  		<?php */ ?>
								</tr>
					  		</tbody>
						</table>
					</div>
				<!-- </form> -->
				<!-- <div class="row">
				    <div class="col-sm-6 text-left"><?php //echo $pagination; ?></div>
				    <div class="col-sm-6 text-right"><?php //echo $results; ?></div>
				</div> -->
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$( document ).ready(function() {
		    $('#filter_order_no').focus();
		});
		function filter_function(){
	  		var url = 'index.php?route=catalog/orderprocess/add&token=<?php echo $token; ?>';
			var filter_order_no = $('input[name=\'filter_order_no\']').val();
		  	if (filter_order_no != '') {
				url += '&filter_order_no=' + encodeURIComponent(filter_order_no);
		  	} else {
		  		alert('Please Enter Order Number');
		  		return false;
		  	}
			location = url;
			return false;
		}
		$(document).on('keydown', '.inputs', function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13){
				filter_function();
			}
		});

		function doSomething() {
    		$(".in_process_order_date_time").each(function() {
			    ids = $(this).attr('id');
			  	s_id = ids.split('_');
				id = s_id[1];

			    order_date_time = new Date($(this).val());
			    current_date_time = new Date();

			    // get total seconds between the times
				var delta = Math.abs(order_date_time - current_date_time) / 1000;

				// calculate (and subtract) whole days
				var days = Math.floor(delta / 86400);
				delta -= days * 86400;

				// calculate (and subtract) whole hours
				var hours = Math.floor(delta / 3600) % 24;
				delta -= hours * 3600;

				// calculate (and subtract) whole minutes
				var minutes = Math.floor(delta / 60) % 60;
				delta -= minutes * 60;

				// what's left is seconds
				var seconds = delta % 60;  // in theory the modulus is not required

				if(days == 0 && hours == 0 && minutes < 5){
					$('#in-process-href-id_'+id).css('color', 'orange');			    
				} else if(days == 0 && hours == 0 && minutes >= 5 && minutes < 10) {
					$('#in-process-href-id_'+id).css('color', 'red');
				} else {
					$('#in-process-href-id_'+id).css('color', 'red');
					$('#in-process-href-id_'+id).addClass('blink_me');
				}
				// console.log($(this).val());
			 	// console.log(days);
			 	// console.log(hours);
			 	// console.log(minutes);
			 	// console.log(seconds);
			});

			$(".order_ready_order_date_time").each(function() {
			    ids = $(this).attr('id');
			  	s_id = ids.split('_');
				id = s_id[1];

			    order_date_time = new Date($(this).val());
			    current_date_time = new Date();

			    // get total seconds between the times
				var delta = Math.abs(order_date_time - current_date_time) / 1000;

				// calculate (and subtract) whole days
				var days = Math.floor(delta / 86400);
				delta -= days * 86400;

				// calculate (and subtract) whole hours
				var hours = Math.floor(delta / 3600) % 24;
				delta -= hours * 3600;

				// calculate (and subtract) whole minutes
				var minutes = Math.floor(delta / 60) % 60;
				delta -= minutes * 60;

				// what's left is seconds
				var seconds = delta % 60;  // in theory the modulus is not required

				//console.log($(this).val());
			    //console.log(days);
			    //console.log(hours);
			    //console.log(minutes);
			    //console.log(seconds);

				if(days == 0 && hours == 0 && minutes < 5){
					$('#ready-href-id_'+id).css('color', 'orange');			    
				} else if(days == 0 && hours == 0 && minutes >= 5 && minutes < 10) {
					$('#ready-href-id_'+id).css('color', 'red');
				} else {
					$('#ready-href-id_'+id).css('color', 'red');
					$('#ready-href-id_'+id).addClass('blink_me');
				}
			});
		}
		setInterval(doSomething, 5000); // Time in milliseconds
	</script>
</div>
<?php echo $footer; ?>