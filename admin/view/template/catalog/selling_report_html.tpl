<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
	<h1 style="text-align:center;font-weight: bold;color: #000;">
    <?php echo "Selling Report"; ?><br />
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
    <?php echo 'From : '. $filter_startdate; ?>
   	<?php echo 'To : '. $filter_enddate; ?><br />
    </span>
    <span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
  	</h1>
		
		  	<table class="product" style="width:100% !important;">
				<thead>
					<tr>
						<td style="text-align: center;"><?php echo 'Item Name'; ?></td>
						<td style="text-align: center;"><?php echo 'Total Quantity'; ?></td>
						<td style="text-align: center;"><?php echo 'Total Amount'; ?></td>		            
					</tr>
				</thead>
				<tbody>
				<?php if($final_datas){ ?>
					<?php foreach($final_datas as $fkeys => $fvalues){ ?>
							<tr>			          
								<td style="text-align: center;" class="left">
									<?php echo $fvalues['item_name']; ?>
								</td>
								<td style="text-align: center;" class="left">
									<?php echo $fvalues['total_qty']; ?>
								</td>
								<td style="text-align: center;" class="left">
									<?php echo $fvalues['total_amount']; ?>
								</td>
							</tr>
					<?php } ?>
				<?php } ?>
				</tbody>
			</table>
		
 	</div>
</body>
</html>
