<?php echo $header; ?>
<div id="content" class="sucess">
  	<div class="page-header">
  		<form action="<?php echo $action; ?>" id="payform" method="post">
			<div class="container-fluid">
				<h1><?php echo 'Tendering Screen' ; ?></h1>
				<center>
					<a id="payform" onclick="save()" title="Update" class="btn btn-primary" style="font-size:20px;">Ok</a>
			  		<!-- <a href="<?php echo $add; ?>" id="payform"  title="Update" class="btn btn-primary" style="font-size:20px;">Ok</a> -->
			  	</center>
			</div>
			<div class="container-fluid">
				<div class="form-group row">
			  	</div>
			  	<div class="form-group row">
			    	<label class="col-sm-1 col-form-label" style="font-size:20px;margin-top: 5px;text-align: center;width:100%">Total Pay : <?php echo round($billdata['grand_total']) ?></label>
			    	<input type="hidden" name="cash" value="<?php echo round($billdata['grand_total']) ?>" id="cashval">
			    	<input type="hidden" id="grand_t" value="<?php echo round($billdata['grand_total']) ?>">
					<input type="hidden" name="orderid" id="orderid">
			  	</div>
			  	<div class="form-group row">
			    	<label class="col-sm-2 col-form-label" style="font-size:20px;margin-top: 5px;text-align: center;width: 50%">Total Received</label>
			    	<input autocomplete="off" type="text" class="col-sm-2 form-control amt" value="" id="t_receved" style="font-size:20px;font-weight: bold ;display: inline;width: 10%">
			  	</div>
			  	<div class="form-group row">
			    	<label class="col-sm-2 col-form-label" style="font-size:20px;margin-top: 5px;text-align: center;width: 50%">Balance</label>
			    	<input autocomplete="off" type="text" class="col-sm-2 form-control bal" value="" id="balance" style="font-size:20px;font-weight: bold ;display: inline;width: 10%">
			  	</div>
			</div>
		</form>
  	</div>
  	<script type="text/javascript">
		$(document).on('keyup','#t_receved', function(e){
			cash_recv = parseInt($('#t_receved').val());
			//alert(cashval);
			totalamt = parseInt($('#grand_t').val());
			if(cash_recv > totalamt){
				remamountcash = cash_recv - totalamt;
				$('#balance').val(remamountcash);
				if( $('#t_receved').val() == ''){
					$('#balance').val(0);
				}
			} else {
				$('#balance').val(0);
			}
		});

function save(){
  	action = '<?php echo $action; ?>';
  	action = action.replace("&amp;", "&");
  	$.post(action, $('#payform').serialize()).done(function(data) {
    	var parsed = JSON.parse(data);
     	if(parsed.info == 1){
     		$('.sucess').html('');
     		$('.sucess').append(parsed.done);
     	} else {
     		alert("Nothing is changed");
     	}
	});
}
  	</script>
</div>
  	