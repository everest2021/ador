<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-5">
              <input type="text" name="tem_name"  value="<?php echo $tem_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label  class="col-sm-2 control-label" for="input-mobile"><?php echo 'Template Description'; ?><br /><span class=""><br/>#customer_name : a placeholder for Customername<br/><br/>#hotelname : a placeholder for Hotelname<br/></span></label>

            <div class="col-sm-5">
              <textarea cols="40" rows="13"   name="tem_description" id="description" class="form-control"><?php echo $tem_description; ?></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo 'Status'; ?></label>
            <div class="col-sm-3">
              <select   name="status" id='input-status' class="form-control">
                <?php foreach($statuss as $skey => $svalue) { ?>
                  <?php if($skey == $status) { ?>
                    <option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
                  <?php } else { ?>
                    <option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name"><?php echo 'Charecter Count'; ?></label>
            <div class="col-sm-3">
                <input type="text"  readonly   value="" placeholder="count" id="input-charecter" class="form-control" />
            </div>
            <label class="col-sm-1 control-label" for="input-name"><?php echo 'SMS credit'?></label>
            <div class="col-sm-3">
                <input type="text"  readonly value="" placeholder="<?php echo $entry_name; ?>" id="credit" class="form-control" />
            </div>
          </div>
        </form>

      </div>
    </div>
     <div class="pull-right">
        <button type="submit" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
  </div>
</div>
<script type='text/javascript'>
    $(document).ready(function () {
        $('#description').keyup(function () {
          var lengthCount = this.value.length;
          var credit_q = lengthCount/160;

          if (credit_q > 0 && credit_q < 1 ) {
              credit = 1;
          }else if (credit_q > 1 && credit_q < 2) {
            credit = 2;
          }else if (credit_q > 2 && credit_q < 3) {
            credit = 3;
          }
          //alert(credit);
          $('#credit').val(credit);

          $('#input-charecter').val(lengthCount);
          
        });
    });
    // $(document).ready(function () {
    //   $("#credit").keyup(function(){
    //     val  = $('#credit').val();
    //     alert(val);
    //     alert("The text has been changed.");
    //   });
    // });
</script>
<script type="text/javascript">
     
     

    // $('#description').keyup(function(){ 
    //  char = $('#description').length;
    //  //alert(char);
    //   $('#input-charecter').val(char);
    // });  

   //char = $('#input-tem_description').val().length();

/*
$('input[name=\'name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/color/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['color_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
  }
});
*/
</script>
<?php echo $footer; ?>