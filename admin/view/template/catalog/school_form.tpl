<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-school" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-school" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-udias_number"><?php echo 'UDIAS क्रमांक / UDIAS Number'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="udias_number" value="<?php echo $udias_number; ?>" placeholder="<?php echo 'UDIAS Number'; ?>" id="input-udias_number" class="form-control" />
              <input type="hidden" name="school_id" value="<?php echo $school_id; ?>" id="input-school_id" class="form-control" />
              <input type="hidden" name="is_set" value="<?php echo $is_set; ?>" id="input-is_set" class="form-control" />
              <?php if ($error_udias_number) { ?>
              <div class="text-danger"><?php echo $error_udias_number; ?></div>
              <?php } ?>
              <div class="" id="error_udias_number" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-registration_name"><?php echo 'UDIAS नंबर नोंदणी प्रमाणपत्र अपलोड करा / Upload UDIAS Number Registration Certificate'; ?></label>
            <div class="col-sm-10">
              <input readonly="readonly" type="text" name="registration_name" value="<?php echo $registration_name; ?>" placeholder="<?php echo 'Registration Certificate'; ?>" id="input-photo" class="form-control" />
              <input type="hidden" name="registration_source" value="<?php echo $registration_source; ?>" id="input-registration_source" class="form-control" />
              <span class="input-group-btn registraton_source_span">
                <button type="button" id="button-registration_source" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>
              </span>
              <?php if($registration_source != ''){ ?>
                <a target="_blank" style="cursor: pointer;" id="registration_source" class="" href="<?php echo $registration_source; ?>">View Document</a>
              <?php } ?>
              <?php if ($error_registration_source) { ?>
              <div class="text-danger"><?php echo $error_registration_source; ?></div>
              <?php } ?>
              <div class="" id="error_registration_source" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-principal_name"><?php echo 'प्राचार्य नाव / Principal Name'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="principal_name" value="<?php echo $principal_name; ?>" placeholder="<?php echo 'Principal Name'; ?>" id="input-principal_name" class="form-control" />
              <?php if ($error_principal_name) { ?>
              <div class="text-danger"><?php echo $error_principal_name; ?></div>
              <?php } ?>
              <div class="" id="error_principal_name" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-principal_email"><?php echo 'प्राचार्य ईमेल / Principal Email'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="principal_email" value="<?php echo $principal_email; ?>" placeholder="<?php echo 'Principal Email'; ?>" id="input-principal_email" class="form-control" />
              <?php if ($error_principal_email) { ?>
              <div class="text-danger"><?php echo $error_principal_email; ?></div>
              <?php } ?>
              <div class="" id="error_principal_email" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-principal_mobile"><?php echo 'प्राचार्य मोबाईल / Principal Mobile'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="principal_mobile" value="<?php echo $principal_mobile; ?>" placeholder="<?php echo 'Principal Mobile'; ?>" id="input-principal_mobile" class="form-control" />
              <?php if ($error_principal_mobile) { ?>
              <div class="text-danger"><?php echo $error_principal_mobile; ?></div>
              <?php } ?>
              <div class="" id="error_principal_mobile" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-principal_address"><?php echo 'प्राचार्य पत्ता / Principal Address'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="principal_address" value="<?php echo $principal_address; ?>" placeholder="<?php echo 'Principal Address'; ?>" id="input-principal_name" class="form-control" />
              <?php if ($error_principal_address) { ?>
              <div class="text-danger"><?php echo $error_principal_address; ?></div>
              <?php } ?>
              <div class="" id="error_principal_address" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-participant_type"><?php echo 'जिल्हा / District'; ?></label>
            <div class="col-sm-10">
              <select name="district" id="input-district" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($districts as $skey => $svalue) { ?>
                  <?php if ($skey == $district) { ?>
                    <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                  <?php } else { ?>
                    <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
              <?php if ($error_district) { ?>
              <div class="text-danger"><?php echo $error_district; ?></div>
              <?php } ?>
              <div class="" id="error_district" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-taluka"><?php echo 'तालुका / Taluka'; ?></label>
            <div class="col-sm-10">
              <select name="taluka" id="input-taluka" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($talukas as $skey => $svalue) { ?>
                  <?php if ($skey == $taluka) { ?>
                    <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                  <?php } else { ?>
                    <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
              <?php if ($error_taluka) { ?>
              <div class="text-danger"><?php echo $error_taluka; ?></div>
              <?php } ?>
              <div class="" id="error_taluka" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-class_from"><?php echo 'पासून वर्ग / Class From'; ?></label>
            <div class="col-sm-10">
              <select name="class_from" id="input-class_from" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($standards as $skey => $svalue) { ?>
                  <?php if ($skey == $class_from) { ?>
                    <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                  <?php } else { ?>
                    <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
              <?php if ($error_class_from) { ?>
              <div class="text-danger"><?php echo $error_class_from; ?></div>
              <?php } ?>
              <div class="" id="error_class_from" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-class_to"><?php echo 'वर्ग ते / Class To'; ?></label>
            <div class="col-sm-10">
              <select name="class_to" id="input-class_to" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($standards as $skey => $svalue) { ?>
                  <?php if ($skey == $class_to) { ?>
                    <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                  <?php } else { ?>
                    <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
              <?php if ($error_class_to) { ?>
              <div class="text-danger"><?php echo $error_class_to; ?></div>
              <?php } ?>
              <div class="" id="error_class_to" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo 'शाळा/कॉलेज नाव / School/College Name'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo 'School / College Name'; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
              <div class="" id="error_name" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-education_trust_name"><?php echo 'शिक्षण संस्थेचे नाव / Education Trust Name'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="education_trust_name" value="<?php echo $education_trust_name; ?>" placeholder="<?php echo 'Education Trsut Name'; ?>" id="input-education_trust_name" class="form-control" />
              <?php if ($error_education_trust_name) { ?>
              <div class="text-danger"><?php echo $error_education_trust_name; ?></div>
              <?php } ?>
              <div class="" id="error_education_trust_name" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-school_email"><?php echo 'शाळा/कॉलेज ईमेल / School/College Email'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="school_email" value="<?php echo $school_email; ?>" placeholder="<?php echo 'School Email'; ?>" id="input-school_email" class="form-control" />
              <?php if ($error_school_email) { ?>
              <div class="text-danger"><?php echo $error_school_email; ?></div>
              <?php } ?>
              <div class="" id="error_school_email" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required individual_class">
            <label class="col-sm-2 control-label" for="input-school_mobile"><?php echo 'शाळा/कॉलेज मोबाईल / School/College Mobile'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="school_mobile" value="<?php echo $school_mobile; ?>" placeholder="<?php echo 'School Mobile'; ?>" id="input-school_mobile" class="form-control" />
              <?php if ($error_school_mobile) { ?>
              <div class="text-danger"><?php echo $error_school_mobile; ?></div>
              <?php } ?>
              <div class="" id="error_school_mobile" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-school_landline"><?php echo 'शाळा/कॉलेज दूरध्वनी क्रमांक / School/College Landline'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="school_landline" value="<?php echo $school_landline; ?>" placeholder="<?php echo 'School Landline'; ?>" id="input-school_landline" class="form-control" />
              <?php if ($error_school_landline) { ?>
              <div class="text-danger"><?php echo $error_school_landline; ?></div>
              <?php } ?>
              <div class="" id="error_school_landline" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-school_address"><?php echo 'शाळा/कॉलेज / School/College Address'; ?></label>
            <div class="col-sm-10">
              <input type="tel" name="school_address" value="<?php echo $school_address; ?>" placeholder="<?php echo 'School Address'; ?>" id="input-school_address" class="form-control" />
              <?php if ($error_school_address) { ?>
              <div class="text-danger"><?php echo $error_school_address; ?></div>
              <?php } ?>
              <div class="" id="error_school_address" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-school_pincode"><?php echo 'शाळा/कॉलेज पत्ता / School/College Pincode'; ?></label>
            <div class="col-sm-10">
              <input type="tel" name="school_pincode" value="<?php echo $school_pincode; ?>" placeholder="<?php echo 'School Pincode'; ?>" id="input-school_pincode" class="form-control" />
              <?php if ($error_school_pincode) { ?>
              <div class="text-danger"><?php echo $error_school_pincode; ?></div>
              <?php } ?>
              <div class="" id="error_school_pincode" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-pt_teacher_name"><?php echo 'क्रीडाशिक्षक नाव / PT Teacher Name'; ?></label>
            <div class="col-sm-10">
              <input type="tel" name="pt_teacher_name" value="<?php echo $pt_teacher_name; ?>" placeholder="<?php echo 'PT Teacher Name'; ?>" id="input-pt_teacher_name" class="form-control" />
              <?php if ($error_pt_teacher_name) { ?>
              <div class="text-danger"><?php echo $error_pt_teacher_name; ?></div>
              <?php } ?>
              <div class="" id="error_pt_teacher_name" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-pt_teacher_email"><?php echo 'क्रीडाशिक्षक ईमेल / PT Teacher Email'; ?></label>
            <div class="col-sm-10">
              <input type="tel" name="pt_teacher_email" value="<?php echo $pt_teacher_email; ?>" placeholder="<?php echo 'PT Teacher Email'; ?>" id="input-pt_teacher_email" class="form-control" />
              <?php if ($error_pt_teacher_email) { ?>
              <div class="text-danger"><?php echo $error_pt_teacher_email; ?></div>
              <?php } ?>
              <div class="" id="error_pt_teacher_email" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-pt_teacher_contact"><?php echo 'क्रीडाशिक्षक संपर्क साधा / PT Teacher Contact'; ?></label>
            <div class="col-sm-10">
              <input type="tel" name="pt_teacher_contact" value="<?php echo $pt_teacher_contact; ?>" placeholder="<?php echo 'PT Teacher Contact'; ?>" id="input-pt_teacher_contact" class="form-control" />
              <?php if ($error_pt_teacher_contact) { ?>
              <div class="text-danger"><?php echo $error_pt_teacher_contact; ?></div>
              <?php } ?>
              <div class="" id="error_pt_teacher_contact" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-password"><?php echo 'Password / Password'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="password" value="<?php echo $password; ?>" placeholder="<?php echo 'Password'; ?>" id="input-password" class="form-control" />
              <?php if ($error_password) { ?>
              <div class="text-danger"><?php echo $error_password; ?></div>
              <?php } ?>
              <div class="" id="error_password" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo 'स्थिती / Status'; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status == '1') { ?>
                <option value="1" selected="selected"><?php echo 'Active'; ?></option>
                <option value="2"><?php echo 'Inactive'; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo 'Active'; ?></option>
                <option value="2" selected="selected"><?php echo 'Inactive'; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--

$('#button-registration_source').on('click', function() {
  $('#form-registration_source').remove();
  $('body').prepend('<form enctype="multipart/form-data" id="form-registration_source" style="display: none;"><input type="file" name="file" /></form>');
  $('#form-registration_source input[name=\'file\']').trigger('click');
  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }
  timer = setInterval(function() {
    if ($('#form-registration_source input[name=\'file\']').val() != '') {
      clearInterval(timer); 
      image_name = 'Registration-Certificate';  
      $.ajax({
        url: 'index.php?route=catalog/school/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
        type: 'post',   
        dataType: 'json',
        data: new FormData($('#form-registration_source')[0]),
        cache: false,
        contentType: false,
        processData: false,   
        beforeSend: function() {
          $('#button-upload').button('loading');
        },
        complete: function() {
          $('#button-upload').button('reset');
        },  
        success: function(json) {
          if (json['error']) {
            alert(json['error']);
          }
          if (json['success']) {
            alert(json['success']);
            console.log(json);
            $('input[name=\'registration_name\']').attr('value', json['filename']);
            $('input[name=\'registration_source\']').attr('value', json['link_href']);
            d = new Date();
            var previewHtml = '<a target="_blank" style="cursor: pointer;" id="registration_source" href="'+json['link_href']+'">View Document</a>';
            $('#registration_source').remove();
            $('.registraton_source_span').after(previewHtml);
          }
        },      
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});

$( document ).ready(function() {
  
});

$('.date').datetimepicker({
  pickTime: false,
  format: 'DD-MM-YYYY'
});
//--></script>
<?php echo $footer; ?>