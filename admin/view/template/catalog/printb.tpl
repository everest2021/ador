
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>" />
    <link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
    <link href="view/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />
    <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
    <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
    <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
    <script src="view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
    <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
    <link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />
    <script src="view/javascript/common.js" type="text/javascript"></script>
  </head>
  <body>
    <div  align="center" class="container" style="width:50%;">
      <table align="center" class="table table_custom no_border" style="width:80%;margin-bottom: 2px;">
        <tr>
          <th style="text-align:center;border-top: 0px;padding: 1px;font-size: 12px;"><b>YAARI</b></th>
        </tr>
        <tr>
          <th style="text-align:center;font-size: 10px;border-top: 0px;padding: 1px;font-size: 8px;"><b>Shop No 5,Mansorovar</b></th>
        </tr>
        <tr>
          <th style="text-align:center;font-size: 10px;border-top: 0px;padding: 1px;font-size: 8px;" ><b>Opp. Narayan E-School</b></th>
        </tr>
         <tr>
          <th style="text-align:center;font-size: 10px;border-top: 0px;padding: 1px;font-size: 8px;"><b>Swami Satyanand Marg</b></th>
        </tr>
        <tr>
          <th style="text-align:center;font-size: 10px;border-top: 0px;padding: 1px;font-size: 8px;"><b>Bhayandar-W</b></th>
        </tr>
      </table>
      <h3 style="text-align:center;font-size: 12px;padding: 1px;font-weight: bold;color: #000;"><?php echo $title.'-'.$final_info['order_no'];; ?></h3>
      <table align="center" class="table table_custom no_border" style="width:80%;margin-bottom: 2px;">
        <tr>
          <td style="text-align:left;border-top: 0px;padding: 1px;font-weight: bold;font-size: 10px;"><?php echo 'Name : ' . $final_info['cust_name']; ?></th>
          <td style="text-align:right;border-top: 0px;padding: 1px;font-weight: bold;font-size: 10px;"><?php echo 'Mobile : ' . $final_info['cust_contact']; ?></th>
        </tr>
        <tr>
          <td style="text-align:left;border-top: 0px;padding: 1px;font-weight: bold;font-size: 10px;" colspan="2"><?php echo 'Address : ' . $final_info['cust_address'];?></td>
        </tr>
      </table>
      <table align="center" class="table table_custom no_border" style="width:80%;margin-bottom: 2px;">
        <tr>
          <td style="text-align:left;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><?php echo 'Location : '.$final_info['location']; ?></td>
          <td style="text-align:right;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><?php echo 'Table : '.$final_info['t_name']; ?></td> 
        </tr>
        <tr>
          <td style="text-align:left;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><?php echo 'Waiter : '.$final_info['waiter'];?></td>
          <td style="text-align:right;padding-left:10%;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><?php echo 'Captain : '.$final_info['captain']; ?></td>
        </tr>
      </table>
      <?php echo '--------------------------------------------------------------------------------------------------'; ?>
      <table align="center" class="table table_custom no_border" style="width:80%;margin-bottom: 2px;">
        <tr>
          <th style="text-align: left;border-top: 0px;padding: 1px;font-size: 10px;width: 42%;">Name</th>
          <th style="text-align:right;border-top: 0px;padding: 1px;font-size: 10px;width: 19%;">Rate</th> 
          <th style="text-align:right;border-top: 0px;padding: 1px;font-size: 10px;width: 19%;">QTY</th> 
          <th style="text-align:right;border-top: 0px;padding: 1px;font-size: 10px;width: 19%;">Amt</th>  
        </tr>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <td style="font-size: 10px;text-align: left;border-top: 0px;padding: 1px;"><?php echo $final_data['name']; ?></td>
            <td style="font-size: 10px;text-align:right;border-top: 0px;padding: 1px;"><?php echo $final_data['rate']; ?></td>
            <td style="font-size: 10px;text-align:right;border-top: 0px;padding: 1px;"><?php echo $final_data['qty']; ?></td>
            <td style="font-size: 10px;text-align:right;border-top: 0px;padding: 1px;"><?php echo $final_data['amt']; ?></td>
          </tr>
        <?php } ?>
      </table>
      <?php echo '--------------------------------------------------------------------------------------------------'; ?>
      <table align="center" class="table table_custom no_border" style="width:80%;margin-bottom: 2px;">
        <tr>
          <td style="text-align:left;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><?php echo 'F.Total : '.$final_info['ftotal']; ?></td>
          <td style="text-align:right;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><?php echo 'SGST : '.$csgst; ?></th> 
        </tr>
        <tr>
          <td style="text-align:left;border-top: 0px;padding: 1px;"><?php echo '  '; ?></td>
          <td style="text-align:right;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><?php echo 'CGST : '.$csgst; ?></th> 
        </tr>
      </table>
      <?php if($flag == 1 ) {?>
        <?php echo '--------------------------------------------------------------------------------------------------'; ?>
        <table align="center" class="table table_custom no_border" style="width:80%;margin-bottom: 2px;">
          <tr>
            <th style="text-align: left;border-top: 0px;padding: 1px;font-size: 10px;width: 42%;">Name</th>
            <th style="text-align:right;border-top: 0px;padding: 1px;font-size: 10px;width: 19%;">Rate</th> 
            <th style="text-align:right;border-top: 0px;padding: 1px;font-size: 10px;width: 19%;">QTY</th> 
            <th style="text-align:right;border-top: 0px;padding: 1px;font-size: 10px;width: 19%;">Amt</th> 
          </tr>
          <?php foreach($final_datass as $final_data) { ?>
            <tr>
              <td style="font-size: 10px;text-align: left;border-top: 0px;padding: 1px;"><?php echo $final_data['name']; ?></td>
              <td style="font-size: 10px;text-align:right;border-top: 0px;padding: 1px;"><?php echo $final_data['rate']; ?></td>
              <td style="font-size: 10px;text-align:right;border-top: 0px;padding: 1px;"><?php echo $final_data['qty']; ?></td>
              <td style="font-size: 10px;text-align:right;border-top: 0px;padding: 1px;"><?php echo $final_data['amt']; ?></td>
            </tr>
          <?php } ?>
        </table>
        <?php echo '--------------------------------------------------------------------------------------------------'; ?>
        <table align="center" class="table table_custom no_border" style="width:80%;margin-bottom: 2px;">
          <tr>
            <td style="text-align:left;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><?php echo 'L.Total : '.$final_info['ltotal']; ?></td>
            <td style="text-align:right;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><?php echo 'Vat : '.$final_info['vat']; ?></th> 
          </tr>
        </table>
      <?php } ?>
      <?php echo '--------------------------------------------------------------------------------------------------'; ?>
      <table align="center" class="table table_custom no_border" style="width:80%;margin-bottom: 2px;">
        <tr>
          <td style="text-align:left;border-top: 0px;padding: 1px;font-weight: bold;font-size: 10px;"><b><?php echo 'Grand Total'; ?></b></td>
          <td style="text-align:right;border-top: 0px;padding: 1px;font-weight: bold;font-size: 10px;"><b><?php echo $gtotal; ?></b></td> 
        </tr>
      </table>
      <?php if($merge_datas){ ?>
        <?php echo '--------------------------------------------------------------------------------------------------'; ?>
        <h3 style="text-align:center;font-size: 12px;padding: 1px;font-weight: bold;color: #000;"><?php echo 'Merged Bills' ?></h3>
        <?php 
          $mcount = count($merge_datas) - 1;
        ?>
        <?php foreach($merge_datas as $mkey => $mvalue){ ?>
          <table align="center" class="table table_custom no_border" style="width:80%;margin-bottom: 2px;">
            <tr>
              <td colspan="2" style="text-align:left;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><b><?php echo 'Bill Number : ' . $mvalue['order_no']; ?></b></td>
            </tr>
            <?php if($mvalue['ftotal'] > '0'){ ?>
              <tr>
                <td style="text-align:left;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><b><?php echo 'F Total : ' . $mvalue['ftotal']; ?></b></td>
                <td style="text-align:right;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><?php echo 'SGST : '.$mvalue['gst'] / 2; ?></th> 
              </tr>
              <tr>
                <td style="text-align:left;font-size: 10px;border-top: 0px;padding: 1px;"><?php echo '  '; ?></td>
                <td style="text-align:right;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><?php echo 'CGST : '.$mvalue['gst'] / 2; ?></th> 
              </tr>
            <?php } ?>
            <?php if($mvalue['ltotal'] > '0'){ ?>
              <tr>
                <td style="text-align:left;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><?php echo 'L.Total : '.$mvalue['ltotal']; ?></td>
                <td style="text-align:right;font-size: 10px;border-top: 0px;padding: 1px;font-weight: bold;"><?php echo 'Vat : '.$mvalue['vat']; ?></th> 
              </tr>
            <?php } ?>
            <?php 
              $sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - $mvalue['discount'];
            ?>
            <tr>
              <td colspan="2" style="text-align:left;border-top: 0px;padding: 1px;font-weight: bold;font-size: 10px;"><?php echo 'GRAND TOTAL : ' . $sub_gtotal; ?></td>
            </tr>
            <?php 
              $gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - $mvalue['discount']);
            ?>
          </table>
          <?php if($mkey < $mcount){ ?>
            <?php echo '--------------------------------------------------------------------------------------------------'; ?>      
          <?php } ?>
        <?php } ?>
        <?php echo '--------------------------------------------------------------------------------------------------'; ?>
        <table align="center" class="table table_custom no_border" style="width:80%;margin-bottom: 2px;">
          <tr>
            <td style="text-align:left;border-top: 0px;padding: 1px;font-weight: bold;font-size: 10px;"><b><?php echo 'Merge Grand Total'; ?></b></td>
            <td style="text-align:right;border-top: 0px;padding: 1px;font-weight: bold;font-size: 10px;"><b><?php echo $gtotal; ?></b></td> 
          </tr>
        </table>
      <?php } ?>
    </div>
  </body>
</html>
<script type="text/javascript">
$(document).ready(function() {
  window.print();
});
</script>