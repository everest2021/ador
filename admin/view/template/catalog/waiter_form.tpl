<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right">
		       <button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
		       <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
	           <h1><?php echo $heading_title; ?></h1>
	           <ul class="breadcrumb">
		           <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		            <?php } ?>
	           </ul>
	    </div>
  </div>
  <div class="container-fluid">
	   <?php if ($error_warning) { ?>
	   <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	       <button type="button" class="close" data-dismiss="alert">&times;</button>
	   </div>
	    <?php } ?>
	   <div class="panel panel-default">
	       <div class="panel-heading">
		       <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
	        </div>
	   <div class="panel-body">
		   <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
		        <div class="form-group required">
			        <label class="col-sm-3 control-label"><?php echo ' Name'; ?></label>
			        <div class="col-sm-3">
			            <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" class="form-control" />
			            <?php if ($error_name) { ?>
			            	<div class="text-danger"><?php echo $error_name; ?></div>
			             <?php } ?>
			        </div>
			        <label class="col-sm-3 control-label"><?php echo 'Code'; ?></label>
			    <div class="col-sm-3">
			        <input type="text" name="code" value="<?php echo $code; ?>" placeholder="<?php echo 'Code'; ?>" class="form-control" />
		            <?php if ($error_code) { ?>
		            	<div class="text-danger"><?php echo $error_code; ?></div>
		            <?php } ?>
			    </div>
		        </div> 
		    <div class="form-group ">
		    	<label class="col-sm-3 control-label"><?php echo 'Address'; ?></label>
			    <div class="col-sm-3">
			        <input type="text" name="address" value="<?php echo $address; ?>" placeholder="<?php echo 'Address'; ?>" class="form-control" />
		           
			    </div>
			   <label class="col-sm-3 control-label"><?php echo 'Refer By'; ?></label>
			    <div class="col-sm-3">
			        <input type="text" name="refer" value="<?php echo $refer; ?>" placeholder="<?php echo 'Refer By'; ?>" class="form-control" />
		            
			    </div>
			    
		    </div>
		    <div class="form-group ">
			   <label class="col-sm-3 control-label"><?php echo 'Place'; ?></label>
			    <div class="col-sm-3">
			        <input type="text" name="place" value="<?php echo $place; ?>" placeholder="<?php echo 'Place'; ?>" class="form-control" />
		            
			    </div>
			    <label class="col-sm-3 control-label"><?php echo 'Pin No'; ?></label>
			    <div class="col-sm-3">
			        <input type="text" name="pin" value="<?php echo $pin; ?>" placeholder="<?php echo 'Pin'; ?>" class="form-control" />
		            
			    </div>
		    </div>
		     <div class="form-group ">
			   <label class="col-sm-3 control-label"><?php echo 'Contact'; ?></label>
			    <div class="col-sm-3">
			        <input type="text" name="contact" value="<?php echo $contact; ?>" placeholder="<?php echo 'Contact'; ?>" class="form-control" />
		            
			    </div>
			   <label class="col-sm-3 control-label"><?php echo 'Date Of Join'; ?></label>
						    <div class="col-sm-3">
						     	<input type="text" id="date_doj" name='doj' value="<?php echo $doj?>" class="form-control form_datetime" placeholder="DOB">
						    </div>
		    </div>
		    	<div class="form-group ">
		    	<label class="col-sm-3 control-label"><?php echo 'Roll'; ?></label>
				   		<div class="col-sm-3" >
				        	<select name="roll" id="input-roll" class="form-control">
				            	<option ><?php echo "Please Select" ?></option>
					          	<?php foreach($rolls as $skey => $svalue){ ?>
					          		<?php if($skey == $roll){ ?>
						      			<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue; ?></option>
					          		<?php } else { ?>
						      			<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
					          		<?php } ?>
					          	<?php } ?>
				        	</select>
				   		</div>
				   		<div class="col-sm-4" style="width: 15%;">
							<?php if($commission == '1'){ ?>
								<input type="checkbox" name="commission" value="1" checked ="checked"  style="margin-right:8%">Commission<br>
							<?php } else { ?>
								<input type="checkbox" id="commission" name="commission" value="1" style="margin-right:8%;">Commission <br />
							<?php } ?>
				   		</div>
				   		<div class="col-sm-4">
							<?php if($activate == '1'){ ?>
								<input type="checkbox" name="activate" value="1" checked ="checked"  style="margin-right:8%">Activate<br>
							<?php } else { ?>
								<input type="checkbox" id="activate" name="activate" value="1" style="margin-right:8%;"> Activate <br />
							<?php } ?>
				   		</div>
				</div>
		  </div>
		</form>
	  </div>
	</div>
  </div>
</div> 
<?php echo $footer; ?>
<script type="text/javascript">
	 	$("#date_doj").datepicker();
	</script>