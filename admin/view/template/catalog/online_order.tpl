<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
				<div class="pull-right">
					<a style="display: none;" href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
				<button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
				</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>    
	</div>
	<div class="container-fluid">
			<?php if ($error_warning) { ?>
				<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
			<?php if ($success) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
			<?php if ($warning) { ?>
				<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
		</div>
		<div style="display: none;" class="well">
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label" for="input-name"><?php echo 'Location Name'; ?></label>
						<input type="text" name="filter_location" value="<?php echo $filter_location; ?>" placeholder="<?php echo ' Name'; ?>" id="input-filter_location" class="form-control" />
						<input type="hidden" name="filter_location_id" value="<?php echo $filter_location_id; ?>" id="input-filter_location_id" class="form-control" />
						<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
					</div>
				</div>
			</div>
		</div>
		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport">
			<div class="table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<td style="background: #e72b0b; color: white;" class="text-center">
								<?php echo "Order No"; ?>
							</td>
							<td style="background: #e72b0b; color: white;" class="text-center">
								<?php echo "Customer Name"; ?>
							</td>
							<td style="background: #e72b0b; color: white;" class="text-center">
								<?php echo "Email"; ?>
							</td>
							<td style="background: #e72b0b; color: white;" class="text-center">
								<?php echo "Address"; ?>
							</td>
							<td style="background: #e72b0b; color: white;" class="text-center">
								<?php echo "Telephone"; ?>
							</td>
							<td style="background: #e72b0b; color: white;" class="text-center">
								<?php echo "Order Time"; ?>
							</td>
							<td style="background: #e72b0b; color: white;" class="text-center">
								<?php echo "Order Date"; ?>
							</td>
							<td style="background: #e72b0b; color: white;" class="text-center">
								<?php echo "Grand Total"; ?>
							</td>
							<td style="width: 18%; background: #e72b0b; color: white;" class="text-center">
								<?php echo "KOT"; ?>
							</td>
						</tr>
					</thead>
					<tbody>
						<?php $inn = 0; $order_id = ''; ?>
						<?php if ($online_orders) { ?>
							<?php $i = '1';?>
							<?php foreach ($online_orders as $orders) { //echo "<pre>";print_r($orders);exit; ?>
								<tr style="<?php echo $orders['colors']; ?>">
									<td style=" font-weight: 900; <?php echo $orders['textcolor']; ?>; border-bottom: 2px solid black;" class="text-center"><?php echo $orders['order_id']; ?></td>
									<td style=" font-weight: 900; <?php echo $orders['textcolor']; ?>; border-bottom: 2px solid black;" class="text-left"><?php echo $orders['name']; ?></td>
									<td style=" font-weight: 900; <?php echo $orders['textcolor']; ?>; border-bottom: 2px solid black;" class="text-left"><?php echo $orders['email']; ?></td>
									<td style=" font-weight: 900; <?php echo $orders['textcolor']; ?>; border-bottom: 2px solid black;" class="text-left"><?php echo $orders['address']; ?></td>
									<td style=" font-weight: 900; <?php echo $orders['textcolor']; ?>; border-bottom: 2px solid black;" class="text-left"><?php echo $orders['telephone']; ?></td>
									<td style=" font-weight: 900; <?php echo $orders['textcolor']; ?>; border-bottom: 2px solid black;" class="text-center"><?php echo date('h:i a', strtotime($orders['order_time'])); ?></td>
									<td style=" font-weight: 900; <?php echo $orders['textcolor']; ?>; border-bottom: 2px solid black;" class="text-center"><?php echo $orders['order_date']; ?></td>
									<td style=" font-weight: 900; <?php echo $orders['textcolor']; ?>; border-bottom: 2px solid black;" class="text-right"><?php echo $orders['grand_tot']; ?></td>
									<td style=" font-weight: 900; <?php echo $orders['textcolor']; ?>; border-bottom: 2px solid black;" class="text-right">
										<?php if (($order_id != $orders['order_id']) || ($order_id != $orders['order_id'])) { ?>
											<a style="border-color:white; " class="pull-left btn btn-primary" onclick="online_kot(<?php echo $orders['order_id']; ?>, <?php echo $orders['grand_tot']; ?>, <?php echo $orders['live_order_id']; ?>)";>KOT</a>
											<a style="border-color:white;  background: #3a3939;" class="pull-left btn btn-primary" onclick="online_bill(<?php echo $orders['order_id']; ?>, <?php echo $orders['grand_tot']; ?>, <?php echo $orders['live_order_id']; ?>)";>Bill</a>
											<a style="border-color:white;  background: darkred;" class="pull-right btn btn-primary" onclick="order_details(<?php echo $orders['order_id']; ?>, <?php echo $orders['live_order_id']; ?>)";>Show Details</a>
										<?php } ?>
									</td>
								</tr>
								<tr style="display: none;" id="details_head_<?php echo $orders['order_id']; ?>" >
									<input type="hidden" value="0" class="details_head_status_<?php echo $orders['order_id']; ?>">
									<td colspan="2" style="background: #286090;color: white;" class="text-center">
										<b><?php echo "Item"; ?></b>
									</td>
									<td style="background: #286090;color: white;" class="text-center">
										<b><?php echo "Special Notes"; ?></b>
									</td>
									<td style="background: #286090;color: white;" class="text-center">
										<b><?php echo "Quantity"; ?></b>
									</td>
									<td style="background: #286090;color: white;" class="text-center">
										<b><?php echo "Price"; ?></b>
									</td>
									<td style="background: #286090;color: white;" class="text-center">
										<b><?php echo "Total"; ?></b>
									</td>
									<td style="background: #286090;color: white;" class="text-center">
										<b><?php echo "GST"; ?></b>
									</td>
									<td style="background: #286090;color: white;" class="text-center">
										<b><?php echo "GST Val"; ?></b>
									</td>
									<td style="background: #286090;color: white;" class="text-center">
										<b><?php echo "Grand Total"; ?></b>
									</td>
								</tr>
								<?php foreach ($orders['online_order_item'] as $orders2) { ?>
									<tr style="display: none;" class="details_body_<?php echo $orders['order_id']; ?>">
										<td colspan="2" style=" font-weight: 900;" class="text-left"><?php echo $orders2['item']; ?></td>
										<td style=" font-weight: 900;" class="text-left"><?php echo $orders2['special_notes']; ?></td>
										<td style=" font-weight: 900;" class="text-right"><?php echo $orders2['qty']; ?></td>
										<td style=" font-weight: 900;" class="text-right"><?php echo $orders2['price']; ?></td>
										<td style=" font-weight: 900;" class="text-right"><?php echo $orders2['total']; ?></td>
										<td style=" font-weight: 900;" class="text-right"><?php echo $orders2['gst']; ?></td>
										<td style=" font-weight: 900;" class="text-right"><?php echo $orders2['gst_val']; ?></td>
										<td style=" font-weight: 900;" class="text-right"><?php echo $orders2['grand_tot']; ?></td>
									</tr>
								<?php } ?>
								<?php $order_id = $orders['order_id']; ?>
								<?php $i++?>
							<?php } ?>
						<?php } else { ?>
							<tr>
								<td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</form>
		<div class="row">
			<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
			<div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
      setTimeout(function () {
        console.log('Reloading Page');
        history.go(0);
      }, 15000);
    });
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/location&token=<?php echo $token; ?>';

	var filter_location = $('input[name=\'filter_location\']').val();
	if (filter_location) {
	var filter_location_id = $('input[name=\'filter_location_id\']').val();
	if (filter_location_id) {
		url += '&filter_location_id=' + encodeURIComponent(filter_location_id);
		}
		url += '&filter_location=' + encodeURIComponent(filter_location);
	}
	location = url;
});

function online_kot(order_id, total, live_order_id) {
	var url = 'index.php?route=catalog/online_order/online_kot&token=<?php echo $token; ?>';

	if (order_id) {
		url += '&order_id=' + encodeURIComponent(order_id);
	}
	if (total) {
		url += '&total=' + encodeURIComponent(total);
	}
	if (live_order_id) {
		url += '&live_order_id=' + encodeURIComponent(live_order_id);
	}
	
	location = url;
}

function online_bill(order_id, total, live_order_id) {
	var url = 'index.php?route=catalog/online_order/OnlineBill&token=<?php echo $token; ?>';

	if (order_id) {
		url += '&order_id=' + encodeURIComponent(order_id);
	}
	if (total) {
		url += '&grand_total=' + encodeURIComponent(total);
	}
	if (live_order_id) {
		url += '&live_order_id=' + encodeURIComponent(live_order_id);
	}
	
	location = url;
}

function order_details(order_id) {
	var status = $('.details_head_status_'+order_id).val();
	if (status == 0) {
		$('.details_head_status_'+order_id).val(1);
		$('#details_head_'+order_id).show();
		$('.details_body_'+order_id).show();
	} else {
		$('.details_head_status_'+order_id).val(0);
		$('#details_head_'+order_id).hide();
		$('.details_body_'+order_id).hide();
	}
}

$('input[name=\'filter_location\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/location/autocomplete&token=<?php echo $token; ?>&filter_location=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {   
				response($.map(json, function(item) {
					return {
						label: item.location,
						value: item.location_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_location\']').val(ui.item.label);
		$('input[name=\'location_id\']').val(ui.item.value);
		
		return false;
	},
	focus: function(event, ui) {

		return false;
	}
});

	

</script></div>
<?php echo $footer; ?>