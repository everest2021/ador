<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right">
		       	 <button class="btn btn-success" onclick="amt()" id="submit">Update</button>
		       	<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo 'Cancel' ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
		    </div>
		    <h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
		    </ul>
	    </div>
	</div>
    <div class="container-fluid">
	    <div class="panel panel-default">
		    <div class="panel-heading">
		        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		    </div>
			<div class="panel-body">
		   		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
				    <div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Delivery Boy Name'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="customername" id="customername" value="<?php echo $dboy_name ?>" placeholder="<?php echo 'Name'; ?>" class="form-control"/>
							<!-- <input type="hidden" name="c_id" id="c_id" value="<?php echo $c_id ?>" class="form-control"/> -->
							
						</div>
					</div>

					<div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Order ID'; ?></label>
						<div class="col-sm-3">
							<input type="text" readonly="readonly" id="orderid" value="<?php echo $order_id; ?>" class="form-control"/>
						</div>
					</div>

					<div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Bill Total'; ?></label>
						<div class="col-sm-3">
							<input type="text" readonly="readonly" id="balance" placeholder="<?php echo $grand_total ; ?>" class="form-control"/>
						</div>
					</div>

					<div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Extra Money'; ?></label>
						<div class="col-sm-3">
							<input type="text" readonly="readonly" id="balance" placeholder="<?php echo $dboy_amount ; ?>" class="form-control"/>
						</div>
					</div>


					<div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Total'; ?></label>
						<div class="col-sm-3">
							<input type="text" readonly="readonly" id="balance" placeholder="<?php echo $dboy_amount + $grand_total ; ?>" class="form-control"/>
						</div>
					</div>

					<div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Left Amount'; ?></label>
						<div class="col-sm-3">
							<input type="text" readonly="readonly" id="balance" placeholder="<?php echo $dboy_amount + $grand_total - $given_amount ; ?>" class="form-control"/>
						</div>
					</div>

					<div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Paid'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="credit" id="credit" placeholder="<?php echo 'Paid'; ?>" value="<?php echo $given_amount; ?>" class="form-control"/>
						</div>
					</div>

				</form>
	  		</div>
		</div>
  	</div>
  	<script type="text/javascript">

    function amt(){
    	credit = $('#credit').val();
    	orderid = $('#orderid').val();
    	if(credit == ''){
    		alert('Enter the Return Amount');
    	} else{
	    	$.ajax({
			type: "POST",
				url: 'index.php?route=catalog/deliveryboyupdate/amts&token=<?php echo $token; ?>&credit='+credit+'&orderid='+orderid,
				dataType: 'json',
				success: function(json){
					console.log(json);
				}
			});	

	    	alert('Successfully Update');
	    	window.location = "index.php?route=catalog/delivery&token=<?php echo $token; ?>";
    	}
    }


  	</script>
</div>
<?php echo $footer; ?>