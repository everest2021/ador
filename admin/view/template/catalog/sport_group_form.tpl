<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-sport_group" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport_group" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo 'स्पोर्ट सांघिक नाव / Sport Group Name'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="sport_group" value="<?php echo $sport_group; ?>" placeholder="<?php echo $entry_name; ?>" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo 'खेळ प्रकार / Sport Type'; ?></label>
            <div class="col-sm-10">
              <select name="sport_type_id" id="input-sport_type_id" class="form-control">
                <?php foreach($sport_types as $skey => $svalue){ ?>
                  <?php if($skey == $sport_type_id){ ?>
                    <option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
                  <?php } else { ?>
                    <option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
              <?php if ($error_sport_type_id) { ?>
                <div class="text-danger"><?php echo $error_sport_type_id; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-participation_id"><?php echo 'सहभागी प्रकार / Participation Type'; ?></label>
            <div class="col-sm-10">
              <select name="participation_id" id="input-participation_id" class="form-control">
                <?php foreach ($part_types as $skey => $svalue) { ?>
                  <?php if ($skey == $participation_id) { ?>
                    <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                  <?php } else { ?>
                    <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
              <?php if ($error_participation_id) { ?>
              <div class="text-danger"><?php echo $error_participation_id; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-payment_date"><?php echo 'जन्म तारीख पासून / Birth Date From'; ?></label>
            <div class="col-sm-3">
              <input  type="text" name="birthdate_date_from" value="<?php echo $from_date; ?>" placeholder="<?php echo ' Date'; ?>" id="input-from_date" class="form-control date" />
            </div>
            <label class="col-sm-2 control-label" for="input-payment_date"><?php echo 'ते / To'; ?></label>
            <div class="col-sm-3">
              <input  type="text" name="birthdate_date_to" value="<?php echo $to_date; ?>" placeholder="<?php echo ' Date'; ?>" id="input-to_date" class="form-control date" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo 'Sports'; ?></label>
            <div class="col-sm-10">
              <div class="well well-sm sports_div" style="height: 350px; overflow: auto;">
                  <?php foreach ($sports as $skey => $svalue) { ?>
                  <div class="checkbox">
                    <label>
                      <?php if (in_array($skey, $sport_datas)) { ?>
                      <input type="checkbox" name="sport_datas[<?php echo $skey; ?>]" value="<?php echo $svalue; ?>" checked="checked" />
                      <?php echo $svalue; ?>
                      <?php } else { ?>
                      <input type="checkbox" name="sport_datas[<?php echo $skey; ?>]" value="<?php echo $svalue; ?>" />
                      <?php echo $svalue; ?>
                      <?php } ?>
                    </label>
                  </div>
                  <?php } ?>
                </div>
              <?php /* ?>
              <?php if($sport_type == '1'){ ?>
                <div class="well well-sm kala_class" style="height: 350px; overflow: auto;">
                  <?php foreach ($kala_sports as $skey => $svalue) { ?>
                  <div class="checkbox">
                    <label>
                      <?php if (in_array($skey, $sport_datas)) { ?>
                      <input type="checkbox" name="sport_datas[<?php echo $skey; ?>]" value="<?php echo $svalue; ?>" checked="checked" />
                      <?php echo $svalue; ?>
                      <?php } else { ?>
                      <input type="checkbox" name="sport_datas[<?php echo $skey; ?>]" value="<?php echo $svalue; ?>" />
                      <?php echo $svalue; ?>
                      <?php } ?>
                    </label>
                  </div>
                  <?php } ?>
                </div>
                <div class="well well-sm krida_class" style="height: 350px; overflow: auto;display: none;">
                  <?php foreach ($krida_sports as $skey => $svalue) { ?>
                  <div class="checkbox">
                    <label>
                      <?php if (in_array($skey, $sport_datas)) { ?>
                      <input type="checkbox" name="sport_datas[<?php echo $skey; ?>]" value="<?php echo $svalue; ?>" checked="checked" />
                      <?php echo $svalue; ?>
                      <?php } else { ?>
                      <input type="checkbox" name="sport_datas[<?php echo $skey; ?>]" value="<?php echo $svalue; ?>" />
                      <?php echo $svalue; ?>
                      <?php } ?>
                    </label>
                  </div>
                  <?php } ?>
                </div>
              <?php } else { ?>
                <div class="well well-sm kala_class" style="height: 350px; overflow: auto;display: none;">
                  <?php foreach ($kala_sports as $skey => $svalue) { ?>
                  <div class="checkbox">
                    <label>
                      <?php if (in_array($skey, $sport_datas)) { ?>
                      <input type="checkbox" name="sport_datas[<?php echo $skey; ?>]" value="<?php echo $svalue; ?>" checked="checked" />
                      <?php echo $svalue; ?>
                      <?php } else { ?>
                      <input type="checkbox" name="sport_datas[<?php echo $skey; ?>]" value="<?php echo $svalue; ?>" />
                      <?php echo $svalue; ?>
                      <?php } ?>
                    </label>
                  </div>
                  <?php } ?>
                </div>
                <div class="well well-sm krida_class" style="height: 350px; overflow: auto;">
                  <?php foreach ($krida_sports as $skey => $svalue) { ?>
                  <div class="checkbox">
                    <label>
                      <?php if (in_array($skey, $sport_datas)) { ?>
                      <input type="checkbox" name="sport_datas[<?php echo $skey; ?>]" value="<?php echo $svalue; ?>" checked="checked" />
                      <?php echo $svalue; ?>
                      <?php } else { ?>
                      <input type="checkbox" name="sport_datas[<?php echo $skey; ?>]" value="<?php echo $svalue; ?>" />
                      <?php echo $svalue; ?>
                      <?php } ?>
                    </label>
                  </div>
                  <?php } ?>
                </div>
              <?php } ?>
              <?php */ ?>
              <?php if ($error_sports_data) { ?>
                <div class="text-danger"><?php echo $error_sports_data; ?></div>
              <?php } ?>
              <a style="cursor: pointer;" onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo 'Select All'; ?></a> / <a style="cursor: pointer;" onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo 'Unselect All'; ?></a></div>
          </div>
        </form>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false,
  format: 'DD-MM-YYYY',
});
$(document).ready(function() {
  /*
  sport_type_id = $('#input-sport_type_id').val();
  if(sport_type_id == '1'){
    $('.kala_class').show();
    $('.krida_class').hide();
  } else {
    $('.kala_class').hide();
    $('.krida_class').show();
  }
  */
});

$('#input-sport_type_id, #input-participation_id').on('change', function() {
  sport_type_id = $('#input-sport_type_id').val();
  participation_id = $('#input-participation_id').val();
  $.ajax({
    url: 'index.php?route=catalog/sport_group/getsports&token=<?php echo $token; ?>&filter_sport_type=' +  encodeURIComponent(sport_type_id)+'&filter_participant_type=' +  encodeURIComponent(participation_id),
    dataType: 'json',
    success: function(json) {   
      $('.sports_div').html('');
      if(json['html']){
        $('.sports_div').html(json['html'])
      }
    }
  });
});
/*
$(document).on('change','#input-sport_type_id', function (e) {
  sport_type_id = $('#input-sport_type_id').val();
  if(sport_type_id == '1'){
    $('.kala_class').show();
    $('.krida_class').hide();
  } else {
    $('.kala_class').hide();
    $('.krida_class').show();
  }
});
*/
//--></script></div> 
<?php echo $footer; ?>