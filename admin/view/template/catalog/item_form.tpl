<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right">
		       	<button id="save" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
		       	<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
		    </div>
		    <h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
		    </ul>
	    </div>
	</div>
    <div class="container-fluid">
	    <?php if ($error_warning) { ?>
		    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			    <button type="button" class="close" data-dismiss="alert">&times;</button>
		    </div>
	    <?php } ?>
	    <div class="panel panel-default">
		    <div class="panel-heading">
		        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		    </div>
			<div class="panel-body">
		   		<form  method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
			   		<div class="form-group">
				   		<div class="col-sm-1" style="width: 15%;">
							<?php if($activate_item == '1'){ ?>
								<input type="checkbox" name="activate_item" value="1" checked ="checked"  style="margin-right:8%">Activate Item<br>
							<?php } else { ?>
								<input type="checkbox" id="activate_item" name="activate_item" value="1" style="margin-right:8%;">Activate Item <br />
							<?php } ?>
				   		</div>
				   		<div class="col-sm-2">
							<?php if($stock_register == '1'){ ?>
								<input type="checkbox" name="stock_register" value="1" checked ="checked"  style="margin-right:8%">Stock Register<br>
							<?php } else { ?>
								<input type="checkbox" id="stock_register" name="stock_register" value="1" style="margin-right:8%;"> Stock Register <br />
							<?php } ?>
				   		</div>
				   		<div class="col-sm-2">
				   			<?php if($create_stock_item == '1'){ ?>
								<input type="checkbox" name="create_stock_item" value="1" checked ="checked" style="margin-right:8%" onclick="return false;">Create Stock Item<br>
							<?php } else { ?>
								<input type="checkbox" name="create_stock_item" value="1"  style="margin-right:8%">Create Stock Item<br/>
							<?php } ?>
							<input type="hidden" name="is_create_stock" value="<?php echo $is_create_stock ?>"  >
				   		</div>
				   		<div class="col-sm-2">
							<?php if($no_discount == '1'){ ?>
								<input type="checkbox" name="no_discount" value="1" checked ="checked" style="margin-right:8%">No Discount<br>
							<?php } else { ?>
								<input type="checkbox" name="no_discount" value="1"  style="margin-right:8%">No Discount<br/>
							<?php } ?>
				   		</div>
				   		<div class="col-sm-2">
							<?php if($decimal_mesurement == '1'){ ?>
						 		<input type="checkbox" name="decimal_mesurement" value="1" checked="checked"  style="margin-right:8%">Decimal Measurement<br>
						 	<?php } else { ?>
						 		<input type="checkbox" name="decimal_mesurement" value="0"  style="margin-right:8%">Decimal Measurement<br/>
						 	<?php } ?>
				   		</div>
				   		<div class="col-sm-2">
							<?php if($inapp == '1'){ ?>
						 		<input type="checkbox" name="inapp" value="1" checked="checked"  style="margin-right:8%">Show in app<br>
						 	<?php } else { ?>
						 		<input type="checkbox" name="inapp" value="1" style="margin-right:8%">Show in app<br/>
						 	<?php } ?>
				   		</div>
			    	</div>
			    	<div class="form-group">
				  		<label class="col-sm-1 style= control-label" style="text-align:left"><?php echo 'Item Code'; ?></label>
				   		<div class="col-sm-3"  >
					  		<input type="text" name="item_code" value="<?php echo $item_code; ?>" placeholder="<?php echo $item_code; ?>" class="form-control" />
				   			<?php if ($error_item_code) { ?>
			                	<div class="text-danger"><?php echo $error_item_code; ?></div>
			                <?php } ?>
				   		</div>
				   		<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Item Name'; ?></label>
				    	<div class="col-sm-3">
				       		<input type="text" name="item_name" value="<?php echo $item_name; ?>" placeholder="<?php echo $item_name; ?>" class="form-control" />
				    		<?php if ($error_item_name) { ?>
			                	<div class="text-danger"><?php echo $error_item_name; ?></div>
			                <?php } ?>
				    	</div>
				    	<div class="col-sm-2">
							<?php if($ismodifier == '1'){ ?>
						 		<input type="checkbox" name="ismodifier" value="1" checked="checked"  style="margin-right:8%">Is Modifier<br>
						 	<?php } else { ?>
						 		<input type="checkbox" name="ismodifier" value="1" style="margin-right:8%">Is Modifier<br/>
						 	<?php } ?>
				   		</div>

				   		<label class="col-sm-2 control-label"></label>
					<div class="col-sm-2">
			        <a href="<?php echo $stock_item ?>" class="btn btn-primary">Stock Item</a>
			        <a href="<?php echo $bom ?>" class="btn btn-primary">BOM</a>
			        </div>
				 	</div>
			    	<div class="form-group ">
				   		<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Department'; ?></label>
				   		<div class="col-sm-3" >
				        	<select name="department" id="input-department" class="form-control">
				            	<option ><?php echo "Please Select" ?></option>
					          	<?php foreach($departments as $skey => $svalue){ ?>
					          		<?php if($skey == $department){ ?>
						      			<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue; ?></option>
					          		<?php } else { ?>
						      			<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
					          		<?php } ?>
					          	<?php } ?>
				        	</select>
				        	<?php if ($error_department) { ?>
			                	<div class="text-danger"><?php echo $error_department; ?></div>
			                <?php } ?>
				   		</div>
				        <label class="col-sm-1 control-label" style="text-align:left"><?php echo 'kitchen Display'; ?></label>
				        <div class="col-sm-3">
				           	<select name="kitchen_dispaly" id="input-kitchen_dispaly" class="form-control">
					            <?php foreach($kitchen_dispalys as $skey => $svalue){ ?>
					            	<?php if($skey == $kitchen_dispaly){ ?>
						        		<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
					            	<?php } else { ?>
						        		<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
					            	<?php } ?>
					            <?php } ?>
				           	</select>
				       	</div>
			    	</div>
		    		<div class="form-group">
			   			<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Purchase Price'; ?></label>
			    		<div class="col-sm-3" >
				  			<input type="text" name="purchase_price" value="<?php echo $purchase_price; ?>" placeholder="<?php echo $purchase_price; ?>" class="form-control" />
			    		</div>
			   			<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Short Name'; ?></label>
			   			<div class="col-sm-3">
				  			<input type="text" name="short_name" value="<?php echo $short_name; ?>" placeholder="<?php echo $short_name; ?>" class="form-control" />
			   			</div>
		    		</div>
		    		<div class="form-group">
			   			<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Description'; ?></label>
			    		<div class="col-sm-3" >
				  			<textarea name="description" class="form-control"><?php echo $description; ?></textarea>
			    		</div>
			   			<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Ingredient'; ?></label>
			   			<div class="col-sm-3">
				  			<textarea name="ingredient" class="form-control"><?php echo $ingredient; ?></textarea>
			   			</div>
		    		</div>
		    		<div class="form-group">
			   			<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Rate 1'; ?></label>
			   			<div class="col-sm-3">
			      			<input type="text" name="rate_1" id ="rate_1" value="<?php echo $rate_1; ?>" placeholder="<?php echo $rate_1; ?>" class="form-control" />
			      			<input type="hidden" name="inc_rate_1" id ="inc_rate_1" value="<?php echo $inc_rate_1; ?>" class="form-control" />
			   			</div>
			   			<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'UOM'; ?></label>
			   			<div class="col-sm-3">
					 		<select name="uom" id="input-umo" class="form-control">
						  		<?php foreach($uoms as $skey => $svalue){ ?>
						    		<?php if($skey == $uom){ ?>
							 			<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
							  		<?php } else { ?>
							 			<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
						   			<?php } ?>
						 		<?php } ?>
					 		</select>
			   			</div>
					</div>
					<div class="form-group"> 
				       	<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Rate 2'; ?></label>
				   		<div class="col-sm-3">
					  		<input type="text" name="rate_2" id ="rate_2" value="<?php echo $rate_2; ?>" placeholder="<?php echo $rate_2; ?>" class="form-control" />
					  		<input type="hidden" name="inc_rate_2" id ="inc_rate_2" value="<?php echo $inc_rate_2; ?>" placeholder="<?php echo $rate_2; ?>" class="form-control" />
				   		</div>
				   		<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Company'; ?></label>
				  		<div class="col-sm-3">
					  		<select name="company" id="input-Company" class="form-control">
							    <?php foreach($companys as $skey => $svalue){ ?>
								  	<?php if($skey == $company){ ?>
								     	<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
								 	<?php } else { ?>
								      	<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
							     	<?php } ?>
							   	<?php } ?>
					  		</select>
				  		</div>
				  	</div>
					<div class="form-group "> 
					    <label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Rate 3'; ?></label>
					   	<div class="col-sm-3">
							<input type="text" name="rate_3" id="rate_3" value="<?php echo $rate_3; ?>" placeholder="<?php echo $rate_3; ?>" class="form-control" />
							<input type="hidden" name="inc_rate_3" id="inc_rate_3" value="<?php echo $inc_rate_3; ?>" placeholder="<?php echo $rate_3; ?>" class="form-control" />
					   	</div>
					   	<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Modifier Group'; ?></label>
					    <div class="col-sm-3">
					      	<select name="modifier_group" id="input-modifier_group" class="form-control">
					      		<option value="" selected="selected">Please Select</option>
						    	<?php foreach($modifier_groups as $skey => $svalue){ ?>
						      		<?php if($svalue['id'] == $modifier_group){ ?>
							    		<option value="<?php echo $svalue['id'] ?>" selected="selected"><?php echo $svalue['modifier_name'] ?></option>
						      		<?php } else { ?>
							    		<option value="<?php echo $svalue['id'] ?>"><?php echo $svalue['modifier_name'] ?></option>
						      		<?php } ?>
						    	<?php } ?>
					      	</select>
					    </div>
					    <label class="col-sm-1 control-label"><?php echo 'Modifier Qty' ?></label>
					   	<div class="col-sm-1">
							<input type="text" name="modifier_qty" id="rates_3" value="<?php echo $modifier_qty; ?>" placeholder="<?php echo $modifier_qty; ?>" class="form-control" />
					   	</div>
					</div>     
		   			<div class="form-group">
			  			<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Rate 4'; ?></label>
			  			<div class="col-sm-3">
			    			<input type="text" name="rate_4" id ="rate_4" value="<?php echo $rate_4; ?>" placeholder="<?php echo $rate_4; ?>" class="form-control" />
			    			<input type="hidden" name="inc_rate_4" id ="inc_rate_4" value="<?php echo $inc_rate_4; ?>" placeholder="<?php echo $rate_4; ?>" class="form-control" />
			  			</div>	
			   			<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Max Item'; ?></label>
			    		<div class="col-sm-3">
				   			<input type="text" name="max_item" value="<?php echo $max_item; ?>" placeholder="<?php echo $max_item; ?>" class="form-control" />
			    		</div>
					</div>
					<div class="form-group">    
			    		<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Rate 5'; ?></label>
			  			<div class="col-sm-3">
				  			<input type="text" name="rate_5" id ="rate_5" value="<?php echo $rate_5; ?>" placeholder="<?php echo $rate_5; ?>" class="form-control" />
				  			<input type="hidden" name="inc_rate_5" id ="inc_rate_5" value="<?php echo $inc_rate_5; ?>" placeholder="<?php echo $rate_5; ?>" class="form-control" />
			  			</div>
			  			<label class="col-sm-1 control-label" style="text-align:left" ><?php echo 'Height'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="height" value="<?php echo $height; ?>" placeholder="<?php echo $height; ?>" class="form-control" />
						</div>
					</div>
					<div class="form-group "> 
			     		<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Rate 6'; ?></label>
			  			<div class="col-sm-3">
				  			<input type="text" name="rate_6" id="rate_6" value="<?php echo $rate_6; ?>" placeholder="<?php echo $rate_6; ?>" class="form-control" />
				  			<input type="hidden" name="inc_rate_6" id="inc_rate_6" value="<?php echo $inc_rate_6; ?>" placeholder="<?php echo $rate_6; ?>" class="form-control" />

			  			</div>
			  			<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Width'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="width" value="<?php echo $width; ?>" placeholder="<?php echo $width; ?>" class="form-control" />
						</div> 

						 <label class="col-sm-1 control-label" style="text-align:left"><?php echo 'API RATE'; ?></label>
					    <div class="col-sm-3">
					        <input type="text" name="api_rate" id="api_rate" value="<?php echo $api_rate; ?>" placeholder="<?php echo $api_rate; ?>" class="form-control" />
					        <input type="hidden" name="inc_api_rate" id="inc_api_rate" value="<?php echo $inc_api_rate; ?>" placeholder="<?php echo $api_rate; ?>" class="form-control" />
					        <?php if ($error_api_rate) { ?>
			                	<div class="text-danger"><?php echo $error_api_rate; ?></div>
			                <?php } ?>
					    </div>  
		    		</div>

		    		<div class="form-group "> 
			     		<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Birthday Point'; ?></label>
			  			<div class="col-sm-3">
				  			<input type="text" name="birthday_point" id="birthday_point" value="<?php echo $birthday_point; ?>" placeholder="" class="form-control" />
				  			<!--<input type="hidden" name="inc_rate_6" id="inc_rate_6" value="<?php echo $inc_rate_6; ?>"placeholder="<?php echo $rate_6; ?>" class="form-control" />-->

			  			</div>
			  			<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Normal Point'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="normal_point" value="<?php echo $normal_point; ?>" placeholder="" class="form-control" />
						</div>   
		    		</div>
		    		<div class="form-group">
			   			<label class="col-sm-1 control-label"  style="text-align:left; width:8%"><?php echo 'Item Category'; ?></label>
			    		<div class="col-sm-3">
				   			<select name="item_category_id" id="item_category_id" class="form-control">
				   				<option><?php echo "Please Select" ?></option>
				    			<?php foreach($item_categorys as $skey => $svalue){ ?>
					   				<?php if($skey == $item_category_id){ ?>
					      				<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
					   				<?php } else { ?>
					      				<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
					   				<?php } ?>
				    			<?php } ?>
				   			</select>
				    		<input type="hidden" name="item_category" id="item_category"  value="<?php echo $item_category; ?>"  class="form-control" />
				    		<?php if ($error_item_category_id) { ?>
			                	<div class="text-danger"><?php echo $error_item_category_id; ?></div>
			                <?php } ?>
			    		</div>
			    		<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Item Sub Category'; ?></label>
			  			<div class="col-sm-3">
				 			<select name="item_sub_category_id" id="item_sub_category_id" class="form-control">
					 			<option><?php echo "Please Select" ?></option>
					 			<?php foreach($item_sub_categorys as $skey => $svalue){ ?>
					  				<?php if($svalue['category_id'] == $item_sub_category_id){ ?>
					   					<option value="<?php echo $svalue['category_id'] ?>" selected="selected"><?php echo $svalue['name'] ?></option>
					   				<?php } else { ?>
					   					<option value="<?php echo $svalue['category_id'] ?>"><?php echo $svalue['name'] ?></option>
					  				<?php } ?>
					 			<?php } ?>
				 			</select>
				  			<input type="hidden" name="item_sub_category" id="item_sub_category"  value="<?php echo $item_sub_category; ?>" class="form-control"/>
				  			<?php if ($error_item_sub_category_id) { ?>
			                	<div class="text-danger"><?php echo $error_item_sub_category_id; ?></div>
			                <?php } ?>
			  			</div>
		    		</div>
					<div class="form-group">
		       			<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Brand'; ?></label>
						<div class="col-sm-3">
					  		<select name="brand_id" id="input-brand_id" class="form-control">
						 		<option><?php echo "Please Select" ?></option>
								<?php foreach($brands as $skey => $svalue){ ?>
									<?php if($svalue['brand_id'] == $brand_id){ ?>
					   					<option value="<?php echo $svalue['brand_id'] ?>" selected="selected"><?php echo $svalue['brand'] ?></option>
									<?php } else { ?>
					   					<option value="<?php echo $svalue['brand_id'] ?>"><?php echo $svalue['brand'] ?></option>
					   				<?php } ?>
					   			<?php } ?>
					  		</select>
					  		<input type="hidden" name="brand" id="brand"  value="<?php echo $brand; ?>"  class="form-control" />
						</div> 
						<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Barcode'; ?></label>
			    		<div class="col-sm-3">
				  			<input type="text" name="barcode" value="<?php echo $barcode; ?>" placeholder="<?php echo $barcode; ?>" class="form-control" />
			    		</div> 	
					</div>
					<div class="form-group">
				 		<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Size'; ?></label>
						<div class="col-sm-3">
					 		<select name="quantity" id="input-quantity" class="form-control">
					 		<option>Please Select</option>
					   		<?php foreach($quantitys as $skey => $svalue){ ?>
						 		<?php if($svalue['id'] == $quantity['id']){ ?>
						   			<option value="<?php echo $svalue['id'] ?>" selected="selected"><?php echo $svalue['size'] ?></option>
						   		<?php } else { ?>
						   			<option value="<?php echo $svalue['id'] ?>"><?php echo $svalue['size'] ?></option>
						 		<?php } ?>
					  		<?php } ?>
					 		</select>
						</div>
						<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'colour'; ?></label>
					  	<div class="col-sm-3">
					  		<?php if($colour != ''){ ?>
					    		<input type="text" name="colour" value="<?php echo $colour; ?>" placeholder="<?php echo $colour; ?>" id="colorSelector" class="form-control" style="background-color: <?php echo $colour; ?>" />
					  		<?php } else { ?>
					  			<input type="text" name="colour" value="<?php echo $colour; ?>" placeholder="<?php echo $colour; ?>" id="colorSelector" class="form-control" />
					  		<?php } ?>
					  	</div>
						<div class="col-sm-2">
							<?php if($hot_key == '1'){ ?>
						   		<input type="checkbox" name="hot_key" value="1" checked="checked"  style="margin-right:8%">HotKey<br>
						 	<?php } else { ?>
						   		<input type="checkbox" name="hot_key" value="1" style="margin-right:8%">HotKey<br/>
						 	<?php } ?>
						</div>
					</div>
					<div class="form-group">
				  		<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'MRP'; ?></label>
				    	<div class="col-sm-3">
					  		<input type="text" name="mrp" value="<?php echo $mrp; ?>" placeholder="<?php echo $mrp; ?>" class="form-control" />
				    	</div>
				    	<label class="col-sm-1 control-label" style="text-align:left" ><?php echo 'Is.Liquor'; ?></label>
				        <div class="col-sm-3">
				        	<select name="is_liq" id="input-is_liq" class="form-control">
						        <?php foreach($is_liqs as $skey => $svalue){ ?>
						        	<?php if($skey == $is_liq){ ?>
							    		<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
						           <?php } else { ?>
							    		<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
						      		<?php } ?>
						    	<?php } ?>
				      		</select>
				    	</div>
					</div>
					<div class="form-group ">
				  		<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'S.Tax'; ?></label>
				  		<div class="col-sm-3">
					  		<select name="s_tax" id="input-s_tax" class="form-control">
						 		<option><?php echo "Please Select" ?></option>
						 		<?php foreach($s_taxs as $skey => $svalue){ ?>
						 			<?php if($skey == $s_tax){ ?>
						 				<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
						 			<?php } else { ?>
						 				<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
						 			<?php } ?>
						 		<?php } ?>
					  		</select>	
				   		</div>
				   		<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Tax 1'; ?></label>
				  		<div class="col-sm-3">
					 		<select name="vat" id="input-vat" class="form-control">
					   			<option value="99"><?php echo "Please Select" ?></option>
								<?php foreach($taxs as $skey => $svalue){ ?>
						 			<?php if ($svalue['id'] == $vat){ ?>
						  				<option value="<?php echo $svalue['id'] ?>" selected="selected"><?php echo $svalue['tax_name'] ?></option>
						  			<?php } else { ?>
						 				<option value="<?php echo $svalue['id'] ?>"><?php echo $svalue['tax_name']?></option>
					   				<?php } ?>
					  			<?php } ?>
					 		</select>
					 		<?php if ($gst_error) { ?>
			                	<div class="text-danger"><?php echo $gst_error; ?></div>
			                <?php } ?>
					 		<input type="hidden" id="tax_percent" value="<?php echo $tax_percent ?>">
				  		</div>
				  		<div class="col-sm-4" >
							<?php if($gst_incu == '1'){ ?>
						   		<input type="checkbox" name="gst_incu" id="gst_incu" value="1" checked="checked"  style="margin-right:8%">GST INCL<br>
						  	<?php } else { ?>
						    	<input type="checkbox" name="gst_incu" id="gst_incu" value="1" style="margin-right:8%">GST INCL<br/>
						 	<?php } ?>
				    	</div>
			    	</div>
			    	<div class="form-group ">
				  		<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Tax 2'; ?></label>
				  		<div class="col-sm-3">
					 		<select name="tax2" id="input-tax2" class="form-control">
					   			<option><?php echo "Please Select" ?></option>
								<?php foreach($taxs as $skey => $svalue){ ?>
						 			<?php if ($svalue['id'] == $tax2){ ?>
						  				<option value="<?php echo $svalue['id'] ?>" selected="selected"><?php echo $svalue['tax_name'] ?></option>

						  			<?php } else { ?>
						 				<option value="<?php echo $svalue['id'] ?>"><?php echo $svalue['tax_name']?></option>
					   				<?php } ?>
					  			<?php } ?>
					 		</select>
				  		</div>
				  		<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Kotmsg Category'; ?></label>
				  		<div class="col-sm-3">
					  		<select name="kotmsg_cat" class="form-control">
						 		<option><?php echo "Please Select" ?></option>
						 		<?php foreach($kotmsg_cats as $svalue){ ?>
						 			<?php if($svalue['id'] == $kotmsg_cat){ ?>
						 				<option value="<?php echo $svalue['id'] ?>" selected="selected"><?php echo $svalue['name'] ?></option>
						 			<?php } else { ?>
						 				<option value="<?php echo $svalue['id'] ?>"><?php echo $svalue['name'] ?></option>
						 			<?php } ?>
						 		<?php } ?>
					  		</select>	
				   		</div>
			    	</div>
					<div class="form-group">
				    	<label class="col-sm-1 control-label" style="text-align:left; width: 8.33333%"><?php echo 'Captain Com.Rate%'; ?></label>
					  	<div class="col-sm-3">
							<input type="text" name="captain_rate_per" value="<?php echo $captain_rate_per; ?>" placeholder="<?php echo $captain_rate_per; ?>" class="form-control" />
					  	</div>
				    	<div class="col-sm-4">
					   		<select name="captain_rate_value" id="input-captain_rate_value" class="form-control">
						 		<?php foreach($captain_rate_values as $skey => $svalue){ ?>
						   			<?php if($skey == $captain_rate_value){ ?>
						      			<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
						    		<?php } else { ?>
						      			<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
						    		<?php } ?>
								<?php } ?>
					  		</select>
				    	</div>
					</div>
					<div class="form-group">
						<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Stweward Rate %'; ?></label>
				    	<div class="col-sm-3">
				        	<input type="text" name="stweward_rate_per" value="<?php echo $stweward_rate_per; ?>" placeholder="<?php echo 'Stweward Rate Percent'; ?>" class="form-control" />
				    	</div>
				    	<div class="col-sm-4">
					    	<select name="stweward_rate_value" id="input-stweward_rate_value" class="form-control">
						   		<?php foreach($stweward_rate_values as $skey => $svalue){ ?>
						     		<?php if($skey == $stweward_rate_value){ ?>
							   			<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
						     		<?php } else { ?>
							   			<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
						   			<?php } ?>
						 		<?php } ?>
					    	</select>
				    	</div>
				    	<div class="col-sm-4" >
							<?php if($with_qty == '1'){ ?>
						   		<input type="checkbox" name="with_qty" value="1" checked="checked" id = "with_qty" style="margin-right:8%">WithQty<br>
						  	<?php } else { ?>
						    	<input type="checkbox" name="with_qty" value="1" id = "with_qty" style="margin-right:8%">WithQty<br/>
						 	<?php } ?>
				    	</div>
				    		
					</div>
					<div class="form-group">
						<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Image'; ?></label>
					    <div class="col-sm-3">
					  		<input readonly="readonly" type="text" name="photo" value="<?php echo $photo; ?>" placeholder="<?php echo 'Image'; ?>" id="input-photo" class="form-control" />
					  		<input type="hidden" name="photo_source" value="<?php echo $photo_source; ?>" id="input-photo_source" class="form-control" />
					  		<br>
					  		<span class="input-group-btn">
								<button type="button" id="button-photo" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>
					  		</span>
							<?php if($photo_source != ''){ ?>
					  			<br/ ><a target="_blank" style="cursor: pointer;" id="photo_source" class="thumbnail" href="<?php echo $photo_source; ?>">View Image</a>
							<?php } ?>
						</div>
						<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Packaging Charge'; ?></label>
				    	<div class="col-sm-3">
				        	<input type="text" name="packaging_charge" value="<?php echo $packaging_charge; ?>" placeholder="<?php echo 'Packaging Charge'; ?>" class="form-control" />
				    	</div>

				    	<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Weight(For Api Use Only)'; ?></label>
				    	<div class="col-sm-3">
				        	<input type="number" name="weight_for_api" value="<?php echo $weight_for_api; ?>" placeholder="<?php echo 'Weight'; ?>" class="form-control" />
				    	</div>
				    	
			    	</div>

			    	<div class="form-group">
				    	<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Is Item in Stock (For Api Use)'; ?></label>
						<div class="col-sm-3">
					    	<select name="item_stock_api" id="item_stock_api" class="form-control">
						        <?php foreach($Item_avilable_api as $ikey => $ivalue){ ?>
						        	<?php if($ikey == $item_stock_api){ ?>
							    		<option value="<?php echo $ikey ?>" selected="selected"><?php echo $ivalue ?></option>
						           <?php } else { ?>
							    		<option value="<?php echo $ikey ?>"><?php echo $ivalue ?></option>
						      		<?php } ?>
						    	<?php } ?>
					  		</select>
					  		 <?php if ($error_item_stock_api) { ?>
			                	<div class="text-danger"><?php echo $error_item_stock_api; ?></div>
			                <?php } ?>
						</div>

				    	<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Serves (For Api Use)'; ?></label>
				    	<div class="col-sm-3">
				        	<input type="number" name="serves" value="<?php echo $serves; ?>" class="form-control" />
				        	  <?php if ($error_serves) { ?>
			                	<div class="text-danger"><?php echo $error_serves; ?></div>
			                <?php } ?>
				    	</div>

				    	<label class="col-sm-1 control-label" style="text-align:left"><?php echo 'Food Type(For Api Use)'; ?></label>
						<div class="col-sm-3">
					    	<select name="food_type_api" id="food_type_api" class="form-control">
						        <?php foreach($food_type_api_array as $fkey => $fvalue){ ?>
						        	<?php if($fkey == $food_type_api){ ?>
							    		<option value="<?php echo $fkey ?>" selected="selected"><?php echo $fvalue ?></option>
						           <?php } else { ?>
							    		<option value="<?php echo $fkey ?>"><?php echo $fvalue ?></option>
						      		<?php } ?>
						    	<?php } ?>
					  		</select>
					  		 <?php if ($error_food_type_api) { ?>
			                	<div class="text-danger"><?php echo $error_food_type_api; ?></div>
			                <?php } ?>
						</div>
				    </div>
				    <div class="form-group">
			            <label class="col-sm-1 control-label">Fulfillment modes (For Api Use)</label>
			            <div class="col-sm-5">
			              <div class="well well-sm" style="height: 150px; overflow: auto;">
			                <?php foreach ($fullfillment_mode_array as $fullfilled => $fullfilledvaluel) { ?>
			                <div class="checkbox">
			                  <label>
			                    <?php if (in_array($fullfilled, $fullfillmode)) { ?>
			                    <input type="checkbox" name="fullfillmode[]" value="<?php echo $fullfilledvaluel; ?>" checked="checked" />
			                    <?php echo ucwords($fullfilledvaluel); ?>
			                    <?php } else { ?>
			                    <input type="checkbox" name="fullfillmode[]" value="<?php echo $fullfilledvaluel; ?>" />
			                    <?php echo ucwords($fullfilledvaluel); ?>
			                    <?php } ?>
			                  </label>
			                </div>
			                <?php } ?>
			              </div>
			              <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
			               <?php if ($error_fullfillmode) { ?>
			                	<div class="text-danger"><?php echo $error_fullfillmode; ?></div>
			                <?php } ?>
			          	</div>
			            <label class="col-sm-1 control-label">Included Platform (For Api Use)</label>
			            <div class="col-sm-5">
				            <div class="well well-sm" style="height: 150px; overflow: auto;">
				                <?php foreach ($included_platforms_array as $included_platkey => $included_platformvalue) { ?>
				                <div class="checkbox">
				                  <label>
				                    <?php if (in_array($included_platkey, $platfome_included)) { ?>
				                    <input type="checkbox" name="platfome_included[]" value="<?php echo $included_platformvalue; ?>" checked="checked" />
				                    <?php echo ucwords($included_platformvalue); ?>
				                    <?php } else { ?>
				                    <input type="checkbox" name="platfome_included[]" value="<?php echo $included_platformvalue; ?>" />
				                    <?php echo ucwords($included_platformvalue); ?>
				                    <?php } ?>
				                  </label>
				                </div>
				                <?php } ?>
				            </div>
			              <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
			                <?php if ($error_platfome_included) { ?>
			                	<div class="text-danger"><?php echo $error_platfome_included; ?></div>
			                <?php } ?>
			          </div>
			          </div>
				</form>
	  		</div>
		</div>
  	</div>	
</div>
<script type="text/javascript"><!--

/*$('#input-uom').on('change', function() {
  unit_name = $('#input-uom option:selected').text();
  $('#unit_name').val(unit_name);
});

unit_name = $('#input-uom option:selected').text();
  $('#unit_name').val(unit_name);*/


	$("#save").click(function(){
		$('#save').attr('onclick','').unbind('click');
	    action1 = '<?php echo $action; ?>';
		//alert(action1);
		action1 = action1.replace("&amp;", "&");
		action1 = action1.replace("&amp;", "&");
		$('#form-sport').attr("action", action1); 
		$('#form-sport').submit();
		//alert('inn');
		return false;
	});



$('.date').datetimepicker({
  pickTime: false,
  format: 'DD-MM-YYYY',
});
$(document).ready(function() {
  part_type = $('#input-part_type').val();
 
  if(part_type == '2' || part_type == ''){
	$('.part').hide();
   
  }
 if(part_type == '1'){
	$('.part').show();
   
  }});

$(document).on('change','#input-part_type', function (e) {
  part_type = $('#input-part_type').val();
 
  if(part_type == '2' || part_type == ''){
	$('.part').hide();
   
  }
 if(part_type == '1'){
	$('.part').show();
   
  }});

$('#item_category_id').on('change', function() {
  category_id = $('#item_category_id option:selected').text();
  $('#item_category').val(category_id);

  category = $('#item_category_id').val();
  //alert('in');
  $.ajax({
	url: 'index.php?route=catalog/item/getsubcategory&token=<?php echo $token; ?>&filter_category_id=' +  encodeURIComponent(category),
	dataType: 'json',
	success: function(json) {   
	  $('#item_sub_category_id').find('option').remove();
	  if(json['item_categorys']){
		$.each(json['item_categorys'], function (i, item) {
		  $('#item_sub_category_id').append($('<option>', { 
			  value: item.subcategory_id,
			  text : item.subcategory 
		  }));
		});
	  }
	}
  });
});

$('#item_sub_category_id').on('change', function() {
	category_id = $('#item_sub_category_id option:selected').text();
  $('#item_sub_category').val(category_id);

});

$('#input-vat').on('change', function() {

});

$(document).ready(function() {
	atstart = $("#input-vat option:selected").val();
	if(atstart == 99){
    	$('#tax_percent').val(0);
  	}
});

// $('#item_sub_category_id').on('change', function() {
//   sub_category_id = $('#item_sub_category_id option:selected').text();
//   $('#item_sub_category').val(sub_category_id);
//   category = $('#item_sub_category_id').val();
//   $.ajax({
// 	url: 'index.php?route=catalog/item/getbrands&token=<?php echo $token; ?>&filter_subcategory_id=' +  encodeURIComponent(category),
// 	dataType: 'json',
// 	success: function(json) {   
// 	  $('#input-brand_id').find('option').remove();
// 	  if(json['brands']){
// 		$.each(json['brands'], function (i, item) {
// 		  $('#input-brand_id').append($('<option>', { 
// 			  value: item.brand_id,
// 			  text : item.brand 
// 		  }));
// 		});
// 	  }
// 	}
//   });
// });

$('#input-brand_id').on('change', function() {
  brand_id = $('#input-brand_id option:selected').text();
  $('#brand').val(brand_id);
  brand_id_val = $('#input-brand_id option:selected').val();
  $.ajax({
  	url: 'index.php?route=catalog/item/getBrandQuantity&token=<?php echo $token; ?>&brand_id=' +  encodeURIComponent(brand_id_val),	
  	dataType: 'json',
  	success:function(json){
  		$('#input-quantity').find('option').remove();
  		if(json){
		$.each(json, function (i, item) {
		  $('#input-quantity').append($('<option>', { 
			  value: item.qty_id,
			  text : item.qty 
		  }));
		});
	  }
  	}
  })
});

$('#colorSelector').ColorPicker({
	color: '#0000ff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
		$('#colorSelector').css('backgroundColor', '#' + hex);
		$('#colorSelector').val('#' + hex);
	},
	onSubmit: function (hsb, hex, rgb) {
		$('#colorSelector').css('backgroundColor', '#' + hex);
		$('#colorSelector').val('#' + hex);
	}
});

$('#button-photo').on('click', function() {
  $('#form-photo').remove();
  $('body').prepend('<form enctype="multipart/form-data" id="form-photo" style="display: none;"><input type="file" name="file" /></form>');
  $('#form-photo input[name=\'file\']').trigger('click');
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  timer = setInterval(function() {
	if ($('#form-photo input[name=\'file\']').val() != '') {
	  clearInterval(timer); 
	  image_name = 'item';  
	  $.ajax({
		url: 'index.php?route=catalog/item/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-photo')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload').button('loading');
		},
		complete: function() {
		  $('#button-upload').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
			alert(json['error']);
		  }
		  if (json['success']) {
			alert(json['success']);
			console.log(json);
			$('input[name=\'photo\']').attr('value', json['filename']);
			$('input[name=\'photo_source\']').attr('value', json['link_href']);
			d = new Date();
			var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="photo_source" href="'+json['link_href']+'">View Image</a>';
			$('#photo_source').remove();
			$('#button-photo').after(previewHtml);
		  }
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});

//$("input[type='checkbox']").val();


// $('#gst_incu').on('change', function() {

// 	rate_1 = $('#rate_1 option:selected').val();
//     alert(rate_1);
// )}


// if($('#gst_incu').is(":checked")){
// 		//rate_1= $("#rate_1").val();
//         //$('input[name=\'inc_rate_1\']').val(rate_1);

// 	$('#input-vat').change(function(){
//              test = $("#test").val();
// 			 	rate_1= $("#rate_1").val();
// 			 	alert(rate_1);
//             	rate_value = rate_1 * test / 100;
//             	rate_1_val = rate_1 -  rate_value;
//             $('input[name=\'inc_rate_1\']').val(rate_1_val);
//         else {
//         	rate_1= $("#rate_1").val();
//         	$('input[name=\'inc_rate_1\']').val(rate_1);
//         }    
// 	)};
// )};

// $('#gst_incu').change(function(){


// )};
	 // if($('#gst_incu').is(":not(:checked)")){
	 // 		rate_1= $("#rate_1").val();
	 // 		$('input[name=\'inc_rate_1\']').val(rate_1);				
	 // 	)};

	

$(document).ready(function(){
	$('#rate_1').focusout(function(){
		console.log("in");
	  	rate_1 = $('#rate_1').val();
	  	tax_percent = $('#tax_percent').val();

	  	if($('#gst_incu').is(":checked")){
	  		inc_checked = 1;
	  	} else {
	  		inc_checked = 0;
	  	}
	  	rate_1= $("#rate_1").val();
   		if(tax_percent > 0){
   			rate_value = rate_1 * tax_percent / 100;
	   		if(inc_checked == 1){
	   			rate_1_val = rate_1 - rate_value;
	   		} else {
	   			rate_1_val = rate_1;
	   		}
	   	} else {
	   		rate_1_val = rate_1;
	   	}	
	   	console.log("in2");
   		$('#inc_rate_1').val(rate_1_val); 
    });

    
    $('#gst_incu').click(function(){
        if($(this).is(":checked")){
			tax_percent = $("#tax_percent").val();
			//start
			rate_1= $("#rate_1").val();
       		if(tax_percent > 0){
	       		rate_value = rate_1 * tax_percent / 100;
	       		rate_1_val = rate_1 -  rate_value;
	       	} else {
	       		rate_1_val = rate_1;
	       	}
       		$('input[name=\'inc_rate_1\']').val(rate_1_val);
       		//end

       		//start 2
			rate_2= $("#rate_2").val();
       		if(tax_percent > 0){
	       		rate_value = rate_2 * tax_percent / 100;
	       		rate_2_val = rate_2 -  rate_value;
	       	} else {
	       		rate_2_val = rate_2;
	       	}
       		$('input[name=\'inc_rate_2\']').val(rate_2_val);
       		//end 2

       		//start 3
			rate_3= $("#rate_3").val();
       		if(tax_percent > 0){
	       		rate_value = rate_3 * tax_percent / 100;
	       		rate_3_val = rate_3 -  rate_value;
	       	} else {
	       		rate_3_val = rate_3;
	       	}
       		$('input[name=\'inc_rate_3\']').val(rate_3_val);
       		//end 3

       		//start 4
			rate_4= $("#rate_4").val();
       		if(tax_percent > 0){
	       		rate_value = rate_4 * tax_percent / 100;
	       		rate_4_val = rate_4 -  rate_value;
	       	} else {
	       		rate_4_val = rate_4;
	       	}
       		$('input[name=\'inc_rate_4\']').val(rate_4_val);
       		//end 4

       		//start 5
			rate_5= $("#rate_5").val();
       		if(tax_percent > 0){
	       		rate_value = rate_5 * tax_percent / 100;
	       		rate_5_val = rate_5 -  rate_value;
	       	} else {
	       		rate_5_val = rate_5;
	       	}
       		$('input[name=\'inc_rate_5\']').val(rate_5_val);
       		//end 5

       		//start 5
			rate_6= $("#rate_6").val();
       		if(tax_percent > 0){
	       		rate_value = rate_6 * tax_percent / 100;
	       		rate_6_val = rate_6 -  rate_value;
	       	} else {
	       		rate_6_val = rate_6;
	       	}
       		$('input[name=\'inc_rate_6\']').val(rate_6_val);

       		api_rate= $("#api_rate").val();
       		if(tax_percent > 0){
	       		rate_value = api_rate * tax_percent / 100;
	       		api_rate_val = api_rate -  rate_value;
	       	} else {
	       		api_rate_val = api_rate;
	       	}
       		$('input[name=\'inc_api_rate\']').val(api_rate_val);
       		//end 5

        } else if($(this).is(":not(:checked)")){
        	//$('#input-vat').off('change');
        	//start
        	rate_1 = $("#rate_1").val();
        	$('input[name=\'rate_1\']').val(rate_1);
        	$('input[name=\'inc_rate_1\']').val(rate_1);
        	//end

        	//start
        	rate_2 = $("#rate_2").val();
        	$('input[name=\'rate_2\']').val(rate_2);
        	$('input[name=\'inc_rate_2\']').val(rate_2);
        	//end

        	//start
        	rate_3 = $("#rate_3").val();
        	$('input[name=\'rate_3\']').val(rate_3);
        	$('input[name=\'inc_rate_3\']').val(rate_3);
        	//end

        	//start
        	rate_4 = $("#rate_4").val();
        	$('input[name=\'rate_4\']').val(rate_4);
        	$('input[name=\'inc_rate_4\']').val(rate_4);
        	//end

        	//start
        	rate_5 = $("#rate_5").val();
        	$('input[name=\'rate_5\']').val(rate_5);
        	$('input[name=\'inc_rate_5\']').val(rate_5);
        	//end

        	//start
        	rate_6 = $("#rate_6").val();
        	$('input[name=\'rate_6\']').val(rate_6);
        	$('input[name=\'inc_rate_6\']').val(rate_6);
        	//end

        		//start
        	api_rate = $("#api_rate").val();
        	$('input[name=\'api_rate\']').val(api_rate);
        	$('input[name=\'inc_api_rate\']').val(api_rate);
        	//end
        }
    });

    $('#input-vat').change(function(){
	  	tax_id = $('#input-vat option:selected').val();
		$.ajax({
			type: "POST",
  			url: 'index.php?route=catalog/item/gettaxs&token=<?php echo $token; ?>&filter_tax_id=' +  encodeURIComponent(tax_id),
  			dataType: 'json',
  			success: function(data) {  
	  		   	var test = data.taxs;
				$('#tax_percent').val(test.tax_value);
				tax_percent = $("#tax_percent").val();
				if($('#gst_incu').is(":checked")){
			  		inc_checked = 1;
			  	} else {
			  		inc_checked = 0;
			  	}
			  	//start
			  	rate_1= $("#rate_1").val();
		   		if(tax_percent > 0){
		   			rate_value = rate_1 * tax_percent / 100;
			   		if(inc_checked == 1){
			   			rate_1_val = rate_1 - rate_value;
			   		} else {
			   			rate_1_val = rate_1;
			   		}
			   	} else {
			   		rate_1_val = rate_1;
			   	}	
			   	console.log("in1");
		   		$('#inc_rate_1').val(rate_1_val);
		   		//end


		   		//start
			  	rate_2= $("#rate_2").val();
		   		if(tax_percent > 0){
		   			rate_value = rate_2 * tax_percent / 100;
			   		if(inc_checked == 1){
			   			rate_2_val = rate_2 - rate_value;
			   		} else {
			   			rate_2_val = rate_2;
			   		}
			   	} else {
			   		rate_2_val = rate_2;
			   	}	
		   		$('#inc_rate_2').val(rate_2_val);
		   		//end

		   		//start
			  	rate_3= $("#rate_3").val();
		   		if(tax_percent > 0){
		   			rate_value = rate_3 * tax_percent / 100;
			   		if(inc_checked == 1){
			   			rate_3_val = rate_3 - rate_value;
			   		} else {
			   			rate_3_val = rate_3;
			   		}
			   	} else {
			   		rate_3_val = rate_3;
			   	}	
		   		$('#inc_rate_3').val(rate_3_val);
		   		//end

		   		//start
			  	rate_4= $("#rate_4").val();
		   		if(tax_percent > 0){
		   			rate_value = rate_4 * tax_percent / 100;
			   		if(inc_checked == 1){
			   			rate_4_val = rate_4 - rate_value;
			   		} else {
			   			rate_4_val = rate_4;
			   		}
			   	} else {
			   		rate_4_val = rate_4;
			   	}	
		   		$('#inc_rate_4').val(rate_4_val);
		   		//end

		   		//start
			  	rate_5= $("#rate_5").val();
		   		if(tax_percent > 0){
		   			rate_value = rate_5 * tax_percent / 100;
			   		if(inc_checked == 1){
			   			rate_5_val = rate_5 - rate_value;
			   		} else {
			   			rate_5_val = rate_5;
			   		}
			   	} else {
			   		rate_5_val = rate_5;
			   	}	
		   		$('#inc_rate_5').val(rate_5_val);
		   		//end

		   		//start
			  	rate_6= $("#rate_6").val();
		   		if(tax_percent > 0){
		   			rate_value = rate_6 * tax_percent / 100;
			   		if(inc_checked == 1){
			   			rate_6_val = rate_6 - rate_value;
			   		} else {
			   			rate_6_val = rate_6;
			   		}
			   	} else {
			   		rate_6_val = rate_6;
			   	}	
		   		$('#inc_rate_6').val(rate_6_val);
		   		//end

		   		//start
			  	api_rate= $("#api_rate").val();
		   		if(tax_percent > 0){
		   			rate_value = api_rate * tax_percent / 100;
			   		if(inc_checked == 1){
			   			api_rate_val = api_rate - rate_value;
			   		} else {
			   			api_rate_val = api_rate;
			   		}
			   	} else {
			   		api_rate_val = api_rate;
			   	}	
		   		$('#inc_api_rate').val(api_rate_val);
		   		//end
  			}
		});
    });
});

/*
$(document).ready(function(){
	    oldvalue2 = $("#rate_2").val();
        $('#gst_incu').click(function(){
            if($(this).is(":checked")){
            					test = $("#test").val();
								rate_2= $("#rate_2").val();
			               		rate_value = rate_2 * test / 100;
			               		rate_2_val = rate_2 - rate_value;
			               		$('input[name=\'inc_rate_2\']').val(rate_2_val);
            	$('#input-vat').change(function(){
        			  sub_category_id = $('#input-vat option:selected').val();
					  $('#input-vat').val(sub_category_id);
					  category = $('#input-vat').val();
						$.ajax({
							type: "POST",
					  		url: 'index.php?route=catalog/item/gettaxs&token=<?php echo $token; ?>&filter_subcategory_id=' +  encodeURIComponent(category),
					  		dataType: 'json',
					  		data: {"data":"check"},
					  		success: function(data) {  
					  		    $("#rate_2").val(oldvalue2); 
						  		var test = data.taxs;
								$('#test').val(test.tax_value);
								test = $("#test").val();
								rate_2= $("#rate_2").val();
			               		rate_value = rate_2 * test / 100;
			               		rate_2_val = rate_2 -  rate_value;
			               		console.log(rate_2_val);
			              		$('input[name=\'inc_rate_2\']').val(rate_2_val);
					  		}
						});
	            });
            }
            else if($(this).is(":not(:checked)")){
            	$('#input-vat').off('change');
            	$('input[name=\'rate_2\']').val(oldvalue2);
            	$('input[name=\'inc_rate_2\']').val(oldvalue2);
                
            }
        });
    });

$(document).ready(function(){
	    oldvalue3 = $("#rate_3").val();
        $('#gst_incu').click(function(){
            if($(this).is(":checked")){
            					test = $("#test").val();
								rate_3= $("#rate_3").val();
			               		rate_value = rate_3 * test / 100;
			               		rate_3_val = rate_3 -  rate_value;
			               		$('input[name=\'inc_rate_3\']').val(rate_3_val);
            	$('#input-vat').change(function(){
        			  sub_category_id = $('#input-vat option:selected').val();
					  $('#input-vat').val(sub_category_id);
					  category = $('#input-vat').val();
						$.ajax({
							type: "POST",
					  		url: 'index.php?route=catalog/item/gettaxs&token=<?php echo $token; ?>&filter_subcategory_id=' +  encodeURIComponent(category),
					  		dataType: 'json',
					  		data: {"data":"check"},
					  		success: function(data) {  
					  		    $("#rate_3").val(oldvalue3); 
						  		var test = data.taxs;
								$('#test').val(test.tax_value);
								test = $("#test").val();
								rate_3= $("#rate_3").val();
			               		rate_value = rate_3 * test / 100;
			               		rate_3_val = rate_3 -  rate_value;
			               		console.log(rate_3_val);
			              		$('input[name=\'inc_rate_3\']').val(rate_3_val);
					  		}
						});
	            });
            }
            else if($(this).is(":not(:checked)")){
            	$('#input-vat').off('change');
            	$('input[name=\'rate_3\']').val(oldvalue3);
            	$('input[name=\'inc_rate_3\']').val(oldvalue3);
                
            }
        });
    });
$(document).ready(function(){
	    oldvalue4 = $("#rate_4").val();
        $('#gst_incu').click(function(){
            if($(this).is(":checked")){
            					test = $("#test").val();
								rate_4= $("#rate_4").val();
			               		rate_value = rate_4 * test / 100;
			               		rate_4_val = rate_4 -  rate_value;
			               		$('input[name=\'inc_rate_4\']').val(rate_4_val);
            	$('#input-vat').change(function(){
        			  sub_category_id = $('#input-vat option:selected').val();
					  $('#input-vat').val(sub_category_id);
					  category = $('#input-vat').val();
						$.ajax({
							type: "POST",
					  		url: 'index.php?route=catalog/item/gettaxs&token=<?php echo $token; ?>&filter_subcategory_id=' +  encodeURIComponent(category),
					  		dataType: 'json',
					  		data: {"data":"check"},
					  		success: function(data) {  
					  		    $("#rate_4").val(oldvalue4); 
						  		var test = data.taxs;
								$('#test').val(test.tax_value);
								test = $("#test").val();
								rate_4= $("#rate_4").val();
			               		rate_value = rate_4 * test / 100;
			               		rate_4_val = rate_4 -  rate_value;
			               		console.log(rate_4_val);
			              		$('input[name=\'inc_rate_4\']').val(rate_4_val);
					  		}
						});
	            });
            }
            else if($(this).is(":not(:checked)")){
            	$('#input-vat').off('change');
            	$('input[name=\'rate_4\']').val(oldvalue4);
            	$('input[name=\'inc_rate_4\']').val(oldvalue4);
                
            }
        });
    });
 
 $(document).ready(function(){
	    oldvalue5 = $("#rate_5").val();
        $('#gst_incu').click(function(){
            if($(this).is(":checked")){
            					test = $("#test").val();
								rate_5= $("#rate_5").val();
			               		rate_value = rate_5 * test / 100;
			               		rate_5_val = rate_5 -  rate_value;
			               		$('input[name=\'inc_rate_5\']').val(rate_5_val);
            	$('#input-vat').change(function(){
        			  sub_category_id = $('#input-vat option:selected').val();
					  $('#input-vat').val(sub_category_id);
					  category = $('#input-vat').val();
						$.ajax({
							type: "POST",
					  		url: 'index.php?route=catalog/item/gettaxs&token=<?php echo $token; ?>&filter_subcategory_id=' +  encodeURIComponent(category),
					  		dataType: 'json',
					  		data: {"data":"check"},
					  		success: function(data) {  
					  		    $("#rate_5").val(oldvalue5); 
						  		var test = data.taxs;
								$('#test').val(test.tax_value);
								test = $("#test").val();
								rate_5= $("#rate_5").val();
			               		rate_value = rate_5 * test / 100;
			               		rate_5_val = rate_5 -  rate_value;
			               		console.log(rate_5_val);
			              		$('input[name=\'inc_rate_5\']').val(rate_5_val);
					  		}
						});
	            });
            }
            else if($(this).is(":not(:checked)")){
            	$('#input-vat').off('change');
            	$('input[name=\'rate_5\']').val(oldvalue5);
            	$('input[name=\'inc_rate_5\']').val(oldvalue5);
                
            }
        });
    });
$(document).ready(function(){
	    oldvalue6 = $("#rate_6").val();
        $('#gst_incu').click(function(){
            if($(this).is(":checked")){
            					test = $("#test").val();
								rate_6= $("#rate_6").val();
			               		rate_value = rate_6 * test / 100;
			               		rate_6_val = rate_6 -  rate_value;
			               		$('input[name=\'inc_rate_6\']').val(rate_6_val);

            	$('#input-vat').change(function(){
        			  sub_category_id = $('#input-vat option:selected').val();
					  $('#input-vat').val(sub_category_id);
					  category = $('#input-vat').val();
						$.ajax({
							type: "POST",
					  		url: 'index.php?route=catalog/item/gettaxs&token=<?php echo $token; ?>&filter_subcategory_id=' +  encodeURIComponent(category),
					  		dataType: 'json',
					  		data: {"data":"check"},
					  		success: function(data) {  
					  		    $("#rate_6").val(oldvalue6); 
						  		var test = data.taxs;
								$('#test').val(test.tax_value);
								test = $("#test").val();
								rate_6= $("#rate_6").val();
			               		rate_value = rate_6 * test / 100;
			               		rate_6_val = rate_6 -  rate_value;
			               		console.log(rate_6_val);
			              		$('input[name=\'inc_rate_6\']').val(rate_6_val);
					  		}
						});
	            });
            }
            else if($(this).is(":not(:checked)")){
            	$('#input-vat').off('change');
            	$('input[name=\'rate_6\']').val(oldvalue6);
            	$('input[name=\'inc_rate_6\']').val(oldvalue6);
                
            }
        });
    });
*/

//--></script></div> 
<?php echo $footer; ?>