<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
			   <a style = "display:none" href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
			   <h1><?php echo $heading_title; ?></h1>
			   <ul class="breadcrumb">
				   <?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
					<?php } ?>
			   </ul>
		</div>
  </div>
  <div class="container-fluid">
	   <?php if ($error_warning) { ?>
	   <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
	   </div>
		<?php } ?>
	   <div class="panel panel-default">
		   <!-- <div class="panel-heading">
			   <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
			</div> -->
	   <div class="panel-body">
		   <form class="form-horizontal">
				<div class="form-group required">
					<label class="col-sm-3 control-label"><?php echo 'Printer'; ?></label>
					<div class="col-sm-3">
						<select onchange="printers(this)" id="printer" name="printer" class="form-control">
							<option value="">Printer Type</option>
							<option value="1">Network</option>
							<option value="2">Windows</option>
						</select>
						<div style="display: none;" class="error_print text-danger">Please Select Printer</div>
					</div>
				</div>
				<div class="form-group required">
					<label class="col-sm-3 control-label"><?php echo 'IP'; ?></label>
					<div class="col-sm-3">
						<select name="ip" id="ip" class="form-control" >
							<option value="">Select Printer</option>
						</select>
						<!-- <input id="ip" type="text" name="ip" value="<?php echo $ip; ?>" placeholder="<?php echo "IP"; ?>" class="form-control" /> -->
						<div style="display: none;" class="error_ip text-danger">Please Enter IP</div>
					</div>
				</div>
				<div class="form-group required">
					<label class="col-sm-3 control-label"><?php echo 'Message'; ?></label>
					<div class="col-sm-3">
						<input id="msg" type="text" name="msg" value="<?php echo $msg; ?>" placeholder="<?php echo "Message"; ?>" class="form-control" />
						<div style="display: none;" class="error_ip text-danger">Please Enter Message</div>
					</div>
				</div>
				<div class="col-sm-12">
					<br>
					<center>
						<a onclick="test_print()" data-toggle="tooltip" title="Print" class="btn btn-primary">Print</a>
					</center>
				</div>
		  	</form>
		</div>
	  </div>
	</div>
  </div>
 <script>

 	function printers(type) {
 		var ptype = '';
 		if (type.value == 1) {
 			ptype = 'Network';
 		} else if (type.value == 2) {
 			ptype = 'Windows';
 		}
		var html = '<option value="">Select Printer</option>';
 		if (type.value == '') {
 			$("#ip").html('');
			$("#ip").html(html);
 			return false;
 		}
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/testprint/getprinters&token=<?php echo $token; ?>&ptype='+ptype,
			dataType: 'json',
			success: function(data){
				if (data.success == 1) {
					$.each(data.uniq_user_printers, function (i, item) {
						html += '<option value="'+item.printer_name+'">'+item.printer_name+'</option>'
					});
					$("#ip").html('');
					$("#ip").html(html);
				} else {
					alert('No Printers found!');
					$("#ip").html('');
					$("#ip").html(html);
				}
			}
		});
 	}

 	function printers2(type) {
 		var ptype = '';
 		if (type == 1) {
 			ptype = 'Network';
 		} else if (type == 2) {
 			ptype = 'Windows';
 		}
		var html = '<option value="">Select Printer</option>';
 		if (type == '') {
 			$("#ip").html('');
			$("#ip").html(html);
 			return false;
 		}
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/testprint/getprinters&token=<?php echo $token; ?>&ptype='+ptype,
			dataType: 'json',
			success: function(data){
				if (data.success == 1) {
					$.each(data.uniq_user_printers, function (i, item) {
						html += '<option value="'+item.printer_name+'">'+item.printer_name+'</option>'
					});
					$("#ip").html('');
					$("#ip").html(html);
				} else {
					alert('No Printers found!');
					$("#ip").html('');
					$("#ip").html(html);
				}
			}
		});
 	}

  	function test_print() {
  		var printer = $("#printer").val();
  		var ip = $("#ip").val();
  		var msg = $("#msg").val();
  		if (printer == '') {
  			alert("Please select printer!");
  			return false;
  		}
  		if (ip == '') {
  			alert("Please select ip!");
  			return false;
  		}
  		if (msg == '') {
  			alert("Please select msg!");
  			return false;
  		}

  		if (printer != '' && ip != '' && msg != '') {
  			$.ajax({
				type: "POST",
				url: 'index.php?route=catalog/testprint/print_test&token=<?php echo $token; ?>&printer='+printer+'&ip='+ip+'&msg='+msg,
				dataType: 'json',
				success: function(data){
					if (data.print_success == 1) {
						alert('Test print sent successfully!');
						location.reload();
					} else {
						alert('Somethingh went wrong!');
					}
				}
			});
  		}
  	}

  	$(document).ready(function() {
  		$("#printer").val(2);
  		$("#msg").val("pickup ");
  		printers2(2);
  	});

  	$(document).on('keypress','#msg', function (e) {
  		if (e.which == 13) {
  			test_print();
  		}
  	});
</script>
</div> 

<?php echo $footer; ?>