<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
	    	<?php if ($success) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				   <button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
		      <h1><?php echo "Online Food Reject Report" ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo "Online Food Reject Report"; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-3">
						       		<div class="form-row">
									    <div class="col-sm-3">
									    	<label>Start Date</label>
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<label>End Date</label>
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									    <div class="col-sm-3">
								   		<label>Order From</label>
								    	<select class="form-control" name="filter_type">
								    		<option value='' selected="selected"><?php echo "All" ?></option>
								    		<?php foreach($types as $key => $value) { ?>
								    			<?php if($key == $type) { ?>
								    				<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
								    			<?php } else { ?>
								    				<option value="<?php echo $key ?>"><?php echo $value?></option>
								    			<?php } ?>
								    		<?php } ?>
								    	</select>
								    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
					
				 	<div class="col-sm-offset-10" style="display: none;">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
						<a id="sendmail" data-toggle="tooltip" title="<?php echo "Send Mail"; ?>" style="width: 70px;height: 34px;" class="btn btn-primary "><i class="fa fa-envelope "></i></a>

				 		<button id="export" type="button" class="btn btn-primary">Export</button>
				 	</div>
					<div class="col-sm-10 col-sm-offset-1">
					<!-- <h4>
					<center>
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center><h4> -->
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Online Food Reject Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  	<table class="table table-bordered table-hover" style="text-align: center;">
							<tr>
								<th style="text-align: center;">Sr No</th>
								<th style="text-align: center;">Date</th>
								<th style="text-align: center;">Order No</th>
								<th style="text-align: center;">Grand Total </th>
								<th style="text-align: center;">Order From</th>
								<th style="text-align: center;">Payment Type</th>
								<th style="text-align: center;">Reject Reason</th>


							</tr>
							<?php if(isset($_POST['submit'])){ ?>
								<?php $discount = 0; $grandtotal = 0; $vat = 0; $gst = 0; $foodtotal = 0; $fdistotal = 0; $liqtotal = 0;$ldistotal = 0; $discountvalue = 0; $afterdiscount = 0; $stax = 0; $roundoff = 0; $total = 0; $advance = 0;?>
							  	<?php foreach ($billdatas as $key => $value) {?>
							  		<tr>
							  			<td colspan="7"><b><?php echo $key ?><b></td>
							  		</tr>
							  		<?php $sr_no = 1; ?>
								  	<?php foreach ($value as $data) {?>
								  		<tr>
								  			<td><?php echo $sr_no ?></td>
								  			<td><?php echo $data['order_date_time'] ?></td>
								  			<td><?php echo $data['order_id'] ?></td>
								  			<td><?php echo round($data['gross_amount']) ?></td>
								  			<td><?php echo $data['order_from'] ?></td>
								  			<td><?php echo $data['payment_mode'] ?></td>
								  			<?php if($data['status'] == 1){ ?>
								  				<td><?php echo "Items out of stock." ?></td>
								  			<?php } elseif ($data['status'] == 2) { ?>
								  				<td><?php echo "No delivery boys available." ?></td>
								  				
								  			<?php } elseif ($data['status'] == 3) { ?>
								  				<td><?php echo "Nearing closing time" ?></td>
								  				
								  			<?php } elseif ($data['status'] == 4) { ?>
								  				<td><?php echo "Out of Subzone/Area" ?></td>

								  			<?php } elseif ($data['status'] == 5) { ?>
								  				<td><?php echo "Kitchen is Full" ?></td>
								  			<?php } ?>
								  			
								  		</tr>
								  	<?php $sr_no++; } ?>
								<?php } ?>
							<?php } ?>
					  	</table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/reportbill/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}


		$('#export').on('click', function() {
		  var url = 'index.php?route=catalog/reportbill/export&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		$('#sendmail').on('click', function() {
		  var url = 'index.php?route=catalog/reportbill/sendmail&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>

</div>
<?php echo $footer; ?>