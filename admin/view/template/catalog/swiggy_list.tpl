<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
    	<?php if ($warning) { //echo "<pre>";print_r($warning);exit;?>
			  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			  </div>
			<?php } ?>
	    <div class="container-fluid">
	        <div style="display: none;" class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" 
	             class="btn btn-primary"><i class="fa fa-plus"></i></a>
		         <button style="display: none;"> type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
	        </div>
	        <h1><?php echo $heading_title; ?></h1>
	        <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
	        </ul>
	    </div>
  	</div>

	<div class="form-popup-bg" style="font-family: cursive; font-size: 22px;">
    <div class="form-container">
      <button id="btnCloseForm" class="close-button">X</button>
      <h1>Item List</h1><br>
      	<table id="item_datas">
            <tbody>
            </tbody>
        </table> 
    </div>
  </div>

	<div class="panel-body">
		<div class="well">
		    <div class="row">
    			<div class="col-sm-3">
          	<label class="control-label" for="input-name" style="font-size: 18px;"><?php echo 'Date'; ?></label>
          	<input type="date" name='filter_date' value="<?php echo $filter_date?>" class="form-control form_datetime" placeholder="">
      		</div>
					<div class="col-sm-2">
              <label class="control-label" for="input-status" style="font-size: 18px;"><?php echo 'Partner'; ?></label>
              <select name="filter_partner" id="filter_partner" class="form-control">
                	<option  value=""><?php echo "ALL" ?></option>
                	<?php if ($filter_partner == '1') { ?>
                  	<option value="1" selected="selected">Swiggy</option>
                  	<option value="2">Zomato</option>
                	<?php } else if ($filter_partner == '2') { ?>
                  	<option value="1">Swiggy</option>
                  	<option value="2" selected="selected">Zomato</option>
                	<?php } else { ?>
                  	<option value="1">Swiggy</option>
                  	<option value="2">Zomato</option>
                	<?php } ?>
              </select>
          </div>
          <div class="col-sm-2">
              <label class="control-label" for="input-status" style="font-size: 18px;"><?php echo 'Status'; ?></label>
              <select name="order_status" id="order_status" class="form-control">
                	<?php if ($order_status == '0') { ?>
                  	<option value="0" selected="selected">Panding</option>
                  	<option value="2">All</option>
                  	<option value="1">Done</option>
                	<?php } else if ($order_status == '1') { ?>
                  	<option value="0">Panding</option>
                  	<option value="2">All</option>
                  	<option value="1" selected="selected">Done</option>
                	<?php } else { ?>
                  	<option value="0">Panding</option>
                  	<option value="2" selected>All</option>
                  	<option value="1">Done</option>
                	<?php } ?>
              </select>
          </div>           
	    		<div class="col-sm-1">
	        	<button type="button" id="button-filter" class="btn btn-primary "><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
	    		</div>
			    <div class="col-sm-1">
			    	<button type="button" id="button-filter" class="btn btn-primary pull-right"><?php echo 'PULL DATA'; ?></button>
					</div>
			 		<div class="col-sm-2">
			    	<button type="button" id="button-filter" class="btn btn-primary pull-right"> <?php echo 'Pull Data Without Print'; ?></button>
					</div>
		    </div>
		</div>
	  <div class="table-responsive">
			<table class="table table-bordered table-hover">
			  	<thead>
					<tr>
					 	<td class="text-left" style="font-size: 18px;"><?php  echo'Sr.No'; ?></td>
					 	<td class="text-left" style="font-size: 18px;"><?php  echo'Order No'; ?></td>
					 	<td class="text-left" style="font-size: 18px;"><?php  echo'Name '; ?></td>
					 	<td class="text-left" style="font-size: 18px;"><?php  echo'Date '; ?></td>
					 	<td class="text-left" style="font-size: 18px;"><?php  echo'Time '; ?></td>
					 	<td class="text-left" style="font-size: 18px;"><?php  echo'Delivery Partner '; ?></td>
					  <td class="text-left" style="font-size: 18px;"><?php echo'Amount '; ?></td>
					  <td class="text-center" style="font-size: 18px;"><?php echo'Action '; ?></td>
					</tr>
		  		</thead>
		  	<tbody>
					<?php $i=1; foreach($orderarray as $test => $value) { //echo "<pre>";print_r($value);exit;
							$orgDate = $value['date'];  
	    				$newDate = date("d-m-Y", strtotime($orgDate)); 
						?>
					<tr>
					  	<td class="" ><?php echo $i++?></td>
					  	<td class="text-left" style="font-size: 18px;" onclick="btnOpenForm(<?php echo $value['order_id']?>)"><?php echo $value['order_id']?></td>
					  	<td class="text-left" style="font-size: 18px;" onclick="btnOpenForm(<?php echo $value['order_id']?>)"><?php echo $value['cust_name']?></td>
					  	<td class="text-left" style="font-size: 18px;"onclick="btnOpenForm(<?php echo $value['order_id']?>)"><?php echo $newDate?></td>
					  	<td class="text-left" style="font-size: 18px;" onclick="btnOpenForm(<?php echo $value['order_id']?>)"><?php echo $value['time']?></td>
					  	<td class="text-left" style="font-size: 18px;" onclick="btnOpenForm(<?php echo $value['order_id']?>)"><?php echo $value['delivery_partner']?></td>
					  	<td class="text-left" style="font-size: 18px;" onclick="btnOpenForm(<?php echo $value['order_id']?>)"><?php echo $value['totals']?></td>
					  	<td class="text-center" >
					  		<?php if($value['kot_status'] == '0'){ ?>
					  			<button name="kot_btn" onclick="kot_fun(<?php echo $value['order_id']?>)" class="btn btn-danger"><b>BILL</b></button>&nbsp;
					  		<?php } else { ?>
					  			<!-- <button class="btn btn-success"><b>KOT ALREADY DONE</b></button>&nbsp; -->
						  		<button name="bill_btn" onclick="bill_fun(<?php echo $value['order_id']?>)" class="btn btn-primary"><b> Duplicate BILL </b></button>
					  		<?php } ?>
					  	</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="row">
		    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	  </div>
	</div>
<script type="text/javascript">
	function kot_fun(online_order_id) {
		$.ajax({
			type: "POST",
		 	url: 'index.php?route=catalog/swiggy/prints&token=<?php echo $token; ?>&online_order_id=' + online_order_id,
		 	dataType: 'json',
		 	success: function(json) {
	  		window.location.reload();
	  	}
		})
	}
</script>

<script type="text/javascript">
	function bill_fun(online_order_id) {
		// alert(online_order_id);
		$.ajax({
			type: "POST",
		 	url: 'index.php?route=catalog/swiggy/printsb&token=<?php echo $token; ?>&online_order_id=' + online_order_id,
		 	dataType: 'json',
		 	success: function(json) {
		 		// alert(json);
	  		window.location.reload();
	  	}
		})
	}
</script>

<script type="text/javascript">
$('#button-filter').on('click', function() {
  var url = 'index.php?route=catalog/swiggy&token=<?php echo $token; ?>';

  // var filter_partner = $('select[name=\'filter_partner\']').val();
  var filter_partner = $('select[name=\'filter_partner\']').val();
  var order_status = $('select[name=\'order_status\']').val();
  if (filter_partner) {
	  url += '&filter_partner=' + encodeURIComponent(filter_partner);
  }

  if (order_status) {
	  url += '&order_status=' + encodeURIComponent(order_status);
  }
  // alert(url);
  location = url;
});
</script>

<script type="text/javascript">
	function closeForm() {
  $('.form-popup-bg').removeClass('is-visible');
}

function btnOpenForm(order_id) {
      // alert(order_id);
      // alert(question_id);
    $(document).ready(function() {
        $.ajax({
          url: 'index.php?route=catalog/swiggy/order_itemss&token=<?php echo $token; ?>&order_id=' + order_id,
          dataType: 'json',
          success: function(teamls) {
          	console.log(teamls);
              if (teamls.length != 0) {
                  $('#item_datas tbody').html(teamls.html);
              }
          }
        });
    });
    $('.form-popup-bg').addClass('is-visible');
    
}

$(document).ready(function($) {
  
  /* Contact Form Interactions */


  $('#btnOpenForm').on('click', function(event) {
    event.preventDefault();

    $('.form-popup-bg').addClass('is-visible');
  });
  
    //close popup when clicking x or off popup
  $('.form-popup-bg').on('click', function(event) {
    if ($(event.target).is('.form-popup-bg') || $(event.target).is('#btnCloseForm')) {
      event.preventDefault();
      $(this).removeClass('is-visible');
    }
  });
  
});
</script>

</div>
<script type="text/javascript">
$('.date').datetimepicker({
  pickTime: false
});
</script>
<?php echo $footer; ?>