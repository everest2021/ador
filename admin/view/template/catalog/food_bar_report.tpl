<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" id="form" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-13">
					       		<center><h4><b>Select Data</b></h4></center><br>
					       		<div class="form-row">
								    <div class="col-sm-2 col-sm-offset-4">
								    	<label>Start Date</label>
								     	<input type="text" name='filter_startdate' value="<?php echo $filter_startdate?>" class="form-control form_datetime" placeholder="Start Date">
								    </div>
								    <div class="col-sm-2">
								    	<label>End Date</label>
								    	<input type="text" name='filter_enddate' value="<?php echo $filter_enddate?>" class="form-control form_datetime" placeholder="End Date">
								    </div>
								   <div style="display:none;" class="col-sm-2">
								       	<label>Item Name</label>
									    <input type="text" id="filter_itemname" name="filter_itemname" value="<?php echo $filter_itemname ?>" class="form-control" placeholder="Item name">
									    <input type="hidden" id="filter_itemid" name="filter_itemid" value="<?php echo $filter_itemid ?>" class="form-control">
									</div>
									<div style="display:none;" class="col-sm-3">
								    	<label>Recipe Store</label>
								     	<select name="store" id="store" class="form-control" style="width: 40%">
								     		<option value="">All</option>
								     		<?php foreach($recipestores as $store) { ?>
								     			<?php if($store['id'] == $store_id) { ?>
								     				<option value = "<?php echo $store['id'] ?>" selected="selected"><?php echo $store['store_name'] ?></option>
								     			<?php } else { ?>
								     				<option value = "<?php echo $store['id'] ?>" ><?php echo $store['store_name'] ?></option>
								     			<?php } ?>
								     		<?php } ?>
								     	</select>
								    </div>
									<div class="col-sm-12">
										<br>
										<center><a id ="filter" class="btn btn-primary" value="Show">Submit</a>
											<a id ="print" type="button" class="btn btn-primary" >Print</a>
										<a id="export" type="button" class="btn btn-primary">Export</a></center>

									</div>
								</div>
							</div>
					    </div>
					</div>
				 	<!-- <div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 	</div> -->
					<div class="col-sm-6 col-sm-offset-3">
						<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
						<?php date_default_timezone_set("Asia/Kolkata");?>
						<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
						<center><h3><b>Bar And Food Report</b></h3></center>
						<h3>From : <?php echo $filter_startdate; ?></h3>
						<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $filter_enddate; ?></h3><br>
						<?php if ($final_data) {?>
					  	<table class="table table-bordered table-hover" style="text-align: center;">
							<thead>
								<tr>
									<td style="text-align: left;"><?php echo 'Date'; ?></td>
									<td style="text-align: left;"><?php echo 'Amount'; ?></td>
									<td style="text-align: left;"><?php echo 'Discount'; ?></td>
									<td style="text-align: left;"><?php echo 'Net'; ?></td>		            
									<td style="text-align: left;"><?php echo 'GST'; ?></td>
									<td style="text-align: left;"><?php echo 'Total'; ?></td>
									<td style="text-align: left;"><?php echo 'Bill No.'; ?></td>
								</tr>
							</thead>
							<tbody>
								<tr>
										<td colspan="7" style="text-align: left;  "><strong><?php echo 'FOOD'; ?></strong></td>
									</tr>
								<?php foreach($final_data['food_datas'] as $result) { ?>

									<tr>			          
										<td class="left">
											<?php echo $result['date']; ?>
										</td>
										<td class="left">
											<?php echo $result['amount']; ?>
										</td>
										
										<td class="left">
											<?php echo $result['discount']; ?>
										</td>
										<td class="left">
											<?php echo $result['net']; ?>
										</td>
										<td class="left">
											<?php echo $result['gst']; ?>
										</td>
										<td class="left">
											<?php echo $result['gtotal']; ?>
										</td>
										<td class="left">
											<?php echo $result['billno']; ?>
										</td>
										
									</tr>
								<?php } ?>
								<tr>
										<td colspan="7" style="text-align: left;  "><strong><?php echo 'BAR'; ?></strong></td>
									</tr>
								<?php foreach($final_data['liq_datas'] as $result) { ?>

									<tr>			          
										<td class="left">
											<?php echo $result['date']; ?>
										</td>
										<td class="left">
											<?php echo $result['amount']; ?>
										</td>
										
										<td class="left">
											<?php echo $result['discount']; ?>
										</td>
										<td class="left">
											<?php echo $result['net']; ?>
										</td>
										<td class="left">
											<?php echo $result['vat']; ?>
										</td>
										<td class="left">
											<?php echo $result['gtotal']; ?>
										</td>
										<td class="left">
											<?php echo $result['billno']; ?>
										</td>
										
									</tr>
								<?php } ?>

									<tr style="border-top:2px solid #000000;padding-top:#000000;">
										<td colspan="<?php echo 5+$storecount['total'] ?>" style="border-bottom:2px solid #000000;padding-bottom:#000000;" ></td>
									</tr>
							</tbody>
						</table>
						<?php }  ?>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker({
			dateFormat: 'dd-mm-yy',			
	  	});

		$('#filter').on('click', function() {
		  	var url = 'index.php?route=catalog/food_bar_report&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  	var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  	var filter_itemname = $('input[name=\'filter_itemname\']').val();
		 	var filter_itemid = $('input[name=\'filter_itemid\']').val();
		 	var store = $('#store').val();
			if (filter_startdate) {
				url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}
			if (filter_enddate) {
				url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  	}
		   	if (filter_itemname) {
				if (filter_itemid) {
				  	url += '&filter_itemid=' + encodeURIComponent(filter_itemid);
					url += '&filter_itemname=' + encodeURIComponent(filter_itemname);
			  	}
		  	}
		  	if (store) {
				url += '&store=' + encodeURIComponent(store);
		  	}
		  	location = url;
		});

		function close_fun_1(){
			window.location.reload();
		}

		$('#export').on('click', function() {
		  	var url = 'index.php?route=catalog/food_bar_report/export&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  	var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  	var filter_itemname = $('input[name=\'filter_itemname\']').val();
		 	var filter_itemid = $('input[name=\'filter_itemid\']').val();
			if (filter_startdate) {
				url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}
			if (filter_enddate) {
				url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  	}
		   	if (filter_itemname) {
				if (filter_itemid) {
				  	url += '&filter_itemid=' + encodeURIComponent(filter_itemid);
					url += '&filter_itemname=' + encodeURIComponent(filter_itemname);
			  	}
		  	}
		  	location = url;
		});


		$('#print').on('click', function() {
		  	var url = 'index.php?route=catalog/food_bar_report/prints&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  	var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  	var filter_itemname = $('input[name=\'filter_itemname\']').val();
		 	var filter_itemid = $('input[name=\'filter_itemid\']').val();
			if (filter_startdate) {
				url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}
			if (filter_enddate) {
				url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  	}
		   	if (filter_itemname) {
				if (filter_itemid) {
				  	url += '&filter_itemid=' + encodeURIComponent(filter_itemid);
					url += '&filter_itemname=' + encodeURIComponent(filter_itemname);
			  	}
		  	}
		  	location = url;
		});

		
		$('#type').on('change', function() {
			$('#form').submit();
		});

		$('#filter_itemname').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
				$.ajax({
			  		url: 'index.php?route=catalog/itemwisereport/name&token=<?php echo $token; ?>&filter_itemname=' +  encodeURIComponent(request.term),
			  		dataType: 'json',
			  		success: function(json) {   
						response($.map(json, function(item) {
				  			return {
								label: item.name,
								value: item.item_id,
				  			}
						}));
			  		}
				});
		  	}, 
		  	select: function(event, ui) {
		  		$('#filter_itemid').val(ui.item.value);
		  		$('#filter_itemname').val(ui.item.label);
		  		return false;
		  	}
		});
	</script>

	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>