<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" id="form" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Data</b></h4></center><br>
					       		<div class="form-row">
								    <div class="col-sm-2 col-sm-offset-3">
								    	<label>Start Date</label>
								     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
								    </div>
								    <div class="col-sm-2">
								    	<label>End Date</label>
								    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
								    </div>
								    <div class="col-sm-2">
								    	<label>Select Store</label>
								    	<select class="form-control" name="filter_storename">
										    	<option value='' selected="selected"><?php echo "All" ?></option>
									    		<?php foreach($storenames as $key => $value) { ?>
										    		<?php if($value['id'] == $storename ) { ?>
										    			<option value="<?php echo $value['id'] ?>" selected="selected"><?php echo $value['store_name']?></option>
										    		<?php } else { ?>
										    			<option value="<?php echo $value['id'] ?>"><?php echo $value['store_name']?></option>
										    		<?php } ?>
									    		<?php } ?>
									    </select>
								    </div>
									<div class="col-sm-12">
										<br>
										<center><input type="submit" class="btn btn-primary" value="Show">
										<button id="export" type="button" class="btn btn-primary">Export HTML</button>	
								 		<button id="exportexcel" type="button" class="btn btn-primary">Export EXCEL</button></center>
									</div>
								</div>
							</div>
					    </div>
					</div>
				 	<!-- <div class="col-sm-offset-10">
				 	</div> -->
					<div class="col-sm-12">
						<center style = "display:none;">
							<?php echo HOTEL_NAME ?>
							<?php echo HOTEL_ADD ?>
						</center>
						<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
						<?php date_default_timezone_set("Asia/Kolkata");?>
						<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
						<center><h3><b>Purchase Report Loose</b></h3></center>
						<h3>From : <?php echo $startdate; ?></h3>
						<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
						<div style="overflow-x: auto;">
						<table class="table table-bordered table-hover" style="text-align: center;">
							<tr>
								<th style="text-align: center;">Item Name</th>
								<th style="text-align: center;" colspan="13">Opening Stock</th>
								<th style="text-align: center;" colspan="13">Purches Stock</th>
								<th style="text-align: center;" colspan="13">Sale Stock</th>
								<th style="text-align: center;" colspan="13">Closing Qty</th>
							</tr>
							<?php if($liquor_type){ //echo "<pre>";print_r($liquor_type);exit; ?>
								<?php foreach($liquor_type as $lkey => $lvalue) { ?>
									<tr>
										<th style="text-align: left;" colspan="55"><?php echo $lvalue['type']; ?></th>
									</tr>
									<tr>
										<th style="text-align: center;">Brand Name</th>
										<?php foreach($size[$lvalue['type']] as $szkey => $szvalue){ ?>
											<td><?php echo $szvalue['size'];?></td>
										<?php } ?>
										<?php foreach($size[$lvalue['type']] as $szkey => $szvalue){ //echo "<pre>";print_r($size[$lvalue['type']]);exit; ?>
											<td><?php echo $szvalue['size'];?></td>
										<?php } ?>
										<?php foreach($size[$lvalue['type']] as $szkey => $szvalue){ ?>
											<td><?php echo $szvalue['size'];?></td>
										<?php } ?>
										<?php foreach($size[$lvalue['type']] as $szkey => $szvalue){ ?>
											<td><?php echo $szvalue['size'];?></td>
										<?php } ?>
									</tr>
									<?php foreach($liquor_name[$lvalue['type']] as $lnkey => $lnvalue){ //echo "<pre>";print_r($liquor_name);exit;?>
										<tr>

											<td><?php echo $lnvalue['brand'] ?></td>
											<?php foreach($brand as $bkey => $bvalue) {  ?>
												<?php foreach ($bvalue['opening_qtys'] as $okey => $ovalue) { //echo "<pre>";print_r($brand);exit; ?>
													<?php if($bkey == $lnvalue['brand']){ ?>
														<td><?php echo $ovalue['closing_balance'] ?></td>
													<?php } ?>
												<?php }?>
											<?php } ?>
											<?php foreach($brand as $bkey => $bvalue) {  ?>
												<?php foreach ($bvalue['pqty'] as $pkey => $pvalue) { //echo "<pre>";print_r($brand);exit; ?>
													<?php if($bkey == $lnvalue['brand']){ ?>
														<?php if($pvalue['sqty'] !=''){ ?>
															<td><?php echo $pvalue['sqty'] ?></td>
														<?php } else { ?>
															<td><?php echo "0.00";?></td>
														<?php } ?>																														
													<?php } ?>
												<?php }?>
											<?php } ?>
											<?php foreach($brand as $bkey => $bvalue) {  ?>
												<?php foreach ($bvalue['sale_qtys'] as $skey => $svalue) { //echo "<pre>";print_r($svalue);exit; ?>
													<?php if($bkey == $lnvalue['brand']){ ?>
														<?php if($svalue['sale_qty'] !=''){ ?>
															<td><?php echo $svalue['sale_qty'] ?></td>
														<?php } else { ?>	
															<td><?php echo "0.00";?></td>
														<?php } ?>															
													<?php } ?>
												<?php }?>
											<?php } ?>
											<?php foreach($brand as $bkey => $bvalue) {  ?>
												<?php foreach ($bvalue['closing_qtys'] as $ckey => $cvalue) { //echo "<pre>";print_r($cvalue);exit; ?>
													<?php if($bkey == $lnvalue['brand']){ ?>
														<?php if($cvalue['closing_bal'] !=''){ ?>
															<td><?php echo $cvalue['closing_bal'] ?></td>
														<?php } else { ?>	
															<td><?php echo "0.00";?></td>
														<?php } ?>															
													<?php } ?>
												<?php }?>
											<?php } ?>
											
										</tr>
									<!-- <?php //return false; ?> -->
									<?php } ?>	
								<?php } ?>	
							<?php } ?>
		
						</table>
						</div>  
						<br>
						<center><h5><b>End of Report</b></h5></center>
				 	</div>
				</div>
			</form>
		</div>
	</div>
<script type="text/javascript">
$(".form_datetime").datepicker();

$('#export').on('click', function() {
  	var url = 'index.php?route=catalog/complete_liquor_details/export&token=<?php echo $token; ?>';
	var filter_startdate = $('input[name=\'filter_startdate\']').val();
  	var filter_enddate = $('input[name=\'filter_enddate\']').val();
  	var store_id = $('select[name=\'filter_storename\']').val();
  	var type = 'html';
  	
	if (filter_startdate) {
		url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
  	}
	if (filter_enddate) {
		url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
  	}
  	if (store_id) {
	  url += '&store_id=' + encodeURIComponent(store_id);
    }
    url += '&type=' + encodeURIComponent(type);
    console.log(url);
    // return false;
   	
  	location = url;
});


$('#exportexcel').on('click', function() {
  	var url = 'index.php?route=catalog/complete_liquor_details/export&token=<?php echo $token; ?>';
	var filter_startdate = $('input[name=\'filter_startdate\']').val();
  	var filter_enddate = $('input[name=\'filter_enddate\']').val();
  	var store_id = $('select[name=\'filter_storename\']').val();
  	var type = 'html';
  	
	if (filter_startdate) {
		url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
  	}
	if (filter_enddate) {
		url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
  	}
  	if (store_id) {
	  url += '&store_id=' + encodeURIComponent(store_id);
    }
    console.log(url);
    // return false;
   	
  	location = url;
});

$('#print').on('click', function() {
  var url = 'index.php?route=catalog/reportstock/itemwisereport&token=<?php echo $token; ?>';

  var filter_startdate = $('input[name=\'filter_startdate\']').val();
  var filter_enddate = $('input[name=\'filter_enddate\']').val();
  var filter_storename = $('select[name=\'filter_storename\']').val();
  var filter_type = $('select[name=\'filter_type\']').val();

  if (filter_startdate) {
	  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
  }

  if (filter_enddate) {
	  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
  }

  if (filter_storename) {
	  url += '&filter_storename=' + encodeURIComponent(filter_storename);
  }

  location = url;
  setTimeout(close_fun_1, 50);
});

function close_fun_1(){
	window.location.reload();
}
</script>


<style>
	 td,th {
		  font-size: 20px;
		  color: black;
		}

	h3,h4 {
		color: black;
	}
</style>
</div>
<?php echo $footer; ?>