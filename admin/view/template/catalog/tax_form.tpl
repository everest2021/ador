<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right">
		       <button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
		       <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
	           <h1><?php echo $heading_title; ?></h1>
	           <ul class="breadcrumb">
		           <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		            <?php } ?>
	           </ul>
	    </div>
  </div>
  <div class="container-fluid">
	   <?php if ($error_warning) { ?>
	   <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	       <button type="button" class="close" data-dismiss="alert">&times;</button>
	   </div>
	    <?php } ?>
	   <div class="panel panel-default">
	       <div class="panel-heading">
		       <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
	        </div>
	   <div class="panel-body">
		   <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
		    	<div class="form-group ">
		    	<label class="col-sm-3 control-label"><?php echo 'Tax Type'; ?></label>
				   		<div class="col-sm-3" >
				        	<select name="tax_type" id="input-tax_type" class="form-control">
				            	<option ><?php echo "Please Select" ?></option>
					          	<?php foreach($taxs as $skey => $svalue){ ?>
					          		<?php if($skey == $tax_type){ ?>
						      			<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue; ?></option>
					          		<?php } else { ?>
						      			<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
					          		<?php } ?>
					          	<?php } ?>
				        	</select>
				   		</div>
				   		<div class="col-sm-4" style="width: 15%;">
							<?php if($include_in_rates == '1'){ ?>
								<input type="checkbox" name="include_in_rates" value="1" checked ="checked"  style="margin-right:8%">Included In Rates<br>
							<?php } else { ?>
								<input type="checkbox" id="include_in_rates" name="include_in_rates" value="1" style="margin-right:8%;">Included In Rates <br />
							<?php } ?>
				   		</div>
				</div>
				<div class="form-group ">
			    	<label class="col-sm-3 control-label"><?php echo 'Tax Name'; ?></label>
				    <div class="col-sm-3">
				        <input type="text" name="tax_name" value="<?php echo $tax_name; ?>" placeholder="<?php echo 'Tax Name'; ?>" class="form-control" />
			           
				    </div>
				   <label class="col-sm-3 control-label"><?php echo 'Tax Code'; ?></label>
				    <div class="col-sm-3">
				        <input type="text" name="tax_code" value="<?php echo $tax_code; ?>" placeholder="<?php echo 'refer'; ?>" class="form-control" />
			       </div>
		       </div>
		       <div class="form-group ">
			    	<label class="col-sm-3 control-label"><?php echo 'Tax Value'; ?></label>
				    <div class="col-sm-3">
				        <input type="text" name="tax_value" value="<?php echo $tax_value; ?>" placeholder="<?php echo 'Tax Value'; ?>" class="form-control" />
				    </div>
		       </div>
		       
		  </div>
		</form>
	  </div>
	</div>
  </div>
</div> 
<?php echo $footer; ?>
<script type="text/javascript">
	 	$("#date_doj").datepicker();
	</script>