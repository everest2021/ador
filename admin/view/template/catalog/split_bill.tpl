<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;">
  	<div class="page-header">
		<div class="container-fluid">
	  		<div class="pull-right sav">
				<button type="submit" form="form-split_bill" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
	  		<h1><?php echo 'Split Bill'; ?></h1>
		</div>
  	</div>
  	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="alert alert-danger" style="display: none;">
			<i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		  	<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php if ($success) { ?>
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		  </div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
			</div>
		  	<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-split_bill" class="form-horizontal">
			  		<div class="form-group">
						<label class="col-sm-3 control-label" style="width:20%"><?php echo 'Master table No.'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="order_no" id="order_no" value="<?php echo $order_no ?>" class="form-control" />
							<div class="" style="display: none;" id="order_no_error"></div>
						</div>
						<div class="col-sm-2">
							<span id="food_total_span" style="font-weight: bold;"><?php echo $food_total ?></span>
							<input type="hidden" name="food_total" id="food_total" value="<?php echo $food_total ?>" class="form-control" />
							&nbsp;&nbsp;
							<span id="liquor_total_span" style="font-weight: bold;"><?php echo $liquor_total ?></span>
							<input type="hidden" name="liquor_total" id="liquor_total" value="<?php echo $liquor_total ?>" class="form-control" />
							&nbsp;&nbsp;
							<span id="grand_total_span" style="font-weight: bold;"><?php echo $grand_total ?></span>	
							<input type="hidden" name="grand_total" id="grand_total" value="<?php echo $grand_total ?>" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" style=""><?php echo 'Divid in person'; ?></label>
						<div class="col-sm-3">
							<input tabindex="1" type="text" name="order_no_1" id="order_no_1" value="" class="form-control" />
							<div class="" style="display: none;" id="order_no_1_error"></div>
						</div>
						<div class="col-sm-2" style="">
							<span id="Split_1_span" style="font-weight: bold;"><?php echo 'Split Bill Amount in each person' ?></span>	
							<input type="hidden" name="Split_1" id="Split_1" value="<?php echo $Split_1 ?>" class="form-control" />
							&nbsp;&nbsp;
						</div>
			  		</div>
				</form>
		  	</div>
		</div>
  	</div>
</div> 
<script type="text/javascript">
$('#order_no').on('focusout', function() {
  	order_no = $('#order_no').val();
  	console.log(order_no);
  	if(order_no != '' && order_no != '0'){
	  	$.ajax({
			url: 'index.php?route=catalog/split_bill/getorderinfo&token=<?php echo $token; ?>&order_no='+order_no,
			dataType: 'json',
			success: function(data){
				console.log(data);
				if(data.status == 1){
					$('#order_no_error').hide();
					$('#order_no_error').removeClass('text-danger');
					$('#food_total_span').html('Food Total : ' + data.ftotal);
					$('#liquor_total_span').html('Liquor Total : ' + data.ltotal);
					$('#grand_total_span').html('Grand Total : ' + data.grand_total);
					$('#food_total').val(data.ftotal);
					$('#liquor_total').val(data.ltotal);
					$('#grand_total').val(data.grand_total);
				} else {
					$('#order_no_error').addClass('text-danger');
					if(data.status == 2){
						$('#order_no_error').html('Ref Number Already Merged with Another Bill');
					} else {
						$('#order_no_error').html('Ref Number Does not Exist');
					}
					$('#order_no_error').show();
					$('#food_total_span').html('');
					$('#liquor_total_span').html('');
					$('#grand_total_span').html('');
					$('#food_total').val('');
					$('#liquor_total').val('');
					$('#grand_total').val('');
				}
			}
		});
	} else {
		$('#order_no_error').removeClass('text-danger');
		$('#order_no_error').html('');
		$('#order_no_error').hide();
		$('#food_total_span').html('');
		$('#liquor_total_span').html('');
		$('#grand_total_span').html('');
		$('#food_total').val('');
		$('#liquor_total').val('');
		$('#grand_total').val('');	
	}
});

$('#order_no_1').on('focusout', function() {
  	person_no = $('#order_no_1').val();
  	grand_total = $('#grand_total').val();
  	if(order_no != '' && order_no != '0'){
	  	$.ajax({
			url: 'index.php?route=catalog/split_bill/split&token=<?php echo $token; ?>&person='+person_no+'&grand_total='+grand_total,
			dataType: 'json',
			success: function(data){
				console.log(data);
				if(data.status == 1){
					$('#order_no_1_error').hide();
					$('#order_no_1_error').removeClass('text-danger');
					$('#Split_1_span').html('Split Bill Amount in each person : ' + data.divid_amt);
					$('#Split_1').val(data.divid_amt);
				} else {
					$('#order_no_1_error').addClass('text-danger');
					if(data.status == 2){
						$('#order_no_1_error').html('Ref Number Already Merged with Another Bill');
					} else {
						$('#order_no_1_error').html('Ref Number Does not Exist');
					}
					$('#order_no_1_error').show();
					$('#food_total_1_span').html('');
					$('#liquor_total_1_span').html('');
					$('#grand_total_1_span').html('');
					$('#food_total_1').val('');
					$('#liquor_total_1').val('');
					$('#grand_total_1').val('');
				}
			}
		});
	} else {
		$('#order_no_1_error').removeClass('text-danger');
		$('#order_no_1_error').html('');
		$('#order_no_1_error').hide();
		$('#food_total_1_span').html('');
		$('#liquor_total_1_span').html('');
		$('#grand_total_1_span').html('');
		$('#food_total_1').val('');
		$('#liquor_total_1').val('');
		$('#grand_total_1').val('');	
	}
});

$(document).on('keypress','input,select', function (e) {
//$('input,select').on('keypress', function (e) {
    if (e.which == 13) {
        e.preventDefault();
        var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
        if (!$next.length) {
            $next = $('[tabIndex=1]');
        }
        $next.focus();
    }
});

$(document).on('keyup','#save_button', function (e) {
    if (e.keyCode == 33) {
        save();
    }
});

$(document).ready(function() {
  	//order_no = $('#order_no').val();
  	//if(order_no != '' && order_no != '0' && order_no != undefined){
  		$('.sav').html('');
		html = '<a tabindex="4" id="save_button" onclick="save()" title="Update" class="btn btn-primary">Update</a>';
		$('.sav').append(html);
  	//}
  	$('#order_no').focus();
});

function save(){
  	action = '<?php echo $action; ?>';
  	action = action.replace("&amp;", "&");
  	$.post(action, $('#form-split_bill').serialize()).done(function(data) {
    	var parsed = JSON.parse(data);
     	if(parsed.info == 1){
     		$('.sucess').html('');
     		$('.sucess').append(parsed.done);
     	}
	});
}
</script>