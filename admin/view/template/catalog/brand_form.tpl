<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header">
		<div class="container-fluid">
	  		<div class="pull-right">
				<button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
	  		<h1><?php echo $heading_title; ?></h1>
	  		<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
	  		</ul>
		</div>
  	</div>
  	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		    <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		  	</div>
		  	<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-department" class="form-horizontal">
			  		<div class="form-group required">
						<label class="col-sm-3 control-label"><?php echo 'Brand'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="brand" value="<?php echo $brand; ?>" placeholder="<?php echo 'Brand'; ?>" class="form-control" />
				  			<?php if ($error_brand) { ?>
			                <div class="text-danger"><?php echo $error_brand; ?></div>
			                <?php } ?>
						</div>
			   		</div>
			   		<div class="form-group ">
						<label class="col-sm-3 control-label"><?php echo 'SubCategory'; ?></label>
						<div class="col-sm-3">
				  			<select name="subcategory_id" id="input-subcategory_id" class="form-control">
								<?php foreach($item_subcategorys as $skey => $svalue){ ?>
					  				<?php if($skey == $subcategory_id){ ?>
										<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
					  				<?php } else { ?>
										<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
					  				<?php } ?>
								<?php } ?>
				  			</select>
				  			<input type="hidden" name="subcategory" id="subcategory" value="<?php echo $subcategory; ?>" class="form-control" />
						</div>
			  		</div>
			  		<div class="form-group ">
						<label class="col-sm-3 control-label"><?php echo 'Type'; ?></label>
						<div class="col-sm-3">
				  			<select name="type" id="type" class="form-control">
								<?php foreach($types as $skey => $svalue){ ?>
					  				<?php if($skey == $type_id){ ?>
										<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
					  				<?php } else { ?>
										<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
					  				<?php } ?>
								<?php } ?>
				  			</select>
				  			<input type="hidden" name="type_name" id="type_name" value="<?php echo $type; ?>" class="form-control" />
						</div>
			  		</div>
				</form>
		  	</div>
		</div>
  	</div>
</div> 
<script type="text/javascript">

$('#input-subcategory_id').on('change', function() {
  loc_name = $('#input-subcategory_id option:selected').text();
  $('#subcategory').val(loc_name);
 
});

$('#type').on('change', function() {
  type_name = $('#type option:selected').text();
  $('#type_name').val(type_name);
 
});
</script>
<?php echo $footer; ?>