<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
	    	<?php if ($success) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				   <button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Customer Bal. Point</b></h4></center><br>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show">
									<button id="print" type="button" class="btn btn-primary">Print</button></center>
								</div>
							</div>
					    </div>
					</div>
					
				 	
					<div class="col-sm-10 col-sm-offset-1">
					<h4>
					<center>
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center><h4>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Customer Balance Points Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;">ID</th>
							<th style="text-align: center;">Name</th>
							<th style="text-align: center;">Total Points Get</th>
							<th style="text-align: center;">Use Point</th>
							<th style="text-align: center;">Bal. Point</th>
							
						</tr>
						<?php if(isset($_POST['submit'])){ ?>
								<?php foreach($cust_details as $ckey => $cvalue) {//echo'<pre>';print_r($new5);exit;?>
							  		<tr >
								  		<td><?php echo $cvalue['c_id'] ?></td>
								  		<td><?php echo $cvalue['name'] ?></td>
								  		<td><?php echo $new1[$ckey] + $new2[$ckey] + $new5[$ckey]?></td>
								  		<td><?php echo $new3[$ckey] + $new4[$ckey]?></td>
								  		<td><?php echo number_format($new1[$ckey] + $new2[$ckey] + $new5[$ckey],2) - number_format($new3[$ckey] + $new4[$ckey],2) ?></td>
								  	</tr>
									  		
							  	<?php }  ?>
							  	
					  	<?php } ?>
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	

		function close_fun_1(){
			window.location.reload();
		}


		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<script type="text/javascript">
	$('#print').on('click', function() {

		  var url = 'index.php?route=catalog/bal_point/prints&token=<?php echo $token; ?>';

		  //var filter_startdate = $('input[name=\'filter_startdate\']').val();

		  // if (filter_startdate) {
			 //  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  // }

		  location = url;
		  //setTimeout(close_fun_1, 50);
		});

	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>

</div>
<?php echo $footer; ?>