<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;">
	<div class="page-header">
		<div class="container-fluid">
			<div id="overlay">
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>
			</div>
			<div class="pull-right sav">
				<!-- <button id="savsp" type="submit" form="form-bill_merge" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a> -->
			</div>
			<h1><?php echo 'Backup'; ?></h1>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="alert alert-danger" style="display: none;" id="warning_div">
			<i class="fa fa-exclamation-circle"></i> <?php echo 'Unbilled Tables Present'; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<div class="alert alert-danger" style="display: none;" id="warning_div_1">
			<i class="fa fa-exclamation-circle"></i> <?php echo 'Warning: Cannot Close Current Date'; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php if ($success) { ?>
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		  </div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-dayclose" class="form-horizontal">
					<!-- <div class="form-group">
						<label class="col-sm-3 control-label" style=""><?php //echo 'Date to Close'; ?></label>
						<div class="col-sm-3">
							<input tabindex="1" readonly="readonly" type="text" name="open_date" id="open_date" value="<?php echo $open_date ?>" class="form-control" />
							<input type="hidden" name="input_warning" id="input_warning" value="0" />
							<div class="" style="display: none;" id="dayclose_error"></div>
						</div>
					</div> -->
					<div>
							 <!-- <label class="control-label"><?php //echo 'Backup'; ?></label> -->
   				 		<button id="print" type="button" id="bkp_button" onclick="backup()" title="Backup" class="btn btn-primary">Backup</button>
     			</div>
				</form>
			</div>
		</div>
	</div>
</div> 
<progress value="0" max="100" id="prog" style="display: none;"></progress>
<!-- <div id="here"></div> -->
<style>
#savsp{
	display:block;
	border:solid #ccc 1px;
  	cursor: pointer;
}

#overlay{	
	position: fixed;
	top: 0;
	z-index: 100;
	width: 100%;
	height:100%;
	display: none;
	background: rgba(0,0,0,0.6);
}
.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 40px;
	height: 40px;
	border: 4px #ddd solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
	0% { 
		transform: rotate(0deg); 
	}
	100% { 
		transform: rotate(359deg); 
	}
}
.is-hide{
	display:none;
}
</style>

<script type="text/javascript">

// function test(argument) {
	
// 	$.ajax({
// 		type: "POST",
// 		url: 'index.php?route=catalog/order/get_pending_datas&token=<?php echo $token; ?>',
// 		dataType: 'json',
// 		data: {"data":"check"},
// 		beforeSend: function() {
// 	        // setting a timeout
// 	        $('"prog').show();
// 	    },
// 		success: function(data){
// }

function backup(){
	// console.log("innnnnn");
	$(document).ajaxSend(function() {
		$("#overlay").fadeIn(300);　
	});
	$.ajax({
		type: "POST",
		url: 'index.php?route=catalog/backup/bkpdatabase&token=<?php echo $token; ?>',
		dataType: 'json',
		success: function(data){
			alert('Database Backup Successfully done');
		}
	}).done(function() {
		setTimeout(function(){
			$("#overlay").fadeOut(300);
		},500);
	});
}
</script>