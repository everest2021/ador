


<style>
* {
    margin: 0;
    padding: 0;
    }
    body {
    font: 71%/1.5 Verdana, Sans-Serif;
    background: #eee;
    }
    #container {
    margin: 10px auto;
    width: 688px;
    }

    #write {
    margin: 0 0 5px;
    padding: 5px;
    width: 671px;
    height: 120px;
    font: 1em/1.5 Verdana, Sans-Serif;
    background: #fff;
    border: 1px solid #f9f9f9;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    }
    #keyboard {
    margin: 0;
    padding: 0;
    list-style: none;
    }
        #keyboard li {
        float: left;
        margin: 0 5px 5px 0;
        width: 39px;
        height: 40px;
        line-height: 40px;
        text-align: center;
        background: #fff;
        border: 1px solid #f9f9f9;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        }
            .capslock, .tab, .left-shift {
            clear: left;
            }
                #keyboard .tab, #keyboard .delete {
                width: 70px;
                }
                #keyboard .capslock {
                width: 80px;
                }
                #keyboard .return {
                width: 77px;
                }
                #keyboard .left-shift {
                width: 95px;
                }
                #keyboard .right-shift {
                width: 109px;
                }
            .lastitem {
            margin-right: 0;
            }
            .uppercase {
            text-transform: uppercase;
            }
            #keyboard .space {
            clear: left;
            width: 681px;
            }
            .on {
            display: none;
            }
            #keyboard li:hover {
            position: relative;
            top: 1px;
            left: 1px;
            border-color: #e5e5e5;
            cursor: pointer;
            }
            #myDIV{
                display: none;
            }

            .btn-group {
              background-color: #ffc800; /* Green */
              border: none;
              color: white;
              padding: 15px 32px;
              text-align: center;
              text-decoration: none;
              display: inline-block;
              font-size: 16px;
              border-radius: 8px;
            }
</style>

<button class="btn-group" onclick="myFunction()">Try it</button>
<div id="myDIV">
    <div id="container">
        <textarea id="write" rows="6" cols="60"></textarea>
        <ul id="keyboard"><b>
            <li class="symbol"><span class="off">`</span><span class="on">~</span></li>
            <li class="symbol"><span class="off">1</span><span class="on">!</span></li>
            <li class="symbol"><span class="off">2</span><span class="on">@</span></li>
            <li class="symbol"><span class="off">3</span><span class="on">#</span></li>
            <li class="symbol"><span class="off">4</span><span class="on">$</span></li>
            <li class="symbol"><span class="off">5</span><span class="on">%</span></li>
            <li class="symbol"><span class="off">6</span><span class="on">^</span></li>
            <li class="symbol"><span class="off">7</span><span class="on">&amp;</span></li>
            <li class="symbol"><span class="off">8</span><span class="on">*</span></li>
            <li class="symbol"><span class="off">9</span><span class="on">(</span></li>
            <li class="symbol"><span class="off">0</span><span class="on">)</span></li>
            <li class="symbol"><span class="off">-</span><span class="on">_</span></li>
            <li class="symbol"><span class="off">=</span><span class="on">+</span></li>
            <li class="delete lastitem">Delete</li>
            <li class="tab">tab</li>
            <li class="letter">q</li>
            <li class="letter">w</li>
            <li class="letter">e</li>
            <li class="letter">r</li>
            <li class="letter">t</li>
            <li class="letter">y</li>
            <li class="letter">u</li>
            <li class="letter">i</li>
            <li class="letter">o</li>
            <li class="letter">p</li>
            <li class="symbol"><span class="off">[</span><span class="on">{</span></li>
            <li class="symbol"><span class="off">]</span><span class="on">}</span></li>
            <li class="symbol lastitem"><span class="off">\</span><span class="on">|</span></li>
            <li class="capslock">caps lock</li>
            <li class="letter">a</li>
            <li class="letter">s</li>
            <li class="letter">d</li>
            <li class="letter">f</li>
            <li class="letter">g</li>
            <li class="letter">h</li>
            <li class="letter">j</li>
            <li class="letter">k</li>
            <li class="letter">l</li>
            <li class="symbol"><span class="off">;</span><span class="on">:</span></li>
            <li class="symbol"><span class="off">'</span><span class="on">&quot;</span></li>
            <li class="return lastitem">Enter</li>
            <li class="left-shift">shift</li>
            <li class="letter">z</li>
            <li class="letter">x</li>
            <li class="letter">c</li>
            <li class="letter">v</li>
            <li class="letter">b</li>
            <li class="letter">n</li>
            <li class="letter">m</li>
            <li class="symbol"><span class="off">,</span><span class="on">&lt;</span></li>
            <li class="symbol"><span class="off">.</span><span class="on">&gt;</span></li>
            <li class="symbol"><span class="off">/</span><span class="on">?</span></li>
            <li class="right-shift lastitem">shift</li>
            <li class="space lastitem">&nbsp;</li>
        </ul>
    </div>
</div>    
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<!-- <script type="text/javascript" src="js/keyboard.js"></script> -->
<script>
    $(function(){
    var $write = $('#write'),
      shift = false,
      capslock = false;
    
    $('#keyboard li').click(function(){
      var $this = $(this),
        character = $this.html(); // If it's a lowercase letter, nothing happens to this variable
      
      // Shift keys
      if ($this.hasClass('left-shift') || $this.hasClass('right-shift')) {
        $('.letter').toggleClass('uppercase');
        $('.symbol span').toggle();
        
        shift = (shift === true) ? false : true;
        capslock = false;
        return false;
      }
      
      // Caps lock
      if ($this.hasClass('capslock')) {
        $('.letter').toggleClass('uppercase');
        capslock = true;
        return false;
      }
      
      // Delete
      if ($this.hasClass('delete')) {
        var html = $write.html();
        
        $write.html(html.substr(0, html.length - 1));
        return false;
      }
      
      // Special characters
      if ($this.hasClass('symbol')) character = $('span:visible', $this).html();
      if ($this.hasClass('space')) character = ' ';
      if ($this.hasClass('tab')) character = "\t";
      if ($this.hasClass('return')) character = "\n";
      
      // Uppercase letter
      if ($this.hasClass('uppercase')) character = character.toUpperCase();
      
      // Remove shift once a key is clicked.
      if (shift === true) {
        $('.symbol span').toggle();
        if (capslock === false) $('.letter').toggleClass('uppercase');
        
        shift = false;
      }
      
      // Add the character
      $write.html($write.html() + character);
    });
    });

</script>

<script>
function myFunction() {
  var x = document.getElementById("myDIV");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

</script>

<?php echo $footer; ?>