<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;">
  	<div class="page-header">
		<div class="container-fluid">
	  		<div class="pull-right sav">
				<button onclick="save();" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
	  		<h1><?php echo 'Change Table Number' ; ?></h1>
		</div>
  	</div>
  	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="alert alert-danger" style="display: none;" id="warning_div">
			<i class="fa fa-exclamation-circle"></i> <?php echo 'From and To Table are Same'; ?>
		  	<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<div class="alert alert-danger" style="display: none;" id="warning_div_1">
			<i class="fa fa-exclamation-circle"></i> <?php echo 'Table To Does Not Exist'; ?>
		  	<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<div class="alert alert-danger" style="display: none;" id="warning_div_2">
			<i class="fa fa-exclamation-circle"></i> <?php echo 'Table Is Running'; ?>
		  	<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<div class="alert alert-danger" style="display: none;" id="warning_div_3">
			<i class="fa fa-exclamation-circle"></i> <?php echo 'Please Select Table From'; ?>
		  	<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>

		<div class="alert alert-danger" style="display: none;" id="warning_div_4">
			<i class="fa fa-exclamation-circle"></i> <?php echo 'Table To Is Already Billed'; ?>
		  	<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				
		  	</div>
		  	<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-changetable" class="form-horizontal">
			  		<div class="form-group" style="display: none;">
						<label class="col-sm-3 control-label" style=""><?php echo 'Bill Ref No.'; ?></label>
						<div class="col-sm-3">
							<input readonly="readonly" type="text" name="order_no" id="order_no" value="<?php echo $order_no ?>" class="form-control" />
						</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-3 control-label" style=""><?php echo 'From'; ?></label>
						<div class="col-sm-3">
							<select name="table_frm" id="table_frm" class="form-control">
								<option value=""><?php echo "Please Select" ?></option>
								<?php foreach($tables_from as $skey => $svalue){ ?>
									<?php if($skey == $table_frm){ ?>
										<option selected="selected" value="<?php echo $skey ?>"><?php echo $svalue ?></option>
									<?php } else { ?>
						   				<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
						   			<?php } ?>
								<?php } ?>
							</select>
							<!-- <input type="hidden" name="table_frm" id="table_frm" value="<?php echo $table_frm; ?>" class="form-control" /> -->
							<input type="hidden" name="table_frm1" id="table_frm1" value="<?php echo $table_frm1; ?>" class="form-control" />
					  	</div>
				  	</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" style=""><?php echo 'To'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="table_to" id="table_to" value="<?php echo $table_to; ?>" class="form-control" />
							<?php /* ?>
							<!-- <select tabindex="1" name="table_to" id="table_to" class="form-control">
								<option ><?php echo "Please Select" ?></option>
								<?php foreach($tables as $skey => $svalue){ ?>
						    		<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
								<?php } ?>
							</select> -->
							<!-- <input type="hidden" name="table_to1" id="table_to1" value="<?php echo $table_to1; ?>" class="form-control" /> -->
				  			<?php */ ?>
				  			<?php if ($table_to) { ?>
								<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $table_to; ?></div>
							<?php } ?>
							
						</div>
					</div>
				</form>
		  	</div>
		</div>
  	</div>
</div> 
<script type="text/javascript">
$('#table_frm').on('change', function() {
  table_frm = $('#table_frm option:selected').text();
  $('#table_frm1').val(table_frm);
});
// $('#table_to').on('change', function() {
//   table_to = $('#table_to option:selected').text();
//   $('#table_to1').val(table_to);
// });
$(document).ready(function() {
  	order_no = $('#order_no').val();
  	if(order_no != '' && order_no != '0' && order_no != undefined){
  		$('.sav').html('');
  		html = '<a tabindex="2" id="save_button" data-toggle="tooltip" onclick="save()" title="Update" class="btn btn-primary">Update</a>';
  		$('.sav').append(html);
	}
	$('#table_from').focus();
});

$(document).on('keyup','#save_button', function (e) {
    if (e.keyCode == 33) {
        save();
    }
});

$(document).on('keypress','input,select', function (e) {
//$('input,select').on('keypress', function (e) {
    if (e.which == 13) {
        e.preventDefault();
        var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
        if (!$next.length) {
            $next = $('[tabIndex=1]');
        }
        $next.focus();
    }
});

function save(){
	var filter_code = $('#table_to').val();
	format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
	filter_code =  format.test(filter_code);
	//alert(filter_code);
	if(filter_code == false){
	  	action = '<?php echo $action; ?>';
	  	action = action.replace("&amp;", "&");
	  	$.post(action, $('#form-changetable').serialize()).done(function(data) {
	    	var parsed = JSON.parse(data);
	     	if(parsed.info == 1){
	     		$('.sucess').html('');
	     		$('.sucess').append(parsed.done);
	     		$('#warning_div').hide();
	     		$('#warning_div_1').hide();
	     		$('#warning_div_2').hide();
	     		$('#warning_div_3').hide();
	     		$('#warning_div_4').hide();

	     		//$('.custom-class').trigger("click");
	     		//$('.ui-icon-closethick').trigger('click');
	     	} else {
	     		$('#warning_div').hide();
	     		$('#warning_div_1').hide();
	     		$('#warning_div_2').hide();
	     		$('#warning_div_3').hide();
	     		$('#warning_div_4').hide();

	     		if(parsed.status == 1){
	     			$('#warning_div').show();
	     		} else if(parsed.status == 2){
	     			$('#warning_div_1').show();
	     		} else if(parsed.status == 3){
	     			$('#warning_div_2').show();
	     		} else if(parsed.status == 4){
	     			$('#warning_div_3').show();
	     		} else if(parsed.status == 5){
	     			$('#warning_div_4').show();
	     		}
	     		$('#table_to').focus();
	     	}
		});
	} else {
		alert("Please Remove Special Character");
	 		return false;
	}
}
</script>