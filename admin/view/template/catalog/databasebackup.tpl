<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
		    <div style = "display:none" class="pull-right">
		    	<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
		       	<button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
		    </div>
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
	    </div>
    </div>
  <div class="container-fluid">
	<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>

		<?php if ($warning_send) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $warning_send; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success_send) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success_send; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
		    </div>
		<div class="panel-body">
			<div class="col-sm-12">
		       	<label style="font-weight: bold;font-size: 30px;"> Remaining Data Send To Server : <?php echo $order_info ?></label>
		       	<br>

		       	<label style="font-weight: bold;font-size: 30px;"> Remaining Data Send To Server : <?php echo $order_items ?></label>
	       </div>	
		    <div class="well">
			   <div class="row">
			        <div class="col-sm-6" style="display: none;">
				       	<button type="button" id="button-filter" class="btn btn-primary pull-right"><i></i> <?php echo 'Export Database'; ?></button>
			       </div>

			       <div class="col-sm-6">
				       	<button type="sync_to" id="button-sync_to" class="btn btn-primary pull-right"><i></i> <?php echo 'Sync To Server'; ?></button>
			       </div>
			       <div class="col-sm-6" style="display: none;" >
				       	<button type="repair" id="button-repair" class="btn btn-primary pull-right"><i></i> <?php echo 'Repair'; ?></button>
			       </div>
			    </div>
			    
		   </div>
		   
	    </div>
		
		
	  </div>
	</div>
  </div>
</div>
<div id="dialog-form_paymentmode" title = "Payment Mode">
</div>	
<script type="text/javascript">
dialog_paymentmode = $("#dialog-form_paymentmode").dialog({
	closeOnEscape: false,
	autoOpen: false,
	height: 730,
	width: 1000,
	modal: true,
	open: function(event, ui) {
    	$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
	}
});



function closeIFrame(){
      location.replace('index.php?route=catalog/advance&token=<?php echo $token; ?>');
}

$('#button-filter').on('click', function() {
  	var url = 'index.php?route=catalog/databasebackup/export&token=<?php echo $token; ?>&export=1';

  	location = url;
});


$('#button-sync_to').on('click', function() {
  	var url = 'index.php?route=catalog/sync_to&token=<?php echo $token; ?>';

  	location = url;
});

$('#button-repair').on('click', function() {
  	var url = 'index.php?route=catalog/repair&token=<?php echo $token; ?>';

  	location = url;
});

$('input[name=\'filter_customername\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/advance/autocomplete&token=<?php echo $token; ?>&filter_customername=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.customername+" - "+item.contact,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_customername\']').val((ui.item.label).substring(0, ui.item.label.indexOf(' - ')));
    $('input[name=\'filter_item_id\']').val(ui.item.value);	
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});
</script>
<?php echo $footer; ?>