<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right">
		       <button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
		       <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
	           <h1><?php echo $heading_title; ?></h1>
	           <ul class="breadcrumb">
		           <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		            <?php } ?>
	           </ul>
	    </div>
  </div>
  <div class="container-fluid">
	   <?php if ($error_warning) { ?>
	   <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	       <button type="button" class="close" data-dismiss="alert">&times;</button>
	   </div>
	    <?php } ?>
	   <div class="panel panel-default">
	       <div class="panel-heading">
		       <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
	        </div>
	   <div class="panel-body">
		   <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
		        <div class="form-group required">
			        <label class="col-sm-3 control-label"><?php echo 'Store Name'; ?></label>
			        <div class="col-sm-3">
			            <input type="text" name="store_name" value="<?php echo $store_name; ?>" placeholder="<?php echo 'Store Name'; ?>" class="form-control" />
			            <?php if ($error_name) { ?>
			            	<div class="text-danger"><?php echo $error_name; ?></div>
			             <?php } ?>
			        </div>
		        </div> 
		    <div class="form-group ">
			   <label class="col-sm-3 control-label"><?php echo ' Store Code'; ?></label>
			    <div class="col-sm-3">
			        <input type="text" name="store_code" value="<?php echo $store_code; ?>" placeholder="<?php echo 'Code'; ?>" class="form-control" />
		            <?php if ($error_code) { ?>
		            	<div class="text-danger"><?php echo $error_code; ?></div>
		            <?php } ?>
			    </div>
		    </div> 
		    <div class="form-group ">
		    	<label class="col-sm-3 control-label"><?php echo 'Store Type'; ?></label>
				   		<div class="col-sm-3" >
				        	<select name="store_type" id="input-store_type" class="form-control">
				            	<option ><?php echo "Please Select" ?></option>
					          	<?php foreach($store_types as $skey => $svalue){ ?>
					          		<?php if($skey == $store_type){ ?>
						      			<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue; ?></option>
					          		<?php } else { ?>
						      			<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
					          		<?php } ?>
					          	<?php } ?>
				        	</select>
				   		</div>
				</div>
				<div class="form-group">
			      	<?php if($activate_store == '1'){ ?>
			      				<label class="col-sm-3 control-label"><?php echo 'Activate Store'; ?></label>
			      			<div class="col-sm-3">
								<input type="checkbox" name="activate_store" value="1" checked ="checked"  style="margin-right:8%"> <br>
							</div>
							<?php } else { ?>
								<label class="col-sm-3 control-label"><?php echo 'Activate Store'; ?></label>
							<div class="col-sm-3">
								<input type="checkbox" id="activate_store" name="activate_store" value="1" style="margin-right:8%;">  <br />
							</div>
							<?php } ?>
		       	</div>
		  </div>
		</form>
	  </div>
	</div>
  </div>
</div> 
<?php echo $footer; ?>