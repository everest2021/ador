<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right">
		       	<button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" id="save" class="btn btn-primary"><i class="fa fa-save"></i></button>
		       	<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
		    </div>
		    <h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
		    </ul>
	    </div>
	</div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
	    <div class="panel panel-default">
		    <div class="panel-heading">
		        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		    </div>
			<div class="panel-body">
		   		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
				    <div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Customer Name'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="customername" id="customername" value="<?php echo $customername ?>" placeholder="<?php echo 'Name'; ?>" class="form-control" style="width:89%;display:inline;"/>
							<input type="hidden" name="c_id" id="c_id" value="<?php echo $c_id ?>" class="form-control"/>
							<input type="hidden" name="email" id="email" value="<?php echo $email ?>" class="form-control"/>
							<input type="hidden" name="gst_no" id="gst_no" value="<?php echo $gst_no ?>" class="form-control"/>
							<a target="_blank" href="<?php echo $add_customer; ?>" data-toggle="tooltip" title="<?php echo 'Add Customer'; ?>" class="btn btn-default"><i class="fa fa-plus"></i></a> 
						</div>
					</div>
					<div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Contact'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="contact" id="contact" value="<?php echo $contact ?>" placeholder="<?php echo 'Contact'; ?>" class="form-control"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label"><?php echo 'Delivery Address'; ?></label>
						<div class="col-sm-3">
    						<textarea class="form-control" name="address" value="<?php echo $address ?>" placeholder="Address" id="address" rows="3"></textarea>
						</div>
					</div>
					<?php /*<div class="form-group"> ?>
						<label class="col-sm-3 control-label"><?php echo 'Booking Date'; ?></label>
					    <div class="col-sm-3">
					     	<input autocomplete="off" type="text" name='booking_date' value="<?php echo $booking_date ?>" class="form-control form_datetime" placeholder="Booking Date">
					    </div>
				    </div>
				    <?php */?>
				  	<div class="form-group">
						<label class="col-sm-3 control-label"><?php echo 'Delivery Date'; ?></label>
					    <div class="col-sm-3">
					     	<input autocomplete="off" type="text" name='delivery_date' value="<?php echo $delivery_date ?>" class="form-control form_datetime" placeholder="Delivery Date">
					    </div>
				    </div>
				     <div class="form-group">
						<label class="col-sm-3 control-label"><?php echo 'Delivery Time'; ?></label>
					    <div class="col-sm-3">
					     	<input autocomplete="off" type="time" name='delivery_time' value="<?php echo $delivery_time ?>" class="form-control" placeholder="Delivery Time">
					    </div>
				    </div>
				    <div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Item Name'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="itemname" id="itemname" placeholder="<?php echo 'Item Name'; ?>" class="form-control"/>
						</div>
					</div>
					<table id="data" class="table table-bordered">
						<tr>
							<th>Sr.No</th>
							<th>Item Name</th>
							<th>Quantity</th>
							<th>Rate</th>
							<th>Amount</th>
							<th>Message</th>
							<th>Remove</th>
						</tr>
						<?php $i=1; ?>
						<?php if($testdatas) { ?>
							<?php foreach($testdatas as $testdata) { ?>
								<tr id="re_<?php echo $i ?>">
									<td><?php echo $i?></td>
									<td><input type="text" autocomplete = "off" name="po_datas[<?php echo $i ?>][item]" value="<?php echo $testdata['item_name'] ?>" class="form-control"/></td>
									<td><input type="text" autocomplete = "off" name="po_datas[<?php echo $i ?>][qty]" id="qty_<?php echo $i ?>" value="<?php echo $testdata['qty'] ?>" class="qty form-control"/></td>
									<input type="hidden" name="po_datas[<?php echo $i ?>][itemid]" value="<?php echo $testdata['item_id'] ?>" class="form-control"/>
									<input type="hidden" name="po_datas[<?php echo $i ?>][itemcode]" value="<?php echo $testdata['item_code'] ?>" class="form-control"/>
									<input type="hidden" name="po_datas[<?php echo $i ?>][tax1]" value="<?php echo $testdata['tax1'] ?>" class="form-control"/>
									<input type="hidden" name="po_datas[<?php echo $i ?>][taxvalue1]" value="<?php echo $testdata['taxvalue1'] ?>" class="taxvalue1 form-control"/>
									<input type="hidden" name="po_datas[<?php echo $i ?>][tax2]" value="<?php echo $testdata['tax2'] ?>" class="form-control"/>
									<input type="hidden" name="po_datas[<?php echo $i ?>][taxvalue2]" value="<?php echo $testdata['taxvalue2'] ?>" class="taxvalue2 form-control"/>
									<input type="hidden" name="po_datas[<?php echo $i ?>][subcategoryid]" value="<?php echo $testdata['subcategoryid'] ?>" class="subcategoryid form-control"/>
									<td><input type="text" autocomplete = "off" name="po_datas[<?php echo $i ?>][rate]" id="rate_<?php echo $i ?>" value="<?php echo $testdata['rate'] ?>" class="rate form-control"/></td>
									<td><input type="text" readonly = "readonly" name="po_datas[<?php echo $i ?>][amt]" id="amt_<?php echo $i ?>" value="<?php echo $testdata['amt'] ?>" class="amt form-control"/></td>
									<td><input type="text" name="po_datas[<?php echo $i ?>][msg]" id="msg_<?php echo $i ?>" value="<?php echo $testdata['msg'] ?>" class="msg form-control"/></td>
									<td><a style="cursor: pointer;font-size: 16px;" onclick="remove_folder(<?php echo $i ?>)" class="button inputs remove " id="remove_<?php echo $i ?>" ><i class="fa fa-trash-o"></i></a></td>
								</tr>
							<?php $i++; ?>
							<?php } ?>
						<?php } ?>
					</table>
					<div class="pull-right">
						<div class="form-group">
						   <label class="col-sm-3 control-label"><?php echo 'Total'; ?></label>
							<div class="col-sm-9">
								<input type="text" readonly = "readonly" value="<?php echo $total ?>" name="total" id="total" placeholder="<?php echo 'Total'; ?>" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><?php echo 'GST'; ?></label>
							<div class="col-sm-9">
								<input type="text" name="gst" readonly = "readonly" value="<?php echo $gst ?>" id="gst" placeholder="<?php echo 'GST'; ?>" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><?php echo 'Service Charge'; ?></label>
							<div class="col-sm-9">
								<input type="text" name="sc" readonly = "readonly" value="<?php echo $sc ?>" id="sc" placeholder="<?php echo 'Service Charge'; ?>" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><?php echo 'Grand Total'; ?></label>
							<div class="col-sm-9">
								<input type="text" name="grandtotal" readonly = "readonly" value="<?php echo $grandtotal ?>" id="grandtotal" placeholder="<?php echo 'Grand Total'; ?>" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><?php echo 'Advance'; ?></label>
							
							<div class="col-sm-9">
								<input type="text" name="advance" id="advance" value="<?php echo $advance ?>" placeholder="<?php echo 'Advance'; ?>" class="form-control"/>
							</div>
							<?php if (isset($error_advance)) { ?>
                  			<span class="error" style= "color:#FF0000;"><?php echo $error_advance; ?></span>
                  			<?php } ?>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><?php echo 'Balance'; ?></label>
							<div class="col-sm-9">
								<input type="text" name="balance" readonly = "readonly" id="balance" value="<?php echo $balance ?>" placeholder="<?php echo 'Balance'; ?>" class="form-control"/>
							</div>
						</div>
					</div>
				</form>
	  		</div>
		</div>
  	</div>
  	<script type="text/javascript">
  	inclusive = '<?php echo $INCLUSIVE ?>';
  	var total = 0;
  	var scharge = '<?php echo $SERVICE_CHARGE_FOOD ?>';
  	var i = '<?php echo $i ?>';
  	$('#itemname').autocomplete({
	  delay: 500,
	  source: function(request, response) {
	    $.ajax({
	      url: 'index.php?route=catalog/advance/autocompleteitemname&token=<?php echo $token; ?>&filter_item_name=' +  encodeURIComponent(request.term),
	      dataType: 'json',
	      success: function(json) {   
	        response($.map(json, function(item) {
	          return {
	            label: item.item_name,
	            value: item.item_id,
	            code : item.item_code,
	            rate : item.rate,
	            tax1 : item.tax1,
	            tax2 : item.tax2,
	            subcategoryid : item.subcategoryid,
	          }
	        }));
	      }
	    });
	  }, 
	  select: function(event, ui) {
		html = '<tr id="re_'+i+'">'; 
		  html += '<td>'+i+'</td>';
	      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][item]" value="'+ui.item.label+'"  class="form-control"/></td>';
	      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][qty]" id="qty_'+i+'" value = "1" style="display:inline-block" class="qty form-control"/>';
	      html += '<input type="hidden" name="po_datas[' + i + '][itemid]" value="'+ui.item.value+'"  class="form-control"/>';
	      html += '<input type="hidden" name="po_datas[' + i + '][itemcode]" value="'+ui.item.code+'"  class="form-control"/>';
	      html += '<input type="hidden" name="po_datas[' + i + '][tax1]" id="tax1_'+i+'" value="'+ui.item.tax1+'"  class="form-control"/>';
	      html += '<input type="hidden" name="po_datas[' + i + '][tax2]" id="tax2_'+i+'" value="'+ui.item.tax2+'"  class="form-control"/>';
	      html += '<input type="hidden" name="po_datas[' + i + '][taxvalue1]" id="taxvalue1_'+i+'" class="taxvalue1 form-control"/>';
	      html += '<input type="hidden" name="po_datas[' + i + '][taxvalue2]" id="taxvalue2_'+i+'" class="taxvalue2 form-control"/>';
	      html += '<input type="hidden" name="po_datas[' + i + '][subcategoryid]" id="subcategoryid_'+i+'"  value="'+ui.item.subcategoryid+'" class="subcategoryid form-control"/>';
	      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][rate]" value="'+ui.item.rate+'" id="rate_'+i+'" class=" rate form-control"/></td>';
	      html += '<td><input type="text" name="po_datas[' + i + '][amt]" readonly = "readonly" value="'+ui.item.rate+'" id="amt_'+i+'" class="amt form-control"/></td>';
	      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][msg]" id="msg_'+i+'" class="msg form-control"/></td>';
	      html += '<input type="hidden" name="po_datas[' + i + '][msgcode]" id="msgcode_'+i+'" class="form-control"/>';
	      html += '<td><a style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove " id="remove_'+i+'" ><i class="fa fa-trash-o"></i></a></td>';
      	html += '</tr>';
      	$('#data').append(html);

      	tax1 = parseFloat(ui.item.tax1);
		tax2 = parseFloat(ui.item.tax2);
		amt  = parseFloat(ui.item.rate);

		tax1_value = amt*(tax1/100);
		tax2_value = amt*(tax2/100);

		$('#taxvalue1_'+i).val(tax1_value.toFixed(2));
		$('#taxvalue2_'+i).val(tax2_value.toFixed(2));

		total = gettotal();
		$('#total').val(total);

		gst = getgst();
		$('#gst').val(gst);

		sc = total * (scharge/100);
		$('#sc').val(sc);

		stax  = parseInt($('#sc').val());

		if(inclusive == 1){
			grandtotal = total + stax;
		} else{
			grandtotal = total + gst + stax;
		}
		$('#grandtotal').val(grandtotal.toFixed(2));
		$('#balance').val(grandtotal.toFixed(2));

		i++;
	    return false;
	  },
	  focus: function(event, ui) {
	  	$('#itemname').val('');
	    return false;
	  }
	});

	function remove_folder(c) {
		$('#re_'+c).remove();
		total = gettotal();
		$('#total').val(total);

		gst = getgst();
		$('#gst').val(gst);

		sc = total * (scharge/100);
		$('#sc').val(sc);

		stax  = parseInt($('#sc').val());

		if(inclusive == 1){
			grandtotal = total + stax;
		} else{
			grandtotal = total + gst + stax;
		}
		$('#grandtotal').val(grandtotal.toFixed(2));
		$('#balance').val(grandtotal.toFixed(2));
	}

    $(".form_datetime").datepicker();

    $('#customername').autocomplete({
	  delay: 500,
	  source: function(request, response) {
	    $.ajax({
	      url: 'index.php?route=catalog/advance/autocompletecustname&token=<?php echo $token; ?>&filter_item_name=' +  encodeURIComponent(request.term),
	      dataType: 'json',
	      success: function(json) {   
	        response($.map(json, function(item) {
	          return {
	            label: item.name + " - " + item.contact,
	            value: item.name,
	            address : item.address,
	            contact:item.contact,
	            c_id:item.c_id,
	            email:item.email,
	            gst_no:item.gst_no
	          }
	        }));
	      }
	    });
	  }, 
	  select: function(event, ui) {
	  	$('#customername').val((ui.item.label).substring(0, ui.item.label.indexOf('-')));
	  	$('#address').val(ui.item.address);
	  	$('#contact').val(ui.item.contact);
	  	$('#c_id').val(ui.item.c_id);
	  	$('#email').val(ui.item.email);
	  	$('#gst_no').val(ui.item.gst_no);
	    return false;
	  },
	  focus: function(event, ui) {

	    return false;
	  }
	});

    $(document).on('keyup', '.msg', function(e){
		$('.msg').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
				$.ajax({
			  		url: 'index.php?route=catalog/advance/autocompletekotmsg&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			  		dataType: 'json',
			  		success: function(json) {   
						response($.map(json, function(item) {
				  			return {
								label: item.message,
								value: item.msg_code
				  			}
						}));
			  		}
				});
		  	}, 
		  	select: function(event, ui) {
		  		idss = $(this).attr('id');
				s_id = idss.split('_');
				$('#msg_'+s_id[1]).val(ui.item.label);
				$('#msgcode_'+s_id[1]).val(ui.item.value);
				//$('#message_'+s_id[1]).val(ui.item.label);
				return false;
		  	},
		  	focus: function(event, ui) {
				return false;
		  	}
		});
	});

	$(document).on('keyup', '.qty', function(e) {
		idss = $(this).attr('id');
	  	s_id = idss.split('_');
	  	rate  = parseFloat($('#rate_'+s_id[1]).val());
	  	qty = parseFloat($('#qty_'+s_id[1]).val());

	  	if(rate == '' || rate == '0' || isNaN(rate)){
			rate = 0;
		}
		if(qty == '' || qty == '0' || isNaN(qty)){
			qty = 0;
		}

	  	amt = qty * rate;
	  	$('#amt_'+s_id[1]).val(amt);

	  	tax1 = parseFloat($('#tax1_'+s_id[1]).val());
		tax2 = parseFloat($('#tax2_'+s_id[1]).val());
		amt  = parseFloat($('#amt_'+s_id[1]).val());
		
		tax1_value = amt*(tax1/100);
		tax2_value = amt*(tax2/100);

		$('#taxvalue1_'+s_id[1]).val(tax1_value.toFixed(2));
		$('#taxvalue2_'+s_id[1]).val(tax2_value.toFixed(2));

		total = gettotal();
		$('#total').val(total);

		gst = getgst();
		$('#gst').val(gst);

		sc = total * (scharge/100);
		$('#sc').val(sc);

		stax  = parseInt($('#sc').val());

		if(inclusive == 1){
			grandtotal = total + stax;
		} else{
			grandtotal = total + gst + stax;
		}
		$('#grandtotal').val(grandtotal.toFixed(2));
		$('#balance').val(grandtotal.toFixed(2));

	});

	$(document).on('keyup', '.rate', function(e) {
		idss = $(this).attr('id');
	  	s_id = idss.split('_');
	  	rate  = parseFloat($('#rate_'+s_id[1]).val());
	  	qty = parseFloat($('#qty_'+s_id[1]).val());

	  	if(rate == '' || rate == '0' || isNaN(rate)){
			rate = 0;
		}
		if(qty == '' || qty == '0' || isNaN(qty)){
			qty = 0;
		}

	  	amt = qty * rate;
	  	$('#amt_'+s_id[1]).val(amt);

	  	tax1 = parseFloat($('#tax1_'+s_id[1]).val());
		tax2 = parseFloat($('#tax2_'+s_id[1]).val());
		amt  = parseFloat($('#amt_'+s_id[1]).val());
		
		tax1_value = amt*(tax1/100);
		tax2_value = amt*(tax2/100);

		$('#taxvalue1_'+s_id[1]).val(tax1_value.toFixed(2));
		$('#taxvalue2_'+s_id[1]).val(tax2_value.toFixed(2));

		total = gettotal();
		$('#total').val(total);

		gst = getgst();
		$('#gst').val(gst);

		sc = total * (scharge/100);
		$('#sc').val(sc);

		stax  = parseInt($('#sc').val());

		if(inclusive == 1){
			grandtotal = total + stax;
		} else{
			grandtotal = total + gst + stax;
		}
		$('#grandtotal').val(grandtotal.toFixed(2));
		$('#balance').val(grandtotal.toFixed(2));

	});

	$('#advance').keyup(function(){

		grandtotal = parseFloat($('#grandtotal').val());
		advance = parseFloat($('#advance').val());
		if(advance == '' || advance == '0' || isNaN(advance)){
			advance = 0;
		}

		if(advance < 0 || advance > grandtotal){
			alert("Please Check amount properly");
			$('#advance').val(0);
			return false;
		}

		balance = grandtotal - advance;
		if(balance == '' || balance == '0' || isNaN(balance)){
			balance = 0;
		}

		$('#balance').val(balance.toFixed(2));

	});

	function gettotal(){
		total = 0;
		$('.amt').each(function( index ) {
	  		if(!isNaN($(this).val()) && $(this).val() != '' && $(this).val() != '0' && $(this).val() != 0){
	  			total = total + parseFloat($(this).val());
	  		}
	  	});
	  	return total;
	}

	function getgst(){
		taxvalue1 = 0;
		taxvalue2 = 0;
		$('.taxvalue1').each(function( index ) {
	  		if(!isNaN($(this).val()) && $(this).val() != '' && $(this).val() != '0' && $(this).val() != 0){
	  			taxvalue1 = taxvalue1 + parseFloat($(this).val());
	  		}
	  	});

	  	$('.taxvalue2').each(function( index ) {
	  		if(!isNaN($(this).val()) && $(this).val() != '' && $(this).val() != '0' && $(this).val() != 0){
	  			taxvalue2 = taxvalue2 + parseFloat($(this).val());
	  		}
	  	});
	  	return parseFloat(taxvalue1 + taxvalue2);
	}

  	</script>
</div>
<?php echo $footer; ?>