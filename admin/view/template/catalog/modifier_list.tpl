<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
		    <div class="pull-right">
		    	<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
		       	<button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
		    </div>
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
	    </div>
    </div>
  <div class="container-fluid">
	<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
		    </div>
		<div class="panel-body">
		    <div class="well">
			   <div class="row">
			       <div class="col-sm-4">
				       <div class="form-group">
				          <label class="control-label" for="input-name"><?php echo 'Modifier Name'; ?></label>
				          <input type="text" name="filter_item_name" value="<?php echo $filter_item_name; ?>" placeholder="<?php echo 'Item Name'; ?>" id="input-filter_item_name" class="form-control" />
				          <input type="hidden" name="filter_item_id" value="<?php echo $filter_item_id; ?>" id="input-filter_item_id" class="form-control" />
				        </div>
				       	<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
			       </div>
			    </div>
		   </div>
	    </div>
		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport">
		    <div class="table-responsive">
			  <table class="table table-bordered table-hover">
			   <thead>
				<tr>
				  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
				  <td><?php if ($sort == 'item_name') { ?>
					<a href="<?php echo $sort_item_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Item Name'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_item_name; ?>"><?php echo 'Item Name'; ?></a>
					<?php } ?></td>
				  <td class="text-right"><?php echo $column_action; ?></td>
				</tr>
			  </thead>
			  <tbody>
				<?php if ($items) { ?>
				<?php foreach ($items as $item) { ?>
				<tr>
				  <td><?php if (in_array($item['item_id'], $selected)) { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $item['item_id']; ?>" checked="checked" />
					<?php } else { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $item['item_id']; ?>" />
					<?php } ?></td>
				  <td><?php echo $item['item_name']; ?></td>
				  <td class="text-right"><a href="<?php echo $item['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
				</tr>
				<?php } ?>
				<?php } else { ?>
				<tr>
				  <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
				</tr>
				<?php } ?>
			  </tbody>
			  </table>
		    </div>
		</form>
		<div class="row">
		    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script type="text/javascript">
$('#button-filter').on('click', function() {
  	var url = 'index.php?route=catalog/modifier&token=<?php echo $token; ?>';

  	var filter_item_name = $('input[name=\'filter_item_name\']').val();
  	if (filter_item_name) {
		var filter_item_id = $('input[name=\'filter_item_id\']').val();
		if (filter_item_id) {
	  		url += '&filter_item_id=' + encodeURIComponent(filter_item_id);
	  		url += '&filter_item_name=' + encodeURIComponent(filter_item_name);
	  	}
  	}

  	location = url;
});

$('input[name=\'filter_item_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/modifier/autocomplete&token=<?php echo $token; ?>&filter_item_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.item_name,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_item_name\']').val(ui.item.label);
    $('input[name=\'filter_item_id\']').val(ui.item.value);
		
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});
</script>
<?php echo $footer; ?>