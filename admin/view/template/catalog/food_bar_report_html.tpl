<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
	<h1 style="text-align:center;font-weight: bold;color: #000;">
    <?php echo "Food And Bar Day Wise Report"; ?><br />
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
    <?php echo 'From : '. $filter_startdate; ?>
   	<?php echo 'To : '. $filter_enddate; ?><br />
    </span>
    <span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
  	</h1>
		<?php if ($final_data) {?>
		<table class="product" style="width:100% !important;">
			<thead>
				<tr>
					<td style="text-align: left;"><?php echo 'Date'; ?></td>
					<td style="text-align: left;"><?php echo 'Amount'; ?></td>
					<td style="text-align: left;"><?php echo 'Discount'; ?></td>
					<td style="text-align: left;"><?php echo 'Net'; ?></td>		            
					<td style="text-align: left;"><?php echo 'GST'; ?></td>
					<td style="text-align: left;"><?php echo 'Total'; ?></td>
					<td style="text-align: left;"><?php echo 'Bill No.'; ?></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="7" style="text-align: left;  "><strong><?php echo 'FOOD'; ?></strong></td>
				</tr>
				<?php foreach($final_data['food_datas'] as $result) { ?>
					<tr>			          
						<td class="left">
							<?php echo $result['date']; ?>
						</td>
						<td class="left">
							<?php echo $result['amount']; ?>
						</td>
						
						<td class="left">
							<?php echo $result['discount']; ?>
						</td>
						<td class="left">
							<?php echo $result['net']; ?>
						</td>
						<td class="left">
							<?php echo $result['gst']; ?>
						</td>
						<td class="left">
							<?php echo $result['gtotal']; ?>
						</td>
						<td class="left">
							<?php echo $result['billno']; ?>
						</td>
						
					</tr>
				<?php } ?>
				<tr>
					<td colspan="7" style="text-align: left;  "><strong><?php echo 'BAR'; ?></strong></td>
				</tr>
				<?php foreach($final_data['liq_datas'] as $result) { ?>
					<tr>			          
						<td class="left">
							<?php echo $result['date']; ?>
						</td>
						<td class="left">
							<?php echo $result['amount']; ?>
						</td>
						
						<td class="left">
							<?php echo $result['discount']; ?>
						</td>
						<td class="left">
							<?php echo $result['net']; ?>
						</td>
						<td class="left">
							<?php echo $result['vat']; ?>
						</td>
						<td class="left">
							<?php echo $result['gtotal']; ?>
						</td>
						<td class="left">
							<?php echo $result['billno']; ?>
						</td>
					</tr>
				<?php } ?>

					<tr style="border-top:2px solid #000000;padding-top:#000000;">
						<td colspan="7" style="border-bottom:2px solid #000000;padding-bottom:#000000;" ></td>
					</tr>
			</tbody>
		</table>
		<?php }  ?>
 	</div>
</body>
</html>
