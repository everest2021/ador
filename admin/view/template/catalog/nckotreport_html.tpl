<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
	<h1 style="text-align:center;font-weight: bold;color: #000;">
    <?php echo "NC KOT REPORT"; ?><br />
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
    <?php echo 'From : '. $filter_startdate; ?>
   	<?php echo 'To : '. $filter_enddate; ?><br />
    </span>
    <span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
  	</h1>
	  	<table class="product" style="width:100% !important;">
			<thead>
				<tr>
					<th style="text-align: center;">Ref No</th>
					<th style="text-align: center;">Table No.</th>
					<th style="text-align: center;">Bill Date</th>
					<th style="text-align: center;">Item Name</th>
					<th style="text-align: center;">Qty</th>
					<th style="text-align: center;">Rate</th>
					<th style="text-align: center;">Amount</th>
					<th style="text-align: center;">Nc Kot Reason</th>
				</tr>
			</thead>
			<tbody>
			<?php if($nckotdata){ ?>
						<?php $totalamount = 0;$amount = 0;$totalqty = 0;?>
						<?php foreach($nckotdata as $key => $data) { ?>
								<tr>
									<td><?php echo $data['order_no'] ?></td>
									<td><?php echo $data['table_id'] ?></td>
									<td><?php echo $data['bill_date'] ?></td>
									<td><?php echo $data['name'] ?></td>
									<td><?php echo $data['qty'] ?></td>
									<td><?php echo $data['rate'] ?></td>
									<td><?php echo $data['qty']*$data['rate'] ?></td>
									<td><?php echo $data['nc_kot_reason'] ?></td>
								</tr>
							<?php $totalamount = $totalamount + ($data['qty']*$data['rate']);
							$totalqty = $totalqty + $data['qty'];?>
						<?php } ?>
						<tr>
							<td colspan="4">Total</td>
							<td><?php echo $totalqty;?></td>
							<td></td>
							<td><?php echo $totalamount;?></td>
							<td></td>
						</tr>
				  	<?php } ?>
			</tbody>
		</table>
 	</div>
</body>
</html>
