<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
	<h1 style="text-align:center;font-weight: bold;color: #000;">
    <?php echo "Item Wise Report"; ?><br />
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
    <?php echo 'From : '. $filter_startdate; ?>
   	<?php echo 'To : '. $filter_enddate; ?><br />
    </span>
    <span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
  	</h1>
		<?php if ($final_data) {?>
		<table class="product" style="width:100% !important;">
			<thead>
				<tr>
					<td style="text-align: left;"><?php echo 'Sr.No'; ?></td>
					<td style="text-align: left;"><?php echo 'Item Name'; ?></td>
					<?php foreach($stores as $store) { ?>
						<td style="text-align: left;"><?php echo $store['outlet_name']; ?></td>
					<?php } ?>
					<td style="text-align: left;"><?php echo 'Notes'; ?></td>
					<td style="text-align: left;"><?php echo 'Total Quantity'; ?></td>		            
				</tr>
			</thead>
			<tbody>
				<?php foreach($final_data as $result) { ?>
					<tr>			          
						<td class="left">
							<?php echo $result['i']; ?>
						</td>
						<td class="left">
							<?php echo $result['item_name']; ?>
						</td>
						<?php foreach($result['storewise'] as $key => $value) { ?>
							<td><?php echo $value['quantity'] ?></td>
						<?php } ?>
						<td class="left">
							<?php echo $result['notes']; ?>
						</td>
						<td class="left">
							<?php echo $result['quantity']; ?>
						</td>
						
					</tr>
				<?php } ?>			            
					<tr style="border-top:2px solid #000000;padding-top:#000000;">
						<td colspan="6" style="border-bottom:2px solid #000000;padding-bottom:#000000;" ></td>
					</tr>
			</tbody>
		</table>
		<?php }  ?>
 	</div>
</body>
</html>
