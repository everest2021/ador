<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
	    	<?php if ($success) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				   <button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-4">
						       		<div class="form-row">
									    <div class="col-sm-3">
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									      
									</div>
									<div class="col-sm-3" style="margin-top: -23px;">
										<label>Status</label>
										<select name="filter_status" class="form-control">
											<option> </option>
											<?php foreach($data1 as $dkey => $dvalue) { ?>
												<?php if($skey == $STARTUP_PAGE) { ?>
													<option value="<?php echo $dkey; ?>"><?php echo $dvalue?></option>
												<?php } else { ?>
													<option><?php echo $dvalue?></option>
												<?php } ?>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
					
				 	
					<div class="col-sm-10 col-sm-offset-1">
					<h4>
					<center>
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center><h4>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Delivery Boy Money Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;">Ref No</th>
							<th style="text-align: center;">Bill No</th>
							<th style="text-align: center;">Order ID</th>
							<th style="text-align: center;">Delivery Boy Name</th>
							<th style="text-align: center;">Food Total</th>
							<th style="text-align: center;">EXT. Amt.</th>
							<th style="text-align: center;">Return Amt.</th>
							<!-- <th style="text-align: center;">Edit</th> -->

						</tr>
						<?php if(isset($_POST['submit'])){ ?>
							<?php $discount = 0; $dboy_amount = 0; $grandtotal = 0; $vat = 0; $gst = 0; $foodtotal = 0; $fdistotal = 0; $liqtotal = 0;$ldistotal = 0; $discountvalue = 0; $afterdiscount = 0; $stax = 0; $roundoff = 0; $total = 0; $advance = 0; $packaging = 0;$packaging_cgst = 0;$packaging_sgst = 0;?>
							  	<?php foreach ($billdatas as $key => $value) {?>
							  		<tr>
							  			<td colspan="17"><b><?php echo $key ?><b></td>
							  		</tr>
							  		<?php $sdiscount = 0; $sgrandtotal = 0; $svat = 0; $sgst = 0; $sfoodtotal = 0; $sfdistotal = 0; $sliqtotal = 0;$sldistotal = 0; $sdiscountvalue = 0; $safterdiscount = 0; $sstax = 0; $sroundoff = 0; $stotal = 0; $sadvance = 0; $spackaging = 0;$spackaging_cgst = 0;$spackaging_sgst = 0;?>
								  	<?php foreach ($value as $data) { //echo'<pre>';print_r($data);exit;?>
								  		<tr >
									  		<td><?php echo $data['order_no'] ?></td>
									  		<td><?php echo $data['billno'] ?></td>
									  		<td><?php echo $data['order_id'] ?></td>
									  		<td><?php echo $data['dboy_name'] ?></td>
									  		<td><?php echo $data['grand_total'] ?></td>
									  		<td><?php echo $data['dboy_amount'] ?></td>
									  		<td><?php echo $data['given_amount'] ?></td>
									  	<!-- 	<td>
									  		<?php if($edit_bill == 1){ ?>
					  							<a href="index.php?route=catalog/deliveryboyupdate&token=<?php echo $token;?>&order_id=<?php echo $data['order_id']?>"class="btn btn-primary" onclick="return confirm('Are you sure?')">Edit</a>
					  							<?php } ?>
				  							</td> -->
									  		
									<?php } ?>
									
							  	<?php }  ?>
							  	<tr>
							  		<td colspan="17"><b style="font-size: 25px;">All Total </b></td>
							  	<tr>
							  		<td colspan="10"><b>Food Total :   <?php echo $famts ?></b></td>
							  		
							  	</tr>
							  	<tr>
							  		<td colspan="10"><b>Extra Amt. Total :   <?php echo $amts ?></b></td>
							  		
							  	</tr>	
							  	<tr>
							  		<td colspan="10"><b> Total :   <?php echo $amts + $famts ?></b></td>
							  		
							  	</tr>
							  	<tr>
							  		<b><td colspan="10">Return Amt. :   <?php echo $gamt ?> </td></b>
							  		
						  		</tr>
							  	</tr>
							  	
					  	<?php } ?>
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	

		function close_fun_1(){
			window.location.reload();
		}


		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>

</div>
<?php echo $footer; ?>