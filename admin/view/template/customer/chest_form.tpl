<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-customer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-customer" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <div class="row">
                <div style="display: none;" class="col-sm-2">
                  <ul class="nav nav-pills nav-stacked" id="address">
                    <li class="active"><a href="#tab-customer" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                  </ul>
                </div>
                <div class="col-sm-10">
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab-customer">
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-sport_type"><?php echo 'खेळ प्रकार / Sport Type'; ?></label>
                        <div class="col-sm-10">
                          <select name="sport_type" id="input-sport_type" class="form-control">
                            <option value="" disabled="disabled"><?php echo $text_select; ?></option>
                            <?php foreach ($sport_types as $skey => $svalue) { ?>
                              <?php if ($skey == $sport_type) { ?>
                                <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                          <?php if ($error_sport_type) { ?>
                          <div class="text-danger"><?php echo $error_sport_type; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required individual_class">
                        <label class="col-sm-2 control-label" for="input-start_number"><?php echo 'प्रारंभ क्रमांक / Starting Number'; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="start_number" value="<?php echo $start_number; ?>" placeholder="<?php echo 'Starting Number'; ?>" id="input-start_number" class="form-control" />
                          <?php if ($error_start_number) { ?>
                          <div class="text-danger"><?php echo $error_start_number; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $( document ).ready(function() {
      
    });
  </script>
</div>
<?php echo $footer; ?>