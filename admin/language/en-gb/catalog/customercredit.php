<?php
// Heading
$_['heading_title']     = 'Customer Credit';

// Text
$_['text_success']      = 'Success: You have modified credit item!';
$_['text_list']         = 'Customer Credit List';
$_['text_add']          = 'Add Credit';
$_['text_edit']         = 'Edit Credit';

$_['column_action']     = 'Action';