<?php
// Heading
$_['heading_title']     = 'MSR Refund';

// Text
$_['text_success']      = 'Success: You have modified MSR Refund!';
$_['text_list']         = 'MSR Refund List';
$_['text_add']          = 'Add MSR Refund';
$_['text_edit']         = 'Edit MSR Refund';

$_['column_action']     = 'Action';