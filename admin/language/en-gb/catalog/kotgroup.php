<?php
// Heading
$_['heading_title']     = 'Kotgroup';

// Text
$_['text_success']      = 'Success: You have modified Kotgroup!';
$_['text_list']         = 'Kotgroup List';
$_['text_add']          = 'Add Kotgroup';
$_['text_edit']         = 'Edit Kotgroup';
$_['text_upload']       = 'Your file was successfully uploaded!';

// Column
$_['column_name']       = 'Subcategory Name';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Description';
$_['entry_filename']    = 'Filename';
$_['entry_mask']        = 'Mask';

// Help
$_['help_filename']     = 'You can upload via the upload button or use FTP to upload to the sport directory and enter the details below.';
$_['help_mask']         = 'It is recommended that the filename and the mask are different to stop people trying to directly link to your Department.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify Department!';
$_['error_name']        = 'Department Name must be between 3 and 64 characters!';
$_['error_upload']      = 'Upload required!';
$_['error_filename']    = 'Filename must be between 3 and 128 characters!';
$_['error_exists']      = 'File does not exist!';
$_['error_mask']        = 'Mask must be between 3 and 128 characters!';
$_['error_filetype']    = 'Invalid file type!';
$_['error_product']     = 'Warning: This sport cannot be deleted as it is currently assigned to %s products!';