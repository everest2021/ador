<?php
// Heading
$_['heading_title']     = 'MSR Redeem';

// Text
$_['text_success']      = 'Success: You have modified MSR Redeem!';
$_['text_list']         = 'MSR Redeem List';
$_['text_add']          = 'Add MSR Redeem';
$_['text_edit']         = 'Edit MSR Redeem';

$_['column_action']     = 'Action';