<?php
// Heading
$_['heading_title']     = 'Supplier';

// Text
$_['text_success']      = 'Success: You have modified Supplier!';
$_['text_list']         = 'Supplier List';
$_['text_add']          = 'Add Supplier';
$_['text_edit']         = 'Edit Supplier';
$_['text_upload']       = 'Your file was successfully uploaded!';

// Column
$_['column_name']       = 'Supplier Name';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Supplier Name';
$_['entry_filename']    = 'Filename';
$_['entry_mask']        = 'Mask';

// Help
$_['help_filename']     = 'You can upload via the upload button or use FTP to upload to the sport directory and enter the details below.';
$_['help_mask']         = 'It is recommended that the filename and the mask are different to stop people trying to directly link to your Supplier.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify Supplier!';
$_['error_name']        = 'Supplier Name must be between 3 and 64 characters!';
$_['error_upload']      = 'Upload required!';
$_['error_filename']    = 'Filename must be between 3 and 128 characters!';
$_['error_exists']      = 'File does not exist!';
$_['error_mask']        = 'Mask must be between 3 and 128 characters!';
$_['error_filetype']    = 'Invalid file type!';
$_['error_product']     = 'Warning: This sport cannot be deleted as it is currently assigned to %s products!';