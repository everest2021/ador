<?php
// Heading
$_['heading_title']      = 'Purchase Entry Food';

// Text
$_['text_success']       = 'Success: You have modified Purchase Entry Food!';
$_['text_list']          = 'Purchase Entry Food';
$_['text_add']           = 'Add Purchase Entry Food';
$_['text_edit']          = 'View Purchase Entry Food';
$_['text_default']       = 'Default';
$_['text_percent']       = 'Percentage';
$_['text_amount']        = 'Fixed Amount';

// Column
$_['column_name']        = 'Supplier Name';
$_['column_sort_order']  = 'Sort Order';
$_['column_action']      = 'Action';

// Entry
$_['entry_name']         = 'Supplier Name';
$_['entry_store']        = 'Stores';
$_['entry_keyword']      = 'SEO URL';
$_['entry_image']        = 'Image';
$_['entry_sort_order']   = 'Sort Order';
$_['entry_type']         = 'Type';

// Help
$_['help_keyword']       = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify Purchase Entry!';
$_['error_name']         = 'Supplier! Name must be between 2 and 64 characters!';
$_['error_keyword']      = 'SEO URL already in use!';
$_['error_product']      = 'Warning: This Supplier cannot be deleted as it is currently assigned to %s products!';
