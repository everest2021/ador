<?php
// Heading
$_['heading_title']     = 'Meal Master';

// Text
$_['text_success']      = 'Success: You have modified Meal Master!';
$_['text_list']         = 'Meal List';
$_['text_add']          = 'Add Meal';
$_['text_edit']         = 'Edit Meal';
$_['text_upload']       = 'Your file was successfully uploaded!';

// Column
$_['column_meal_name']       = 'Meal Name';
$_['column_venue_name']       = 'Venue Name';
$_['column_rate']       = 'Rate';

$_['column_action']     = 'Action';

// Entry
$_['entry_meal_name']        = 'Meal Name';
$_['entry_venue_name']        = 'Venue Name';
$_['entry_rate']        = 'Rate';
$_['entry_capacity']        = 'Capacity';
$_['entry_filename']    = 'Filename';
$_['entry_mask']        = 'Mask';

// Help
$_['help_filename']     = 'You can upload via the upload button or use FTP to upload to the sport directory and enter the details below.';
$_['help_mask']         = 'It is recommended that the filename and the mask are different to stop people trying to directly link to your Department.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify Venue!';
$_['error_meal_name']        = 'Meal Name must be between 3 and 64 characters!';
$_['error_venue_name']        = 'Select Venue Name!';
$_['error_rate']        = 'Enter Rate!';

$_['error_upload']      = 'Upload required!';
$_['error_filename']    = 'Filename must be between 3 and 128 characters!';
$_['error_exists']      = 'File does not exist!';
$_['error_mask']        = 'Mask must be between 3 and 128 characters!';
$_['error_filetype']    = 'Invalid file type!';
$_['error_product']     = 'Warning: This sport cannot be deleted as it is currently assigned to %s products!';