<?php
// Heading
$_['heading_title']     = 'MSR Recharge';

// Text
$_['text_success']      = 'Success: You have modified MSR Recharge!';
$_['text_list']         = 'MSR Recharge List';
$_['text_add']          = 'Add MSR Recharge';
$_['text_edit']         = 'Edit MSR Recharge';

$_['column_action']     = 'Action';