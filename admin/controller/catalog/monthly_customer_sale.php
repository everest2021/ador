<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class Controllercatalogmonthlycustomersale extends Controller {

	public function index() {
		$this->load->language('catalog/monthly_customer_sale');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {


		$this->load->model('catalog/order');
		$this->load->language('catalog/monthly_customer_sale');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/monthly_customer_sale', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_cust_name'])){
			$data['filter_cust_name'] = $this->request->post['filter_cust_name'];
		}
		else{
			$data['filter_cust_name'] = '';
		}

		if(isset($this->request->post['filter_cust_id'])){
			$data['filter_cust_id'] = $this->request->post['filter_cust_id'];
		}
		else{
			$data['filter_cust_id'] = '';
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';
		$data['showdata'] = array();

		$data['final_datass'] = array();
		$final_datas = array();


		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);
			if($data['filter_cust_name'] != ''){
				$cust_id =  $data['filter_cust_id'];
			} else {
				$cust_id = '';
			}

		//echo"<pre>";print_r($this->request->post);exit;

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			$sql = "SELECT oi.`cust_name`,oi.`cust_address`, oi.`order_id`, oi.`gst` ,oi.`cust_contact`, oi.`grand_total`, oi.`bill_date`, oit.`billno` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON(oi.`order_id` = oit.`order_id`) WHERE 1=1 ";
			
			if (!empty($start_date)) {
				$sql .= " AND oi.`bill_date` >= '" . $this->db->escape($start_date) . "'";
			}
			if (!empty($end_date)) {
				$sql .= " AND oi.`bill_date` <= '" . $this->db->escape($end_date) . "'";
			}

			if (!empty($cust_id)) {
				$sql .= " AND oi.`cust_id` = '" . $this->db->escape($cust_id) . "'";
			}
				
			$sql .= " AND cust_contact > '0' AND oi.cancel_status = '0' AND oit.cancelstatus = '0'  GROUP BY oit.`order_id`";

		// echo"<pre>";print_r($sql);exit;

			
			$cust_datas = $this->db->query($sql)->rows;
			$b_count = 1;
			foreach ($cust_datas as $ckey => $cvalue) {
				$sql1 = "SELECT * FROM `oc_order_items_report` WHERE 1=1 ";
				if (!empty($start_date)) {
					$sql1 .= " AND `bill_date` >= '" . $this->db->escape($start_date) . "'";
				}
				if (!empty($end_date)) {
					$sql1 .= " AND `bill_date` <= '" . $this->db->escape($end_date) . "'";
				}

				$sql1 .= "AND order_id = '".$cvalue['order_id']."' ";

				$sql1 .=" AND cancelstatus = '0' ";
				$item_datas = $this->db->query($sql1)->rows;


				$sub_datas = array();
				$sr_no = 1;
				foreach($item_datas as $ikey => $ivalue){
					$sub_datas[] = array(
						'kot_no' => $ivalue['kot_no'],
						'name' => $ivalue['name'],
						'rate' => $ivalue['rate'],
						'qty' => $ivalue['qty'],
						'amt' => $ivalue['amt'],
						'sr_no' => $sr_no,
					);
					$sr_no ++;
				}

				$final_datas[] = array(
						'b_count' => $b_count,
						'order_id' => $cvalue['order_id'],
						'bill_date' => $cvalue['bill_date'],
						'cust_address' => $cvalue['cust_address'],
						'cust_name' => $cvalue['cust_name'],
						'cust_contact' => $cvalue['cust_contact'],
						'billno' => $cvalue['billno'],
						'gst' => $cvalue['gst'],
						'grand_total' => $cvalue['grand_total'],
						'sub_data' =>$sub_datas,
					);
				$b_count ++;
			}

			$data['final_datass'] = $final_datas;
			// echo'<pre>';
			// print_r($final_datas);
			// exit;
		}

		$data['action'] = $this->url->link('catalog/monthly_customer_sale', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/monthly_customer_sale', $data));
	}

	public function autocomplete_name(){
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		$json = array();
		if (isset($this->request->get['filter_cust_name'])) {
			$sql = "SELECT * FROM `oc_customerinfo` WHERE 1=1 ";
			if(!empty($this->request->get['filter_cust_name'])){
				$sql .= " AND `name` LIKE '%".$this->request->get['filter_cust_name']."%'";
			}
			
			//echo $sql;exit;
			$results = $this->db->query($sql)->rows;
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['c_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function export(){
		$this->load->model('catalog/order');
		$this->load->language('catalog/monthly_customer_sale');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/monthly_customer_sale', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$data['enddate'] = $this->request->get['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_cust_name'])){
			$data['filter_cust_name'] = $this->request->get['filter_cust_name'];
		}
		else{
			$data['filter_cust_name'] = '';
		}

		if(isset($this->request->get['filter_cust_id'])){
			$data['filter_cust_id'] = $this->request->get['filter_cust_id'];
		}
		else{
			$data['filter_cust_id'] = '';
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';


		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);
			if($data['filter_cust_name'] != ''){
				$cust_id =  $data['filter_cust_id'];
			} else {
				$cust_id = '';
			}

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			$sql = "SELECT oi.`cust_name`, oi.`cust_address`, oi.`order_id`, oi.`gst` ,oi.`cust_contact`, oi.`grand_total`, oi.`bill_date`, oit.`billno` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON(oi.`order_id` = oit.`order_id`) WHERE 1=1 ";
			
			if (!empty($start_date)) {
				$sql .= " AND oi.`bill_date` >= '" . $this->db->escape($start_date) . "'";
			}
			if (!empty($end_date)) {
				$sql .= " AND oi.`bill_date` <= '" . $this->db->escape($end_date) . "'";
			}

			if (!empty($cust_id)) {
				$sql .= " AND oi.`cust_id` = '" . $this->db->escape($cust_id) . "'";
			}
				
			$sql .= " AND cust_contact > '0' GROUP BY oit.`order_id`";

			$cust_datas = $this->db->query($sql)->rows;
			$b_count = 1;
			foreach ($cust_datas as $ckey => $cvalue) {
				$sql1 = "SELECT * FROM `oc_order_items_report` WHERE 1=1 ";
				if (!empty($start_date)) {
					$sql1 .= " AND `bill_date` >= '" . $this->db->escape($start_date) . "'";
				}
				if (!empty($end_date)) {
					$sql1 .= " AND `bill_date` <= '" . $this->db->escape($end_date) . "'";
				}

				$sql1 .= "AND order_id = '".$cvalue['order_id']."' ";

				$sql1 .=" AND cancelstatus = '0' ";
				$item_datas = $this->db->query($sql1)->rows;


				$sub_datas = array();
				$sr_no = 1;
				foreach($item_datas as $ikey => $ivalue){
					$sub_datas[] = array(
						'kot_no' => $ivalue['kot_no'],
						'name' => $ivalue['name'],
						'rate' => $ivalue['rate'],
						'qty' => $ivalue['qty'],
						'amt' => $ivalue['amt'],
						'sr_no' => $sr_no,
					);
					$sr_no ++;
				}

				$final_datas[] = array(
						'b_count' => $b_count,
						'order_id' => $cvalue['order_id'],
						'bill_date' => $cvalue['bill_date'],
						'cust_address' => $cvalue['cust_address'],

						'cust_name' => $cvalue['cust_name'],
						'cust_contact' => $cvalue['cust_contact'],
						'billno' => $cvalue['billno'],
						'gst' => $cvalue['gst'],
						'grand_total' => $cvalue['grand_total'],
						'sub_data' =>$sub_datas,
					);
				$b_count ++;
			}

			$data['final_datass'] = $final_datas;
			// echo'<pre>';
			// print_r($final_datas);
			// exit;
		}
		$data['action'] = $this->url->link('catalog/monthly_customer_sale', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		// echo "<pre>";
		// print_r($data);
		// exit;
		$html = $this->load->view('sale/monthly_customer_sale_html', $data);
		
				
		$filename = 'Customerwise_item_report';
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		echo $html;
		exit;
		// header('Content-disposition: attachment; filename=' . $filename);
		// header('Content-type: text/html');
		// echo $html;exit;

	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}


	public function prints() {
		//echo"<pre>";print_r($this->request->get);
		$this->load->model('catalog/order');
		$this->load->language('catalog/monthly_customer_sale');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/monthly_customer_sale', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$data['enddate'] = $this->request->get['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_cust_name'])){
			$data['filter_cust_name'] = $this->request->get['filter_cust_name'];
		}
		else{
			$data['filter_cust_name'] = '';
		}

		if(isset($this->request->get['filter_cust_id'])){
			$data['filter_cust_id'] = $this->request->get['filter_cust_id'];
		}
		else{
			$data['filter_cust_id'] = '';
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';
		$data['showdata'] = array();

		$data['final_datass'] = array();
		$final_datas = array();


		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);
			if($data['filter_cust_name'] != ''){
				$cust_id =  $data['filter_cust_id'];
			} else {
				$cust_id = '';
			}

		//echo"<pre>";print_r($this->request->get);exit;

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			$sql = "SELECT oi.`cust_name`,oi.`cust_address`, oi.`order_id`, oi.`gst` ,oi.`cust_contact`, oi.`grand_total`, oi.`bill_date`, oit.`billno` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON(oi.`order_id` = oit.`order_id`) WHERE 1=1 ";
			
			if (!empty($start_date)) {
				$sql .= " AND oi.`bill_date` >= '" . $this->db->escape($start_date) . "'";
			}
			if (!empty($end_date)) {
				$sql .= " AND oi.`bill_date` <= '" . $this->db->escape($end_date) . "'";
			}

			if (!empty($cust_id)) {
				$sql .= " AND oi.`cust_id` = '" . $this->db->escape($cust_id) . "'";
			}
				
			$sql .= " AND cust_contact > '0' AND oi.cancel_status = '0' AND oit.cancelstatus = '0'  GROUP BY oit.`order_id`";

		//echo"<pre>";print_r($sql);exit;
		// echo"<pre>";print_r($sql);exit;

			
			$cust_datas = $this->db->query($sql)->rows;
			$b_count = 1;
			foreach ($cust_datas as $ckey => $cvalue) {
				$sql1 = "SELECT * FROM `oc_order_items_report` WHERE 1=1 ";
				if (!empty($start_date)) {
					$sql1 .= " AND `bill_date` >= '" . $this->db->escape($start_date) . "'";
				}
				if (!empty($end_date)) {
					$sql1 .= " AND `bill_date` <= '" . $this->db->escape($end_date) . "'";
				}

				$sql1 .= "AND order_id = '".$cvalue['order_id']."' ";

				$sql1 .=" AND cancelstatus = '0' ";
				$item_datas = $this->db->query($sql1)->rows;


				$sub_datas = array();
				$sr_no = 1;
				foreach($item_datas as $ikey => $ivalue){
					$sub_datas[] = array(
						'kot_no' => $ivalue['kot_no'],
						'name' => $ivalue['name'],
						'rate' => $ivalue['rate'],
						'qty' => $ivalue['qty'],
						'amt' => $ivalue['amt'],
						'sr_no' => $sr_no,
					);
					$sr_no ++;
				}

				$final_datas[] = array(
						'b_count' => $b_count,
						'order_id' => $cvalue['order_id'],
						'bill_date' => $cvalue['bill_date'],
						'cust_address' => $cvalue['cust_address'],
						'cust_name' => $cvalue['cust_name'],
						'cust_contact' => $cvalue['cust_contact'],
						'billno' => $cvalue['billno'],
						'gst' => $cvalue['gst'],
						'grand_total' => $cvalue['grand_total'],
						'sub_data' =>$sub_datas,
					);
				$b_count ++;
			}

			// echo'<pre>';
			// print_r($final_datas);
			// exit;

			if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
			try {
		    // Enter the share name for your USB printer here
		    //$connector = new WindowsPrintConnector("XP-58C");
		    // Print a "Hello world" receipt" //
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
			    $printer->feed(1);
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad(date('d/m/Y'),30)."".date('H:i'));
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Customer Wise Item Report");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("BillNo",6)."  ".str_pad("Date",10)." ".str_pad("CustName",9)."      ".str_pad("CustNo ",7));
			  	$printer->feed(1);
			  	$printer->text(str_pad("SrNo",6)."  ".str_pad("KotNo",7)." ".str_pad("ItemName",12)." ".str_pad("Rate ",7)."".str_pad("Qty ",7));
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	foreach ($final_datas as $key => $value) { //echo"<pre>";print_r($value);exit;
			  		if($value!=array()){
			  			$printer->text("------------------------------------------------");
					  	$printer->feed(1);
			  			$printer->setJustification(Printer::JUSTIFY_LEFT);
				  		$printer->text(str_pad($value['b_count'],6)."".str_pad($value['bill_date'],9)."    ".substr($value['cust_name'],0,9)."     ".substr($value['cust_contact'],0,8));
				  		$printer->feed(1);

				  		foreach ($value['sub_data'] as $key => $avalue) {
				  			$printer->text(str_pad($avalue['sr_no'],6)."".str_pad($avalue['kot_no'],9)."".substr($avalue['name'],0,9)."     ".substr($avalue['rate'],0,6)."   ".str_pad($avalue['qty'],6));
				  			$printer->feed(1);
				  		}
					  	
					  	$printer->text(str_pad("Total ",29).$value['grand_total']);
					  	$printer->feed(1);
			  		}
				}
				$printer->setJustification(Printer::JUSTIFY_CENTER);
				$printer->text("---- End Report ----");
			  	$printer->feed(1);
	  			$printer->cut();
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
		} 
	}


}
?>