<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogClearTable extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		// $this->load->model('catalog/cleartable');
		$this->getList();
	}

	public function getList() {

		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		// $this->load->model('catalog/cleartable');
		
		$data['table'] = '';

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/cleartable', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['orders'] = array();
		
		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/cleartable', $data));


	}

	public function cleartbls(){
		// echo"<pre>";print_r($this->request->get);exit;
		if (isset($this->request->get['tbl'])) {
		
			$sqlss =$this->db->query("SELECT * FROM oc_order_info WHERE t_name = '".$this->request->get['tbl']."' AND `bill_status` = '0' AND `pay_method` = '0' AND `cancel_status` = '0' ORDER BY order_id DESC ");

			$sql =$this->db->query("SELECT * FROM oc_order_info WHERE t_name = '".$this->request->get['tbl']."' AND `bill_status` = '0' AND `pay_method` = '0' AND `cancel_status` = '0' ORDER BY order_id DESC ")->row;

			$this->db->query("DELETE FROM oc_order_info WHERE order_id ='".$sql['order_id']."' AND `bill_status` = '0' AND t_name = '".$this->request->get['tbl']."' AND `pay_method` = '0' AND `cancel_status` = '0' ");
			$this->db->query("DELETE FROM oc_order_items WHERE order_id ='".$sql['order_id']."' ");

			if($sqlss->num_rows > 0){
				$json['status'] = 1;
			} else {
				$json['status'] = 0;
			}

		}	//echo"<pre>";print_r($json);exit;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function checktbls(){
		// echo"<pre>";print_r($this->request->get);exit;
		if (isset($this->request->get['tbl'])) {
		
			$sql =$this->db->query("SELECT `order_id` FROM oc_order_info WHERE t_name = '".$this->request->get['tbl']."' AND `bill_status` = '0' AND `pay_method` = '0' AND `cancel_status` = '0' ORDER BY order_id DESC LIMIT 1")->row;


			$sqlss = $this->db->query("SELECT name, qty, rate FROM oc_order_items WHERE order_id ='".$sql['order_id']."' ")->rows;

			foreach($sqlss as $value){
				$json[] = array(
					'name'=> $value['name'],
					'qty'=> $value['qty'],
					'rate'=> $value['rate'],
				);
			}	
			// echo"<pre>";print_r($json);exit;
			

		}	

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function prints() {
		// echo"<pre>";print_r($this->request->get);exit;
		$this->load->model('catalog/order');
		$this->load->language('catalog/customerwise_item_report');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		if(isset($this->request->get['filter_cust_name'])){
			$data['filter_cust_name'] = $this->request->get['filter_cust_name'];
		}
		else{
			$data['filter_cust_name'] = '';
		}

		if (isset($this->request->get['tbl'])) {
		
			$sql =$this->db->query("SELECT `order_id` FROM oc_order_info WHERE t_name = '".$this->request->get['tbl']."' AND `pay_method` = '0' AND `cancel_status` = '0' ORDER BY order_id DESC LIMIT 1")->row;


			$sqlss = $this->db->query("SELECT name, qty, rate FROM oc_order_items WHERE order_id ='".$sql['order_id']."' ")->rows;

			foreach($sqlss as $value){
				$json[] = array(
					'name'=> $value['name'],
					'qty'=> $value['qty'],
					'rate'=> $value['rate'],
				);
			}	
		
			// echo'<pre>';
			// print_r($json);
			// exit;

			if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
			try {
		    // Enter the share name for your USB printer here
		    //$connector = new WindowsPrintConnector("XP-58C");
		    // Print a "Hello world" receipt" //
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
			    $printer->feed(1);
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad(date('d/m/Y'),30)."      ".date('H:i'));
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Clear Table Items");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Item Name",31)."".str_pad("qty",7)."".str_pad("Rate",9));
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	foreach ($json as $key => $value) { //echo"<pre>";print_r($value);exit;
			  		if($value!=array()){
			  			$printer->text("------------------------------------------------");
					  	$printer->feed(1);
			  			$printer->setJustification(Printer::JUSTIFY_LEFT);
				  		$printer->text(str_pad($value['name'],30)." ".str_pad($value['qty'],4)."    ".substr($value['rate'],0,9));
				  		$printer->feed(1);
			  		}
				}
				$printer->setJustification(Printer::JUSTIFY_CENTER);
				$printer->text("---- End Report ----");
			  	$printer->feed(1);
	  			$printer->cut();
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
		} 
	}

	
}