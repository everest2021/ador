<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
date_default_timezone_set('Asia/Kolkata');
class Controllercataloghourlyreport extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('catalog/hourly_report');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/hourly_report');
		$this->document->setTitle($this->language->get('heading_title'));

		if(isset($this->request->get['filter_startdate'])){
			$filter_startdate = $this->request->get['filter_startdate'];
		} else {
			$filter_startdate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$filter_enddate = $this->request->get['filter_enddate'];
		} else {
			$filter_enddate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_starttime'])){
			$filter_starttime = $this->request->get['filter_starttime'];
		} else {
			$filter_starttime = date('d-m-Y');
		}

		if(isset($this->request->get['filter_endtime'])){
			$filter_endtime = $this->request->get['filter_endtime'];
		} else {
			$filter_endtime = date('d-m-Y');
		}
		
		$url = '';

		if (isset($this->request->get['filter_startdate'])) {
			$url .= '&filter_startdate=' . $this->request->get['filter_startdate'];
		}

		if (isset($this->request->get['filter_starttime'])) {
			$url .= '&filter_starttime=' . $this->request->get['filter_starttime'];
		}
		
		if (isset($this->request->get['filter_endtime'])) {
			$url .= '&filter_endtime=' . $this->request->get['filter_endtime'];
		}
		// echo "<pre>";print_r($this->request->get);exit;

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/hourly_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		$filter_data = array(
			'filter_startdate' => $filter_startdate,
			'filter_starttime' => $filter_starttime,
			'filter_endtime' => $filter_endtime,
		);
		

		$all_cap = '';
		$all_cap1 = '';
		$all_cap2 = '';
		$all_cap3 = '';
		$all_cap4 = '';
		$all_cap5 = '';
		$all_cap6 = '';
		$all_cap7 = '';
		$all_cap8 = '';
		$all_cap9 = '';
		$all_cap10 = '';
		$all_cap11 = '';
		$all_cap12 = '';
		$all_cap13 = '';
		$all_cap14 = '';
		$all_cap15 = '';
		$all_cap16 = '';
		$all_cap17 = '';
		$all_cap18 = '';
		$all_cap19 = '';
		$all_cap20 = '';
		$all_cap21 = '';
		$all_cap22 = '';
		$all_cap23 = '';
		$cap_datas = array();
		$cap_datas1 = array();
		$cap_datas2 = array();
		$cap_datas3 = array();
		$cap_datas4 = array();
		$cap_datas5 = array();
		$cap_datas6 = array();
		$cap_datas7 = array();
		$cap_datas8 = array();
		$cap_datas9 = array();
		$cap_datas10 = array();
		$cap_datas11 = array();
		$cap_datas12 = array();
		$cap_datas13 = array();
		$cap_datas14 = array();
		$cap_datas15 = array();
		$cap_datas16 = array();
		$cap_datas17 = array();
		$cap_datas18 = array();
		$cap_datas19 = array();
		$cap_datas20 = array();
		$cap_datas21 = array();
		$cap_datas22 = array();
		$cap_datas23 = array();
		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql .= " AND `time` >= '01:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql .= " AND `time` <= '01:59:59'";
					}
					$sql .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					// echo"<pre>";print_r($sql);exit;
					$all_cap = $this->db->query($sql)->rows;
					
					foreach ($all_cap as $skey => $svalue) {
							$cap_datas[] = array(
								'date' => $svalue['date'],
								'order_id' => $svalue['totalbill'],
								'grand_total' => $svalue['t_grand_total'],
							);
					}
			
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql1 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql1 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql1 .= " AND `time` >= '02:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql1 .= " AND `time` <= '02:59:59'";
					}
					$sql1 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap1 = $this->db->query($sql1)->rows;
					// echo"<pre>";print_r($all_cap1);exit;
					
					foreach ($all_cap1 as $ekey => $evalue) {
							$cap_datas1[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql2 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql2 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql2 .= " AND `time` >= '03:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql2 .= " AND `time` <= '03:59:59'";
					}
					$sql2 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap2 = $this->db->query($sql2)->rows;
					// echo"<pre>";print_r($all_cap2);exit;
					
					foreach ($all_cap2 as $ekey => $evalue) {
							$cap_datas2[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql3 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql3 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql3 .= " AND `time` >= '04:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql3 .= " AND `time` <= '04:59:59'";
					}
					$sql3 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap3 = $this->db->query($sql3)->rows;
					// echo"<pre>";print_r($all_cap3);exit;
					
					foreach ($all_cap3 as $ekey => $evalue) {
							$cap_datas3[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql4 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql4 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql4 .= " AND `time` >= '05:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql4 .= " AND `time` <= '05:59:59'";
					}
					$sql4 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap4 = $this->db->query($sql4)->rows;
					// echo"<pre>";print_r($sql4);exit;
					
					foreach ($all_cap4 as $ekey => $evalue) {
							$cap_datas4[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql5 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill`  FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql5 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql5 .= " AND `time` >= '06:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql5 .= " AND `time` <= '06:59:59'";
					}
					$sql5 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap5 = $this->db->query($sql5)->rows;
					// echo"<pre>";print_r($all_cap5);exit;
					
					foreach ($all_cap5 as $ekey => $evalue) {
							$cap_datas5[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql6 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql6 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql6 .= " AND `time` >= '07:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql6 .= " AND `time` <= '07:59:59'";
					}
					$sql6 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap6 = $this->db->query($sql6)->rows;
					// echo"<pre>";print_r($all_cap6);exit;
					
					foreach ($all_cap6 as $ekey => $evalue) {
							$cap_datas6[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql7 = "SELECT *,SUM(`grand_total`) as `t_grand_total`,  COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql7 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql7 .= " AND `time` >= '08:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql7 .= " AND `time` <= '08:59:59'";
					}
					$sql7 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap7 = $this->db->query($sql7)->rows;
					// echo"<pre>";print_r($all_cap7);exit;
					
					foreach ($all_cap7 as $ekey => $evalue) {
							$cap_datas7[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql8 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql8 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql8 .= " AND `time` >= '09:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql8 .= " AND `time` <= '09:59:59'";
					}
					$sql8 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap8 = $this->db->query($sql8)->rows;
					// echo"<pre>";print_r($all_cap8);exit;
					
					foreach ($all_cap8 as $ekey => $evalue) {
							$cap_datas8[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql9 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql9 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql9 .= " AND `time` >= '10:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql9 .= " AND `time` <= '10:59:59'";
					}
					$sql9 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap9 = $this->db->query($sql9)->rows;
					// echo"<pre>";print_r($all_cap9);exit;
					
					foreach ($all_cap9 as $ekey => $evalue) {
							$cap_datas9[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql10 = "SELECT *,SUM(`grand_total`) as `t_grand_total`,  COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql10 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql10 .= " AND `time` >= '11:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql10 .= " AND `time` <= '11:59:59'";
					}
					$sql10 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap10 = $this->db->query($sql10)->rows;
					// echo"<pre>";print_r($all_cap10);exit;
					
					foreach ($all_cap10 as $ekey => $evalue) {
							$cap_datas10[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql11 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql11 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql11 .= " AND `time` >= '12:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql11 .= " AND `time` <= '12:59:59'";
					}
					$sql11 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap11 = $this->db->query($sql11)->rows;
					// echo"<pre>";print_r($all_cap11);exit;
					
					foreach ($all_cap11 as $ekey => $evalue) {
							$cap_datas11[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql12 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql12 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql12 .= " AND `time` >= '13:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql12 .= " AND `time` <= '13:59:59'";
					}
					$sql12 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap12 = $this->db->query($sql12)->rows;
					// echo"<pre>";print_r($all_cap12);exit;
					
					foreach ($all_cap12 as $ekey => $evalue) {
							$cap_datas12[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql13 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql13 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql13 .= " AND `time` >= '14:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql13 .= " AND `time` <= '14:59:59'";
					}
					$sql13 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap13 = $this->db->query($sql13)->rows;
					// echo"<pre>";print_r($all_cap13);exit;
					
					foreach ($all_cap13 as $ekey => $evalue) {
							$cap_datas13[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql14 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql14 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql14 .= " AND `time` >= '14:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql14 .= " AND `time` <= '14:59:59'";
					}
					$sql14 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap14 = $this->db->query($sql14)->rows;
					// echo"<pre>";print_r($all_cap14);exit;
					
					foreach ($all_cap14 as $ekey => $evalue) {
							$cap_datas14[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql15 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql15 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql15 .= " AND `time` >= '15:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql15 .= " AND `time` <= '15:59:59'";
					}
					$sql15 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap15 = $this->db->query($sql15)->rows;
					// echo"<pre>";print_r($all_cap15);exit;
					
					foreach ($all_cap15 as $ekey => $evalue) {
							$cap_datas15[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql16 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql16 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql16 .= " AND `time` >= '16:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql16 .= " AND `time` <= '16:59:59'";
					}
					$sql16 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap16 = $this->db->query($sql16)->rows;
					// echo"<pre>";print_r($all_cap16);exit;
					
					foreach ($all_cap16 as $ekey => $evalue) {
							$cap_datas16[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql17 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql17 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql17 .= " AND `time` >= '17:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql17 .= " AND `time` <= '17:59:59'";
					}
					$sql17 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap17 = $this->db->query($sql17)->rows;
					// echo"<pre>";print_r($all_cap17);exit;
					
					foreach ($all_cap17 as $ekey => $evalue) {
							$cap_datas17[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}


		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql18 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql18 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql18 .= " AND `time` >= '18:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql18 .= " AND `time` <= '18:59:59'";
					}
					$sql18 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap18 = $this->db->query($sql18)->rows;
					// echo"<pre>";print_r($all_cap18);exit;
					
					foreach ($all_cap18 as $ekey => $evalue) {
							$cap_datas18[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql19 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql19 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql19 .= " AND `time` >= '19:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql19 .= " AND `time` <= '19:59:59'";
					}
					$sql19 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap19 = $this->db->query($sql19)->rows;
					// echo"<pre>";print_r($all_cap19);exit;
					
					foreach ($all_cap19 as $ekey => $evalue) {
							$cap_datas19[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql20 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql20 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql20 .= " AND `time` >= '20:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql20 .= " AND `time` <= '20:59:59'";
					}
					$sql20 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap20 = $this->db->query($sql20)->rows;
					// echo"<pre>";print_r($all_cap20);exit;
					
					foreach ($all_cap20 as $ekey => $evalue) {
							$cap_datas20[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql21 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql21 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql21 .= " AND `time` >= '21:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql21 .= " AND `time` <= '21:59:59'";
					}
					$sql21 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap21 = $this->db->query($sql21)->rows;
					// echo"<pre>";print_r($all_cap21);exit;
					
					foreach ($all_cap21 as $ekey => $evalue) {
							$cap_datas21[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql22 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql22 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql22 .= " AND `time` >= '22:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql22 .= " AND `time` <= '22:59:59'";
					}
					$sql22 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap22 = $this->db->query($sql22)->rows;
					// echo"<pre>";print_r($all_cap22);exit;
					
					foreach ($all_cap22 as $ekey => $evalue) {
							$cap_datas22[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		// echo"<pre>";print_r($cap_datas1);exit;
		}

		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql23 = "SELECT *,SUM(`grand_total`) as `t_grand_total`, COUNT(`order_id`) as `totalbill` FROM `oc_order_info`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sql23 .= " AND `bill_date` = '" . $start_date . "'";
					}
					
					if (!empty($filter_data['filter_startdate'])) {
						$sql23 .= " AND `time` >= '23:00:00'";
					}
					if (!empty($filter_data['filter_startdate'])) {
						$sql23 .= " AND `time` <= '23:59:59'";
					}
					$sql23 .="AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'";
					
					$all_cap23 = $this->db->query($sql23)->rows;
					// echo"<pre>";print_r($all_cap23);exit;
					
					foreach ($all_cap23 as $ekey => $evalue) {
							$cap_datas23[] = array(
								'date' => $evalue['date'],
								'order_id' => $evalue['totalbill'],
								'grand_total' => $evalue['t_grand_total'],
							);
					}
			
		}
		$data['sql24'] = '';
		if(isset($this->request->get['filter_startdate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$start_date = date('Y-m-d', $startdate);
					
					$sql24 = $this->db->query("SELECT SUM(`grand_total`) as total FROM `oc_order_info`  WHERE `bill_date` = '" . $start_date . "' AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'")->row;

					$new = $sql24['total'];
					$data['total'] = $new ;
					
					$sql25 = $this->db->query("SELECT COUNT(`order_id`) as billno FROM `oc_order_info`  WHERE `bill_date` = '" . $start_date . "' AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'")->row;
                
					$new1 = $sql25['billno'];
		           //echo"<pre>";print_r($new1);exit;

					$data['billno'] = $new1 ;

		
		}


		$data['all_cap'] = $all_cap;
		$data['all_cap1'] = $all_cap1;
		$data['all_cap2'] = $all_cap2;
		$data['all_cap3'] = $all_cap3;
		$data['all_cap4'] = $all_cap4;
		$data['all_cap5'] = $all_cap5;
		$data['all_cap6'] = $all_cap6;
		$data['all_cap7'] = $all_cap7;
		$data['all_cap8'] = $all_cap8;
		$data['all_cap9'] = $all_cap9;
		$data['all_cap10'] = $all_cap10;
		$data['all_cap11'] = $all_cap11;
		$data['all_cap12'] = $all_cap12;
		$data['all_cap13'] = $all_cap13;
		$data['all_cap14'] = $all_cap14;
		$data['all_cap15'] = $all_cap15;
		$data['all_cap16'] = $all_cap16;
		$data['all_cap17'] = $all_cap17;
		$data['all_cap18'] = $all_cap18;
		$data['all_cap19'] = $all_cap19;
		$data['all_cap20'] = $all_cap20;
		$data['all_cap21'] = $all_cap21;
		$data['all_cap22'] = $all_cap22;
		$data['all_cap23'] = $all_cap23;
		$data['cap_datas'] = $cap_datas;
		$data['cap_datas1'] = $cap_datas1;
		$data['cap_datas2'] = $cap_datas2;
		$data['cap_datas3'] = $cap_datas3;
		$data['cap_datas4'] = $cap_datas4;
		$data['cap_datas5'] = $cap_datas5;
		$data['cap_datas6'] = $cap_datas6;
		$data['cap_datas7'] = $cap_datas7;
		$data['cap_datas8'] = $cap_datas8;
		$data['cap_datas9'] = $cap_datas9;
		$data['cap_datas10'] = $cap_datas10;
		$data['cap_datas11'] = $cap_datas11;
		$data['cap_datas12'] = $cap_datas12;
		$data['cap_datas13'] = $cap_datas13;
		$data['cap_datas14'] = $cap_datas14;
		$data['cap_datas15'] = $cap_datas15;
		$data['cap_datas16'] = $cap_datas16;
		$data['cap_datas17'] = $cap_datas17;
		$data['cap_datas18'] = $cap_datas18;
		$data['cap_datas19'] = $cap_datas19;
		$data['cap_datas20'] = $cap_datas20;
		$data['cap_datas21'] = $cap_datas21;
		$data['cap_datas22'] = $cap_datas22;
		$data['cap_datas23'] = $cap_datas23;
		$data['filter_startdate'] = $filter_startdate;
		$data['filter_enddate'] = $filter_enddate;
		
		
		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/hourly_report', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/hourly_report', $data));
	}
	public function name(){
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$sql = "SELECT * FROM `oc_waiter` WHERE 1=1 ";
			if(!empty($this->request->get['filter_name'])){
				$sql .= " AND `name` LIKE '%".$this->request->get['filter_name']."%'";
			}
			if(!empty($this->request->get['filter_type'])){
				if($this->request->get['filter_type'] == 1){
					$sql .= " AND `roll` = 'Captain' ";
				} elseif($this->request->get['filter_type'] == 2) {
					$sql .= " AND `roll` = 'Waiter' ";
				}
			}
			//echo $sql;exit;
			$results = $this->db->query($sql)->rows;
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['waiter_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	

}