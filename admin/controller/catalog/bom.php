<?php
class ControllerCatalogbom extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/bom');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/bom');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/bom', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_bomtype'])){
			$data['bomtype'] = $this->request->post['filter_bomtype'];
		}
		else{
			$data['bomtype'] = '';
		}

		if(isset($this->request->post['filter_bomcategory'])){
			$data['bomcategory'] = $this->request->post['filter_bomcategory'];
		}
		else{
			$data['bomcategory'] = '';
		}

		$datacategory = array();
		$data['categorys'] = array();
		$itemdata = array(); 
		$data['itemdatas'] = array();


		if(isset($this->request->post['filter_bomtype']) || isset($this->request->post['filter_bomcategory'])){

			$bomtype = $this->request->post['filter_bomtype'];
			$bomcategory = $this->request->post['filter_bomcategory'];

			if($bomtype == 'Menu Item'){
				$categorys = $this->db->query("SELECT * FROM oc_category")->rows;
				foreach($categorys as $category){
					$datacategory[] = array(
										'category_id' => $category['category_id'],
										'category' => $category['category']
											);
				}
				$itemdata = $this->db->query("SELECT * FROM oc_item WHERE item_category_id = '".$bomcategory."'")->rows;
			} elseif($bomtype == 'Finish / Semi Finish'){
				$stockcategorys = $this->db->query("SELECT * FROM oc_stockcategory")->rows;
				foreach($stockcategorys as $category){
					$datacategory[] = array(
										'category_id' => $category['id'],
										'category' => $category['name']
											);
				}
				$itemdata = $this->db->query("SELECT * FROM oc_stock_item WHERE category_id = '".$bomcategory."' AND (item_type = 'Finish' OR item_type = 'Semi Finish')")->rows;
			 } 
				//elseif($bomtype == 'Modifier'){
			// 	$datacategory[] = array();
			// 	$itemdata = array();
			// }
		}

		$data['itemdatas'] = $itemdata;
		$data['categorys'] = $datacategory;
		$data['bomtypes'] = array(
			'Menu Item' => 'Menu Item',
			'Finish / Semi Finish' => 'Finish / Semi Finish',
			//'Modifier'  => 'Modifier' 
		);
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/bom', 'token=' . $this->session->data['token'] . $url, true);
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/bom', $data));
	}
	
}