<?php
class ControllerCatalogtendering extends Controller {

	public function index() {
		//$this->load->model('catalog/settlement');
		$this->getList();
	}
	public function add() {
		
		$json['done'] = '<script>parent.closeIFrame();</script>';
		$json['info'] = 1;
		$this->response->setOutput(json_encode($json));
		//$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token']));
	}

	public function getlist(){
		$url = '';
		if(isset($this->request->get['orderidmodify'])){
			$data['orderidmodify'] = $this->request->get['orderidmodify'];
		} else{
			$data['orderidmodify'] = '';
		}
		

		if(isset($this->request->get['order_id'])){
			$data['order_id'] = $this->request->get['order_id'];
		} else{
			$data['order_id'] = '';
		}

		
		$order_id = $this->request->get['order_id'];
		$billdata = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;

		
		$data['billdata'] = $billdata;
		$data['add'] = $this->url->link('catalog/tendering/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['action'] = $this->url->link('catalog/tendering/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/tendering', $data));
	}

	public function autocomplete(){
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$results = $this->db->query("SELECT * FROM oc_customerinfo WHERE name LIKE '%".$this->request->get['filter_name']."%' OR contact LIKE '%".$this->request->get['filter_name']."%' ORDER BY c_id DESC LIMIT 0,5")->rows;
			foreach ($results as $result) {
				$json[] = array(
					'c_id' => $result['c_id'],
					'contact'        => $result['contact'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function credit(){
		$json = array();

		if (isset($this->request->get['c_id'])) {
			$credit = $this->db->query("SELECT SUM(credit) as credit FROM oc_customercredit WHERE c_id = '".$this->request->get['c_id']."'");
			if($credit->num_rows > 0){
				$credit = $credit->row['credit'];
				$deductedcredit = $this->db->query("SELECT SUM(onac) as onac FROM oc_order_info WHERE onaccust = '".$this->request->get['c_id']."'");
				if($deductedcredit->num_rows > 0){
					$totalcredit = $credit - $deductedcredit->row['onac'];
					$credit = $totalcredit;
				}
			} else{
				$credit = 0;
			}
			$json = array(
				'credit' => $credit
			);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

}