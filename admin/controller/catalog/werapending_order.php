<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogWeraPendingOrder extends Controller {
	private $error = array();

	public function index() {
		
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/werapending_order');
		$this->getItems();
	}

	public function add() {
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/werapending_order');
		
		$this->getList();
	}

	public function order_cancel() {
		// echo'innn';
		// exit;
		$this->load->language('catalog/order');
		$this->document->setTitle('Online Pending Order');
		$this->load->model('catalog/werapending_order');

		$body_data = array(
  			'merchant_id' => MERCHANT_ID
  		);
		
		$body = json_encode($body_data);
		//echo '<pre>'; print_r($body) ;exit;
		$url = "https://api.werafoods.com/pos/v2/merchant/cancelorders";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','X-Wera-Api-Key:'.WERA_API_KEY,'Accept: application/json' ));
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		$datas = json_decode($response,true);
		// echo'<pre>';
		// print_r($datas);
		// exit();
		curl_close($ch);
		if($datas['code'] == 1){
			foreach ($datas['details'] as $dkey => $dvalue) {
				$this->db->query("UPDATE oc_werafood_orders SET status = '4' WHERE order_id = '".$dvalue['order_id']."'");
					
			}
			$json['info'] = 1;
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
			//$this->session->data['success'] = 'Items Send Successfully!';
		} elseif($datas['code'] == 2) {
			$json['info'] = 2;
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		} else {
			$json['info'] = 0;
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
	}

	public function getList() {
		// echo'innn';
		// exit;
		$this->load->language('catalog/order');
		$this->document->setTitle('Online Pending Order');
		$this->load->model('catalog/werapending_order');

		
		$body_data = array(
  			'merchant_id' => MERCHANT_ID
  		);
		
		$body = json_encode($body_data);
		//echo '<pre>'; print_r($body) ;exit;
		$url = "https://api.werafoods.com/pos/v2/merchant/pendingorders";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','X-Wera-Api-Key:'.WERA_API_KEY,'Accept: application/json' ));
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		$datas = json_decode($response,true);
			/*echo'<pre>';
			print_r($datas);
			exit;*/

		foreach ($datas['details']['orders'] as $okey => $ovalue) {
			$order_datas = $this->db->query("SELECT order_id FROM oc_werafood_orders WHERE order_id = '".$ovalue['order_id']."'");
			if($order_datas->num_rows == 0) {
			$order_date_time = date('Y-m-d H:i:s', $ovalue['order_date_time']);
			$order_date = date('Y-m-d', $ovalue['order_date_time']);

				$this->db->query("INSERT INTO `oc_werafood_orders` SET 
							`order_id` = '".$ovalue['order_id']."',
							`restaurant_id` = '".$ovalue['restaurant_id']."',
							`restaurant_name` = '".$ovalue['restaurant_name']."',
							`external_order_id` = '".$ovalue['external_order_id']."',
							`order_from` = '".$ovalue['order_from']."',
							`status` = '".$ovalue['status']."',
							`order_date_time` = '".$order_date_time."',
							`date` = '".$order_date."',

							`enable_delivery` = '".$ovalue['enable_delivery']."',
							`net_amount` = '".$ovalue['net_amount']."',
							`gross_amount` = '".$ovalue['gross_amount']."',
							`payment_mode` = '".$ovalue['payment_mode']."',
							`order_type` = '".$ovalue['order_type']."',
							`order_instructions` = '".strip_tags(html_entity_decode( $ovalue['order_instructions'], ENT_QUOTES, 'UTF-8'))  ."',
							`foodready` = '".$ovalue['foodready']."',
							`packaging` = '".$ovalue['packaging']."',
							`packaging_cgst_percent` = '".$ovalue['packaging_cgst_percent']."',
							`packaging_sgst_percent` = '".$ovalue['packaging_sgst_percent']."',
							`packaging_cgst` = '".$ovalue['packaging_cgst']."',
							`packaging_sgst` = '".$ovalue['packaging_sgst']."',
							`gst` = '".$ovalue['gst']."',
							`cgst` = '".$ovalue['cgst']."',
							`sgst` = '".$ovalue['sgst']."',
							`discount` = '".$ovalue['discount']."',
							`order_otp` = '".$ovalue['order_otp']."',
							`is_pop_order` = '".$ovalue['is_pop_order']."',
							`delivery_charge` = '".$ovalue['delivery_charge']."'
				");

				$this->db->query("INSERT INTO `oc_werafood_order_customer` SET
						`order_id` = '".$ovalue['order_id']."',
						`name` = '".strip_tags(html_entity_decode( $ovalue['customer_details']['name'], ENT_QUOTES, 'UTF-8')) ."',
						`phone_number` = '".$ovalue['customer_details']['phone_number']."',
						`email` = '".$ovalue['customer_details']['email']."',
						`address` = '".strip_tags(html_entity_decode(  $ovalue['customer_details']['address'], ENT_QUOTES, 'UTF-8'))."',
						`delivery_area` = '".strip_tags(html_entity_decode( $ovalue['customer_details']['delivery_area'], ENT_QUOTES, 'UTF-8')) ."',
						`address_instructions` = '".strip_tags(html_entity_decode( $ovalue['customer_details']['address_instructions'], ENT_QUOTES, 'UTF-8'))."'
				");
				

				$this->db->query("INSERT INTO `oc_werafood_order_rider` SET
						`order_id` = '".$ovalue['order_id']."',
						`is_rider_available` = '".$ovalue['rider']['is_rider_available']."',
						`arrival_time` = '".$ovalue['rider']['arrival_time']."'
						
				");

				

				foreach ($ovalue['order_items'] as $ikey => $ivalue) {
					if(isset($ivalue['cgst'])){
						$cgst = $ivalue['cgst'];
					} else {
						$cgst = 0;
					}

					if(isset($ivalue['cgst_percent'])){
						$cgst_percent = $ivalue['cgst_percent'];
					} else {
						$cgst_percent = 0;
					}

					if(isset($ivalue['sgst'])){
						$sgst = $ivalue['sgst'];
					} else {
						$sgst = 0;
					}

					if(isset($ivalue['sgst_percent'])){
						$sgst_percent = $ivalue['sgst_percent'];
					} else {
						$sgst_percent = 0;
					}


					$this->db->query("INSERT INTO `oc_werafood_order_items` SET 
								`order_id` = '".$ovalue['order_id']."',
								`wera_item_id` = '".$ivalue['wera_item_id']."',
								`item_id` = '".$ivalue['item_id']."',
								`item_name` = '".$ivalue['item_name']."',
								`item_unit_price` = '".$ivalue['item_unit_price']."',
								`subtotal` = '".$ivalue['subtotal']."',
								`item_quantity` = '".$ivalue['item_quantity']."',
								`gst` = '".$ivalue['gst']."',
								`gst_percent` = '".$ivalue['gst_percent']."',
								`packaging` = '".$ivalue['packaging']."',
								`instructions` =   '".strip_tags(html_entity_decode($ivalue['instructions'], ENT_QUOTES, 'UTF-8'))."',
								`discount` = '".$ivalue['discount']."',
								`cgst` = '".$cgst."',
								`sgst` = '".$sgst."',
								`cgst_percent` = '".$cgst_percent."',
								`sgst_percent` = '".$sgst_percent."',
								`packaging_gst` = '".$ivalue['packaging_gst']."'
					");
					if($ivalue['variants']){
						foreach ($ivalue['variants'] as $vkey => $vvalue) {
							$this->db->query("INSERT INTO `oc_werafood_order_variants` SET
										`order_id` = '".$ovalue['order_id']."',
										`order_item_id` = '".$ivalue['item_id']."',
										`size_id` = '".$vvalue['size_id']."',
										`size_name` = '".$vvalue['size_name']."',
										`price` = '".$vvalue['price']."'
							");
						}
					}

					if($ivalue['addons']){
						foreach ($ivalue['addons'] as $akey => $avalue) {
							$this->db->query("INSERT INTO `oc_werafood_order_addons` SET
										`order_id` = '".$order_id."',
										`order_item_id` = '".$ivalue['item_id']."',
										`addon_id` = '".$avalue['addon_id']."',
										`name` = '".$avalue['name']."',
										`price` = '".$avalue['price']."',
										`cgst` = '".$avalue['cgst']."',
										`sgst` = '".$avalue['sgst']."',
										`cgst_percent` = '".$avalue['cgst_percent']."',
										`sgst_percent` = '".$avalue['sgst_percent']."'
							");
						}
					}
				}
			}
			$json['order_id'] = $ovalue['order_id'];
			$json['order_from'] = $ovalue['order_from'];
		}

		curl_close($ch);

		

		if($datas['code'] == 1){
			$json['info'] = 1;
			
			$this->response->addHeader('Content-Type: application/json');

			$this->response->setOutput(json_encode($json));
			//$this->session->data['success'] = 'Items Send Successfully!';
		} else {
			$json['info'] = 0;
			$json['order_id'] = '';
			$json['order_from'] = '';
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
			//$this->session->data['errors'] = 'Items Not Send !';
		}
		// echo 'done';
		// exit;
		// $data['superadmin'] = $this->session->data['superadmin'];
		// $data['header'] = $this->load->controller('common/header');
		// $data['column_left'] = $this->load->controller('common/column_left');
		// $data['footer'] = $this->load->controller('common/footer');
		// // echo'<pre>';
		// // print_r($data);
		// // exit;
		// $this->response->setOutput($this->load->view('catalog/werapending_order', $data));



	}

	public function addstore() {
		// echo'innn';
		// exit;
		$this->load->language('catalog/order');
		$this->document->setTitle('Online Pending Order');
		$this->load->model('catalog/werapending_order');
		$json = array();

		$this->load->model('catalog/order');
		$setting_value_hotel_name = $this->model_catalog_order->get_settings('HOTEL_NAME');
		if (utf8_strlen($setting_value_hotel_name) == '' ) {
			$json['error']['hotel_name'] = 'Please Add Hotel Name In Setting';
		}
		$setting_value_hotel_add = $this->model_catalog_order->get_settings('HOTEL_ADD');
		if (utf8_strlen($setting_value_hotel_add) == '' ) {
			$json['error']['hotel_add'] = 'Please Add Hotel Address In Setting';
		}

		$setting_contact = $this->model_catalog_order->get_settings('CONTACT_NUMBER');
		if (utf8_strlen($setting_contact) < 10) {
			$json['error']['hotel_phone'] = 'Please Add Valid Phone Number In Setting';
		}

		$setting_email = $this->model_catalog_order->get_settings('EMAIL_USERNAME');
		if ((utf8_strlen($setting_email) > 96) || !filter_var($setting_email, FILTER_VALIDATE_EMAIL)) {
			$json['error']['hotel_email'] = 'Please Enter Valid Email In Setting';
		}
		if (!$json) {
			$stores_array[] = array(
				'city' => 'MUMBAI',
				'name' => $setting_value_hotel_name,
				'min_pickup_time' => 900,
  				'min_delivery_time' => 1800,
  				'contact_phone' => $setting_contact,
  				'notification_phones' =>  array (0 => $setting_contact),
  				'ref_id' => HOTEL_LOCATION_ID,
  				'min_order_value' => 200,
  				'hide_from_ui' => false,
  				'address' => $setting_value_hotel_add,
  				'notification_emails' =>   array ( 0 => $setting_email),
  				'zip_codes' => array(),
  				'geo_longitude' => '',
  				'active' => true,
  				'geo_latitude' => '',
  				'ordering_enabled' => true,
  				'translations' => array(),
  				'excluded_platforms' => array(),
  				'platform_data' => array(),
  				'timings' => array(),
			);
			$store_array = array (
				'stores' =>  $stores_array
			); 
			//echo "<pre>";print_r($store_array);exit;
			$body = json_encode($store_array);

			//$body = json_encode($store_array, JSON_PRETTY_PRINT);//echo "<pre>";print_r($body);exit;
			
			$curl = curl_init();

				curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://pos-int.urbanpiper.com/external/api/v1/stores/',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS => $body ,
			  CURLOPT_HTTPHEADER => array(
			    'Authorization:'.URBANPIPER_API_KEY,
			    'Content-Type: application/json',
			  ),
			));

			$response = curl_exec($curl);
			$datas = json_decode($response,true);
			echo "<pre>";print_r($datas);exit;
			curl_close($curl);

			//echo "<pre>";print_r( $response );exit;
		
			$json['success'] = "Successfull Data Send";
		}
		$this->response->addHeader('Content-Department: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function order_ready(){
		$this->load->model('catalog/order');
		$this->load->model('catalog/msr_recharge');
		$this->load->language('catalog/order');

		if(isset($this->request->get['order_id'])){
			$orderid = $this->request->get['order_id'];
			// echo'inn';
			// 	exit;
			$edit = 0;
			if($orderid != ''){
				$body_data = array(
	  				'merchant_id' => MERCHANT_ID,
	  				'order_id' => $orderid,
	  			);
		
				$body = json_encode($body_data);
				//echo '<pre>'; print_r($body) ;exit;
				//$url = "https://api.werafoods.com/pos/v2/order/food-ready";
				$url = "https://api.werafoods.com/pos/v1/order/food-ready";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','X-Wera-Api-Key:'.WERA_API_KEY,'Accept: application/json' ));
				
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$response  = curl_exec($ch);
				$datas = json_decode($response,true);
					// echo'<pre>';
					// print_r($response);
					// exit;
				if($datas['code'] == 1){
					$this->db->query("UPDATE oc_werafood_orders SET status = 'Order Placed' WHERE order_id = '".$orderid."'");
					$json['info'] = 1;
				} elseif($datas['code'] == 2) {
					$json['info'] = 2;
					$json['reason'] = $data['msg'];
				} else {
					$json['info'] = 0;
				}
				
			}
		}

		$json['done'] = '<script>parent.closeIFrame();</script>';
		$this->response->setOutput(json_encode($json));
	}



	public function getItems() {
		// echo'innn';
		// exit;
		$this->load->language('catalog/order');
		$this->document->setTitle('Online Order');
		$this->load->model('catalog/werapending_order');

		if(isset($this->request->post['store_status'])) {
			$data['store_status'] = $this->request->post['store_status'];
		} else {
			$data['store_status'] = '1';
		}

		$pending_orders = array();
		$data['pending_orders'] = array();
		$order_datas = $this->db->query("SELECT * FROM `oc_orders_app` WHERE (online_order_status = 'Acknowledged' OR online_order_status = 'Food Ready') order by online_order_id ASC")->rows;
		$sr_no = 1;
		foreach ($order_datas as $okey => $ovalue) {
			$order_time = date('h:i:s', strtotime($ovalue['order_time']));
			$online_deliverydatetime = '';
			if($ovalue['online_deliverydatetime'] != ''){
				$online_deliverydatetime =  date('d-m-Y H:i:s', $ovalue['online_deliverydatetime']/1000); //date("d-m-Y h:i:sa", date($ovalue['online_deliverydatetime']));
			}

			$local_orderdate = '';
			if($ovalue['order_date'] != ''){
				$local_orderdate =  date('d-m-Y', strtotime($ovalue['order_date'])); //date("d-m-Y h:i:sa", date($ovalue['online_deliverydatetime']));
			}
			
			$pending_orders[] = array(
				'sr_no' => $sr_no,
				'order_id' => $ovalue['online_order_id'],
				//'order_from' => $ovalue['order_from'],
				'payment_mode' => $ovalue['online_payment_method'],
				'order_type' => $ovalue['online_order_type_placed_delie'],
				'gross_amount' => $ovalue['grand_tot'],
				'status' => $ovalue['online_order_status'],
				'order_time' => $local_orderdate.' '.$order_time,
				'delivery_time' => $online_deliverydatetime,
			);
			$sr_no++;
		} 

		$data['pending_orders'] = $pending_orders;
		/*echo'<pre>';
		print_r($pending_orders); 
		exit;*/
		$data['token'] = $this->session->data['token'];

		$data['superadmin'] = $this->session->data['superadmin'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('catalog/werapending_order', $data));
	}


	public function bill_print() {
		
		if (isset($this->request->get['order_id'])) {

			$orderid = $this->request->get['order_id'];
			$wera_order_datas = $this->db->query("SELECT * FROM oc_werafood_orders WHERE order_id = '".$orderid."'")->row;

			$order_ids = $this->db->query("SELECT * FROM oc_order_info WHERE wera_order_id = '".$orderid."'")->row;

			$order_id = $order_ids['order_id'];
			$ans = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
		
			$duplicate = '0';
			$advance_id = '0';
			$advance_amount = '0';
			$grand_total = '0';
			$orderno = '0';
			// 	echo'<pre>';
			// print_r($ans);
			// exit;
			if(($ans['bill_status'] == 1 && $duplicate == '0') || ($ans['bill_status'] == 1 && $duplicate == '1')){
			
				date_default_timezone_set('Asia/Kolkata');
				$this->load->language('customer/customer');
				$this->load->model('catalog/order');
				$merge_datas = array();
				$this->document->setTitle('BILL');
				$te = 'BILL';
				
				$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
				$last_open_dates = $this->db->query($last_open_date_sql);
				if($last_open_dates->num_rows > 0){
					$last_open_date = $last_open_dates->row['bill_date'];
				} else {
					$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
					$last_open_dates = $this->db->query($last_open_date_sql);
					if($last_open_dates->num_rows > 0){
						$last_open_date = $last_open_dates->row['bill_date'];
					} else {
						$last_open_date = date('Y-m-d');
					}
				}

				$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

				$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

				if($last_open_dates_liq->num_rows > 0){
					$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
				} else {
					$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

					$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

					if($last_open_dates_liq->num_rows > 0){
						$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
					} else {
						$last_open_date_liq = date('Y-m-d');
					}
				}

				$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
				$last_open_dates_order = $this->db->query($last_open_date_sql_order);
				if($last_open_dates_order->num_rows > 0){
					$last_open_date_order = $last_open_dates_order->row['bill_date'];
				} else {
					$last_open_date_order = date('Y-m-d');
				}

				if($ans['order_no'] == '0'){
					$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
					$orderno = 1;
					if(isset($orderno_q['order_no'])){
						$orderno = $orderno_q['order_no'] + 1;
					}

					$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
					if($kotno2->num_rows > 0){
						$kot_no2 = $kotno2->row['billno'];
						$kotno = $kot_no2 + 1;
					} else{
						$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
						if($kotno2->num_rows > 0){
							$kot_no2 = $kotno2->row['billno'];
							$kotno = $kot_no2 + 1;
						} else {
							$kotno = 1;
						}
					}

					$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
					if($kotno1->num_rows > 0){
						$kot_no1 = $kotno1->row['billno'];
						$botno = $kot_no1 + 1;
					} else{
						$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
						if($kotno1->num_rows > 0){
							$kot_no1 = $kotno1->row['billno'];
							$botno = $kot_no1 + 1;
						} else {
							$botno = 1;
						}
					}

					$this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0' ");
					$this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0' ");

					//$ansbb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
					if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
						$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
					} else{
						$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
					}
					$apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
					if($apporder['app_order_id'] != '0'){
						$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
					}
					$this->db->query($update_sql);
				}

				if($advance_id != '0'){
					$this->db->query("UPDATE oc_order_info SET advance_billno = '".$advance_id."', advance_amount = '".$advance_amount."', grand_total = '".$grand_total."' WHERE order_id = '".$order_id."'");
				}
				$anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
				// echo'<pre>';
				// print_r($anss);
				// exit;

				$testfood = array();
				$testliq = array();
				$testtaxvalue1food = 0;
				$testtaxvalue1liq = 0;
				$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
				foreach($tests as $test){
					$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
					$testfoods[] = array(
						'tax1' => $test['tax1'],
						'amt' => $amt,
						'tax1_value' => $test['tax1_value']
					);
				}
				
				$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
				foreach($testss as $testa){
					$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
					$testliqs[] = array(
						'tax1' => $testa['tax1'],
						'amt' => $amts,
						'tax1_value' => $testa['tax1_value']
					);
				}
				
				$infosl = array();
				$infos = array();
				$flag = 0;
				$totalquantityfood = 0;
				$totalquantityliq = 0;
				$disamtfood = 0;
				$disamtliq = 0;
				$modifierdatabill = array();
				foreach ($anss as $lkey => $result) {
					foreach($anss as $lkeys => $results){
						if($lkey == $lkeys) {

						} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['code'] = '';
								}
							}
						} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['qty'] = $result['qty'] + $results['qty'];
									if($result['nc_kot_status'] == '0'){
										$result['amt'] = $result['qty'] * $result['rate'];
									}
								}
							}
						}
					}
					
					if($result['code'] != ''){
						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
						if($decimal_mesurement->num_rows > 0){
							if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
								$qty = (int)$result['qty'];
							} else {
									$qty = $result['qty'];
							}
						} else {
							$qty = $result['qty'];
						}
						if($result['is_liq']== 0){
							$infos[] = array(
								'id'			=> $result['id'],
								'billno'		=> $result['billno'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2'],
								'discount_value'=> $result['discount_value']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityfood = $totalquantityfood + $result['qty'];
							$disamtfood = $disamtfood + $result['discount_value'];
						} else {
							$flag = 1;
							$infosl[] = array(
								'id'			=> $result['id'],
								'billno'		=> $result['billno'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2'],
								'discount_value'=> $result['discount_value']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityliq = $totalquantityliq + $result['qty'];
							$disamtliq = $disamtliq + $result['discount_value'];
						}
					}
				}
				
				$merge_datas = array();
				if($orderno != '0'){
					$merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$orderno."' AND `order_no` <> '".$orderno."' ")->rows;
				}
				
				if($ans['parcel_status'] == '0'){
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal = ($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
					} else {
						$gtotal = ($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
					}
				} else {
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal =($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
					} else {
						$gtotal =($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
					}
				}

				
				
				$csgst=$ans['gst']/2;
				$csgsttotal = $ans['gst'];

				$ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 125);
				$ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
				$ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
				$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
				if($ans['advance_amount'] == '0.00'){
					$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
				} else{
					$gtotal = utf8_substr(html_entity_decode($ans['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
				}
				//$gtotal = ceil($gtotal);
				$gtotal = round($gtotal);

				$printtype = '';
				$printername = '';

				if ($printtype == '' || $printername == '' ) {
					$printtype = $this->user->getPrinterType();
					$printername = $this->user->getPrinterName();
					$bill_copy = 1;
				}

				if ($printtype == '' || $printername == '' ) {
					$locationData =  $this->db->query("SELECT `bill_copy`, `bill_printer_type`, `bill_printer_name` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
					if($locationData->num_rows > 0){
						$locationData = $locationData->row;
						$printtype = $locationData['bill_printer_type'];
						$printername = $locationData['bill_printer_name'];
						$bill_copy = $locationData['bill_copy'];
					} else{
						$printtype = '';
						$printername = '';
						$bill_copy = 1;
					}
				}
				// $this->db->query("UPDATE oc_werafood_orders SET status = 'Order Placed' WHERE order_id = '".$orderid."'");
				
				if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
					$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
				 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
				}
				$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
				$LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');

					if($printerModel ==0){
						try {
							    if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 		$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							 	}
							 	if($connector != ''){
								    $printer = new Printer($connector);
								    $printer->selectPrintMode(32);

								   	$printer->setEmphasis(true);
								   	for($i = 1; $i <= $bill_copy; $i++){
									   	$printer->setTextSize(2, 1);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
									    $printer->feed(1);
									    $printer->setTextSize(1, 1);
									    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
									    if($ans['bill_status'] == 1 && $duplicate == '1'){
									    	$printer->feed(1);
									    	$printer->text("Duplicate Bill");
									    }
									    $printer->setJustification(Printer::JUSTIFY_CENTER);
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
										if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
											
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										 	$printer->text(("Name : ".$ans['cust_name']));
											$printer->feed(1);
											$printer->text(("Mobile :".$ans['cust_contact']));
											$printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
										 	$printer->text(("Address : ".$ans['cust_address']));
											$printer->feed(1);
											$printer->text("Gst No :".$ans['gst_no']);
											$printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
										 	$printer->text(("Name : ".$ans['cust_name']));
										    $printer->feed(1);
										    $printer->text("Address : ".$ans['cust_address']);
										    $printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
										 	$printer->text(("Mobile :".$ans['cust_contact']));
										    $printer->feed(1);
										    $printer->text("Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
										 	$printer->text("Name : ".$ans['cust_name']);
										    $printer->feed(1);
										    $printer->text("Gst No :".$ans['gst_no']);
										    $printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
										 	$printer->text("Mobile :".$ans['cust_contact']);
										    $printer->feed(1);
										    $printer->text("Address : ".$ans['cust_address']);
										    $printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Name :".$ans['cust_name']."");
										    $printer->feed(1);
										}
										else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Mobile :".$ans['cust_contact']."");
										    $printer->feed(1);
										}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Address : ".$ans['cust_address']."");
										    $printer->feed(1);
										}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
										    $printer->text("Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}else{
										    $printer->text("Name : ".$ans['cust_name']);
										    $printer->feed(1);
										    $printer->text("Mobile :".$ans['cust_contact']."");
										    $printer->feed(1);
										    $printer->text("Address : ".$ans['cust_address']);
										    $printer->feed(1);
										    $printer->text("Gst No :".$ans['gst_no']);
										    $printer->feed(1);
										}
										$printer->text(str_pad("User Id :".$ans['login_id'],30)."K.Ref.No :".$order_id);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($infos){
									   			
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
									   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('H:i:s'));
									   		$printer->feed(1);
											   		
											/*$printer->text("Order From: ".$wera_order_datas['order_from']."");
									   		$printer->feed(1);

									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   		$printer->setTextSize(2, 2);
									   		$printer->text("Order No: ".$wera_order_datas['order_id']."");
									   		$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);*/
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	/*$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
										    $printer->feed(1);*/
									   		$printer->setEmphasis(false);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",24)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
											$printer->feed(1);
									   	 	$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infos as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
										    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
									    	 	if($modifierdatabill != array()){
												    	foreach($modifierdatabill as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
											    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
														    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}
											    	$total_items_normal ++ ;
										    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

										    }
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											/*// foreach($testfoods as $tkey => $tvalue){
											// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										 //    	$printer->feed(1);
											// }
											$printer->text("----------------------------------------------");
											$printer->feed(1);*/
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ans['fdiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
												$printer->feed(1);
											} elseif($ans['discount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
												$printer->feed(1);
											}
											$printer->text(str_pad("",35)."SCGST :".$csgst."");
											$printer->feed(1);
											$printer->text(str_pad("",35)."CCGST :".$csgst."");
											$printer->feed(1);
											$printer->text(str_pad("",30)."Packaging :".$wera_order_datas['packaging']."");
											$printer->feed(1);
											$printer->text(str_pad("",25)."Packaging CGST :".$wera_order_datas['packaging_cgst']."");
											$printer->feed(1);
											$printer->text(str_pad("",25)."Packaging SGST :".$wera_order_datas['packaging_sgst']."");
											$printer->feed(1);
											
											//order_ids
											
											$printer->setEmphasis(true);
											$printer->text(str_pad("",29)."Net total :".($ans['grand_total'])."");
											$printer->setEmphasis(false);
										}
									   	
										$printer->feed(1);
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
										$printer->setTextSize(2, 2);
										$printer->setJustification(Printer::JUSTIFY_RIGHT);
										$printer->text("GRAND TOTAL  :  ".round($ans['grand_total']));
										$printer->setTextSize(1, 1);
										$printer->feed(1);
										
										$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
										if($SETTLEMENT_status == '1'){
											if(isset($this->session->data['credit'])){
												$credit = $this->session->data['credit'];
											} else {
												$credit = '0';
											}
											if(isset($this->session->data['cash'])){
												$cash = $this->session->data['cash'];
											} else {
												$cash = '0';
											}
											if(isset($this->session->data['online'])){
												$online = $this->session->data['online'];
											} else {
												$online ='0';
											}

											if(isset($this->session->data['onac'])){
												$onac = $this->session->data['onac'];
												$onaccontact = $this->session->data['onaccontact'];
												$onacname = $this->session->data['onacname'];

											} else {
												$onac ='0';
											}
										}
										if($SETTLEMENT_status=='1'){
											if($credit!='0' && $credit!=''){
												$printer->text("PAY BY: CARD");
											}
											if($online!='0' && $online!=''){
												$printer->text("PAY BY: ONLINE");
											}
											if($cash!='0' && $cash!=''){
												$printer->text("PAY BY: CASH");
											}
											if($onac!='0' && $onac!=''){
												$printer->text("PAY BY: ON.ACCOUNT");
												$printer->feed(1);
												$printer->text("Name: '".$onacname."'");
												$printer->feed(1);
												$printer->text("Contact: '".$onaccontact."'");
											}
										}
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									   
										
										$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
										$printer->feed(1);
									   	/*$printer->setTextSize(2, 2);

										$printer->text("Order OTP :".($wera_order_datas['order_otp']) );
										$printer->feed(1);*/
									   	$printer->setTextSize(1, 1);

										if($this->model_catalog_order->get_settings('TEXT1') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT1'));
											$printer->feed(1);
										}
										if($this->model_catalog_order->get_settings('TEXT2') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT2'));
											$printer->feed(1);
										}
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										if($this->model_catalog_order->get_settings('TEXT3') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT3'));
										}
										$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	$printer->setTextSize(2, 2);
										$printer->text("Order From: ".$wera_order_datas['order_from']."");
										$printer->feed(1);
								   		$printer->text("Order No: ".$wera_order_datas['order_id']."");
								   		$printer->feed(1);
										$printer->text("Order OTP :".($wera_order_datas['order_otp'] ));
										$printer->feed(2);
										$printer->cut();
									}
									// Close printer //
								    $printer->close();
							    	$this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

								    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
							    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							    $this->session->data['warning'] = $printername." "."Not Working";
							}
						
					}
					else{  // 45 space code starts from here

						try {
							    if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 		$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							 	}
							 	if($connector != ''){
								    $printer = new Printer($connector);
								    $printer->selectPrintMode(32);

								   	$printer->setEmphasis(true);
								   	// $printer->text('45 Spacess');
					    			$printer->feed(1);
								   	for($i = 1; $i <= $bill_copy; $i++){
									   	$printer->setTextSize(2, 1);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
									    $printer->feed(1);
									    $printer->setTextSize(1, 1);
									    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
									    if($ans['bill_status'] == 1 && $duplicate == '1'){
									    	$printer->feed(1);
									    	$printer->text("Duplicate Bill");
									    }
									    $printer->setJustification(Printer::JUSTIFY_CENTER);
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
										if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
											
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										 	$printer->text(("Name : ".$ans['cust_name']));
											$printer->feed(1);
											$printer->text(("Mobile :".$ans['cust_contact']));
											$printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
										 	$printer->text(("Address : ".$ans['cust_address']));
											$printer->feed(1);
											$printer->text("Gst No :".$ans['gst_no']);
											$printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
										 	$printer->text(("Name : ".$ans['cust_name']));
										    $printer->feed(1);
										    $printer->text("Address : ".$ans['cust_address']);
										    $printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
										 	$printer->text(("Mobile :".$ans['cust_contact']));
										    $printer->feed(1);
										    $printer->text("Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
										 	$printer->text("Name : ".$ans['cust_name']);
										    $printer->feed(1);
										    $printer->text("Gst No :".$ans['gst_no']);
										    $printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
										 	$printer->text("Mobile :".$ans['cust_contact']);
										    $printer->feed(1);
										    $printer->text("Address : ".$ans['cust_address']);
										    $printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Name :".$ans['cust_name']."");
										    $printer->feed(1);
										}
										else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Mobile :".$ans['cust_contact']."");
										    $printer->feed(1);
										}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Address : ".$ans['cust_address']."");
										    $printer->feed(1);
										}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
										    $printer->text("Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}else{
										    $printer->text("Name : ".$ans['cust_name']);
										    $printer->feed(1);
										    $printer->text("Mobile :".$ans['cust_contact']."");
										    $printer->feed(1);
										    $printer->text("Address : ".$ans['cust_address']);
										    $printer->feed(1);
										    $printer->text("Gst No :".$ans['gst_no']);
										    $printer->feed(1);
										}
										$printer->text(str_pad("User Id :".$ans['login_id'],20)."K.Ref.No :".$order_id);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($infos){
									   			
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infos[0]['billno'],6)."Time:".date('H:i'));
									   		$printer->feed(1);
											/*$printer->text("Order From: ".$wera_order_datas['order_from']."");
									   		$printer->feed(1);

									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   		$printer->setTextSize(2, 2);
									   		$printer->text("Order No: ".$wera_order_datas['order_id']."");
									   		$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);*/
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	/*$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
										    $printer->feed(1);*/
									   		$printer->setEmphasis(false);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
											$printer->feed(1);
									   	 	$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infos as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
									    	 	if($modifierdatabill != array()){
												    	foreach($modifierdatabill as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
											    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
														    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}
											    	$total_items_normal ++ ;
										    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

										    }
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											// foreach($testfoods as $tkey => $tvalue){
											// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										 //    	$printer->feed(1);
											// }
											// $printer->text("------------------------------------------");
											// $printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ans['fdiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
												$printer->feed(1);
											} elseif($ans['discount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
												$printer->feed(1);
											}
											$printer->text(str_pad("",25)."SCGST :".$csgst."");
											$printer->feed(1);
											$printer->text(str_pad("",25)."CCGST :".$csgst."");
											$printer->feed(1);
											$printer->text(str_pad("",20)."Packaging :".$wera_order_datas['packaging']."");
											$printer->feed(1);
											$printer->text(str_pad("",20)."Packaging CGST :".$wera_order_datas['packaging_cgst']."");
											$printer->feed(1);
											$printer->text(str_pad("",20)."Packaging SGST :".$wera_order_datas['packaging_sgst']."");
											$printer->feed(1);
											
											$printer->setEmphasis(true);
											$printer->text(str_pad("",25)."Net total :".($ans['grand_total'])."");
											$printer->setEmphasis(false);
										}
									   	
										$printer->feed(1);
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
										if($ans['advance_amount'] != '0.00'){
											$printer->text(str_pad("Advance Amount",38).$ans['advance_amount']."");
											$printer->feed(1);
										}
										$printer->setTextSize(2, 2);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text("GRAND TOTAL : ".round($ans['grand_total']));
										$printer->setTextSize(1, 1);
										$printer->feed(1);
										if($ans['dtotalvalue']!=0){
												$printer->text(str_pad("",20)."Delivery Charge:".$ans['dtotalvalue']);
												$printer->feed(1);
											}
										$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
										if($SETTLEMENT_status == '1'){
											if(isset($this->session->data['credit'])){
												$credit = $this->session->data['credit'];
											} else {
												$credit = '0';
											}
											if(isset($this->session->data['cash'])){
												$cash = $this->session->data['cash'];
											} else {
												$cash = '0';
											}
											if(isset($this->session->data['online'])){
												$online = $this->session->data['online'];
											} else {
												$online ='0';
											}

											if(isset($this->session->data['onac'])){
												$onac = $this->session->data['onac'];
												$onaccontact = $this->session->data['onaccontact'];
												$onacname = $this->session->data['onacname'];

											} else {
												$onac ='0';
											}
										}
										if($SETTLEMENT_status=='1'){
											if($credit!='0' && $credit!=''){
												$printer->text("PAY BY: CARD");
											}
											if($online!='0' && $online!=''){
												$printer->text("PAY BY: ONLINE");
											}
											if($cash!='0' && $cash!=''){
												$printer->text("PAY BY: CASH");
											}
											if($onac!='0' && $onac!=''){
												$printer->text("PAY BY: ON.ACCOUNT");
												$printer->feed(1);
												$printer->text("Name: '".$onacname."'");
												$printer->feed(1);
												$printer->text("Contact: '".$onaccontact."'");
											}
										}
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									   
										
										$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
										$printer->feed(1);
									   /*	$printer->setTextSize(2, 2);

										$printer->text("Order OTP :".($wera_order_datas['order_otp']) );
												$printer->feed(1);*/
									   	$printer->setTextSize(1, 1);

										if($this->model_catalog_order->get_settings('TEXT1') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT1'));
											$printer->feed(1);
										}
										if($this->model_catalog_order->get_settings('TEXT2') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT2'));
											$printer->feed(1);
										}
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										if($this->model_catalog_order->get_settings('TEXT3') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT3'));
										}
										$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	$printer->setTextSize(2, 2);
										$printer->text("Order From: ".$wera_order_datas['order_from']."");
										$printer->feed(1);
								   		$printer->text("Order No: ".$wera_order_datas['order_id']."");
								   		$printer->feed(1);
										$printer->text("Order OTP :".($wera_order_datas['order_otp'] ));
										$printer->feed(2);
										$printer->cut();
									}
									// Close printer //
								    $printer->close();
							    	$this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

								    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
							    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							    $this->session->data['warning'] = $printername." "."Not Working";
							}
					}
					if($ans['bill_status'] == 1 && $duplicate == '1'){
						$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
					} else{
						$json = array();
						$json = array(
							'LOCAL_PRINT' => 0,
							'status' => 1,
						);
					}
					$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
				
			} else {
				$this->session->data['warning'] = 'Bill already printed';
				$this->request->post = array();
				$_POST = array();
				$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
			}
		}

		// $this->request->post = array();
		// $_POST = array();
		// $this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
	}

	public function order_info_datas(){
		$json = array();

		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
			
			$customer_data = $this->db->query("SELECT * FROM `oc_werafood_order_customer` WHERE order_id = '".$order_id."'")->row;
			$json['customer_data'] = array(
				'name' => $customer_data['name'],
				'phone_number' => $customer_data['phone_number'],
				'email' => $customer_data['email'],
				'address' => $customer_data['address'],

			);

			$results = $this->db->query("SELECT * FROM `oc_werafood_order_items` WHERE order_id = '".$order_id."'")->rows;

			foreach ($results as $result) {
				

				$json['item_data'][] = array(
					'order_id' => $result['order_id'],
					'instructions' => $result['instructions'],
					'item_quantity' => $result['item_quantity'],
					'item_unit_price' => $result['item_unit_price'],
					'subtotal' => $result['subtotal'],
					'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		// echo'<pre>';
		// print_r($json);
		// exit;

		$sort_order = array();
		//$json['sucess'] = 1;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
	

}