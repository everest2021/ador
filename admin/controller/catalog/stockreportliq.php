<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogStockReportLiq extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/stockreportliq');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/stockreportliq');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/stockreportliq', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['item_name'])){
			$data['item_name'] = $this->request->post['item_name'];
		}
		else{
			$data['item_name'] = '';
		}

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('Y-m-d');
		}

		if(isset($this->request->post['item_id'])){
			$data['item_id'] = $this->request->post['item_id'];
		}
		else{
			$data['item_id'] = '';
		}

		if(isset($this->request->post['unit_id'])){
			$data['unit_id'] = $this->request->post['unit_id'];
		}
		else{
			$data['unit_id'] = '';
		}

		if(isset($this->request->post['store'])){
			$data['store_id'] = $this->request->post['store'];
			$store_id = $this->request->post['store'];
		}
		else{
			$data['store_id'] = '';
			$store_id = '';
		}

		$this->load->model('catalog/purchaseentry');

		$data['final_data'] = '';
		// echo '<pre>';
		// print_r($data);
		// exit;
		$final_data = array();
		//echo $store_id;
		// echo "<pre>";print_r($this->request->post);exit;
		if(!empty($this->request->post['store'])){
			 $startdate = date('Y-m-d',strtotime($data['startdate']));
			$sql = "SELECT * FROM oc_brand WHERE 1=1";

			if(!empty($this->request->post['item_id']) && !empty($this->request->post['item_name'])){
				$sql .= " AND type_id = '".$this->request->post['item_id']."' AND brand = '".$this->request->post['item_name']."'";
			} 
			// echo"<pre>";print_r($sql);exit;
			$brand_datas = $this->db->query($sql)->rows;

			foreach($brand_datas as $brand_data){
				$sql1 = "SELECT * FROM oc_brand_type WHERE type_id = '".$brand_data['type_id']."' AND loose = 0";

				if(!empty($this->request->post['unit_id'])){
					$sql1 .= " AND id = '".$this->request->post['unit_id']."'";
				}

				$brand_types = $this->db->query($sql1)->rows;

				foreach($brand_types as $brand_type){
					  // echo"<pre>";print_r($brand_data);
					 //  echo"<pre>";print_r($brand_data);exit;
					$avail_quantity = $this->model_catalog_purchaseentry->availableQuantity($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$opening_d = $this->model_catalog_purchaseentry->availableQuantity_report($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$total_purchase = $this->model_catalog_purchaseentry->total_purchase($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'', 0,$startdate);
					$stock_tran_f = $this->model_catalog_purchaseentry->stock_transfer_f($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'', 0, $startdate);
					$stock_tran_t = $this->model_catalog_purchaseentry->stock_transfer_t($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$sale = $this->model_catalog_purchaseentry->stock_sale($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$loose = $this->model_catalog_purchaseentry->loose_stock($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0 ,$startdate);
					$qty = $this->model_catalog_purchaseentry->westage_qty($brand_type['id'], $brand_data['brand_id'],$startdate);
						// echo"<pre>";print_r($avail_quantity);echo"<br>";
						// echo"<pre>";print_r($opening_d);echo"<br>";
						// echo"<pre>";print_r($total_purchase);echo"<br>";
						// echo"<pre>";print_r($stock_tran_f);echo"<br>";
						// echo"<pre>";print_r($stock_tran_t);echo"<br>";
						// echo"<pre>";print_r($sale);echo"<br>";
						// echo"<pre>";print_r($loose);echo"<br>";
						// echo"<pre>";print_r($qty);echo"<br>";
						// echo 'Stop';
					// echo'<pre>';
					// print_r($qty);
					//exit;
					if($avail_quantity != 0 || $opening_d != 0 || $total_purchase != 0 || $stock_tran_f != 0 || $stock_tran_t != 0 || $sale != 0 || $loose != 0 || $qty !=0){
						// echo"innnnn";exit;
						$final_data[] = array(
							'item_name'			=> $brand_data['brand'],
							'unit'				=> $brand_type['size'],
							'avail_quantity' 	=> $avail_quantity,
							'opening_bal' 	=> $opening_d,
							'total_purchase' 	=> $total_purchase,
							's_t_f' 	=> $stock_tran_f,
							's_t_t' 	=> $stock_tran_t,
							'sale' => $sale,
							'loose' => $loose,
							'qty'  => $qty,


						);

					}
				}
					//$avail_quantity = $opening_d + $total_purchase + $stock_tran_t - $stock_tran_f - $sale - $loose - $qty ;
			
			 } //exit;
		}
		$data['final_data'] = $final_data;
		$data['stores'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Liquor'")->rows;
		// echo'<pre>';
		// print_r($final_data);
		// exit;
		$data['action'] = $this->url->link('catalog/stockreportliq', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/stockreportliq', $data));
	}

	public function printer(){
		$this->load->model('catalog/order');
		$this->load->language('catalog/stockreportliq');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/stockreportliq', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->get['item_name'])){
			$data['item_name'] = $this->request->get['item_name'];
		}
		else{
			$data['item_name'] = '';
		}

		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}
		else{
			$data['startdate'] = date('Y-m-d');
		}

		if(isset($this->request->get['item_id'])){
			$data['item_id'] = $this->request->get['item_id'];
		}
		else{
			$data['item_id'] = '';
		}

		if(isset($this->request->get['unit_id'])){
			$data['unit_id'] = $this->request->get['unit_id'];
		}
		else{
			$data['unit_id'] = '';
		}

		if(isset($this->request->get['store_id'])){
			$data['store_id'] = $this->request->get['store_id'];
			$store_id = $this->request->get['store_id'];
		}
		else{
			$data['store_id'] = '';
			$store_id = '';
		}

		// echo"<pre>";print_r($data['item_name'] );exit;
		$this->load->model('catalog/purchaseentry');
		

		$data['final_data'] = '';
		$final_data = array();
		// echo"<pre>";print_r($this->request->get);exit;
		if(!empty($this->request->get['store_id'])){
			$startdate = date('Y-m-d',strtotime($data['startdate']));
			$sql = "SELECT * FROM oc_brand WHERE 1=1";
			if(!empty($this->request->get['item_id']) && !empty($this->request->get['item_name'])){
				$sql .= " AND type_id = '".$this->request->get['item_id']."' AND brand = '".$this->request->get['item_name']."'";
			} 
			$brand_datas = $this->db->query($sql)->rows;


			foreach($brand_datas as $brand_data){
			// echo"<pre>";print_r($brand_data);exit;
				$sql1 = "SELECT * FROM oc_brand_type WHERE type_id = '".$brand_data['type_id']."' AND loose = 0";
				if(!empty($this->request->get['unit_id'])){
					$sql1 .= " AND id = '".$this->request->get['unit_id']."'";
				}

				$brand_types = $this->db->query($sql1)->rows;

				foreach($brand_types as $brand_type){
					// echo "<pre>";print_r('inn'.$brand_type['id']);
					// echo "<pre>";print_r('inn11'.$brand_data['brand_id']);
					// echo "<pre>";print_r('inn22'.$store_id);
					// echo "<pre>";print_r('inn33'.$startdate);
					$avail_quantity = $this->model_catalog_purchaseentry->availableQuantity($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$opening_d = $this->model_catalog_purchaseentry->availableQuantity_report($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$total_purchase = $this->model_catalog_purchaseentry->total_purchase($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'', 0,$startdate);
					$stock_tran_f = $this->model_catalog_purchaseentry->stock_transfer_f($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'', 0, $startdate);
					$stock_tran_t = $this->model_catalog_purchaseentry->stock_transfer_t($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$sale = $this->model_catalog_purchaseentry->stock_sale($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$loose = $this->model_catalog_purchaseentry->loose_stock($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0 ,$startdate);
					$qty = $this->model_catalog_purchaseentry->westage_qty($brand_type['id'], $brand_data['brand_id'],$startdate);
						// echo"<pre>";print_r($avail_quantity);echo"<br>";
						// echo"<pre>";print_r($opening_d);echo"<br>";
						// echo"<pre>";print_r($total_purchase);echo"<br>";
						// echo"<pre>";print_r($stock_tran_f);echo"<br>";
						// echo"<pre>";print_r($stock_tran_t);echo"<br>";
						// echo"<pre>";print_r($sale);echo"<br>";
						// echo"<pre>";print_r($loose);echo"<br>";
						// echo"<pre>";print_r($qty);echo"<br>";
						// echo 'Stop';
					

					
					if($avail_quantity != 0 || $opening_d != 0 || $total_purchase != 0 || $stock_tran_f != 0 || $stock_tran_t != 0 || $sale != 0 || $loose != 0 || $qty !=0){
						//echo"innnnnn";exit;
						$final_data[] = array(
							'item_name'			=> $brand_data['brand'],
							'unit'				=> $brand_type['size'],
							'avail_quantity' 	=> $avail_quantity,
							'opening_bal' 	=> $opening_d,
							'total_purchase' 	=> $total_purchase,
							's_t_f' 	=> $stock_tran_f,
							's_t_t' 	=> $stock_tran_t,
							'sale' => $sale,
							'loose' => $loose,
							'qty'  => $qty,


						);

						$avail_quantity = $opening_d + $total_purchase + $stock_tran_t - $stock_tran_f - $sale - $loose - $qty ;

					}
				}
			
			}   //exit;
		}
		try{
			if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
		 	$printer = new Printer($connector);
		    $printer->selectPrintMode(32);

		    $printer->setEmphasis(true);
		   	$printer->setTextSize(2, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
		    $printer->feed(1);
		    $printer->setTextSize(1, 1);
			$printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
		    $printer->feed(1);
		    $printer->setJustification(Printer::JUSTIFY_LEFT);
			$printer->text("------------------------------------------------");
			$printer->feed(1);
			$printer->setJustification(Printer::JUSTIFY_LEFT);
			$printer->text(str_pad(date('d/m/Y'),30)."".date('h:i:sa'));
			$printer->feed(2);
			$printer->setJustification(Printer::JUSTIFY_CENTER);
			$printer->text("Stock Report");
			$printer->feed(2);
			$printer->text(str_pad("ItemName",15)." ".str_pad("Unit",5)." ".str_pad("OpenBal",4)." ".str_pad("Purchase",4)." ".str_pad("Sale",4)." ".str_pad("Qty",4));
			$printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->setEmphasis(false);
		    $printer->setJustification(Printer::JUSTIFY_LEFT);
		    foreach($final_data as $nkey => $nvalue){
	    	  	$nvalue['item_name'] = utf8_substr(html_entity_decode($nvalue['item_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$nvalue['unit'] = utf8_substr(html_entity_decode($nvalue['unit'], ENT_QUOTES, 'UTF-8'), 0, 6);
				$nvalue['opening_bal'] = utf8_substr(html_entity_decode($nvalue['opening_bal'], ENT_QUOTES, 'UTF-8'), 0, 6);
				$nvalue['total_purchase'] = utf8_substr(html_entity_decode($nvalue['total_purchase'], ENT_QUOTES, 'UTF-8'), 0, 10);
				//$nvalue['s_t_t'] = utf8_substr(html_entity_decode($nvalue['stock_tran_t'], ENT_QUOTES, 'UTF-8'), 0, 10);
				//$nvalue['s_t_f'] = utf8_substr(html_entity_decode($nvalue['stock_tran_f'], ENT_QUOTES, 'UTF-8'), 0, 10);
				//$nvalue['loose'] = utf8_substr(html_entity_decode($nvalue['loose'], ENT_QUOTES, 'UTF-8'), 0, 10);
				$nvalue['sale'] = utf8_substr(html_entity_decode($nvalue['sale'], ENT_QUOTES, 'UTF-8'), 0, 10);
				$nvalue['avail_quantity'] = utf8_substr(html_entity_decode($nvalue['total_purchase'] + $nvalue['opening_bal'] + $nvalue['s_t_t'] - $nvalue['s_t_f'] - $nvalue['loose'] - $nvalue['sale'] , ENT_QUOTES, 'UTF-8'), 0, 10);
				$new = $nvalue['avail_quantity'] - $nvalue['qty'];
				
		    	$printer->setJustification(Printer::JUSTIFY_LEFT);
		    	$printer->text("".str_pad($nvalue['item_name'],16)."".str_pad($nvalue['unit'],8)."".str_pad($nvalue['opening_bal'],7)."".str_pad($nvalue['total_purchase'],8)."".str_pad($nvalue['sale'],5)."".str_pad($new,4));
		    }
		    	$printer->feed(1);
		    	$printer->text("----------------------------------------------");
		   		$printer->feed(1);
				$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("End of Report");
			  	$printer->feed(1);
			  	$printer->cut();
			    // Close printer //
			    $printer->close();
		} catch (Exception $e) {
		    $this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
		}
		$this->getList();
	    
	}

	public function export() {
		$this->load->language('catalog/stockreportliq');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/stockreportliq', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->get['item_name'])){
			$data['item_name'] = $this->request->get['item_name'];
		}
		else{
			$data['item_name'] = '';
		}

		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}
		else{
			$data['startdate'] = date('Y-m-d');
		}

		if(isset($this->request->get['item_id'])){
			$data['item_id'] = $this->request->get['item_id'];
		}
		else{
			$data['item_id'] = '';
		}

		if(isset($this->request->get['unit_id'])){
			$data['unit_id'] = $this->request->get['unit_id'];
		}
		else{
			$data['unit_id'] = '';
		}

		if(isset($this->request->get['store_id'])){
			$data['store_id'] = $this->request->get['store_id'];
			$store_id = $this->request->get['store_id'];
		}
		else{
			$data['store_id'] = '';
			$store_id = '';
		}

		// echo"<pre>";print_r($data['item_name'] );exit;
		$this->load->model('catalog/purchaseentry');
		

		$data['final_data'] = '';
		$final_data = array();
		if(!empty($this->request->get['store_id'])){
			$startdate = date('Y-m-d',strtotime($data['startdate']));
			$sql = "SELECT * FROM oc_brand WHERE 1=1";

			if(!empty($this->request->get['item_id']) && !empty($this->request->get['item_name'])){
				$sql .= " AND type_id = '".$this->request->get['item_id']."' AND brand = '".$this->request->get['item_name']."'";
			} 
			$brand_datas = $this->db->query($sql)->rows;

			$filename = "stockreportliq.csv";
			$fp = fopen('php://output', 'w');
			header('Content-type: application/csv');
			header('Content-Disposition: attachment; filename='.$filename);
			$header = array("Item Name","Unit","Opening Balance","Purchase","Store Trans. From","Store Trans. To","Loose Stock","Wastage & Adjustment","Sales","Closing Balance");
			
			fputcsv($fp, $header);

			foreach($brand_datas as $brand_data){
				$sql1 = "SELECT * FROM oc_brand_type WHERE type_id = '".$brand_data['type_id']."' AND loose = 0";
				if(!empty($this->request->get['unit_id'])){
					$sql1 .= " AND id = '".$this->request->get['unit_id']."'";
				}

				$brand_types = $this->db->query($sql1)->rows;

				foreach($brand_types as $brand_type){
					// echo "<pre>";print_r('inn'.$brand_type['id']);
					// echo "<pre>";print_r('inn11'.$brand_data['brand_id']);
					// echo "<pre>";print_r('inn22'.$store_id);
					// echo "<pre>";print_r('inn33'.$startdate);
					$avail_quantity = $this->model_catalog_purchaseentry->availableQuantity($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$opening_d = $this->model_catalog_purchaseentry->availableQuantity_report($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$total_purchase = $this->model_catalog_purchaseentry->total_purchase($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'', 0,$startdate);
					$stock_tran_f = $this->model_catalog_purchaseentry->stock_transfer_f($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'', 0, $startdate);
					$stock_tran_t = $this->model_catalog_purchaseentry->stock_transfer_t($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$sale = $this->model_catalog_purchaseentry->stock_sale($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$loose = $this->model_catalog_purchaseentry->loose_stock($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0 ,$startdate);
					$qty = $this->model_catalog_purchaseentry->westage_qty($brand_type['id'], $brand_data['brand_id'],$startdate);
						

					
					if($avail_quantity != 0 || $opening_d != 0 || $total_purchase != 0 || $stock_tran_f != 0 || $stock_tran_t != 0 || $sale != 0 || $loose != 0 || $qty !=0){
						//echo"innnnnn";exit;
						$final_data[] = array(
							'item_name'			=> $brand_data['brand'],
							'unit'				=> $brand_type['size'],
							'avail_quantity' 	=> $avail_quantity,
							'opening_bal' 	=> $opening_d,
							'total_purchase' 	=> $total_purchase,
							's_t_f' 	=> $stock_tran_f,
							's_t_t' 	=> $stock_tran_t,
							'sale' => $sale,
							'loose' => $loose,
							'qty'  => $qty,


						);

						$avail_quantity = $total_purchase + $stock_tran_t - $stock_tran_f - $sale - $loose - $qty ;

						$row = array($brand_data['brand'], $brand_type['size'],$opening_d, $total_purchase, $stock_tran_f, $stock_tran_t, $loose, $qty , $sale,$avail_quantity);
						fputcsv($fp, $row);
					}
				}
			
			}   //exit;
		}
		
		$data['stores'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Liquor'")->rows;
		// echo'<pre>';
		// print_r($final_data);
		// exit;
		$data['action'] = $this->url->link('catalog/stockreportliq', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		exit;

		

		
		// echo'<pre>';
		// print_r($final_data);
		// exit;

		$this->response->setOutput($this->load->view('catalog/stockreportliq', $data));
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	 public function autocomplete() {
	 	$json = array();
	 	if(isset($this->request->get['filter_name'])){
	 		$results = $this->db->query("SELECT type_id, brand FROM oc_brand WHERE brand LIKE '%".$this->request->get['filter_name']."%' LIMIT 0,10")->rows;
	 		foreach($results as $key){
	 			$json[] = array(
	 						'id'	=> $key['type_id'],
	 						'name'  => $key['brand']
	 					  );
	 		}
	 	}
	 	$this->response->setOutput(json_encode($json));
	}

	 public function getUnit() {
	 	$json = array();
	 	if(isset($this->request->get['filter_id'])){
			$results = $this->db->query("SELECT * FROM oc_brand_type WHERE type_id = '".$this->request->get['filter_id']."' AND loose = 0")->rows;
			$jsons[] = array(
							'id' => '',
							'name' => 'Please Select'
						);
	 		foreach($results as $key){
	 			$jsons[] = array(
	 						'id'	=> $key['id'],
	 						'name'  => $key['size']
	 					  );
	 		}
	 	}
	 	$json['units'] = $jsons;
	 	$this->response->setOutput(json_encode($json));
	}
}