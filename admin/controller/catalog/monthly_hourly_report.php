<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
date_default_timezone_set('Asia/Kolkata');
class Controllercatalogmonthlyhourlyreport extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('catalog/monthly_hourly_report');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/monthly_hourly_report');
		$this->document->setTitle($this->language->get('heading_title'));

		if(isset($this->request->get['filter_startdate'])){
			$filter_startdate = $this->request->get['filter_startdate'];
		} else {
			$filter_startdate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$filter_enddate = $this->request->get['filter_enddate'];
		} else {
			$filter_enddate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_starttime'])){
			$filter_starttime = $this->request->get['filter_starttime'];
		} else {
			$filter_starttime = date('d-m-Y');
		}

		if(isset($this->request->get['filter_endtime'])){
			$filter_endtime = $this->request->get['filter_endtime'];
		} else {
			$filter_endtime = date('d-m-Y');
		}
		
		$url = '';

		if (isset($this->request->get['filter_enddate'])) {
			$url .= '&filter_enddate=' . $this->request->get['filter_enddate'];
		}

		if (isset($this->request->get['filter_startdate'])) {
			$url .= '&filter_startdate=' . $this->request->get['filter_startdate'];
		}

		if (isset($this->request->get['filter_starttime'])) {
			$url .= '&filter_starttime=' . $this->request->get['filter_starttime'];
		}
		
		if (isset($this->request->get['filter_endtime'])) {
			$url .= '&filter_endtime=' . $this->request->get['filter_endtime'];
		}
		// echo "<pre>";print_r($this->request->get);exit;

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/monthly_hourly_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		$filter_data = array(
			'filter_startdate' => $filter_startdate,
			'filter_starttime' => $filter_starttime,
			'filter_endtime' => $filter_endtime,
		);
		


		$finala_array = '';
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate = strtotime($this->request->get['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

					$sqls = "SELECT `bill_date`,`time`,grand_total, order_id FROM `oc_order_info_report`  WHERE 1=1";

					if (!empty($filter_data['filter_startdate'])) {
						$sqls .= " AND `bill_date` >= '" . $start_date . "'";
					}

					if (!empty($filter_data['filter_startdate'])) {
						$sqls .= " AND `bill_date` <= '" . $end_date . "'";
					}
					
					$sqls .=" AND `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1' ORDER BY bill_date";

					//$sqls .=" GROUP BY `bill_date`";

					$all_caps = $this->db->query($sqls)->rows;

					//echo"<pre>";print_r($all_caps);exit;

					$finala_array = array();
					$t_id = 0;
					$t_id1 = 0;
					$t_id2 = 0;
					$t_id3 = 0;
					$t_id4 = 0;
					$t_id5 = 0;
					$t_id6 = 0;
					$t_id7 = 0;
					$t_id8 = 0;
					$t_id9 = 0;
					$t_id10 = 0;
					$t_id11 = 0;
					$t_id12 = 0;
					$t_id13 = 0;
					$t_id14 = 0;
					$t_id15 = 0;
					$t_id16 = 0;
					$t_id17 = 0;
					$t_id18 = 0;
					$t_id19 = 0;
					$t_id20 = 0;
					$t_id21 = 0;
					$t_id22 = 0;
					$t_id23 = 0;
					foreach ($all_caps as $akey => $avalue) {
							$date = $avalue['bill_date'];
							if(isset($all_caps[$akey+1])){
								$bill_date_future = $all_caps[$akey+1]['bill_date'];
								$in = 0;
								if($bill_date_future == $date ){
									$in = 1;
									$bill_date_future1 = $all_caps[$akey+1]['bill_date'];
								}
							}
							if(isset($bill_date_future1) && $bill_date_future1 == $avalue['bill_date']){
								$t_id = 0;
								$t_id1 = 0;
								$t_id2 = 0;
								$t_id3 = 0;
								$t_id4 = 0;
								$t_id5 = 0;
								$t_id6 = 0;
								$t_id7 = 0;
								$t_id8 = 0;
								$t_id9 = 0;
								$t_id10 = 0;
								$t_id11 = 0;
								$t_id12 = 0;
								$t_id13 = 0;
								$t_id14 = 0;
								$t_id15 = 0;
								$t_id16 = 0;
								$t_id17 = 0;
								$t_id18 = 0;
								$t_id19 = 0;
								$t_id20 = 0;
								$t_id21 = 0;
								$t_id22 = 0;
								$t_id23 = 0;
							}

							
							$t_01 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['01:00:00 to 01:59:59']['sum']) && $avalue['time'] >= '01:00:00' && $avalue['time'] <= '01:59:59'){ 
								$sum1 = $finala_array[$avalue['bill_date']]['01:00:00 to 01:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 // $count_1 =  $finala_array[$avalue['bill_date']]['01:00:00 to 01:59:59']['count'] + 1; 
								 $count_1 =  $finala_array[$avalue['bill_date']]['01:00:00 to 01:59:59']['count'] + 1; 
								 $finala_array[$avalue['bill_date']]['01:00:00 to 01:59:59']['sum']  = $sum1;
								  $finala_array[$avalue['bill_date']]['01:00:00 to 01:59:59']['count']  = $count_1;
								// echo"<pre>";print_r($count_1);
								// echo"<pre>";print_r($sum1);
							} else {
								if($avalue['time'] >= '01:00:00' && $avalue['time'] <= '01:59:59'){ 
									$t_01 += $avalue['grand_total'];
									$t_id++;
									$finala_array[$avalue['bill_date']]['01:00:00 to 01:59:59']['sum'] =$t_01;
									$finala_array[$avalue['bill_date']]['01:00:00 to 01:59:59']['count'] =$t_id;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['01:00:00 to 01:59:59'])){
								$finala_array[$avalue['bill_date']]['01:00:00 to 01:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['01:00:00 to 01:59:59']['count'] =0;
							}

							$t_02 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['02:00:00 to 02:59:59']['sum']) && $avalue['time'] >= '02:00:00' && $avalue['time'] <= '02:59:59'){
								$sum2 = $finala_array[$avalue['bill_date']]['02:00:00 to 02:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_2 =  $finala_array[$avalue['bill_date']]['02:00:00 to 02:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['02:00:00 to 02:59:59']['sum']  = $sum2;
								  $finala_array[$avalue['bill_date']]['02:00:00 to 02:59:59']['count']  = $count_2;
							} else {
								if($avalue['time'] >= '02:00:00' && $avalue['time'] <= '02:59:59'){
									$t_02 += $avalue['grand_total'];
									$t_id1++;
									$finala_array[$avalue['bill_date']]['02:00:00 to 02:59:59']['sum'] =$t_02;
									$finala_array[$avalue['bill_date']]['02:00:00 to 02:59:59']['count'] =$t_id1;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['02:00:00 to 02:59:59'])){
								$finala_array[$avalue['bill_date']]['02:00:00 to 02:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['02:00:00 to 02:59:59']['count'] =0;
							}


							$t_03 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['03:00:00 to 03:59:59']['sum'])  && $avalue['time'] >= '03:00:00' && $avalue['time'] <= '03:59:59'){ 
								$sum3 = $finala_array[$avalue['bill_date']]['03:00:00 to 03:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_3 =  $finala_array[$avalue['bill_date']]['03:00:00 to 03:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['03:00:00 to 03:59:59']['sum']  = $sum3;
								  $finala_array[$avalue['bill_date']]['03:00:00 to 03:59:59']['count']  = $count_3;
							} else {
								if($avalue['time'] >= '03:00:00' && $avalue['time'] <= '03:59:59'){	
									$t_03 += $avalue['grand_total'];
									$t_id2++;
									$finala_array[$avalue['bill_date']]['03:00:00 to 03:59:59']['sum'] =$t_03;
									$finala_array[$avalue['bill_date']]['03:00:00 to 03:59:59']['count'] =$t_id2;
								} 
							}
							if(!isset($finala_array[$avalue['bill_date']]['03:00:00 to 03:59:59'])){
								$finala_array[$avalue['bill_date']]['03:00:00 to 03:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['03:00:00 to 03:59:59']['count'] =0;
							}
							

							$t_04 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['04:00:00 to 04:59:59']['sum']) && $avalue['time'] >= '04:00:00' && $avalue['time'] <= '04:59:59'){
								$sum4 = $finala_array[$avalue['bill_date']]['04:00:00 to 04:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_4 =  $finala_array[$avalue['bill_date']]['04:00:00 to 04:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['04:00:00 to 04:59:59']['sum']  = $sum4;
								  $finala_array[$avalue['bill_date']]['04:00:00 to 04:59:59']['count']  = $count_4;
							} else {
								if($avalue['time'] >= '04:00:00' && $avalue['time'] <= '04:59:59'){
									$t_04 += $avalue['grand_total'];
									$t_id3++;
									$finala_array[$avalue['bill_date']]['04:00:00 to 04:59:59']['sum'] =$t_04;
									$finala_array[$avalue['bill_date']]['04:00:00 to 04:59:59']['count'] =$t_id3;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['04:00:00 to 04:59:59'])){
								$finala_array[$avalue['bill_date']]['04:00:00 to 04:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['04:00:00 to 04:59:59']['count'] =0;
							}


							$t_05 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['05:00:00 to 05:59:59']['sum']) && $avalue['time'] >= '05:00:00' && $avalue['time'] <= '05:59:59'){
								$sum5 = $finala_array[$avalue['bill_date']]['05:00:00 to 05:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_5 =  $finala_array[$avalue['bill_date']]['05:00:00 to 05:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['05:00:00 to 05:59:59']['sum']  = $sum5;
								  $finala_array[$avalue['bill_date']]['05:00:00 to 05:59:59']['count']  = $count_5;
							} else {
								if($avalue['time'] >= '05:00:00' && $avalue['time'] <= '05:59:59'){
									$t_05 += $avalue['grand_total'];
									$t_id4++;
									$finala_array[$avalue['bill_date']]['05:00:00 to 05:59:59']['sum'] =$t_05;
									$finala_array[$avalue['bill_date']]['05:00:00 to 05:59:59']['count'] =$t_id4;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['05:00:00 to 05:59:59'])){
								$finala_array[$avalue['bill_date']]['05:00:00 to 05:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['05:00:00 to 05:59:59']['count'] =0;
							}


							$t_06 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['06:00:00 to 06:59:59']['sum']) && $avalue['time'] >= '06:00:00' && $avalue['time'] <= '06:59:59'){
								$sum6 = $finala_array[$avalue['bill_date']]['06:00:00 to 06:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_6 =  $finala_array[$avalue['bill_date']]['06:00:00 to 06:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['06:00:00 to 06:59:59']['sum']  = $sum6;
								  $finala_array[$avalue['bill_date']]['06:00:00 to 06:59:59']['count']  = $count_6;
							} else {
								if($avalue['time'] >= '06:00:00' && $avalue['time'] <= '06:59:59'){
									$t_06 += $avalue['grand_total'];
									$t_id5++;
									$finala_array[$avalue['bill_date']]['06:00:00 to 06:59:59']['sum'] =$t_06;
									$finala_array[$avalue['bill_date']]['06:00:00 to 06:59:59']['count'] =$t_id5;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['06:00:00 to 06:59:59'])){
								$finala_array[$avalue['bill_date']]['06:00:00 to 06:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['06:00:00 to 06:59:59']['count'] =0;
							}


							$t_07 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['07:00:00 to 07:59:59']['sum']) && $avalue['time'] >= '07:00:00' && $avalue['time'] <= '07:59:59'){ 
								$sum7 = $finala_array[$avalue['bill_date']]['07:00:00 to 07:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_7 =  $finala_array[$avalue['bill_date']]['07:00:00 to 07:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['07:00:00 to 07:59:59']['sum']  = $sum7;
								  $finala_array[$avalue['bill_date']]['07:00:00 to 07:59:59']['count']  = $count_7;
							} else {
								if($avalue['time'] >= '07:00:00' && $avalue['time'] <= '07:59:59'){ 
									$t_07 += $avalue['grand_total'];
									$t_id6++;
									$finala_array[$avalue['bill_date']]['07:00:00 to 07:59:59']['sum'] =$t_07;
									$finala_array[$avalue['bill_date']]['07:00:00 to 07:59:59']['count'] =$t_id6;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['07:00:00 to 07:59:59'])){
								$finala_array[$avalue['bill_date']]['07:00:00 to 07:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['07:00:00 to 07:59:59']['count'] =0;
							}


							$t_08 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['08:00:00 to 08:59:59']['sum']) &&  $avalue['time'] >= '08:00:00' && $avalue['time'] <= '08:59:59'){
								$sum8 = $finala_array[$avalue['bill_date']]['08:00:00 to 08:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_8 =  $finala_array[$avalue['bill_date']]['08:00:00 to 08:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['08:00:00 to 08:59:59']['sum']  = $sum8;
								  $finala_array[$avalue['bill_date']]['08:00:00 to 08:59:59']['count']  = $count_8;
							} else {
								if($avalue['time'] >= '08:00:00' && $avalue['time'] <= '08:59:59'){
									$t_08 += $avalue['grand_total'];
									$t_id7++;
									$finala_array[$avalue['bill_date']]['08:00:00 to 08:59:59']['sum'] =$t_08;
									$finala_array[$avalue['bill_date']]['08:00:00 to 08:59:59']['count'] =$t_id7;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['08:00:00 to 08:59:59'])){
								$finala_array[$avalue['bill_date']]['08:00:00 to 08:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['08:00:00 to 08:59:59']['count'] =0;
							}


							$t_09 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['09:00:00 to 09:59:59']['sum']) && $avalue['time'] >= '09:00:00' && $avalue['time'] <= '09:59:59'){
								$sum9 = $finala_array[$avalue['bill_date']]['09:00:00 to 09:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_9 =  $finala_array[$avalue['bill_date']]['09:00:00 to 09:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['09:00:00 to 09:59:59']['sum']  = $sum9;
								  $finala_array[$avalue['bill_date']]['09:00:00 to 09:59:59']['count']  = $count_9;
							} else {
								if($avalue['time'] >= '09:00:00' && $avalue['time'] <= '09:59:59'){
									$t_09 += $avalue['grand_total'];
									$t_id8++;
									$finala_array[$avalue['bill_date']]['09:00:00 to 09:59:59']['sum'] =$t_09;
									$finala_array[$avalue['bill_date']]['09:00:00 to 09:59:59']['count'] =$t_id8;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['09:00:00 to 09:59:59'])){
								$finala_array[$avalue['bill_date']]['09:00:00 to 09:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['09:00:00 to 09:59:59']['count'] =0;
							}


							$t_10 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['10:00:00 to 10:59:59']['sum']) && $avalue['time'] >= '10:00:00' && $avalue['time'] <= '10:59:59'){
								$sum10 = $finala_array[$avalue['bill_date']]['10:00:00 to 10:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_10 =  $finala_array[$avalue['bill_date']]['10:00:00 to 10:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['10:00:00 to 10:59:59']['sum']  = $sum10;
								  $finala_array[$avalue['bill_date']]['10:00:00 to 10:59:59']['count']  = $count_10;
							} else {
								if($avalue['time'] >= '10:00:00' && $avalue['time'] <= '10:59:59'){
									$t_10 += $avalue['grand_total'];
									$t_id9++;
									$finala_array[$avalue['bill_date']]['10:00:00 to 10:59:59']['sum'] =$t_10;
									$finala_array[$avalue['bill_date']]['10:00:00 to 10:59:59']['count'] =$t_id9;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['10:00:00 to 10:59:59'])){
								$finala_array[$avalue['bill_date']]['10:00:00 to 10:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['10:00:00 to 10:59:59']['count'] =0;
							}


							$t_11 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['11:00:00 to 11:59:59']['sum']) && $avalue['time'] >= '11:00:00' && $avalue['time'] <= '11:59:59'){
								$sum11 = $finala_array[$avalue['bill_date']]['11:00:00 to 11:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_11 =  $finala_array[$avalue['bill_date']]['11:00:00 to 11:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['11:00:00 to 11:59:59']['sum']  = $sum11;
								  $finala_array[$avalue['bill_date']]['11:00:00 to 11:59:59']['count']  = $count_11;
							} else {
								if($avalue['time'] >= '11:00:00' && $avalue['time'] <= '11:59:59'){
									$t_11 += $avalue['grand_total'];
									$t_id10++;
									$finala_array[$avalue['bill_date']]['11:00:00 to 11:59:59']['sum'] =$t_11;
									$finala_array[$avalue['bill_date']]['11:00:00 to 11:59:59']['count'] =$t_id10;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['11:00:00 to 11:59:59'])){
								$finala_array[$avalue['bill_date']]['11:00:00 to 11:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['11:00:00 to 11:59:59']['count'] =0;
							}


							$t_12 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['12:00:00 to 12:59:59']['sum']) && $avalue['time'] >= '12:00:00' && $avalue['time'] <= '12:59:59'){
								$sum12 = $finala_array[$avalue['bill_date']]['12:00:00 to 12:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_12 =  $finala_array[$avalue['bill_date']]['12:00:00 to 12:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['12:00:00 to 12:59:59']['sum']  = $sum12;
								  $finala_array[$avalue['bill_date']]['12:00:00 to 12:59:59']['count']  = $count_12;
							} else {
								if($avalue['time'] >= '12:00:00' && $avalue['time'] <= '12:59:59'){
									$t_12 += $avalue['grand_total'];
									$t_id11++;
									$finala_array[$avalue['bill_date']]['12:00:00 to 12:59:59']['sum'] =$t_12;
									$finala_array[$avalue['bill_date']]['12:00:00 to 12:59:59']['count'] =$t_id11;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['12:00:00 to 12:59:59'])){
								$finala_array[$avalue['bill_date']]['12:00:00 to 12:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['12:00:00 to 12:59:59']['count'] =0;
							}


							$t_13 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['13:00:00 to 13:59:59']['sum']) && $avalue['time'] >= '13:00:00' && $avalue['time'] <= '13:59:59'){
								$sum13 = $finala_array[$avalue['bill_date']]['13:00:00 to 13:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_13 =  $finala_array[$avalue['bill_date']]['13:00:00 to 13:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['13:00:00 to 13:59:59']['sum']  = $sum13;
								  $finala_array[$avalue['bill_date']]['13:00:00 to 13:59:59']['count']  = $count_13;
							} else {
								if($avalue['time'] >= '13:00:00' && $avalue['time'] <= '13:59:59'){
									$t_13 += $avalue['grand_total'];
									$t_id12++;
									$finala_array[$avalue['bill_date']]['13:00:00 to 13:59:59']['sum'] =$t_13;
									$finala_array[$avalue['bill_date']]['13:00:00 to 13:59:59']['count'] =$t_id12;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['13:00:00 to 13:59:59'])){
								$finala_array[$avalue['bill_date']]['13:00:00 to 13:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['13:00:00 to 13:59:59']['count'] =0;
							}

							$t_14 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['14:00:00 to 14:59:59']['sum']) && $avalue['time'] >= '14:00:00' && $avalue['time'] <= '14:59:59'){
								$sum14 = $finala_array[$avalue['bill_date']]['14:00:00 to 14:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_14 =  $finala_array[$avalue['bill_date']]['14:00:00 to 14:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['14:00:00 to 14:59:59']['sum']  = $sum14;
								  $finala_array[$avalue['bill_date']]['14:00:00 to 14:59:59']['count']  = $count_14;
							} else {
								if($avalue['time'] >= '14:00:00' && $avalue['time'] <= '14:59:59'){
									$t_14 += $avalue['grand_total'];
									$t_id13++;
									$finala_array[$avalue['bill_date']]['14:00:00 to 14:59:59']['sum'] =$t_14;
									$finala_array[$avalue['bill_date']]['14:00:00 to 14:59:59']['count'] =$t_id13;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['14:00:00 to 14:59:59'])){
								$finala_array[$avalue['bill_date']]['14:00:00 to 14:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['14:00:00 to 14:59:59']['count'] =0;
							}


							$t_15 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['15:00:00 to 15:59:59']['sum']) && $avalue['time'] >= '15:00:00' && $avalue['time'] <= '15:59:59'){
								$sum15 = $finala_array[$avalue['bill_date']]['15:00:00 to 15:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_15 =  $finala_array[$avalue['bill_date']]['15:00:00 to 15:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['15:00:00 to 15:59:59']['sum']  = $sum15;
								  $finala_array[$avalue['bill_date']]['15:00:00 to 15:59:59']['count']  = $count_15;
							} else {
								if($avalue['time'] >= '15:00:00' && $avalue['time'] <= '15:59:59'){
									$t_15 += $avalue['grand_total'];
									$t_id14++;
									$finala_array[$avalue['bill_date']]['15:00:00 to 15:59:59']['sum'] =$t_15;
									$finala_array[$avalue['bill_date']]['15:00:00 to 15:59:59']['count'] =$t_id14;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['15:00:00 to 15:59:59'])){
								$finala_array[$avalue['bill_date']]['15:00:00 to 15:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['15:00:00 to 15:59:59']['count'] =0;
							}

							$t_16 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['16:00:00 to 16:59:59']['sum']) && $avalue['time'] >= '16:00:00' && $avalue['time'] <= '16:59:59'){
								$sum16 = $finala_array[$avalue['bill_date']]['16:00:00 to 16:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_16 =  $finala_array[$avalue['bill_date']]['16:00:00 to 16:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['16:00:00 to 16:59:59']['sum']  = $sum16;
								  $finala_array[$avalue['bill_date']]['16:00:00 to 16:59:59']['count']  = $count_16;
							} else {
								if($avalue['time'] >= '16:00:00' && $avalue['time'] <= '16:59:59'){
									$t_16 += $avalue['grand_total'];
									$t_id15++;
									$finala_array[$avalue['bill_date']]['16:00:00 to 16:59:59']['sum'] =$t_16;
									$finala_array[$avalue['bill_date']]['16:00:00 to 16:59:59']['count'] =$t_id15;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['16:00:00 to 16:59:59'])){
								$finala_array[$avalue['bill_date']]['16:00:00 to 16:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['16:00:00 to 16:59:59']['count'] =0;
							}

							$t_17 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['17:00:00 to 17:59:59']['sum']) && $avalue['time'] >= '17:00:00' && $avalue['time'] <= '17:59:59'){
								$sum17 = $finala_array[$avalue['bill_date']]['17:00:00 to 17:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_17 =  $finala_array[$avalue['bill_date']]['17:00:00 to 17:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['17:00:00 to 17:59:59']['sum']  = $sum17;
								  $finala_array[$avalue['bill_date']]['17:00:00 to 17:59:59']['count']  = $count_17;
							} else {
								if($avalue['time'] >= '17:00:00' && $avalue['time'] <= '17:59:59'){
									$t_17 += $avalue['grand_total'];
									$t_id16++;
									$finala_array[$avalue['bill_date']]['17:00:00 to 17:59:59']['sum'] =$t_17;
									$finala_array[$avalue['bill_date']]['17:00:00 to 17:59:59']['count'] =$t_id16;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['17:00:00 to 17:59:59'])){
								$finala_array[$avalue['bill_date']]['17:00:00 to 17:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['17:00:00 to 17:59:59']['count'] =0;
							}

							$t_18 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['18:00:00 to 18:59:59']['sum']) && $avalue['time'] >= '18:00:00' && $avalue['time'] <= '18:59:59'){
								$sum18 = $finala_array[$avalue['bill_date']]['18:00:00 to 18:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_18 =  $finala_array[$avalue['bill_date']]['18:00:00 to 18:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['18:00:00 to 18:59:59']['sum']  = $sum18;
								  $finala_array[$avalue['bill_date']]['18:00:00 to 18:59:59']['count']  = $count_18;
							} else {
								if($avalue['time'] >= '18:00:00' && $avalue['time'] <= '18:59:59'){
									$t_18 += $avalue['grand_total'];
									$t_id17++;
									$finala_array[$avalue['bill_date']]['18:00:00 to 18:59:59']['sum'] =$t_18;
									$finala_array[$avalue['bill_date']]['18:00:00 to 18:59:59']['count'] =$t_id17;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['18:00:00 to 18:59:59'])){
								$finala_array[$avalue['bill_date']]['18:00:00 to 18:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['18:00:00 to 18:59:59']['count'] =0;
							}

							$t_19 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['19:00:00 to 19:59:59']['sum']) && $avalue['time'] >= '19:00:00' && $avalue['time'] <= '19:59:59'){
								$sum19 = $finala_array[$avalue['bill_date']]['19:00:00 to 19:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_19 =  $finala_array[$avalue['bill_date']]['19:00:00 to 19:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['19:00:00 to 19:59:59']['sum']  = $sum19;
								  $finala_array[$avalue['bill_date']]['19:00:00 to 19:59:59']['count']  = $count_19;
							} else {
								if($avalue['time'] >= '19:00:00' && $avalue['time'] <= '19:59:59'){
									$t_19 += $avalue['grand_total'];
									$t_id18++;
									$finala_array[$avalue['bill_date']]['19:00:00 to 19:59:59']['sum'] =$t_19;
									$finala_array[$avalue['bill_date']]['19:00:00 to 19:59:59']['count'] =$t_id18;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['19:00:00 to 19:59:59'])){
								$finala_array[$avalue['bill_date']]['19:00:00 to 19:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['19:00:00 to 19:59:59']['count'] =0;
							}

							$t_20 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['20:00:00 to 20:59:59']['sum']) && $avalue['time'] >= '20:00:00' && $avalue['time'] <= '20:59:59'){
								$sum20 = $finala_array[$avalue['bill_date']]['20:00:00 to 20:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_20 =  $finala_array[$avalue['bill_date']]['20:00:00 to 20:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['20:00:00 to 20:59:59']['sum']  = $sum20;
								  $finala_array[$avalue['bill_date']]['20:00:00 to 20:59:59']['count']  = $count_20;
							} else {
								if($avalue['time'] >= '20:00:00' && $avalue['time'] <= '20:59:59'){
									$t_20 += $avalue['grand_total'];
									$t_id19++;
									$finala_array[$avalue['bill_date']]['20:00:00 to 20:59:59']['sum'] =$t_20;
									$finala_array[$avalue['bill_date']]['20:00:00 to 20:59:59']['count'] =$t_id19;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['20:00:00 to 20:59:59'])){
								$finala_array[$avalue['bill_date']]['20:00:00 to 20:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['20:00:00 to 20:59:59']['count'] =0;
							}


							$t_21 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['21:00:00 to 21:59:59']['sum']) && $avalue['time'] >= '21:00:00' && $avalue['time'] <= '21:59:59'){
								$sum21 = $finala_array[$avalue['bill_date']]['21:00:00 to 21:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_21 =  $finala_array[$avalue['bill_date']]['21:00:00 to 21:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['21:00:00 to 21:59:59']['sum']  = $sum21;
								  $finala_array[$avalue['bill_date']]['21:00:00 to 21:59:59']['count']  = $count_21;
							} else {
								if($avalue['time'] >= '21:00:00' && $avalue['time'] <= '21:59:59'){
									$t_21 += $avalue['grand_total'];
									$t_id20++;
									$finala_array[$avalue['bill_date']]['21:00:00 to 21:59:59']['sum'] =$t_21;
									$finala_array[$avalue['bill_date']]['21:00:00 to 21:59:59']['count'] =$t_id20;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['21:00:00 to 21:59:59'])){
								$finala_array[$avalue['bill_date']]['21:00:00 to 21:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['21:00:00 to 21:59:59']['count'] =0;
							}


							$t_22 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['22:00:00 to 22:59:59']['sum']) && $avalue['time'] >= '22:00:00' && $avalue['time'] <= '22:59:59'){
								$sum22 = $finala_array[$avalue['bill_date']]['22:00:00 to 22:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_22 =  $finala_array[$avalue['bill_date']]['22:00:00 to 22:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['22:00:00 to 22:59:59']['sum']  = $sum22;
								  $finala_array[$avalue['bill_date']]['22:00:00 to 22:59:59']['count']  = $count_22;
							} else {
								if($avalue['time'] >= '22:00:00' && $avalue['time'] <= '22:59:59'){
									$t_22 += $avalue['grand_total'];
									$t_id21++;
									$finala_array[$avalue['bill_date']]['22:00:00 to 22:59:59']['sum'] =$t_22;
									$finala_array[$avalue['bill_date']]['22:00:00 to 22:59:59']['count'] =$t_id21;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['22:00:00 to 22:59:59'])){
								$finala_array[$avalue['bill_date']]['22:00:00 to 22:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['22:00:00 to 22:59:59']['count'] =0;
							}


							$t_23 = 0;
							
							if(isset($finala_array[$avalue['bill_date']]['23:00:00 to 23:59:59']['sum']) && $avalue['time'] >= '23:00:00' && $avalue['time'] <= '23:59:59'){
								$sum23 = $finala_array[$avalue['bill_date']]['23:00:00 to 23:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_23 =  $finala_array[$avalue['bill_date']]['23:00:00 to 23:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['23:00:00 to 23:59:59']['sum']  = $sum23;
								  $finala_array[$avalue['bill_date']]['23:00:00 to 23:59:59']['count']  = $count_23;
							} else {
								if($avalue['time'] >= '23:00:00' && $avalue['time'] <= '23:59:59'){
									$t_23 += $avalue['grand_total'];
									$t_id22++;
									$finala_array[$avalue['bill_date']]['23:00:00 to 23:59:59']['sum'] =$t_23;
									$finala_array[$avalue['bill_date']]['23:00:00 to 23:59:59']['count'] =$t_id22;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['23:00:00 to 23:59:59'])){
								$finala_array[$avalue['bill_date']]['23:00:00 to 23:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['23:00:00 to 23:59:59']['count'] =0;
							}

							$t_24 = 0;

							if(isset($finala_array[$avalue['bill_date']]['00:00:00 to 00:59:59']['sum']) && $avalue['time'] >= '00:00:00' && $avalue['time'] <= '00:59:59'  ){
								$sum24 = $finala_array[$avalue['bill_date']]['00:00:00 to 00:59:59']['sum'] 
								 +  $avalue['grand_total'];
								 $count_24 =  $finala_array[$avalue['bill_date']]['00:00:00 to 00:59:59']['count'] + 1;
								 $finala_array[$avalue['bill_date']]['00:00:00 to 00:59:59']['sum']  = $sum24;
								  $finala_array[$avalue['bill_date']]['00:00:00 to 00:59:59']['count']  = $count_24;
								
							} else {
								if($avalue['time'] >= '00:00:00' && $avalue['time'] <= '00:59:59'){
									$t_24 += $avalue['grand_total'];
									$t_id23++;
									$finala_array[$avalue['bill_date']]['00:00:00 to 00:59:59']['sum'] =$t_24;
									$finala_array[$avalue['bill_date']]['00:00:00 to 00:59:59']['count'] =$t_id23;
								}
							}
							if(!isset($finala_array[$avalue['bill_date']]['00:00:00 to 00:59:59'])){
								$finala_array[$avalue['bill_date']]['00:00:00 to 00:59:59']['sum'] =0;
								$finala_array[$avalue['bill_date']]['00:00:00 to 00:59:59']['count'] =0;
							}
					}
			// echo"<pre>";print_r($finala_array);
			// 		exit;
		}

		$data['finala_array'] = $finala_array;
		$data['sql24'] = '';
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate'])){
			// echo "innnn";exit;

			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate = strtotime($this->request->get['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
					
					$sql24 = $this->db->query("SELECT SUM(`grand_total`) as total FROM `oc_order_info_report`  WHERE `bill_date` >= '" . $start_date . "'AND `bill_date` <= '" . $end_date . "' AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'")->row;

					$new = $sql24['total'];
					$data['total'] = $new ;
					
					$sql25 = $this->db->query("SELECT COUNT(`order_id`) as billno FROM `oc_order_info_report`  WHERE `bill_date` >= '" . $start_date . "' AND `bill_date` <= '" . $end_date . "' AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'")->row;
					// echo"<pre>";print_r("SELECT COUNT(`order_id`) as billno FROM `oc_order_info_report`  WHERE `bill_date` >= '" . $start_date . "' AND `bill_date` <= '" . $end_date . "' AND  `cancel_status` = '0' AND `food_cancel` = '0' AND `liq_cancel` = '0' AND `pay_method` = 1 AND `bill_status` = '1'");exit;

					$new1 = $sql25['billno'];
					$data['billno'] = $new1 ;

		}

		$data['filter_startdate'] = $filter_startdate;
		$data['filter_enddate'] = $filter_enddate;
		
		
		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/monthly_hourly_report', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/monthly_hourly_report', $data));
	}
	public function name(){
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$sql = "SELECT * FROM `oc_waiter` WHERE 1=1 ";
			if(!empty($this->request->get['filter_name'])){
				$sql .= " AND `name` LIKE '%".$this->request->get['filter_name']."%'";
			}
			if(!empty($this->request->get['filter_type'])){
				if($this->request->get['filter_type'] == 1){
					$sql .= " AND `roll` = 'Captain' ";
				} elseif($this->request->get['filter_type'] == 2) {
					$sql .= " AND `roll` = 'Waiter' ";
				}
			}
			//echo $sql;exit;
			$results = $this->db->query($sql)->rows;
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['waiter_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	

}