<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
date_default_timezone_set('Asia/Kolkata');
class ControllerCatalogNcKotreport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/nckotreport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/nckotreport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/nckotreport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['nckotdata'] = array();
		$nckotdata = array();

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);
			
			$nckotdata = $this->db->query("SELECT oi.`order_no`,oi.`table_id`,oi.`bill_date`,oit.`name`,oit.`qty`,oit.`rate`,oit.`nc_kot_reason` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oit.`nc_kot_status`= 1 AND oit.`qty` > '0' ")->rows;
		}
		// echo "<pre>";
		// print_r($nckotdata);
		// exit();
		$data['nckotdata'] = $nckotdata;

		$data['action'] = $this->url->link('catalog/nckotreport', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/nckotreport', $data));
	}

	public function prints(){

		if(isset($this->request->get['filter_category'])){
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = '99';
		}
	

		$data['nckotdata'] = array();
		$nckotdata = array();
		//echo"<pre>";print_r($this->request->get);exit;

		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);
			
			$nckotdata = $this->db->query("SELECT oi.`order_no`,oi.`table_id`,oi.`bill_date`,oit.`name`,oit.`qty`,oit.`rate`,oit.`nc_kot_reason` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oit.`nc_kot_status`= 1")->rows;

			$total = $this->db->query("SELECT  SUM(oit. qty) FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oit.`nc_kot_status`= 1")->rows;

			$totalamt = $this->db->query("SELECT  SUM(oit. rate) FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oit.`nc_kot_status`= 1")->rows;
		}
		$data['nckotdata'] = $nckotdata;
		$data['total'] = $total;
		$data['totalamt'] = $totalamt;
		// echo"<pre>";print_r($totalamt);exit;

		$this->load->model('catalog/order');

		try {
		    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

		   	$printer->setEmphasis(true);
		   	$printer->setTextSize(2, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
		    $printer->feed(1);
		    $printer->setTextSize(1, 1);
		    $printer->text("Recipe");
		    $printer->feed(1);
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		   	$printer->setJustification(Printer::JUSTIFY_LEFT);
		   	$printer->text(str_pad("Date :".date('Y-m-d'),30)."Time :".date('H:i:s'));
		   	$printer->feed(1);
		   	$printer->text("----------------------------------------------");
		   	$printer->feed(1);
		   	$printer->text(str_pad("RefNo",7)."".str_pad("TableNo.",7)." ".str_pad("Name",15)."".str_pad("Qty",6)."".str_pad("Amount",7));
		   	$printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->setEmphasis(false);
		    $total_items_normal = 0;
			$total_quantity_normal = 0;
		    foreach($nckotdata as $nkey => $nvalue){ 
	    	 $printer->text(str_pad($nvalue['order_no'],8)."".str_pad($nvalue['table_id'],6)."".substr($nvalue['name'],0,15)." ".str_pad($nvalue['qty'],6)."".str_pad($nvalue['rate'],7));
				$printer->feed(1);
	    	}  //echo"<pre>";print_r($totalamt);exit;
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->text(str_pad("Total Qty",30)."".$total[0]['SUM(oit. qty)']);
			$printer->feed(1);
			$printer->text(str_pad("Total",30)."".$totalamt[0]['SUM(oit. rate)']);
			$printer->feed(1);
			$printer->text("------------------- END REPORT -------------------");
		    $printer->feed(1);
		    $printer->cut();
			$printer->feed(2);
		    // Close printer //
		    $printer->close();
		} catch (Exception $e) {
		   echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
		}
		$this->getList();
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}


	public function export() {
		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		//echo "<pre>";print_r($this->request->get);exit;
		//echo "in";exit;
		$this->load->language('catalog/nckotreport');
		$this->document->setTitle($this->language->get('heading_title'));

		if(isset($this->request->get['filter_startdate'])){
			$filter_startdate = $this->request->get['filter_startdate'];
		} else {
			$filter_startdate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$filter_enddate = $this->request->get['filter_enddate'];
		} else {
			$filter_enddate = date('d-m-Y');
		}
		
		$url = '';

		if (isset($this->request->get['filter_startdate'])) {
			$url .= '&filter_startdate=' . $this->request->get['filter_startdate'];
		}

		if (isset($this->request->get['filter_enddate'])) {
			$url .= '&filter_enddate=' . $this->request->get['filter_enddate'];
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/captain_and_waiter', 'token=' . $this->session->data['token'] . $url, true)
		);

		$filter_data = array(
			'filter_startdate' 	=> $filter_startdate,
			'filter_enddate' 	=> $filter_enddate,
		);

		
		$data['nckotdata'] = array();
		$nckotdata = array();
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$nckotdata = $this->db->query("SELECT oi.`order_no`,oi.`table_id`,oi.`bill_date`,oit.`name`,oit.`qty`,oit.`rate`,oit.`nc_kot_reason` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oit.`nc_kot_status`= 1 AND oit.`qty` > '0' ")->rows;
		}
		$data['nckotdata'] = $nckotdata;
		$data['filter_startdate'] = $filter_startdate;
		$data['filter_enddate'] = $filter_enddate;
		
		$html = $this->load->view('catalog/nckotreport_html.tpl', $data); // exit;
	 	$filename = "NC_KOT_REPORT.html";
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;

		// header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		// header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
		// header("Expires: 0");
		// header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		// header("Cache-Control: private",false);
		// echo $html;
		// exit;		
		$this->response->setOutput($this->load->view('catalog/nckotreport', $data));
	}
}