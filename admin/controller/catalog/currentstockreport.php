<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
class ControllerCatalogcurrentstockreport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/currentstockreport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/currentstockreport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/currentstockreport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_storename'])){
			$data['storename'] = $this->request->post['filter_storename'];
		}
		else{
			$data['storename'] = '';
		}

		$data['storenames'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'food'")->rows;
		$data['token'] = $this->session->data['token'];

		$data['action'] = $this->url->link('catalog/currentstockreport', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/currentstockreport', $data));
	}


	// public function prints(){
	// 	date_default_timezone_set("Asia/Kolkata");
	// 	$finaldatas = array();

	// 	if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
	// 		$startdate = strtotime($this->request->get['filter_startdate']);
	// 		$enddate =  strtotime($this->request->get['filter_enddate']);

	// 		$start_date = date('Y-m-d', $startdate);
	// 		$end_date = date('Y-m-d', $enddate);

	// 		$startdate1 = date('d-m-Y',strtotime($start_date));
	// 		$enddate1 = date('d-m-Y',strtotime($end_date));

	// 		$dates = $this->GetDays($start_date,$end_date);

	// 			$date_array = array();
	// 			$datacash = array();
	// 			$datacredit = array();
	// 			foreach($dates as $date){
	// 				$datacash = $this->db->query("SELECT count(*) as cashcount , SUM(vat) as cashvat, SUM(gst) as cashgst, SUM(grand_total) as grandtotalcash FROM `oc_order_info` WHERE pay_cash <> '0' AND pay_method = '1' AND `bill_date` = '".$date."'")->rows;

	// 				$datacredit = $this->db->query("SELECT count(*) as creditcount , SUM(vat) as creditvat, SUM(gst) as creditgst, SUM(grand_total) as grandtotalcredit FROM `oc_order_info` WHERE pay_card <> '0' AND pay_method = '1' AND `bill_date` = '".$date."'")->rows;

	// 				$totalcount = $datacash[0]['cashcount'] + $datacredit[0]['creditcount'];

	// 				$cashvat =  $datacash[0]['cashvat'];
	// 				$cashgst =  $datacash[0]['cashgst'];
	// 				$totalcash = ($datacash[0]['grandtotalcash']) - ($cashvat + $cashgst) ;

	// 				$creditvat =  $datacredit[0]['creditvat'];
	// 				$creditgst =  $datacredit[0]['creditgst'];
	// 				$totalcredit = ($datacredit[0]['grandtotalcredit']) - ($creditvat + $creditgst);

	// 				$totalvat = $cashvat + $creditvat ; 
	// 				$totalgst = $cashgst + $creditgst;
	// 				$totalnetsale = $totalcash + $totalcredit;

	// 				$date_array[] = array(
	// 					'totalcount' => $totalcount,
	// 					'totalvat' => $totalvat,
	// 					'totalgst' => $totalgst,
	// 					'grandtotalcash' => $totalcash,
	// 					'grandtotalcredit' => $totalcredit,
	// 					'total' => $totalnetsale,
	// 					'date' => date('d-m-Y', strtotime($date)),
	// 				);
	
	// 			}
	// 			$finaldatas[] = array(
	// 				'date_array' => $date_array,
	// 			);

	// 		$total = 0;

	// 		try {
	// 	    // Enter the share name for your USB printer here
	// 	    $connector = new WindowsPrintConnector("XP-58C");
	// 	    // Print a "Hello world" receipt" //
	// 	    $printer = new Printer($connector);
	// 	    $printer->selectPrintMode(32);

	// 		   	$printer->setEmphasis(true);
	// 		   	$printer->setTextSize(2, 1);
	// 		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 		    $printer->feed(1);
	// 		   	//$printer->setFont(Printer::FONT_B);
	// 		    $printer->text("YAARI\n");
	// 		    $printer->setTextSize(1, 1);
	// 		    $printer->text("Shop No 5,Mansorovar\nOpp. Narayan E-School\nSwami Satyanand Marg\nBhayandar-W\n");
	// 		    $printer->feed(1);
	// 		    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 		  	$printer->text("------------------------------------------------");
	// 		  	$printer->feed(1);
	// 		  	$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 		  	$printer->text(str_pad(date('d/m/Y'),35)."".date('h:i:sa'));
	// 		  	$printer->feed(2);
	// 		  	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 		  	$printer->text("Daily Sales Summary Report");
	// 		  	$printer->feed(2);
	// 		  	$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 		  	$printer->text(str_pad("From :".$startdate1,30)."To :".$enddate1);
	// 		  	$printer->feed(2);
	// 		    foreach($finaldatas as $finaldata) {
	// 			  	$printer->text(str_pad("DATE",15)."".str_pad("CASH",5)."".str_pad("CARD",5)."".str_pad("N.SALE",7).""
	// 			  		.str_pad("VAT",5)."".str_pad("GST",5)."T.BILL");
	// 			  	$printer->feed(1);
	// 			  	$printer->text("------------------------------------------------");
	// 			  	$printer->feed(1);
	// 			  		foreach($finaldata['date_array'] as $data) {
	// 			  			$printer->text(str_pad($data['date'],15)."".str_pad($data['grandtotalcash'],5)."".str_pad($data['grandtotalcredit'],5)."".str_pad($data['total'] ,7)."".str_pad($data['totalvat'],5)."".str_pad($data['totalgst'],5).$data['totalcount']);
	// 			  			$printer->feed(1);
	// 			  			$printer->text("------------------------------------------------");
	// 			  			$printer->feed(1);
	// 			  		}
	// 			 	$printer->feed(1);	
	// 			}
	// 		  	$printer->feed(1);
	// 			$printer->cut();
	// 		    // Close printer //
	// 		    $printer->close();
	// 		} catch (Exception $e) {
	// 		    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
	// 		}
	// 		$this->getList();
	// 	}		
	// }
}