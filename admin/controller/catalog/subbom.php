<?php
class ControllerCatalogsubbom extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/subbom');
		$this->document->setTitle("Sub BOM");
		$this->load->model('catalog/subbom');
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/subbom');
		$this->document->setTitle("Sub BOM");
		$this->load->model('catalog/subbom');

		$url = '';

		$category = '';
		$data['category'] = '';
		$bomtype = '';
		$catname = '';
		$itemname = '';
		$unitname = '';
		$storename = '';
		$data['subbomcategory'] = array();
		$subbomitemdata = array();
		$data['subbomitemdata'] = array();
		$lists = array();
		$data['lists'] = array();

		if(isset($this->request->get['filter_bomtype']) || isset($this->request->get['filter_bomcategory'])){

			$bomtype = $this->request->get['filter_bomtype'];
			$bomcategory = $this->request->get['filter_bomcategory'];

			if($bomtype == 'Menu Item'){
				$category = $this->db->query("SELECT category FROM oc_category WHERE category_id = '".$bomcategory."'")->row['category'];
				$itemdata = $this->db->query("SELECT item_code FROM oc_item WHERE item_code = '".$this->request->get['code']."'")->row['item_code'];
			} else if($bomtype == 'Finish / Semi Finish'){
				$category = $this->db->query("SELECT name FROM oc_stockcategory WHERE id = '".$bomcategory."'")->row['name'];
				$itemdata = $this->db->query("SELECT item_code FROM oc_stock_item WHERE item_code = '".$this->request->get['code']."'")->row['item_code'];
			} else if($bomtype == 'Modifier'){
				$category = array();
			}

			$mainlist = $this->db->query("SELECT * FROM oc_bom WHERE stock_item_code = '".$itemdata."'")->row;
			if($mainlist != array()){
				$lists = $this->db->query("SELECT * FROM oc_bom_items WHERE parent_item_code = '".$mainlist['stock_item_code']."'")->rows;
			}
		}


		if (isset($this->request->get['code'])) {
			$url .= '&code=' . $this->request->get['code'];
		}

		if (isset($this->request->get['name'])) {
			$url .= '&name=' . $this->request->get['name'];
		}

		if (isset($this->request->get['filter_bomtype'])) {
			$url .= '&filter_bomtype=' . $this->request->get['filter_bomtype'];
		}

		if (isset($this->request->get['filter_bomcategory'])) {
			$url .= '&filter_bomcategory=' . $this->request->get['filter_bomcategory'];
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		if(!empty($this->request->post['filter_subbomcategory'])){
			$data['subbomcategory'] = $this->request->post['filter_subbomcategory'];
			$subbomcategory = $this->request->post['filter_subbomcategory'];
			$subbomitemdata = $this->db->query("SELECT * FROM oc_stock_item WHERE category_id = '".$subbomcategory."'")->rows;
			$catname = $this->db->query("SELECT name FROM oc_stockcategory WHERE id = '".$this->request->post['filter_subbomcategory']."'")->row['name'];
		} else{
			$data['subbomcategory'] = '';
			$catname = '';
		}

		if(!empty($this->request->post['filter_subbomitem'])){
			$data['subbomitem'] = $this->request->post['filter_subbomitem'];
			$itemname = $this->db->query("SELECT item_name FROM oc_stock_item WHERE item_code = '".$this->request->post['filter_subbomitem']."'")->row['item_name'];
		} else{
			$data['subbomitem'] = '';
			$itemname = '';
		}

		if(!empty($this->request->post['filter_unit'])){
			$data['unit'] = $this->request->post['filter_unit'];
			$unitname = $this->db->query("SELECT unit FROM oc_unit WHERE unit_id = '".$this->request->post['filter_unit']."'")->row['unit'];
		} else{
			$data['unit'] = '';
			$unitname = '';
		}

		if(!empty($this->request->post['qty'])){
			$data['qty'] = $this->request->post['qty'];
		} else{
			$data['qty'] = '';
		}

		if(!empty($this->request->post['filter_store'])){
			$data['store'] = $this->request->post['filter_store'];
			$storename = $this->db->query("SELECT store_name FROM oc_store_name WHERE id = '".$this->request->post['filter_store']."'")->row['store_name'];
		} else{
			$data['store'] = '';
		}
		// echo'<pre>';
		// print_r($this->request->post);
		// exit;

		if(!empty($this->request->post['filter_store'])){
			$subdata = array(
						'item_name' => $this->request->get['name'],
						'item_code' => $itemdata,
						'catname' => $catname,
						'itemname' => $itemname,
						'unitname' => $unitname,
						'storename' => $storename
						);
			$this->model_catalog_subbom->addItem($this->request->post,$subdata);
			$this->response->redirect($this->url->link('catalog/subbom', 'token=' . $this->session->data['token'] . $url, true));;
		}

		$data['subbomcategorys'] = $this->db->query("SELECT * FROM oc_stockcategory")->rows;

		$data['category'] = $category;
		$data['bomtype'] = $bomtype;
		$data['code'] = $this->request->get['code'];
		$data['name'] = $this->request->get['name'];

		$data['subbomitemdata'] = $subbomitemdata;
		$data['units'] = $this->db->query("SELECT * FROM oc_unit")->rows;
		$data['stores'] = $this->db->query("SELECT * FROM oc_store_name")->rows;
		$data['lists'] = $lists;
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['token'] = $this->session->data['token'];

		$data['action'] = $this->url->link('catalog/subbom', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/subbom/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['cancel'] = $this->url->link('catalog/bom', 'token=' . $this->session->data['token'] . $url, true);
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/subbom', $data));
	}

	public function delete(){

		$this->load->model('catalog/subbom');

		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $item_id) {
				$this->model_catalog_subbom->deleteItem($item_id);
			}
		}
		$this->getList();
	}

	public function getprice(){
		$json = array();
		$code = $this->request->get['filter_id'];
		$purchaseprice = $this->db->query("SELECT purchase_rate, unit_name, uom FROM oc_stock_item WHERE item_code = '".$code."'")->row;
		$json[] = array(
					'purchase_price' => $purchaseprice['purchase_rate'],
					'unit_name' => $purchaseprice['unit_name'],
					'unit_id' => $purchaseprice['uom'],
				);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
}