<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class Controllercatalogbalpoint extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/report');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/bal_point');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/bal_point', 'token=' . $this->session->data['token'] . $url, true)
		);


		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';

		$data['nc_kot_amt'] = 0;

		
			$new_c_id = '';
			$new_c_point = '';
			$new_c_points = '';
			$new_u_point = '';
			$new_u_points = '';
			$new_rc_points = '';
			
			
				$cust_details = $this->db->query("SELECT * FROM oc_customerinfo")->rows;

				$custdetails = $this->db->query("SELECT * FROM oc_customerinfo");

				if($custdetails->num_rows > 0 ) {
					foreach ($custdetails->rows as $key => $value) {
						$custpoints = $this->db->query("SELECT sum(cust_point) as cpoint FROM oc_order_info WHERE `cust_id` = '".$value['c_id']."' ")->row;

						$custpoint = $this->db->query("SELECT sum(cust_point) as cpoint FROM oc_order_info_report WHERE `cust_id` = '".$value['c_id']."' ")->row;

						$refcustpoint = $this->db->query("SELECT sum(ref_cust_point) as rcpoint FROM oc_ref_cust WHERE `ref_cust_id` = '".$value['c_id']."' ")->row;

						$usepoints = $this->db->query("SELECT sum(cust_red_point) as upoint FROM oc_order_info WHERE `cust_id` = '".$value['c_id']."' ")->row;

						$usepoint = $this->db->query("SELECT sum(cust_red_point) as upoint FROM oc_order_info_report WHERE `cust_id` = '".$value['c_id']."' ")->row;
						
						$new_c_points .=$custpoint['cpoint'].',';
						$new_c_point .=$custpoints['cpoint'].',';
						$new_u_point .=$usepoints['upoint'].',';
						$new_u_points .=$usepoint['upoint'].',';
						$new_rc_points .=$refcustpoint['rcpoint'].',';
					}

					$spoint = explode(',',$new_c_points);
					$spoints = explode(',',$new_c_point);
					$upoints = explode(',',$new_u_point);
					$upoint = explode(',',$new_u_points);
					$rcpoint = explode(',',$new_rc_points);
					$new1 = $spoint;
					$new2 = $spoints;
					$new3 = $upoints;
					$new4 = $upoint;
					$new5 = $rcpoint;
					$data['new1'] = $new1 ;
					$data['new2'] = $new2 ;
					$data['new3'] = $new3 ;
					$data['new4'] = $new4 ;
					$data['new5'] = $new5 ;
					// echo"<pre>";print_r($new3);exit;
				}


		$data['billdatas'] = $billdata;
		$data['cust_details'] = $cust_details;
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['edit_bill'] = $this->user->getEditBill();

		$data['action'] = $this->url->link('catalog/bal_point', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/bal_point', $data));
	}

	public function prints(){
		// echo "innnnnnnnnnnnnnnnnnnnn";exit;

		$this->load->model('catalog/order');
		$this->load->language('catalog/bal_point');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/bal_point', 'token=' . $this->session->data['token'] . $url, true)
		);


		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';

		$data['nc_kot_amt'] = 0;

		
			$new_c_id = '';
			$new_c_point = '';
			$new_c_points = '';
			$new_u_point = '';
			$new_u_points = '';
			$new_rc_points = '';
			
			
				$cust_details = $this->db->query("SELECT * FROM oc_customerinfo")->rows;

				$custdetails = $this->db->query("SELECT * FROM oc_customerinfo");

				if($custdetails->num_rows > 0 ) {
					foreach ($custdetails->rows as $key => $value) {
						$custpoints = $this->db->query("SELECT sum(cust_point) as cpoint FROM oc_order_info WHERE `cust_id` = '".$value['c_id']."' ")->row;

						$custpoint = $this->db->query("SELECT sum(cust_point) as cpoint FROM oc_order_info_report WHERE `cust_id` = '".$value['c_id']."' ")->row;

						$refcustpoint = $this->db->query("SELECT sum(ref_cust_point) as rcpoint FROM oc_ref_cust WHERE `ref_cust_id` = '".$value['c_id']."' ")->row;

						$usepoints = $this->db->query("SELECT sum(cust_red_point) as upoint FROM oc_order_info WHERE `cust_id` = '".$value['c_id']."' ")->row;

						$usepoint = $this->db->query("SELECT sum(cust_red_point) as upoint FROM oc_order_info_report WHERE `cust_id` = '".$value['c_id']."' ")->row;
						
						$new_c_points .=$custpoint['cpoint'].',';
						$new_c_point .=$custpoints['cpoint'].',';
						$new_u_point .=$usepoints['upoint'].',';
						$new_u_points .=$usepoint['upoint'].',';
						$new_rc_points .=$refcustpoint['rcpoint'].',';
					}

					$spoint = explode(',',$new_c_points);
					$spoints = explode(',',$new_c_point);
					$upoints = explode(',',$new_u_point);
					$upoint = explode(',',$new_u_points);
					$rcpoint = explode(',',$new_rc_points);
					$new1 = $spoint;
					$new2 = $spoints;
					$new3 = $upoints;
					$new4 = $upoint;
					$new5 = $rcpoint;
					$data['new1'] = $new1 ;
					$data['new2'] = $new2 ;
					$data['new3'] = $new3 ;
					$data['new4'] = $new4 ;
					$data['new5'] = $new5 ;
					// echo"<pre>";print_r($new3);exit;
				}


		$data['billdatas'] = $billdata;
		$data['cust_details'] = $cust_details;

		// echo"<pre>";print_r($cust_details);exit;	
		$data['billdatas'] = $billdata;

		$this->load->model('catalog/order');

		try {
		    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

		   	$printer->setEmphasis(true);
		   	$printer->setTextSize(2, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
		    $printer->feed(1);
		    $printer->setTextSize(1, 1);
		    $printer->text("Recipe");
		    $printer->feed(1);
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		   	$printer->setJustification(Printer::JUSTIFY_LEFT);
		   	$printer->text(str_pad("Date :".date('Y-m-d'),30)."Time :".date('H:i:s'));
		   	$printer->feed(1);
		   	$printer->text("----------------------------------------------");
		   	$printer->feed(1);
		   	$printer->text(str_pad("ID",7)."".str_pad("Name.",7)." ".str_pad("Totalpoint",15)."".str_pad("Usepoint",10)."".str_pad("bal.poi",7));
		   	$printer->feed(1);
		    $printer->setEmphasis(false);
		    foreach($cust_details as $nkey => $nvalue){ 
		    	$printer->text(str_pad($nvalue['c_id'],4)."".substr($nvalue['name'],0,11)."  ".str_pad($new1[$nkey]+$new2[$nkey]+$new5[$nkey],13)." ".str_pad($new3[$nkey]+$new4[$nkey],7)."  ".str_pad(($new1[$nkey]+$new2[$nkey]+$new5[$nkey])-($new3[$nkey]+$new4[$nkey]),7));
					$printer->feed(1);
		    }
	    	$printer->text("----------------------------------------------");
		    $printer->feed(1); 
			
			$printer->text("------------------- END REPORT -----------------");
		    $printer->feed(1);
		    $printer->cut();
			$printer->feed(2);
		    // Close printer //
		    $printer->close();
		} catch (Exception $e) {
		   echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
		}
		$this->getList();
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	
}