<?php
class ControllerCatalogNckot extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/order');
		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/order');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$orderid = $this->request->post['orderid'];
			$po_datas = $this->request->post['po_datas'];
			$ischecked = 0;
			foreach ($po_datas as $po_data) {
				if(isset($po_data['checkedtotransfer'])){
					$ischecked = 1;
					$this->db->query("UPDATE `oc_order_items` SET `amt` = '0', `tax1_value` = '0', `nc_kot_status` = '1', `nc_kot_reason` = '".$po_data['nc_kot_reason']."' WHERE `id`='".$po_data['checkedtotransfer']."' ");
					$this->log->write("UPDATE `oc_order_items` SET `amt` = '0', `tax1_value` = '0', `nc_kot_status` = '1', `nc_kot_reason` = '".$po_data['nc_kot_reason']."' WHERE `id`='".$po_data['checkedtotransfer']."' ");
				}
			}
			if(isset($orderid)){
				$this->updateorderinfo($orderid);
			}
			if($ischecked == 1){
				//$json['done'] = '<img src="'.HTTP_CATALOG.'system/storage/download/done.gif" style="width:100%" >';
				$json['done'] = '<script>parent.closeIFrame();</script>';
				$json['info'] = 1;
				$this->response->setOutput(json_encode($json));
			} else {
				$json['info'] = 0;
				$this->response->setOutput(json_encode($json));
			}
		}
	}

	public function updateorderinfo($orderid){
		$this->load->model('catalog/order');
		$ftotal = 0;
		$ltotal = 0;
		$gst = 0;
		$vat = 0;
		$grand_total = 0;
		$ftotalvalue = 0;
		$ftotal_discount = 0;
		$ltotalvalue = 0;
		$ltotal_discount = 0;
		$gsttaxamt = 0;
		$staxfood = 0;
		$staxliq = 0;
		$vattaxamt = 0;
		$totalamtfood = 0;
		$totalamtliq = 0;
		
		$fromtotable = $this->db->query("SELECT * FROM `oc_order_items` WHERE `order_id` = '".$orderid."' AND `is_liq` = '0' AND ismodifier = '1' AND cancelstatus = '0'")->rows;
		$fromtotableliquir = $this->db->query("SELECT * FROM `oc_order_items` WHERE `order_id` = '".$orderid."' AND `is_liq` = '1' AND ismodifier = '1' AND cancelstatus = '0'")->rows;
		$order_fromtotable = $this->db->query("SELECT * FROM `oc_order_info` WHERE `order_id` = '".$orderid."'")->row;
		$tax_food1 = 0;
		foreach ($fromtotable as $key => $value) {
			if($key == 0){
				$taxes = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$value['code']."' ");
				if($taxes->num_rows > 0){
					$vattax = $taxes->row['vat'];
					$valtax2 = $taxes->row['tax2'];
				} else{
					$vattax = '0';
					$valtax2 = '0';
				}
				$taxfood1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$vattax."'");
				if($taxfood1->num_rows > 0){
					$tax_food1 = $taxfood1->row['tax_value'];
				} else{
					$tax_food1 = '0';
				}
				$taxfood2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$valtax2."'");
				if($taxfood2->num_rows > 0){
					$tax_food2 = $taxfood2->row['tax_value'];
				} else{
					$tax_food2 = '0';
				}
			}
			$ftotal = $ftotal + $value['amt'];
		}
		$servicechargefood = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$service_charge = $this->db->query("SELECT `service_charge` FROM oc_location WHERE `location_id` = '".$order_fromtotable['location_id']."'")->row['service_charge'];

		$staxfood = 0;
		foreach ($fromtotable as $key => $value) {
			if($order_fromtotable['fdiscountper'] > 0){
				$discount_value = $value['amt']*($order_fromtotable['fdiscountper']/100);
				$afterdiscountamt = $value['amt'] - $discount_value;
				$staxfood_1 = 0;
				if($servicechargefood > 0){
					if ($service_charge == 1) {
						$staxfood_1 = $afterdiscountamt*($servicechargefood/100);		
					} else {
						$staxfood_1 = 0;
					}
				}
				$staxfood = $staxfood + $staxfood_1;
				$tax1_value = ($afterdiscountamt + $staxfood_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($afterdiscountamt + $staxfood_1) * ($value['tax2'] / 100);
		  		$gsttaxamt = $gsttaxamt + $tax1_value + $tax2_value;
		  		$ftotalvalue = $ftotalvalue + $discount_value;
		  		$ftotal_discount = $ftotal_discount + $afterdiscountamt; 
				$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '".$order_fromtotable['fdiscountper']."',
				 							discount_value = '".$discount_value."',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxfood_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			}elseif($order_fromtotable['discount'] > 0){
				$discount_per = (($order_fromtotable['discount'])/$ftotal)*100;
				$discount_value = $value['amt']*($discount_per/100);
				$afterdiscountamt = $value['amt'] - $discount_value;
				$staxfood_1 = 0;
				if($servicechargefood > 0){
					if($service_charge == 1){
						$staxfood_1 = $afterdiscountamt*($servicechargefood/100);
					}else{
						$staxfood_1 = 0;
					}
				}
				$staxfood = $staxfood + $staxfood_1;
				$tax1_value = ($afterdiscountamt + $staxfood_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($afterdiscountamt + $staxfood_1) * ($value['tax2'] / 100);
				$gsttaxamt = $gsttaxamt + $tax1_value + $tax2_value;
		  		$ftotalvalue = $ftotalvalue + $discount_value;
				$ftotal_discount = $ftotal_discount + $afterdiscountamt; 
				$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '".$discount_per."',
				 							discount_value = '".$discount_value."',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxfood_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			}else{
				$staxfood_1 = 0;
				if($servicechargefood > 0){
					if ($service_charge == 1) {
						$staxfood_1 = $value['amt'] * ($servicechargefood/100);		
					}else{
						$staxfood_1 = 0;
					}
				}
				$staxfood = $staxfood + $staxfood_1;
				$tax1_value = ($value['amt'] + $staxfood_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($value['amt'] + $staxfood_1) * ($value['tax2'] / 100);
				$gsttaxamt = $gsttaxamt + $tax1_value + $tax2_value;
				$ftotal_discount = $ftotal_discount + $value['amt']; 
		  		$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '0.00',
				 							discount_value = '0.00',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxfood_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			}
		}
		$stax_f = $staxfood;
		$gst = $gsttaxamt;
		
		$tax_liq1 = 0;
		foreach ($fromtotableliquir as $key => $value) {
			if($key == 0){
				$taxes = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$value['code']."' ");
				if($taxes->num_rows > 0){
					$vattax = $taxes->row['vat'];
				} else{
					$vattax = '0';
				}
				$taxliq1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$vattax."'");
				if($taxliq1->num_rows > 0){
					$tax_liq1 = $taxliq1->row['tax_value'];
				} else{
					$tax_liq1 = '0';
				}
			}
			$ltotal = $ltotal + $value['amt'];
		}

		$servicechargeliq = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');
		$staxliq = 0;
		foreach ($fromtotableliquir as $key => $value) {
			if($order_fromtotable['ldiscountper'] > 0){
				$discount_value = $value['amt']*($order_fromtotable['ldiscountper']/100);
				$afterdiscountamt = $value['amt'] - $discount_value;
				$staxliq_1 = 0;
				if($servicechargeliq > 0){
					if ($service_charge == 1) {
						$staxliq_1 = $afterdiscountamt * ($servicechargeliq/100);		
					}else{
						$staxliq_1 = 0;	
					}
				}
				$staxliq = $staxliq + $staxliq_1;
				$tax1_value = ($afterdiscountamt + $staxliq_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($afterdiscountamt + $staxliq_1) * ($value['tax2'] / 100);
				$vattaxamt = $vattaxamt + $tax1_value + $tax2_value;
		  		$ltotalvalue = $ltotalvalue + $discount_value;
		  		$ltotal_discount = $ltotal_discount + $afterdiscountamt; 
				$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '".$order_fromtotable['ldiscountper']."',
				 							discount_value = '".$discount_value."',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxliq_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			} elseif($order_fromtotable['ldiscount'] > 0){
				$discount_per = ($order_fromtotable['ldiscount']/$ltotal)*100;
				$discount_value = $value['amt']*($discount_per/100);
				$afterdiscountamt = $value['amt'] - $discount_value;
				$staxliq_1 = 0;
				if($servicechargeliq > 0){
					if ($service_charge == 1) {
						$staxliq_1 = $afterdiscountamt * ($servicechargeliq/100);		
					}else{
						$staxliq_1 = 0;	
					}
				}
				$staxliq = $staxliq + $staxliq_1;
				$tax1_value = ($afterdiscountamt + $staxliq_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($afterdiscountamt + $staxliq_1) * ($value['tax2'] / 100);
				$vattaxamt = $vattaxamt + $tax1_value + $tax2_value;
		  		$ltotalvalue = $ltotalvalue + $discount_value;
		  		$ltotal_discount = $ltotal_discount + $afterdiscountamt; 
				$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '".$discount_per."',
				 							discount_value = '".$discount_value."',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxliq_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			} else{
				$staxliq_1 = 0;
				if($servicechargeliq > 0){
					if ($service_charge == 1) {
						$staxliq_1 = $value['amt'] * ($servicechargeliq/100);		
					} else {
						$staxliq_1 = 0;	
					}
				}
				$staxliq = $staxliq + $staxliq_1;
				$tax1_value = ($value['amt'] + $staxliq_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($value['amt'] + $staxliq_1) * ($value['tax2'] / 100);
				$vattaxamt = $vattaxamt + $tax1_value + $tax2_value;
		  		$ltotal_discount = $ltotal_discount + $value['amt']; 
		  		$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '0.00',
				 							discount_value = '0.00',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxliq_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			}
		}
		$stax_l = $staxliq;
		$vat = $vattaxamt;
		$stax = $stax_l + $stax_f;
		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
			$grand_total = ($ftotal + $ltotal + $stax_f + $stax_l) - ($ftotalvalue + $ltotalvalue);
		} else{
			$grand_total = ($ftotal + $ltotal + $gst + $vat + $stax_f + $stax_l) - ($ftotalvalue + $ltotalvalue);
		}
		$grand_totals = number_format($grand_total,2,'.','');
		$grand_total_explode = explode('.', $grand_total);
		$roundtotal = 0;
		if(isset($grand_total_explode[1])){
			if($grand_total_explode[1] >= 50){
				$roundtotals = 100 - $grand_total_explode[1];
				$roundtotal = ($roundtotals / 100); 
			} else {
				$roundtotal = -($grand_total_explode[1] / 100);
			}
		}
		$dtotalvalue = 0;
		if($order_fromtotable['dchargeper'] > 0){
			$dtotalvalue = $grand_total * ($order_fromtotable['dchargeper'] / 100);
		} elseif($order_fromtotable['dcharge'] > 0){
			$dtotalvalue = $order_fromtotable['dcharge'];
		}
		$totalitemandqty = $this->db->query("SELECT SUM(qty) as totalqty, COUNT(*) as totalitem FROM oc_order_items WHERE order_id = '".$orderid."' AND cancelstatus = '0' AND ismodifier = '1'")->row;
		//$totalvalue = ($order_fromtotable['ltotalvalue'] + $order_fromtotable['ftotalvalue'] + $order_fromtotable['discount'] + $order_fromtotable['ldiscount']);
		//$grand_total = $grand_total - $totalvalue;
		$this->db->query("UPDATE `oc_order_info` 
									SET `ftotal` = '".$ftotal."', 
									`ltotal` = '".$ltotal."', 
									`gst` = '".$gst."', 
									`vat` = '".$vat."', 
									`grand_total` = '".$grand_total."', 
									`dtotalvalue` = '".$dtotalvalue."', 
									`ftotalvalue` = '".$ftotalvalue."', 
									`ftotal_discount` = '".$ftotal_discount."', 
									`ltotalvalue` = '".$ltotalvalue."', 
									`ltotal_discount` = '".$ltotal_discount."', 
									`staxfood` = '".$stax_f."', 
									`staxliq` = '".$stax_l."', 
									`stax` = '".$stax."',
									`roundtotal` = '".$roundtotal."',
									`total_items` = '".$totalitemandqty['totalitem']."',
									`item_quantity` = '".$totalitemandqty['totalqty']."',
									`login_id` = '".$this->user->getId()."',
									`login_name` = '".$this->user->getUserName()."',
									`cancel_status` = '0'
									WHERE `order_id` = '".$orderid."'");

	   	$test = $this->db->query("SELECT * FROM oc_order_items WHERE ismodifier = '0'")->rows;
		$testmain = $this->db->query("SELECT * FROM oc_order_items WHERE ismodifier = '1'")->rows;
		foreach($testmain as $testmaindata){
			foreach($test as $testdata){
				if($testdata['parent_id'] == $testmaindata['id']){
					$testdata['qty'] = $testmaindata['qty'];
					$testdata['amt'] = $testdata['qty'] * $testdata['rate'];
					$this->db->query("UPDATE oc_order_items SET qty = '".$testdata['qty']."', amt = '".$testdata['amt']."' WHERE parent_id ='".$testmaindata['id']."' AND id='".$testdata['id']."'");
				}
			}
		}		
	}

	public function getList() {
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/order');
		
		if(isset($this->request->post['table'])){
			$data['table'] = $this->request->post['table'];
		} elseif (isset($this->request->get['table'])) {
			$data['table'] = $this->request->get['table'];
		} else {
			$data['table'] = '';
		}

		$data['orders'] = array();

		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$table_froms = $this->db->query("SELECT * FROM ".DB_PREFIX."order_info WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' ORDER BY `order_id` ")->rows;
		
		$data['tables_from'] = array();
		foreach ($table_froms as $dvalue) {
			$data['tables_from'][$dvalue['table_id']]= $dvalue['t_name'];
		}
		
		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$data['action'] = $this->url->link('catalog/nckot/add', 'token=' . $this->session->data['token'], true);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/nckot_form', $data));
	}

	public function tableinfo() {
		$tid = $this->request->get['tid'];
		$lid = $this->request->get['lid'];
		$tab_index = $this->request->get['tab_index'];

		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$tableinfo =  $this->db->query("SELECT  * FROM oc_order_info WHERE `location_id` ='".$lid."' AND `table_id` = '".$tid."' AND `bill_date` = '" . $this->db->escape($last_open_date) . "' AND bill_status = '0' ORDER BY order_id DESC LIMIT 1 ");
		$data['html'] = '';
		if($tableinfo->num_rows > 0) {
			$order =	$tableinfo->row['order_id'];
			$order_no =	$tableinfo->row['order_no'];
			$data['order'] = $order;
			$tableitems = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$tableinfo->row['order_id']."' AND `cancelstatus` = '0' ")->rows;
			$data['i']=1;
			foreach ($tableinfo->row as $tkey => $tinfo) {
				$data[$tkey] = $tinfo;
			}
			$html = '<tr>';
				$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">No.</td>';
				$html .= "<td style='width:0%;padding-top: 2px;padding-bottom: 2px;'></td>";
				$html .= '<td style="width: 10%;padding-top: 2px;padding-bottom: 2px;">Code</td>';
				$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">Name</td>';
				$html .= '<td style="width: 10%;padding-top: 2px;padding-bottom: 2px;">Qty</td>';
				$html .= '<td style="width: 10%;padding-top: 2px;padding-bottom: 2px;">Rate</td>';
				$html .= '<td style="width: 10%;padding-top: 2px;padding-bottom: 2px;">Amt</td>';
				$html .= '<td style="width: 20%;padding-top: 2px;padding-bottom: 2px;">KOT</td>';
				$html .= '<td style="width: 20%;padding-top: 2px;padding-bottom: 2px;">Reason</td>';
			$html .= '</tr>';
			foreach ($tableitems as $lkey => $items) {
				$html .= '<tr>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
						$html .= ''.$data['i'].'<input type="hidden" name="po_datas['.$data['i'].'][kot_status]" value="'.$items['kot_status'].'" id="kot_status'.$data['i'].'" /><input type="hidden" name="po_datas['.$data['i'].'][pre_qty]" value="'.$items['qty'].'" id="pre_qty'.$data['i'].'" />';
					$html .= '</td>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">'; 
						$html .= '<input style="padding: 0px 1px;" type="checkbox" name="po_datas['.$data['i'].'][checkedtotransfer]" value="'.$items['id'].'"/>';
						$tab_index ++;
					$html .= '</td>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
						$html .= '<input style="padding: 0px 1px;" type="text" class="inputs code form-control readonly" name="po_datas['.$data['i'].'][code]" value="'.$items['code'].'" id="code_'.$data['i'].'" />';
						$tab_index ++;
					$html .= '</td>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
						$html .= '<input style="padding: 0px 1px;" type="text" class="inputs names form-control readonly" name="po_datas['.$data['i'].'][name]" value="'.$items['name'].'" id="name_'.$data['i'].'" />';
						$tab_index ++;
					$html .= '</td>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
						$html .= '<input style="padding: 0px 1px;" type="number" name="po_datas['.$data['i'].'][qty]" value="'.$items['qty'].'" class="inputs qty form-control readonly" id="qty_'.$data['i'].'" />';
						$tab_index ++;
					$html .= '</td>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
						$html .= '<input style="padding: 0px 1px;" type="number" name="po_datas['.$data['i'].'][rate]" value="'.$items['rate'].'" class="inputs rate form-control readonly" id="rate_'.$data['i'].'" />';
						$tab_index ++;
					$html .= '</td>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
						$html .= '<input style="padding: 0px 1px;background-color: #f2f2f2;" type="text" readonly="readonly" name="po_datas['.$data['i'].'][amt]" value="'.$items['amt'].'" class="inputs form-control" id="amt_'.$data['i'].'" />';
					$html .= '</td>';		
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
						$html .= '<input style="padding: 0px 1px;" type="text" name="po_datas['.$data['i'].'][message]" value="'.$items['message'].'" class="inputs lst form-control readonly" id="message_'.$data['i'].'" /><input type="hidden" name="po_datas['.$data['i'].'][is_liq]" value="'.$items['is_liq'].'" class="" id="is_liq_'.$data['i'].'" />';
						$tab_index ++;
					$html .= '</td>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
						$html .= '<input style="padding: 0px 1px;" type="text" name="po_datas['.$data['i'].'][nc_kot_reason]" value="'.$items['nc_kot_reason'].'" class="inputs lst form-control" id="nc_kot_reason_'.$data['i'].'" />';
						$tab_index ++;
					$html .= '</td>';
				$html .= '</tr>';
				$data['i'] ++;
			}
			$html .= '<input type="hidden" name="orderid" value="'.$order.'"  id="orderid" />';
			$html .= '<input type="hidden" name="order_no" value="'.$order_no.'"  id="orderno" />';
			$data['html'] = $html;
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function findtable() {
		$json = array();
		if (isset($this->request->get['filter_tname'])) {
			$this->load->model('catalog/order');
			$filter_data = array(
				'filter_name' => $this->request->get['filter_tname'],
			);
			$results = $this->model_catalog_order->gettables_data($filter_data);
			if(isset($results['location_id'])){
				$json['table_id'] = $filter_data['filter_name'];//$results['table_id'];
				$json['loc_id'] = $results['location_id'];
				$json['rate_id'] = $results['rate_id'];
				$json['loc_name'] = strip_tags(html_entity_decode($results['location'], ENT_QUOTES, 'UTF-8'));
				$json['name'] = strip_tags(html_entity_decode($filter_data['filter_name'], ENT_QUOTES, 'UTF-8'));
				$json['status'] = 1;
			} else {
				$json['status'] = 0;
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}