<?php
class ControllerCatalogAccountExpense extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/accountexpense');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/accountexpense');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/accountexpense');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/accountexpense');
		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		print_r($this->request->post);
		exit;*/

			$this->model_catalog_accountexpense->addAccexpense($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_acc_exp_name'])) {
				$url .= '&filter_acc_exp_name=' . $this->request->get['filter_acc_exp_name'];
			}

			if (isset($this->request->get['parent_id'])) {
				$url .= '&parent_id=' . $this->request->get['parent_id'];
			}

			if (isset($this->request->get['filter_acc_exp_type'])) {
				$url .= '&filter_acc_exp_type=' . $this->request->get['filter_acc_exp_type'];
			}


			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/accountexpense', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/accountexpense');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/accountexpense');
		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_accountexpense->editAccexpense($this->request->get['expense_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_acc_exp_name'])) {
				$url .= '&filter_acc_exp_name=' . $this->request->get['filter_acc_exp_name'];
			}

			if (isset($this->request->get['filter_parent'])) {
				$url .= '&filter_parent=' . $this->request->get['filter_parent'];
			}

			if (isset($this->request->get['filter_acc_exp_type'])) {
				$url .= '&filter_acc_exp_type=' . $this->request->get['filter_acc_exp_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/accountexpense', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/accountexpense');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/accountexpense');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $expense_id) {
				$this->model_catalog_accountexpense->deleteAccexpense($expense_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_acc_exp_name'])) {
				$url .= '&filter_acc_exp_name=' . $this->request->get['filter_acc_exp_name'];
			}

			if (isset($this->request->get['filter_acc_exp_type'])) {
				$url .= '&filter_acc_exp_type=' . $this->request->get['filter_acc_exp_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/accountexpense', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_parent'])) {
			$filter_parent = $this->request->get['filter_parent'];
		} else {
			$filter_parent = null;
		}

		if (isset($this->request->get['filter_acc_exp_type'])) {
			$filter_acc_exp_type = $this->request->get['filter_acc_exp_type'];
		} else {
			$filter_acc_exp_type = '';
		}

		if (isset($this->request->get['filter_acc_exp_name'])) {
			$filter_acc_exp_name = $this->request->get['filter_acc_exp_name'];
		} else {
			$filter_acc_exp_name = '';
		}

		
		if (isset($this->request->get['filter_expense_id'])) {
			$filter_expense_id = $this->request->get['filter_expense_id'];
		} else {
			$filter_expense_id = null;
		}		

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'expense_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_acc_exp_name'])) {
			$url .= '&filter_acc_exp_name=' . $this->request->get['filter_acc_exp_name'];
		}
		if (isset($this->request->get['filter_acc_exp_type'])) {
			$url .= '&filter_acc_exp_type=' . $this->request->get['filter_acc_exp_type'];
		}
		
		if (isset($this->request->get['filter_expense_id'])) {
			$url .= '&filter_expense_id=' . $this->request->get['filter_expense_id'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/accountexpense', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/accountexpense/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/accountexpense/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			'filter_acc_exp_name' => $filter_acc_exp_name,
			'filter_acc_exp_type' => $filter_acc_exp_type,
			'filter_expense_id' => $filter_expense_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$table_total = $this->model_catalog_accountexpense->getTotalAccexpense($filter_data);

		$results = $this->model_catalog_accountexpense->getAccexpenses($filter_data);

		$data['tables'] = array();
		foreach ($results as $result) {
			$data['tables'][] = array(
				'expense_id' => $result['expense_id'],
				'exp_acc_name'   => $result['exp_acc_name'],
				'exp_acc_type'        => $result['exp_acc_type'],
				'contact_no'        => $result['contact_no'],
				'edit'        => $this->url->link('catalog/accountexpense/edit', 'token=' . $this->session->data['token'] . '&expense_id=' . $result['expense_id'] . $url, true)
			);
		 
		}

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';
		if (isset($this->request->get['filter_acc_exp_name'])) {
			$url .= '&filter_acc_exp_name=' . $this->request->get['filter_acc_exp_name'];
		}
		if (isset($this->request->get['filter_acc_exp_type'])) {
			$url .= '&filter_acc_exp_type=' . $this->request->get['filter_acc_exp_type'];
		}
		
		if (isset($this->request->get['filter_expense_id'])) {
			$url .= '&filter_expense_id=' . $this->request->get['filter_expense_id'];
		}
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['name'] = $this->url->link('catalog/accountexpense', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['expense_id'] = $this->url->link('catalog/accountexpense', 'token=' . $this->session->data['token'] . '&sort=expense_id' . $url, true);
		$data['sort_parent_category'] = $this->url->link('catalog/accountexpense', 'token=' . $this->session->data['token'] . '&sort = parent_category' . $url, true);
		$url = '';

		if (isset($this->request->get['filter_acc_exp_name'])) {
			$url .= '&filter_acc_exp_name=' . $this->request->get['filter_acc_exp_name'];
		}
		if (isset($this->request->get['filter_acc_exp_type'])) {
			$url .= '&filter_acc_exp_type=' . $this->request->get['filter_acc_exp_type'];
		}
		
		if (isset($this->request->get['filter_expense_id'])) {
			$url .= '&filter_expense_id=' . $this->request->get['filter_expense_id'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $table_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/accountexpense', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($table_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($table_total - $this->config->get('config_limit_admin'))) ? $table_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $table_total, ceil($table_total / $this->config->get('config_limit_admin')));

		$data['filter_expense_id'] = $filter_expense_id;
		$data['filter_acc_exp_name'] = $filter_acc_exp_name;
		$data['filter_acc_exp_type'] = $filter_acc_exp_type;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/accountexpense_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['expense_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		

		$url = '';

		if (isset($this->request->get['filter_acc_exp_name'])) {
			$url .= '&filter_acc_exp_name=' . $this->request->get['filter_acc_exp_name'];
		}

		if (isset($this->request->get['filter_acc_exp_type'])) {
			$url .= '&filter_acc_exp_type=' . $this->request->get['filter_acc_exp_type'];
		}

		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$category_array = $this->model_catalog_accountexpense->getCategory();
		
		

		$data['account_types']  = array('Supplier' =>'Supplier',
		'Employee' =>'Employee',
		'Other' =>'Other');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/accountexpense', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['expense_id'])) {
			$data['action'] = $this->url->link('catalog/accountexpense/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/accountexpense/edit', 'token=' . $this->session->data['token'] . '&expense_id=' . $this->request->get['expense_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/accountexpense', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['expense_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_accountexpense->getAccexpense($this->request->get['expense_id']);
		}

		$data['token'] = $this->session->data['token'];

		

		if (isset($this->request->post['exp_acc_name'])) {
			$data['exp_acc_name'] = $this->request->post['exp_acc_name'];
		} elseif (!empty($category_info)) {
			$data['exp_acc_name'] = $category_info['exp_acc_name'];
		} else {
			$data['exp_acc_name'] = '';
		}

		if (isset($this->request->post['exp_acc_type'])) {
			$data['exp_acc_type'] = $this->request->post['exp_acc_type'];
		} elseif (!empty($category_info)) {
			$data['exp_acc_type'] = $category_info['exp_acc_type'];
		} else {
			$data['exp_acc_type'] = '';
		}

		if (isset($this->request->post['contact_no'])) {
			$data['contact_no'] = $this->request->post['contact_no'];
		} elseif (!empty($category_info)) {
			$data['contact_no'] = $category_info['contact_no'];
		} else {
			$data['contact_no'] = '';
		}

		if (isset($this->request->post['address'])) {
			$data['address'] = $this->request->post['address'];
		} elseif (!empty($category_info)) {
			$data['address'] = $category_info['address'];
		} else {
			$data['address'] = '';
		}

		if (isset($this->request->post['gst_no'])) {
			$data['gst_no'] = $this->request->post['gst_no'];
		} elseif (!empty($category_info)) {
			$data['gst_no'] = $category_info['gst_no'];
		} else {
			$data['gst_no'] = '';
		}
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/accountexpense_from', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/accountexpense')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// $datas = $this->request->post;
		// if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 255)) {
		// 	$this->error['name'] = 'Please Enter Name';
		// } else {
		// 	if(isset($this->request->get['expense_id'])){
		// 		$is_exist = $this->db->query("SELECT `expense_id` FROM `oc_subcategory` WHERE `name` = '".$datas['name']."' AND `expense_id` <> '".$this->request->get['expense_id']."' ");
		// 		if($is_exist->num_rows > 0){
		// 			$this->error['name'] = 'Category Name Already Exists';
		// 		}
		// 	} else {
		// 		$is_exist = $this->db->query("SELECT `expense_id` FROM `oc_subcategory` WHERE `name` = '".$datas['name']."' ");
		// 		if($is_exist->num_rows > 0){
		// 			$this->error['name'] = 'Category Name Already Exists';
		// 		}
		// 	}
		// }

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/subcategory')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/subcategory');

		return !$this->error;
	}

		

	public function autocompletecname() {
		$json = array();

		if (isset($this->request->get['filter_acc_exp_name'])) {

			$this->load->model('catalog/accountexpense');

			$filter_data = array(
				'filter_acc_exp_name' => $this->request->get['filter_acc_exp_name'],
				'start'       => 0,
				'limit'       => 20
			);

			//echo $this->request->get['filter_acc_exp_name'];exit;

			$results = $this->model_catalog_accountexpense->getAccexpenses($filter_data);
			//echo "<pre>";print_r($results);exit;
			foreach ($results as $result) {
				$json[] = array(
					'expense_id' => $result['expense_id'],
					'name'        => strip_tags(html_entity_decode($result['exp_acc_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	
}