<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogBarcode extends Controller {

	private $error = array();



	public function index() {
		$this->load->language('catalog/barcode');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/barcode');
		$this->load->model('catalog/order');
		$this->getform();

	}

	protected function getForm() {

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_rate'] = $this->language->get('entry_rate');
		// $data['entry_capacity'] = $this->language->get('entry_capacity');

		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['start'])) {
			$data['error_start'] = $this->error['name'];
		} else {
			$data['error_start'] = array();
		}

		if (isset($this->error['end'])) {
			$data['error_end'] = $this->error['end'];
		} else {
			$data['error_end'] = array();
		}


		$url = '';



		if (isset($this->request->get['filter_name'])) {

			$url .= '&filter_name=' . $this->request->get['filter_name'];

		}


		if (isset($this->request->get['sort'])) {

			$url .= '&sort=' . $this->request->get['sort'];

		}



		if (isset($this->request->get['order'])) {

			$url .= '&order=' . $this->request->get['order'];

		}



		if (isset($this->request->get['page'])) {

			$url .= '&page=' . $this->request->get['page'];

		}



		$data['breadcrumbs'] = array();



		$data['breadcrumbs'][] = array(

			'text' => $this->language->get('text_home'),

			'href' => $this->url->link('common/barcode', 'token=' . $this->session->data['token'], true)

		);



		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/barcode', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/barcode', 'token=' . $this->session->data['token'] . $url, true);
		}


		$data['cancel'] = $this->url->link('catalog/barcode', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['start'])) {
			$data['start'] = $this->request->post['start'];
		} else {
			$data['start'] = '';
		}



		if (isset($this->request->post['end'])) {
			$data['end'] = $this->request->post['end'];
		} else {
			$data['end'] = '';
		}
		//echo "<pre>";print_r($this->request->POST);exit;

		
		// if ($this->request->server['REQUEST_METHOD'] == 'POST')	{
		// //echo "in";exit;
		// 	$printername = $this->model_catalog_order->get_settings('BARCODE_PRINTER');
			 
		// 	//10 - Barcode Number
		// 	//99 - Item Code
		// 	//250 - MRP
		// 	//$str = "1";
			
		// 	//echo $a;
		// 	//exit;
		// 	$start = $data['start'];
		// 	$end =$data['end'];
		// 	//echo $start;
		// 	//exit;
		// 	//$a = "{A"."$a";
		// 	try {
		//  		$connector = new WindowsPrintConnector($printername);
		// 	    $printer = new Printer($connector);
		// 	    $printer->selectPrintMode(32);
		// 	   	$printer->setEmphasis(true);
		// 	   	$printer->setTextSize(2, 1);
		// 	   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    
		// 		//$printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_BELOW);
		// 	    //foreach(array($a, $b, $c) as $item)  {
		// 		for ($i=$start; $i <= $end; $i++) {
		// 			$printer->text("FUNSHALLA");
		// 	    	$printer->feed(1);
		// 	    	$printer->feed(1);
		// 	    	$printer->text("PASSPORT NO");
		// 	    	$printer->feed(1);
		// 	    	$printer->setJustification(Printer::JUSTIFY_CENTER);
		// 			$printer->setBarcodeHeight(80);
		// 			$aa = str_pad($i,7,"0",STR_PAD_LEFT);
		// 			$a = "FUN$aa" ;
		// 			$a = "{A"."$a";
		// 		    $printer->barcode($a, Printer::BARCODE_CODE128);
		// 		    $printer->feed(1);
		// 		    $sub = substr($a, 2);
		// 		    $printer->text($sub);
		// 	    	$printer->feed(1);
			    
		// 		//}
		// 	    $printer->setJustification(Printer::JUSTIFY_CENTER);
		// 	    //$printer->setTextSize(1, 1);
		// 	    //$printer->text("Barcode Number : 10");
		// 	    //$printer->feed(1);
		// 	    //$printer->text("Item Name : Soup");
		// 	    //$printer->feed(1);
		// 	    //$printer->text("MRP : 100");
		// 	    $printer->feed(1);
		// 	    $printer->feed(1);
		// 	    $printer->cut();
		// 	    }
		// 	    $printer->close();
		// 	} catch (Exception $e) {
		// 	    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
		// 	}
		// }

		// echo $data['start'];
		// echo "</br>";
		// echo $data['end'];

		

		$data['header'] = $this->load->controller('common/header');

		$data['column_left'] = $this->load->controller('common/column_left');

		$data['footer'] = $this->load->controller('common/footer');

		if ($this->request->server['REQUEST_METHOD'] == 'POST')	{
			//echo "<pre>";print_r($data);exit;
			//echo $this->request->post['end'];exit;
			$this->response->setOutput($this->load->view('catalog/barcodeprint', $data));
		}else{
			//echo "<pre>";print_r($data);exit;
			$this->response->setOutput($this->load->view('catalog/barcode_form', $data));
		}
		
	}



	protected function validateForm() {

		if (!$this->user->hasPermission('modify', 'catalog/barcode')) {

			$this->error['warning'] = $this->language->get('error_permission');

		}



		$datas = $this->request->post;

		if (is_nan($this->request->post['start'])) {
			$this->error['start'] = 'Please Enter  Numner';
		}

		if (is_nan($this->request->post['end'])) {
			$this->error['end'] = 'Please Enter  Numner';
		}


		// if ((utf8_strlen($this->request->post['start']) < 2) || (utf8_strlen($this->request->post['start']) > 255)) {

		// 	$this->error['start'] = 'Please Enter  Numner';

		// }

		// if ((utf8_strlen($this->request->post['end']) < 2) || (utf8_strlen($this->request->post['end']) > 255)) {

		// 	$this->error['end'] = 'Please Enter  Number';

		// }

		// if ((utf8_strlen($this->request->post['capacity']) < 2) || (utf8_strlen($this->request->post['capacity']) > 255)) {

		// 	$this->error['capacity'] = 'Please Enter  capacity';

		// }

		// // else {

		// 	if(isset($this->request->get['department_id'])){

		// 		$is_exist = $this->db->query("SELECT `department_id` FROM `oc_department` WHERE `name` = '".$datas['name']."' AND `department_id` <> '".$this->request->get['department_id']."' ");

		// 		if($is_exist->num_rows > 0){

		// 			$this->error['name'] = 'Department Name Already Exists';

		// 		}

		// 	} else {

		// 		$is_exist = $this->db->query("SELECT `department_id` FROM `oc_department` WHERE `name` = '".$datas['name']."' ");

		// 		if($is_exist->num_rows > 0){

		// 			$this->error['name'] = 'Department Name Already Exists';

		// 		}

		// 	}

		// }



		return !$this->error;

	}
	public function autocomplete() {

		$json = array();



		if (isset($this->request->get['filter_name'])) {

			$this->load->model('catalog/barcode');



			$filter_data = array(

				'filter_name' => $this->request->get['filter_name'],

				'start'       => 0,

				'limit'       => 20

			);



			$results = $this->model_catalog_barcode->getbarcodes($filter_data);



			foreach ($results as $result) {

				$json[] = array(

					'id' => $result['id'],

					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))

				);

			}

		}



		$sort_order = array();



		foreach ($json as $key => $value) {

			$sort_order[$key] = $value['name'];

		}



		array_multisort($sort_order, SORT_ASC, $json);



		$this->response->addHeader('Content-Type: application/json');

		$this->response->setOutput(json_encode($json));

	}

}