<?php
class Controllercatalogcustomerhistory extends Controller {
	private $error = array();

	public function index() {
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		// echo"<pre>";print_r($this->request->get);exit;
		if(isset($this->request->get['filter_name'])){
			$data['filter_name'] = $this->request->get['filter_name'];
		}
		else{
			$data['filter_name'] ='';
		}	

		if(isset($this->request->get['c_id'])){
			$data['c_id'] = $this->request->get['c_id'];
		}
		else{
			$data['c_id'] ='';
		}	

		$order_datas = array();

		if($data['filter_name'] != ''){
		$order_datas = $this->db->query("SELECT * FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON(oi.`order_id` = oit.`order_id`) WHERE  oi. `onacname` = '".$data['filter_name']."' AND oi. `onaccust` = '".$data['c_id']."'  GROUP BY oi.`order_id` ")->rows;

		
		$order_datasss = $this->db->query("SELECT * FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON(oi.`order_id` = oit.`order_id`) WHERE  oi. `onacname` = '".$data['filter_name']."' AND oi. `onaccust` = '".$data['c_id']."'  GROUP BY oi.`order_id` ")->rows;

		}
		$data['order_datas'] = $order_datas;
		$data['order_datasss'] = $order_datasss;
		// echo'<pre>';
		// print_r($order_datasss);
		// exit;
		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/customer_history', $data));
	}

	public function getdatafood(){
		$this->load->model('catalog/order');
		// echo"<pre>";print_r($this->request->get);exit;
		if(isset($this->request->get['onaccust'])){
			$data['onaccust'] = $this->request->get['onaccust'];
		}
		else{
			$data['onaccust'] = '';
		}

		if(isset($this->request->get['onacname'])){
			$data['onacname'] = $this->request->get['onacname'];
		}
		else{
			$data['onacname'] = '';
		}

		if(isset($this->request->get['bill_date'])){
			$data['bill_date'] = $this->request->get['bill_date'];
		}
		else{
			$data['bill_date'] = '';
		}

		if(isset($this->request->get['order_id'])){
			$data['order_id'] = $this->request->get['order_id'];
		}
		else{
			$data['order_id'] = '';
		}

		$json = array();

		if($data['bill_date'] != ''){
		$json = $this->db->query("SELECT * FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON(oi.`order_id` = oit.`order_id`) WHERE oi. `order_id` = '".$data['order_id']."' ")->rows;

		}
		
		$this->response->setOutput(json_encode($json));

	}

	public function getdatafoods(){
		$this->load->model('catalog/order');
		// echo"<pre>";print_r($this->request->get);exit;
		if(isset($this->request->get['onaccust'])){
			$data['onaccust'] = $this->request->get['onaccust'];
		}
		else{
			$data['onaccust'] = '';
		}

		if(isset($this->request->get['onacname'])){
			$data['onacname'] = $this->request->get['onacname'];
		}
		else{
			$data['onacname'] = '';
		}

		if(isset($this->request->get['bill_date'])){
			$data['bill_date'] = $this->request->get['bill_date'];
		}
		else{
			$data['bill_date'] = '';
		}

		if(isset($this->request->get['order_id'])){
			$data['order_id'] = $this->request->get['order_id'];
		}
		else{
			$data['order_id'] = '';
		}

		$json = array();

		
		if($data['bill_date'] != ''){
		
		$json = $this->db->query("SELECT * FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON(oi.`order_id` = oit.`order_id`) WHERE oi. `order_id` = '".$data['order_id']."' ")->rows;
		}
		
		$this->response->setOutput(json_encode($json));

	}
}	