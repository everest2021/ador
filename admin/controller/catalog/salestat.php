<?php
class ControllerCatalogSaleStat extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/salestat');
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/salestat');

		$data['famous_food'] = $this->model_catalog_salestat->mostFamousFood();
		$data['famous_liq'] = $this->model_catalog_salestat->mostFamousLiq();

		$previous_week = strtotime("-1 week +1 day");
		$start_week = strtotime("last sunday midnight",$previous_week);
		$end_week = strtotime("next saturday",$start_week);
		$start_week = date("Y-m-d",$start_week);
		$end_week = date("Y-m-d",$end_week);
		$data['weekly_famous_food'] = $this->model_catalog_salestat->weeklyFamousfood($start_week, $end_week);
		$data['weekly_famous_liq'] = $this->model_catalog_salestat->weeklyFamousLiq($start_week, $end_week);

		$data['highest_sale'] = $this->model_catalog_salestat->highestSale($start_week, $end_week);
		// echo '<pre>';
		// print_r($data['highest_sale']);
		// exit;

		// $cancelamountkot = 0;
		// $cancelqtykot = 0;
		// $cancelamountbot = 0;
		// $cancelqtytbot = 0;
		// $cancelkotbot = array();
		// $totalcancelkotbot = array();
		$bill_dates =  $this->db->query("SELECT bill_date FROM oc_order_info WHERE day_close_status = '0' order by bill_date desc LIMIT 1");
		if($bill_dates->num_rows > 0){
			$bill_datee = $bill_dates->row['bill_date'];
			$bill_date = date('Y-m-d', strtotime($bill_datee));
		} else {
			$bill_date = date('Y-m-d');
		}

		
		$data['fooddata'] = $this->db->query("SELECT SUM(qty) as cancelqty_food, SUM(amt) as cancelamt_food FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE oit.`cancelstatus` = '1' AND is_liq = '0' AND oit.`ismodifier` = '1' AND oit.`date` = '".$bill_date."'")->row;

		$data['liqdata'] = $this->db->query("SELECT SUM(qty) as cancelqty_liq, SUM(amt) as cancelamt_liq FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE oit.`cancelstatus` = '1' AND is_liq = '1' AND oit.`ismodifier` = '1' AND oi.`date` = '".$bill_date."'")->row;

		if($this->model_catalog_order->get_settings('SETTLEMENT') == '1'){
			//$data['unsettlebill'] = $this->model_catalog_salestat->unsettleAmount($bill_date);
		}else{
			$data['unsettlebill'] = 0;
		}
		

		$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$bill_date."'");
		if($advance->num_rows > 0){
			$data['advancetotal'] = $advance->row['advancetotal'];
		} else{
			$data['advancetotal'] = 0;
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/salestat', $data));
	}
}