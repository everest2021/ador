<?php
class Controllercatalogdeliveryboyupdate extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('catalog/deliveryboyupdate');

		$this->document->setTitle($this->language->get('heading_title'));

		//$this->load->model('catalog/customercredit');

		$this->getform();
	}
	protected function getForm() {
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['item_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		// $data['button_save'] = $this->language->get('button_save');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['item_name'])) {
			$data['error_item_name'] = $this->error['item_name'];
		} else {
			$data['error_item_name'] = '';
		}


		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		// if (isset($this->request->get['order_id'])) {
		// 	$data['order_id'] = $this->request->get['order_id'];
		// } else {
		// 	$data['order_id'] = '';
		// }

		$sql = $this->db->query("SELECT dboy_name, dboy_amount, grand_total, given_amount FROM oc_order_info WHERE order_id ='".$this->request->get['order_id']."'")->row;
		
		// echo"<pre>";print_r($this->request->get);exit;

		if (isset($this->request->post['dboy_name'])) {
			$data['dboy_name'] = $this->request->post['dboy_name'];
		} elseif (!empty($sql)) {
			$data['dboy_name'] = $sql['dboy_name'];
		} else {
			$data['dboy_name'] = '';
		}

		if (isset($this->request->post['dboy_amount'])) {
			$data['dboy_amount'] = $this->request->post['dboy_amount'];
		} elseif (!empty($sql)) {
			$data['dboy_amount'] = $sql['dboy_amount'];
		} else {
			$data['dboy_amount'] = '';
		}

		if (isset($this->request->get['order_id'])) {
			$data['order_id'] = $this->request->get['order_id'];
		} elseif (!empty($sql)) {
			$data['order_id'] = $sql['order_id'];
		} else {
			$data['order_id'] = '';
		}

		if (isset($this->request->get['grand_total'])) {
			$data['grand_total'] = $this->request->get['grand_total'];
		} elseif (!empty($sql)) {
			$data['grand_total'] = $sql['grand_total'];
		} else {
			$data['grand_total'] = '';
		}

		if (isset($this->request->get['given_amount'])) {
			$data['given_amount'] = $this->request->get['given_amount'];
		} elseif (!empty($sql)) {
			$data['given_amount'] = $sql['given_amount'];
		} else {
			$data['given_amount'] = '';
		}
		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/customercredit', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('localisation/language');


		if (isset($this->request->post['customername'])) {
			$data['customername'] = $this->request->post['customername'];
		} elseif (!empty($item_info)) {
			$data['customername'] = $item_info['customer_name'];
		} else {
			$data['customername'] = '';
		}

		$data['action'] = $this->url->link('catalog/delivery', 'token=' . $this->session->data['token'] . $url, true);
		$data['cancel'] = $this->url->link('catalog/delivery', 'token=' . $this->session->data['token'] . $url, true);
		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		//echo $data['inc_rate_1'];
		//exit();

		$this->response->setOutput($this->load->view('catalog/deliveryboyupdate', $data));
		// $this->response->redirect($this->url->link('catalog/delivery', 'token=' . $this->session->data['token'] . $url, true));
	}

	public function amts(){

		//echo"<pre>";print_r("UPDATE oc_order_info SET moneyrecive_status = '1' or given_amount ='".$_GET['credit']."' where order_id ='".$this->request->get['orderid']."'");exit;
		$sql = $this->db->query("UPDATE oc_order_info SET moneyrecive_status = '1', given_amount ='".$_GET['credit']."' where order_id ='".$this->request->get['orderid']."'");

		$this->response->redirect($this->url->link('catalog/delivery', 'token=' . $this->session->data['token'], true));
	}

}