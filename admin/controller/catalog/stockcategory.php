<?php
class ControllerCatalogStockcategory extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/stockcategory');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stockcategory');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/stockcategory');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stockcategory');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		print_r($this->request->post);
		exit;*/

			$this->model_catalog_stockcategory->addWaiter($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_msg_code'])) {
				$url .= '&filter_msg_code=' . $this->request->get['filter_msg_code'];
			}

			if (isset($this->request->get['filter_id'])) {
				$url .= '&filter_id=' . $this->request->get['filter_id'];
			}

			if (isset($this->request->get['filter_message'])) {
				$url .= '&filter_message=' . $this->request->get['filter_message'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/stockcategory', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/stockcategory');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stockcategory');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_stockcategory->editWaiter($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_msg_code'])) {
				$url .= '&filter_msg_code=' . $this->request->get['filter_msg_code'];
			}

			if (isset($this->request->get['filter_message'])) {
				$url .= '&filter_message=' . $this->request->get['filter_message'];
			}

			if (isset($this->request->get['filter_waiter_id'])) {
				$url .= '&filter_waiter_id=' . $this->request->get['filter_waiter_id'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/stockcategory', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/stockcategory');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stockcategory');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_stockcategory->deleteWaiter($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_msg_code'])) {
				$url .= '&filter_msg_code=' . $this->request->get['filter_msg_code'];
			}

			if (isset($this->request->get['id'])) {
				$url .= '&id=' . $this->request->get['id'];
			}

			if (isset($this->request->get['filter_message'])) {
				$url .= '&filter_message=' . $this->request->get['filter_message'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/stockcategory', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_code'])) {
			$filter_code = $this->request->get['filter_code'];
		} else {
			$filter_code = null;
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_id'])) {
			$filter_id = $this->request->get['filter_id'];
		} else {
			$filter_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'msg_code';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/stockcategory', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/stockcategory/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['send_timing'] = $this->url->link('catalog/stockcategory/send_timing', 'token=' . $this->session->data['token'] . $url, true);
		


		
		$filter_data = array(
			'filter_code' => $filter_code,
			'filter_name' => $filter_name,
			'filter_id' => $filter_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$sport_total = $this->model_catalog_stockcategory->getTotalWaiter();

		$results = $this->model_catalog_stockcategory->getWaiters($filter_data);
		// echo '<pre>';
		// print_r($results);
		// exit();
		$data['waiters'] = array();
		foreach ($results as $result) {
			$data['waiters'][] = array(
				'filter_code' => $result['code'],
				'filter_name'        => $result['name'],
				'filter_id'  => $result['id'],
				'edit'        => $this->url->link('catalog/stockcategory/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
        $data['sort_name'] = $this->url->link('catalog/stockcategory', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
        $data['sort_code'] = $this->url->link('catalog/stockcategory', 'token=' . $this->session->data['token'] . '&sort=code' . $url, true);
        
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}
		
		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $sport_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/stockcategory', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($sport_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sport_total - $this->config->get('config_limit_admin'))) ? $sport_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sport_total, ceil($sport_total / $this->config->get('config_limit_admin')));

		$data['filter_code'] = $filter_code;
		$data['filter_name'] = $filter_name;
		$data['filter_id'] = $filter_id;
		

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/stockcategory_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}



		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = array();
		}

		

		$url = '';

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/stockcategory', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/stockcategory/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/stockcategory/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/stockcategory', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$waiter_info = $this->model_catalog_stockcategory->getWaiter($this->request->get['id']);
		}

		$data['token'] = $this->session->data['token'];

		$i_code = $this->db->query(" SELECT code FROM oc_stockcategory ORDER BY code DESC limit 0,1 ");
		 if ($i_code->num_rows > 0) {
		 	$new_code = $i_code->row['code'] + 1;
		 }else { 
		 	$new_code = 1; 
		 }

		if (isset($this->request->get['code'])) {
			$data['code'] = $this->request->get['code'];
		} elseif (!empty($waiter_info)) {
			$data['code'] = $waiter_info['code'];
		} else {
			$data['code'] = $new_code;
		}

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($waiter_info)) {
			$data['name'] = $waiter_info['name'];
		} else {
			$data['name'] = '';
		}


		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/stockcategory_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/stockcategory')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 255)) {
			$this->error['name'] = 'Enter valid Name';
		} 

		// if ($this->request->post['department_id'] == '') {
		// 	$this->error['department_id'] = 'Please Enter department ID';
		// }
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/stockcategory')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/stockcategory');

		return !$this->error;
	}

	

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/stockcategory');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_stockcategory->getWaiters($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function send_timing() {
		$this->load->language('catalog/subcategory');
		$this->load->model('catalog/subcategory');
		$subcategory_sql = $this->db->query("SELECT `category_id`,`title_for_time` FROM `oc_subcategory` WHERE `title_for_time` != ''");

		$final_array = array();
		$my_array = array();
		if($subcategory_sql->num_rows > 0){
			//echo "<pre>";print_r($subcategory_sql);exit;
			foreach ($subcategory_sql->rows as $skey => $svalue) {
				if($svalue['title_for_time'] != ''){
					$title_name = $svalue['title_for_time'];
				} else {
					$title_name = 'Best Time Best Food';
				}

				$urbanpiper_category_time =  $this->db->query("SELECT * FROM `oc_urbanpiper_category_time` WHERE category_id = '".$svalue['category_id']."' ");
				$slots = array();
				if($urbanpiper_category_time->num_rows > 0){
					foreach ($urbanpiper_category_time->rows as $ukey => $uvalue) {
						$slots[] = array(
								'day' => $uvalue['day'],
								'slots' => 
									array(
										'0' => array(
											'start_time'=> date ('H:i',strtotime($uvalue['start_time']))   ,
											'end_time'=>  date('H:i',strtotime($uvalue['end_time']))


										)
									)
								); 
						$my_array =  array(
							'title' => $title_name,
							'category_ref_ids' => (array)$uvalue['category_id'],
							'day_slots' => $slots
							
						);
					}
					$final_array['timing_groups'][] = $my_array;
				}
			}
		}
		
		if(!empty($final_array)){

			
			//echo "<pre>";print_r($final_array);exit;
			$bodys = json_encode($final_array);
			//echo "<pre>";print_r($bodys);exit;

			//echo "<pre>";print_r($bodys);exit;

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt_array($curl, array(
		  	CURLOPT_URL => CATEGORY_TIMING_WISE,
		  	CURLOPT_RETURNTRANSFER => true,
		  	CURLOPT_ENCODING => '',
		 	CURLOPT_MAXREDIRS => 10,
		  	CURLOPT_TIMEOUT => 0,
		  	CURLOPT_FOLLOWLOCATION => true,
		  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  	CURLOPT_CUSTOMREQUEST => 'POST',
		  	CURLOPT_POSTFIELDS => $bodys,
			CURLOPT_HTTPHEADER => array(
			    'Content-Type: application/json',
	   			'Authorization:'.URBANPIPER_API_KEY,
			  ),
			));

			$response = curl_exec($curl);
			if (curl_errno($curl)) {
			    $error_msg = curl_error($curl);
			    
			}
			$datas = json_decode($response,true);
			curl_close($curl);
			if (isset($error_msg)) {
				//echo "<pre>";print_r($error_msg);
			}
			//echo "<pre>";print_r($response);exit;

			if($datas['status'] == 'success'){
				$this->session->data['success'] = 'Category Time Set Successfully!';
			} elseif($datas['status'] == 'error') {
				$this->session->data['errors'] = 'Category Time Not Send! ' .$datas['status'];
			} else if (isset($error_msg)) {
				$this->session->data['errors'] = $error_msg;
			} else {
				$this->session->data['errors'] = $datas;
			}
		}
		$this->response->redirect($this->url->link('catalog/subcategory', 'token=' . $this->session->data['token'] , true));
	}
}