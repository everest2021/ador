<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogCancelBot extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/cancelbot');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/cancelbot');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/cancelbot', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['cancelbotdatas'] = array();
		$cancelbotdata = array();

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			foreach($dates as $date){
				$cancelbotdata[$date] = $this->db->query("SELECT `order_id`, name, `kot_no`, SUM(qty) as qty, SUM(rate) as rate, SUM(amt) as amt, `login_name`, `time`, cancel_kot_reason FROM `oc_order_items_report` WHERE `cancelstatus` = '1' AND `is_liq` = '1'  AND ismodifier = '1' AND `date` = '".$date."' GROUP BY `order_id`")->rows;
			}
		}	
		// echo "<pre>";
		// print_r($cancelbotdata);
		// exit();
		$data['cancelbotdatas'] = $cancelbotdata;

		$data['action'] = $this->url->link('catalog/cancelbot', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/cancelbot', $data));
	}

	public function prints(){

		if(isset($this->request->get['filter_category'])){
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = '99';
		}
	
		$data['cancelbotdatas'] = array();
		$cancelbotdata = array();

		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			foreach($dates as $date){
				$cancelbotdata[$date] = $this->db->query("SELECT `order_id`, name, `kot_no`, SUM(qty) as qty, SUM(rate) as rate, SUM(amt) as amt, `login_name`, `time`FROM `oc_order_items_report` WHERE `cancelstatus` = '1' AND `is_liq` = '1'  AND ismodifier = '1' AND `date` = '".$date."' GROUP BY `order_id`")->rows;


				$totalamt = $this->db->query("SELECT  SUM(oit. amt), SUM(oit. qty) FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oit. `cancelstatus` = '1' AND oit. `is_liq` = '1'  AND oit. ismodifier = '1'")->rows;

			}
		}	
		// echo "<pre>";
		// print_r($totalamt);
		// exit();
		$data['cancelbotdatas'] = $cancelbotdata;
		$data['totalamts'] = $totalamt;
		// echo"<pre>";print_r($totalamt);exit;

		$this->load->model('catalog/order');

		try {
		    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

		   	$printer->setEmphasis(true);
		   	$printer->setTextSize(2, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
		    $printer->feed(1);
		    $printer->setTextSize(1, 1);
		    $printer->text("Cancelled Bot Report");
		    $printer->feed(1);
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		   	$printer->setJustification(Printer::JUSTIFY_LEFT);
		   	$printer->text(str_pad("Date :".date('Y-m-d'),30)."Time :".date('H:i:s'));
		   	$printer->feed(1);
		   	$printer->text("----------------------------------------------");
		   	$printer->feed(1);
		   	$printer->text(str_pad("K.RefNo",7)."".str_pad("KotNo.",7)." ".str_pad("Item",15)."".str_pad("Qty",6)."".str_pad("Amount",7)."".str_pad("User",7));
		   	$printer->feed(1);
		    $printer->setEmphasis(false);
		    foreach($cancelbotdata as $nkey => $nvalue){ 
	    	$printer->text("----------------------------------------------");
		    $printer->feed(1);
	    	}  //echo"<pre>";print_r($totalamt);exit;
	    	foreach($nvalue as $dvalue){ 
	    	$printer->text(str_pad($dvalue['order_id'],8)."".str_pad($dvalue['kot_no'],6)."".substr($dvalue['name'],0,15)." ".str_pad($dvalue['qty'],6)."".str_pad($dvalue['amt'],7)."".str_pad(substr($dvalue['login_name'],0,5),1));
				$printer->feed(1);
	    	}
	    	$printer->text("----------------------------------------------");
		    $printer->feed(1); 
		    $printer->text(str_pad("Total Qty",30)."".$totalamt[0]['SUM(oit. qty)']);
			$printer->feed(1);
			$printer->text(str_pad("Total",30)."".$totalamt[0]['SUM(oit. amt)']);
			$printer->feed(1);
			$printer->text("------------------- END REPORT -----------------");
		    $printer->feed(1);
		    $printer->cut();
			$printer->feed(2);
		    // Close printer //
		    $printer->close();
		} catch (Exception $e) {
		   echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
		}
		$this->getList();
	}


	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}