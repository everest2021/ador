<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogTestPrint extends Controller {

	private $error = array();

	public function index() {
		$this->load->language('catalog/testprint');
		$this->document->setTitle($this->language->get('heading_title'));
		// $this->load->model('catalog/testprint');
		$this->load->model('catalog/order');
		$this->getform();

	}

	protected function getForm() {

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_rate'] = $this->language->get('entry_rate');
		// $data['entry_capacity'] = $this->language->get('entry_capacity');

		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$url = '';



		if (isset($this->request->get['filter_name'])) {

			$url .= '&filter_name=' . $this->request->get['filter_name'];

		}


		if (isset($this->request->get['sort'])) {

			$url .= '&sort=' . $this->request->get['sort'];

		}



		if (isset($this->request->get['order'])) {

			$url .= '&order=' . $this->request->get['order'];

		}



		if (isset($this->request->get['page'])) {

			$url .= '&page=' . $this->request->get['page'];

		}



		$data['breadcrumbs'] = array();



		$data['breadcrumbs'][] = array(

			'text' => $this->language->get('text_home'),

			'href' => $this->url->link('common/barcode', 'token=' . $this->session->data['token'], true)

		);



		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/testprint', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/testprint', 'token=' . $this->session->data['token'] . $url, true);
		}


		$data['cancel'] = $this->url->link('catalog/testprint', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['printer'])) {
			$data['printer'] = $this->request->post['printer'];
		} else {
			$data['printer'] = '';
		}

		if (isset($this->request->post['ip'])) {
			$data['ip'] = $this->request->post['ip'];
		} else {
			$data['ip'] = '';
		}

		if (isset($this->request->post['msg'])) {
			$data['msg'] = $this->request->post['msg'];
		} else {
			$data['msg'] = '';
		}

		// $user_printers = $this->db->query("SELECT printer_type, printer_name FROM oc_user WHERE printer_type <> '' AND printer_name <> '' UNION ALL SELECT printer_type, printer FROM oc_kotgroup WHERE printer_type <> '' AND printer <> '' UNION ALL SELECT bill_printer_type, bill_printer_name FROM oc_location WHERE bill_printer_type <> '' AND bill_printer_name <> '' ")->rows;

		// // $uniq_user_printers = array_unique($user_printers);
		// $data['uniq_user_printers'] = array_map("unserialize", array_unique(array_map("serialize", $user_printers)));

		$data['header'] = $this->load->controller('common/header');

		$data['column_left'] = $this->load->controller('common/column_left');

		$data['footer'] = $this->load->controller('common/footer');

		if ($this->request->server['REQUEST_METHOD'] == 'POST')	{
			//echo "<pre>";print_r($data);exit;
			// echo"<pre>";
			// print_r($this->request->post);
			// exit;
			$this->response->setOutput($this->load->view('catalog/testprint_form', $data));
		}else{
			//echo "<pre>";print_r($data);exit;
			$this->response->setOutput($this->load->view('catalog/testprint_form', $data));
		}
		
	}

	public function getprinters() {
		$user_printers = $this->db->query("SELECT printer_type, printer_name FROM oc_user WHERE printer_type = '".$this->request->get['ptype']."' AND printer_name <> '' UNION ALL SELECT printer_type, printer FROM oc_kotgroup WHERE printer_type = '".$this->request->get['ptype']."' AND printer <> '' UNION ALL SELECT bill_printer_type, bill_printer_name FROM oc_location WHERE bill_printer_type = '".$this->request->get['ptype']."' AND bill_printer_name <> '' ")->rows;

		$json['uniq_user_printers'] = array_map("unserialize", array_unique(array_map("serialize", $user_printers)));
		if (!empty($json['uniq_user_printers'])) {
			$json['success'] = 1;
		} else {
			$json['success'] = 0;
		}


	    $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function print_test() {
		// echo"<pre>";
		// print_r($this->request->get);
		// exit;
	    $json['print_success'] = 0;

		$printtype = '';
		$printername = '';
		if ($this->request->get['printer'] == 1) {
			$printtype = 'Network';
			$printername = $this->request->get['ip'];
		} elseif ($this->request->get['printer'] == 2) {
			$printtype = 'Windows';
			$printername = $this->request->get['ip'];
		}

		try {
			if($printtype == 'Network'){
				$connector = new NetworkPrintConnector($printername, 9100);
			} else if($printtype == 'Windows'){
			 	$connector = new WindowsPrintConnector($printername);
			} else {
			 	$connector = '';
			}
			$printer = new Printer($connector);

		   	$printer->setEmphasis(true);
		   	$printer->setTextSize(2, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		   	$this->load->model('catalog/order');
		    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
		    $printer->feed(2);
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		    $printer->text('Your test print is successfully printed!');
		    $printer->feed(2);
		    $printer->text($this->request->get['msg']);
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		    $printer->feed(2);
		    $printer->text('Don\'t Say No To Food');
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		    $printer->feed(5);
		    $printer->cut();
		    $printer->close();
		    $json['print_success'] = 1;
		    unset($this->session->data['print_success']);
		} catch (Exception $e) {
		}
	    $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

}