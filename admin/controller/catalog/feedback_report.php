<?php
class ControllerCatalogFeedbackReport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/feedback');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/feedback');

		$this->load->model('catalog/product');

		$this->getFeedback();
	}

	public function add() {

		$this->load->language('catalog/feedback');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/feedback');
	
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//echo "i m in";exit;
			// echo'<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_feedback->addFeedback($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/feedback', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/feedback');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/feedback');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo'<pre>';
			// print_r($this->request->get);
			// exit;
			$this->model_catalog_feedback->editFeedback($this->request->get['feedback_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/feedback', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/feedback');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/feedback');
		if (isset($this->request->get['feedback_id'])){
			$feedback_id=$this->request->get['feedback_id'];
			$this->model_catalog_feedback->deleteFeedback($feedback_id);
			$this->response->redirect($this->url->link('catalog/feedback', 'token=' . $this->session->data['token'] , true));
		}else{
			$this->response->redirect($this->url->link('catalog/feedback', 'token=' . $this->session->data['token'] , true));
		}
		

		//$this->getFeedback();
	}

	protected function getFeedback() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_contact_no'])) {
			$filter_contact_no = $this->request->get['filter_contact_no'];
		} else {
			$filter_contact_no = null;
		}

		if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = null;
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}



		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_contact_no'])) {
			$url .= '&filter_contact_no=' . urlencode(html_entity_decode($this->request->get['filter_contact_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}


		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/feedback_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		// $data['add'] = $this->url->link('catalog/feedback/add', 'token=' . $this->session->data['token'] . $url, true);
		// $data['delete'] = $this->url->link('catalog/feedback/delete', 'token=' . $this->session->data['token'] . $url, true);
		// $data['export'] = $this->url->link('catalog/feedback/export_new1', 'token=' . $this->session->data['token'] . $url, true);

		$data['feedbacks'] = array();

		$filter_data = array(
			'filter_name'	  => $filter_name,
			'filter_contact_no'	  => $filter_contact_no,
			'filter_email'	  => $filter_email,
			'filter_date'	  => $filter_date,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		//$data = $this->request->get;
		
		//$data = $this->request->post;

		$feedback_total = $this->model_catalog_feedback->getTotalFeedbacks($filter_data);
		$sql = "SELECT * FROM oc_feedback fcd LEFT JOIN oc_feedback_transaction fbt ON (fcd.`id` = fbt.`cust_id`) WHERE 1=1 ";
		if (!empty($this->request->get['filter_name'])) {
			$sql .= " AND fcd.`name` LIKE '%" . $this->db->escape($this->request->get['filter_name']) . "%'";
		}

		if (!empty($this->request->get['filter_date'])) {
			$sql .= " AND fcd.`date` = '" . $this->db->escape($this->request->get['filter_date']) . "'";
		}

		if (!empty($this->request->get['filter_contact_no'])) {
			$sql .= " AND fcd.`contact_no` LIKE '%" . $this->db->escape($this->request->get['filter_contact_no']) . "%'";
		}

		if (!empty($this->request->get['filter_email'])) {
			$sql .= " AND fcd.`email` LIKE '%" . $this->db->escape($this->request->get['filter_email']) . "%'";
		}

		$sql .= " GROUP BY fbt.`cust_id` ";
		// 	echo'<pre>';
		// print_r($sql);
		// exit();
		$results = $this->db->query($sql)->rows;
		
		//$results = $this->model_catalog_feedback->getFeedbacks($filter_data);
		foreach ($results as $result) {
			$data['feedbacks'][] = array(
				'feedback_id'  => $result['id'],
				'cust_name'     => $result['name'],
				'contact_no'     => $result['contact_no'],
				'email'     => $result['email'],
				'date'     => date('d-m-Y',strtotime($result['date'])),
				'edit'     => $this->url->link('catalog/feedback_report/edit', 'token=' . $this->session->data['token'] . '&feedback_id=' . $result['cust_id'] . $url, true),
				'delete'     => $this->url->link('catalog/feedback_report/delete', 'token=' . $this->session->data['token'] . '&feedback_id=' . $result['cust_id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_contact_no'])) {
			$url .= '&filter_contact_no=' . urlencode(html_entity_decode($this->request->get['filter_contact_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/feedback_report', 'token=' . $this->session->data['token'] . '&sort=student_name' . $url, true);
		
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_contact_no'])) {
			$url .= '&filter_contact_no=' . urlencode(html_entity_decode($this->request->get['filter_contact_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $feedback_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/feedback_report', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($feedback_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($feedback_total - $this->config->get('config_limit_admin'))) ? $feedback_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $feedback_total, ceil($feedback_total / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_contact_no'] = $filter_contact_no;
		$data['filter_email'] = $filter_email;
		$data['filter_date'] = $filter_date;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];

		$this->response->setOutput($this->load->view('catalog/feedback_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['feedback_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['contact_no'])) {
			$data['error_contact_no'] = $this->error['contact_no'];
		} else {
			$data['error_contact_no'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		$url = '';

		

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_student_id'])) {
			$url .= '&filter_student_id=' . urlencode(html_entity_decode($this->request->get['filter_student_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_contact_no'])) {
			$url .= '&filter_contact_no=' . urlencode(html_entity_decode($this->request->get['filter_contact_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_teacher_id'])) {
			$url .= '&filter_teacher_id=' . urlencode(html_entity_decode($this->request->get['filter_teacher_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_division_id'])) {
			$url .= '&filter_division_id=' . urlencode(html_entity_decode($this->request->get['filter_division_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . urlencode(html_entity_decode($this->request->get['filter_date_added'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/feedback', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['feedback_id'])) {
			$data['is_new'] = 1; 
			$data['action'] = $this->url->link('catalog/feedback/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['is_new'] = 0;
			$data['action'] = $this->url->link('catalog/feedback/edit', 'token=' . $this->session->data['token'] . '&feedback_id=' . $this->request->get['feedback_id'] . $url, true);
		}
		$data['cancel'] = $this->url->link('catalog/feedback_report', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['feedback_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$feedback_info = $this->model_catalog_feedback->getFeedback($this->request->get['feedback_id']);
		}

		// 	echo "<pre>";
		// print_r($feedback_info);
		// exit();
		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($feedback_info)) {
			$data['name'] = $feedback_info['name'];
		} else {
			$data['name'] = '';
		}

		if (isset($this->request->post['contact_no'])) {
			$data['contact_no'] = $this->request->post['contact_no'];
		} elseif (!empty($feedback_info)) {
			$data['contact_no'] = $feedback_info['contact_no'];
		} else {
			$data['contact_no'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} elseif (!empty($feedback_info)) {
			$data['email'] = $feedback_info['email'];
		} else {
			$data['email'] = '';
		}

		if (isset($this->request->post['location'])) {
			$data['location'] = $this->request->post['location'];
		} elseif (!empty($feedback_info)) {
			$data['location'] = $feedback_info['location'];
		} else {
			$data['location'] = '';
		}


		if (isset($this->request->post['date_dob'])) {
			$data['date_dob'] = $this->request->post['date_dob'];
		} elseif (!empty($feedback_info)) {
			$data['date_dob'] = $feedback_info['date_dob'];
		} else {
			$data['date_dob'] = '';
		}

		if (isset($this->request->post['month_dob'])) {
			$data['month_dob'] = $this->request->post['month_dob'];
		} elseif (!empty($feedback_info)) {
			$data['month_dob'] = $feedback_info['month_dob'];
		} else {
			$data['month_dob'] = '';
		}

		if (isset($this->request->post['year_dob'])) {
			$data['year_dob'] = $this->request->post['year_dob'];
		} elseif (!empty($feedback_info)) {
			$data['year_dob'] = $feedback_info['year_dob'];
		} else {
			$data['year_dob'] = '';
		}


		if (isset($this->request->post['date_doa'])) {
			$data['date_doa'] = $this->request->post['date_doa'];
		} elseif (!empty($feedback_info)) {
			$data['date_doa'] = $feedback_info['date_doa'];
		} else {
			$data['date_doa'] = '';
		}

		if (isset($this->request->post['month_doa'])) {
			$data['month_doa'] = $this->request->post['month_doa'];
		} elseif (!empty($feedback_info)) {
			$data['month_doa'] = $feedback_info['month_doa'];
		} else {
			$data['month_doa'] = '';
		}

		if (isset($this->request->post['year_doa'])) {
			$data['year_doa'] = $this->request->post['year_doa'];
		} elseif (!empty($feedback_info)) {
			$data['year_doa'] = $feedback_info['year_doa'];
		} else {
			$data['year_doa'] = '';
		}

		if (isset($this->request->post['suggestion'])) {
			$data['suggestion'] = $this->request->post['suggestion'];
		} elseif (!empty($feedback_info)) {
			$data['suggestion'] = $feedback_info['suggestion'];
		} else {
			$data['suggestion'] = '';
		}

		if (isset($this->request->post['remark'])) {
			$data['remark'] = $this->request->post['remark'];
		} elseif (!empty($feedback_info)) {
			$data['remark'] = $feedback_info['remark'];
		} else {
			$data['remark'] = '';
		}


		if (isset($this->request->post['ans_datas'])) {
			$data['ans_datas'] = $this->request->post['ans_datas'];
		} elseif (!empty($feedback_info)) {
			$data['ans_datas'] =  $this->db->query("SELECT `question_id`,`values` FROM `oc_feedback_transaction` WHERE cust_id = '".$feedback_info['cust_id']."' ")->rows;
		// 	echo "<pre>";
		// print_r($data['ans_datas']);
		// exit();
		} else {
			$data['ans_datas'] = '';
		}

		

		$data['location_datas'] = array(
			'Please-Select' => 'Please-Select',
			'Agashi' => 'Agashi',
			'Aannad-Nagar' => 'Aannad-Nagar',
			'Gass' => 'Gass',
			'Nalasopara' => 'Nalasopara',
			'Navghar-Manickpur' => 'Navghar-Manickpur',
			'Ram-Rahim-Nagar' => 'Ram-Rahim-Nagar',
			'Sai-Nagar' => 'Sai-Nagar',
			'Sativali' => 'Sativali',
			'Vishal-Nagar' => 'Vishal-Nagar',
			'Vasai' => 'Vasai',
			'Virar' => 'Virar',
			'Waliv' => 'Waliv',
		);

		$question_data = $this->db->query("SELECT * FROM `oc_feedback_question` ")->rows;
		$data['question_datas'] = $question_data;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/feedback_form_report', $data));
	}

	protected function validateForm() {

		if (!$this->user->hasPermission('modify', 'catalog/feedback')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$datas = $this->request->post;

		if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 64)) {
			
			$this->error['name'] = $this->language->get('error_name');
		}

		if ((utf8_strlen($this->request->post['email']) < 1) || (utf8_strlen($this->request->post['email']) > 64)) {
			
			$this->error['email'] = "Please Enter E-mail ID!";
		}

		if ((utf8_strlen($this->request->post['contact_no']) < 1) || (utf8_strlen($this->request->post['contact_no']) > 11)) {
			
			$this->error['contact_no'] = "Please Enter Contact No.!";
		}

		$is_exist = $this->db->query("SELECT `contact_no` FROM `oc_feedback` WHERE `contact_no` = '".$datas['contact_no']."' ");
		if($is_exist->num_rows > 0){
			$this->error['contact_no'] = 'Contact No Already Exists';
		}

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_contact_no'])) {
			$this->load->model('catalog/feedback');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_contact_no'],
				'start'       => 0,
				'limit'       => 20

			);
			$sql = "SELECT * FROM " . DB_PREFIX . "fb_cus_detail WHERE 1=1 ";
				if (!empty($this->request->get['filter_contact_no'])) {
					$sql .= " AND `contact_no` LIKE '%" . $this->request->get['filter_contact_no'] . "%'";
				}
		
			$results = $this->db->query($sql)->rows;
			//$results = $this->model_catalog_feedback->getcontacts($filter_data);
			// echo'<pre>';
			// print_r($this->request->get['filter_contact_no']);
			// exit;
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'contact_no'  => strip_tags(html_entity_decode($result['contact_no'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['contact_no'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');

		$this->response->setOutput(json_encode($json));
	}

	public function autocompletename() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/feedback');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20

			);
			$sql = "SELECT * FROM " . DB_PREFIX . "fb_cus_detail WHERE 1=1 ";
				if (!empty($this->request->get['filter_name'])) {
					$sql .= " AND `name` LIKE '%" . $this->request->get['filter_name'] . "%'";
				}
		
			$results = $this->db->query($sql)->rows;
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'name'  => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');

		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteemail() {
		$json = array();

		if (isset($this->request->get['filter_email'])) {
			$this->load->model('catalog/feedback');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_email'],
				'start'       => 0,
				'limit'       => 20

			);
			$sql = "SELECT * FROM " . DB_PREFIX . "fb_cus_detail WHERE 1=1 ";
				if (!empty($this->request->get['filter_email'])) {
					$sql .= " AND `email` LIKE '%" . $this->request->get['filter_email'] . "%'";
				}
		
			$results = $this->db->query($sql)->rows;
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'email'  => strip_tags(html_entity_decode($result['email'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['email'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');

		$this->response->setOutput(json_encode($json));
	}
	
	public function autocompletedate() {
		$json = array();

		if (isset($this->request->get['filter_date'])) {
			$this->load->model('catalog/feedback');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_date'],
				'start'       => 0,
				'limit'       => 20

			);
			$sql = "SELECT * FROM " . DB_PREFIX . "fb_cus_detail WHERE 1=1 ";
				if (!empty($this->request->get['filter_date'])) {
					$sql .= " AND `date` LIKE '%" . $this->request->get['filter_date'] . "%'";
				}
		
			$results = $this->db->query($sql)->rows;
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'date'  => strip_tags(html_entity_decode($result['date'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['date'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');

		$this->response->setOutput(json_encode($json));
	}
}