<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class Controllercatalogcustomerpoint extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/report');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/customer_point');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/customer_point', 'token=' . $this->session->data['token'] . $url, true)
		);


		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';

		$data['nc_kot_amt'] = 0;

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate'])){

			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);
			$data['custp'] = '';
			$data['gamt'] = '';
			
			foreach($dates as $date){

				$billdata[$date] = $this->db->query("SELECT * FROM `oc_order_info` oit LEFT JOIN oc_order_items oi on (oit.`order_id`=oi.`order_id`) WHERE oit.`bill_date` = '".$date."' AND `bill_status` = '1' AND `cust_id` !='' GROUP BY oi.`order_id` ")->rows;

				$custpo = $this->db->query("SELECT sum(cust_point) as custp FROM `oc_order_info` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `cust_id` !=''  ")->row;

				// foreach ($custid as $bkey => $bvalue) {
				// 		$custid = $bvalue['cust_id'];
				// }

				// $cpoint = $this->db->query("SELECT sum(cust_point) as custp FROM `oc_order_info` WHERE `cust_id` = '".$custid."' ")->rows;

				$new = $custpo['custp'];

				$data['custp'] = $new ;
				// echo"<pre>";print_r($data['custp']);exit;
			}
		} 
		


		
		



		$data['billdatas'] = $billdata;
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['edit_bill'] = $this->user->getEditBill();

		$data['action'] = $this->url->link('catalog/customer_point', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/customer_point', $data));
	}

	public function prints(){

		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$data['enddate'] = $this->request->get['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';

		$data['nc_kot_amt'] = 0;

		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate'])){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);
			$data['custp'] = '';
			$data['gamt'] = '';
			
			foreach($dates as $date){

				$billdata[$date] = $this->db->query("SELECT * FROM `oc_order_info` oit LEFT JOIN oc_order_items oi on (oit.`order_id`=oi.`order_id`) WHERE oit.`bill_date` = '".$date."' AND `bill_status` = '1' AND `cust_id` !='' GROUP BY oi.`order_id` ")->rows;

				$custpo = $this->db->query("SELECT sum(cust_point) as custp FROM `oc_order_info` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `cust_id` !=''  ")->row;

				$new = $custpo['custp'];

				$data['custp'] = $new ;

		// 		foreach ($billdata as $key => $value) {
		// 			foreach ($value as $bvalue) {
		// 				# code...
		// echo"<pre>";print_r($bvalue);exit;	
		// 			}
		// 		}
			}
		} 
		

		//echo"<pre>";print_r($billdata);exit;	
		$data['billdatas'] = $billdata;

		$this->load->model('catalog/order');

		try {
		    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

		   	$printer->setEmphasis(true);
		   	$printer->setTextSize(2, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
		    $printer->feed(1);
		    $printer->setTextSize(1, 1);
		    $printer->text("Recipe");
		    $printer->feed(1);
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		   	$printer->setJustification(Printer::JUSTIFY_LEFT);
		   	$printer->text(str_pad("Date :".date('Y-m-d'),30)."Time :".date('H:i:s'));
		   	$printer->feed(1);
		   	$printer->text("----------------------------------------------");
		   	$printer->feed(1);
		   	$printer->text(str_pad("BNo",7)."".str_pad("Date.",7)." ".str_pad("CustName",15)."".str_pad("BillAmt",10)."".str_pad("Point",7));
		   	$printer->feed(1);
		    $printer->setEmphasis(false);
		    foreach($billdata as $nkey => $nvalue){ 
	    	$printer->text("----------------------------------------------");
		    $printer->feed(1);
		    	foreach($nvalue as $dvalue){ 
		    	$printer->text(str_pad($dvalue['billno'],4)."".str_pad($dvalue['bill_date'],6)."  ".str_pad($dvalue['cust_name'],15)." ".str_pad($dvalue['grand_total'],6)."  ".str_pad($dvalue['cust_point'],7));
					$printer->feed(1);
		    	}
		    }
	    	$printer->text("----------------------------------------------");
		    $printer->feed(1); 
			$printer->text(str_pad("Total Points : ",30)."".$new);
			$printer->feed(1);
			$printer->text("------------------- END REPORT -----------------");
		    $printer->feed(1);
		    $printer->cut();
			$printer->feed(2);
		    // Close printer //
		    $printer->close();
		} catch (Exception $e) {
		   echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
		}
		$this->getList();
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	
}