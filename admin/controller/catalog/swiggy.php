<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\EscposImage;

class ControllerCatalogSwiggy extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/swiggy');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/swiggy');
		// echo "inn";exit;

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/swiggy');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/swiggy');
		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
			 print_r($this->request->post);
			exit;*/

			$this->model_catalog_swiggy->addBrand($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_brand'])) {
				$url .= '&filter_brand=' . $this->request->get['filter_brand'];
			}

			if (isset($this->request->get['filter_brand_id'])) {
				$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
			}


			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/swiggy', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/brand');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/brand');
		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_brand->editBrand($this->request->get['brand_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_brand'])) {
				$url .= '&filter_brand=' . $this->request->get['filter_brand'];
			}

			if (isset($this->request->get['filter_subcategory_id'])) {
				$url .= '&filter_subcategory_id=' . $this->request->get['filter_subcategory_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/brand', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/brand');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/brand');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $brand_id) {
				$this->model_catalog_brand->deleteBrand($brand_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_brand'])) {
				$url .= '&filter_brand=' . $this->request->get['filter_brand'];
			}

			if (isset($this->request->get['filter_brand_id'])) {
				$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/brand', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}


	protected function getList() {
		// echo "<pre>";print_r($this->request->get);exit;
		if (isset($this->request->get['filter_date'])) {
			$filter_date = date("Y-m-d", strtotime($this->request->get['filter_date'])); 
			$data['filter_date'] = date("m/d/Y", strtotime($this->request->get['filter_date'])); 
		} else {
			$filter_date = '';
			$data['filter_date'] = '';
		}

		if (isset($this->request->get['filter_partner'])) {
			$filter_partner = $this->request->get['filter_partner'];
		} else {
			$filter_partner = '';
		}

		if (isset($this->request->get['order_status'])) {
			$order_status = $this->request->get['order_status'];
		} else {
			$order_status = '0';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['order_status'])) {
			$url .= '&order_status=' . $this->request->get['order_status'];
		}
	
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/swiggy', 'token=' . $this->session->data['token'] . $url, true)
		);
		$data['add'] = $this->url->link('catalog/swiggy/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/swiggy/delete', 'token=' . $this->session->data['token'] . $url, true);

		$filter_data = array(
			'filter_date'	  	=> $filter_date,
			'filter_partner'   	=> $filter_partner,
			'order_status'   	=> $order_status,
			'order'           	=> $order,
			'start'           	=> ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           	=> $this->config->get('config_limit_admin')
		);

		// echo "<pre>";print_r($filter_data);exit;
		$results = $this->model_catalog_swiggy->getorders($filter_data);
		// echo "<pre>";print_r($results);exit;
		$orderarray = array();
		foreach ($results as $key => $cvalue) { 
			$order_data = $this->db->query("SELECT * FROM `oc_order_info` oi LEFT JOIN `oc_order_items` ooi ON(oi.`order_id` = ooi.`order_id`) WHERE oi.`online_order_id` ='".$cvalue['order_id']."' GROUP BY ooi.order_id ");

			if ($order_data->num_rows > 0) {
				$kot_status = $order_data->row['kot_status'];
			} else {
				$kot_status = '0';
			}

			$order_items = $this->db->query("SELECT * FROM `oc_online_order_items` WHERE `order_id` ='".$cvalue['order_id']."' ");

			if ($order_items->num_rows > 0) {
				$order_items = $order_items->rows;
			} else {
				$order_items = '';
			}
			// echo "<pre>";print_r($cvalue);exit;

			if ($cvalue['delivery_partner'] == 1) {
				$delivery_partner = 'Swiggy';
			} elseif($cvalue['delivery_partner'] == 2) {
				$delivery_partner = 'Zomato';
			} else {
				$delivery_partner = '';
			}
			$orderarray[] = array(
				'order_id' 			=> $cvalue['order_id'],
				'date' 				=> $cvalue['date'],
				'totals' 			=> $cvalue['totals'],
				'cust_name' 		=> $cvalue['customer_name'],
				'time' 				=> $cvalue['time'],
				'delivery_partner' 	=> $delivery_partner,
				'kot_status' 		=> $kot_status,
				'order_itemsss' 	=> $order_items,
			);
			// }
		}
		$data['orderarray'] = $orderarray;

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}


		if (isset($this->session->data['warning'])) {
			$data['warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$data['warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['order_status'])) {
			$url .= '&order_status=' . $this->request->get['order_status'];
		}
		// if (isset($this->request->get['filter_brand'])) {
		
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['brand'] = $this->url->link('catalog/swiggy', 'token=' . $this->session->data['token'] . '&sort=brand' . $url, true);
		$url = '';

		if (isset($this->request->get['order_status'])) {
			$url .= '&order_status=' . $this->request->get['order_status'];
		}
		
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$order_total = 1;
		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/swiggy', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

		// $data['filter_brand'] = $filter_brand;
		// $data['filter_brand_id'] = $filter_brand_id;
		// $data['filter_subcategory'] = $filter_subcategory;
		// $data['filter_subcategory_id'] = $filter_subcategory_id;
		//$data['sort'] = $sort;
		$data['order'] = $order;

		// = $this->db->query("SELECT * FROM oc_app_item WHERE `order_id` = 11")->rows;
		
		$data['action'] = $this->url->link('catalog/swiggy', 'token=' . $this->session->data['token'] . $url, true);
		//echo $data['action'];exit;
		$data['order_status'] = $order_status;
		// echo "<pre>";
		// print_r($filter_date);
		// exit();
		// $data['filter_date'] = date("m/d/Y", strtotime($filter_date)); 
		$data['filter_partner'] = $filter_partner;
		$data['order_status'] = $order_status;
		// echo "<pre>";
		// print_r($orderarray);
		// exit();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/swiggy_list', $data));
	}

	public function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		$data['success'] = '';

		$cutumer = $this->db->query("SELECT * FROM  oc_order_app WHERE order_id = '".$this->request->get['order_id']."' ")->row;
		// echo "<pre>";
		// print_r($cutumer);exit;
		$cut_detail = array(
			'order_no' => $cutumer['order_id'],	
			'cus_name' => $cutumer['cus_name'],
			'delivery_address' => $cutumer['delivery_address'],
			'contact' => $cutumer['contact'],
			'exp_delivery_time' => $cutumer['exp_delivery_time'],
			'status' => $cutumer['status'],
			// 'except'        => $this->url->link('catalog/swiggy/except', 'token='. $this->session->data['token'] .'&order_id='.$cutumer['order_id'], true),
			//'cancel'        => $this->url->link('catalog/swiggy/cancle', 'token='. $this->session->data['token'] .'&order_id='.$cutumer['order_id'], true),
		);

		$item_info = $this->db->query("SELECT * FROM  oc_app_item  WHERE order_id = '".$this->request->get['order_id']."' ")->rows;
		$itemarray = array();
		foreach($item_info as $test2){
			$itemarray[] = array(
				'total' => $test2['total'],
				'quantity' => $test2['quantity'],
				'rate'  => $test2['rate'],
				'item_name' => $test2['item_name']
			);
		}

		$data['cut_detail'] = $cut_detail;
		$data['item_detail'] = $itemarray;
		$data['except'] = $this->url->link('catalog/swiggy/except', 'token='. $this->session->data['token'] .'&order_id='.$cutumer['order_id'], true); 
		$data['cancel'] = $this->url->link('catalog/swiggy/cancel', 'token='. $this->session->data['token'] .'&order_id='.$cutumer['order_id'], true);
		$data['back'] = $this->url->link('catalog/swiggy', 'token=' . $this->session->data['token'], true);

		$data['timee'] = array(
			'5' => 5,
			'10' => 10,
			'15' => 15,
			'30' => 30, 
			'45' => 45
		);
		//echo "in";exit;
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/swiggy_form',$data));
	}

	public function order_itemss() {
		
		$json = array();
		if(isset($this->request->get['order_id'])){
			$results = $this->db->query("SELECT * FROM `oc_online_order_items` WHERE `order_id` ='".$this->request->get['order_id']."' ")->rows;
			// echo "<pre>";print_r($results);exit;
			if($results){
				$html = '';
				$html .= '<div class="table-wrapper">';
				$html .= '<table class="fl-table" style="text-align: center;">';
				$html .= '<thead>';
				$html .= '<tr>';
				$html .= '<th style="text-align: center;">Sr No.</th>';
				$html .= '<th style="text-align: center;">Item Name</th>';
				$html .= '<th style="text-align: center;">Quentity</th>';
				$html .= '<th style="text-align: center;">Rate</th>';
				$html .= '<th style="text-align: center;">Message</th>';
				$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody>';
				$total_item = '';
				$total_amt = '';
				$cnt = '1';
				foreach ($results as $rkey => $result) {
					// echo"<pre>";print_r($result);exit;
					$html .= '<tr>';
					$html .= '<td style="text-align: center;">'.$cnt.'</td>';
					$html .= '<td style="text-align: center;">'.$result['item_name'].'</td>';
					$html .= '<td style="text-align: center;">'.$result['qty'].'</td>';
					$html .= '<td style="text-align: center;">'.$result['rate'].'</td>';
					$html .= '<td style="text-align: center;">'.$result['extra_message'].'</td>';
					$html .= '</tr>';
					$total_item = $total_item + $result['qty'];
					$total_amt = $total_amt + $result['rate'];
					$cnt++;
				}

				$html .= '<tr>';
				$html .= '<td style="text-align: center;" colspan="1"></td>';
				$html .= '<td style="text-align: center;" colspan="1">TOTAL  :- </td>';
				$html .= '<td style="text-align: center;" colspan="1">'.$total_item.'</td>';
				$html .= '<td style="text-align: center;" colspan="1">'.$total_amt.'</td>';
				$html .= '</tr>';
				$html .= '<tbody>';
				$html .= '</tr>';
				$html .= '</thead>';
				$html .= '</table>';
				$html .= '</div>';

				$json['html'] = $html;
			}
		}	

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	// KOT FUNCTION START
	public function prints() {
		// echo "<pre>";print_r($this->request->post);exit;
		if(isset($this->request->get['online_order_id'])){
			$online_order_id = $this->request->get['online_order_id'];
		} else {
			$online_order_id = '';
		}

		if(isset($this->request->get['edit']) && ($this->request->get['edit'] == 1)){
			$edit = $this->request->get['edit'];
		} else{
			$edit = '0';
		}

		$this->load->model('catalog/swiggy');
		$this->load->model('catalog/order');

		$order_data = $this->db->query("SELECT * FROM `oc_online_order` oo LEFT JOIN `oc_online_order_items` ooi ON(oo.`order_id` = ooi.`order_id`) WHERE oo.`order_id` ='".$online_order_id."' ")->rows;
		// echo "<pre>";print_r($order_data);exit;
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $online_order_id !='') {
			$order_id = $this->model_catalog_swiggy->addOrder($order_data);
		} 
		// else {
		// 	$order_id = $this->request->get['order_id'];
		// }
		// echo "<pre>";print_r($order_id);exit;

		if ($order_id || isset($this->request->get['order_id'])) {

			$anss = $this->db->query("SELECT `id`,`order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '0' AND cancelstatus = '0' ORDER BY id,subcategoryid")->rows;
			// echo "<pre>";print_r("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '0' AND cancelstatus = '0' AND ismodifier = '1' ORDER BY id,subcategoryid");exit;

			$kot_group_datas = array();
			$printerinfos = $this->db->query("SELECT `subcategory`, `printer_type`, `printer`, `description`, `code` FROM oc_kotgroup WHERE master_kotprint = '0'")->rows;
			

			foreach($printerinfos as $pkeys => $pvalues){
				if($pvalues['subcategory'] != ''){
					$subcategoryid_exp = explode(',', $pvalues['subcategory']);
					foreach($subcategoryid_exp as $skey => $svalue){
						$kot_group_datas[$svalue] = $pvalues;
					}
				}
			}

			$infos_normal = array();
			$infos_cancel = array();
			$infos_liqurnormal = array();
			$infos_liqurcancel = array();
			$infos = array();
			$modifierdata = array();
			$allKot = array();
			$ans = $this->db->query("SELECT `order_id`, `time_added`, `date_added`, `location`, `location_id`, `t_name`, `table_id`, `waiter`, `waiter_id`, `captain`, `captain_id`, `item_quantity`, `total_items`, `login_id`, `login_name`, `person`, `ftotal`, `ltotal`, `grand_total`, `bill_status` ,`cust_name`,`cust_contact`,`cust_address` FROM oc_order_info WHERE order_id = '".$order_id."'")->rows;
			//$orderitem_time = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' ORDER BY `id` DESC LIMIT 1")->row['time'];

			$locationData =  $this->db->query("SELECT `kot_different`, `kot_copy`, `bill_copy`, `direct_bill`, `bill_printer_name`, `bill_printer_type` FROM oc_location WHERE location_id = '".$ans[0]['location_id']."'");
			if($locationData->num_rows > 0){
				$locationData = $locationData->row;
				if($locationData['kot_different'] == 1){
					$kot_different = 1;
				} else {
					$kot_different = 0;
				}
				$kot_copy = $locationData['kot_copy'];
				$bill_copy = $locationData['bill_copy'];
				$direct_bill = $locationData['direct_bill'];
				$bill_printer_type = $locationData['bill_printer_type'];
				$bill_printer_name = $locationData['bill_printer_name'];
			} else{
				$kot_different = 0;
				$kot_copy = 1;
				$direct_bill = 0;
				$bill_printer_type = '';
				$bill_printer_name = '';
			}

			foreach ($ans as $resultt) {
				$infoss[] = array(
					'order_id'   => $resultt['order_id'],
					'time_added'  => $resultt['time_added'],
					'date_added'  => $resultt['date_added'],
					'location'  => $resultt['location'],
					'location_id' => $resultt['location_id'],
					't_name'    => $resultt['t_name'],
					'table_id'  => $resultt['table_id'],
					'waiter'    => $resultt['waiter'],
					'waiter_id'    => $resultt['waiter_id'],
					'captain'   => $resultt['captain'],
					'captain_id'   => $resultt['captain_id'],
					'item_quantity'   => $resultt['item_quantity'],
					'total_items'   => $resultt['total_items'],
					'login_id' => $resultt['login_id'],
					'login_name' => $resultt['login_name'],
					'person' => $resultt['person'],
					'ftotal' => $resultt['ftotal'],
					'ltotal' => $resultt['ltotal'],
					'grand_total' => $resultt['grand_total'],
					'bill_status' => $resultt['bill_status'],
					'cust_name' => $resultt['cust_name'],
					'cust_contact' => $resultt['cust_contact'],
					'cust_address' => $resultt['cust_address'],
				);
				$this->log->write("KOT COMPLETE DETAILS");
				$this->log->write($resultt);
			}

			foreach ($anss as $lkey => $result) {
				foreach($anss as $lkeys => $resultss){
					if($lkey == $lkeys) {

					} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['code'] = '';
							}
						}
					} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['qty'] = $result['qty'] + $resultss['qty'];
								if($result['nc_kot_status'] == '0'){
									$result['amt'] = $result['qty'] * $result['rate'];
								}
							}
						}
					}
				}

				if($result['code'] != ''){
					if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
						$kot_group_datas[$result['subcategoryid']]['code'] = 1;
					}

					$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
					if ($decimal_mesurement == 0) {
							$qty = (int)$result['qty'];
					} else {
							$qty = $result['qty'];
					}

					
					$infos_normal[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
						'id'				=> $result['id'],
						'name'           	=> $result['name'],
						'qty'         		=> $qty,
						'code'         		=> $result['code'],
						'amt'				=> $result['amt'],
						'rate'				=> $result['rate'],
						'message'         	=> $result['message'],
						'subcategoryid'		=> $result['subcategoryid'],
						'kot_no'            => $result['kot_no'],
						'time_added'        => $result['time'],
					);

					// echo'<pre>';
					// print_r($result);
					// exit;
				}
			}
			// echo "<pre>";print_r($infoss);
			// echo "<pre>";print_r($anss);
			// echo "<pre>";print_r($infos_normal);

			$LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');
			// echo "<pre>";print_r($LOCAL_PRINT);

			if($LOCAL_PRINT == 0  || $kot_copy > 0){
				if($kot_copy > 0 && $LOCAL_PRINT == 0){

					if($infos_normal){
						// echo'<pre>';
						// print_r($infos_normal);
						// exit;
						foreach($infos_normal as $nkeys => $nvalues){
							// echo '<pre>';
							// print_r($nvalues);
							// echo '<br />';
							// echo '<pre>';
							// print_r($nvalues[0]['time_added']);
							// echo '<br />';
							// echo '<pre>';
							// print_r(date('H:i:s', strtotime($nvalues[0]['time_added'])));
							// echo '<br />';
							// exit;
							// echo'<pre>';
							// print_r($infos_normal);
							// exit;
						 	$printtype = '';
						 	$printername = '';
						 	$description = '';
						 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];
						 
					 		if (isset($kot_group_datas[$sub_category_id_compare]['printer_type'])) {
								$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
					 		}
							// echo "<pre>";print_r($printtype);exit;

							if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
								$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
							}
							// }

							if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
								$description = $kot_group_datas[$sub_category_id_compare]['description'];
							}

							$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
							if($printerModel ==0){

								try {
							 		if($printtype == 'Network'){
								 		$connector = new NetworkPrintConnector($printername, 9100);
								 	} elseif($printtype == 'Windows'){
								 		$connector = new WindowsPrintConnector($printername);
								 	} else {
								 		$connector = '';
								 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
								 	}
								 	if($connector != ''){
								 		$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
								 		if($printerModel == 0){
									 		if($kot_different == 0){
										    	//echo"innnn kot";exit;
											    $printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 1);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											   	$printer->beep(8,3);
											    for($i = 1; $i <= $kot_copy; $i++){
											    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
													   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
										    			$printer->feed(1);
										    		}

										    		
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($description);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
													$printer->setTextSize(1, 1);
													if($this->model_catalog_order->get_settings('CUST_DETAILS') == 1){
														$printer->text(("Name : ".$infoss[0]['cust_name']));
														
													    $printer->feed(1);
													    $printer->text(("Mobile : ".$infoss[0]['cust_contact']));
													    $printer->feed(1);
												    }

												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $kot_no_string = '';
												    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){

												    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
												    	$printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
														$total_amount_normal = 0;
												    	foreach($nvalues as $keyss => $nvalue){
													    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
													    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
													    		
													    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
													    	if($nvalue['message'] != ''){
													    		$printer->setTextSize(2, 2);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
													    	if($modifierdata != array()){
														    	foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $nvalue['id']){
													    				foreach($value as $modata){
																    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																	    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																	    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																    		$printer->feed(1);
															    		}
															    	}
													    		}
													    	}
													    	$total_items_normal ++ ;
											    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
											    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    			$kot_no_string .= $nvalue['kot_no'].",";
													    }
											    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
													    $printer->setTextSize(1, 1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
												    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
											    		$printer->feed(1);
											    		$printer->setTextSize(2, 1);
											    		$printer->text("G.Total :".$total_g );
											    		$printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
												    } else {
														$printer->text("Qty     Description");
												    	$printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
													    foreach($nvalues as $keyss => $valuess){
													    	$printer->setTextSize(2, 1);
													    	//$printer->setTextSize(2, 2);
												    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],46,"\n"));
													    	if($valuess['message'] != ''){
													    		$printer->setTextSize(2, 1);
													    		//$printer->setTextSize(2, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_normal ++ ;
													    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
													    	$kot_no_string .= $valuess['kot_no'].",";
												    	}

												    	$printer->setTextSize(1, 1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
												}
											} else{ 
												$printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 1);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											   	for($i = 1; $i <= $kot_copy; $i++){
											   		//echo'innn2';exit;
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($description);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setTextSize(1, 1);
												    if($this->model_catalog_order->get_settings('CUST_DETAILS') == 1){
													    $printer->text(("Name : ".$infoss[0]['cust_name']));
													    $printer->feed(1);
													    $printer->text(("Mobile : ".$infoss[0]['cust_contact']));
													    $printer->feed(1);
													}

												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $kot_no_string = '';
												    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
													   	$printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
												    	$total_amount_normal = 0;
												    	foreach($nvalues as $keyss => $nvalue){
													    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
													    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
													    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
													    	if($nvalue['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
													    	if($modifierdata != array()){
														    	foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $nvalue['id']){
													    				foreach($value as $modata){
																    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																	    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																	    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																    		$printer->feed(1);
															    		}
															    	}
													    		}
													    	}
													    	$total_items_normal ++ ;
											    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
											    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    			$kot_no_string .= $nvalue['kot_no'].",";
													    }
												    	$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];

													    $printer->setTextSize(1, 1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
												    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
											    		$printer->feed(1);
											    		$printer->setTextSize(2, 1);
											    		$printer->text("G.Total :".$total_g );
											    		$printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
												    } else {
													    $printer->text("Qty     Description");
													    $printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
													    foreach($nvalues as $keyss => $valuess){
													    	//echo'<pre>';print_r($valuess);exit;
													    	$printer->setTextSize(2, 1);
													    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],46,"\n"));
													    	if($valuess['message'] != ''){
													    		//$printer->setTextSize(1, 1);
													    		$printer->setTextSize(2, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_normal ++ ;
													    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
													    	$kot_no_string .= $valuess['kot_no'].",";
												    	}
														$printer->setTextSize(1, 1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
													foreach($nvalues as $keyss => $valuess){
														$valuess['qty'] = round($valuess['qty']);
														for($i = 1; $i <= $valuess['qty']; $i++){
															$total_items_normal = 0;
															$total_quantity_normal = 0;
															$qtydisplay = 1;
															//echo $valuess['qty'];
															$printer = new Printer($connector);
														    $printer->selectPrintMode(32);
														   	$printer->setEmphasis(true);
														   	$printer->setTextSize(2, 1);
														   	$printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->text($infoss[0]['location']);
														    $printer->feed(1);
														    $printer->text($description);
														    $printer->feed(1);
														    $printer->setTextSize(2, 1);
														   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
														    $printer->text("Tbl.No ".$table_id);
														    $printer->feed(1);
														    $printer->setJustification(Printer::JUSTIFY_LEFT);
														    $printer->setTextSize(1, 1);
														    if($this->model_catalog_order->get_settings('CUST_DETAILS') == 1){
														    $printer->text(("Name : ".$infoss[0]['cust_name']));
														    $printer->feed(1);
														    $printer->text(("Mobile : ".$infoss[0]['cust_contact']));
														    $printer->feed(1);
															}
														    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
															$printer->feed(1);
														    $printer->setEmphasis(true);
														   	$printer->setTextSize(1, 1);
														    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														   	$printer->setTextSize(1, 1);
														   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
														    $printer->feed(1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("Qty     Description");
														    $printer->feed(1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
													    	$printer->setTextSize(2, 1);
												    	  	$printer->text($qtydisplay." ".wordwrap($valuess['name']."-".$i,36,"\n"));
													    	if($valuess['message'] != ''){
													    		$printer->feed(1);
													    		$printer->setTextSize(2, 1);
													    		$printer->text("(".wordwrap($valuess['message'],10,"<br>\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
														    $printer->setTextSize(2, 1);
													    	$total_items_normal ++ ;
													    	$total_quantity_normal ++;
													    	$qtydisplay ++;
													    	$printer->setTextSize(1, 1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
														    $printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
														}
													}
												}
											}
										} else { 
											
										}
										// Close printer //
									    $printer->close();
									    $kot_no_string = rtrim($kot_no_string, ',');
										$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
									}
								} catch (Exception $e) {
								    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
								    if(isset($kot_no_string)){
									    $kot_no_string = rtrim($kot_no_string, ',');
									    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
									} else {
										$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
									}
								    $this->session->data['warning'] = $printername." "."Not Working";
									//continue;
								}
								
							} else {  // 45 space code starts from here

								try {
							 		if($printtype == 'Network'){
								 		$connector = new NetworkPrintConnector($printername, 9100);
								 	} elseif($printtype == 'Windows'){
								 		$connector = new WindowsPrintConnector($printername);
								 	} else {
								 		$connector = '';
								 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
								 	}
								 	if($connector != ''){
									 		if($kot_different == 0){
										    	// echo"innnn kot";exit;
											    $printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 1);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											   	// $printer->text('45 Spacess');
											   	$printer->beep(8,3);
						    					$printer->feed(1);
											    for($i = 1; $i <= $kot_copy; $i++){
											    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
													   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
										    			$printer->feed(1);
										    		}
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($description);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
													$printer->setTextSize(1, 1);
													
													$printer->text(("Name : ".$infoss[0]['cust_name']));
													//$printer->text(("444444 : ".$infoss[0]['cust_name']));
												    $printer->feed(1);
												    $printer->text(("Mobile : ".$infoss[0]['cust_contact']));
												    $printer->feed(1);
												    $printer->text(("Address : ".$infoss[0]['cust_address']));
												    $printer->feed(1);

												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],12).str_pad("Persons :".$infoss[0]['person'],12)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $kot_no_string = '';
												    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){

												    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
												    	$printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
														$total_amount_normal = 0;
												    	foreach($nvalues as $keyss => $nvalue){
													    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
													    		
													    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
													    	if($nvalue['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($nvalue['message'],20,"\n").")");
													    	}
													    	$printer->feed(1);
													    	if($modifierdata != array()){
														    	foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $nvalue['id']){
													    				foreach($value as $modata){
																    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																	    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																	    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																    		$printer->feed(1);
															    		}
															    	}
													    		}
													    	}
													    	$total_items_normal ++ ;
											    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
											    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    			$kot_no_string .= $nvalue['kot_no'].",";
													    }
											    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
												    
													    $printer->setTextSize(1, 1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
												    	$printer->text("T.I. : ".str_pad($total_items_normal,5)."T.Q. :".str_pad($total_quantity_normal,5)."F.T. :".$total_amount_normal);
											    		$printer->feed(1);
											    		$printer->setTextSize(2, 1);
											    		$printer->text("G.Total :".$total_g );
											    		$printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
												    } else {
														$printer->text("Qty     Description");
												    	$printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
													    foreach($nvalues as $keyss => $valuess){
													    	$printer->setTextSize(2, 1);

												    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],46,"\n"));
													    	if($valuess['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],30,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			//$printer->setTextSize(1, 1);
												    			$printer->setTextSize(2, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],25,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_normal ++ ;
													    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
													    	$kot_no_string .= $valuess['kot_no'].",";
												    	}

												    	$printer->setTextSize(1, 1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
												}
											} else{ 
												$printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 1);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											   	for($i = 1; $i <= $kot_copy; $i++){
											   		//echo'innn2';exit;
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($description);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setTextSize(1, 1);

												    $printer->text(("Name : ".$infoss[0]['cust_name']));
												    //$printer->text(("555555555 : ".$infoss[0]['cust_name']));
												    $printer->feed(1);
												    $printer->text(("Mobile : ".$infoss[0]['cust_contact']));
												    $printer->feed(1);
												    $printer->text(("Mobile : ".$infoss[0]['cust_address']));
												    $printer->feed(1);

												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],10).str_pad("Persons :".$infoss[0]['person'],10)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $kot_no_string = '';
												    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
													   	$printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
												    	$total_amount_normal = 0;
												    	foreach($nvalues as $keyss => $nvalue){
													    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
													    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
													    	if($nvalue['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
													    	if($modifierdata != array()){
														    	foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $nvalue['id']){
													    				foreach($value as $modata){
																    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																	    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																	    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																    		$printer->feed(1);
															    		}
															    	}
													    		}
													    	}
													    	$total_items_normal ++ ;
											    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
											    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    			$kot_no_string .= $nvalue['kot_no'].",";
													    }
												    	$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];

													    $printer->setTextSize(1, 1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
												    	$printer->text("T.I. : ".str_pad($total_items_normal,5)."T.Q. :".str_pad($total_quantity_normal,5)."F.T. :".$total_amount_normal);
											    		$printer->feed(1);
											    		$printer->setTextSize(2, 1);
											    		$printer->text("G.Total :".$total_g );
											    		$printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
												    } else {
													    $printer->text("Qty     Description");
													    $printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
													    foreach($nvalues as $keyss => $valuess){
													    	//echo'<pre>';print_r($valuess);exit;
													    	$printer->setTextSize(2, 1);
													    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],46,"\n"));
													    	if($valuess['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],20,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			//$printer->setTextSize(1, 1);
												    			$printer->setTextSize(2, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],20,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_normal ++ ;
													    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
													    	$kot_no_string .= $valuess['kot_no'].",";
												    	}
														$printer->setTextSize(1, 1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T. Q. :  ".$total_quantity_normal."     T. I. :  ".$total_items_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
													foreach($nvalues as $keyss => $valuess){
														$valuess['qty'] = round($valuess['qty']);
														for($i = 1; $i <= $valuess['qty']; $i++){
															$total_items_normal = 0;
															$total_quantity_normal = 0;
															$qtydisplay = 1;
															//echo $valuess['qty'];
															$printer = new Printer($connector);
														    $printer->selectPrintMode(32);
														   	$printer->setEmphasis(true);
														   	$printer->setTextSize(2, 1);
														   	$printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->text($infoss[0]['location']);
														    $printer->feed(1);
														    $printer->text($description);
														    $printer->feed(1);
														    $printer->setTextSize(2, 1);
														   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
														    $printer->text("Tbl.No ".$table_id);
														    $printer->feed(1);
														    $printer->setJustification(Printer::JUSTIFY_LEFT);
														    $printer->setTextSize(1, 1);

														    $printer->text(("Name : ".$infoss[0]['cust_name']));
														    //$printer->text(("000000000 : ".$infoss[0]['cust_name']));
														    $printer->feed(1);
														    $printer->text(("Mobile : ".$infoss[0]['cust_contact']));
														    $printer->feed(1);
														    $printer->text(("Mobile : ".$infoss[0]['cust_address']));
														    $printer->feed(1);

														    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
															$printer->feed(1);
														    $printer->setEmphasis(true);
														   	$printer->setTextSize(1, 1);
														   $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														   	$printer->setTextSize(1, 1);
														   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
														    $printer->feed(1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("Qty     Description");
														    $printer->feed(1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
													    	$printer->setTextSize(2, 2);
												    	  	$printer->text($qtydisplay." ".wordwrap($valuess['name']."-".$i,46,"\n"));
													    	if($valuess['message'] != ''){
													    		$printer->feed(1);
													    		$printer->setTextSize(2, 1);
													    		$printer->text("(".wordwrap($valuess['message'],10,"<br>\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
														    $printer->setTextSize(2, 1);
													    	$total_items_normal ++ ;
													    	$total_quantity_normal ++;
													    	$qtydisplay ++;
													    	$printer->setTextSize(1, 1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
														    $printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
														}
													}
												}
											}
										// Close printer //
									    $printer->close();
									    $kot_no_string = rtrim($kot_no_string, ',');
										$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
									}
								} catch (Exception $e) {
								    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
								    if(isset($kot_no_string)){
									    $kot_no_string = rtrim($kot_no_string, ',');
									    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
									} else {
										$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
									}
								    $this->session->data['warning'] = $printername." "."Not Working";
									//continue;
								}
							}
							$json = array(
								'status' => 1,

							);
							$this->response->addHeader('Content-Type: application/json');
							$this->response->setOutput(json_encode($json));
						}
					}

				}
			}

			// Direct Bill Function Start 
			if(($infoss[0]['bill_status'] == 0 && $edit == '0')){
				if($LOCAL_PRINT == 0 && $direct_bill == 1){
					$online_order_id = $this->db->query("SELECT online_order_id FROM oc_order_info WHERE order_id ='".$order_id."' ")->row;
					$merge_datas = array();
					$ansb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
					// echo"<pre>";print_r($ansb);exit;
					if($edit == '0'){
						$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
						$last_open_dates = $this->db->query($last_open_date_sql);
						if($last_open_dates->num_rows > 0){
							$last_open_date = $last_open_dates->row['bill_date'];
						} else {
							$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
							$last_open_dates = $this->db->query($last_open_date_sql);
							if($last_open_dates->num_rows > 0){
								$last_open_date = $last_open_dates->row['bill_date'];
							} else {
								$last_open_date = date('Y-m-d');
							}
						}

						$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
						$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
						if($last_open_dates_liq->num_rows > 0){
							$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
						} else {
							$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
							$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
							if($last_open_dates_liq->num_rows > 0){
								$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
							} else {
								$last_open_date_liq = date('Y-m-d');
							}
						}

						$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
						$last_open_dates_order = $this->db->query($last_open_date_sql_order);
						if($last_open_dates_order->num_rows > 0){
							$last_open_date_order = $last_open_dates_order->row['bill_date'];
						} else {
							$last_open_date_order = date('Y-m-d');
						}
						
						$ordernogenrated = $this->db->query("SELECT order_no FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
						
						if($ordernogenrated['order_no'] == '0'){
							$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
							$orderno = 1;
							if(isset($orderno_q['order_no'])){
								$orderno = $orderno_q['order_no'] + 1;
							}
							
							$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
							if($kotno2->num_rows > 0){
								$kot_no2 = $kotno2->row['billno'];
								$kotno = $kot_no2 + 1;
							} else{
								$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
								if($kotno2->num_rows > 0){
									$kot_no2 = $kotno2->row['billno'];
									$kotno = $kot_no2 + 1;
								} else {
									$kotno = 1;
								}
							}

							$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
							if($kotno1->num_rows > 0){
								$kot_no1 = $kotno1->row['billno'];
								$botno = $kot_no1 + 1;
							} else{
								$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
								if($kotno1->num_rows > 0){
									$kot_no1 = $kotno1->row['billno'];
									$botno = $kot_no1 + 1;
								}else {
									$botno = 1;
								}
							}

							$this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0'");
							$this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0'");

							// echo'<pre>';
							// print_r($this->session->data);
							// exit;
							if(isset($this->session->data['cash'])){
								$cashh = $this->session->data['cash'];
							} else{
								$cashh = '';
							}

							if(isset($this->session->data['credit'])){
								$creditt = $this->session->data['credit'];
							} else{
								$creditt = '';
							}

							if(isset($this->session->data['online'])){
								$onlinee = $this->session->data['online'];
							} else{
								$onlinee = '';
							}

							if(isset($this->session->data['onac'])){
								$onacc = $this->session->data['onac'];
							} else{
								$onacc = '';
							}

							// echo $cashh;
							// echo'<br />';
							// echo $creditt;
							// exit;
							if(isset($this->session->data['payment_type'])){
								$payment_type = $this->session->data['payment_type'];
								if($payment_type==0){
									$payment_type = '';
								}
							} else{
								$payment_type = '';
							}

							if(($onacc != '0' && $onacc != '') && ($cashh != '0' && $cashh != '')){
								$tot_pay = $onacc + $cashh;
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."', pay_cash = '".$cashh."', onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

							}elseif(($onacc != '0' && $onacc != '') && ($creditt != '0' && $creditt != '')){
								$tot_pay = $onacc + $creditt;
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."', pay_card = '".$creditt."', onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

							}elseif(($onacc != '0' && $onacc != '') && ($onlinee != '0' && $onlinee != '')){
								$tot_pay = $onacc + $onlinee;
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."', pay_online = '".$onlinee."', onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
							
							}elseif(($cashh != '0' && $cashh != '') && ($creditt != '0' && $creditt != '')){
								$tot_pay = $cashh + $creditt;
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$cashh."',pay_card = '".$creditt."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

							
							}elseif(($cashh != '0' && $cashh != '') && ($onlinee != '0' && $onlinee != '')){
								$tot_pay = $cashh + $onlinee;
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$cashh."' , pay_online = '".$onlinee."',payment_type='".$payment_type."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";


							}elseif(($creditt != '0' && $creditt != '') && ($onlinee != '0' && $onlinee != '')){
								$tot_pay =  $creditt + $onlinee;
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_card = '".$creditt."',pay_online = '".$onlinee."',payment_type='".$payment_type."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";


							}elseif($cashh != '0' && $cashh != ''){

								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$cashh."', payment_status = '1', total_payment = '".$cashh."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
								// unset($this->session->data['cash']);
								// unset($this->session->data['credit']);
								// unset($this->session->data['online']);
								// unset($this->session->data['onac']);
								// unset($this->session->data['onaccust']);
								// unset($this->session->data['onaccontact']);
								// unset($this->session->data['onacname']);

							}elseif( $creditt != '0' && $creditt != ''){
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_card = '".$creditt."', payment_status = '1', total_payment = '".$creditt."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
								// unset($this->session->data['cash']);
								// unset($this->session->data['credit']);
								// unset($this->session->data['online']);
								// unset($this->session->data['onac']);
								// unset($this->session->data['onaccust']);
								// unset($this->session->data['onaccontact']);
								// unset($this->session->data['onacname']);
							}elseif( $onlinee != '0' && $onlinee != ''){
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_online = '".$onlinee."',payment_type='".$payment_type."', payment_status = '1', total_payment = '".$onlinee."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
								// unset($this->session->data['cash']);
								// unset($this->session->data['credit']);
								// unset($this->session->data['online']);
								// unset($this->session->data['onac']);
								// unset($this->session->data['onaccust']);
								// unset($this->session->data['onaccontact']);
								// unset($this->session->data['onacname']);
							}elseif( $onacc != '0' && $onacc != ''){
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."',onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$this->session->data['onac']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
								// unset($this->session->data['cash']);
								// unset($this->session->data['credit']);
								// unset($this->session->data['online']);
								// unset($this->session->data['onac']);
								// unset($this->session->data['onaccust']);
								// unset($this->session->data['onaccontact']);
								// unset($this->session->data['onacname']);
							

							} elseif($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ansb['grand_total']."', payment_status = '1', total_payment = '".$ansb['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
							} else{
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
							}
							$ordernogenrated['order_no'] = $orderno;
							$this->db->query($update_sql);
							$apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
							if($apporder['app_order_id'] != '0'){
								$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
							}
						} 
						// echo"in";
						// echo"<pre>";print_r($ordernogenrated);
						// echo"<pre>";print_r($ordernogenrated);
						//echo"<pre>";print_r($ordernogenrated);
						
						// echo"<pre>";print_r($merge_datas);exit;
					}

					$anssb = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
					$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
					foreach($tests as $test){
						$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
						$testfoods[] = array(
							'tax1' => $test['tax1'],
							'amt' => $amt,
							'tax1_value' => $test['tax1_value']
						);
					}
					
					$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
					foreach($testss as $testa){
						$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
						$testliqs[] = array(
							'tax1' => $testa['tax1'],
							'amt' => $amts,
							'tax1_value' => $testa['tax1_value']
						);
					}
					$infoslb = array();
					$infosb = array();
					$flag = 0;
					$totalquantityfood = 0;
					$totalquantityliq = 0;
					$disamtfood = 0;
					$disamtliq = 0;
					$modifierdatabill = array();
					foreach ($anssb as $lkey => $result) {
						foreach($anssb as $lkeys => $results){
							if($lkey == $lkeys) {

							} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
								if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
									if($result['parent'] == '0'){
										$result['code'] = '';
									}
								}
							} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
								if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
									if($result['parent'] == '0'){
										$result['qty'] = $result['qty'] + $results['qty'];
										if($result['nc_kot_status'] == '0'){
											$result['amt'] = $result['qty'] * $result['rate'];
										}
									}
								}
							}
						}
						if($result['code'] != ''){
							$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
								if ($decimal_mesurement == 0) {
										$qty = (int)$result['qty'];
								} else {
										$qty = $result['qty'];
								}
							if($result['is_liq']== 0){
								$infosb[] = array(
									'billno'		=> $result['billno'],
									'id'			=> $result['id'],
									'name'          => $result['name'],
									'rate'          => $result['rate'],
									'amt'           => $result['amt'],
									'qty'         	=> $qty,
									'tax1'         	=> $result['tax1'],
									'tax2'          => $result['tax2']
								);
								$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
								$totalquantityfood = $totalquantityfood + $result['qty'];
								$disamtfood = $disamtfood + $result['discount_value'];
							} else {
								$flag = 1;
								$infoslb[] = array(
									'billno'		=> $result['billno'],
									'id'			=> $result['id'],
									'name'          => $result['name'],
									'rate'          => $result['rate'],
									'amt'           => $result['amt'],
									'qty'         	=> $qty,
									'tax1'         	=> $result['tax1'],
									'tax2'          => $result['tax2']
								);
								$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
								$totalquantityliq = $totalquantityliq + $result['qty'];
								$disamtliq = $disamtliq + $result['discount_value'];
							}
						}
					}
					
					if($ansb['parcel_status'] == '0'){
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$gtotal = ($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
						} else{
							$gtotal = ($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
						}
					} else {
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$gtotal =($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
						} else{
							$gtotal =($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
						}
					}

					$ordernoo = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;

					//$onac = $this->session->data['onac'];

					// echo'<pre>';
					// print_r($this->session->data);
					// exit;
				

					$csgst=$ansb['gst'] / 2;
					$csgsttotal = $ansb['gst'];

					$ansb['cust_name'] = utf8_substr(html_entity_decode($ansb['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 30);
					$ansb['cust_address'] = utf8_substr(html_entity_decode($ansb['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 60);
					$ansb['location'] = utf8_substr(html_entity_decode($ansb['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
					$ansb['waiter'] = utf8_substr(html_entity_decode($ansb['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
					$ansb['ftotal'] = utf8_substr(html_entity_decode($ansb['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
					$ansb['ltotal'] = utf8_substr(html_entity_decode($ansb['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
					$ansb['cust_contact'] = utf8_substr(html_entity_decode($ansb['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
					$ansb['t_name'] = utf8_substr(html_entity_decode($ansb['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
					$ansb['captain'] = utf8_substr(html_entity_decode($ansb['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
					$ansb['vat'] = utf8_substr(html_entity_decode($ansb['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
					$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
					if($ansb['advance_amount'] == '0.00'){
						$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
					} else{
						$gtotal = utf8_substr(html_entity_decode($ansb['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
					}
					//$gtotal = ceil($gtotal);
					$gtotal = round($gtotal);


					if((($infos_cancel == array() && $infos_liqurcancel == array()) || $edit == '1') && $bill_copy > 0){
						$locationData =  $this->db->query("SELECT `parcel_detail` FROM oc_location WHERE location_id = '".$ansb['location_id']."'");
						if($locationData->num_rows > 0){
							$parcel_detail = $locationData->row['parcel_detail'];
						} else {
							$parcel_detail = 0;
						}


						$printtype = $bill_printer_type;
			 			$printername = $bill_printer_name;
						if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
							$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
						 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
						}
					
						$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
						if($printerModel ==0){
									$this->log->write("Innnnn Bill Function ");


							try {
						    	if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 	}
							    if($connector != ''){
								    $printer = new Printer($connector);
								    $printer->selectPrintMode(32);

								   	$printer->setEmphasis(true);
								   	$printer->setTextSize(2, 1);
								   	$printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->feed(1);
								   	//$printer->setFont(Printer::FONT_B);
									for($i = 1; $i <= $bill_copy; $i++){
									    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
									    $printer->feed(1);
									    $printer->setTextSize(1, 1);
									    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
									    $printer->setJustification(Printer::JUSTIFY_CENTER);
									    $printer->setTextSize(2, 1);
									    $printer->text("Ref no: ".$ordernoo['order_no']."");
									    //$printer->setJustification(Printer::JUSTIFY_CENTER);
									    //$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	$printer->feed(1);
									    if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
											
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
										 	$printer->text(("Name : ".$ansb['cust_name']));
											$printer->feed(1);
											$printer->text(("Mobile :".$ansb['cust_contact']));
											$printer->feed(1);
										}
										else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
											$printer->feed(1);
										 	$printer->text(("Address : ".$ansb['cust_address']));
											$printer->feed(1);
											$printer->text("Gst No :".$ansb['gst_no']);
											$printer->feed(1);
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
											$printer->feed(1);
										 	$printer->text(("Name : ".$ansb['cust_name']));
										    $printer->feed(1);
										    $printer->text("Address : ".$ansb['cust_address']);
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
											$printer->feed(1);
										 	$printer->text(("Mobile :".$ansb['cust_contact']));
										    $printer->feed(1);
										    $printer->text("Gst No :".$ansb['gst_no']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
											$printer->feed(1);
										 	$printer->text("Name : ".$ansb['cust_name']);
										    $printer->feed(1);
										    $printer->text("Gst No :".$ansb['gst_no']);
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
											$printer->feed(1);
										 	$printer->text("Mobile :".$ansb['cust_contact']);
										    $printer->feed(1);
										    $printer->text("Address : ".$ansb['cust_address']);
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
											$printer->feed(1);
										    $printer->text("Name :".$ansb['cust_name']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
											$printer->feed(1);
										    $printer->text("Mobile :".$ansb['cust_contact']."");
										    $printer->feed(1);
										}else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
											$printer->feed(1);
										    $printer->text("Address : ".$ansb['cust_address']."");
										    $printer->feed(1);
										}else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
											$printer->feed(1);
										    $printer->text("Gst No :".$ansb['gst_no']."");
										    $printer->feed(1);
										}
										else{
										    $printer->text("Name : ".$ansb['cust_name']);
										    $printer->feed(1);
										    $printer->text("Mobile :".$ansb['cust_contact']."");
										    $printer->feed(1);
										    $printer->text("Address : ".$ansb['cust_address']);
										    $printer->feed(1);
										    $printer->text("Gst No :".$ansb['gst_no']);
										    $printer->feed(1);
										}
										$printer->text(str_pad("User Id :".$ansb['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
										$printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($infosb){
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),11)."Bill no: ".str_pad($infosb[0]['billno'],6)."Time:".date('h:i:s'));
									   		$printer->feed(1);
									   		$printer->text("Ref no: ".$ansb['order_no']."");
									   		$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ansb['location'],8)." ".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
											$printer->feed(1);
									   	 	$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infosb as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
										    	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
													    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
													    		$printer->text(str_pad(".".$modata['name'],29)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}
										    	$total_items_normal ++ ;
							    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
							    			}
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty:".str_pad($total_quantity_normal,7)."F.Total :".$ansb['ftotal']);
											$printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											foreach($testfoods as $tkey => $tvalue){
												$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										    	$printer->feed(1);
											}
											$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ansb['fdiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['fdiscountper']."%) :".$ansb['ftotalvalue']."");
												$printer->feed(1);
											} elseif($ansb['discount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['ftotalvalue']."rs):".$ansb['ftotalvalue']."");
												$printer->feed(1);
											}
											if($this->model_catalog_order->get_settings('GST_SHOW') == 1){
											$printer->text(str_pad("",20)."CCGST(2.5%) :".$csgst."");
											$printer->feed(1);
											$printer->text(str_pad("",20)."CCGST(2.5%) :".$csgst."");
											$printer->feed(1);
											}
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
													 	$printer->text(str_pad("",30)."SCRG :".$ansb['staxfood']."");
														$printer->feed(1);
													}
													$netamountfood = (($ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
												} else{
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
														$printer->text(str_pad("",30)."SCRG :".$ansb['staxfood']."");
														$printer->feed(1);
													}
													$netamountfood = (($csgsttotal + $ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountfood = ($ansb['ftotal'] - ($disamtfood));
												} else{
													$netamountfood = ($csgsttotal + $ansb['ftotal'] - ($disamtfood));
												}
											}
											$printer->setEmphasis(true);
											$printer->text(str_pad("",29)."Net total :".ceil($netamountfood)."");
											$printer->setEmphasis(false);
										}
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->feed(1);	
											$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
											if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
												$printer->feed(1);				   		
										   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
									   		}
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   	}
									   	if($infoslb){
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
									   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infoslb[0]['billno'],5)."Time :".date('h:i:s'));
									   		$printer->feed(1);
									   		$printer->text("Ref no: ".$ansb['order_no']."");
									 		$printer->feed(1);
									 		$printer->setEmphasis(true);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	$printer->text(str_pad("Loc",8)."".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ansb['location'],8)."".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",7)."".str_pad("Amt",8));
											$printer->feed(1);
									    	$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infoslb as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
										    	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
													    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}
										    	$total_items_normal ++ ;
							    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
										    }
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T QTY :".str_pad($total_quantity_normal,7)."L.Total :".$ansb['ltotal']);
											$printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											// foreach($testliqs as $tkey => $tvalue){
											// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										 //    	$printer->feed(1);
											// }
											// $printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ansb['ldiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['ldiscountper']."%) :".$ansb['ltotalvalue']."");
												$printer->feed(1);
											} elseif($ansb['ldiscount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['ltotalvalue']."rs) :".$ansb['ltotalvalue']."");
												$printer->feed(1);
											}
											if($this->model_catalog_order->get_settings('VAT_SHOW') == 1){
												$printer->setTextSize(2, 1);
												$printer->text(str_pad("",32)."VAT:".$ansb['vat']."");
												$printer->feed(1);
											}
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
													 	$printer->text(str_pad("",32)."SCRG :".$ansb['staxliq']."");
														$printer->feed(1);
													}
													$netamountliq = (($ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
												} else{
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
													 	$printer->text(str_pad("",32)."SCRG :".$ansb['staxliq']."");
														$printer->feed(1);
													}
													$netamountliq = (($ansb['vat'] + $ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountliq = ($ansb['ltotal'] - ($disamtliq));
												} else{
													$netamountliq = ($ansb['vat'] + $ansb['ltotal'] - ($disamtliq));
												}
											}
											$printer->setEmphasis(true);
											$printer->text(str_pad("",25)."Net total :".ceil($netamountliq)."");
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										}
										$printer->feed(1);
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
										if($ansb['advance_amount'] != '0.00'){
											$printer->text(str_pad("Advance Amount",38).$ansb['advance_amount']."");
											$printer->feed(1);
										}
										$printer->setTextSize(2, 2);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->text(str_pad("",28)."GRAND TOTAL:".$gtotal);
										$printer->setTextSize(1, 1);
										$printer->feed(1);
										if($ansb['dtotalvalue']!=0){
											$printer->text("Delivery Charge:".$ansb['dtotalvalue']);
											$printer->feed(1);
										}
										$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
										// echo $SETTLEMENT_status;
										// exit;
										if($SETTLEMENT_status == '1'){
											if(isset($this->session->data['credit'])){
												$credit = $this->session->data['credit'];
											} else {
												$credit = '0';
											}
											if(isset($this->session->data['cash'])){
												$cash = $this->session->data['cash'];
											} else {
												$cash = '0';
											}
											if(isset($this->session->data['online'])){
												$online = $this->session->data['online'];
											} else {
												$online ='0';
											}

											if(isset($this->session->data['onac'])){
												$onac = $this->session->data['onac'];
												$onaccontact = $this->session->data['onaccontact'];
												$onacname = $this->session->data['onacname'];

											} else {
												$onac ='0';
											}
										}
										if($SETTLEMENT_status=='1'){
											if($credit!='0' && $credit!=''){
												$printer->text("PAY BY: CARD");
											}
											if($online!='0' && $online!=''){
												$printer->text("PAY BY: ONLINE");
											}
											if($cash!='0' && $cash!=''){
												$printer->text("PAY BY: CASH");
											}
											if($onac!='0' && $onac!=''){
												$printer->text("PAY BY: ON.ACCOUNT");
												$printer->feed(1);
												$printer->text("Name: ".$onacname."");
												$printer->feed(1);
												$printer->text("Contact: ".$onaccontact."");
											}
										}
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
										
										$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
										$printer->feed(1);
										if($this->model_catalog_order->get_settings('TEXT1') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT1'));
											$printer->feed(1);
										}
										if($this->model_catalog_order->get_settings('TEXT2') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT2'));
											$printer->feed(1);
										}
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										if($this->model_catalog_order->get_settings('TEXT3') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT3'));
										}
										$printer->feed(2);
										$printer->cut();
									    // Close printer //
									}
								    $printer->close();
								    if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
										$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ansb['grand_total']."', payment_status = '1', total_payment = '".$ansb['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

										$update_status ="UPDATE `oc_online_order` SET `panding_status` ='1' WHERE `order_id` = '".$online_order_id['online_order_id']."' ";
									} else{
										$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ansb['grand_total']."', payment_status = '1', total_payment = '".$ansb['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

										$update_status ="UPDATE `oc_online_order` SET `panding_status` ='1' WHERE `order_id` = '".$online_order_id['online_order_id']."' ";
									}
									$this->db->query($update_sql);
									$this->db->query($update_status);
								    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
								if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
										$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ansb['grand_total']."', payment_status = '1', total_payment = '".$ansb['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

										$update_status ="UPDATE `oc_online_order` SET `panding_status` ='1' WHERE `order_id` = '".$online_order_id['online_order_id']."' ";
									} else{
										$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ansb['grand_total']."', payment_status = '1', total_payment = '".$ansb['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

										$update_status ="UPDATE `oc_online_order` SET `panding_status` ='1' WHERE `order_id` = '".$online_order_id['online_order_id']."' ";
									}
									$this->db->query($update_sql);
									$this->db->query($update_status);
							    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
								$this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
							}
							
						}
						else{  // 45 space code starts from here


							try {
						    	if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 	}
							    if($connector != ''){
								    $printer = new Printer($connector);
								    $printer->selectPrintMode(32);

								   	$printer->setEmphasis(true);
								   	$printer->setTextSize(2, 1);
								   	$printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->feed(1);
								    // $printer->text('45 Spacess');
					    			$printer->feed(1);
								   	//$printer->setFont(Printer::FONT_B);
									for($i = 1; $i <= $bill_copy; $i++){
									    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
									    $printer->feed(1);
									    $printer->setTextSize(1, 1);
									    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
									    //$printer->setJustification(Printer::JUSTIFY_CENTER);
									    //$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	$printer->feed(1);
									    if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
											
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
											$printer->feed(1);
										 	$printer->text(("Name : ".$ansb['cust_name']));
											$printer->feed(1);
											$printer->text(("Mobile :".$ansb['cust_contact']));
											$printer->feed(1);
										}
										else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
											$printer->feed(1);
										 	$printer->text(("Address : ".$ansb['cust_address']));
											$printer->feed(1);
											$printer->text("Gst No :".$ansb['gst_no']);
											$printer->feed(1);
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
											$printer->feed(1);
										 	$printer->text(("Name : ".$ansb['cust_name']));
										    $printer->feed(1);
										    $printer->text("Address : ".$ansb['cust_address']);
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
											$printer->feed(1);
										 	$printer->text(("Mobile :".$ansb['cust_contact']));
										    $printer->feed(1);
										    $printer->text("Gst No :".$ansb['gst_no']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
											$printer->feed(1);
										 	$printer->text("Name : ".$ansb['cust_name']);
										    $printer->feed(1);
										    $printer->text("Gst No :".$ansb['gst_no']);
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
											$printer->feed(1);
										 	$printer->text("Mobile :".$ansb['cust_contact']);
										    $printer->feed(1);
										    $printer->text("Address : ".$ansb['cust_address']);
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
											$printer->feed(1);
										    $printer->text("Name :".$ansb['cust_name']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
											$printer->feed(1);
										    $printer->text("Mobile :".$ansb['cust_contact']."");
										    $printer->feed(1);
										}else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
											$printer->feed(1);
										    $printer->text("Address : ".$ansb['cust_address']."");
										    $printer->feed(1);
										}else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
											$printer->feed(1);
										    $printer->text("Gst No :".$ansb['gst_no']."");
										    $printer->feed(1);
										}else{
											$printer->feed(1);
										    $printer->text("Name : ".$ansb['cust_name']);
										    $printer->feed(1);
										    $printer->text("Mobile :".$ansb['cust_contact']."");
										    $printer->feed(1);
										    $printer->text("Address : ".$ansb['cust_address']);
										    $printer->feed(1);
										    $printer->text("Gst No :".$ansb['gst_no']);
										    $printer->feed(1);
										}
										$printer->text(str_pad("User Id :".$ansb['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
										$printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($infosb){
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
									   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infosb[0]['billno'],5)."Time:".date('H:i'));
									   		$printer->feed(1);
									   		$printer->text("Ref no: ".$ansb['order_no']."");
									   		$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ansb['location'],8)." ".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
											$printer->feed(1);
									   	 	$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infosb as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
										    	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
													    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
													    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}
										    	$total_items_normal ++ ;
							    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
							    			}
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty:".str_pad($total_quantity_normal,7)."F.Total :".$ansb['ftotal']);
											$printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											foreach($testfoods as $tkey => $tvalue){
												$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										    	$printer->feed(1);
											}
											$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ansb['fdiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['fdiscountper']."%) :".$ansb['ftotalvalue']."");
												$printer->feed(1);
											} elseif($ansb['discount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['ftotalvalue']."rs):".$ansb['ftotalvalue']."");
												$printer->feed(1);
											}
											if($this->model_catalog_order->get_settings('GST_SHOW') == 1){
											$printer->text(str_pad("",20)."CCGST(2.5%) :".$csgst."");
											$printer->feed(1);
											$printer->text(str_pad("",20)."CCGST(2.5%) :".$csgst."");
											$printer->feed(1);
											}
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
													 	$printer->text(str_pad("",25)."SCRG :".$ansb['staxfood']."");
														$printer->feed(1);
													}
													$netamountfood = (($ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
												} else{
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
													 	$printer->text(str_pad("",25)."SCRG :".$ansb['staxfood']."");
														$printer->feed(1);
													}
													$netamountfood = (($csgsttotal + $ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountfood = ($ansb['ftotal'] - ($disamtfood));
												} else{
													$netamountfood = ($csgsttotal + $ansb['ftotal'] - ($disamtfood));
												}
											}
											$printer->setEmphasis(true);
											$printer->text(str_pad("",25)."Net total :".ceil($netamountfood)."");
											$printer->setEmphasis(false);
										}
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->feed(1);	
											$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
											if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
												$printer->feed(1);				   		
										   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
									   		}
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   	}
									   	if($infoslb){
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
									   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infoslb[0]['billno'],5)."Time:".date('H:i'));
									   		$printer->feed(1);
									   		$printer->text("Ref no: ".$ansb['order_no']."");
									 		$printer->feed(1);
									 		$printer->setEmphasis(true);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	$printer->text(str_pad("Loc",8)."".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ansb['location'],8)."".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",7)."".str_pad("Amt",8));
											$printer->feed(1);
									    	$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infoslb as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
										    	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
													    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}
										    	$total_items_normal ++ ;
							    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
										    }
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T QTY :".str_pad($total_quantity_normal,7)."L.Total :".$ansb['ltotal']);
											$printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											// foreach($testliqs as $tkey => $tvalue){
											// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										 //    	$printer->feed(1);
											// }
											// $printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ansb['ldiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['ldiscountper']."%) :".$ansb['ltotalvalue']."");
												$printer->feed(1);
											} elseif($ansb['ldiscount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['ltotalvalue']."rs) :".$ansb['ltotalvalue']."");
												$printer->feed(1);
											}
											if($this->model_catalog_order->get_settings('VAT_SHOW') == 1){
												$printer->setTextSize(2, 1);
												$printer->text(str_pad("",21)."VAT :".$ansb['vat']."");
												$printer->feed(1);
											}
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
													 	$printer->text(str_pad("",21)."SCRG :".$ansb['staxliq']."");
														$printer->feed(1);
													}
													$netamountliq = (($ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
												} else{
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
													 	$printer->text(str_pad("",21)."SCRG :".$ansb['staxliq']."");
														$printer->feed(1);
													}
													$netamountliq = (($ansb['vat'] + $ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountliq = ($ansb['ltotal'] - ($disamtliq));
												} else{
													$netamountliq = ($ansb['vat'] + $ansb['ltotal'] - ($disamtliq));
												}
											}
											$printer->setEmphasis(true);
											$printer->text(str_pad("",25)."Net total :".ceil($netamountliq)."");
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										}
										$printer->feed(1);
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
										if($ansb['advance_amount'] != '0.00'){
											$printer->text(str_pad("Advance Amount",38).$ansb['advance_amount']."");
											$printer->feed(1);
										}
										$printer->setTextSize(2, 2);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text("GRAND TOTAL:".$gtotal);
										$printer->setTextSize(1, 1);
										$printer->feed(1);
										if($ansb['dtotalvalue']!=0){
											$printer->text("Delivery Charge:".$ansb['dtotalvalue']);
											$printer->feed(1);
										}
										$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
										if($SETTLEMENT_status == '1'){
											if(isset($this->session->data['credit'])){
												$credit = $this->session->data['credit'];
											} else {
												$credit = '0';
											}
											if(isset($this->session->data['cash'])){
												$cash = $this->session->data['cash'];
											} else {
												$cash = '0';
											}
											if(isset($this->session->data['online'])){
												$online = $this->session->data['online'];
											} else {
												$online ='0';
											}

											if(isset($this->session->data['onac'])){
												$onac = $this->session->data['onac'];
												$onaccontact = $this->session->data['onaccontact'];
												$onacname = $this->session->data['onacname'];

											} else {
												$onac ='0';
											}
										}
										if($SETTLEMENT_status=='1'){
											if($credit!='0' && $credit!=''){
												$printer->text("PAY BY: CARD");
											}
											if($online!='0' && $online!=''){
												$printer->text("PAY BY: ONLINE");
											}
											if($cash!='0' && $cash!=''){
												$printer->text("PAY BY: CASH");
											}
											if($onac!='0' && $onac!=''){
												$printer->text("PAY BY: ON.ACCOUNT");
												$printer->feed(1);
												$printer->text("Name: '".$onacname."'");
												$printer->feed(1);
												$printer->text("Contact: '".$onaccontact."'");
											}
										}
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
										
										$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
										$printer->feed(1);
										if($this->model_catalog_order->get_settings('TEXT1') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT1'));
											$printer->feed(1);
										}
										if($this->model_catalog_order->get_settings('TEXT2') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT2'));
											$printer->feed(1);
										}
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										if($this->model_catalog_order->get_settings('TEXT3') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT3'));
										}
										$printer->feed(2);
										$printer->cut();
									    // Close printer //
									}
								    $printer->close();

								    if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
										$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ansb['grand_total']."', payment_status = '1', total_payment = '".$ansb['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

										$update_status ="UPDATE `oc_online_order` SET `panding_status` ='1' WHERE `order_id` = '".$online_order_id['online_order_id']."' ";
									} else{
										$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ansb['grand_total']."', payment_status = '1', total_payment = '".$ansb['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

										$update_status ="UPDATE `oc_online_order` SET `panding_status` ='1' WHERE `order_id` = '".$online_order_id['online_order_id']."' ";
									}
									$this->db->query($update_sql);
									$this->db->query($update_status);

								    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
								if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
									$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ansb['grand_total']."', payment_status = '1', total_payment = '".$ansb['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

									$update_status ="UPDATE `oc_online_order` SET `panding_status` ='1' WHERE `order_id` = '".$online_order_id['online_order_id']."' ";
								} else{
									$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ansb['grand_total']."', payment_status = '1', total_payment = '".$ansb['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

									$update_status ="UPDATE `oc_online_order` SET `panding_status` ='1' WHERE `order_id` = '".$online_order_id['online_order_id']."' ";
								}
								$this->db->query($update_sql);
								$this->db->query($update_status);

							    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
								$this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
							}
						}
						if($parcel_detail == 1){
							$setting_value_link_1 = $this->model_catalog_order->get_settings('SMS_LINK_1');
							$link_1 = html_entity_decode($setting_value_link_1, ENT_QUOTES, 'UTF-8');
							$HOTEL_NAME = $this->model_catalog_order->get_settings('HOTEL_NAME');
							$hote_name=str_replace(" ", "%20", $HOTEL_NAME);
							$i =1;
							$food_items = '';
							$liq_items ='';
							foreach($infosb as $nkey => $nvalue){
								$food_items .= $i.'-'.$nvalue['name'].','.'%0a';
								$i++;
							}
							$l = $i;
							foreach($infoslb as $nakey => $navalue){
								$liq_items .= $l.'-'.$navalue['name'].','.'%0a';
								$l++;
							}

							$text =  $hote_name.'%0a'.$food_items.''.$liq_items.'%0a'.'Grand Total :- '.$gtotal;
							$msg = str_replace(" ", "%20", $text);

							$link= $link_1.$ansb['cust_contact']."&message=".$msg;
							// echo'<pre>';
							// print_r($link);
							// exit;
							if($ansb['cust_contact'] != ''){
								file($link);
							}

							// if($ret[0] != 'Daily Message Limit Reached'){
							// 	$this->session->data['success'] .= "SMS Send Successfully<br>";
							// } else {
							// 	$this->session->data['success'] .= "Sub Batch ".$i." Not Send<br>";
							// }
						}
					}
					$json = array();
					$json = array(
						'direct_bill' => $direct_bill,
						'order_id' => $order_id,
						'grand_total' => $gtotal,
						'status' => 1,
						'orderidmodify' =>$order_id,
						'edit' => $edit,
					);
					$this->response->addHeader('Content-Type: application/json');
					$this->response->setOutput(json_encode($json));
					unset($this->session->data['cash']);
					unset($this->session->data['credit']);
					unset($this->session->data['online']);
					unset($this->session->data['onac']);
					unset($this->session->data['onaccust']);
					unset($this->session->data['onaccontact']);
					unset($this->session->data['onacname']);
				} else {
							$local_print = $this->model_catalog_order->get_settings('LOCAL_PRINT');
							$KOT_RATE_AMT = $this->model_catalog_order->get_settings('KOT_RATE_AMT');
							$HOTEL_NAME = $this->model_catalog_order->get_settings('HOTEL_NAME');
							$HOTEL_ADD = $this->model_catalog_order->get_settings('HOTEL_ADD');
							$INCLUSIVE = $this->model_catalog_order->get_settings('INCLUSIVE');
							$BAR_NAME =	$this->model_catalog_order->get_settings('BAR_NAME');
							$BAR_ADD = $this->model_catalog_order->get_settings('BAR_ADD');
							$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
							$GST_NO	= $this->model_catalog_order->get_settings('GST_NO');
							$TEXT1 = $this->model_catalog_order->get_settings('TEXT1');
							$TEXT2 = $this->model_catalog_order->get_settings('TEXT2');
							$TEXT3 = $this->model_catalog_order->get_settings('TEXT3');

							if(isset($this->session->data['credit'])){
								$credit = $this->session->data['credit'];
							} else {
								$credit = '0';
							}
							if(isset($this->session->data['cash'])){
								$cash = $this->session->data['cash'];
							} else {
								$cash = '0';
							}
							if(isset($this->session->data['online'])){
								$online = $this->session->data['online'];
							} else {
								$online ='0';
							}

							if(isset($this->session->data['onac'])){
								$onac = $this->session->data['onac'];
								$onaccontact = $this->session->data['onaccontact'];
								$onacname = $this->session->data['onacname'];

							} else {
								$onac ='0';
								$onaccontact = '';
								$onacname = '';
							}

							if(isset($testliqs)){
								$testliqs = $testliqs;
							} else {
								$testliqs =array();
							}

							if(isset($testfoods)){
								$testfoods = $testfoods;
							} else {
								$testfoods =array();
							}

							if(isset($ansb)){
								$ansb = $ansb;
							} else {
								$ansb =array();
							}

							if(isset($infosb)){
								$infosb = $infosb;
							} else {
								$infosb =array();
							}

							if(isset($infoslb)){
								$infoslb = $infoslb;
							} else {
								$infoslb =array();
							}

							if(isset($merge_datas)){
								$merge_datas = $merge_datas;
							} else {
								$merge_datas =array();
							}

							if(isset($modifierdatabill)){
								$modifierdatabill = $modifierdatabill;
							} else {
								$modifierdatabill =array();
							}



							$final_datas = array();
							if($local_print == 1){
								$final_datas = array(
									'infoss' => $infoss,
									'infos_normal' => $infos_normal,
									'modifierdata' => $modifierdata,
									'allKot' => $allKot,
									'infos_liqurnormal' => $infos_liqurnormal,
									'infos_liqurcancel' => $infos_liqurcancel,
									'infos_cancel' => $infos_cancel,
									'kot_different' => $kot_different,
									'edit' => $edit,
									'ansb' => $ansb,
									'infosb' => $infosb,
									'infoslb' => $infoslb,
									'merge_datas' => $merge_datas,
									'modifierdatabill' => $modifierdatabill,
									'KOT_RATE_AMT' =>$KOT_RATE_AMT,
									'HOTEL_NAME' => $HOTEL_NAME,
									'HOTEL_ADD' =>$HOTEL_ADD,
									'INCLUSIVE' => $INCLUSIVE,
									'BAR_NAME' => $BAR_NAME,
									'BAR_ADD' => $BAR_ADD,
									'GST_NO' => $GST_NO,
									'SETTLEMENT_status' => $SETTLEMENT_status,
									'direct_bill' => $direct_bill,
									'bill_copy' =>$bill_copy,
									'localprint' => $local_print,
									'kot_copy' => $kot_copy,
									'testfoods' => $testfoods,
									'testliqs' => $testliqs,
									'cash' => $cash,
									'credit' => $credit,
									'online' => $online,
									'onac' => $onac,
									'onaccontact' => $onaccontact,
									'onacname' => $onacname,

									'TEXT1' => $TEXT1,
									'TEXT2' => $TEXT2,
									'TEXT3' => $TEXT3,


								);
							}
							
							$json = array();
							$json = array(
								'status' => 0,
								'final_datas' => $final_datas,
								'LOCAL_PRINT' => $local_print,

								
							);

							$this->response->addHeader('Content-Type: application/json');
							$this->response->setOutput(json_encode($json));	
							unset($this->session->data['cash']);
							unset($this->session->data['credit']);
							unset($this->session->data['online']);
							unset($this->session->data['onac']);
							unset($this->session->data['onaccust']);
							unset($this->session->data['onaccontact']);
							unset($this->session->data['onacname']);
						}
			} else {
				$local_print = $this->model_catalog_order->get_settings('LOCAL_PRINT');

				$json = array();
				$json = array(
					'status' => 0,
					'localprints' => $local_print,

				);
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
				//$this->session->data['warning'] = 'Bill already printed';
			}

			// Direct Bill Function End
		}
	}

	// BILL PRINT FUNCTION
	public function printsb() {
		$json = array();
		if (isset($this->request->get['online_order_id'])) {

			date_default_timezone_set('Asia/Calcutta');
			$today1 = date('Y-m-d ');

			$order_id = $this->request->get['online_order_id'];
			$ans = $this->db->query("SELECT * FROM oc_order_info WHERE online_order_id = '".$order_id."'")->row;
			if(($ans['bill_status'] == 0 && $ans['bill_status'] == '0')){
				// echo "<pre>";print_r($ans['order_id']);exit;
				date_default_timezone_set('Asia/Kolkata');
				$this->load->language('customer/customer');
				$this->load->model('catalog/order');
				$merge_datas = array();
				$this->document->setTitle('BILL');
				$te = 'BILL';
				

				$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
				$last_open_dates = $this->db->query($last_open_date_sql);
				if($last_open_dates->num_rows > 0){
					$last_open_date = $last_open_dates->row['bill_date'];
				} else {
					$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
					$last_open_dates = $this->db->query($last_open_date_sql);
					if($last_open_dates->num_rows > 0){
						$last_open_date = $last_open_dates->row['bill_date'];
					} else {
						$last_open_date = date('Y-m-d');
					}
				}

				$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

				$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

				if($last_open_dates_liq->num_rows > 0){
					$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
				} else {
					$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

					$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

					if($last_open_dates_liq->num_rows > 0){
						$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
					} else {
						$last_open_date_liq = date('Y-m-d');
					}
				}

				$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
				$last_open_dates_order = $this->db->query($last_open_date_sql_order);
				if($last_open_dates_order->num_rows > 0){
					$last_open_date_order = $last_open_dates_order->row['bill_date'];
				} else {
					$last_open_date_order = date('Y-m-d');
				}
				// echo "<pre>";print_r($ans);exit;
				if($ans['order_no'] == '0'){
					$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
					$orderno = 1;
					if(isset($orderno_q['order_no'])){
						$orderno = $orderno_q['order_no'] + 1;
					}

					$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
					if($kotno2->num_rows > 0){
						$kot_no2 = $kotno2->row['billno'];
						$kotno = $kot_no2 + 1;
					} else{
						$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
						if($kotno2->num_rows > 0){
							$kot_no2 = $kotno2->row['billno'];
							$kotno = $kot_no2 + 1;
						} else {
							$kotno = 1;
						}
					}

					$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
					if($kotno1->num_rows > 0){
						$kot_no1 = $kotno1->row['billno'];
						$botno = $kot_no1 + 1;
					} else{
						$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
						if($kotno1->num_rows > 0){
							$kot_no1 = $kotno1->row['billno'];
							$botno = $kot_no1 + 1;
						} else {
							$botno = 1;
						}
					}

					$this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$ans['order_id']."' AND cancelstatus = '0' ");
					$this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$ans['order_id']."' AND cancelstatus = '0' ");

					//$ansbb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
					if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
						$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ans['grand_total']."', payment_status = '1', total_payment = '".$ans['grand_total']."', pay_method = '1' WHERE `order_id` = '".$ans['order_id']."' ";

						$update_status ="UPDATE `oc_online_order` SET `panding_status` ='1' WHERE `order_id` = '".$ans['online_order_id']."' ";
					} else{
						$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ans['grand_total']."', payment_status = '1', total_payment = '".$ans['grand_total']."', pay_method = '1' WHERE `order_id` = '".$ans['order_id']."' ";

						$update_status ="UPDATE `oc_online_order` SET `panding_status` ='1' WHERE `order_id` = '".$ans['online_order_id']."' ";
					}
					$this->db->query($update_sql);
					$this->db->query($update_status);
				}

				$anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$ans['order_id']."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;

				$testfood = array();
				$testliq = array();
				$testtaxvalue1food = 0;
				$testtaxvalue1liq = 0;
				$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$ans['order_id']."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
				foreach($tests as $test){
					$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
					$testfoods[] = array(
						'tax1' => $test['tax1'],
						'amt' => $amt,
						'tax1_value' => $test['tax1_value']
					);
				}
				
				$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$ans['order_id']."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
				foreach($testss as $testa){
					$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
					$testliqs[] = array(
						'tax1' => $testa['tax1'],
						'amt' => $amts,
						'tax1_value' => $testa['tax1_value']
					);
				}
				
				$infosl = array();
				$infos = array();
				$flag = 0;
				$totalquantityfood = 0;
				$totalquantityliq = 0;
				$disamtfood = 0;
				$disamtliq = 0;
				$modifierdatabill = array();

				// echo'<pre>';
				// print_r($ans);
				// echo'<br>';
				foreach ($anss as $lkey => $result) {
					foreach($anss as $lkeys => $results){
						if($lkey == $lkeys) {

						} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['code'] = '';
								}
							}
						} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['qty'] = $result['qty'] + $results['qty'];
									if($result['nc_kot_status'] == '0'){
										$result['amt'] = $result['qty'] * $result['rate'];
									}
								}
							}
						}
					}
					// echo"<pre>";print_r($result);exit;
					if($result['code'] != '' ){
						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
						if ($decimal_mesurement == 0) {
								$qty = (int)$result['qty'];
						} else {
								$qty = $result['qty'];
						}
						if($result['is_liq']== 0){
							$infos[] = array(
								'id'			=> $result['id'],
								'billno'		=> $result['billno'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2'],
								'discount_value'=> $result['discount_value']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityfood = $totalquantityfood + $result['qty'];
							$disamtfood = $disamtfood + $result['discount_value'];
						} else {
							$flag = 1;
							$infosl[] = array(
								'id'			=> $result['id'],
								'billno'		=> $result['billno'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2'],
								'discount_value'=> $result['discount_value']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityliq = $totalquantityliq + $result['qty'];
							$disamtliq = $disamtliq + $result['discount_value'];
						}
					}
				}

				if($ans['parcel_status'] == '0'){
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal = ($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
					} else {
						$gtotal = ($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
					}
				} else {
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal =($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
					} else {
						$gtotal =($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
					}
				}
				// echo"in2";
				// echo "<pre>";print_r($result);
								
				$csgst=$ans['gst']/2;
				$csgsttotal = $ans['gst'];

				$ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
				//$ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['cust_address'] = html_entity_decode(wordwrap($ans['cust_address'],112,"\n"), ENT_QUOTES, 'UTF-8');

				$ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
				$ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
				$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
				// echo'<pre>';
				// print_r($ans);
				// exit;
				$this->log->write("bill details");
				$this->log->write($ans);
				$ansz = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$ans['order_id']."' ")->row;
				// echo "<pre>";print_r($ansz);exit;
				if($ansz['advance_amount'] == '0.00'){
					$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
				} else{
					$gtotal = utf8_substr(html_entity_decode($ansz['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
				}
				//$gtotal = ceil($gtotal);
				$gtotal = round($gtotal);

				$printtype = '';
				$printername = '';

				$bill_copy = 1;

				if ($printtype == '' || $printername == '' ) {
					$printtype = $this->user->getPrinterType();
					$printername = $this->user->getPrinterName();
					$locationData =  $this->db->query("SELECT `bill_copy`, `bill_printer_type`, `bill_printer_name`,`parcel_detail` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
					if($locationData->num_rows > 0){
						$locationData = $locationData->row;
						$bill_copy = $locationData['bill_copy'];
					} 
				}

				if ($printtype == '' || $printername == '' ) {
					$locationData =  $this->db->query("SELECT `bill_copy`, `bill_printer_type`, `bill_printer_name`,`parcel_detail` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
					if($locationData->num_rows > 0){
						$locationData = $locationData->row;
						$printtype = $locationData['bill_printer_type'];
						$printername = $locationData['bill_printer_name'];
						$bill_copy = $locationData['bill_copy'];
					} else{
						$printtype = '';
						$printername = '';
						$bill_copy = 1;
					}
				}
				if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
					$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
				 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
				}
				$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
				$LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');
				// echo "<pre>";print_r($LOCAL_PRINT);exit;
				if($LOCAL_PRINT == 0){

					if($printerModel ==0){
						// echo'<pre>';
						// print_r($printtype);
						// print_r($printername);
						// exit;
						try {
							    if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 		$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$ans['order_id']."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$ans['order_id']."'");
							 	}
							 	// echo "<pre>";print_r($connector);exit;
							 	if($connector != ''){
								    $printer = new Printer($connector);
								    $printer->selectPrintMode(32);

								   	$printer->setEmphasis(true);
								   	for($i = 1; $i <= $bill_copy; $i++){
								   		if($this->model_catalog_order->get_settings('IS_LOGO') == 1){
								   			$logo = EscposImage::load(DIR_SYSTEM . 'library/escpos-php-development/example/resources/escpos-php.png', false);
								   			$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->bitImage($logo);
											$printer->feed(1);
										}
									   	$printer->setTextSize(2, 2);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
									    $printer->feed(1);
									    $printer->setTextSize(1, 1);
									    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
									    if($ans['bill_status'] == 1 && $duplicate == '1'){
									    	//$printer->feed(1);
									    	$printer->text("Duplicate Bill");
									    	$printer->feed(1);
									    }
									    $printer->setJustification(Printer::JUSTIFY_CENTER);
									    //$printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);

										if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
											
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										 	$printer->text(("Name : ".$ans['cust_name']));
											$printer->feed(1);
											$printer->text(("Mobile :".$ans['cust_contact'].""));
											$printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
										 	$printer->text(("Address : ".$ans['cust_address']));
											$printer->feed(1);
											$printer->text(("Gst No :".$ans['gst_no'].""));
											$printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
										 	$printer->text(("Name : ".$ans['cust_name']));
										    $printer->feed(1);
										    $printer->text(("Address : ".$ans['cust_address'].""));
										    $printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
										 	$printer->text(("Mobile :".$ans['cust_contact']));
										    $printer->feed(1);
										    $printer->text(("Gst No :".$ans['gst_no'].""));
										    $printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
										 	$printer->text(("Name : ".$ans['cust_name']));
										    $printer->feed(1);
										    $printer->text(("Gst No :".$ans['gst_no'].""));
										    $printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
										 	$printer->text(("Mobile :".$ans['cust_contact'])."");
										    $printer->feed(1);
										    $printer->text(("Address : ".$ans['cust_address'].""));
										    $printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Name :".$ans['cust_name']."");
										    $printer->feed(1);
										}
										else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Mobile :".$ans['cust_contact']."");
										    $printer->feed(1);
										}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Address : ".$ans['cust_address']."");
										    $printer->feed(1);
										}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
										    $printer->text("Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}else{
										    $printer->text(("Name : ".$ans['cust_name']));
										    $printer->feed(1);
										    $printer->text(("Mobile :".$ans['cust_contact'].""));
										    $printer->feed(1);
										    $printer->text(("Address : ".$ans['cust_address']));
										    $printer->feed(1);
										    $printer->text(("Gst No :".$ans['gst_no'].""));
										    $printer->feed(1);
										}
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->setEmphasis(true);
									   	$printer->setTextSize(2, 2);
										//$printer->text( "REF NO: ".$orderno." ");
										if(isset($ans['order_no']) && $ans['order_no'] > 0){
											$printer->text( "REF NO: ".$ans['order_no']." ");
										} else {
											$printer->text( "REF NO: ".$orderno." ");
										}
										$printer->feed(1);
									   	$printer->setTextSize(2, 2);

									   	if(isset($ans['t_name'])){
											$printer->text( "Tbl No: ".$ans['t_name']."-".$ans['location']." ");
										}
										$printer->feed(1);
									   	$printer->setTextSize(1, 1);
										//$printer->text(str_pad("User Id :".$ans['login_id'],10)."    /*".str_pad("Ref no: ".$orderno,10)."*/     ".str_pad("K.Ref.No :".$order_id,10));
										$printer->text(str_pad("User Id :".$ans['login_id'],20)."       ".str_pad("K.Ref.No :".$ans['order_id'],20));

									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($infos){
									   			
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
									   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('H:i:s'));
									   		$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	$printer->text(str_pad("Loc",10)."   ".str_pad("Wtr",17)."".str_pad("Cpt",10)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ans['location'],10)."   ".str_pad(substr($ans['waiter'],0,17),17)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",24)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
									   	 	$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infos as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
										    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
									    	 	if($modifierdatabill != array()){
												    	foreach($modifierdatabill as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
											    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
														    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}
											    	$total_items_normal ++ ;
										    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

										    }
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											// foreach($testfoods as $tkey => $tvalue){
											// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										 //    	$printer->feed(1);
											// }
											// $printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ans['fdiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
												$printer->feed(1);
											} elseif($ans['discount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
												$printer->feed(1);
											}
											if($this->model_catalog_order->get_settings('GST_SHOW') == 1){
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("",23)."SCGST (2.5) :".$csgst."");
											$printer->feed(1);
											$printer->text(str_pad("",23)."CCGST (2.5) :".$csgst."");
											$printer->feed(1);
											}
											if($ans['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
														$printer->text(str_pad("",30)."SCRG :".$ans['staxfood']."");
														$printer->feed(1);
													}
													$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
												}else{
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
														$printer->text(str_pad("",30)."SCRG :".$ans['staxfood']."");
														$printer->feed(1);
													}
													$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountfood = ($ans['ftotal'] - ($disamtfood));
												} else{
													$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
												}
											}
											// $printer->setEmphasis(false);
											// $printer->setJustification(Printer::JUSTIFY_RIGHT);
											// // $printer->setTextSize(2, 1);
											// $printer->text("Net total :".ceil($netamountfood)."");
											// $printer->setEmphasis(false);

											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->setTextSize(2, 1);
											$printer->text(str_pad("",1)."Net total :".ceil($netamountfood)."");
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
												//$printer->feed(1);				   		

										}
										
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
								   		if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
								   			$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->feed(1);	
											$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
											if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
												$printer->feed(1);				   		
										   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
									   		}
									   		$printer->feed(1);	
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   	}
									   	if($infosl){
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
									   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infosl[0]['billno'],5)."Time :".date('H:i:s'));
									   		$printer->feed(1);
									   		//$printer->text("Ref no: ".$orderno."");
									   		if(isset($ans['order_no']) && $ans['order_no'] > 0){
												$printer->text( "REF NO: ".$ans['order_no']." ");
											} else {
												$printer->text( "REF NO: ".$orderno." ");
											}
									 		$printer->feed(1);
									 		$printer->setEmphasis(true);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",24)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
											$printer->feed(1);
									    	$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_liquor_normal = 0;
										    $total_quantity_liquor_normal = 0;
										    foreach($infosl as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
										    	$nvalue['rate'] =utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".$nvalue['amt'],8);
										    	$printer->feed(1);
									    	 	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
										    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
															    	$modata['rate'] = utf8_substr(html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8'), 0, 7);
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}

										    	$total_items_liquor_normal ++;
										    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];
										    }
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T Qty :".str_pad($total_quantity_liquor_normal,7)."L.Total :".str_pad($ans['ltotal'],7));
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											// foreach($testliqs as $tkey => $tvalue){
											// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										 //    	$printer->feed(1);
											// }
											// $printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ans['ldiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ldiscountper']."%) :".$ans['ltotalvalue']."");
												$printer->feed(1);
											} elseif($ans['ldiscount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ltotalvalue']."rs) :".$ans['ltotalvalue']."");
												$printer->feed(1);
											}
											if($this->model_catalog_order->get_settings('VAT_SHOW') == 1){
												$printer->setTextSize(2, 1);
												$printer->text(str_pad("",30)."VAT :".$ans['vat']."");
												$printer->feed(1);
											}
											if($ans['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
														$printer->text(str_pad("",29)."SCRG :".$ans['staxliq']."");
														$printer->feed(1);
													}
													$netamountliq = (($ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
												} else{
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
														$printer->text(str_pad("",29)."SCRG :".$ans['staxliq']."");
														$printer->feed(1);
													}
													$netamountliq = (($ans['vat'] + $ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountliq = ($ans['vat'] + $ans['ltotal'] - ($disamtliq));
												} else{
													$netamountliq = ($ans['ltotal'] - ($disamtliq));
												}
											}
											// $printer->setEmphasis(false);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->setTextSize(2, 1);
											$printer->text(str_pad("",1)."Net total :".ceil($netamountliq)."");
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										}
										$printer->feed(1);
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
										if($ansz['advance_amount'] != '0.00'){
											$printer->text(str_pad("Advance Amount",38).$ansz['advance_amount']."");
											$printer->feed(1);
										}
										$printer->setTextSize(2, 2);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->text("GRAND TOTAL:".$gtotal);
										$printer->setTextSize(1, 1);
										$printer->feed(1);
										if($ans['dtotalvalue']!=0){
												$printer->text("Delivery Charge:".$ans['dtotalvalue']);
												$printer->feed(1);
											}
										$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
										if($SETTLEMENT_status == '1'){
											if(isset($this->session->data['credit'])){
												$credit = $this->session->data['credit'];
											} else {
												$credit = '0';
											}
											if(isset($this->session->data['cash'])){
												$cash = $this->session->data['cash'];
											} else {
												$cash = '0';
											}
											if(isset($this->session->data['online'])){
												$online = $this->session->data['online'];
											} else {
												$online ='0';
											}

											if(isset($this->session->data['onac'])){
												$onac = $this->session->data['onac'];
												$onaccontact = $this->session->data['onaccontact'];
												$onacname = $this->session->data['onacname'];

											} else {
												$onac ='0';
											}
										}
										if($SETTLEMENT_status=='1'){
											if($credit!='0' && $credit!=''){
												$printer->text("PAY BY: CARD");
											}
											if($online!='0' && $online!=''){
												$printer->text("PAY BY: ONLINE");
											}
											if($cash!='0' && $cash!=''){
												$printer->text("PAY BY: CASH");
											}
											if($onac!='0' && $onac!=''){
												$printer->text("PAY BY: ON.ACCOUNT");
												$printer->feed(1);
												$printer->text("Name: '".$onacname."'");
												$printer->feed(1);
												$printer->text("Contact: '".$onaccontact."'");
												$printer->feed(1);
											}

										}
										
										$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);

										
										$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
										$printer->feed(1);
										if($this->model_catalog_order->get_settings('TEXT1') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT1'));
											$printer->feed(1);
										}
										if($this->model_catalog_order->get_settings('TEXT2') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT2'));
											$printer->feed(1);
										}
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										if($this->model_catalog_order->get_settings('TEXT3') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT3'));
										}
										$printer->feed(2);
										$printer->cut();
									}
									// Close printer //
								    $printer->close();
							    	$this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$ans['order_id']."'");

								    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$ans['order_id']."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$ans['order_id']."'");
								}
							} catch (Exception $e) {
							    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$ans['order_id']."'");
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$ans['order_id']."'");
							    $this->session->data['warning'] = $printername." "."Not Working";
							}
						
					} else {  // 45 space code starts from here

						try {
							    if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 		$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$ans['order_id']."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$ans['order_id']."'");
							 	}
							 	if($connector != ''){
								    $printer = new Printer($connector);
								    $printer->selectPrintMode(32);

								   	$printer->setEmphasis(true);
								   	// $printer->text('45 Spacess');
					    			$printer->feed(1);
								   	for($i = 1; $i <= $bill_copy; $i++){
								   		if($this->model_catalog_order->get_settings('IS_LOGO') == 1){
								   			$logo = EscposImage::load(DIR_SYSTEM . 'library/escpos-php-development/example/resources/escpos-php.png', false);
								   			$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->bitImage($logo);
											$printer->feed(1);
										}
									   	$printer->setTextSize(2, 2);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
									    $printer->feed(1);
									    $printer->setTextSize(1, 1);
									    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
									    if($ans['bill_status'] == 1 && $duplicate == '1'){
									    	//$printer->feed(1);
									    	$printer->text("Duplicate Bill");
									    	$printer->feed(1);
									    }
									    $printer->setJustification(Printer::JUSTIFY_CENTER);
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);

										if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
											
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										 	$printer->text(("Name : ".$ans['cust_name']));
											$printer->feed(1);
											$printer->text(("Mobile :".$ans['cust_contact'].""));
											$printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
										 	$printer->text(("Address : ".$ans['cust_address']));
											$printer->feed(1);
											$printer->text(("Gst No :".$ans['gst_no'].""));
											$printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
										 	$printer->text(("Name : ".$ans['cust_name']));
										    $printer->feed(1);
										    $printer->text(("Address : ".$ans['cust_address'].""));
										    $printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
										 	$printer->text(("Mobile :".$ans['cust_contact']));
										    $printer->feed(1);
										    $printer->text(("Gst No :".$ans['gst_no'].""));
										    $printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
										 	$printer->text(("Name : ".$ans['cust_name']));
										    $printer->feed(1);
										    $printer->text(("Gst No :".$ans['gst_no'].""));
										    $printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
										 	$printer->text(("Mobile :".$ans['cust_contact'])."");
										    $printer->feed(1);
										    $printer->text(("Address : ".$ans['cust_address'].""));
										    $printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Name :".$ans['cust_name']."");
										    $printer->feed(1);
										}
										else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Mobile :".$ans['cust_contact']."");
										    $printer->feed(1);
										}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Address : ".$ans['cust_address']."");
										    $printer->feed(1);
										}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
										    $printer->text("Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}else{
										    $printer->text(("Name : ".$ans['cust_name']));
										    $printer->feed(1);
										    $printer->text(("Mobile :".$ans['cust_contact'].""));
										    $printer->feed(1);
										    $printer->text(("Address : ".$ans['cust_address']));
										    $printer->feed(1);
										    $printer->text(("Gst No :".$ans['gst_no'].""));
										    $printer->feed(1);
										}
										$printer->text(str_pad("User Id :".$ans['login_id'],20)."K.Ref.No :".$ans['order_id']);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($infos){
									   			
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infos[0]['billno'],6)."Time:".date('H:i'));
									   		$printer->feed(1);
									   		//$printer->text("Ref no: ".$orderno."");
									   		if(isset($ans['order_no']) && $ans['order_no'] > 0){
												$printer->text( "REF NO: ".$ans['order_no']." ");
											} else {
												$printer->text( "REF NO: ".$orderno." ");
											}
									   		$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
											$printer->feed(1);
									   	 	$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infos as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
									    	 	if($modifierdatabill != array()){
												    	foreach($modifierdatabill as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
											    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
														    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}
											    	$total_items_normal ++ ;
										    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

										    }
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											// foreach($testfoods as $tkey => $tvalue){
											// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										 //    	$printer->feed(1);
											// }
											// $printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ans['fdiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
												$printer->feed(1);
											} elseif($ans['discount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
												$printer->feed(1);
											}
											if($this->model_catalog_order->get_settings('GST_SHOW') == 1){
											$printer->text(str_pad("",20)."CCGST(2.5%) :".$csgst."");
											$printer->feed(1);
											$printer->text(str_pad("",20)."CCGST(2.5%) :".$csgst."");
											$printer->feed(1);
											}
											if($ans['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
														$printer->text(str_pad("",28)."SCRG :".$ans['staxfood']."");
														$printer->feed(1);
													}
													$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
												}else{
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
														$printer->text(str_pad("",28)."SCRG :".$ans['staxfood']."");
														$printer->feed(1);
													}
													$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountfood = ($ans['ftotal'] - ($disamtfood));
												} else{
													$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
												}
											}
											$printer->setEmphasis(true);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("",1)."Net total :".ceil($netamountfood)."");
											$printer->setEmphasis(false);
										}
										
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
								   		if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
								   			$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->feed(1);	
											$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
											if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
												$printer->feed(1);				   		
										   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
									   		}
									   		$printer->feed(1);	
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   	}
									   	if($infosl){
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infosl[0]['billno'],6)."Time:".date('H:i'));
									   		$printer->feed(1);
									   		//$printer->text("Ref no: ".$orderno."");
									   		if(isset($ans['order_no']) && $ans['order_no'] > 0){
												$printer->text( "REF NO: ".$ans['order_no']." ");
											} else {
												$printer->text( "REF NO: ".$orderno." ");
											}
									 		$printer->feed(1);
									 		$printer->setEmphasis(true);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
											$printer->feed(1);
									    	$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_liquor_normal = 0;
										    $total_quantity_liquor_normal = 0;
										    foreach($infosl as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] =utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".$nvalue['amt'],8);
										    	$printer->feed(1);
									    	 	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
										    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
															    	$modata['rate'] = utf8_substr(html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8'), 0, 7);
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}

										    	$total_items_liquor_normal ++;
										    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];
										    }
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T Qty :".str_pad($total_quantity_liquor_normal,7)."L.Total :".str_pad($ans['ltotal'],7));
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											// foreach($testliqs as $tkey => $tvalue){
											// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										 //    	$printer->feed(1);
											// }
											// $printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ans['ldiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ldiscountper']."%) :".$ans['ltotalvalue']."");
												$printer->feed(1);
											} elseif($ans['ldiscount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ltotalvalue']."rs) :".$ans['ltotalvalue']."");
												$printer->feed(1);
											}
											if($this->model_catalog_order->get_settings('VAT_SHOW') == 1){
												$printer->setTextSize(2, 1);
												$printer->text(str_pad("",35)."VAT :".$ans['vat']."");
												$printer->feed(1);
											}
											if($ans['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
														$printer->text(str_pad("",35)."SCRG :".$ans['staxliq']."");
														$printer->feed(1);
													}
													$netamountliq = (($ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
												} else{
													if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
														$printer->text(str_pad("",35)."SCRG :".$ans['staxliq']."");
														$printer->feed(1);
													}
													$netamountliq = (($ans['vat'] + $ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountliq = ($ans['vat'] + $ans['ltotal'] - ($disamtliq));
												} else{
													$netamountliq = ($ans['ltotal'] - ($disamtliq));
												}
											}
											$printer->setEmphasis(false);
											$printer->setJustification(Printer::JUSTIFY_RIGHT);
										   	$printer->setTextSize(2, 2);
											$printer->text(str_pad("",1)."Net total :".ceil($netamountliq)."");
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										}
										$printer->feed(1);
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
										if($ansz['advance_amount'] != '0.00'){
											$printer->text(str_pad("Advance Amount",38).$ansz['advance_amount']."");
											$printer->feed(1);
										}
										$printer->setTextSize(2, 2);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->text("GRAND TOTAL:".$gtotal);
										$printer->setTextSize(1, 1);
										$printer->feed(1);
										if($ans['dtotalvalue']!=0){
												$printer->text(str_pad("",20)."Delivery Charge:".$ans['dtotalvalue']);
												$printer->feed(1);
											}
										$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
										if($SETTLEMENT_status == '1'){
											if(isset($this->session->data['credit'])){
												$credit = $this->session->data['credit'];
											} else {
												$credit = '0';
											}
											if(isset($this->session->data['cash'])){
												$cash = $this->session->data['cash'];
											} else {
												$cash = '0';
											}
											if(isset($this->session->data['online'])){
												$online = $this->session->data['online'];
											} else {
												$online ='0';
											}

											if(isset($this->session->data['onac'])){
												$onac = $this->session->data['onac'];
												$onaccontact = $this->session->data['onaccontact'];
												$onacname = $this->session->data['onacname'];

											} else {
												$onac ='0';
											}
										}
										if($SETTLEMENT_status=='1'){
											if($credit!='0' && $credit!=''){
												$printer->text("PAY BY: CARD");
											}
											if($online!='0' && $online!=''){
												$printer->text("PAY BY: ONLINE");
											}
											if($cash!='0' && $cash!=''){
												$printer->text("PAY BY: CASH");
											}
											if($onac!='0' && $onac!=''){
												$printer->text("PAY BY: ON.ACCOUNT");
												$printer->feed(1);
												$printer->text("Name: '".$onacname."'");
												$printer->feed(1);
												$printer->text("Contact: '".$onaccontact."'");
											}
										}
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									   
										// $printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
										// $printer->feed(1);
										if($this->model_catalog_order->get_settings('TEXT1') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT1'));
											$printer->feed(1);
										}
										if($this->model_catalog_order->get_settings('TEXT2') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT2'));
											$printer->feed(1);
										}
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										if($this->model_catalog_order->get_settings('TEXT3') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT3'));
										}
										$printer->feed(2);
										$printer->cut();
									}
									// Close printer //
								    $printer->close();
							    	$this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$ans['order_id']."'");

								    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$ans['order_id']."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$ans['order_id']."'");
								}
							} catch (Exception $e) {
							    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$ans['order_id']."'");
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$ans['order_id']."'");
							    $this->session->data['warning'] = $printername." "."Not Working";
							}
					}

					if($ans['bill_status'] == 1 && $duplicate == '1'){
						$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
					} else{
						$json = array();
						$json = array(
							'LOCAL_PRINT' => 0,
							'status' => 1,
						);
					}
				}else {
					$json = array();
						$final_datas = array();
						$HOTEL_NAME = $this->model_catalog_order->get_settings('HOTEL_NAME');
						$HOTEL_ADD = $this->model_catalog_order->get_settings('HOTEL_ADD');
						$INCLUSIVE = $this->model_catalog_order->get_settings('INCLUSIVE');
						$BAR_NAME = $this->model_catalog_order->get_settings('BAR_NAME');
						$BAR_ADD = $this->model_catalog_order->get_settings('BAR_ADD');
						$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
						$GST_NO = $this->model_catalog_order->get_settings('GST_NO');
						$TEXT1 = $this->model_catalog_order->get_settings('TEXT1');
						$TEXT2 = $this->model_catalog_order->get_settings('TEXT2');
						$TEXT3 = $this->model_catalog_order->get_settings('TEXT3');
						$LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');
						if(isset($testfoods)){
							$testfoods = $testfoods;
						} else {
							$testfoods =array();
						}

						if(isset($infosl)){
							$infosl = $infosl;
						} else {
							$infosl =array();
						}

						if(isset($testliqs)){
							$testliqs = $testliqs;
						} else {
							$testliqs =array();
						}
						$cash = 0;
						$credit = 0;
						$online = 0;
						$onac =0;
						$onaccontact = '';
						$onacname = '';
						if($SETTLEMENT_status == '1'){
							if(isset($this->session->data['credit'])){
								$credit = $this->session->data['credit'];
							} else {
								$credit = '0';
							}
							if(isset($this->session->data['cash'])){
								$cash = $this->session->data['cash'];
							} else {
								$cash = '0';
							}
							if(isset($this->session->data['online'])){
								$online = $this->session->data['online'];
							} else {
								$online ='0';
							}

							if(isset($this->session->data['onac'])){
								$onac = $this->session->data['onac'];
								$onaccontact = $this->session->data['onaccontact'];
								$onacname = $this->session->data['onacname'];

							} else {
								$onac =0;
								$onaccontact = '';
								$onacname = '';
							}
						}
										


						$final_datas = array(
							'ans' => $ans,
							'infos' => $infos,
							'modifierdatabill' => $modifierdatabill,
							'testfoods' => $testfoods,
							'infosl' => $infosl,
							'testliqs' => $testliqs,
							'GST_NO' => $GST_NO,
							'TEXT1' => $TEXT1,
							'TEXT2' => $TEXT2,
							'TEXT3' => $TEXT3,
							'bill_copy' => $bill_copy,
							'cash' =>$cash,
							'card' =>$credit,
							'online' =>$online,
							'onac' =>$onac,
							'onaccontact' =>$onaccontact,
							'onacname' =>$onacname,

							'SETTLEMENT_status' => $SETTLEMENT_status,
							'HOTEL_NAME' => $HOTEL_NAME,
							'HOTEL_ADD' => $HOTEL_ADD,
							'INCLUSIVE' => $INCLUSIVE,
							'BAR_NAME' => $BAR_NAME,
							'orderno' => $orderno,
							'order_id' => $ans['order_id'],
							'duplicate' => $duplicate,
							'csgst' => $csgst,
							'csgsttotal' => $csgsttotal,
							'gtotal' => $gtotal,
							'status' => 1,
						);


						$json = array(
							'LOCAL_PRINT' => $LOCAL_PRINT,
							'final_datas' => $final_datas,

							'status' => 1,
						);
				}
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
			} else {
				// * DUPLICATE BILL PRINT START
				if(($ans['bill_status'] == 1 && $ans['bill_status'] == '1')){
					// echo "<pre>";print_r($ans['order_id']);exit;
					date_default_timezone_set('Asia/Kolkata');
					$this->load->language('customer/customer');
					$this->load->model('catalog/order');
					$merge_datas = array();
					$this->document->setTitle('BILL');
					$te = 'BILL';
					

					$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
					$last_open_dates = $this->db->query($last_open_date_sql);
					if($last_open_dates->num_rows > 0){
						$last_open_date = $last_open_dates->row['bill_date'];
					} else {
						$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
						$last_open_dates = $this->db->query($last_open_date_sql);
						if($last_open_dates->num_rows > 0){
							$last_open_date = $last_open_dates->row['bill_date'];
						} else {
							$last_open_date = date('Y-m-d');
						}
					}

					$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

					$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

					if($last_open_dates_liq->num_rows > 0){
						$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
					} else {
						$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

						$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

						if($last_open_dates_liq->num_rows > 0){
							$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
						} else {
							$last_open_date_liq = date('Y-m-d');
						}
					}

					$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
					$last_open_dates_order = $this->db->query($last_open_date_sql_order);
					if($last_open_dates_order->num_rows > 0){
						$last_open_date_order = $last_open_dates_order->row['bill_date'];
					} else {
						$last_open_date_order = date('Y-m-d');
					}
					// echo "<pre>";print_r($ans);exit;
					if($ans['order_no'] == '0'){
						$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
						$orderno = 1;
						if(isset($orderno_q['order_no'])){
							$orderno = $orderno_q['order_no'] + 1;
						}

						$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
						if($kotno2->num_rows > 0){
							$kot_no2 = $kotno2->row['billno'];
							$kotno = $kot_no2 + 1;
						} else{
							$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
							if($kotno2->num_rows > 0){
								$kot_no2 = $kotno2->row['billno'];
								$kotno = $kot_no2 + 1;
							} else {
								$kotno = 1;
							}
						}

						$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
						if($kotno1->num_rows > 0){
							$kot_no1 = $kotno1->row['billno'];
							$botno = $kot_no1 + 1;
						} else{
							$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
							if($kotno1->num_rows > 0){
								$kot_no1 = $kotno1->row['billno'];
								$botno = $kot_no1 + 1;
							} else {
								$botno = 1;
							}
						}
					}

					$anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$ans['order_id']."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;

					$testfood = array();
					$testliq = array();
					$testtaxvalue1food = 0;
					$testtaxvalue1liq = 0;
					$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$ans['order_id']."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
					foreach($tests as $test){
						$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
						$testfoods[] = array(
							'tax1' => $test['tax1'],
							'amt' => $amt,
							'tax1_value' => $test['tax1_value']
						);
					}
					
					$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$ans['order_id']."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
					foreach($testss as $testa){
						$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
						$testliqs[] = array(
							'tax1' => $testa['tax1'],
							'amt' => $amts,
							'tax1_value' => $testa['tax1_value']
						);
					}
					
					$infosl = array();
					$infos = array();
					$flag = 0;
					$totalquantityfood = 0;
					$totalquantityliq = 0;
					$disamtfood = 0;
					$disamtliq = 0;
					$modifierdatabill = array();

					// echo'<pre>';
					// print_r($ans);
					// echo'<br>';
					foreach ($anss as $lkey => $result) {
						foreach($anss as $lkeys => $results){
							if($lkey == $lkeys) {

							} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
								if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
									if($result['parent'] == '0'){
										$result['code'] = '';
									}
								}
							} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
								if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
									if($result['parent'] == '0'){
										$result['qty'] = $result['qty'] + $results['qty'];
										if($result['nc_kot_status'] == '0'){
											$result['amt'] = $result['qty'] * $result['rate'];
										}
									}
								}
							}
						}
						// echo"<pre>";print_r($result);exit;
						if($result['code'] != '' ){
							$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
							if ($decimal_mesurement == 0) {
									$qty = (int)$result['qty'];
							} else {
									$qty = $result['qty'];
							}
							if($result['is_liq']== 0){
								$infos[] = array(
									'id'			=> $result['id'],
									'billno'		=> $result['billno'],
									'name'          => $result['name'],
									'rate'          => $result['rate'],
									'amt'           => $result['amt'],
									'qty'         	=> $qty,
									'tax1'         	=> $result['tax1'],
									'tax2'          => $result['tax2'],
									'discount_value'=> $result['discount_value']
								);
								$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
								$totalquantityfood = $totalquantityfood + $result['qty'];
								$disamtfood = $disamtfood + $result['discount_value'];
							} else {
								$flag = 1;
								$infosl[] = array(
									'id'			=> $result['id'],
									'billno'		=> $result['billno'],
									'name'          => $result['name'],
									'rate'          => $result['rate'],
									'amt'           => $result['amt'],
									'qty'         	=> $qty,
									'tax1'         	=> $result['tax1'],
									'tax2'          => $result['tax2'],
									'discount_value'=> $result['discount_value']
								);
								$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
								$totalquantityliq = $totalquantityliq + $result['qty'];
								$disamtliq = $disamtliq + $result['discount_value'];
							}
						}
					}

					if($ans['parcel_status'] == '0'){
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$gtotal = ($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
						} else {
							$gtotal = ($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
						}
					} else {
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$gtotal =($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
						} else {
							$gtotal =($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
						}
					}
					// echo"in2";
					// echo "<pre>";print_r($result);
									
					$csgst=$ans['gst']/2;
					$csgsttotal = $ans['gst'];

					$ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
					//$ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 15);
					$ans['cust_address'] = html_entity_decode(wordwrap($ans['cust_address'],112,"\n"), ENT_QUOTES, 'UTF-8');

					$ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
					$ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
					$ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
					$ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
					$ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
					$ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
					$ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
					$ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
					$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
					// echo'<pre>';
					// print_r($ans);
					// exit;
					$this->log->write("bill details");
					$this->log->write($ans);
					$ansz = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$ans['order_id']."' ")->row;
					// echo "<pre>";print_r($ansz);exit;
					if($ansz['advance_amount'] == '0.00'){
						$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
					} else{
						$gtotal = utf8_substr(html_entity_decode($ansz['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
					}
					//$gtotal = ceil($gtotal);
					$gtotal = round($gtotal);

					$printtype = '';
					$printername = '';

					$bill_copy = 1;

					if ($printtype == '' || $printername == '' ) {
						$printtype = $this->user->getPrinterType();
						$printername = $this->user->getPrinterName();
						$locationData =  $this->db->query("SELECT `bill_copy`, `bill_printer_type`, `bill_printer_name`,`parcel_detail` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
						if($locationData->num_rows > 0){
							$locationData = $locationData->row;
							$bill_copy = $locationData['bill_copy'];
						} 
					}

					if ($printtype == '' || $printername == '' ) {
						$locationData =  $this->db->query("SELECT `bill_copy`, `bill_printer_type`, `bill_printer_name`,`parcel_detail` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
						if($locationData->num_rows > 0){
							$locationData = $locationData->row;
							$printtype = $locationData['bill_printer_type'];
							$printername = $locationData['bill_printer_name'];
							$bill_copy = $locationData['bill_copy'];
						} else{
							$printtype = '';
							$printername = '';
							$bill_copy = 1;
						}
					}
					if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
						$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
					 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
					}
					$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
					$LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');
					$duplicate = 1;
					// echo "<pre>";print_r($LOCAL_PRINT);exit;
					if($LOCAL_PRINT == 0){

						if($printerModel ==0){
							// echo'<pre>';
							// print_r($printtype);
							// print_r($printername);
							// exit;
							try {
								    if($printtype == 'Network'){
								 		$connector = new NetworkPrintConnector($printername, 9100);
								 	} else if($printtype == 'Windows'){
								 		$connector = new WindowsPrintConnector($printername);
								 	} else {
								 		$connector = '';
								 		$this->db->query("UPDATE oc_order_info SET duplicate = 1, duplicate_time ='".date('h:i:s')."' WHERE order_id = '".$ans['order_id']."'");
								 	}
								 	// echo "<pre>";print_r($connector);exit;
								 	if($connector != ''){
									    $printer = new Printer($connector);
									    $printer->selectPrintMode(32);

									   	$printer->setEmphasis(true);
									   	for($i = 1; $i <= $bill_copy; $i++){
									   		if($this->model_catalog_order->get_settings('IS_LOGO') == 1){
									   			$logo = EscposImage::load(DIR_SYSTEM . 'library/escpos-php-development/example/resources/escpos-php.png', false);
									   			$printer->setJustification(Printer::JUSTIFY_CENTER);
												$printer->bitImage($logo);
												$printer->feed(1);
											}
										   	$printer->setTextSize(2, 2);
										   	$printer->setJustification(Printer::JUSTIFY_CENTER);
										   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
										    $printer->feed(1);
										    $printer->setTextSize(1, 1);
										    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
										    if($ans['bill_status'] == 1 && $duplicate == '1'){
										    	//$printer->feed(1);
										    	$printer->setTextSize(2, 1);
										    	$printer->text("Duplicate Bill");
										    	$printer->feed(1);
										    }
										    $printer->setJustification(Printer::JUSTIFY_CENTER);
										    //$printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->setEmphasis(true);
										   	$printer->setTextSize(1, 1);

											if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
												
											}
											else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
											 	$printer->text(("Name : ".$ans['cust_name']));
												$printer->feed(1);
												$printer->text(("Mobile :".$ans['cust_contact'].""));
												$printer->feed(1);
											}
											else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
											 	$printer->text(("Address : ".$ans['cust_address']));
												$printer->feed(1);
												$printer->text(("Gst No :".$ans['gst_no'].""));
												$printer->feed(1);
											}
											else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
											 	$printer->text(("Name : ".$ans['cust_name']));
											    $printer->feed(1);
											    $printer->text(("Address : ".$ans['cust_address'].""));
											    $printer->feed(1);
											}
											else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
											 	$printer->text(("Mobile :".$ans['cust_contact']));
											    $printer->feed(1);
											    $printer->text(("Gst No :".$ans['gst_no'].""));
											    $printer->feed(1);
											}
											else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
											 	$printer->text(("Name : ".$ans['cust_name']));
											    $printer->feed(1);
											    $printer->text(("Gst No :".$ans['gst_no'].""));
											    $printer->feed(1);
											}
											else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
											 	$printer->text(("Mobile :".$ans['cust_contact'])."");
											    $printer->feed(1);
											    $printer->text(("Address : ".$ans['cust_address'].""));
											    $printer->feed(1);
											}
											else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
											    $printer->text("Name :".$ans['cust_name']."");
											    $printer->feed(1);
											}
											else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
											    $printer->text("Mobile :".$ans['cust_contact']."");
											    $printer->feed(1);
											}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
											    $printer->text("Address : ".$ans['cust_address']."");
											    $printer->feed(1);
											}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
											    $printer->text("Gst No :".$ans['gst_no']."");
											    $printer->feed(1);
											}else{
											    $printer->text(("Name : ".$ans['cust_name']));
											    $printer->feed(1);
											    $printer->text(("Mobile :".$ans['cust_contact'].""));
											    $printer->feed(1);
											    $printer->text(("Address : ".$ans['cust_address']));
											    $printer->feed(1);
											    $printer->text(("Gst No :".$ans['gst_no'].""));
											    $printer->feed(1);
											}
											$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->setEmphasis(true);
										   	$printer->setTextSize(2, 2);
											//$printer->text( "REF NO: ".$orderno." ");
											if(isset($ans['order_no']) && $ans['order_no'] > 0){
												$printer->text( "REF NO: ".$ans['order_no']." ");
											} else {
												$printer->text( "REF NO: ".$orderno." ");
											}
											$printer->feed(1);
										   	$printer->setTextSize(2, 2);

										   	if(isset($ans['t_name'])){
												$printer->text( "Tbl No: ".$ans['t_name']."-".$ans['location']." ");
											}
											$printer->feed(1);
										   	$printer->setTextSize(1, 1);
											//$printer->text(str_pad("User Id :".$ans['login_id'],10)."    /*".str_pad("Ref no: ".$orderno,10)."*/     ".str_pad("K.Ref.No :".$order_id,10));
											$printer->text(str_pad("User Id :".$ans['login_id'],20)."       ".str_pad("K.Ref.No :".$ans['order_id'],20));

										    $printer->setEmphasis(true);
										   	$printer->setTextSize(1, 1);
										   	if($infos){
										   			
										   		$printer->feed(1);
										   		$printer->setJustification(Printer::JUSTIFY_CENTER);
										   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('H:i:s'));
										   		$printer->feed(1);
										   		$printer->setTextSize(1, 1);
										   		$printer->setJustification(Printer::JUSTIFY_LEFT);
										    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
										    	$printer->text(str_pad("Loc",10)."   ".str_pad("Wtr",17)."".str_pad("Cpt",10)."Person");
										    	$printer->feed(1);
										   		$printer->setTextSize(1, 1);
										   		$printer->text(str_pad($ans['location'],10)."   ".str_pad(substr($ans['waiter'],0,17),17)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
											    $printer->feed(1);
										   		$printer->setEmphasis(false);
											    $printer->text("----------------------------------------------");
												$printer->feed(1);
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->text(str_pad("Name",24)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
										   	 	$printer->text("----------------------------------------------");
												$printer->feed(1);
												$printer->setEmphasis(false);
												$total_items_normal = 0;
												$total_quantity_normal = 0;
											    foreach($infos as $nkey => $nvalue){
											    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
											    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
											    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
											    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
											    	$printer->feed(1);
										    	 	if($modifierdatabill != array()){
													    	foreach($modifierdatabill as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $nvalue['id']){
												    				foreach($value as $modata){
												    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
															    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
															    		$printer->feed(1);
														    		}
														    	}
												    		}
												    	}
												    	$total_items_normal ++ ;
											    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

											    }
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setJustification(Printer::JUSTIFY_LEFT);
											    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											    $printer->text("----------------------------------------------");
												$printer->feed(1);
												// foreach($testfoods as $tkey => $tvalue){
												// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
											 //    	$printer->feed(1);
												// }
												// $printer->text("----------------------------------------------");
												$printer->feed(1);
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												if($ans['fdiscountper'] != '0'){
													$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
													$printer->feed(1);
												} elseif($ans['discount'] != '0'){
													$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
													$printer->feed(1);
												}
												if($this->model_catalog_order->get_settings('GST_SHOW') == 1){
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->text(str_pad("",23)."SCGST (2.5) :".$csgst."");
												$printer->feed(1);
												$printer->text(str_pad("",23)."CCGST (2.5) :".$csgst."");
												$printer->feed(1);
												}
												if($ans['parcel_status'] == '0'){
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
															$printer->text(str_pad("",30)."SCRG :".$ans['staxfood']."");
															$printer->feed(1);
														}
														$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
													}else{
														if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
															$printer->text(str_pad("",30)."SCRG :".$ans['staxfood']."");
															$printer->feed(1);
														}
														$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
													}
												} else {
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$netamountfood = ($ans['ftotal'] - ($disamtfood));
													} else{
														$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
													}
												}
												// $printer->setEmphasis(false);
												// $printer->setJustification(Printer::JUSTIFY_RIGHT);
												// // $printer->setTextSize(2, 1);
												// $printer->text("Net total :".ceil($netamountfood)."");
												// $printer->setEmphasis(false);

												$printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->setTextSize(2, 1);
												$printer->text(str_pad("",1)."Net total :".ceil($netamountfood)."");
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
													//$printer->feed(1);				   		

											}
											
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->setEmphasis(true);
										   	$printer->setTextSize(1, 1);
									   		if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
									   			$printer->setJustification(Printer::JUSTIFY_CENTER);
												$printer->feed(1);	
												$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
												if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
													$printer->feed(1);				   		
											   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
										   		}
										   		$printer->feed(1);	
										   		$printer->setJustification(Printer::JUSTIFY_LEFT);
										   	}
										   	if($infosl){
										   		$printer->feed(1);
										   		$printer->setJustification(Printer::JUSTIFY_CENTER);
										   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infosl[0]['billno'],5)."Time :".date('H:i:s'));
										   		$printer->feed(1);
										   		//$printer->text("Ref no: ".$orderno."");
										   		if(isset($ans['order_no']) && $ans['order_no'] > 0){
													$printer->text( "REF NO: ".$ans['order_no']." ");
												} else {
													$printer->text( "REF NO: ".$orderno." ");
												}
										 		$printer->feed(1);
										 		$printer->setEmphasis(true);
										   		$printer->setTextSize(1, 1);
										   		$printer->setJustification(Printer::JUSTIFY_LEFT);
										    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
										    	$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
										    	$printer->feed(1);
										   		$printer->setTextSize(1, 1);
										   		$printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
											    $printer->feed(1);
										   		$printer->setEmphasis(false);
											    $printer->text("----------------------------------------------");
												$printer->feed(1);
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->text(str_pad("Name",24)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
												$printer->feed(1);
										    	$printer->text("----------------------------------------------");
												$printer->feed(1);
												$printer->setEmphasis(false);
												$total_items_liquor_normal = 0;
											    $total_quantity_liquor_normal = 0;
											    foreach($infosl as $nkey => $nvalue){
											    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
											    	$nvalue['rate'] =utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
											    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".$nvalue['amt'],8);
											    	$printer->feed(1);
										    	 	if($modifierdatabill != array()){
												    	foreach($modifierdatabill as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
											    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																    	$modata['rate'] = utf8_substr(html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8'), 0, 7);
																    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}

											    	$total_items_liquor_normal ++;
											    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];
											    }
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setJustification(Printer::JUSTIFY_LEFT);
											    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T Qty :".str_pad($total_quantity_liquor_normal,7)."L.Total :".str_pad($ans['ltotal'],7));
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											    $printer->text("----------------------------------------------");
												$printer->feed(1);
												// foreach($testliqs as $tkey => $tvalue){
												// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
											 //    	$printer->feed(1);
												// }
												// $printer->text("----------------------------------------------");
												$printer->feed(1);
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												if($ans['ldiscountper'] != '0'){
													$printer->text(str_pad("",21)."Discount(".$ans['ldiscountper']."%) :".$ans['ltotalvalue']."");
													$printer->feed(1);
												} elseif($ans['ldiscount'] != '0'){
													$printer->text(str_pad("",21)."Discount(".$ans['ltotalvalue']."rs) :".$ans['ltotalvalue']."");
													$printer->feed(1);
												}
												if($this->model_catalog_order->get_settings('VAT_SHOW') == 1){
													$printer->setTextSize(2, 1);
													$printer->text(str_pad("",30)."VAT :".$ans['vat']."");
													$printer->feed(1);
												}
												if($ans['parcel_status'] == '0'){
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
															$printer->text(str_pad("",29)."SCRG :".$ans['staxliq']."");
															$printer->feed(1);
														}
														$netamountliq = (($ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
													} else{
														if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
															$printer->text(str_pad("",29)."SCRG :".$ans['staxliq']."");
															$printer->feed(1);
														}
														$netamountliq = (($ans['vat'] + $ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
													}
												} else{
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$netamountliq = ($ans['vat'] + $ans['ltotal'] - ($disamtliq));
													} else{
														$netamountliq = ($ans['ltotal'] - ($disamtliq));
													}
												}
												// $printer->setEmphasis(false);
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->setTextSize(2, 1);
												$printer->text(str_pad("",1)."Net total :".ceil($netamountliq)."");
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											}
											$printer->feed(1);
											$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->setEmphasis(true);
											if($ansz['advance_amount'] != '0.00'){
												$printer->text(str_pad("Advance Amount",38).$ansz['advance_amount']."");
												$printer->feed(1);
											}
											$printer->setTextSize(2, 2);
											$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->text("GRAND TOTAL:".$gtotal);
											$printer->setTextSize(1, 1);
											$printer->feed(1);
											if($ans['dtotalvalue']!=0){
													$printer->text("Delivery Charge:".$ans['dtotalvalue']);
													$printer->feed(1);
												}
											$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
											if($SETTLEMENT_status == '1'){
												if(isset($this->session->data['credit'])){
													$credit = $this->session->data['credit'];
												} else {
													$credit = '0';
												}
												if(isset($this->session->data['cash'])){
													$cash = $this->session->data['cash'];
												} else {
													$cash = '0';
												}
												if(isset($this->session->data['online'])){
													$online = $this->session->data['online'];
												} else {
													$online ='0';
												}

												if(isset($this->session->data['onac'])){
													$onac = $this->session->data['onac'];
													$onaccontact = $this->session->data['onaccontact'];
													$onacname = $this->session->data['onacname'];

												} else {
													$onac ='0';
												}
											}
											if($SETTLEMENT_status=='1'){
												if($credit!='0' && $credit!=''){
													$printer->text("PAY BY: CARD");
												}
												if($online!='0' && $online!=''){
													$printer->text("PAY BY: ONLINE");
												}
												if($cash!='0' && $cash!=''){
													$printer->text("PAY BY: CASH");
												}
												if($onac!='0' && $onac!=''){
													$printer->text("PAY BY: ON.ACCOUNT");
													$printer->feed(1);
													$printer->text("Name: '".$onacname."'");
													$printer->feed(1);
													$printer->text("Contact: '".$onaccontact."'");
													$printer->feed(1);
												}

											}
											
											$printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);

											
											$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
											$printer->feed(1);
											if($this->model_catalog_order->get_settings('TEXT1') != ''){
												$printer->text($this->model_catalog_order->get_settings('TEXT1'));
												$printer->feed(1);
											}
											if($this->model_catalog_order->get_settings('TEXT2') != ''){
												$printer->text($this->model_catalog_order->get_settings('TEXT2'));
												$printer->feed(1);
											}
											$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_CENTER);
											if($this->model_catalog_order->get_settings('TEXT3') != ''){
												$printer->text($this->model_catalog_order->get_settings('TEXT3'));
											}
											$printer->feed(2);
											$printer->cut();
										}
										// Close printer //
									    $printer->close();
								    	$this->db->query("UPDATE oc_order_info SET duplicate = 1, duplicate_time ='".date('h:i:s')."' WHERE order_id = '".$ans['order_id']."'");
									}
								} catch (Exception $e) {
								    $this->db->query("UPDATE oc_order_info SET duplicate = 1, duplicate_time ='".date('h:i:s')."' WHERE order_id = '".$ans['order_id']."'");

								    $this->session->data['warning'] = $printername." "."Not Working";
								}
							
						} else {  // 45 space code starts from here

							try {
								    if($printtype == 'Network'){
								 		$connector = new NetworkPrintConnector($printername, 9100);
								 	} else if($printtype == 'Windows'){
								 		$connector = new WindowsPrintConnector($printername);
								 	} else {
								 		$connector = '';
								 		$this->db->query("UPDATE oc_order_info SET duplicate = 1, duplicate_time ='".date('h:i:s')."' WHERE order_id = '".$ans['order_id']."'");
								 	}
								 	if($connector != ''){
									    $printer = new Printer($connector);
									    $printer->selectPrintMode(32);

									   	$printer->setEmphasis(true);
									   	// $printer->text('45 Spacess');
						    			$printer->feed(1);
									   	for($i = 1; $i <= $bill_copy; $i++){
									   		if($this->model_catalog_order->get_settings('IS_LOGO') == 1){
									   			$logo = EscposImage::load(DIR_SYSTEM . 'library/escpos-php-development/example/resources/escpos-php.png', false);
									   			$printer->setJustification(Printer::JUSTIFY_CENTER);
												$printer->bitImage($logo);
												$printer->feed(1);
											}
										   	$printer->setTextSize(2, 2);
										   	$printer->setJustification(Printer::JUSTIFY_CENTER);
										   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
										    $printer->feed(1);
										    $printer->setTextSize(1, 1);
										    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
										    if($ans['bill_status'] == 1 && $duplicate == '1'){
										    	//$printer->feed(1);
										    	$printer->setTextSize(2, 1);
										    	$printer->text("Duplicate Bill");
										    	$printer->feed(1);
										    }
										    $printer->setJustification(Printer::JUSTIFY_CENTER);
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->setEmphasis(true);
										   	$printer->setTextSize(1, 1);

											if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
												
											}
											else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
											 	$printer->text(("Name : ".$ans['cust_name']));
												$printer->feed(1);
												$printer->text(("Mobile :".$ans['cust_contact'].""));
												$printer->feed(1);
											}
											else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
											 	$printer->text(("Address : ".$ans['cust_address']));
												$printer->feed(1);
												$printer->text(("Gst No :".$ans['gst_no'].""));
												$printer->feed(1);
											}
											else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
											 	$printer->text(("Name : ".$ans['cust_name']));
											    $printer->feed(1);
											    $printer->text(("Address : ".$ans['cust_address'].""));
											    $printer->feed(1);
											}
											else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
											 	$printer->text(("Mobile :".$ans['cust_contact']));
											    $printer->feed(1);
											    $printer->text(("Gst No :".$ans['gst_no'].""));
											    $printer->feed(1);
											}
											else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
											 	$printer->text(("Name : ".$ans['cust_name']));
											    $printer->feed(1);
											    $printer->text(("Gst No :".$ans['gst_no'].""));
											    $printer->feed(1);
											}
											else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
											 	$printer->text(("Mobile :".$ans['cust_contact'])."");
											    $printer->feed(1);
											    $printer->text(("Address : ".$ans['cust_address'].""));
											    $printer->feed(1);
											}
											else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
											    $printer->text("Name :".$ans['cust_name']."");
											    $printer->feed(1);
											}
											else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
											    $printer->text("Mobile :".$ans['cust_contact']."");
											    $printer->feed(1);
											}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
											    $printer->text("Address : ".$ans['cust_address']."");
											    $printer->feed(1);
											}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
											    $printer->text("Gst No :".$ans['gst_no']."");
											    $printer->feed(1);
											}else{
											    $printer->text(("Name : ".$ans['cust_name']));
											    $printer->feed(1);
											    $printer->text(("Mobile :".$ans['cust_contact'].""));
											    $printer->feed(1);
											    $printer->text(("Address : ".$ans['cust_address']));
											    $printer->feed(1);
											    $printer->text(("Gst No :".$ans['gst_no'].""));
											    $printer->feed(1);
											}
											$printer->text(str_pad("User Id :".$ans['login_id'],20)."K.Ref.No :".$ans['order_id']);
										    $printer->setEmphasis(true);
										   	$printer->setTextSize(1, 1);
										   	if($infos){
										   			
										   		$printer->feed(1);
										   		$printer->setJustification(Printer::JUSTIFY_LEFT);
										   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infos[0]['billno'],6)."Time:".date('H:i'));
										   		$printer->feed(1);
										   		//$printer->text("Ref no: ".$orderno."");
										   		if(isset($ans['order_no']) && $ans['order_no'] > 0){
													$printer->text( "REF NO: ".$ans['order_no']." ");
												} else {
													$printer->text( "REF NO: ".$orderno." ");
												}
										   		$printer->feed(1);
										   		$printer->setTextSize(1, 1);
										   		$printer->setJustification(Printer::JUSTIFY_LEFT);
										    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
										    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
										    	$printer->feed(1);
										   		$printer->setTextSize(1, 1);
										   		$printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
											    $printer->feed(1);
										   		$printer->setEmphasis(false);
											    $printer->text("------------------------------------------");
												$printer->feed(1);
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
												$printer->feed(1);
										   	 	$printer->text("------------------------------------------");
												$printer->feed(1);
												$printer->setEmphasis(false);
												$total_items_normal = 0;
												$total_quantity_normal = 0;
											    foreach($infos as $nkey => $nvalue){
											    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
											    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
											    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
											    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
											    	$printer->feed(1);
										    	 	if($modifierdatabill != array()){
													    	foreach($modifierdatabill as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $nvalue['id']){
												    				foreach($value as $modata){
												    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
																    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
															    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
															    		$printer->feed(1);
														    		}
														    	}
												    		}
												    	}
												    	$total_items_normal ++ ;
											    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

											    }
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setJustification(Printer::JUSTIFY_LEFT);
											    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											    $printer->text("------------------------------------------");
												$printer->feed(1);
												// foreach($testfoods as $tkey => $tvalue){
												// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
											 //    	$printer->feed(1);
												// }
												// $printer->text("------------------------------------------");
												$printer->feed(1);
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												if($ans['fdiscountper'] != '0'){
													$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
													$printer->feed(1);
												} elseif($ans['discount'] != '0'){
													$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
													$printer->feed(1);
												}
												if($this->model_catalog_order->get_settings('GST_SHOW') == 1){
												$printer->text(str_pad("",20)."CCGST(2.5%) :".$csgst."");
												$printer->feed(1);
												$printer->text(str_pad("",20)."CCGST(2.5%) :".$csgst."");
												$printer->feed(1);
												}
												if($ans['parcel_status'] == '0'){
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
															$printer->text(str_pad("",28)."SCRG :".$ans['staxfood']."");
															$printer->feed(1);
														}
														$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
													}else{
														if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
															$printer->text(str_pad("",28)."SCRG :".$ans['staxfood']."");
															$printer->feed(1);
														}
														$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
													}
												} else{
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$netamountfood = ($ans['ftotal'] - ($disamtfood));
													} else{
														$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
													}
												}
												$printer->setEmphasis(true);
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->text(str_pad("",1)."Net total :".ceil($netamountfood)."");
												$printer->setEmphasis(false);
											}
											
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->setEmphasis(true);
										   	$printer->setTextSize(1, 1);
									   		if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
									   			$printer->setJustification(Printer::JUSTIFY_CENTER);
												$printer->feed(1);	
												$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
												if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
													$printer->feed(1);				   		
											   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
										   		}
										   		$printer->feed(1);	
										   		$printer->setJustification(Printer::JUSTIFY_LEFT);
										   	}
										   	if($infosl){
										   		$printer->feed(1);
										   		$printer->setJustification(Printer::JUSTIFY_LEFT);
										   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infosl[0]['billno'],6)."Time:".date('H:i'));
										   		$printer->feed(1);
										   		//$printer->text("Ref no: ".$orderno."");
										   		if(isset($ans['order_no']) && $ans['order_no'] > 0){
													$printer->text( "REF NO: ".$ans['order_no']." ");
												} else {
													$printer->text( "REF NO: ".$orderno." ");
												}
										 		$printer->feed(1);
										 		$printer->setEmphasis(true);
										   		$printer->setTextSize(1, 1);
										   		$printer->setJustification(Printer::JUSTIFY_LEFT);
										    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
										    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
										    	$printer->feed(1);
										   		$printer->setTextSize(1, 1);
										   		$printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
											    $printer->feed(1);
										   		$printer->setEmphasis(false);
											    $printer->text("------------------------------------------");
												$printer->feed(1);
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
												$printer->feed(1);
										    	$printer->text("------------------------------------------");
												$printer->feed(1);
												$printer->setEmphasis(false);
												$total_items_liquor_normal = 0;
											    $total_quantity_liquor_normal = 0;
											    foreach($infosl as $nkey => $nvalue){
											    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
											    	$nvalue['rate'] =utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
											    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".$nvalue['amt'],8);
											    	$printer->feed(1);
										    	 	if($modifierdatabill != array()){
												    	foreach($modifierdatabill as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
											    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
																    	$modata['rate'] = utf8_substr(html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8'), 0, 7);
																    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}

											    	$total_items_liquor_normal ++;
											    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];
											    }
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setJustification(Printer::JUSTIFY_LEFT);
											    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T Qty :".str_pad($total_quantity_liquor_normal,7)."L.Total :".str_pad($ans['ltotal'],7));
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											    $printer->text("------------------------------------------");
												$printer->feed(1);
												// foreach($testliqs as $tkey => $tvalue){
												// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
											 //    	$printer->feed(1);
												// }
												// $printer->text("------------------------------------------");
												$printer->feed(1);
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												if($ans['ldiscountper'] != '0'){
													$printer->text(str_pad("",21)."Discount(".$ans['ldiscountper']."%) :".$ans['ltotalvalue']."");
													$printer->feed(1);
												} elseif($ans['ldiscount'] != '0'){
													$printer->text(str_pad("",21)."Discount(".$ans['ltotalvalue']."rs) :".$ans['ltotalvalue']."");
													$printer->feed(1);
												}
												if($this->model_catalog_order->get_settings('VAT_SHOW') == 1){
													$printer->setTextSize(2, 1);
													$printer->text(str_pad("",35)."VAT :".$ans['vat']."");
													$printer->feed(1);
												}
												if($ans['parcel_status'] == '0'){
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
															$printer->text(str_pad("",35)."SCRG :".$ans['staxliq']."");
															$printer->feed(1);
														}
														$netamountliq = (($ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
													} else{
														if($this->model_catalog_order->get_settings('SCRG_SHOW') == 1){
															$printer->text(str_pad("",35)."SCRG :".$ans['staxliq']."");
															$printer->feed(1);
														}
														$netamountliq = (($ans['vat'] + $ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
													}
												} else{
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$netamountliq = ($ans['vat'] + $ans['ltotal'] - ($disamtliq));
													} else{
														$netamountliq = ($ans['ltotal'] - ($disamtliq));
													}
												}
												$printer->setEmphasis(false);
												$printer->setJustification(Printer::JUSTIFY_RIGHT);
											   	$printer->setTextSize(2, 2);
												$printer->text(str_pad("",1)."Net total :".ceil($netamountliq)."");
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											}
											$printer->feed(1);
											$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->setEmphasis(true);
											if($ansz['advance_amount'] != '0.00'){
												$printer->text(str_pad("Advance Amount",38).$ansz['advance_amount']."");
												$printer->feed(1);
											}
											$printer->setTextSize(2, 2);
											$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->text("GRAND TOTAL:".$gtotal);
											$printer->setTextSize(1, 1);
											$printer->feed(1);
											if($ans['dtotalvalue']!=0){
													$printer->text(str_pad("",20)."Delivery Charge:".$ans['dtotalvalue']);
													$printer->feed(1);
												}
											$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
											if($SETTLEMENT_status == '1'){
												if(isset($this->session->data['credit'])){
													$credit = $this->session->data['credit'];
												} else {
													$credit = '0';
												}
												if(isset($this->session->data['cash'])){
													$cash = $this->session->data['cash'];
												} else {
													$cash = '0';
												}
												if(isset($this->session->data['online'])){
													$online = $this->session->data['online'];
												} else {
													$online ='0';
												}

												if(isset($this->session->data['onac'])){
													$onac = $this->session->data['onac'];
													$onaccontact = $this->session->data['onaccontact'];
													$onacname = $this->session->data['onacname'];

												} else {
													$onac ='0';
												}
											}
											if($SETTLEMENT_status=='1'){
												if($credit!='0' && $credit!=''){
													$printer->text("PAY BY: CARD");
												}
												if($online!='0' && $online!=''){
													$printer->text("PAY BY: ONLINE");
												}
												if($cash!='0' && $cash!=''){
													$printer->text("PAY BY: CASH");
												}
												if($onac!='0' && $onac!=''){
													$printer->text("PAY BY: ON.ACCOUNT");
													$printer->feed(1);
													$printer->text("Name: '".$onacname."'");
													$printer->feed(1);
													$printer->text("Contact: '".$onaccontact."'");
												}
											}
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										   
											// $printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
											// $printer->feed(1);
											if($this->model_catalog_order->get_settings('TEXT1') != ''){
												$printer->text($this->model_catalog_order->get_settings('TEXT1'));
												$printer->feed(1);
											}
											if($this->model_catalog_order->get_settings('TEXT2') != ''){
												$printer->text($this->model_catalog_order->get_settings('TEXT2'));
												$printer->feed(1);
											}
											$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_CENTER);
											if($this->model_catalog_order->get_settings('TEXT3') != ''){
												$printer->text($this->model_catalog_order->get_settings('TEXT3'));
											}
											$printer->feed(2);
											$printer->cut();
										}
										// Close printer //
									    $printer->close();
								    	$this->db->query("UPDATE oc_order_info SET duplicate = 1, duplicate_time ='".date('h:i:s')."' WHERE order_id = '".$ans['order_id']."'");
									}
								} catch (Exception $e) {
								    $this->db->query("UPDATE oc_order_info SET duplicate = 1, duplicate_time ='".date('h:i:s')."' WHERE order_id = '".$ans['order_id']."'");
								    $this->session->data['warning'] = $printername." "."Not Working";
								}
						}

						$this->response->addHeader('Content-Type: application/json');
						$this->response->setOutput(json_encode($json));
					}
				}
				// * DUPLICATE BILL PRINT END
				// $this->session->data['warning'] = 'Bill already printed';
				// $this->request->post = array();
				// $_POST = array();

			}
			
		}
	}

}