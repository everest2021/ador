<?php
class ControllerCatalogSalePromotion extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/sale_promotion');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/sale_promotion');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/sale_promotion');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/sale_promotion');
		//$this->load->model('catalog/location');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;

			$this->model_catalog_sale_promotion->addSalePromotion($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_c_id'])) {
				$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
			}


			if (isset($this->request->get['filter_contact'])) {
				$url .= '&filter_contact=' . $this->request->get['filter_contact'];
			}


			if (isset($this->request->get['filter_email'])) {
				$url .= '&filter_email=' . $this->request->get['filter_email'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/sale_promotion', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}
	
	public function edit() {
		$this->load->language('catalog/sale_promotion');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/sale_promotion');
		//$this->load->model('catalog/location');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//echo('innn');exit;
			$this->model_catalog_sale_promotion->editSalePromotion($this->request->get['sale_promotion_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_c_id'])) {
				$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
			}

			if (isset($this->request->get['filter_contact'])) {
				$url .= '&filter_contact=' . $this->request->get['filter_contact'];
			}


			if (isset($this->request->get['filter_email'])) {
				$url .= '&filter_email=' . $this->request->get['filter_email'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/sale_promotion', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/sale_promotion');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/sale_promotion');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $sale_promotion_id) {
				$this->model_catalog_sale_promotion->deleteSalePromotionss($sale_promotion_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';


			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_c_id'])) {
				$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
			}

			if (isset($this->request->get['filter_contact'])) {
				$url .= '&filter_contact=' . $this->request->get['filter_contact'];
			}

			if (isset($this->request->get['filter_email'])) {
				$url .= '&filter_email=' . $this->request->get['filter_email'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/sale_promotion', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_c_id'])) {
			$filter_c_id = $this->request->get['filter_c_id'];
		} else {
			$filter_c_id = '';
		}

		if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = '';
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_contact'])) {
			$filter_contact = $this->request->get['filter_contact'];
		} else {
			$filter_contact = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . $this->request->get['filter_email'];
		}
		if (isset($this->request->get['filter_contact'])) {
			$url .= '&filter_contact=' . $this->request->get['filter_contact'];
		}
		if (isset($this->request->get['filter_c_id'])) {
			$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/sale_promotion', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/sale_promotion/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/sale_promotion/delete', 'token=' . $this->session->data['token'] . $url, true);
		//$data['delete'] = $this->url->link('catalog/sale_promotion/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			'filter_name' => $filter_name,
			'filter_c_id' => $filter_c_id,
			'filter_email' => $filter_email,
			'filter_contact' => $filter_contact,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$customer_total = $this->model_catalog_sale_promotion->getTotalSalePromotion($filter_data);

		$results = $this->model_catalog_sale_promotion->getSalePromotion($filter_data);

		$data['sale_promotions'] = array();
		foreach ($results as $result) {
			$data['sale_promotions'][] = array(
				'id' => $result['id'],
				'name'        => $result['name'],
				'edit'        => $this->url->link('catalog/sale_promotion/edit', 'token=' . $this->session->data['token'] . '&sale_promotion_id=' . $result['id'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';

		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_c_id'])) {
			$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . $this->request->get['filter_email'];
		}

		if (isset($this->request->get['filter_contact'])) {
			$url .= '&filter_contact=' . $this->request->get['filter_contact'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['name'] = $this->url->link('catalog/sale_promotion', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['contact'] = $this->url->link('catalog/sale_promotion', 'token=' . $this->session->data['token'] . '&sort=contact' . $url, true);
		$data['email'] = $this->url->link('catalog/sale_promotion', 'token=' . $this->session->data['token'] . '&sort=email' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_c_id'])) {
			$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
		}
		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . $this->request->get['filter_email'];
		}
		if (isset($this->request->get['filter_contact'])) {
			$url .= '&filter_contact=' . $this->request->get['filter_contact'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $customer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/sale_promotion', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($customer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($customer_total - $this->config->get('config_limit_admin'))) ? $customer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $customer_total, ceil($customer_total / $this->config->get('config_limit_admin')));

		$data['filter_email'] = $filter_email;
		$data['filter_c_id'] = $filter_c_id;
		$data['filter_contact'] = $filter_contact;
		$data['filter_name'] = $filter_name;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/sale_promotion_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['location_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');


		$this->load->model('catalog/sale_promotion');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['contact'])) {
			$data['error_contact'] = $this->error['contact'];
		} else {
			$data['error_contact'] = '';
			// echo $data['error_contact'];
			// exit();
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';

		}
		// echo $data['error_name'];
		// 	exit();

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_c_id'])) {
			$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
		}

		if (isset($this->request->get['filter_contact'])) {
			$url .= '&filter_contact=' . $this->request->get['filter_contact'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/sale_promotion', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['sale_promotion_id'])) {
			$data['action'] = $this->url->link('catalog/sale_promotion/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/sale_promotion/edit', 'token=' . $this->session->data['token'] . '&sale_promotion_id=' . $this->request->get['sale_promotion_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/sale_promotion', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['sale_promotion_id'] = 0;
		if (isset($this->request->get['sale_promotion_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$sale_promotion_info = $this->model_catalog_sale_promotion->getSalePromotionss($this->request->get['sale_promotion_id']);
			$data['sale_promotion_id'] = $this->request->get['sale_promotion_id'];
		}

		$data['token'] = $this->session->data['token'];
		
		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($sale_promotion_info)) {
			$data['name'] = $sale_promotion_info['name'];
		} else {
			$data['name'] = '';
		}

		if (isset($this->request->post['per_point'])) {
			$data['per_point'] = $this->request->post['per_point'];
		} elseif (!empty($sale_promotion_info)) {
			$data['per_point'] = $sale_promotion_info['per_point'];
		} else {
			$data['per_point'] = '';
		}

		if (isset($this->request->post['point_rupees'])) {
			$data['point_rupees'] = $this->request->post['point_rupees'];
		} elseif (!empty($sale_promotion_info)) {
			$data['point_rupees'] = $sale_promotion_info['point_rupees'];
		} else {
			$data['point_rupees'] = '';
		}

		if (isset($this->request->post['point'])) {
			$data['point'] = $this->request->post['point'];
		} elseif (!empty($sale_promotion_info)) {
			$data['point'] = $sale_promotion_info['point'];
		} else {
			$data['point'] = '';
		}

		if (isset($this->request->post['amount'])) {
			$data['amount'] = $this->request->post['amount'];
		} elseif (!empty($sale_promotion_info)) {
			$data['amount'] = $sale_promotion_info['amount'];
		} else {
			$data['amount'] = '';
		}

		if (isset($this->request->post['discount_rupees'])) {
			$data['discount_rupees'] = $this->request->post['discount_rupees'];
		} elseif (!empty($sale_promotion_info)) {
			$data['discount_rupees'] = $sale_promotion_info['discount_rupees'];
		} else {
			$data['discount_rupees'] = '';
		}

		if (isset($this->request->post['discount_percentage'])) {
			$data['discount_percentage'] = $this->request->post['discount_percentage'];
		} elseif (!empty($sale_promotion_info)) {
			$data['discount_percentage'] = $sale_promotion_info['discount_percentage'];
		} else {
			$data['discount_percentage'] = '';
		}

		$data['days'] = array();

		if (isset($this->request->post['days'])) {
			$data['days'] = $this->request->post['days'];
		} elseif (!empty($sale_promotion_info)) {
			$days = unserialize($sale_promotion_info['days']);
			if(!empty($days)){
				foreach ($days as $key => $value) {
					$data['days'][] = $value;
				}
			}
		} else {
			$data['days'] = array();
		}
		// echo '<pre>';print_r($data['days']);
		// exit;
		if (isset($this->request->post['subcategory'])) {
			$data['subcategory'] = $this->request->post['subcategory'];
		} elseif (!empty($sale_promotion_info)) {
			$data['subcategory'] = $sale_promotion_info['sub_category_id'];
		} else {
			$data['subcategory'] = '0';
		}

		$data['menuitem'] = array();

		/*if (isset($this->request->post['menuitem'])) {
			$data['menuitem'] = $this->request->post['menuitem'];
		} elseif (!empty($sale_promotion_info)) {
			$menuitem_array = $this->db->query("SELECT * FROM " . DB_PREFIX . "sale_promotion_items WHERE sale_promotion_id = '" . (int)$this->request->get['sale_promotion_id'] . "' AND free_sub_cat = '0' AND sub_category_id = '" . (int)$sale_promotion_info['sub_category_id'] . "' ");
			if($menuitem_array->num_rows > 0){
				foreach ($menuitem_array->rows as $mkey => $mvalue) {
					$data['menuitem'][] = $mvalue['item_id'];
				}
			}
		} else {
			$data['menuitem'] = array();
		}*/

		if (isset($this->request->post['free_subcategory'])) {
			$data['free_subcategory'] = $this->request->post['free_subcategory'];
		} elseif (!empty($sale_promotion_info)) {
			$data['free_subcategory'] = $sale_promotion_info['free_sub_category_id'];
		} else {
			$data['free_subcategory'] = '0';
		}

		$data['free_menuitem'] = array();

		/*if (isset($this->request->post['free_menuitem'])) {
			$data['free_menuitem'] = $this->request->post['free_menuitem'];
		} elseif (!empty($sale_promotion_info)) {
			$menuitem_array = $this->db->query("SELECT * FROM " . DB_PREFIX . "sale_promotion_items WHERE sale_promotion_id = '" . (int)$this->request->get['sale_promotion_id'] . "' AND free_sub_cat = '0' AND sub_category_id = '" . (int)$sale_promotion_info['sub_category_id'] . "' ");
			if($menuitem_array->num_rows > 0){
				foreach ($menuitem_array->rows as $mkey => $mvalue) {
					$data['free_menuitem'][] = $mvalue['item_id'];
				}
			}		} else {
			$data['free_menuitem'] = array();
		}*/

		if (isset($this->request->post['from_date'])) {
			$data['from_date'] = date('Y-m-d',strtotime($this->request->post['from_date']));
		} elseif (!empty($sale_promotion_info)) {
			$data['from_date'] =date('Y-m-d',strtotime( $sale_promotion_info['from_date']));
		} else {
			$data['from_date'] = '';
		}

		if (isset($this->request->post['to_date'])) {
			$data['to_date'] = date('Y-m-d',strtotime($this->request->post['to_date']));
		} elseif (!empty($sale_promotion_info)) {
			$data['to_date'] = date('Y-m-d',strtotime($sale_promotion_info['to_date']));
		} else {
			$data['to_date'] = '';
		}

		if (isset($this->request->post['from_time'])) {
			$data['from_time'] = $this->request->post['from_time'];
		} elseif (!empty($sale_promotion_info)) {
			$data['from_time'] = $sale_promotion_info['from_time'];
		} else {
			$data['from_time'] = '';
		}

		if (isset($this->request->post['to_time'])) {
			$data['to_time'] = $this->request->post['to_time'];
		} elseif (!empty($sale_promotion_info)) {
			$data['to_time'] = $sale_promotion_info['to_time'];
		} else {
			$data['to_time'] = '';
		}

		$data['subcategory_data'] = $this->model_catalog_sale_promotion->getCategory();

		$data['days_array'] = array(
			"1" => "Monday",
			"2" => "Tuesday", 
			"3" => "Wednesday", 
			"4" => "Thursday",
			"5" => "Friday",
			"6" => "Saturday", 
			"7" => "Sunday", 
		);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/sale_promotion_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/sale_promotion')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		/*if ((strlen($this->request->post['name']) < 2) || (strlen($this->request->post['name']) > 255)) {
			$this->error['name'] = 'Please Enter  Name';
		}*/

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/sale_promotion')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/sale_promotion');

		return !$this->error;
	}

	public function items() {
		$json = array();
		$json['items'] = array();
		$json['status'] = 0;
		if (isset($this->request->get['subcategory_id']) && $this->request->get['subcategory_id'] > 0) {
			$this->load->model('catalog/sale_promotion');
			$data = array(
				'subcategory_id' => $this->request->get['subcategory_id'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_sale_promotion->getitems($data);

			$selected_item = array();
			if(isset($this->request->get['sale_promotion_id']) && $this->request->get['sale_promotion_id'] > 0){
				$selected_item_data = $this->db->query("SELECT `item_id`,`quantity` FROM " . DB_PREFIX . "sale_promotion_items WHERE sale_promotion_id = '" . (int)$this->request->get['sale_promotion_id'] . "' AND free_sub_cat = '" . (int)$this->request->get['free_sub_cat'] . "' AND sub_category_id = '" . (int)$this->request->get['subcategory_id'] . "' ");
				if($selected_item_data->num_rows > 0){
					foreach ($selected_item_data->rows as $skey => $svalue) {
						$selected_item[$svalue['item_id']] = $svalue;
					}
				}
			}
		
			foreach ($results as $result) {
				$is_selected = 0;
				$quantity = 0;
				if(isset($selected_item[$result['item_id']])){
					$is_selected = 1;
					$quantity = $selected_item[$result['item_id']]['quantity'];
				}
				$json['items'][] = array(
					'item_id' => $result['item_id'],
					'is_selected' => $is_selected,
					'quantity'	=> $quantity,
					'item_name'    => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8')),
				);
			}
			//echo '<prE>';print_r($json['items']);exit;	
			$json['status'] = 1;	
		}
		$this->response->setOutput(json_encode($json));
	}

}