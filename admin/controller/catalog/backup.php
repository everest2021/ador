<?php
class ControllerCatalogBackup extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/item');
		$this->document->setTitle('Backup');
		$this->getForm();
	}

	protected function getForm() {
		$data['heading_title'] = 'Bill Merge';//$this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['location_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/table', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['action'] = $this->url->link('catalog/dayclose/edit', 'token=' . $this->session->data['token'] . $url, true);
		$data['action1'] = $this->url->link('catalog/dayclose/edit_force', 'token=' . $this->session->data['token'] . $url, true);
		$data['cancel'] = $this->url->link('catalog/dayclose', 'token=' . $this->session->data['token'] . $url, true);

		$data['token'] = $this->session->data['token'];

		/*$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `bill_date` ASC LIMIT 1";
		$last_open_date_purchase_sql = "SELECT `invoice_date` FROM `oc_purchase_transaction` WHERE `invoice_date` < '".date('Y-m-d')."' ORDER BY `invoice_date` ASC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		$last_open_dates_purchase = $this->db->query($last_open_date_purchase_sql);
		if($last_open_dates->num_rows > 0){
			$open_date = $last_open_dates->row['bill_date'];
		} elseif ($last_open_dates_purchase->num_rows > 0) {
			$open_date = $last_open_dates_purchase->row['invoice_date'];
		} else {
			$open_date = date('Y-m-d');
		}*/
		// echo"<pre>";print_r($open_date);exit;

		// $this->log->write(print_r($this->request->get, true));
		/*
		if (isset($this->request->post['open_date'])) {
			$data['open_date'] = $this->request->post['open_date'];
		} else {
			$data['open_date'] = $open_date;
		}*/
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/backup_form', $data));
	}

	public function bkpdatabase() {
			$name = DB_DATABASE;
			$user = DB_USERNAME;
			$pass = DB_PASSWORD;
			$host = DB_HOSTNAME;
			$date = date('Y-m-d');
			$date_1 = date('Y_m_d_H_i_s');
			$download_path = DIR_DOWNLOAD."db_ador_bk_".$date_1.".sql";
			$db_bkp = DATABASE_BKP;
			$command = "\"".$db_bkp.":\\xampp\\mysql\\bin\\mysqldump.exe\" --opt --skip-extended-insert --complete-insert --host=".$host." --user=".$user." --password=".$pass." ".$name." > " . $download_path;
			//echo $command;exit;
			exec($command);

			$data = 1;
			$this->response->setOutput(json_encode($data));
	}
}