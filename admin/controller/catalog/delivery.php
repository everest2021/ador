<?php
class Controllercatalogdelivery extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/report');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/reportdelivery');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/reportdelivery', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_status'])){
			$data['filter_status'] = $this->request->post['filter_status'];
		}
		else{
			$data['filter_status'] = '';
		}

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_moneyrecive_status'])){
			$data['moneyrecive_status'] = $this->request->post['filter_moneyrecive_status'];
		}
		else{
			$data['moneyrecive_status'] = '';
		}
		// echo"<pre>";print_r($this->request->post);exit;

		$data['data1'] = array(	
		      '0' =>'Unpaid',
		      '1' => 'Paid',
		);


		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';

		$data['nc_kot_amt'] = 0;

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate'])){

			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);
			$data['amts'] = '';
			$data['gamt'] = '';
			if($this->request->post['filter_status'] != ''){
				foreach($dates as $date){
					$billdata[$date] = $this->db->query("SELECT * FROM `oc_order_info` oit LEFT JOIN oc_order_items oi on (oit.`order_id`=oi.`order_id`)  WHERE  oit.`bill_date` = '".$date."' AND oit.`bill_status` = '1' AND oit.food_cancel <> '1' AND oit.liq_cancel <> '1' AND oit.cancel_status = 0 AND oit.`complimentary_status` = '0' AND oit.`complimentary_status` = '0' AND oit.`dboy_name` != '' AND oit.`moneyrecive_status` = '".$this->request->post['filter_status']."'  GROUP BY oi.`order_id` ORDER BY oit.order_no ASC ")->rows;
				}
			} else {

				foreach($dates as $date){
					$billdata[$date] = $this->db->query("SELECT * FROM `oc_order_info` oit LEFT JOIN oc_order_items oi on (oit.`order_id`=oi.`order_id`)  WHERE  oit.`bill_date` = '".$date."' AND oit.`bill_status` = '1' AND oit.food_cancel <> '1' AND oit.liq_cancel <> '1' AND oit.cancel_status = 0 AND oit.`complimentary_status` = '0' AND oit.`complimentary_status` = '0' AND oit.`dboy_name` != ''  GROUP BY oi.`order_id` ORDER BY oit.order_no ASC ")->rows;
				}	
			}

			$gtotal = $this->db->query("SELECT SUM(dboy_amount) as amts FROM `oc_order_info` WHERE `bill_date` = '".$start_date."' AND `bill_status` = '1' AND `dboy_name` != '' ")->row;
			$my = $gtotal['amts'];

			$data['amts'] = $my ;

			$ftotal = $this->db->query("SELECT SUM(grand_total) as famts FROM `oc_order_info` WHERE `bill_date` = '".$start_date."' AND `bill_status` = '1' AND `dboy_name` != '' ")->row;

			$new1 = $ftotal['famts'];

			$data['famts'] = $new1 ;

			$ftotal =  $this->db->query("SELECT SUM(given_amount) as gamt FROM `oc_order_info` WHERE `bill_date` = '".$start_date."' AND `bill_status` = '1' AND `dboy_name` != '' AND `moneyrecive_status` = '1' ")->row;
			$new = $ftotal['gamt'];

			$data['gamt'] = $new ;
				
		} 
		


		
		



		$data['billdatas'] = $billdata;
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['edit_bill'] = $this->user->getEditBill();

		$data['action'] = $this->url->link('catalog/delivery', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/delivery', $data));
	}


	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	
}