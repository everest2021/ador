<?php
class Controllercatalogsplitbill extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/item');
		$this->document->setTitle('Split Bill');
		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/table');
		$this->document->setTitle('Split Bill');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// for ($x = 0; $x <= $this->request->post['order_no_1']; $x++) {
			//   echo "The number is: $x <br>";
			// }
			// echo "<pre>";print_r($this->request->post);exit;
			$this->db->query("UPDATE " . DB_PREFIX . "order_info SET split_bill_status ='" . $this->request->post['order_no_1']. "' , split_bill_amt ='" . $this->request->post['Split_1']. "' , split_bill_no ='" . $this->request->post['order_no']. "'WHERE t_name = '" . $this->request->post['order_no'] . "' AND bill_status ='0' ");

			$json['done'] = '<script>parent.closeIFrame();</script>';
			$json['info'] = 1;
			$this->response->setOutput(json_encode($json));
		} else {
			$json['info'] = 0;
			$this->response->setOutput(json_encode($json));
		}
	}

	protected function getForm() {
		$data['heading_title'] = 'Bill Merge';//$this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['location_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/table', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['order_no'])) {
			$data['action'] = $this->url->link('catalog/split_bill/edit', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/split_bill/edit', 'token=' . $this->session->data['token'] . '&order_no=' . $this->request->get['order_no'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/split_bill', 'token=' . $this->session->data['token'] . $url . '&order_no=' . $this->request->get['order_no'], true);

		$data['token'] = $this->session->data['token'];

		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		if (isset($this->request->post['order_no'])) {
			$data['order_no'] = $this->request->post['order_no'];
			// $results = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no` FROM `oc_order_info` WHERE `order_no` = '".$data['order_no']."' AND `date` = '".$last_open_date."' ");
			// $food_total = 0;
			// $liquor_total = 0;
			// if($results->num_rows > 0){
			// 	$result = $results->row;
			// 	$food_total = $result['ftotal'];
			// 	$liquor_total = $result['ltotal'];
			// }
			$data['food_total'] = '';//$food_total;
			$data['liquor_total'] = '';//$liquor_total;
			$data['grand_total'] = '';//$food_total + $liquor_total;
		} elseif (isset($this->request->get['order_no'])) {
			//$data['order_no'] = $this->request->get['order_no'];
			$data['order_no'] = '';
			$data['food_total'] = '';
			$data['liquor_total'] = '';
			$data['grand_total'] = '';
			// $results = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no` FROM `oc_order_info` WHERE `order_no` = '".$data['order_no']."' AND `date` = '".$last_open_date."' ");
			// $food_total = 0;
			// $liquor_total = 0;
			// if($results->num_rows > 0){
			// 	$result = $results->rows;
			// 	$food_total = $result['ftotal'];
			// 	$liquor_total = $result['ltotal'];
			// }
			$data['food_total'] = '';//$food_total;
			$data['liquor_total'] = '';//$liquor_total;
			$data['grand_total'] = '';//$food_total + $liquor_total;
		} else {
			$data['order_no'] = '';
			$data['food_total'] = '';
			$data['liquor_total'] = '';
			$data['grand_total'] = '';
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/split_bill', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/split_bill')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/split_bill')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}

	public function getorderinfo() {
		$json = array();
		$json['status'] = 0;
		if (isset($this->request->get['order_no'])) {
			if(isset($this->request->get['order_no'])){
				$order_no = $this->request->get['order_no'];
			} else {
				$order_no = null;
			}
			
			$filter_data = array(
				'order_no' => $order_no,
				//'master_order_no' => $master_order_no,
			);
			
			$results = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `merge_number`, `grand_total` FROM `oc_order_info` WHERE `t_name` = '".$filter_data['order_no']."' AND `year_close_status` = '0' AND bill_status = '0' ")->rows;
			//$this->log->write(print_r($results, true));
			$in = 0;
			foreach ($results as $result) {
				$in = 1;
				if($result['merge_number'] == '0'){
					$json = array(
						'order_no' => $result['order_no'],
						'ftotal'   => $result['ftotal'],
						'ltotal'   => $result['ltotal'],
						'grand_total' => $result['grand_total'],
						'status' => 1,
					);
				} else {
					$json['status'] = 2;
				}
			}
			if($in == 1){

			}
		}
		//$this->log->write(print_r($json, true));
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function split(){
		//echo "<pre>";print_r($this->request->get);
		$json = array();
		$json['status'] = 0;

		if (isset($this->request->get['person']) && isset($this->request->get['grand_total'])) {
			if(isset($this->request->get['person'])){
				$person = $this->request->get['person'];
			} else {
				$person = '';
			}

			if(isset($this->request->get['grand_total'])){
				$grand_total = $this->request->get['grand_total'];
			} else {
				$grand_total = '';
			}

			$divid_bill_amt = $grand_total/$person;
			//echo "<pre>";print_r($divid_bill_amt);exit;

			$json = array(
				'divid_amt' => $divid_bill_amt,
				'status' => 1,
			);	
		}	
		// echo "<pre>";print_r($json);exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
}