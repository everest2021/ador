<?php
class Controllercatalogurbanpierstore extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/urbanpier_store');
		$this->document->setTitle($this->language->get('heading_title'));
		//$this->getList();
		$this->getForm();
	}

	public function add() {
		$this->load->language('catalog/urbanpier_store');

		$this->document->setTitle($this->language->get('heading_title'));

		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->db->query("DELETE FROM `oc_urbanpiper_store_detail` WHERE 1=1");
			$this->db->query("INSERT INTO `oc_urbanpiper_store_detail` SET
					ref_id = '" . $this->db->escape($this->request->post['entry_store_ref_id']) . "',
					name = '".$this->db->escape($this->request->post['entry_store_nme'])."',
					address = '".$this->db->escape($this->request->post['entry_store_add'])."',
					city = '".$this->db->escape($this->request->post['entry_store_city'])."',
					zip_codes = '".$this->db->escape($this->request->post['entry_store_zipcode'])."',
					contact_phone = '".$this->db->escape($this->request->post['entry_store_contact_phone'])."',
					notification_phones = '".$this->db->escape($this->request->post['entry_store_notification_no'])."',
					min_pickup_time = '".$this->db->escape($this->request->post['entry_minimum_pick_time'])."',
					min_delivery_time = '".$this->db->escape($this->request->post['entry_minimum_delivery_time'])."',
					min_order_value = '".$this->db->escape($this->request->post['entry_minimum_order_value'])."',
					geo_longitude = '".$this->db->escape($this->request->post['entry_geo_longitude'])."',
					geo_latitude = '".$this->db->escape($this->request->post['entry_geo_latitude'])."',
					active = '".$this->db->escape($this->request->post['entry_ordering_enabled'])."',
					notification_emails = '".$this->db->escape($this->request->post['notification_emails'])."',
					fullfillmode = '" . (isset($this->request->post['fullfillmode']) ? $this->db->escape(json_encode($this->request->post['fullfillmode'])) : '') . "', 
					packaging_charge = '".$this->db->escape($this->request->post['entry_packaging_charge'])."',
					delivery_charge = '".$this->db->escape($this->request->post['entry_delivery_charge'])."'
			");

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/urbanpier_store');

		$this->document->setTitle($this->language->get('heading_title'));

		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_product->editProduct($this->request->get['product_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/urbanpier_store');

		$this->document->setTitle($this->language->get('heading_title'));

		

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $product_id) {
				$this->model_catalog_product->deleteProduct($product_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function copy() {
		$this->load->language('catalog/urbanpier_store');

		$this->document->setTitle($this->language->get('heading_title'));

		

		if (isset($this->request->post['selected']) && $this->validateCopy()) {
			foreach ($this->request->post['selected'] as $product_id) {
				$this->model_catalog_product->copyProduct($product_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_model'])) {
			$filter_model = $this->request->get['filter_model'];
		} else {
			$filter_model = null;
		}

		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}

		if (isset($this->request->get['filter_quantity'])) {
			$filter_quantity = $this->request->get['filter_quantity'];
		} else {
			$filter_quantity = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/urbanpier_store/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['copy'] = $this->url->link('catalog/urbanpier_store/copy', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/urbanpier_store/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['products'] = array();

		$filter_data = array(
			'filter_name'	  => $filter_name,
			'filter_model'	  => $filter_model,
			'filter_price'	  => $filter_price,
			'filter_quantity' => $filter_quantity,
			'filter_status'   => $filter_status,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);

		$this->load->model('tool/image');

		$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

		$results = $this->model_catalog_product->getProducts($filter_data);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 40, 40);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 40, 40);
			}

			$special = false;

			$product_specials = $this->model_catalog_product->getProductSpecials($result['product_id']);

			foreach ($product_specials  as $product_special) {
				if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
					$special = $product_special['price'];

					break;
				}
			}

			$data['products'][] = array(
				'product_id' => $result['product_id'],
				'image'      => $image,
				'name'       => $result['name'],
				'model'      => $result['model'],
				'price'      => $result['price'],
				'special'    => $special,
				'quantity'   => $result['quantity'],
				'status'     => ($result['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'edit'       => $this->url->link('catalog/urbanpier_store/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_image'] = $this->language->get('column_image');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store_nme'] = $this->language->get('entry_store_nme');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_copy'] = $this->language->get('button_copy');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, true);
		$data['sort_model'] = $this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . '&sort=p.model' . $url, true);
		$data['sort_price'] = $this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . '&sort=p.price' . $url, true);
		$data['sort_quantity'] = $this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . '&sort=p.quantity' . $url, true);
		$data['sort_status'] = $this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . '&sort=p.status' . $url, true);
		$data['sort_order'] = $this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . '&sort=p.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_model'] = $filter_model;
		$data['filter_price'] = $filter_price;
		$data['filter_quantity'] = $filter_quantity;
		$data['filter_status'] = $filter_status;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/urbanpier_store_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['product_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_plus'] = $this->language->get('text_plus');
		$data['text_minus'] = $this->language->get('text_minus');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_option'] = $this->language->get('text_option');
		$data['text_option_value'] = $this->language->get('text_option_value');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_store_nme_lbl'] = $this->language->get('entry_store_nme');
		$data['entry_store_add_lbl'] = $this->language->get('entry_store_add');
		$data['entry_store_zipcode_lbl'] = $this->language->get('entry_store_zipcode');
		$data['entry_store_city_lbl'] = $this->language->get('entry_store_city');
		
		$data['entry_store_contact_phone_lbl'] = $this->language->get('entry_store_contact_phone');
		$data['entry_store_notification_no_lbl'] = $this->language->get('entry_store_notification_no');

		$data['text_select_all'] = $this->language->get('text_select_all');
		$data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$data['entry_store_notification_email_lbl'] = $this->language->get('entry_store_notification_email_no');
		$data['entry_store_ref_id_lbl'] = $this->language->get('entry_store_ref_id');
		$data['entry_minimum_pick_time_lbl'] = $this->language->get('entry_minimum_pick_time');
		$data['entry_minimum_delivery_time_lbl'] = $this->language->get('entry_minimum_delivery_time');
		$data['entry_minimum_order_value_lbl'] = $this->language->get('entry_minimum_order_value');
		$data['entry_geo_longitude_lbl'] = $this->language->get('entry_geo_longitude');
		$data['entry_geo_latitude_lbl'] = $this->language->get('entry_geo_latitude');
		$data['entry_ordering_enabled_lbl'] = $this->language->get('entry_ordering_enabled');
		$data['entry_ordering_timing_lbl'] = $this->language->get('entry_ordering_timing');

		$data['entry_ordering_start_timing_lbl'] = $this->language->get('entry_ordering_start_timing');
		$data['entry_ordering_end_timing_lbl'] = $this->language->get('entry_ordering_end_timing');

		$data['entry_packaging_charge_lbl'] = $this->language->get('entry_packaging_charge');

		$data['days_name'] = array(
			'sunday' => 'sunday',
		    'monday' => 'monday',
		    'tuesday' => 'tuesday',
		    'wednesday' => 'wednesday',
		    'thursday' => 'thursday',
		    'friday' => 'friday',
		    'saturday' => 'saturday'
		);



		$data['entry_sku'] = $this->language->get('entry_sku');
		$data['entry_upc'] = $this->language->get('entry_upc');
		$data['entry_ean'] = $this->language->get('entry_ean');
		$data['entry_jan'] = $this->language->get('entry_jan');
		$data['entry_isbn'] = $this->language->get('entry_isbn');
		$data['entry_mpn'] = $this->language->get('entry_mpn');
		$data['entry_location'] = $this->language->get('entry_location');
		$data['entry_minimum'] = $this->language->get('entry_minimum');
		$data['entry_shipping'] = $this->language->get('entry_shipping');
		$data['entry_date_available'] = $this->language->get('entry_date_available');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_stock_status'] = $this->language->get('entry_stock_status');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_points'] = $this->language->get('entry_points');
		$data['entry_option_points'] = $this->language->get('entry_option_points');
		$data['entry_subtract'] = $this->language->get('entry_subtract');
		$data['entry_weight_class'] = $this->language->get('entry_weight_class');
		$data['entry_weight'] = $this->language->get('entry_weight');
		$data['entry_dimension'] = $this->language->get('entry_dimension');
		$data['entry_length_class'] = $this->language->get('entry_length_class');
		$data['entry_length'] = $this->language->get('entry_length');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_additional_image'] = $this->language->get('entry_additional_image');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
		$data['entry_download'] = $this->language->get('entry_download');
		$data['entry_category'] = $this->language->get('entry_category');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_related'] = $this->language->get('entry_related');
		$data['entry_attribute'] = $this->language->get('entry_attribute');
		$data['entry_text'] = $this->language->get('entry_text');
		$data['entry_option'] = $this->language->get('entry_option');
		$data['entry_option_value'] = $this->language->get('entry_option_value');
		$data['entry_required'] = $this->language->get('entry_required');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_priority'] = $this->language->get('entry_priority');
		$data['entry_tag'] = $this->language->get('entry_tag');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_reward'] = $this->language->get('entry_reward');
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_recurring'] = $this->language->get('entry_recurring');

		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_sku'] = $this->language->get('help_sku');
		$data['help_upc'] = $this->language->get('help_upc');
		$data['help_ean'] = $this->language->get('help_ean');
		$data['help_jan'] = $this->language->get('help_jan');
		$data['help_isbn'] = $this->language->get('help_isbn');
		$data['help_mpn'] = $this->language->get('help_mpn');
		$data['help_minimum'] = $this->language->get('help_minimum');
		$data['help_manufacturer'] = $this->language->get('help_manufacturer');
		$data['help_stock_status'] = $this->language->get('help_stock_status');
		$data['help_points'] = $this->language->get('help_points');
		$data['help_category'] = $this->language->get('help_category');
		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_download'] = $this->language->get('help_download');
		$data['help_related'] = $this->language->get('help_related');
		$data['help_tag'] = $this->language->get('help_tag');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_attribute_add'] = $this->language->get('button_attribute_add');
		$data['button_option_add'] = $this->language->get('button_option_add');
		$data['button_option_value_add'] = $this->language->get('button_option_value_add');
		$data['button_discount_add'] = $this->language->get('button_discount_add');
		$data['button_special_add'] = $this->language->get('button_special_add');
		$data['button_image_add'] = $this->language->get('button_image_add');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_recurring_add'] = $this->language->get('button_recurring_add');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_attribute'] = $this->language->get('tab_attribute');
		$data['tab_option'] = $this->language->get('tab_option');
		$data['tab_recurring'] = $this->language->get('tab_recurring');
		$data['tab_discount'] = $this->language->get('tab_discount');
		$data['tab_special'] = $this->language->get('tab_special');
		$data['tab_image'] = $this->language->get('tab_image');
		$data['tab_links'] = $this->language->get('tab_links');
		$data['tab_reward'] = $this->language->get('tab_reward');
		$data['tab_design'] = $this->language->get('tab_design');
		$data['tab_openbay'] = $this->language->get('tab_openbay');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['error_entry_store_ref_id'])) {
			$data['error_entry_store_ref_id'] = $this->error['error_entry_store_ref_id'];
		} else {
			$data['error_entry_store_ref_id'] = array();
		}

		if (isset($this->error['error_entry_store_nme'])) {
			$data['error_entry_store_nme'] = $this->error['error_entry_store_nme'];
		} else {
			$data['error_entry_store_nme'] = array();
		}

		if (isset($this->error['error_entry_store_add'])) {
			$data['error_entry_store_add'] = $this->error['error_entry_store_add'];
		} else {
			$data['error_entry_store_add'] = array();
		}


		if (isset($this->error['error_entry_store_city'])) {
			$data['error_entry_store_city'] = $this->error['error_entry_store_city'];
		} else {
			$data['error_entry_store_city'] = array();
		}


		if (isset($this->error['error_entry_store_zipcode'])) {
			$data['error_entry_store_zipcode'] = $this->error['error_entry_store_zipcode'];
		} else {
			$data['error_entry_store_zipcode'] = array();
		}

		if (isset($this->error['error_entry_store_contact_phone_lbl'])) {
			$data['error_entry_store_contact_phone_lbl'] = $this->error['error_entry_store_contact_phone_lbl'];
		} else {
			$data['error_entry_store_contact_phone_lbl'] = array();
		}

		if (isset($this->error['error_entry_store_nme'])) {
			$data['error_entry_store_nme'] = $this->error['error_entry_store_nme'];
		} else {
			$data['error_entry_store_nme'] = array();
		}
		if (isset($this->error['error_entry_store_notification_no'])) {
			$data['error_entry_store_notification_no'] = $this->error['error_entry_store_notification_no'];
		} else {
			$data['error_entry_store_notification_no'] = array();
		}

		if (isset($this->error['error_notification_emails'])) {
			$data['error_notification_emails'] = $this->error['error_notification_emails'];
		} else {
			$data['error_notification_emails'] = array();
		}
		
		if (isset($this->error['error_entry_minimum_pick_time'])) {
			$data['error_entry_minimum_pick_time'] = $this->error['error_entry_minimum_pick_time'];
		} else {
			$data['error_entry_minimum_pick_time'] = array();
		}
		if (isset($this->error['error_entry_minimum_delivery_time'])) {
			$data['error_entry_minimum_delivery_time'] = $this->error['error_entry_minimum_delivery_time'];
		} else {
			$data['error_entry_minimum_delivery_time'] = array();
		}
		if (isset($this->error['error_entry_minimum_order_value'])) {
			$data['error_entry_minimum_order_value'] = $this->error['error_entry_minimum_order_value'];
		} else {
			$data['error_entry_minimum_order_value'] = array();
		}

		if (isset($this->error['error_entry_packaging_charge'])) {
			$data['error_entry_packaging_charge'] = $this->error['error_entry_packaging_charge'];
		} else {
			$data['error_entry_packaging_charge'] = array();
		}


		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['model'])) {
			$data['error_model'] = $this->error['model'];
		} else {
			$data['error_model'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . $url, true)
		);

		
		$data['action'] = $this->url->link('catalog/urbanpier_store/add', 'token=' . $this->session->data['token'], true);
		

		$data['cancel'] = $this->url->link('catalog/webhook', 'token=' . $this->session->data['token'] . $url, true);
		$data['platform_datas_array'] = array(
			'zomato' => 'zomato',
		    'swiggy' => 'swiggy',
		    'ubereats' => 'ubereats',
		    'scootsy' => 'scootsy',
		    'dunzo' => 'dunzo',
		    'dotpe' => 'dotpe',
		    'foodpanda' => 'foodpanda',
		    'amazon' => 'amazon',
		    'swiggystore' => 'swiggystore',
		    'zomatomarket' => 'zomatomarket',
				    
		);
		$data['fullfillment_mode_array']['delivery'] = 'delivery';
		$data['fullfillment_mode_array']['pickup'] = 'pickup';
		$product_info = array();
		$store_info = array();
		$platform_datas = array();
		if (($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$product_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "urbanpiper_store_detail WHERE  1 = '1' ");
			if($product_query->num_rows > 0){
				$product_info = $product_query->row;
			} 
			$store_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "urbanpiper_store WHERE  1 = '1' ");
			if($store_query->num_rows > 0){
				$store_info = $store_query->rows;
			} 

			$platform_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "urbanpiper_platform WHERE  1 = '1' ");
			if($platform_query->num_rows > 0){
				$platform_datas = $platform_query->rows;
			} 
		}

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['fullfillmode'])) {
			$data['fullfillmode'] = $this->request->post['fullfillmode'];
		} elseif (!empty($product_info['fullfillmode'])) {
			$data['fullfillmode'] = json_decode($product_info['fullfillmode']);
		} else {
			$data['fullfillmode'] = array();
		}

		

		if (isset($this->request->post['entry_store_ref_id'])) {
			$data['entry_store_ref_id'] = $this->request->post['entry_store_ref_id'];
		} elseif (!empty($product_info)) {
			$data['entry_store_ref_id'] = $product_info['ref_id'];
		} else {
			$data['entry_store_ref_id'] = '';
		}

		if (isset($this->request->post['entry_ordering_enabled'])) {
			$data['entry_ordering_enabled'] = $this->request->post['entry_ordering_enabled'];
		} elseif (!empty($product_info)) {
			$data['entry_ordering_enabled'] = $product_info['active'];
		} else {
			$data['entry_ordering_enabled'] = '';
		}

		if (isset($this->request->post['entry_store_nme'])) {
			$data['entry_store_nme'] = $this->request->post['entry_store_nme'];
		} elseif (!empty($product_info)) {
			$data['entry_store_nme'] = $product_info['name'];
		} else {
			$data['entry_store_nme'] = '';
		}

		if (isset($this->request->post['entry_store_add'])) {
			$data['entry_store_add'] = $this->request->post['entry_store_add'];
		} elseif (!empty($product_info)) {
			$data['entry_store_add'] = $product_info['address'];
		} else {
			$data['entry_store_add'] = '';
		}

		if (isset($this->request->post['entry_store_city'])) {
			$data['entry_store_city'] = $this->request->post['entry_store_city'];
		} elseif (!empty($product_info)) {
			$data['entry_store_city'] = $product_info['city'];
		} else {
			$data['entry_store_city'] = '';
		}

		if (isset($this->request->post['entry_store_zipcode'])) {
			$data['entry_store_zipcode'] = $this->request->post['entry_store_zipcode'];
		} elseif (!empty($product_info)) {
			$data['entry_store_zipcode'] = $product_info['zip_codes'];
		} else {
			$data['entry_store_zipcode'] = '';
		}

		if (isset($this->request->post['entry_store_contact_phone'])) {
			$data['entry_store_contact_phone'] = $this->request->post['entry_store_contact_phone'];
		} elseif (!empty($product_info)) {
			$data['entry_store_contact_phone'] = $product_info['contact_phone'];
		} else {
			$data['entry_store_contact_phone'] = '';
		}

		if (isset($this->request->post['entry_store_notification_no'])) {
			$data['entry_store_notification_no'] = $this->request->post['entry_store_notification_no'];
		} elseif (!empty($product_info['notification_phones'])) {
			$data['entry_store_notification_no'] = $product_info['notification_phones'];
		} else {
			$data['entry_store_notification_no'] = '';
		}

		if (isset($this->request->post['notification_emails'])) {
			$data['notification_emails'] = $this->request->post['notification_emails'];
		} elseif (!empty($product_info['notification_phones'])) {
			$data['notification_emails'] = $product_info['notification_emails'];
		} else {
			$data['notification_emails'] = '';
		}



		if (isset($this->request->post['entry_minimum_pick_time'])) {
			$data['entry_minimum_pick_time'] = $this->request->post['entry_minimum_pick_time'];
		} elseif (!empty($product_info)) {
			$data['entry_minimum_pick_time'] = $product_info['min_pickup_time'];
		} else {
			$data['entry_minimum_pick_time'] = 900;
		}

		if (isset($this->request->post['entry_packaging_charge'])) {
			$data['entry_packaging_charge'] = $this->request->post['entry_packaging_charge'];
		} elseif (!empty($product_info)) {
			$data['entry_packaging_charge'] = $product_info['packaging_charge'];
		} else {
			$data['entry_packaging_charge'] = 0.00;
		}

		if (isset($this->request->post['entry_delivery_charge'])) {
			$data['entry_delivery_charge'] = $this->request->post['entry_delivery_charge'];
		} elseif (!empty($product_info)) {
			$data['entry_delivery_charge'] = $product_info['delivery_charge'];
		} else {
			$data['entry_delivery_charge'] = 0.00;
		}

		

		if (isset($this->request->post['entry_minimum_delivery_time'])) {
			$data['entry_minimum_delivery_time'] = $this->request->post['entry_minimum_delivery_time'];
		} elseif (!empty($product_info)) {
			$data['entry_minimum_delivery_time'] = $product_info['min_delivery_time'];
		} else {
			$data['entry_minimum_delivery_time'] = 1800;
		}

		if (isset($this->request->post['entry_minimum_order_value'])) {
			$data['entry_minimum_order_value'] = $this->request->post['entry_minimum_order_value'];
		} elseif (!empty($product_info)) {
			$data['entry_minimum_order_value'] = $product_info['min_order_value'];
		} else {
			$data['entry_minimum_order_value'] = 200;
		}

		if (isset($this->request->post['entry_geo_longitude'])) {
			$data['entry_geo_longitude'] = $this->request->post['entry_geo_longitude'];
		} elseif (!empty($product_info)) {
			$data['entry_geo_longitude'] = $product_info['geo_longitude'];
		} else {
			$data['entry_geo_longitude'] = '';
		}

		if (isset($this->request->post['entry_geo_latitude'])) {
			$data['entry_geo_latitude'] = $this->request->post['entry_geo_latitude'];
		} elseif (!empty($product_info)) {
			$data['entry_geo_latitude'] = $product_info['geo_latitude'];
		} else {
			$data['entry_geo_latitude'] = '';
		}

		if (isset($this->request->post['store_all']) && !empty($this->request->post['store_all'])) {
			$data['store_all'] = $this->request->post['store_all'];
		} else if (!empty($store_info)) {
			$data['store_all'] = $store_info;
		} else {
			$data['store_all'] = array();
		}

		if (isset($this->request->post['platform_datas']) && !empty($this->request->post['platform_datas'])) {
			$data['platform_datas'] = $this->request->post['platform_datas'];
		} else if (!empty($platform_datas)) {
			$data['platform_datas'] = $platform_datas;
		} else {
			$data['platform_datas'] = array();
		}
		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/urbanpier_store_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/urbanpier_store')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (($this->request->post['entry_store_ref_id']) == '')  {
			$this->error['error_entry_store_ref_id'] = 'Please Fill Ref ID';
		}

		if (($this->request->post['entry_store_nme']) == '')  {
			$this->error['error_entry_store_nme'] = 'Please Fill Store Name';
		}

		if (($this->request->post['entry_store_add']) == '')  {
			$this->error['error_entry_store_add'] = 'Please Fill Store Address';
		}

		if (($this->request->post['entry_store_city']) == '')  {
			$this->error['error_entry_store_city'] = 'Please Fill Store City';
		}


		if (($this->request->post['entry_store_zipcode']) == '')  {
			$this->error['error_entry_store_zipcode'] = 'Please Fill Zipcode';
		}

		if (($this->request->post['entry_store_contact_phone']) == '')  {
			$this->error['error_entry_store_contact_phone_lbl'] = 'Please Fill Phone no';
		}


		if (($this->request->post['entry_store_notification_no']) == '')  {
			$this->error['error_entry_store_notification_no'] = 'Please Fill Notify no';
		}

		if (($this->request->post['notification_emails']) == '')  {
			$this->error['error_notification_emails'] = 'Please Fill Notify mail';
		}

		if (($this->request->post['entry_minimum_pick_time']) == '')  {
			$this->error['error_entry_minimum_pick_time'] = 'Please Fill Pickup time';
		}


		/*if (($this->request->post['entry_minimum_delivery_time']) == '')  {
			$this->error['error_entry_minimum_delivery_time'] = 'Please Fill Delivery time';
		}*/


		if (($this->request->post['entry_minimum_order_value']) == '')  {
			$this->error['error_entry_minimum_order_value'] = 'Please Fill Order Value';
		}

		if (($this->request->post['entry_packaging_charge']) == '')  {
			$this->error['error_entry_packaging_charge'] = 'Please Fill Packaging Charge';
		}
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/urbanpier_store')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateCopy() {
		if (!$this->user->hasPermission('modify', 'catalog/urbanpier_store')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model'])) {
			
			$this->load->model('catalog/option');

			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if (isset($this->request->get['filter_model'])) {
				$filter_model = $this->request->get['filter_model'];
			} else {
				$filter_model = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 5;
			}

			$filter_data = array(
				'filter_name'  => $filter_name,
				'filter_model' => $filter_model,
				'start'        => 0,
				'limit'        => $limit
			);

			$results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {
				$option_data = array();

				$product_options = $this->model_catalog_product->getProductOptions($result['product_id']);

				foreach ($product_options as $product_option) {
					$option_info = $this->model_catalog_option->getOption($product_option['option_id']);

					if ($option_info) {
						$product_option_value_data = array();

						foreach ($product_option['product_option_value'] as $product_option_value) {
							$option_value_info = $this->model_catalog_option->getOptionValue($product_option_value['option_value_id']);

							if ($option_value_info) {
								$product_option_value_data[] = array(
									'product_option_value_id' => $product_option_value['product_option_value_id'],
									'option_value_id'         => $product_option_value['option_value_id'],
									'name'                    => $option_value_info['name'],
									'price'                   => (float)$product_option_value['price'] ? $this->currency->format($product_option_value['price'], $this->config->get('config_currency')) : false,
									'price_prefix'            => $product_option_value['price_prefix']
								);
							}
						}

						$option_data[] = array(
							'product_option_id'    => $product_option['product_option_id'],
							'product_option_value' => $product_option_value_data,
							'option_id'            => $product_option['option_id'],
							'name'                 => $option_info['name'],
							'type'                 => $option_info['type'],
							'value'                => $product_option['value'],
							'required'             => $product_option['required']
						);
					}
				}

				$json[] = array(
					'product_id' => $result['product_id'],
					'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'model'      => $result['model'],
					'option'     => $option_data,
					'price'      => $result['price']
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function gettiming(){
		$json = array();
	 	if (isset($this->request->post) && $this->request->post['day_open_store'] != '') {
			$store_sql = $this->db->query("SELECT day FROM `oc_urbanpiper_store` WHERE day='".$this->request->post['day_open_store']."' ");
			if ($store_sql->num_rows > 0) {
				$this->db->query("UPDATE `oc_urbanpiper_store` SET
					day = '" . $this->request->post['day_open_store'] . "',
					start_time = '".$this->request->post['store_intime']."',
					end_time = '".$this->request->post['store_outtime']."' WHERE day ='".$store_sql->row['day']."'
				");
			} else {
				$this->db->query("INSERT INTO `oc_urbanpiper_store` SET
					day = '" . $this->request->post['day_open_store'] . "',
					start_time = '".$this->request->post['store_intime']."',
					end_time = '".$this->request->post['store_outtime']."'
				");
			}

			$horse_to_tr = $this->db->query("SELECT * FROM `oc_urbanpiper_store` WHERE 1=1 ");
			$html = '';
			foreach ($horse_to_tr->rows as $hkey => $hvalue) {
				$html .= '<tr >';
					$html .= '<td class="text-center">'.$hvalue['day'].'</td>';
		                $html .= '<td class="text-center">'.$hvalue['start_time'].'</td>';
		                $html .= '<td class="text-center">'.$hvalue['end_time'].'</td>';
	              	$html .= '<td class="text-center" >';
						$html .= '<a type="button" class="btn btn-danger"  onclick="delete_store_day('.$hvalue['id'].')" ><i class="fa fa-minus-circle"></i></a>';
					$html .= '</td>';
	            $html .= '</tr>';

			}
			$json['html'] = $html;
		} 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function deleterecord(){
		$json = array();
	 	if (isset($this->request->get) && $this->request->get['id'] != '') {
			$this->db->query("DELETE FROM `oc_urbanpiper_store` WHERE id ='".$this->request->get['id']."'");
			$urbanpiper_store = $this->db->query("SELECT * FROM `oc_urbanpiper_store` WHERE 1=1 ");
			$html = '';
			if($urbanpiper_store->num_rows > 0){
				foreach ($urbanpiper_store->rows as $hkey => $hvalue) {
					$html .= '<tr >';
						$html .= '<td class="text-center">'.$hvalue['day'].'</td>';
		                $html .= '<td class="text-center">'.$hvalue['start_time'].'</td>';
		                $html .= '<td class="text-center">'.$hvalue['end_time'].'</td>';
		              	$html .= '<td class="text-center" >';
							$html .= '<a type="button" class="btn btn-danger"  onclick="delete_store_day('.$hvalue['id'].')" ><i class="fa fa-minus-circle"></i></a>';
						$html .= '</td>';
		            $html .= '</tr>';

				}
			} else {
				$html .= '<tr>';
					$html .= '<td class="text-center" colspan="4">No Result</td>';
				$html .= '</tr>';
			}
			
			$json['html'] = $html;
		} 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function deleteplatform(){
        $json = array();
        if (isset($this->request->get) && $this->request->get['id'] != '') {
            $this->db->query("DELETE FROM `oc_urbanpiper_platform` WHERE id ='".$this->request->get['id']."'");
            $urbanpiper_store = $this->db->query("SELECT * FROM `oc_urbanpiper_platform` WHERE 1=1 ");
            $html = '';
            if($urbanpiper_store->num_rows > 0){
                foreach ($urbanpiper_store->rows as $hkey => $hvalue) {
                    $html .= '<tr >';
                        $html .= '<td class="text-center">'.$hvalue['platform_name'].'</td>';
                        $html .= '<td class="text-center">'.$hvalue['platform_link'].'</td>';
                        $html .= '<td class="text-center">'.$hvalue['platform_store_id'].'</td>';
                        $html .= '<td class="text-center" >';
                            $html .= '<a type="button" class="btn btn-danger"  onclick="deleteplatform('.$hvalue['id'].')" ><i class="fa fa-minus-circle"></i></a>';
                        $html .= '</td>';
                    $html .= '</tr>';

                }
            } else {
                $html .= '<tr>';
                    $html .= '<td class="text-center" colspan="4">No Result</td>';
                $html .= '</tr>';
            }
            
            $json['html'] = $html;
        } 
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getplatform(){
        $json = array();
        if (isset($this->request->post) && $this->request->post['platform_name'] != '') {
            $store_sql = $this->db->query("SELECT platform_name FROM `oc_urbanpiper_platform` WHERE platform_name='".$this->request->post['platform_name']."' ");
            if ($store_sql->num_rows > 0) {
                $this->db->query("UPDATE `oc_urbanpiper_platform` SET
                    platform_name = '" . $this->request->post['platform_name'] . "',
                    platform_link = '".$this->request->post['platform_link']."',
                    platform_store_id = '".$this->request->post['platform_store_id']."' WHERE platform_name ='".$store_sql->row['platform_name']."'
                ");
            } else {
                $this->db->query("INSERT INTO `oc_urbanpiper_platform` SET
                    platform_name = '" . $this->request->post['platform_name'] . "',
                    platform_link = '".$this->request->post['platform_link']."',
                    platform_store_id = '".$this->request->post['platform_store_id']."'
                ");
            }

            $horse_to_tr = $this->db->query("SELECT * FROM `oc_urbanpiper_platform` WHERE 1=1 ");
            $html = '';
            foreach ($horse_to_tr->rows as $hkey => $hvalue) {
                $html .= '<tr >';
                    $html .= '<td class="text-center">'.$hvalue['platform_name'].'</td>';
                        $html .= '<td class="text-center">'.$hvalue['platform_link'].'</td>';
                        $html .= '<td class="text-center">'.$hvalue['platform_store_id'].'</td>';
                    $html .= '<td class="text-center" >';
                        $html .= '<a type="button" class="btn btn-danger"  onclick="delete_platform('.$hvalue['id'].')" ><i class="fa fa-minus-circle"></i></a>';
                    $html .= '</td>';
                $html .= '</tr>';

            }
            $json['html'] = $html;
        } 
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
