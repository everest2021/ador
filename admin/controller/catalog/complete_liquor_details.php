<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
class Controllercatalogcompleteliquordetails extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/complete_liquor_details');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		// echo "<pre>";print_r($this->request->post);exit;
		$this->load->language('catalog/complete_liquor_details');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/complete_liquor_details', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_storename'])){
			$data['storename'] = $this->request->post['filter_storename'];
		}
		else{
			$data['storename'] = '';
		}

		$liquor_type = array();
		$liquor_name = array(); 
		$brand_sizes = array();
		$brand = array();
		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) && isset($this->request->post['filter_storename'])){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$storecode = $this->request->post['filter_storename'];

			$liquor_type = $this->db->query("SELECT type, type_id FROM `oc_brand` where `type` !='' GROUP BY `type`")->rows;
			
			$liquor_name = array();
			foreach($liquor_type as $lkey => $lvalue){
				$size = array();
				$liquor_name[$lvalue['type']] = $this->db->query("SELECT brand,type_id FROM `oc_brand` where `type`='".$lvalue['type']."' ")->rows;

				$brand_sizes[$lvalue['type']] = $this->db->query("SELECT * FROM `oc_brand_type` WHERE type_id = '".$lvalue['type_id']."' ")->rows;

				$size[$lvalue['type']] = $this->db->query("SELECT * FROM `oc_brand_type` WHERE type_id = '".$lvalue['type_id']."' ")->rows;
				foreach($liquor_name[$lvalue['type']] as $lnkey => $lnvalue){
					$purchase_qty = array();
					$sale_qty = array();
					$opening_qty = array();
					$closing_qty = array();
					foreach($size[$lvalue['type']] as $skey => $svalue){
					
						$purchase_qty[$svalue['size']] = $this->db->query("SELECT SUM(qty) as sqty, description, unit_id FROM oc_purchase_transaction pt LEFT JOIN oc_purchase_items_transaction it ON(pt.`id` = it.`p_id`) WHERE pt.`store_id` ='".$storecode."' AND pt.`invoice_date` >='".$start_date."' AND pt.`invoice_date` <='".$end_date."' AND it.`description` ='".$lnvalue['brand']."' AND it.`unit_id` ='".$svalue['id']."' AND it.`type` ='".$lvalue['type_id']."' ")->row;


						$newString = str_replace(" ","",$svalue['size']);
						$name_size = $lnvalue['brand'].' '.$newString;

						$sale_qty[$svalue['size']] = $this->db->query("SELECT SUM(qty) as sale_qty , oit.`name` FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oi.`bill_date`>='".$start_date."' AND oi.`bill_date` <='".$end_date."' AND oit.`name` ='".$name_size."' ")->row;

						$opening_qty[$svalue['size']] = $this->db->query("SELECT closing_balance FROM purchase_test WHERE purchase_size_id = '".$svalue['id']."' AND item_name = '".$lnvalue['brand']."' AND store_id = '".$storecode."' AND `invoice_date`>='".$start_date."' AND `invoice_date` <='".$end_date."' ")->row;
						
						$closing_qty[$svalue['size']]['closing_bal'] = ($opening_qty[$svalue['size']]['closing_balance'] + $purchase_qty[$svalue['size']]['sqty']) - $sale_qty[$svalue['size']]['sale_qty'];
						// echo "<pre>";print_r($opening_qty);
					}

					$brand[$lnvalue['brand']] = array(
						'pqty' => $purchase_qty,
						'sale_qtys' => $sale_qty,
						'opening_qtys' => $opening_qty,
						'closing_qtys' => $closing_qty,
					);
					// echo "<pre>";print_r($brand);
				} 
			} 
		}
		// echo "<pre>";print_r($brand);
		// exit;
		$data['liquor_type'] = $liquor_type;
		$data['liquor_name'] = $liquor_name;
		$data['size'] = $brand_sizes;
		$data['brand'] = $brand;
		$data['storenames'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Liquor'")->rows;

		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/complete_liquor_details', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/complete_liquor_details', $data));
	}

	public function export(){
		// echo "<pre>";print_r($this->request->get);exit;
		$this->load->model('catalog/order');
		$this->load->language('catalog/reportbill');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$data['enddate'] = $this->request->get['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->get['store_id'])){
			$data['store_id'] = $this->request->get['store_id'];
		}
		else{
			$data['store_id'] = '';
		}

		$liquor_type = array();
		$liquor_name = array(); 
		$brand_sizes = array();
		$brand = array();
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) && isset($this->request->get['store_id'])){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$storecode = $this->request->get['store_id'];

			// echo "<pre>";print_r($start_date);exit;

			$liquor_type = $this->db->query("SELECT type, type_id FROM `oc_brand` where `type` !='' GROUP BY `type`")->rows;
			
			$liquor_name = array();
			foreach($liquor_type as $lkey => $lvalue){
				$size = array();
				$liquor_name[$lvalue['type']] = $this->db->query("SELECT brand,type_id FROM `oc_brand` where `type`='".$lvalue['type']."' ")->rows;

				$brand_sizes[$lvalue['type']] = $this->db->query("SELECT * FROM `oc_brand_type` WHERE type_id = '".$lvalue['type_id']."' ")->rows;

				$size[$lvalue['type']] = $this->db->query("SELECT * FROM `oc_brand_type` WHERE type_id = '".$lvalue['type_id']."' ")->rows;
				foreach($liquor_name[$lvalue['type']] as $lnkey => $lnvalue){
					$purchase_qty = array();
					$sale_qty = array();
					$opening_qty = array();
					$closing_qty = array();
					foreach($size[$lvalue['type']] as $skey => $svalue){
					
						$purchase_qty[$svalue['size']] = $this->db->query("SELECT SUM(qty) as sqty, description, unit_id FROM oc_purchase_transaction pt LEFT JOIN oc_purchase_items_transaction it ON(pt.`id` = it.`p_id`) WHERE pt.`store_id` ='".$storecode."' AND pt.`invoice_date` >='".$start_date."' AND pt.`invoice_date` <='".$end_date."' AND it.`description` ='".$lnvalue['brand']."' AND it.`unit_id` ='".$svalue['id']."' AND it.`type` ='".$lvalue['type_id']."' ")->row;


						$newString = str_replace(" ","",$svalue['size']);
						$name_size = $lnvalue['brand'].' '.$newString;

						$sale_qty[$svalue['size']] = $this->db->query("SELECT SUM(qty) as sale_qty , oit.`name` FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oi.`bill_date`>='".$start_date."' AND oi.`bill_date` <='".$end_date."' AND oit.`name` ='".$name_size."' ")->row;

						$opening_qty[$svalue['size']] = $this->db->query("SELECT closing_balance FROM purchase_test WHERE purchase_size_id = '".$svalue['id']."' AND item_name = '".$lnvalue['brand']."' AND store_id = '".$storecode."' AND `invoice_date`>='".$start_date."' AND `invoice_date` <='".$end_date."' ")->row;
						
						$closing_qty[$svalue['size']]['closing_bal'] = ($opening_qty[$svalue['size']]['closing_balance'] + $purchase_qty[$svalue['size']]['sqty']) - $sale_qty[$svalue['size']]['sale_qty'];
						// echo "<pre>";print_r($opening_qty);
					}

					$brand[$lnvalue['brand']] = array(
						'pqty' => $purchase_qty,
						'sale_qtys' => $sale_qty,
						'opening_qtys' => $opening_qty,
						'closing_qtys' => $closing_qty,
					);
					// echo "<pre>";print_r($brand);
				} 
			} 
		}
		// echo "<pre>";print_r($brand);
		// exit;
		$data['liquor_type'] = $liquor_type;
		$data['liquor_name'] = $liquor_name;
		$data['size'] = $brand_sizes;
		$data['brand'] = $brand;
		$data['storenames'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Liquor'")->rows;

		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/complete_liquor_details', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/complete_liquor_details', $data));

		// echo "<pre>";
		// print_r($data);
		// exit;
		$html = $this->load->view('sale/complete_liquer_details_html', $data);
		
		if($this->request->get['type'] == 'html'){
			$filename = 'complete_liquor_details.html';
			header('Content-disposition: attachment; filename=' . $filename);
			header('Content-type: text/html');
			echo $html;exit;
		} else {
			$filename = "complete_liquor_details";
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
		}

	}
}