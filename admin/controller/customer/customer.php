<?php
class ControllerCustomerCustomer extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('customer/customer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/customer');

		$this->getList();
	}

	public function add() {
		$this->load->language('customer/customer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$customer_id = $this->model_customer_customer->addCustomer($this->request->post);

			$customer_datas = $this->db->query("SELECT * FROM `oc_customer` WHERE `customer_id` = '".$customer_id."' ");
			if($customer_datas->num_rows > 0){	
				$customer_data = $customer_datas->row;
				$exist_customer_datas = $this->db->query("SELECT * FROM `oc_customer` WHERE `telephone` = '".$customer_data['telephone']."' AND `customer_id` <> '".$customer_data['customer_id']."' AND active_status = '1' ");
				if($exist_customer_datas->num_rows == 0){
					// $customer_data['firstname'] = 'Digamber';
					// $customer_data['middlename'] = 'Narayan';
					// $customer_data['lastname'] = 'Patil';
					$username = 'dindia';
					$password = 'DIGITALINDIA007';
					$senderid = 'PaYYou';
					if($customer_data['middlename'] != ''){
						$student_name = $customer_data['firstname'].'%20'.$customer_data['middlename'].'%20'.$customer_data['lastname'];
					} else {
						$student_name = $customer_data['firstname'].'%20'.$customer_data['lastname'];
					}
					$message = 'Hi,%20'.$student_name.',%20Thank%20you%20for%20registering%20at%20NM%20Kala%20Krida%202017.%20for%20more%20information%20visit%20vvcs.in';
					//$number = '9096147809';
					//$number = '9987760244';
					$number = $customer_data['telephone'];
					$url = 'http://sms.thedigitalindia.com/api/sendsms?username='.$username.'&password='.$password.'&senderid='.$senderid.'&message='.$message.'&numbers='.$number.'&dndrefund=1';
					// echo $url;
					// echo '<br />';
					// exit;
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$output = curl_exec($ch);
					if($output === false){
						$this->log->write('Curl error: ' . curl_error($ch));
						//echo 'Curl error: ' . curl_error($ch);
						//echo '<br />';
					} else {
						$this->log->write('Message Sent');
						// 	echo 'Operation completed without any errors';
						// 	echo '<br />';	
					}
					curl_close($ch);
					// echo $url;
					// echo '<br />';
					// $json_decode = json_decode($output);
					// echo '<pre>';
					// print_r($output);
					// echo '<pre>';
					// print_r($json_decode);
					// exit;
				
					require_once(DIR_SYSTEM . 'library/PHPMailer/PHPMailerAutoload.php');
					require_once(DIR_SYSTEM . 'library/PHPMailer/class.phpmailer.php');
					require_once(DIR_SYSTEM . 'library/PHPMailer/class.smtp.php');

					$mail = new PHPMailer();
					//$mail->SMTPDebug = 3;
					//$mail->isMail(); // Set mailer to use SMTP
					//$mail->Host = 'localhost'; 
					if($customer_data['middlename'] != ''){
						$student_name = $customer_data['firstname'].' '.$customer_data['middlename'].' '.$customer_data['lastname'];
					} else {
						$student_name = $customer_data['firstname'].' '.$customer_data['lastname'];
					}
					$message = 'Hi '.$student_name. ", \n\n";
					$message .= 'Thank you for registering at NM Kala Krida. for more information visit vvcs.in';
					$subject = 'NM Kala Krida Registration';
					$from_email = 'fargose.aaron@gmail.com';
					//$to_emails = 'fargose.aaron@gmail.com,eric.fargose@gmail.com';
					$to_emails = $customer_data['email'];
					$to_emails_array = array();
					if($to_emails != ''){
						$to_emails_array = explode(',', $to_emails);
					}
					// echo '<pre>';
					// print_r($to_emails_array);
					// exit;
					//$to_email = 'fargose.aaron@gmail.com';
					$mail->IsSMTP();
					$mail->SMTPAuth = true;
					$mail->Host = 'localhost';//$this->config->get('config_smtp_host');
					$mail->Port = '587';//$this->config->get('config_smtp_port');
					$mail->Username = 'sportreg2017@sportsbasket.in';//$this->config->get('config_smtp_username');
					$mail->Password = 'sportregist2017';//$this->config->get('config_smtp_password');
					$mail->SMTPSecure = false;
					$mail->smtpConnect([
					    'ssl' => [
					        'verify_peer' => false,
					        'verify_peer_name' => false,
					        'allow_self_signed' => true
					    ]
					]);
					$mail->SetFrom('sportreg2017@sportsbasket.in', 'NM Kala Krida 2017');
					$mail->Subject = $subject;
					//$mail->isHTML(true);
					//$mail->MsgHTML($invoice_mail_text);
					$mail->Body = html_entity_decode($message);
					//$mail->addAttachment($file_path, $filename);
					foreach($to_emails_array as $ekey => $evalue){
						//$to_name = 'Aaron Fargose';
						$mail->AddAddress($evalue);
					}
					if($mail->Send()) {
						$this->log->write('Mail Sent');
					} else {
						$this->log->write('Mail Not Sent');
						$this->log->write(print_r($mail->ErrorInfo, true));
					}
				}
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_school'])) {
				$url .= '&filter_school=' . urlencode(html_entity_decode($this->request->get['filter_school'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_standard'])) {
				$url .= '&filter_standard=' . urlencode(html_entity_decode($this->request->get['filter_standard'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . urlencode(html_entity_decode($this->request->get['filter_sport_type'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_sport'])) {
				$url .= '&filter_sport=' . urlencode(html_entity_decode($this->request->get['filter_sport'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('customer/customer', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('customer/customer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_customer_customer->editCustomer($this->request->get['customer_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_school'])) {
				$url .= '&filter_school=' . urlencode(html_entity_decode($this->request->get['filter_school'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_standard'])) {
				$url .= '&filter_standard=' . urlencode(html_entity_decode($this->request->get['filter_standard'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . urlencode(html_entity_decode($this->request->get['filter_sport_type'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_sport'])) {
				$url .= '&filter_sport=' . urlencode(html_entity_decode($this->request->get['filter_sport'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('customer/customer', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('customer/customer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/customer');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $customer_id) {
				$this->model_customer_customer->deleteCustomer($customer_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_school'])) {
				$url .= '&filter_school=' . urlencode(html_entity_decode($this->request->get['filter_school'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_standard'])) {
				$url .= '&filter_standard=' . urlencode(html_entity_decode($this->request->get['filter_standard'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . urlencode(html_entity_decode($this->request->get['filter_sport_type'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_sport'])) {
				$url .= '&filter_sport=' . urlencode(html_entity_decode($this->request->get['filter_sport'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('customer/customer', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_school'])) {
			$filter_school = $this->request->get['filter_school'];
		} else {
			$filter_school = null;
		}

		if (isset($this->request->get['filter_standard'])) {
			$filter_standard = $this->request->get['filter_standard'];
		} else {
			$filter_standard = null;
		}

		if (isset($this->request->get['filter_sport_type'])) {
			$filter_sport_type = $this->request->get['filter_sport_type'];
		} else {
			$filter_sport_type = null;
		}

		if (isset($this->request->get['filter_sport'])) {
			$filter_sport = $this->request->get['filter_sport'];
		} else {
			$filter_sport = null;
		}

		if (isset($this->request->get['filter_participant_type'])) {
			$filter_participant_type = $this->request->get['filter_participant_type'];
		} else {
			$filter_participant_type = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = '1';
		}

		//echo $filter_status;exit;

		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'c.date_added';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_school'])) {
			$url .= '&filter_school=' . urlencode(html_entity_decode($this->request->get['filter_school'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_standard'])) {
			$url .= '&filter_standard=' . urlencode(html_entity_decode($this->request->get['filter_standard'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_sport_type'])) {
			$url .= '&filter_sport_type=' . urlencode(html_entity_decode($this->request->get['filter_sport_type'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_participant_type'])) {
			$url .= '&filter_participant_type=' . urlencode(html_entity_decode($this->request->get['filter_participant_type'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_sport'])) {
			$url .= '&filter_sport=' . urlencode(html_entity_decode($this->request->get['filter_sport'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('customer/customer', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('customer/customer/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('customer/customer/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['generate_code'] = $this->url->link('customer/customer/generate_code', 'token=' . $this->session->data['token'] . $url, true);
		$data['export_code'] = $this->url->link('customer/customer/export_code', 'token=' . $this->session->data['token'] . $url, true);
		$data['export'] = $this->url->link('customer/customer/export', 'token=' . $this->session->data['token'] . $url, true);

		$data['customers'] = array();

		$filter_data = array(
			'filter_name'              => $filter_name,
			'filter_school'            => $filter_school,
			'filter_standard' 		   => $filter_standard,
			'filter_sport_type' 	   => $filter_sport_type,
			'filter_participant_type'  => $filter_participant_type,
			'filter_sport' 		   	   => $filter_sport,
			'filter_status'            => $filter_status,
			'filter_date_added'        => $filter_date_added,
			'sort'                     => $sort,
			'order'                    => $order,
			'start'                    => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                    => $this->config->get('config_limit_admin')
		);

		$customer_total = $this->model_customer_customer->getTotalCustomers($filter_data);
		$results = $this->model_customer_customer->getCustomers($filter_data);

		foreach ($results as $result) {
			$data['customers'][] = array(
				'customer_id'    => $result['customer_id'],
				'name'           => $result['fullname'],
				'school'         => $result['school'],
				'standard' 		 => $result['standard'],
				'participant_type' => $result['participant_type'],
				'sport_type'     => $result['sport_type'],
				'sport'     	 => $result['sport'],
				'status'         => ($result['status'] ? 'सक्रिय / Active' : 'निष्क्रिय / Inactive'),
				'date_added'     => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'edit'           => $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, true)
			);
		}

		$school_namess = $this->db->query("SELECT * FROM `oc_school` WHERE 1=1 AND `status` = '1' ")->rows;
		$school_names['7777'] = 'Open';
		foreach($school_namess as $skey => $svalue){
			$school_names[$svalue['school_id']] = $svalue['name'];
		}
		$data['school_names'] = $school_names;
		// $data['school_names'] = array(
		// 	'1' => 'St Francis De Sales',
		// 	'2' => 'Maharashtra India',
		// 	'3' => 'JP Luddahani',
		// );

		$data['standards'] = array(
			'11' => 'Sr. Kg',
			'12' => 'Jr. Kg',
			'1' => 'I',
			'2' => 'II',
			'3' => 'III',
			'4' => 'IV',
			'5' => 'V',
			'6' => 'VI',
			'7' => 'VII',
			'8' => 'VIII',
			'9' => 'IX',
			'10' => 'X',
		);

		$data['sport_types'] = array(
			'1' => 'कला',
			'2' => 'क्रिडा',
		);

		$data['participant_types'] = array(
			'1' => 'वैयक्तिक स्पर्धा',
			'2' => 'सांघिक स्पर्धा',
		);

		// if($filter_sport_type == '1'){
		// 	$kala_sportss = $this->db->query("SELECT * FROM `oc_sport` WHERE `sport_type_id` = '1' ORDER BY `sport_type`")->rows;
		// 	foreach($kala_sportss as $skey => $svalue){
		// 		$in = 1;
		// 		if($filter_standard){
		// 			$is_exist = $this->db->query("SELECT * FROM `oc_sport_standard` WHERE `sport_id` = '".$svalue['sport_id']."' AND `standard_id` = '".$data['standards']."' ");
		// 			if($is_exist->num_rows == 0){
		// 				$in = 0;
		// 			}
		// 		}
		// 		if($in == 1){
		// 			$kala_sports[$svalue['sport_id']] = $svalue['name'];
		// 		}
		// 	}
		// 	$data['sports'] = $kala_sports;
		// 	// $data['sports'] = array(
		// 	// 	'1' => 'Drawing',
		// 	// 	'2' => 'Craft',
		// 	// 	'3' => 'Debate',
		// 	// );
		// } elseif($filter_sport_type == '2'){
		// 	$krida_sportss = $this->db->query("SELECT * FROM `oc_sport` WHERE `sport_type_id` = '2' ORDER BY `sport_type`")->rows;
		// 	foreach($krida_sportss as $skey => $svalue){
		// 		$in = 1;
		// 		if($filter_standard){
		// 			$is_exist = $this->db->query("SELECT * FROM `oc_sport_standard` WHERE `sport_id` = '".$svalue['sport_id']."' AND `standard_id` = '".$data['standards']."' ");
		// 			if($is_exist->num_rows == 0){
		// 				$in = 0;
		// 			}
		// 		}
		// 		if($in == 1){
		// 			$krida_sports[$svalue['sport_id']] = $svalue['name'];
		// 		}
		// 	}
		// 	$data['sports'] = $krida_sports;
		// 	// $data['sports'] = array(
		// 	// 	'1000' => 'Cricket',
		// 	// 	'1001' => 'Running',
		// 	// 	'1002' => 'Long Jump',
		// 	// );	
		// } else {
		// 	$sportss = $this->db->query("SELECT * FROM `oc_sport` WHERE 1=1 ORDER BY `sport_type`")->rows;
		// 	foreach($sportss as $skey => $svalue){
		// 		$in = 1;
		// 		if($filter_standard){
		// 			$is_exist = $this->db->query("SELECT * FROM `oc_sport_standard` WHERE `sport_id` = '".$svalue['sport_id']."' AND `standard_id` = '".$data['standards']."' ");
		// 			if($is_exist->num_rows == 0){
		// 				$in = 0;
		// 			}
		// 		}
		// 		if($in == 1){
		// 			$sports[$svalue['sport_id']] = $svalue['name'];
		// 		}
		// 	}
		// 	$data['sports'] = $sports;
		// 	// $data['sports'] = array(
		// 	// 	'1' => 'Drawing',
		// 	// 	'2' => 'Craft',
		// 	// 	'3' => 'Debate',
		// 	// 	'1000' => 'Cricket',
		// 	// 	'1001' => 'Running',
		// 	// 	'1002' => 'Long Jump',
		// 	// );
		// }

		$sql = "SELECT * FROM `oc_sport` WHERE 1=1 ";
		if($filter_sport_type){
			$sql .= " AND `sport_type_id` = '".$filter_sport_type."' ";
		}
		if($filter_participant_type){
			$sql .= " AND `participation_id` = '".$filter_participant_type."' ";
		}
		// if($data['dob']){
		// 	$sql .= " AND `birthdate_date_from` <= '".$data['dob']."' AND `birthdate_date_to` >= '".$data['dob']."' ";
		// }
		$sql .= " ORDER BY `sport_type` ";
		$sportss = $this->db->query($sql)->rows;
		foreach($sportss as $skey => $svalue){
			$in = 1;
			// if($data['standard'] != ''){
			// 	$is_exist = $this->db->query("SELECT * FROM `oc_sport_standard` WHERE `sport_id` = '".$svalue['sport_id']."' AND `standard_id` = '".$data['standards']."' ");
			// 	if($is_exist->num_rows == 0){
			// 		$in = 0;
			// 	}
			// }
			if($in == 1){
				$sports[$svalue['sport_id']] = $svalue['name'];
			}
		}
		$data['sports'] = $sports;

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_email'] = $this->language->get('column_email');
		$data['column_customer_group'] = $this->language->get('column_customer_group');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_approved'] = $this->language->get('column_approved');
		$data['column_ip'] = $this->language->get('column_ip');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_approved'] = $this->language->get('entry_approved');
		$data['entry_ip'] = $this->language->get('entry_ip');
		$data['entry_date_added'] = $this->language->get('entry_date_added');

		$data['button_approve'] = $this->language->get('button_approve');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_login'] = $this->language->get('button_login');
		$data['button_unlock'] = $this->language->get('button_unlock');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_school'])) {
			$url .= '&filter_school=' . urlencode(html_entity_decode($this->request->get['filter_school'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_standard'])) {
			$url .= '&filter_standard=' . urlencode(html_entity_decode($this->request->get['filter_standard'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_sport_type'])) {
			$url .= '&filter_sport_type=' . urlencode(html_entity_decode($this->request->get['filter_sport_type'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_participant_type'])) {
			$url .= '&filter_participant_type=' . urlencode(html_entity_decode($this->request->get['filter_participant_type'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_sport'])) {
			$url .= '&filter_sport=' . urlencode(html_entity_decode($this->request->get['filter_sport'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . '&sort=c.name' . $url, true);
		$data['sort_school'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . '&sort=c.school' . $url, true);
		$data['sort_standard'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . '&sort=c.standard' . $url, true);
		$data['sort_sport_type'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . '&sort=c.sport_type' . $url, true);
		$data['sort_participant_type'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . '&sort=c.participant_type' . $url, true);
		$data['sort_sport'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . '&sort=c.sport' . $url, true);
		$data['sort_status'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . '&sort=c.status' . $url, true);
		$data['sort_date_added'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . '&sort=c.date_added' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_school'])) {
			$url .= '&filter_school=' . urlencode(html_entity_decode($this->request->get['filter_school'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_standard'])) {
			$url .= '&filter_standard=' . urlencode(html_entity_decode($this->request->get['filter_standard'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_sport_type'])) {
			$url .= '&filter_sport_type=' . urlencode(html_entity_decode($this->request->get['filter_sport_type'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_sport'])) {
			$url .= '&filter_sport=' . urlencode(html_entity_decode($this->request->get['filter_sport'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_participant_type'])) {
			$url .= '&filter_participant_type=' . urlencode(html_entity_decode($this->request->get['filter_participant_type'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $customer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($customer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($customer_total - $this->config->get('config_limit_admin'))) ? $customer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $customer_total, ceil($customer_total / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_school'] = $filter_school;
		$data['filter_standard'] = $filter_standard;
		$data['filter_sport_type'] = $filter_sport_type;
		$data['filter_participant_type'] = $filter_participant_type;
		$data['filter_sport'] = $filter_sport;
		$data['filter_status'] = $filter_status;
		$data['filter_date_added'] = $filter_date_added;

		$this->load->model('customer/customer_group');

		$data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

		$this->load->model('setting/store');

		$data['stores'] = $this->model_setting_store->getStores();

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('customer/customer_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['customer_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_add_ban_ip'] = $this->language->get('text_add_ban_ip');
		$data['text_remove_ban_ip'] = $this->language->get('text_remove_ban_ip');

		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_fax'] = $this->language->get('entry_fax');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_confirm'] = $this->language->get('entry_confirm');
		$data['entry_newsletter'] = $this->language->get('entry_newsletter');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_approved'] = $this->language->get('entry_approved');
		$data['entry_safe'] = $this->language->get('entry_safe');
		$data['entry_company'] = $this->language->get('entry_company');
		$data['entry_address_1'] = $this->language->get('entry_address_1');
		$data['entry_address_2'] = $this->language->get('entry_address_2');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_postcode'] = $this->language->get('entry_postcode');
		$data['entry_zone'] = $this->language->get('entry_zone');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_default'] = $this->language->get('entry_default');
		$data['entry_comment'] = $this->language->get('entry_comment');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_amount'] = $this->language->get('entry_amount');
		$data['entry_points'] = $this->language->get('entry_points');

		$data['help_safe'] = $this->language->get('help_safe');
		$data['help_points'] = $this->language->get('help_points');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_address_add'] = $this->language->get('button_address_add');
		$data['button_history_add'] = $this->language->get('button_history_add');
		$data['button_transaction_add'] = $this->language->get('button_transaction_add');
		$data['button_reward_add'] = $this->language->get('button_reward_add');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_upload'] = $this->language->get('button_upload');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_address'] = $this->language->get('tab_address');
		$data['tab_history'] = $this->language->get('tab_history');
		$data['tab_transaction'] = $this->language->get('tab_transaction');
		$data['tab_reward'] = $this->language->get('tab_reward');
		$data['tab_ip'] = $this->language->get('tab_ip');

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['customer_id'])) {
			$data['customer_id'] = $this->request->get['customer_id'];
		} else {
			$data['customer_id'] = 0;
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} elseif(isset($this->session->data['warning'])){
			$data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['school'])) {
			$data['error_school'] = $this->error['school'];
		} else {
			$data['error_school'] = '';
		}

		if (isset($this->error['firstname'])) {
			$data['error_firstname'] = $this->error['firstname'];
		} else {
			$data['error_firstname'] = '';
		}

		if (isset($this->error['middlename'])) {
			$data['error_middlename'] = $this->error['middlename'];
		} else {
			$data['error_middlename'] = '';
		}

		if (isset($this->error['lastname'])) {
			$data['error_lastname'] = $this->error['lastname'];
		} else {
			$data['error_lastname'] = '';
		}

		if (isset($this->error['gender'])) {
			$data['error_gender'] = $this->error['gender'];
		} else {
			$data['error_gender'] = '';
		}

		if (isset($this->error['dob'])) {
			$data['error_dob'] = $this->error['dob'];
		} else {
			$data['error_dob'] = '';
		}

		if (isset($this->error['day'])) {
			$data['error_day'] = $this->error['day'];
		} else {
			$data['error_day'] = '';
		}

		if (isset($this->error['month'])) {
			$data['error_month'] = $this->error['month'];
		} else {
			$data['error_month'] = '';
		}

		if (isset($this->error['year'])) {
			$data['error_year'] = $this->error['year'];
		} else {
			$data['error_year'] = '';
		}

		if (isset($this->error['age'])) {
			$data['error_age'] = $this->error['age'];
		} else {
			$data['error_age'] = '';
		}

		if (isset($this->error['partner_firstname'])) {
			$data['error_partner_firstname'] = $this->error['partner_firstname'];
		} else {
			$data['error_partner_firstname'] = '';
		}

		if (isset($this->error['partner_middlename'])) {
			$data['error_partner_middlename'] = $this->error['partner_middlename'];
		} else {
			$data['error_partner_middlename'] = '';
		}

		if (isset($this->error['partner_lastname'])) {
			$data['error_partner_lastname'] = $this->error['partner_lastname'];
		} else {
			$data['error_partner_lastname'] = '';
		}

		if (isset($this->error['partner_dob'])) {
			$data['error_partner_dob'] = $this->error['partner_dob'];
		} else {
			$data['error_partner_dob'] = '';
		}

		if (isset($this->error['partner_day'])) {
			$data['error_partner_day'] = $this->error['partner_day'];
		} else {
			$data['error_partner_day'] = '';
		}

		if (isset($this->error['partner_month'])) {
			$data['error_partner_month'] = $this->error['partner_month'];
		} else {
			$data['error_partner_month'] = '';
		}

		if (isset($this->error['partner_year'])) {
			$data['error_partner_year'] = $this->error['partner_year'];
		} else {
			$data['error_partner_year'] = '';
		}

		if (isset($this->error['partner_age'])) {
			$data['error_partner_age'] = $this->error['partner_age'];
		} else {
			$data['error_partner_age'] = '';
		}

		if (isset($this->error['standard'])) {
			$data['error_standard'] = $this->error['standard'];
		} else {
			$data['error_standard'] = '';
		}

		if (isset($this->error['participant_type'])) {
			$data['error_participant_type'] = $this->error['participant_type'];
		} else {
			$data['error_participant_type'] = '';
		}

		if (isset($this->error['sport_type'])) {
			$data['error_sport_type'] = $this->error['sport_type'];
		} else {
			$data['error_sport_type'] = '';
		}

		if (isset($this->error['sport'])) {
			$data['error_sport'] = $this->error['sport'];
		} else {
			$data['error_sport'] = '';
		}

		if (isset($this->error['weight'])) {
			$data['error_weight'] = $this->error['weight'];
		} else {
			$data['error_weight'] = '';
		}

		if (isset($this->error['po_datas'])) {
			$data['error_po_datas'] = $this->error['po_datas'];
		} else {
			$data['error_po_datas'] = array();
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

		if (isset($this->error['group_name'])) {
			$data['error_group_name'] = $this->error['group_name'];
		} else {
			$data['error_group_name'] = '';
		}

		if (isset($this->error['aadhar_card_number'])) {
			$data['error_aadhar_card_number'] = $this->error['aadhar_card_number'];
		} else {
			$data['error_aadhar_card_number'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_school'])) {
			$url .= '&filter_school=' . urlencode(html_entity_decode($this->request->get['filter_school'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_standard'])) {
			$url .= '&filter_standard=' . urlencode(html_entity_decode($this->request->get['filter_standard'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_sport_type'])) {
			$url .= '&filter_sport_type=' . urlencode(html_entity_decode($this->request->get['filter_sport_type'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_participant_type'])) {
			$url .= '&filter_participant_type=' . urlencode(html_entity_decode($this->request->get['filter_participant_type'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_sport'])) {
			$url .= '&filter_sport=' . urlencode(html_entity_decode($this->request->get['filter_sport'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('customer/customer', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['customer_id'])) {
			$data['action'] = $this->url->link('customer/customer/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $this->request->get['customer_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['customer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$customer_info = $this->model_customer_customer->getCustomer($this->request->get['customer_id']);
		}

		$this->load->model('customer/customer_group');

		$data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

		if (isset($this->request->post['customer_group_id'])) {
			$data['customer_group_id'] = $this->request->post['customer_group_id'];
		} elseif (!empty($customer_info)) {
			$data['customer_group_id'] = $customer_info['customer_group_id'];
		} else {
			$data['customer_group_id'] = $this->config->get('config_customer_group_id');
		}

		if (isset($this->request->post['school'])) {
			$data['school'] = $this->request->post['school'];
		} elseif (!empty($customer_info)) {
			$data['school'] = $customer_info['school_id'];
		} else {
			$data['school'] = '';
		}

		$school_namess = $this->db->query("SELECT * FROM `oc_school` WHERE 1=1 ")->rows;
		$school_names['7777'] = 'Open';
		foreach($school_namess as $skey => $svalue){
			$school_names[$svalue['school_id']] = $svalue['name'];
		}
		$data['school_names'] = $school_names;
		// $data['school_names'] = array(
		// 	'1' => 'St Francis De Sales',
		// 	'2' => 'Maharashtra India',
		// 	'3' => 'JP Luddahani',
		// );

		if (isset($this->request->post['firstname'])) {
			$data['firstname'] = $this->request->post['firstname'];
		} elseif (!empty($customer_info)) {
			$data['firstname'] = $customer_info['firstname'];
		} else {
			$data['firstname'] = '';
		}

		if (isset($this->request->post['middlename'])) {
			$data['middlename'] = $this->request->post['middlename'];
		} elseif (!empty($customer_info)) {
			$data['middlename'] = $customer_info['middlename'];
		} else {
			$data['middlename'] = '';
		}

		if (isset($this->request->post['lastname'])) {
			$data['lastname'] = $this->request->post['lastname'];
		} elseif (!empty($customer_info)) {
			$data['lastname'] = $customer_info['lastname'];
		} else {
			$data['lastname'] = '';
		}

		if (isset($this->request->post['gender'])) {
			$data['gender'] = $this->request->post['gender'];
		} elseif (!empty($customer_info)) {
			$data['gender'] = $customer_info['gender_id'];
		} else {
			$data['gender'] = '';
		}

		$data['genders'] = array(
			'1' => 'पुरुष / Male',
			'2' => 'स्त्री / Female',
		);

		if (isset($this->request->post['dob'])) {
			$data['dob'] = $this->request->post['dob'];
		} elseif (!empty($customer_info)) {
			$data['dob'] = $customer_info['dob'];
		} else {
			$data['dob'] = '';
		}

		if($data['dob'] != '0000-00-00' && $data['dob'] != '1970-01-01' && $data['dob'] != ''){
			$dob_split = explode('-', $data['dob']);
		} else {
			$dob_split = array();
		}

		if (isset($this->request->post['day'])) {
			$data['day'] = $this->request->post['day'];
		} elseif (!empty($customer_info)) {
			if(isset($dob_split[2])){
				$data['day'] = $dob_split[2];
			} else {
				$data['day'] = '';
			}
		} else {
			$data['day'] = '';
		}

		if (isset($this->request->post['month'])) {
			$data['month'] = $this->request->post['month'];
		} elseif (!empty($customer_info)) {
			if(isset($dob_split[1])){
				$data['month'] = $dob_split[1];
			} else {
				$data['month'] = '';
			}
		} else {
			$data['month'] = '';
		}

		if (isset($this->request->post['year'])) {
			$data['year'] = $this->request->post['year'];
		} elseif (!empty($customer_info)) {
			if(isset($dob_split[0])){
				$data['year'] = $dob_split[0];
			} else {
				$data['year'] = '';
			}
		} else {
			$data['year'] = '';
		}

		if (isset($this->request->post['age'])) {
			$data['age'] = $this->request->post['age'];
		} elseif (!empty($customer_info)) {
			$data['age'] = $customer_info['age'];
		} else {
			$data['age'] = '';
		}

		if (isset($this->request->post['weight'])) {
			$data['weight'] = $this->request->post['weight'];
		} elseif (!empty($customer_info)) {
			$data['weight'] = $customer_info['weight'];
		} else {
			$data['weight'] = '0';
		}

		if (isset($this->request->post['partner_firstname'])) {
			$data['partner_firstname'] = $this->request->post['partner_firstname'];
		} elseif (!empty($customer_info)) {
			$data['partner_firstname'] = $customer_info['partner_firstname'];
		} else {
			$data['partner_firstname'] = '';
		}

		if (isset($this->request->post['partner_middlename'])) {
			$data['partner_middlename'] = $this->request->post['partner_middlename'];
		} elseif (!empty($customer_info)) {
			$data['partner_middlename'] = $customer_info['partner_middlename'];
		} else {
			$data['partner_middlename'] = '';
		}

		if (isset($this->request->post['partner_lastname'])) {
			$data['partner_lastname'] = $this->request->post['partner_lastname'];
		} elseif (!empty($customer_info)) {
			$data['partner_lastname'] = $customer_info['partner_lastname'];
		} else {
			$data['partner_lastname'] = '';
		}

		if (isset($this->request->post['partner_dob'])) {
			$data['partner_dob'] = $this->request->post['partner_dob'];
		} elseif (!empty($customer_info)) {
			$data['partner_dob'] = $customer_info['partner_dob'];
		} else {
			$data['partner_dob'] = '';
		}

		if($data['partner_dob'] != '0000-00-00' && $data['partner_dob'] != '1970-01-01' && $data['partner_dob'] != ''){
			$partner_dob_split = explode('-', $data['partner_dob']);
		} else {
			$partner_dob_split = array();
		}

		if (isset($this->request->post['partner_day'])) {
			$data['partner_day'] = $this->request->post['partner_day'];
		} elseif (!empty($customer_info)) {
			if(isset($partner_dob_split[2])){
				$data['partner_day'] = $partner_dob_split[2];
			} else {
				$data['partner_day'] = '';
			}
		} else {
			$data['partner_day'] = '';
		}

		if (isset($this->request->post['partner_month'])) {
			$data['partner_month'] = $this->request->post['partner_month'];
		} elseif (!empty($customer_info)) {
			if(isset($partner_dob_split[1])){
				$data['partner_month'] = $partner_dob_split[1];
			} else {
				$data['partner_month'] = '';
			}
		} else {
			$data['partner_month'] = '';
		}

		if (isset($this->request->post['partner_year'])) {
			$data['partner_year'] = $this->request->post['partner_year'];
		} elseif (!empty($customer_info)) {
			if(isset($partner_dob_split[0])){
				$data['partner_year'] = $partner_dob_split[0];
			} else {
				$data['partner_year'] = '';
			}
		} else {
			$data['partner_year'] = '';
		}

		if (isset($this->request->post['partner_age'])) {
			$data['partner_age'] = $this->request->post['partner_age'];
		} elseif (!empty($customer_info)) {
			$data['partner_age'] = $customer_info['partner_age'];
		} else {
			$data['partner_age'] = '';
		}

		$data['months'] = array(
			'01' => '01',
			'02' => '02',
			'03' => '03',
			'04' => '04',
			'05' => '05',
			'06' => '06',
			'07' => '07',
			'08' => '08',
			'09' => '09',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);

		$data['years'] = array();
		for($i = 2017; $i >= 1950; $i --){
			$data['years'][$i] = $i;
		}

		$data['days'] = array();
		for($i = 01; $i <= 31; $i ++){
			$i1 = sprintf("%02d", $i);
			$data['days'][$i1] = $i1;
		}

		if (isset($this->request->post['standard'])) {
			$data['standard'] = $this->request->post['standard'];
		} elseif (!empty($customer_info)) {
			$data['standard'] = $customer_info['standard_id'];
		} else {
			$data['standard'] = '';
		}

		$data['standards'] = array(
			'11' => 'Sr. Kg',
			'12' => 'Jr. Kg',
			'1' => 'I',
			'2' => 'II',
			'3' => 'III',
			'4' => 'IV',
			'5' => 'V',
			'6' => 'VI',
			'7' => 'VII',
			'8' => 'VIII',
			'9' => 'IX',
			'10' => 'X',
		);

		if (isset($this->request->post['participant_type'])) {
			$data['participant_type'] = $this->request->post['participant_type'];
		} elseif (!empty($customer_info)) {
			$data['participant_type'] = $customer_info['participant_type_id'];
		} else {
			$data['participant_type'] = '';
		}

		$data['participant_types'] = array(
			'1' => 'वैयक्तिक स्पर्धा',
			'2' => 'सांघिक स्पर्धा',
		);

		if (isset($this->request->post['sport_type'])) {
			$data['sport_type'] = $this->request->post['sport_type'];
		} elseif (!empty($customer_info)) {
			$data['sport_type'] = $customer_info['sport_type_id'];
		} else {
			$data['sport_type'] = '';
		}

		// echo $data['participant_type'];
		// echo '<br />';
		// echo $data['sport_type'];
		// exit;

		$data['sport_types'] = array(
			'1' => 'कला',
			'2' => 'क्रिडा',
		);


		if (isset($this->request->post['sport'])) {
			$data['sport'] = $this->request->post['sport'];
		} elseif (!empty($customer_info)) {
			$data['sport'] = $customer_info['sport_id'];
		} else {
			$data['sport'] = '';
		}

		if (isset($this->request->post['group_id'])) {
			$data['group_id'] = $this->request->post['group_id'];
		} elseif(!empty($customer_info)) {
			$data['group_id'] = $customer_info['group_id'];
		} else {
			$data['group_id'] = '';
		}

		if (isset($this->request->post['group_name'])) {
			$data['group_name'] = $this->request->post['group_name'];
			$data['group_id'] = $this->request->post['group_id'];
		} elseif (!empty($customer_info)) {
			if($customer_info['group_name'] == ''){
				$sport_group_sports = $this->db->query("SELECT `sport_group_id` FROM `oc_sport_group_sports` WHERE `sport_id` = '".$customer_info['sport_id']."' ");
				$group_name = '';
				$group_id = '';
				if($sport_group_sports->num_rows > 0){
					$sport_group_sport = $sport_group_sports->row;
					$sport_groups = $this->db->query("SELECT `sport_group`, `sport_group_id` FROM `oc_sport_group` WHERE `sport_group_id` = '".$sport_group_sport['sport_group_id']."' ");
					if($sport_groups->num_rows > 0){
						$sport_group = $sport_groups->row;
						$group_name = $sport_group['sport_group'];
						$group_id = $sport_group['sport_group_id'];
					}
				}
				$data['group_name'] = $group_name;
				$data['group_id'] = $group_id;
			} else {
				$data['group_name'] = $customer_info['group_name'];
				$data['group_id'] = $customer_info['group_id'];
			}
		} else {
			$data['group_name'] = '';
			$data['group_id'] = '';
		}

		if (isset($this->request->post['sports_groups'])) {
			$data['sports_groups'] = $this->request->post['sports_groups'];
		} else {
			$data['sports_groups'] = array();
		}

		if (isset($this->request->post['partner_required'])) {
			$data['partner_required'] = $this->request->post['partner_required'];
		} elseif (!empty($customer_info)) {
			$data['partner_required'] = $customer_info['partner_required'];
		} else {
			$data['partner_required'] = '0';
		}
		
		$data['hide_birth_date'] = 0;
		$data['edit'] = '0';
		if($data['participant_type'] == '2'){
			$data['edit'] = '1';
			$birth_date_hide = array('100','101', '102', '103', '88', '89', '90', '91', '92', '93');
			if(in_array($data['sport'], $birth_date_hide)){
				$data['hide_birth_date'] = 1;
			}
		}

		// $kala_sportss = $this->db->query("SELECT * FROM `oc_sport` WHERE `sport_type_id` = '1' ORDER BY `sport_type`")->rows;
		// foreach($kala_sportss as $skey => $svalue){
		// 	$in = 1;
		// 	if($data['standard'] != ''){
		// 		$is_exist = $this->db->query("SELECT * FROM `oc_sport_standard` WHERE `sport_id` = '".$svalue['sport_id']."' AND `standard_id` = '".$data['standard']."' ");
		// 		if($is_exist->num_rows == 0){
		// 			$in = 0;
		// 		}
		// 	}
		// 	if($in == 1){
		// 		$kala_sports[$svalue['sport_id']] = $svalue['name'];
		// 	}
		// }
		// $data['kala_sports'] = $kala_sports;		
		// // $data['kala_sports'] = array(
		// // 	'1' => 'Drawing',
		// // 	'2' => 'Craft',
		// // 	'3' => 'Debate',
		// // );

		// $krida_sportss = $this->db->query("SELECT * FROM `oc_sport` WHERE `sport_type_id` = '2' ORDER BY `sport_type`")->rows;
		// foreach($krida_sportss as $skey => $svalue){
		// 	$in = 1;
		// 	if($data['standard'] != ''){
		// 		$is_exist = $this->db->query("SELECT * FROM `oc_sport_standard` WHERE `sport_id` = '".$svalue['sport_id']."' AND `standard_id` = '".$data['standard']."' ");
		// 		if($is_exist->num_rows == 0){
		// 			$in = 0;
		// 		}
		// 	}
		// 	if($in == 1){
		// 		$krida_sports[$svalue['sport_id']] = $svalue['name'];
		// 	}
		// }
		// $data['krida_sports'] = $krida_sports;		
		// $data['krida_sports'] = array(
		// 	'1000' => 'Cricket',
		// 	'1001' => 'Running',
		// 	'1002' => 'Long Jump',
		// );
		/*
		if($data['participant_type'] == '2'){
			$sql = "SELECT * FROM `oc_sport` WHERE 1=1 ";
			if($data['sport_type']){
				$sql .= " AND `sport_type_id` = '".$data['sport_type']."' ";
			}
			if($data['participant_type']){
				$sql .= " AND `participation_id` = '".$data['participant_type']."' ";
			}
			// if($data['dob']){
			// 	$sql .= " AND `birthdate_date_from` <= '".$data['dob']."' AND `birthdate_date_to` >= '".$data['dob']."' ";
			// }
			$sql .= " ORDER BY `sport_type` ";
			//echo $sql;exit;
			$sportss = $this->db->query($sql)->rows;
			foreach($sportss as $skey => $svalue){
				$sports[$svalue['sport_id']] = $svalue['name'];
			}
			$data['sports'] = $sports;
		} else {
		*/

		$sports_groups = array();
		$sql = "SELECT * FROM `oc_sport` WHERE 1=1 ";
		if($data['sport_type']){
			$sql .= " AND `sport_type_id` = '".$data['sport_type']."' ";
		}
		if($data['participant_type']){
			$sql .= " AND `participation_id` = '".$data['participant_type']."' ";
		}
		// if($data['dob']){
		// 	$sql .= " AND `birthdate_date_from` <= '".$data['dob']."' AND `birthdate_date_to` >= '".$data['dob']."' ";
		// }
		$sql .= " ORDER BY `sport_type` ";
		//echo $sql;exit;
		$sportss = $this->db->query($sql)->rows;
		$sports = array();
		foreach($sportss as $skey => $svalue){
			$in = 0;
			if($data['participant_type'] == '1' || $data['participant_type'] == ''){
				if($data['dob'] != '' && $data['dob'] != '0000-00-00'){
					$sport_groups = $this->db->query("SELECT `sport_group_id`, `sport_group` FROM `oc_sport_group` WHERE `birthdate_date_from` <= '".$data['dob']."' AND `birthdate_date_to` >= '".$data['dob']."' AND `sport_type_id` = '".$svalue['sport_type_id']."' AND `participation_id` = '".$svalue['participation_id']."' ");
					//$this->log->write("SELECT `sport_group_id` FROM `oc_sport_group` WHERE `birthdate_date_from` <= '".$filter_dob."' AND `birthdate_date_to` >= '".$filter_dob."' AND `sport_type_id` = '".$svalue['sport_type_id']."' ");
					if($sport_groups->num_rows > 0){
						foreach($sport_groups->rows as $sgkey => $sgvalue){
							$sport_group_sports = $this->db->query("SELECT * FROM `oc_sport_group_sports` WHERE `sport_group_id` = '".$sgvalue['sport_group_id']."' AND `sport_id` = '".$svalue['sport_id']."' ");
							//$this->log->write("SELECT * FROM `oc_sport_group_sports` WHERE `sport_group_id` = '".$sgvalue['sport_group_id']."' AND `sport_id` = '".$svalue['sport_id']."' ");
							if($sport_group_sports->num_rows == 0){
								if($in == 0){
									$in = 0;
								}
							} else {
								$sport_group = $sgvalue['sport_group'];
								$group_id = $sgvalue['sport_group_id'];
								$in = 1;
							}
							if(!isset($sports_groups[$sgvalue['sport_group_id']])){
								if(empty($sports_groups)){
									$selected_group_name = $sgvalue['sport_group'];
									$selected_group_id = $sgvalue['sport_group_id'];
								}
								$sports_groups[$sgvalue['sport_group_id']] = $sgvalue['sport_group']; 
								// $sports_groups[$sgvalue['sport_group_id']] = array(
								// 	'sport_group_id' => $sgvalue['sport_group_id'],
								// 	'sport_group' => $sgvalue['sport_group'],
								// );
							}
						}
					} else {
						$in = 0;
					}
				}
			} else {
				if($data['sport'] != ''){
					$sport_group_sports = $this->db->query("SELECT `sport_group_id` FROM `oc_sport_group_sports` WHERE `sport_id` = '".$data['sport']."' ");
					if($sport_group_sports->num_rows > 0){
						$sport_group_sport = $sport_group_sports->rows;
						foreach($sport_group_sport as $sgkey => $sgvalue){				
							$sql = "SELECT `sport_group`, `sport_group_id` FROM `oc_sport_group` WHERE `sport_group_id` = '".$sgvalue['sport_group_id']."' ";
							if($data['participant_type']){
								$sql .= " AND `participation_id` = '".$data['participant_type']."' ";
							}
							if($data['sport_type']){
								$sql .= " AND `sport_type_id` = '".$data['sport_type']."' ";
							}
							$sport_groups = $this->db->query($sql);
							if($sport_groups->num_rows > 0){
								$sport_group = $sport_groups->row;
								if(!isset($sports_groups[$sport_group['sport_group_id']])){
									if(empty($sports_groups)){
										$selected_group_name = $sport_group['sport_group'];
										$selected_group_id = $sport_group['sport_group_id'];
									}
									$sports_groups[$sport_group['sport_group_id']] = $sport_group['sport_group'];
									// $sports_groups[$sport_group['sport_group_id']] = array(
									// 	'sport_group_id' => $sport_group['sport_group_id'],
									// 	'sport_group' => $sport_group['sport_group'],
									// );
								}
							}
						}
					}
				}
				$in = 1;
			}
			if($in == 1){
				$sports[$svalue['sport_id']] = $svalue['name'];
			}
		}
		$data['sports'] = $sports;
		//}
		$data['sports_groups'] = $sports_groups;		
		// echo '<pre>';
		// print_r($data['sports_groups']);
		// exit;
		// $data['sports'] = array(
		// 	'1' => 'Drawing',
		// 	'2' => 'Craft',
		// 	'3' => 'Debate',
		// 	'1000' => 'Cricket',
		// 	'1001' => 'Running',
		// 	'1002' => 'Long Jump',
		// );

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} elseif(!empty($customer_info)) {
			$data['email'] = $customer_info['email'];
		} else {
			$data['email'] = '';
		}

		if (isset($this->request->post['aadhar_card_number'])) {
			$data['aadhar_card_number'] = $this->request->post['aadhar_card_number'];
		} elseif(!empty($customer_info)) {
			$data['aadhar_card_number'] = $customer_info['aadhar_card_number'];
		} else {
			$data['aadhar_card_number'] = '';
		}

		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} elseif (!empty($customer_info)) {
			$data['telephone'] = $customer_info['telephone'];
		} else {
			$data['telephone'] = '';
		}

		if (isset($this->request->post['po_datas'])) {
			$data['po_datas'] = $this->request->post['po_datas'];
		} elseif (!empty($customer_info)) {
			$po_datas = array();
			for($i=0; $i<=15;$i++){
				$customer_participants = $this->db->query("SELECT * FROM `oc_customer_participant` WHERE `customer_id` = '".$this->request->get['customer_id']."' AND `inc_id` = '".$i."' ");
				if($customer_participants->num_rows > 0){
					$customer_participant = $customer_participants->row;
					if($customer_participant['dob'] != '0000-00-00' && $customer_participant['dob'] != '1970-01-01' && $customer_participant['dob'] != ''){
						$dob_split = explode('-', $customer_participant['dob']);
						$day = $dob_split[2];
						$month = $dob_split[1];
						$year = $dob_split[0];
					} else {
						$dob_split = array();
						$year = '';
						$month = '';
						$day = '';
					}
					$po_datas[] = array(
						'firstname' => $customer_participant['firstname'],
						'middlename' => $customer_participant['middlename'],
						'lastname' => $customer_participant['lastname'],
						'day' => $day,
						'month' => $month,
						'year' => $year,
						'age' => $customer_participant['age'],
						'weight' => $customer_participant['weight'],
						'aadhar_card_number' => $customer_participant['aadhar_card_number'],
						'inc_id' => $i,
					);
				} else {
					$po_datas[] = array(
						'firstname' => '',
						'middlename' => '',
						'lastname' => '',
						'day' => '',
						'month' => '',
						'year' => '',
						'age' => '',
						'weight' => 0,
						'aadhar_card_number' => '',
						'inc_id' => $i,
					);
				}
			}
			$data['po_datas'] = $po_datas;
		} else {
			$po_datas = array();
			for($i=1; $i<=10;$i++){
				$po_datas[] = array(
					'firstname' => '',
					'middlename' => '',
					'lastname' => '',
					'day' => '',
					'month' => '',
					'year' => '',
					'age' => '',
					'weight' => 0,
					'aadhar_card_number' => '',
					'inc_id' => $i,
				);
			}
			$data['po_datas'] = $po_datas;
		}

		// echo '<pre>';
		// print_r($po_datas);
		// exit;

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($customer_info)) {
			$data['status'] = $customer_info['status'];
		} else {
			$data['status'] = true;
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('customer/customer_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'customer/customer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if($this->request->post['participant_type'] == '2'){
			// if ($this->request->post['gender'] == '') {
			// 	$this->error['gender'] = 'Please Select Gender';
			// }

			if ((utf8_strlen($this->request->post['email']) < 1) || (utf8_strlen($this->request->post['email']) > 255)) {
				$this->error['email'] = 'Please Enter Email';
			} else {
				if(!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)){
					$this->error['email'] = 'Please Enter Valid Email';
				}
			}
		} else {
			if ((utf8_strlen($this->request->post['aadhar_card_number']) < 1) || (utf8_strlen($this->request->post['aadhar_card_number']) > 255)) {
				$this->error['aadhar_card_number'] = 'Please Enter Aadhar Card Number';
			}

			if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 255)) {
				$this->error['firstname'] = $this->language->get('error_firstname');
			}

			// if ((utf8_strlen(trim($this->request->post['middlename'])) < 1) || (utf8_strlen(trim($this->request->post['middlename'])) > 255)) {
			// 	$this->error['middlename'] = $this->language->get('error_middlename');
			// }

			if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 255)) {
				$this->error['lastname'] = $this->language->get('error_lastname');
			}

			if ($this->request->post['gender'] == '') {
				$this->error['gender'] = 'Please Select Gender';
			}

			// if ($this->request->post['dob'] == '' || $this->request->post['dob'] == '00-00-0000' || $this->request->post['dob'] == '01-01-1970') {
			// 	$this->error['dob'] = 'Please Select Date Of Birth';
			// }		

			if ($this->request->post['day'] == '') {
				$this->error['day'] = 'Please Select Day';
			}

			if ($this->request->post['month'] == '') {
				$this->error['month'] = 'Please Select Month';
			}

			if ($this->request->post['year'] == '') {
				$this->error['year'] = 'Please Select Year';
			}
		
			if($this->request->post['partner_required'] == '1'){
				if ((utf8_strlen(trim($this->request->post['partner_firstname'])) < 1) || (utf8_strlen(trim($this->request->post['partner_firstname'])) > 255)) {
					$this->error['partner_firstname'] = $this->language->get('error_firstname');
				}

				// if ((utf8_strlen(trim($this->request->post['partner_middlename'])) < 1) || (utf8_strlen(trim($this->request->post['partner_middlename'])) > 255)) {
				// 	$this->error['partner_middlename'] = $this->language->get('error_middlename');
				// }

				if ((utf8_strlen(trim($this->request->post['partner_lastname'])) < 1) || (utf8_strlen(trim($this->request->post['partner_lastname'])) > 255)) {
					$this->error['partner_lastname'] = $this->language->get('error_partner_lastname');
				}

				if ($this->request->post['partner_day'] == '') {
					$this->error['partner_day'] = 'Please Select Day';
				}

				if ($this->request->post['partner_month'] == '') {
					$this->error['partner_month'] = 'Please Select Month';
				}

				if ($this->request->post['partner_year'] == '') {
					$this->error['partner_year'] = 'Please Select Year';
				}
			}
		}

		if ((utf8_strlen($this->request->post['telephone']) < 10) || (utf8_strlen($this->request->post['telephone']) > 10)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

		if ($this->request->post['school'] == '') {
			$this->error['school'] = 'Please Select School';
		}

		if ($this->request->post['participant_type'] == '') {
			$this->error['participant_type'] = 'Please Select Participant Type';
		}

		// if ($this->request->post['standard'] == '') {
		// 	$this->error['standard'] = 'Please Select Standard';
		// }

		if ($this->request->post['sport_type'] == '') {
			$this->error['sport_type'] = 'Please Select Sport Type';
		} else {
			if ($this->request->post['sport'] == '') {
				$this->error['sport'] = 'Please Select Sport';	
			}
		}

		// echo '<pre>';
		// print_r($this->request->post);
		// exit;

		if($this->request->post['participant_type'] == '2'){
			$po_datas = $this->request->post['po_datas'];
			$birth_date_hide = array('100','101', '102', '103', '88', '89', '90', '91', '92', '93');
			if(in_array($this->request->post['sport'], $birth_date_hide)){
				$hide_birth_date = 1;
			} else {
				$hide_birth_date = 0;
			}
			foreach($po_datas as $pkey => $pvalue){
				if($pkey == 0){
					if((utf8_strlen(trim($pvalue['firstname'])) < 1)|| (utf8_strlen(trim($pvalue['lastname'])) < 1) ){
						$this->error['po_datas']['warning'] = 'Please Enter Group Data';
					} else {
						if((utf8_strlen(trim($pvalue['firstname'])) > 1) || (utf8_strlen(trim($pvalue['middlename'])) > 1) || (utf8_strlen(trim($pvalue['lastname'])) > 1) ){
							if ((utf8_strlen(trim($pvalue['firstname'])) < 1) || (utf8_strlen(trim($pvalue['firstname'])) > 255)) {
								$this->error['po_datas'][$pkey]['firstname'] = $this->language->get('error_firstname');
							}
							// if ((utf8_strlen(trim($pvalue['middlename'])) < 1) || (utf8_strlen(trim($pvalue['middlename'])) > 255)) {
							// 	$this->error['po_datas'][$pkey]['middlename'] = $this->language->get('error_middlename');
							// }
							if ((utf8_strlen(trim($pvalue['lastname'])) < 1) || (utf8_strlen(trim($pvalue['lastname'])) > 255)) {
								$this->error['po_datas'][$pkey]['lastname'] = $this->language->get('error_lastname');
							}
							if ((utf8_strlen(trim($pvalue['aadhar_card_number'])) < 1) || (utf8_strlen(trim($pvalue['aadhar_card_number'])) > 255)) {
								$this->error['po_datas'][$pkey]['aadhar_card_number'] = 'Please Enter Aadhar Card Number';
							}
							if($hide_birth_date == 0){
								$in = 0;
								if ($pvalue['day'] == '') {
									$in = 1;
									$this->error['po_datas'][$pkey]['day'] = 'Please Select Day';
								}
								if ($pvalue['month'] == '') {
									$in = 1;
									$this->error['po_datas'][$pkey]['month'] = 'Please Select Month';
								}
								if ($pvalue['year'] == '') {
									$in = 1;
									$this->error['po_datas'][$pkey]['year'] = 'Please Select Year';
								}
								if($in == 0){
									$dob = $pvalue['year'].'-'.$pvalue['month'].'-'.$pvalue['day'];
									$sport_group_sports = $this->db->query("SELECT `sport_group_id` FROM `oc_sport_group_sports` WHERE `sport_id` = '".$this->request->post['sport']."' ");
									if($sport_group_sports->num_rows > 0){
										$sport_group_sport = $sport_group_sports->row;
										$sport_groups = $this->db->query("SELECT `sport_group`, `sport_group_id` FROM `oc_sport_group` WHERE `sport_group_id` = '".$sport_group_sport['sport_group_id']."' AND `birthdate_date_from` <= '".$dob."' AND `birthdate_date_to` >= '".$dob."' ");
										if($sport_groups->num_rows == 0){
											$po_in = 1;
											$this->error['po_datas'][$pkey]['day'] = 'Selected Sport Not Applicable for your Age';
										}
									}
								}
							}				
						}
					}
				} else {
					if((utf8_strlen(trim($pvalue['firstname'])) > 1) || (utf8_strlen(trim($pvalue['lastname'])) > 1) ){
						if ((utf8_strlen(trim($pvalue['firstname'])) < 1) || (utf8_strlen(trim($pvalue['firstname'])) > 255)) {
							$this->error['po_datas'][$pkey]['firstname'] = $this->language->get('error_firstname');
						}
						// if ((utf8_strlen(trim($pvalue['middlename'])) < 1) || (utf8_strlen(trim($pvalue['middlename'])) > 255)) {
						// 	$this->error['po_datas'][$pkey]['middlename'] = $this->language->get('error_middlename');
						// }
						if ((utf8_strlen(trim($pvalue['lastname'])) < 1) || (utf8_strlen(trim($pvalue['lastname'])) > 255)) {
							$this->error['po_datas'][$pkey]['lastname'] = $this->language->get('error_lastname');
						}
						if ((utf8_strlen(trim($pvalue['aadhar_card_number'])) < 1) || (utf8_strlen(trim($pvalue['aadhar_card_number'])) > 255)) {
							$this->error['po_datas'][$pkey]['aadhar_card_number'] = 'Please Enter Aadhar Card Number';
						}
						if($hide_birth_date == 0){
							$in = 0;
							if ($pvalue['day'] == '') {
								$in = 1;
								$this->error['po_datas'][$pkey]['day'] = 'Please Select Day';
							}
							if ($pvalue['month'] == '') {
								$in = 1;
								$this->error['po_datas'][$pkey]['month'] = 'Please Select Month';
							}
							if ($pvalue['year'] == '') {
								$in = 1;
								$this->error['po_datas'][$pkey]['year'] = 'Please Select Year';
							}
							if($in == 0){
								$dob = $pvalue['year'].'-'.$pvalue['month'].'-'.$pvalue['day'];
								$sport_group_sports = $this->db->query("SELECT `sport_group_id` FROM `oc_sport_group_sports` WHERE `sport_id` = '".$this->request->post['sport']."' ");
								if($sport_group_sports->num_rows > 0){
									$sport_group_sport = $sport_group_sports->row;
									$sport_groups = $this->db->query("SELECT `sport_group`, `sport_group_id` FROM `oc_sport_group` WHERE `sport_group_id` = '".$sport_group_sport['sport_group_id']."' AND `birthdate_date_from` <= '".$dob."' AND `birthdate_date_to` >= '".$dob."' ");
									if($sport_groups->num_rows == 0){
										$po_in = 1;
										$this->error['po_datas'][$pkey]['day'] = 'Selected Sport Not Applicable for your Age';
									}
								}
							}
						}				
					}
				}
			}

			// if($this->request->post['total_weight'] > $this->request->post['weight']){
			// 	$this->error['weight'] = 'Weight Entered Exceeded Max Weight Limit For this Sport';
			// }
		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'customer/customer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_email'])) {
			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if (isset($this->request->get['filter_email'])) {
				$filter_email = $this->request->get['filter_email'];
			} else {
				$filter_email = '';
			}

			$this->load->model('customer/customer');

			$filter_data = array(
				'filter_name'  => $filter_name,
				'filter_email' => $filter_email,
				'start'        => 0,
				'limit'        => 5
			);

			$results = $this->model_customer_customer->getCustomers($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'customer_id'       => $result['customer_id'],
					'name'              => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'firstname'         => $result['firstname'],
					'lastname'          => $result['lastname'],
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function address() {
		$json = array();

		if (!empty($this->request->get['address_id'])) {
			$this->load->model('customer/customer');

			$json = $this->model_customer_customer->getAddress($this->request->get['address_id']);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getsports() {
		$json = array();
		$sport_datas = array();
		$sports_groups = array();
		$selected_group_name = '';
		$selected_group_id = '';
		if (isset($this->request->get['filter_sport_type'])) {
			$data['filter_sport_type'] = $this->request->get['filter_sport_type'];
			$data['filter_participant_type'] = $this->request->get['filter_participant_type'];
			$data['filter_dob'] = $this->request->get['filter_dob'];
			
			$filter_sport_type = $this->request->get['filter_sport_type'];
			$filter_participant_type = $this->request->get['filter_participant_type'];
			$filter_dob = $this->request->get['filter_dob'];
			
			$sql = "SELECT * FROM `oc_sport` WHERE 1=1 ";
			if($filter_sport_type){
				$sql .= " AND `sport_type_id` = '".$filter_sport_type."' ";
			}
			if($filter_participant_type){
				$sql .= " AND `participation_id` = '".$filter_participant_type."' ";
			}
			// if($filter_dob){
			// 	$sql .= " AND `birthdate_date_from` <= '".$filter_dob."' AND `birthdate_date_to` >= '".$filter_dob."' ";
			// }
			$sql .= " ORDER BY `sport_type` ";
			//$this->log->write($sql);
			$sportss = $this->db->query($sql)->rows;
			$sport_datas[] = array(
				'sport_name' => '---Please Select---',
				'sport_id' => '',
			);
			foreach($sportss as $skey => $svalue){
				$in = 0;
				$sport_group = '';
				$group_id = '';
				if($filter_participant_type == '2'){
					$in = 1;
				} else {
					if($filter_dob != '' && $filter_dob != '0000-00-00'){
						$sport_groups = $this->db->query("SELECT `sport_group_id`, `sport_group` FROM `oc_sport_group` WHERE `birthdate_date_from` <= '".$filter_dob."' AND `birthdate_date_to` >= '".$filter_dob."' AND `sport_type_id` = '".$svalue['sport_type_id']."' AND `participation_id` = '".$svalue['participation_id']."' ");
						//$this->log->write("SELECT `sport_group_id` FROM `oc_sport_group` WHERE `birthdate_date_from` <= '".$filter_dob."' AND `birthdate_date_to` >= '".$filter_dob."' AND `sport_type_id` = '".$svalue['sport_type_id']."' ");
						if($sport_groups->num_rows > 0){
							foreach($sport_groups->rows as $sgkey => $sgvalue){
								$sport_group_sports = $this->db->query("SELECT * FROM `oc_sport_group_sports` WHERE `sport_group_id` = '".$sgvalue['sport_group_id']."' AND `sport_id` = '".$svalue['sport_id']."' ");
								//$this->log->write("SELECT * FROM `oc_sport_group_sports` WHERE `sport_group_id` = '".$sgvalue['sport_group_id']."' AND `sport_id` = '".$svalue['sport_id']."' ");
								if($sport_group_sports->num_rows == 0){
									if($in == 0){
										$in = 0;
									}
								} else {
									$sport_group = $sgvalue['sport_group'];
									$group_id = $sgvalue['sport_group_id'];
									$in = 1;
								}
								if(!isset($sports_groups[$sgvalue['sport_group_id']])){
									if(empty($sports_groups)){
										$selected_group_name = $sgvalue['sport_group'];
										$selected_group_id = $sgvalue['sport_group_id'];
									}
									$sports_groups[$sgvalue['sport_group_id']] = array(
										'sport_group_id' => $sgvalue['sport_group_id'],
										'sport_group' => $sgvalue['sport_group'],
									);
								}
							}
						} else {
							$in = 0;
						}
					}
				}
				if($in == 1){
					$sport_datas[] = array(
						'sport_name' => $svalue['name'],
						'sport_id' => $svalue['sport_id'],
						'group_name' => $sport_group,
						'group_id' => $group_id,
					);
				}
			}
			//$results = $this->model_catalog_unit->getUnits($data);
			//foreach ($results as $result) {
				/*
				if($filter_sport_type == '1'){
					$sport_datas[] = array(
						'sport_name' => 'All',
						'sport_id' => '',
					);
					$sport_datas[] = array(
						'sport_name' => 'Drawing',
						'sport_id' => '1',
					);
					$sport_datas[] = array(
						'sport_name' => 'Craft',
						'sport_id' => '2',
					);
					$sport_datas[] = array(
						'sport_name' => 'Debate',
						'sport_id' => '3',
					);
				} else {
					$sport_datas[] = array(
						'sport_name' => 'All',
						'sport_id' => '',
					);
					$sport_datas[] = array(
						'sport_name' => 'Cricket',
						'sport_id' => '1000',
					);
					$sport_datas[] = array(
						'sport_name' => 'Running',
						'sport_id' => '1001',
					);
					$sport_datas[] = array(
						'sport_name' => 'Long Jump',
						'sport_id' => '1002',
					);
				}
				*/
			//}
		}
		$json['sport_datas'] = $sport_datas;
		$json['sports_groups'] = $sports_groups;
		$json['selected_group_name'] = $selected_group_name;
		$json['selected_group_id'] = $selected_group_id;
		$this->response->setOutput(json_encode($json));
	}

	public function getsportsdata() {
		$json = array();
		$weight = 0;
		$partner_required = 0;
		$hide_birth_date = 0;
		$sport_datas = array();
		$group_name = '';
		$group_id = '';
		$sports_groups = array();
		$selected_group_name = '';
		$selected_group_id = '';
		if (isset($this->request->get['filter_sport'])) {
			$data['filter_sport'] = $this->request->get['filter_sport'];
			$filter_sport = $this->request->get['filter_sport'];
			$sql = "SELECT * FROM `oc_sport` WHERE 1=1 ";
			if($filter_sport){
				$sql .= " AND `sport_id` = '".$filter_sport."' ";
			}
			$sql .= " ORDER BY `sport_id` ";
			//$this->log->write($sql);
			$sports = $this->db->query($sql)->row;
			$partner_required = $sports['partner_id'];
			$weight = $sports['weight'];
			
			$sport_group_sports = $this->db->query("SELECT `sport_group_id` FROM `oc_sport_group_sports` WHERE `sport_id` = '".$filter_sport."' ");
			//$this->log->write(print_r($sport_group_sports, true));
			if($sport_group_sports->num_rows > 0){
				$sport_group_sport = $sport_group_sports->rows;
				foreach($sport_group_sport as $sgkey => $sgvalue){				
					$sql = "SELECT `sport_group`, `sport_group_id` FROM `oc_sport_group` WHERE `sport_group_id` = '".$sgvalue['sport_group_id']."' ";
					if($filter_participant_type){
						$sql .= " AND `participation_id` = '".$filter_participant_type."' ";
					}
					if($filter_sport_type){
						$sql .= " AND `sport_type_id` = '".$filter_sport_type."' ";
					}
					$sport_groups = $this->db->query($sql);
					if($sport_groups->num_rows > 0){
						$sport_group = $sport_groups->row;
						$group_name = $sport_group['sport_group'];
						$group_id = $sport_group['sport_group_id'];
						
						if(!isset($sports_groups[$sport_group['sport_group_id']])){
							if(empty($sports_groups)){
								$selected_group_name = $sport_group['sport_group'];
								$selected_group_id = $sport_group['sport_group_id'];
							}
							$sports_groups[$sport_group['sport_group_id']] = array(
								'sport_group_id' => $sport_group['sport_group_id'],
								'sport_group' => $sport_group['sport_group'],
							);
						}
					}
				}
			}

			$birth_date_hide = array('100','101', '102', '103', '88', '89', '90', '91', '92', '93');
			if(in_array($filter_sport, $birth_date_hide)){
				$hide_birth_date = 1;
			}
		}
		$json['partner_required'] = $partner_required;
		$json['weight'] = $weight;
		$json['hide_birth_date'] = $hide_birth_date;
		$json['group_name'] = $group_name;
		$json['group_id'] = $group_id;
		$json['sports_groups'] = $sports_groups;
		$json['selected_group_name'] = $selected_group_name;
		$json['selected_group_id'] = $selected_group_id;
		$this->response->setOutput(json_encode($json));
	}

	public function getsports1() {
		$json = array();
		$sport_datas = array();
		if (isset($this->request->get['filter_sport_type'])) {
			$data['filter_sport_type'] = $this->request->get['filter_sport_type'];
			$data['filter_participant_type'] = $this->request->get['filter_participant_type'];
			$data['filter_dob'] = '';//$this->request->get['filter_dob'];
			
			$filter_sport_type = $this->request->get['filter_sport_type'];
			$filter_participant_type = $this->request->get['filter_participant_type'];
			$filter_dob = '';//$this->request->get['filter_dob'];
			
			$sql = "SELECT * FROM `oc_sport` WHERE 1=1 ";
			if($filter_sport_type){
				$sql .= " AND `sport_type_id` = '".$filter_sport_type."' ";
			}
			if($filter_participant_type){
				$sql .= " AND `participation_id` = '".$filter_participant_type."' ";
			}
			if($filter_dob){
				$sql .= " AND `birthdate_date_from` <= '".$filter_dob."' AND `birthdate_date_to` >= '".$filter_dob."' ";
			}
			$sql .= " ORDER BY `sport_type` ";
			//$this->log->write($sql);
			$sportss = $this->db->query($sql)->rows;
			$sport_datas[] = array(
				'sport_name' => 'All',
				'sport_id' => '',
			);
			foreach($sportss as $skey => $svalue){
				$in = 1;
				// if($filter_standard != ''){
				// 	$is_exist = $this->db->query("SELECT * FROM `oc_sport_standard` WHERE `sport_id` = '".$svalue['sport_id']."' AND `standard_id` = '".$filter_standard."' ");
				// 	if($is_exist->num_rows == 0){
				// 		$in = 0;
				// 	}
				// }
				if($in == 1){
					$sport_datas[] = array(
						'sport_name' => $svalue['name'],
						'sport_id' => $svalue['sport_id'],
					);
				}
			}
		}
		$json['sport_datas'] = $sport_datas;
		$this->response->setOutput(json_encode($json));
	}

	public function export_code() {
		$this->load->language('customer/customer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/customer');

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_school'])) {
			$filter_school = $this->request->get['filter_school'];
		} else {
			$filter_school = null;
		}

		if (isset($this->request->get['filter_standard'])) {
			$filter_standard = $this->request->get['filter_standard'];
		} else {
			$filter_standard = null;
		}

		if (isset($this->request->get['filter_sport_type'])) {
			$filter_sport_type = $this->request->get['filter_sport_type'];
		} else {
			$filter_sport_type = null;
		}

		if (isset($this->request->get['filter_sport'])) {
			$filter_sport = $this->request->get['filter_sport'];
		} else {
			$filter_sport = null;
		}

		if (isset($this->request->get['filter_participant_type'])) {
			$filter_participant_type = $this->request->get['filter_participant_type'];
		} else {
			$filter_participant_type = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = '1';
		}

		//echo $filter_status;exit;

		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = '';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array(
			'filter_name'              => $filter_name,
			'filter_school'            => $filter_school,
			'filter_standard' 		   => $filter_standard,
			'filter_sport_type' 	   => $filter_sport_type,
			'filter_participant_type'  => $filter_participant_type,
			'filter_sport' 		   	   => $filter_sport,
			'filter_status'            => $filter_status,
			'filter_date_added'        => $filter_date_added,
			'sort'                     => $sort,
			'order'                    => $order,
			'filter_export'            => 1,
			'start'                    => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                    => $this->config->get('config_limit_admin')
		);

		$results = $this->model_customer_customer->getCustomers($filter_data);
		$cnt = 1;
		foreach ($results as $result) {
			$customers[] = array(
				'Sr.No'    			=> $cnt,//$result['customer_id'],
				'School Name'       => $result['school'],
				'Participant Name'  => $result['fullname'],
				'Sport Type'  	    => $result['sport_type'],
				'Chest Number'  	=> $result['batch_id'],
			);
			$cnt ++;
		}

		if($customers){
			// $fp = fopen(DIR_DOWNLOAD . "Participant_Chest_Number.csv", "w"); 
			// $line = "";
			// $comma = "";
			// foreach($customers as $key => $values) {
			// 	foreach($values as $key => $value){
			// 		$line .= $comma . '"' . str_replace('"', '""', $key) . '"';
			// 		$comma = ",";
			// 	}
			// 	break;
			// }
			// //echo $line;exit;
			// $line .= "\n";
			// fputs($fp, $line);
			// $i = 1;
			// foreach($customers as $key => $value) {
			// 	$comma = "";
			// 	$line = "";
			// 	$line .= $comma . '"' . str_replace('"', '""', $value['Sr.No']) . '"';
			// 	$comma = ",";
			// 	$line .= $comma . '"' . str_replace('"', '""', $value['School Name']) . '"';
			// 	$line .= $comma . '"' . str_replace('"', '""', $value['Participant Name']) . '"';
			// 	$line .= $comma . '"' . str_replace('"', '""', $value['Chest Number']) . '"';
				
			// 	$line .= "\n";
			// 	//echo $line;exit;
			// 	fputs($fp, $line);
			// }
			// //echo $line;exit;
			// fclose($fp);
			// $filename = "Participant_Chest_Number.csv";
			// $file_path = DIR_DOWNLOAD . $filename;
			// $html = file_get_contents($file_path);
			$templates = array();
			$template = new Template('basic');		
			$templates['final_datas'] = $customers;
			$templates['title'] = 'Participant Chest Number';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$templates['base'] = HTTPS_SERVER;
			} else {
				$templates['base'] = HTTP_SERVER;
			}
			foreach ($templates as $key => $value) {
				$template->set($key, $value);
			}
			$html = $template->render('customer/customer_html.tpl');
			//echo $html;exit;
			$filename = "Participant_Chest_Number";
			header('Content-type: text/html');
			header('Content-Disposition: attachment; filename='.$filename.".html");
			echo $html;
			exit;
		} else {

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_school'])) {
				$url .= '&filter_school=' . urlencode(html_entity_decode($this->request->get['filter_school'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_standard'])) {
				$url .= '&filter_standard=' . urlencode(html_entity_decode($this->request->get['filter_standard'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . urlencode(html_entity_decode($this->request->get['filter_sport_type'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_participant_type'])) {
				$url .= '&filter_participant_type=' . urlencode(html_entity_decode($this->request->get['filter_participant_type'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_sport'])) {
				$url .= '&filter_sport=' . urlencode(html_entity_decode($this->request->get['filter_sport'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->session->data['warning'] = 'No Data Found';
			$this->response->redirect($this->url->link('customer/customer', 'token=' . $this->session->data['token'] . $url, true));
		}
	}

	public function generate_code() {
		$this->load->language('customer/customer');

		$this->document->setTitle('Generate Chest Code');

		$this->load->model('customer/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm1()) {
			$post_data = $this->request->post;
			$school_group = $this->db->query("SELECT `school_id` FROM `oc_customer` WHERE `status` = '1' AND `active_status` = '1' AND `sport_type_id` = '".$post_data['sport_type']."' AND `participant_type_id` = '1' GROUP BY `school_id`")->rows;
			
			$count = $post_data['start_number'];
			foreach($school_group as $sgkey => $sgvalue){
				$students_group_datas = $this->db->query("SELECT `fullname`, `telephone` FROM `oc_customer` WHERE `status` = '1' AND `active_status` = '1' AND `school_id` = '".$sgvalue['school_id']."' AND `sport_type_id` = '".$post_data['sport_type']."' AND `participant_type_id` = '1' GROUP BY `fullname`, `telephone` ASC ")->rows;
				foreach($students_group_datas as $skeys => $svalues){
					$students_datas = $this->db->query("SELECT `customer_id` FROM `oc_customer` WHERE `status` = '1' AND `active_status` = '1' AND `school_id` = '".$sgvalue['school_id']."' AND `sport_type_id` = '".$post_data['sport_type']."' AND `participant_type_id` = '1' AND `fullname` = '".$svalues['fullname']."' AND  `telephone` = '".$svalues['telephone']."' ")->rows;
					foreach($students_datas as $skey => $svalue){
						$update_sql = "UPDATE `oc_customer` SET `batch_id` = '".$count."' WHERE `customer_id` = '".$svalue['customer_id']."' ";
						$this->db->query($update_sql);
						$this->log->write($update_sql);
						//echo $update_sql;
						//echo '<br />';
					}
					$count ++ ;
				}
			}
			//echo 'out';exit;
			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_school'])) {
				$url .= '&filter_school=' . urlencode(html_entity_decode($this->request->get['filter_school'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_standard'])) {
				$url .= '&filter_standard=' . urlencode(html_entity_decode($this->request->get['filter_standard'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . urlencode(html_entity_decode($this->request->get['filter_sport_type'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_participant_type'])) {
				$url .= '&filter_participant_type=' . urlencode(html_entity_decode($this->request->get['filter_participant_type'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_sport'])) {
				$url .= '&filter_sport=' . urlencode(html_entity_decode($this->request->get['filter_sport'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->session->data['success'] = 'Chest Number Generated Successfully';
			$this->response->redirect($this->url->link('customer/customer', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getchestForm();
	}

	protected function getchestForm() {
		$data['heading_title'] = 'Generate Chest Code';//$this->language->get('heading_title');

		$data['text_form'] = 'Generate Chest Code';//$this->language->get('text_add');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_add_ban_ip'] = $this->language->get('text_add_ban_ip');
		$data['text_remove_ban_ip'] = $this->language->get('text_remove_ban_ip');

		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_fax'] = $this->language->get('entry_fax');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_confirm'] = $this->language->get('entry_confirm');
		$data['entry_newsletter'] = $this->language->get('entry_newsletter');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_approved'] = $this->language->get('entry_approved');
		$data['entry_safe'] = $this->language->get('entry_safe');
		$data['entry_company'] = $this->language->get('entry_company');
		$data['entry_address_1'] = $this->language->get('entry_address_1');
		$data['entry_address_2'] = $this->language->get('entry_address_2');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_postcode'] = $this->language->get('entry_postcode');
		$data['entry_zone'] = $this->language->get('entry_zone');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_default'] = $this->language->get('entry_default');
		$data['entry_comment'] = $this->language->get('entry_comment');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_amount'] = $this->language->get('entry_amount');
		$data['entry_points'] = $this->language->get('entry_points');

		$data['help_safe'] = $this->language->get('help_safe');
		$data['help_points'] = $this->language->get('help_points');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_address_add'] = $this->language->get('button_address_add');
		$data['button_history_add'] = $this->language->get('button_history_add');
		$data['button_transaction_add'] = $this->language->get('button_transaction_add');
		$data['button_reward_add'] = $this->language->get('button_reward_add');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_upload'] = $this->language->get('button_upload');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_address'] = $this->language->get('tab_address');
		$data['tab_history'] = $this->language->get('tab_history');
		$data['tab_transaction'] = $this->language->get('tab_transaction');
		$data['tab_reward'] = $this->language->get('tab_reward');
		$data['tab_ip'] = $this->language->get('tab_ip');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} elseif(isset($this->session->data['warning'])){
			$data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['sport_type'])) {
			$data['error_sport_type'] = $this->error['sport_type'];
		} else {
			$data['error_sport_type'] = '';
		}

		if (isset($this->error['start_number'])) {
			$data['error_start_number'] = $this->error['start_number'];
		} else {
			$data['error_start_number'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_school'])) {
			$url .= '&filter_school=' . urlencode(html_entity_decode($this->request->get['filter_school'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_standard'])) {
			$url .= '&filter_standard=' . urlencode(html_entity_decode($this->request->get['filter_standard'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_sport_type'])) {
			$url .= '&filter_sport_type=' . urlencode(html_entity_decode($this->request->get['filter_sport_type'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_participant_type'])) {
			$url .= '&filter_participant_type=' . urlencode(html_entity_decode($this->request->get['filter_participant_type'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_sport'])) {
			$url .= '&filter_sport=' . urlencode(html_entity_decode($this->request->get['filter_sport'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Generate Chest Code',
			'href' => $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['action'] = $this->url->link('customer/customer/generate_code', 'token=' . $this->session->data['token'] . $url, true);

		$data['cancel'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->post['sport_type'])) {
			$data['sport_type'] = $this->request->post['sport_type'];
		} else {
			$data['sport_type'] = '';
		}

		$data['sport_types'] = array(
			'1' => 'कला',
			'2' => 'क्रिडा',
		);
		
		if (isset($this->request->post['start_number'])) {
			$data['start_number'] = $this->request->post['start_number'];
		} else {
			$data['start_number'] = '';
		}		

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('customer/chest_form', $data));
	}

	protected function validateForm1() {
		if (!$this->user->hasPermission('modify', 'customer/customer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ($this->request->post['start_number'] == '' || $this->request->post['start_number'] < '0') {
			$this->error['start_number'] = 'Please Enter Valid Start Number';
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}
}