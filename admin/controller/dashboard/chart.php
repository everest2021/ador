<?php
class ControllerDashboardChart extends Controller {
	public function index() {
		$this->load->language('dashboard/chart');

		$data['heading_title'] = 'क्रिडा गट सहभागी /Krida Group Participants';//$this->language->get('heading_title');

		$data['text_view'] = 'View Detail';//$this->language->get('text_view');

		$data['token'] = $this->session->data['token'];

		$sql = "SELECT * FROM `oc_customer` WHERE `sport_type_id` = '2' AND `participant_type_id` = '2' ";
		$count = $this->db->query($sql)->num_rows;
		$data['count'] = $count;

		$data['url'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'].'&filter_sport_type=2&filter_participant_type=2', true);

		return $this->load->view('dashboard/chart', $data);
	}
}