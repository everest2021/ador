<?php
class ControllerCatalogAllfees extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/allfees');
		$this->load->model('catalog/fees');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getForm();
		
	}

	public function add() {
		$this->load->language('catalog/allfees');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/fees');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
			print_r($this->request->post);
			exit;*/
			$fees_datas = $this->request->post['fees_datas'];
			$data = $this->request->post;
			foreach($fees_datas as $fkey => $fees_data){

				if($fees_data['f_receivable'] == '' || $fees_data['f_receivable'] == '0'){
					$fees_data['f_receivable'] = $fees_data['f_paying_amount'];
					$fees_data['old_fees_value'] = $fees_data['f_paying_amount'];
				}

				$fees_value = $fees_data['f_receivable'];
				$received = $fees_data['f_received'];
				$paying_amount = $fees_data['f_paying_amount'];
				$total_paid = $received + $paying_amount;

				if($total_paid == $fees_value){
					$status = 1;
				} else {
					$status = 0;
				}

				if($fees_data['f_fees_type'] == 'monthly' || $fees_data['f_fees_type'] == 'bus' || $fees_data['f_fees_type'] == 'martial' || $fees_data['f_fees_type'] == 'Computer fee'){
					$month_val = $fees_data['f_month'];
				} else {
					$month_val = 1;
				}

				if($data['cheque_no'] == ''){
					$data['cheque_date'] = '0000-00-00';
				}

				if($data['cheque_date'] != '0000-00-00' && $data['cheque_date'] != ''){
					$cheque_date = date('Y-m-d', strtotime($data['cheque_date']));
				} else {
					$cheque_date = '0000-00-00';
				}

				if($data['date_of_payment'] != '0000-00-00' && $data['date_of_payment'] != ''){
					$date_of_payment = date('Y-m-d', strtotime($data['date_of_payment']));
				} else {
					$date_of_payment = '0000-00-00';
				}
				
				if($fees_data['f_transaction_id'] == '0'){
					$student_id = $data['name_id'];
					$student_data = $this->db->query("SELECT * FROM `sc_admission` WHERE `student_id` = '".$student_id."' ")->row;
					$standard_name = $this->db->query("SELECT `name` FROM `sc_standard` WHERE `standard_id` = '".$student_data['course_id']."' ")->row['name'];
					// $division_name = $this->db->query("SELECT `name` FROM `sc_division` WHERE `division_id` = '".$student_data['division_id']."' ")->row['name'];
					// $medium_name = $this->db->query("SELECT `name` FROM `sc_medium` WHERE `medium_id` = '".$student_data['medium_id']."' ")->row['name'];
					
					if($fees_data['f_receivable'] == '' || $fees_data['f_receivable'] == '0'){
						$fees_data['f_receivable'] = $fees_data['f_paying_amount'];
						$fees_data['old_fees_value'] = $fees_data['f_paying_amount'];
					}

					$fees_value = $fees_data['f_receivable'];
					$received = $fees_data['f_received'];
					$paying_amount = $fees_data['f_paying_amount'];
					$total_paid = $received + $paying_amount;

					if($total_paid == $fees_value){
						$status = 1;
					} else {
						$status = 0;
					}

					$st_data =  $this->db->query("SELECT * FROM sc_admission WHERE student_id = '".$student_id."' ")->row;
					$sc_student = $this->db->query("SELECT * FROM sc_student WHERE student_id = '".$student_id."' ");
						//echo "<pre>";print_r($st_data);exit;
					if ($sc_student->num_rows == 0) {
						$sql = "INSERT INTO `sc_student` SET 
							photo = '" . $this->db->escape($st_data['photo']) . "',
							photo_source = '" . $this->db->escape($st_data['photo_source']) . "',
							ssc = '" . $this->db->escape($st_data['ssc']) . "',
							ssc_source = '" . $this->db->escape($st_data['ssc_source']) . "',
							hsc = '" . $this->db->escape($st_data['hsc']) . "',
							hsc_source = '" . $this->db->escape($st_data['hsc_source']) . "',
							
							
							leaving= '" . $this->db->escape($st_data['leaving']) . "',
							leaving_source = '" . $this->db->escape($st_data['leaving_source']) . "',
							caste_cer = '" . $this->db->escape($st_data['caste_cer']) . "',
							caste_cer_source = '" . $this->db->escape($st_data['caste_cer_source']) . "',
							income_cer = '" . $this->db->escape($st_data['income_cer']) . "',
							income_cer_source = '" . $this->db->escape($st_data['income_cer_source']) ."',
							gov_cer = '" . $this->db->escape($st_data['gov_cer']) . "',
							gov_cer_source = '".$this->db->escape($st_data['gov_cer_source'])."',
							phy_cer = '" . $this->db->escape($st_data['phy_cer']) . "',
							phy_cer_source = '" . $this->db->escape($st_data['phy_cer_source']) . "',
							aadhar_cer = '" . $this->db->escape($st_data['aadhar_cer']) . "',
							aadhar_cer_source = '" . $this->db->escape($st_data['aadhar_cer_source']) . "',
							standard_id = '" . $this->db->escape($st_data['course_id']) . "',
							course_id = '" . $this->db->escape($st_data['course_id']) . "',
							name = '" . $this->db->escape($st_data['name']) . "',
							admission_date = '" . $this->db->escape($st_data['admission_date']) . "',
							father_name = '" . $this->db->escape($st_data['father_name']) . "',
							l_name = '" . $this->db->escape($st_data['l_name']) . "',
							mo_name = '" . $this->db->escape($st_data['mo_name']) . "',
							dob = '" . $this->db->escape($st_data['dob']) . "',
							place_birth = '" . $this->db->escape($st_data['place_birth']) . "',
							religious = '" . $this->db->escape($st_data['religious']) . "',
							reg_id = '" . $this->db->escape($st_data['reg_id']) . "',
							marital = '" . $this->db->escape($st_data['marital']) . "',
							marital_id = '" . $this->db->escape($st_data['marital_id']) . "',
							gender = '" . $this->db->escape($st_data['gender']) . "',
							blood_group = '" . $this->db->escape($st_data['blood_group']) . "',
							mobile_no = '" . $this->db->escape($st_data['mobile_no']) . "',
							aadhar_no = '" . $this->db->escape($st_data['aadhar_no']) . "',
							c_house_no = '" . $this->db->escape($st_data['c_house_no']) . "',
							c_street_name = '" . $this->db->escape($st_data['c_street_name']) . "',
							c_landmark = '" . $this->db->escape($st_data['c_landmark']) . "',
							c_locality = '" . $this->db->escape($st_data['c_locality']) . "',
							c_village = '" . $this->db->escape($st_data['c_village']) . "',
							c_taluka = '" . $this->db->escape($st_data['c_taluka']) . "',
							c_state = '" . $this->db->escape($st_data['c_state']) . "',
							c_pin_code = '" . $this->db->escape($st_data['c_pin_code']) . "',
							father_mobile_no = '" . $this->db->escape($st_data['father_mobile_no']) . "',
							c_email = '" . $this->db->escape($st_data['c_email']) . "',
							p_house_no = '" . $this->db->escape($st_data['p_house_no']) . "',
							p_street_name = '" . $this->db->escape($st_data['p_street_name']) . "',
							p_landmark = '" . $this->db->escape($st_data['p_landmark']) . "',
							p_locality = '" . $this->db->escape($st_data['p_locality']) . "',
							p_village= '" . $this->db->escape($st_data['p_village']) . "',
							p_taluka = '" . $this->db->escape($st_data['p_taluka']) . "',
							p_state = '" . $this->db->escape($st_data['p_state']) . "',
							p_pin_code = '" . $this->db->escape($st_data['p_pin_code']) . "',
							father_mobile_no1 = '" . $this->db->escape($st_data['father_mobile_no1']) . "',
							p_email = '" . $this->db->escape($st_data['p_email']) . "',
							dom_state = '" . $this->db->escape($st_data['dom_state']) . "',
							reservation_id= '" . $this->db->escape($st_data['reservation_id']) . "',
							caste = '" . $this->db->escape($st_data['caste']) . "',
							is_physical= '" . $this->db->escape($st_data['is_physical']) . "',
							father_occ = '" . $this->db->escape($st_data['father_occ']) . "',
							annual_income = '" . $this->db->escape($st_data['annual_income']) . "',
							ssc_board = '" . $this->db->escape($st_data['ssc_board']) . "',
							school_name = '" . $this->db->escape($st_data['school_name']) . "',
							ssc_passing = '" . $this->db->escape($st_data['ssc_passing']) . "',
							ssc_percentage = '" . $this->db->escape($st_data['ssc_percentage']) . "',
							hsc_board = '" . $this->db->escape($st_data['hsc_board']) . "',
							college_name = '" . $this->db->escape($st_data['college_name']) . "',
							hsc_passing= '" . $this->db->escape($st_data['hsc_passing']) . "',
							hsc_percentage= '" . $this->db->escape($st_data['hsc_percentage']) . "',
							sem1_res = '" . $this->db->escape($st_data['sem1_res']) . "',
							sem1_clgname = '" . $this->db->escape($st_data['sem1_clgname']) . "',
							sem1_passing= '" . $this->db->escape($st_data['sem1_passing']) . "',
							sem1_percentage= '" . $this->db->escape($st_data['sem1_percentage']) . "',
							sem2_res = '" . $this->db->escape($st_data['sem2_res']) . "',
							sem2_clgname = '" . $this->db->escape($st_data['sem2_clgname']) . "',
							sem2_passing= '" . $this->db->escape($st_data['sem2_passing']) . "',
							sem2_percentage= '" . $this->db->escape($st_data['sem2_percentage']) . "',
							sem3_res = '" . $this->db->escape($st_data['sem3_res']) . "',
							sem3_clgname = '" . $this->db->escape($st_data['sem3_clgname']) . "',
							sem3_passing= '" . $this->db->escape($st_data['sem3_passing']) . "',
							sem3_percentage= '" . $this->db->escape($st_data['sem3_percentage']) . "',
							sem4_res = '" . $this->db->escape($st_data['sem4_res']) . "',
							sem4_clgname = '" . $this->db->escape($st_data['sem4_clgname']) . "',
							sem4_passing= '" . $this->db->escape($st_data['sem4_passing']) . "',
							sem4_percentage= '" . $this->db->escape($st_data['sem4_percentage']) . "',
							sem5_res = '" . $this->db->escape($st_data['sem5_res']) . "',
							sem5_clgname = '" . $this->db->escape($st_data['sem5_clgname']) . "',
							sem5_passing= '" . $this->db->escape($st_data['sem5_passing']) . "',
							sem5_percentage= '" . $this->db->escape($st_data['sem5_percentage']) . "',
							sem6_res = '" . $this->db->escape($st_data['sem6_res']) . "',
							sem6_clgname = '" . $this->db->escape($st_data['sem6_clgname']) . "',
							sem6_passing= '" . $this->db->escape($st_data['sem6_passing']) . "',
							sem6_percentage= '" .$this->db->escape($st_data['sem6_percentage']) . "',
							sem1_cer = '". $this->db->escape($st_data['sem1_cer']) ."',
							sem2_cer ='". $this->db->escape($st_data['sem2_cer']) . "',
							sem3_cer ='". $this->db->escape($st_data['sem3_cer']) . "',
							sem4_cer ='". $this->db->escape($st_data['sem4_cer']) . "',
							sem5_cer ='". $this->db->escape($st_data['sem5_cer']) . "',
							sem6_cer ='". $this->db->escape($st_data['sem6_cer']) . "',
							sem1_cer_source = '". $this->db->escape($st_data['sem1_cer_source']) ."',
							sem2_cer_source ='". $this->db->escape($st_data['sem2_cer_source']) . "',
							sem3_cer_source ='". $this->db->escape($st_data['sem3_cer_source']) . "',
							sem4_cer_source ='". $this->db->escape($st_data['sem4_cer_source']) . "',
							sem5_cer_source ='". $this->db->escape($st_data['sem5_cer_source']) . "',
							sem6_cer_source ='". $this->db->escape($st_data['sem6_cer_source']) . "'
						";
					$this->db->query($sql);
						
					}
					

					$sql = "INSERT INTO `sc_fees_transaction` SET 
										`student_id` = '".$student_id."', 
										`student_name` = '".$student_data['name']."', 
										`standard_id` = '".$student_data['standard_id']."', 
										`standard_name` = '".$standard_name."',
										`medium_id` = '1',
										`medium_name` = 'English', 
										`fees_name` = '".$fees_data['f_fees_type']."', 
										`old_fees_value` = '".$fees_data['f_receivable']."', 
										`fees_value` = '".$fees_data['f_receivable']."',
										`fees_paid_value` = '" . $this->db->escape($fees_data['f_paying_amount']) . "',
										`cheque_no` = '" . $this->db->escape($data['cheque_no']) . "',
										`rec_no` = '" . $this->db->escape($data['rec_no']) . "',
										`rec_no_custom` = '" . $this->db->escape($data['rec_no_custom']) . "',
										`narration` = '" . $this->db->escape($fees_data['f_narration']) . "',
										`cheque_date` = '" . $this->db->escape($cheque_date) . "',
										`date_of_payment` = '" . $this->db->escape($date_of_payment) . "',
										`status` = '".$status."', 
										`active_status` = '1', 
										`month`='1', 
										`a_status` = '1', 
										`year` = '".$data['year']."' ";
							
					//echo $sql;
					//echo '<br />';
					$this->db->query($sql);
					//$this->log->write($sql);
					$f_transaction_id = $this->db->getLastId();

					$this->db->query("INSERT INTO `sc_fees_transaction_history` SET 
						`id` = '" . $this->db->escape($f_transaction_id) . "',
						`date_of_payment` = '" . $this->db->escape($date_of_payment) . "',
						`fees_paid_value` = '" . $this->db->escape($fees_data['f_paying_amount']) . "',
						`cheque_no` = '" . $this->db->escape($data['cheque_no']) . "',
						`rec_no` = '" . $this->db->escape($data['rec_no']) . "',
						`rec_no_custom` = '" . $this->db->escape($data['rec_no_custom']) . "',
						`narration` = '" . $this->db->escape($fees_data['f_narration']) . "',
						`payment_type` = '" . $this->db->escape($data['payment_type']) . "',
						`cheque_date` = '" . $this->db->escape($cheque_date) . "'
					");
				} else {
					$this->db->query("UPDATE `sc_fees_transaction` SET 
						`date_of_payment` = '" . $this->db->escape($date_of_payment) . "',
						`fees_paid_value` = '" . $this->db->escape($fees_data['f_paying_amount']) . "',
						`fees_value` = '".$fees_data['f_receivable']."',
						`cheque_no` = '" . $this->db->escape($data['cheque_no']) . "',
						`rec_no` = '" . $this->db->escape($data['rec_no']) . "',
						`rec_no_custom` = '" . $this->db->escape($data['rec_no_custom']) . "',
						`narration` = '" . $this->db->escape($fees_data['f_narration']) . "',
						`cheque_date` = '" . $this->db->escape($cheque_date) . "',
						`status` = '" . $this->db->escape($status) . "'
						WHERE `id` = '".$fees_data['f_transaction_id']."'
					");

					
					$this->db->query("INSERT INTO `sc_fees_transaction_history` SET 
						`id` = '" . $this->db->escape($fees_data['f_transaction_id']) . "',
						`date_of_payment` = '" . $this->db->escape($date_of_payment) . "',
						`fees_paid_value` = '" . $this->db->escape($fees_data['f_paying_amount']) . "',
						`cheque_no` = '" . $this->db->escape($data['cheque_no']) . "',
						`rec_no` = '" . $this->db->escape($data['rec_no']) . "',
						`rec_no_custom` = '" . $this->db->escape($data['rec_no_custom']) . "',
						`narration` = '" . $this->db->escape($fees_data['f_narration']) . "',
						`payment_type` = '" . $this->db->escape($data['payment_type']) . "',
						`cheque_date` = '" . $this->db->escape($cheque_date) . "'
					");
				}
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			if(isset($data['repeat'])){
				$url .= '&date_of_payment=' . $data['date_of_payment'];
				$url .= '&date_of_payment=' . $data['date_of_payment'].'&name='.$data['name'].'&name_id='.$data['name_id'].'&rec_no='.$data['rec_no'].'&cheque_no='.$data['cheque_no'].'&cheque_date='.$data['cheque_date'].'&year='.$data['year'];
			}
			if(isset($this->request->get['print'])){
				$this->response->redirect($this->url->link('catalog/allfees/prints', 'token=' . $this->session->data['token'] . $url.'&rec_no_custom='.$data['rec_no_custom'], true));
			} else {
				$this->response->redirect($this->url->link('catalog/allfees', 'token=' . $this->session->data['token'] . $url, true));
			}
		}

		$this->getForm();
	}

	public function prints() {
		$this->load->language('catalog/purchaseorder');
		$this->load->language('sale/order');

		$data['title'] = 'Receipt';

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		$data['text_invoice'] = 'Receipt';//$this->language->get('text_invoice');
		
		$data['orders'] = array();
		$orders = array();
		//echo $this->request->get['rec_no_custom'];exit;
		if (isset($this->request->get['rec_no_custom'])) {
			$rec_no_custom = $this->request->get['rec_no_custom'];
		}

		$months = array(
			'0' => 'None',
			'1' => 'June',
			'2' => 'July',
			'3' => 'August',
			'4' => 'September',
			'5' => 'October',
			'6' => 'November',
			'7' => 'December',
			'8' => 'January',
			'9' => 'February',
			'10' => 'March',
			'11' => 'April',
			'12' => 'May',
		);

		$feestype1 = $this->db->query("SELECT * FROM sc_feestype")->rows;
		foreach($feestype1 as $pkey => $value){
				$fees_types[$value['feestype']] = $value['feestype'];
			}

		$payment_types_array = array(
			'1' => 'Cash',		
			'2' => 'BCCB Bank',
			'3' => 'NKGSB Bank'
		);

		$rec_group_sql = "SELECT fth.`cheque_no`, fth.`cheque_date`, fth.`payment_type`, fth.`fees_paid_value`, fth.`rec_no`, ft.`rec_no_custom`, fth.`date_of_payment`, ft.`fees_name`, ft.`student_name`, ft.`standard_name`, ft.`division_name`, ft.`medium_name`, ft.`month`, ft.`roll_no`, ft.`student_id` FROM `sc_fees_transaction` ft LEFT JOIN `sc_fees_transaction_history` fth ON (ft.id = fth.id) WHERE 1=1 AND fth.`rec_no_custom` = '".$rec_no_custom."' GROUP BY `fees_name` ORDER BY `h_id` ASC";
		$rec_group_datas = $this->db->query($rec_group_sql)->rows;
		if ($rec_group_datas) {
			$total = 0;
			$third = 0;
			foreach($rec_group_datas as $pkey => $pvalue){
				$fees_name = $pvalue['fees_name'];
				$product_data = array();
				
				$rec_sql = "SELECT fth.`fees_paid_value`, ft.`fees_name`, ft.`month` FROM `sc_fees_transaction` ft LEFT JOIN `sc_fees_transaction_history` fth ON (ft.id = fth.id) WHERE 1=1 AND fth.`rec_no_custom` = '".$rec_no_custom."' AND `fees_name` = '".$fees_name."' ";		
				$rec_datas = $this->db->query($rec_sql)->rows;

				//$stud_sql = "SELECT `gr_no` FROM `sc_admission` WHERE `student_id` = '".$pvalue['student_id']."' ";		
				//$gr_no = $this->db->query($stud_sql)->row['gr_no'];

				$fees_paid_value = 0;

				$from_month = $rec_datas[0]['month'];
				$cheque_no = '';
				$cheque_date = '';
				$bank_name = '';
				if($from_month != 0){
					$from_month = $months[$from_month];
					end($rec_datas);
					$last_key = key($rec_datas);
					$to_month = $months[$rec_datas[$last_key]['month']];
					if($from_month != $to_month){
						$fees_name = $fees_types[$pvalue['fees_name']].' - ' . $from_month . ' - ' . $to_month; 
					} else {
						$fees_name = $fees_types[$pvalue['fees_name']].' - ' . $from_month; 
					}
					if($pvalue['cheque_no'] != ''){
						if($pvalue['cheque_date'] != '0000-00-00' && $pvalue['cheque_date'] != '1970-01-01'){
							$cheque_no = $pvalue['cheque_no'];
							$cheque_date = date('d-m-Y', strtotime($pvalue['cheque_date']));
							$bank_name = $payment_types_array[$pvalue['payment_type']];
						} else {
							$cheque_no = $pvalue['cheque_no'];
							$cheque_date = '';
							$bank_name = $payment_types_array[$pvalue['payment_type']];
						}
						//$fees_name = $fees_name.' ( '.$payment_data.' )'; 
					} else {
						//$payment_data = $payment_types_array[$pvalue['payment_type']];
					}
				} else {
					$fees_name = $fees_types[$pvalue['fees_name']];
					if($pvalue['cheque_no'] != ''){
						if($pvalue['cheque_date'] != '0000-00-00' && $pvalue['cheque_date'] != '1970-01-01'){
							$cheque_no = $pvalue['cheque_no'];
							$cheque_date = date('d-m-Y', strtotime($pvalue['cheque_date']));
							$bank_name = $payment_types_array[$pvalue['payment_type']];
						} else {
							$cheque_no = $pvalue['cheque_no'];
							$cheque_date = '';
							$bank_name = $payment_types_array[$pvalue['payment_type']];
						}
						//$fees_name = $fees_name.' ( '.$payment_data.' )'; 
					} else {
						//$payment_data = $payment_types_array[$pvalue['payment_type']];
					}
				}

				if($pvalue['fees_name'] == 'Term Fee 1' || $pvalue['fees_name'] == 'Term Fee 2' || $pvalue['fees_name'] == 'monthly'){
					$third = 1;
				}

				foreach ($rec_datas as $rec_data) {
					$fees_paid_value = $fees_paid_value + $rec_data['fees_paid_value'];
					$total = $total + ($rec_data['fees_paid_value']);
				}
				$data['orders'][] = array(
					'rec_no'	       => $pvalue['rec_no'],
					'rec_no_custom'	   => $rec_no_custom,
					'date_of_payment' => $pvalue['date_of_payment'],
					'name' => $pvalue['student_name'],
					'standard_name' => $pvalue['standard_name'],
					'fees_name'	       => $fees_name,
					'raw_fees_name'	       => $pvalue['fees_name'],
					'cheque_no'	       => $cheque_no,
					'bank_name'	   => $bank_name,
					'fees_value'	   => $fees_paid_value,
				);
			}
			$data['orders']['0']['third_print'] = $third;
		}
		// echo '<pre>';
		// print_r($data['orders']);
		// exit;
		$html = $this->load->view('sale/order_invoice', $data);
		echo $html;exit;
		//$filename = 'Purchase_Order_' . $po_number.'.html';
		//header('Content-type: text/html');
		//header('Set-Cookie: fileLoading=true');
		//header('Content-Disposition: attachment; filename='.$filename);
		//echo $html;
		//exit;
		//$this->response->setOutput($this->load->view('sale/order_invoice', $data));
	}

	public function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['paying_amount'])) {
			$data['error_paying_amount'] = $this->error['paying_amount'];
		} else {
			$data['error_paying_amount'] = '';
		}

		if (isset($this->error['fees'])) {
			$data['error_fees'] = $this->error['fees'];
		} else {
			$data['error_fees'] = array();
		}

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/division', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['action'] = $this->url->link('catalog/allfees/add', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['action1'] = $this->url->link('catalog/allfees/add', 'token=' . $this->session->data['token'] . $url.'&print=1', true);
		
		$data['cancel'] = $this->url->link('catalog/allfees', 'token=' . $this->session->data['token'] . $url, true);

		$data['token'] = $this->session->data['token'];

		if(isset($this->request->post['total_paying_amount'])){
			$data['total_paying_amount'] = $this->request->post['total_paying_amount'];
		} else {
			$data['total_paying_amount'] = '';
		}

		if(isset($this->request->post['date_of_payment'])){
			$data['date_of_payment'] = $this->request->post['date_of_payment'];
		} elseif(isset($this->request->get['date_of_payment'])) {
			$data['date_of_payment'] = $this->request->get['date_of_payment'];
		} else {
			$data['date_of_payment'] = date('d-m-Y');
		}

		if(isset($this->request->post['name'])){
			$data['name'] = $this->request->post['name'];
		} elseif(isset($this->request->get['name'])) {
			$data['name'] = $this->request->get['name'];
		} else {
			$data['name'] = '';
		}

		if(isset($this->request->post['name_id'])){
			$data['name_id'] = $this->request->post['name_id'];
		} elseif(isset($this->request->get['name_id'])) {
			$data['name_id'] = $this->request->get['name_id'];
		} else {
			$data['name_id'] = '0';
		}

		// if(isset($this->request->post['id'])){
		// 	$data['id'] = $this->request->post['id'];
		// } else {
		// 	$data['id'] = '0';
		// }

		if(isset($this->request->post['rec_no'])){
			$data['rec_no'] = $this->request->post['rec_no'];
		} elseif(isset($this->request->get['rec_no'])) {
			$data['rec_no'] = $this->request->get['rec_no'];
		} else {
			$data['rec_no'] = '';
		}

		if(isset($this->request->post['type'])){
			$data['type'] = $this->request->post['type'];
		} elseif(isset($this->request->get['type'])) {
			$data['type'] = $this->request->get['type'];
		} else {
			$data['type'] = '';
		}

		if(isset($this->request->post['rec_no_custom'])){
			$data['rec_no_custom'] = $this->request->post['rec_no_custom'];
		} elseif(isset($this->request->get['rec_no_custom'])) {
			$data['rec_no_custom'] = $this->request->get['rec_no_custom'];
		} else {
			$rec_no_custom = $this->db->query("SELECT `rec_no_custom` FROM `sc_fees_transaction_history` ORDER BY `rec_no_custom` DESC LIMIT 1")->row;
			if(isset($rec_no_custom['rec_no_custom'])){
				$rec_no_custom = $rec_no_custom['rec_no_custom'];
				if($rec_no_custom == '0'){
					$rec_no_custom = '20000';
				} else {
					$rec_no_custom = $rec_no_custom + 1;
				}
			} else {
				$rec_no_custom = '20000';
			}
			$data['rec_no_custom'] = $rec_no_custom;
		}

		if(isset($this->request->post['cheque_no'])){
			$data['cheque_no'] = $this->request->post['cheque_no'];
		} elseif(isset($this->request->get['cheque_no'])) {
			$data['cheque_no'] = $this->request->get['cheque_no'];
		} else {
			$data['cheque_no'] = '';
		}

		if(isset($this->request->post['cheque_date'])){
			$data['cheque_date'] = $this->request->post['cheque_date'];
		} elseif(isset($this->request->get['cheque_date'])) {
			$data['cheque_date'] = $this->request->get['cheque_date'];
		} else {
			$data['cheque_date'] = '';
		}

		if(isset($this->request->post['year'])){
			$data['year'] = $this->request->post['year'];
		} elseif(isset($this->request->get['year'])) {
			$data['year'] = $this->request->get['year'];
		} else {
			$data['year'] = '2019';//date('Y');
		}

		if(isset($this->request->post['fees_type'])){
			$data['fees_type'] = $this->request->post['fees_type'];
		} else {
			$data['fees_type'] = '0';
		}

		$months1 = array(
			'June' => '1',
			'July' => '2',
			'August' => '3',
			'September' => '4',
			'October' => '5',
			'November' => '6',
			'December' => '7',
			'January' => '8',
			'February' => '9',
			'March' => '10',
			'April' => '11',
			'May' => '12',
		);

		$data['years'] = array(
			'2019' => '2019-2020'
		);

		$data['fees_types'] = $this->db->query("SELECT * FROM sc_feestype")->rows;
		// echo "<pre>";
		// print_r($data['fees_types']);exit;
		// $data['fees_types'] = array(
		// 	'0' => 'Please Select',		
		// 	'monthly' => 'Monthly',
		// 	'Miscellaneous' => 'Miscellaneous',
		// 	'Term Fee 1' => 'Term Fee 1',
		// 	'Term Fee 2' => 'Term Fee 2',
		// 	'Computer fee' => 'Computer fee',
		// 	//'Id And Calender' => 'Id And Calender',
		// 	'martial' => 'Martial',
		// 	'bus' => 'Bus',
		// 	'fine' => 'Fine',
		// 	'admission' => 'Admission'
		// 	//'kalakrida' => 'Kalakrida',
		// 	//'tentuition' => 'Ten Tuition',
		// 	//'picnic' => 'Picnic',
		// );

		$data['payment_types'] = array(
			'1' => 'Cash',		
			'2' => 'BCCB Bank',
			'3' => 'NKGSB Bank'
		);

		$data['months'] = array(
			'0' => 'None',
			'1' => 'June',
			'2' => 'July',
			'3' => 'August',
			'4' => 'September',
			'5' => 'October',
			'6' => 'November',
			'7' => 'December',
			'8' => 'January',
			'9' => 'February',
			'10' => 'March',
			'11' => 'April',
			'12' => 'May',
		);

		// if(isset($this->request->post['month'])){
		// 	$data['month'] = $this->request->post['month'];
		// } else {
		// 	$data['month'] = $months1[date('F')];
		// }

		$data['current_month'] = $months1[date('F')];

		
		if(isset($this->request->post['narration'])){
			$data['narration'] = $this->request->post['narration'];
		} else {
			$data['narration'] = '';
		}

		if(isset($this->request->post['payment_type'])){
			$data['payment_type'] = $this->request->post['payment_type'];
		} else {
			$data['payment_type'] = '1';
		}
		

		if(isset($this->request->post['fees_datas'])){
			$data['fees_datas'] = $this->request->post['fees_datas'];
		} else {
			$data['fees_datas'] = array();
		}

		/*echo '<pre>';
		print_r($data['fees_datas']);
		exit;
*/
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/allfees_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/allfees')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = 'Please Select Student Name';
		}

		if($this->request->post['name_id'] == '0'){
			$this->error['name'] = 'Please Select The Name From Auto Search';	
		}

		// echo '<pre>';
		// print_r($this->request->post['fees_datas']);
		// exit;

		$fees_datas = $this->request->post['fees_datas'];
		if($fees_datas){
			foreach($fees_datas as $fkey => $fees_data){
				$fees_value = $fees_data['f_receivable'];
				$received = $fees_data['f_received'];
				$paying_amount = $fees_data['f_paying_amount'];
				$total_paid = $received + $paying_amount;

				$hidden_paying_amount = $fees_data['f_old_paying_amount'];
				$discount_amount = $fees_data['f_discount'];

				$in = 0;
				if($hidden_paying_amount == $discount_amount){
					$in = 1;
				}

				if( ($paying_amount == '0' || $paying_amount == '') && $in == 0){
					$this->error['fees'][$fkey]['paying_amount'] = 'Please Enter Paying Amount';
				}
				
				if($fees_data['f_name'] != 'Fine'){
					if($total_paid > $fees_value){
						$this->error['fees'][$fkey]['paying_amount'] = 'Paying Amount is Greater than Fees Receivable';		
					}
				}
			}
		}
		
		// echo '<pre>';
		// print_r($this->error);
		// exit;

		// if ((utf8_strlen($this->request->post['division_code']) < 1) || (utf8_strlen($this->request->post['division_code']) > 64)) {
		// 	$this->error['division_code'] = 'Please Enter Color Code!';
		// }

		// if($this->request->post['category_id'] == '0' || $this->request->post['category_id'] == ''){
		// 	$this->error['category'] = 'Please Select Valid Category!';
		// }

		return !$this->error;
	}

	public function getdata() {
		$json = array();
		//echo "<pre>";print_r($this->request->get);exit;
		if (isset($this->request->get['filter_name']) && isset($this->request->get['filter_name_id']) && isset($this->request->get['filter_fees_type'])) {
			$this->load->model('catalog/student');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_name_id' => $this->request->get['filter_name_id'],
				'filter_fees_type' => $this->request->get['filter_fees_type'],
				'filter_month' => $this->request->get['filter_month'],
				'filter_year' => $this->request->get['filter_year'],
				's_extra_field_row' => $this->request->get['s_extra_field_row'],
			);

			$sql = "SELECT `id`, `fees_value`, `fees_paid_value`, `fees_name` FROM `sc_fees_transaction` WHERE 1=1 ";
			if(!empty($filter_data['filter_name_id'])){
				$sql .= " AND `student_id` = '".$filter_data['filter_name_id']."' ";
			}
			if(!empty($filter_data['filter_month'])){
				$sql .= " AND `month` = '".$filter_data['filter_month']."' ";
			}
			if(!empty($filter_data['filter_year'])){
				$sql .= " AND `year` = '".$filter_data['filter_year']."' ";
			}
			if(!empty($filter_data['filter_fees_type'])){
				$sql .= " AND `fees_name` = '".$filter_data['filter_fees_type']."' ";
			}
			$sql .= " AND `a_status` = '1' ";
			$this->log->write($sql);
			$result = $this->db->query($sql)->row;
			/*echo "<pre>";
			print_r($sql);
			exit;*/

			$receivable = 0;
			$received = 0;
			$pending = 0;
			$id = 0;
			//$this->log->write(print_r($result,true));
			if(isset($result['id'])){
				if($filter_data['filter_fees_type'] != 'fine'){
					$h_results = $this->db->query("SELECT `fees_paid_value` FROM `sc_fees_transaction_history` WHERE `id` = '".$result['id']."'");
					
					//$this->log->write(print_r($h_results,true));

					if($h_results->num_rows > 0){
						$received = 0;
						foreach($h_results->rows as $rkey => $rvalue){
							$received += $rvalue['fees_paid_value'];
						}
					} else {
						$received = '0';//$result['fees_paid_value'];
					}
					
					//if($filter_data['filter_fees_type'] == 'Computer fee'){
						//$comp_month = $result['fees_value'];
						//$receivable = $comp_month / 12;
					//} else {
						$receivable = $result['fees_value'];
					//}
					
					$pending = $result['fees_value'] - $received;
					$id = $result['id'];
				} else {
					$id = 0;
					$receivable = 0;
					$received = 0;
					$pending = 0;
				}
			} else {
				$standard_id = $this->db->query("SELECT `course_id` FROM `sc_admission` WHERE `student_id` = '".$filter_data['filter_name_id']."' ")->row['course_id']; 
				$standard_data = $this->db->query("SELECT * FROM `sc_standard` WHERE `standard_id` = '".$standard_id."' ")->row;
				$student_data = $this->db->query("SELECT * FROM `sc_admission` WHERE `student_id` = '".$filter_data['filter_name_id']."' ")->row;


				$fess_type = $this->db->query("SELECT * FROM  sc_standardfees WHERE standard_id = '".$standard_id."'")->rows;
				foreach ($fess_type as $key => $value) {
					if ($filter_data['filter_fees_type'] == $value['fees_name']){

						if ($filter_data['filter_fees_type'] == 'martial' && $student_data['is_marshal'] == '1') {
							if($student_data['is_marshal'] == '1'){
								$receivable = $value['fees_value'];
							} else {
								$receivable = 0;
							}
						} elseif ($filter_data['filter_fees_type'] == 'martial' && $student_data['is_bus'] == '1') {
							if($student_data['is_bus'] == '1'){
								$receivable = $value['fees_value'];
							} else {
								$receivable = 0;
							}
						}elseif ($filter_data['filter_fees_type'] == 'admission') {
							if($student_data['is_admission'] == '1'){
				 				$receivable = $student_data['admission_fee'];
				 			} else {
				 				$receivable = 0;
				 			}
						} 
						else {
							$receivable  = $value['fees_value'];
						}
					}
				}
				

				// if($filter_data['filter_fees_type'] == 'monthly'){
				// 	$receivable = $standard_data['monthly_fees'];
				// } elseif($filter_data['filter_fees_type'] == 'Miscellaneous'){
				// 	$receivable = $standard_data['extra_fees_val_3'];
				// } elseif($filter_data['filter_fees_type'] == 'Term Fee 1'){
				// 	$receivable = $standard_data['extra_fees_val_1'];
				// } elseif($filter_data['filter_fees_type'] == 'Term Fee 2'){
				// 	$receivable = $standard_data['extra_fees_val_2'];
				// } elseif($filter_data['filter_fees_type'] == 'Computer fee'){
				// 	$receivable = $standard_data['extra_fees_val_4'];
				// 	//$receivable = $comp_month / 12;
				// } elseif($filter_data['filter_fees_type'] == 'Id And Calender'){
				// 	$receivable = $standard_data['extra_fees_val_5'];
				// 
				// } elseif($filter_data['filter_fees_type'] == 'martial' && $student_data['is_marshal'] == '1'){
				// 	$receivable = '50';
				// } elseif($filter_data['filter_fees_type'] == 'bus' && $student_data['is_bus'] == '1'){
				// 	$receivable = $student_data['bus_fee'];
				// } elseif($filter_data['filter_fees_type'] == 'kalakrida'){
				// 	$receivable = '';
				// } elseif($filter_data['filter_fees_type'] == 'tentuition'){
				// 	$receivable = '';
				// } elseif($filter_data['filter_fees_type'] == 'picnic'){
				// 	$receivable = '';
				// } elseif($filter_data['filter_fees_type'] == 'fine'){
				// 	$receivable = 0;
				// } elseif($filter_data['filter_fees_type'] == 'admission'){
				// 	if($student_data['is_admission'] == '1'){
				// 		$receivable = $student_data['admission_fee'];
				// 	} else {
				// 		$receivable = 0;
				// 	}	
				// }
				$received = 0;
				$pending = 0;
				$id = 0;
			}


			$standard  = $this->db->query("SELECT `course_id` FROM sc_admission WHERE student_id = '".$filter_data['filter_name_id']."'")->row;

			$fees_typess  = $this->db->query("SELECT * FROM sc_standardfees WHERE standard_id = '".$standard['course_id']."' AND `fees_value` <> 0 ")->rows;
				//echo "<pre>";print_r($fees_typess);
			$fees_types = array();
			foreach ($fees_typess as $key => $value) {
				$fees_types[$value['fees_name']] = $value['fees_name'];
			}
			//echo $value['fees_name'];
			//echo "<pre>";print_r($fees_types);
			$data['fees_types'] = $fees_types;

			// $data['fees_types'] = array(
			// 	'monthly' => 'Monthly',
			// 	'Miscellaneous' => 'Miscellaneous',
			// 	'Term Fee 1' => 'Term Fee 1',
			// 	'Term Fee 2' => 'Term Fee 2',
			// 	'Computer fee' => 'Computer fee',
			// 	//'Id And Calender' => 'Id And Calender',
			// 	'martial' => 'Martial',
			// 	'bus' => 'Bus',
			// 	'fine' => 'Fine',
			// 	'kalakrida' => 'Kalakrida',
			// 	'tentuition' => 'Ten Tuition',
			// 	'picnic' => 'Picnic',
			// );

			$months_name = array(
				'0' => 'None',
				'1' => 'June',
				'2' => 'July',
				'3' => 'August',
				'4' => 'September',
				'5' => 'October',
				'6' => 'November',
				'7' => 'December',
				'8' => 'January',
				'9' => 'February',
				'10' => 'March',
				'11' => 'April',
				'12' => 'May',
			);

			// if($filter_data['filter_fees_type'] == 'bus') {
			// 	$fees_name = $months_name[$filter_data['filter_month']].'-Bus';
			// } elseif($filter_data['filter_fees_type'] == 'marshal') {
			// 	$fees_name = $months_name[$filter_data['filter_month']].'-Marshal';
			// } elseif($filter_data['filter_fees_type'] == 'Computer fee') {
			// 	$fees_name = $months_name[$filter_data['filter_month']].'-Computer Fee';
			// } else {
				$fees_name = ucwords($filter_data['filter_fees_type']);
			//}

			$json = array(
				'id' => $id,
				's_extra_field_row' => $filter_data['s_extra_field_row'],
				'fees_name' => $fees_name,
				'receivable' => $receivable,
				'received' => $received,
				'pending' => $pending
			);
		}
		//$this->log->write(print_r($json,true));

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getfeestype() {
		$json = array();
		//echo "<pre>";print_r($this->request->get['feestype']);exit;		
		if (isset($this->request->get['feestype'])) {
			$this->load->model('catalog/student');
		$results  = $this->db->query("SELECT * FROM sc_feestype WHERE `feestype` = '".$this->request->get['feestype']."'")->row;
			foreach ($results as $result) {

				$json[] = array(
					'type' => $result['type'],
					'fees_type' => $result['fees_type'],
				);
			}
			
		}
		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/admission');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20,
				'sort'        => 'name',
			);

			
			$results = $this->model_catalog_admission->getStudents($filter_data);

			

			//echo "<pre>";print_r($results);exit;

			foreach ($results as $result) {
				// $standard  = $this->db->query("SELECT `standard_id` FROM sc_student WHERE student_id = '".$result['course_id']."'")->row;

				$fees_typess  = $this->db->query("SELECT * FROM sc_standardfees WHERE standard_id = '".$result['course_id']."' AND fees_value <> '0'")->rows;
				//echo "<pre>";print_r($fees_typess);
				$fees_types = array();
				foreach ($fees_typess as $key => $value) {
					$fees_types[$value['fees_name'].'-'.$value['type']] = $value['fees_name'];
				}
				//echo $value['fees_name'];
				//echo "<pre>";print_r($fees_types);
				$data['fees_types'] = $fees_types;
				//echo "<pre>";print_r($data['fees_types']);exit;
				// $data['fees_types'] = array(
				// 	'0' => 'Please Select',		
				// 	'monthly' => 'Monthly',
				// 	'Miscellaneous' => 'Miscellaneous',
				// 	'Term Fee 1' => 'Term Fee 1',
				// 	'Term Fee 2' => 'Term Fee 2',
				// 	'Computer fee' => 'Computer fee',
				// 	//'Id And Calender' => 'Id And Calender',
				// 	'martial' => 'Martial',
				// 	'bus' => 'Bus',
				// 	'fine' => 'Fine',
				// 	'admission' => 'Admission'
				// 	//'kalakrida' => 'Kalakrida',
				// 	//'tentuition' => 'Ten Tuition',
				// 	//'picnic' => 'Picnic',
				// );
				 $standard_name = $this->db->query("SELECT `name` FROM `sc_standard` WHERE `standard_id` = '".$result['course_id']."' ")->row['name'];
				// $division_name = $this->db->query("SELECT `name` FROM `sc_division` WHERE `division_id` = '".$result['division_id']."' ")->row['name'];
				// $medium_name = $this->db->query("SELECT `name` FROM `sc_medium` WHERE `medium_id` = '".$result['medium_id']."' ")->row['name'];
				
				// if($result['is_bus'] == '0'){
				// 	unset($data['fees_types']['bus']);
				// }
				// if($result['is_marshal'] == '0'){
				// 	unset($data['fees_types']['martial']);
				// }
				// if($result['is_admission'] == '0'){
				// 	unset($data['fees_types']['admission']);
				// }

				$json[] = array(
					'student_id' => $result['student_id'],
					'fees_types' => $data['fees_types'],
					'name'     => strip_tags(html_entity_decode($result['name'].' - ' .$standard_name, ENT_QUOTES, 'UTF-8'))
				);
			}
			//exit;
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function gethistdata() {
		//echo "in";exit;
		$json = array();

		if (isset($this->request->get['filter_student_id'])) {
			$this->load->model('catalog/student');

			//echo $this->request->get['filter_student_id'];exit;
			$filter_data = array(
				'filter_student_id' => $this->request->get['filter_student_id'],
			);

			$month_fees_types = array(
				'monthly' => 'Monthly',
				'Computer fee' => 'Computer fee',
				'martial' => 'Martial',
				'bus' => 'Bus',
			);

			

			


			// $fees_types = array(
			// 	'Miscellaneous' => 'Miscellaneous',
			// 	'Term Fee 1' => 'Term Fee 1',
			// 	'Term Fee 2' => 'Term Fee 2',
			// 	'Id And Calender' => 'Id And Calender',
			// 	//'fine' => 'Fine',
			// 	'admission' => 'Admission',
			// );

			$current_month = date('n');
			if($current_month <= 12 && $current_month >= 6){
				$current_year = date('Y');
			} else {
				$current_year = date('Y');
				$current_year = $current_year - 1;
			}

			$months_name = array(
				'0' => 'None',
				'1' => 'June',
				'2' => 'July',
				'3' => 'August',
				'4' => 'September',
				'5' => 'October',
				'6' => 'November',
				'7' => 'December',
				'8' => 'January',
				'9' => 'February',
				'10' => 'March',
				'11' => 'April',
				'12' => 'May',
			);

			$result = $this->db->query("SELECT `student_id`, `course_id` FROM `sc_admission` WHERE `student_id` = '".$filter_data['filter_student_id']."' ")->row;
			//echo "<pre>";print_r($result);exit;

			// $paid_datas = array();
			// foreach($month_fees_types as $fkey => $fvalue){
			// 	$paid_text = '';
			// 	$applicable = 1;
			// 	if($fkey == 'bus'){
			// 		$is_bus = $this->db->query("SELECT `is_bus` FROM `sc_student` WHERE `student_id` = '".$result['student_id']."' ")->row['is_bus'];
			// 		if($is_bus == '0'){
			// 			$applicable = 0;
			// 		}
			// 	} elseif($fkey == 'martial'){
			// 		$is_marshal = $this->db->query("SELECT `is_marshal` FROM `sc_student` WHERE `student_id` = '".$result['student_id']."' ")->row['is_marshal'];
			// 		if($is_marshal == '0'){
			// 			$applicable = 0;
			// 		}
			// 	} elseif($fkey == 'Computer fee'){
			// 		$is_computer = $this->db->query("SELECT `extra_fees_val_4` FROM `sc_standard` WHERE `standard_id` = '".$result['standard_id']."' AND `medium_id` = '".$result['medium_id']."' ")->row['extra_fees_val_4'];
			// 		if($is_computer == '0'){
			// 			$applicable = 0;
			// 		}	
			// 	}
			// 	$sql = "SELECT ft.`month` FROM `sc_fees_transaction` ft LEFT JOIN `sc_fees_transaction_history` fth ON (ft.id = fth.id) WHERE 1=1 AND `student_id` = '".$result['student_id']."' AND `a_status` = '1' AND `fees_name` = '".$fkey."' AND `status` = '1' AND `year` = '".$current_year."' ORDER BY MONTH DESC LIMIT 1";
			// 	$paid_data = $this->db->query($sql);
			// 	if($paid_data->num_rows > 0){
			// 		$paid_month = $months_name[$paid_data->row['month']];
			// 		$paid_text = 'Paid Upto ' . $paid_month;
					
			// 		$sql = "SELECT ft.`month`, fth.`fees_paid_value` FROM `sc_fees_transaction` ft RIGHT JOIN `sc_fees_transaction_history` fth ON (ft.id = fth.id) WHERE 1=1 AND `student_id` = '".$result['student_id']."' AND `a_status` = '1' AND `fees_name` = '".$fkey."' AND `status` = '0' AND `year` = '".$current_year."' ORDER BY MONTH DESC LIMIT 1";
			// 		$unpaid_data = $this->db->query($sql);
			// 		if($unpaid_data->num_rows > 0){
			// 			$partial_paid_amount = $unpaid_data->row['fees_paid_value'];
			// 			$partial_month = $months_name[$unpaid_data->row['month']];
			// 			$paid_text = $paid_text.' And Partial Payment of Rs ' . $partial_paid_amount . ' For Month of ' . $partial_month;
			// 		}
			// 	} else {
			// 		$sql = "SELECT ft.`month`, fth.`fees_paid_value` FROM `sc_fees_transaction` ft RIGHT JOIN `sc_fees_transaction_history` fth ON (ft.id = fth.id) WHERE 1=1 AND `student_id` = '".$result['student_id']."' AND `a_status` = '1' AND `fees_name` = '".$fkey."' AND `status` = '0' AND `year` = '".$current_year."' ORDER BY MONTH DESC LIMIT 1";
			// 		$unpaid_data = $this->db->query($sql);
			// 		if($unpaid_data->num_rows > 0){
			// 			$partial_paid_amount = $unpaid_data->row['fees_paid_value'];
			// 			$partial_month = $months_name[$unpaid_data->row['month']];
			// 			$paid_text = 'Partial Payment of Rs ' . $partial_paid_amount . ' For Month of ' . $partial_month;
			// 		} else {
			// 			$paid_text = 'Not Paid';
			// 		}
			// 	}
			// 	if($applicable == 1){
			// 		$paid_datas[$fkey] = array(
			// 			'FeeName' => $fvalue,
			// 			'FeeText' => $paid_text,
			// 		);
			// 	}
			// }

			$standard  = $this->db->query("SELECT `course_id` FROM sc_admission WHERE student_id = '".$this->request->get['filter_student_id']."'")->row;
			//echo "<pre>";print_r($standard);exit;
			//echo "SELECT * FROM sc_standardfees WHERE standard_id = '".$standard['standard_id']."' AND fees_value <> '0'";
			$fees_types  = $this->db->query("SELECT * FROM sc_standardfees WHERE standard_id = '".$standard['course_id']."' AND fees_value <> '0'")->rows;
			//echo "<pre>";print_r($fees_types);exit;

			foreach($fees_types as $fkey => $fvalue){
				// echo"<pre>";
				// print_r($fvalue);exit;
				$paid_text = '';
				$applicable = 1;
				// if($fvalue['fees_name'] == 'admission'){
				// 	$is_admission = $this->db->query("SELECT `is_admission` FROM `sc_student` WHERE `student_id` = '".$result['student_id']."' ")->row['is_admission'];
				// 	if($is_admission == '0'){
				// 		$applicable = 0;
				// 	}
				// }elseif($fvalue['fees_name'] == 'bus'){
				// 	$is_bus = $this->db->query("SELECT `is_bus` FROM `sc_student` WHERE `student_id` = '".$result['student_id']."' ")->row['is_bus'];
				// 	if($is_bus == '0'){
				// 		$applicable = 0;
				// 	}
				// } elseif($fvalue['fees_name'] == 'martial'){
				// 	$is_marshal = $this->db->query("SELECT `is_marshal` FROM `sc_student` WHERE `student_id` = '".$result['student_id']."' ")->row['is_marshal'];
				// 	if($is_marshal == '0'){
				// 		$applicable = 0;
				// 	}
				// } elseif($fvalue['fees_name'] == 'Computer fee'){
				// 	$fees_value = $this->db->query("SELECT `fees_value` FROM `sc_standardfees` WHERE `standard_id` = '".$fvalue['standard_id']."' AND `fees_value` = '".$fvalue['fees_value']."' ")->row['fees_value'];
				// 	if($fees_value == '0'){
				// 		$applicable = 0;
				// 	}	
				// }else{
				// 	$applicable = '1';
				// }

				$sql = "SELECT ft.`month` FROM `sc_fees_transaction` ft LEFT JOIN `sc_fees_transaction_history` fth ON (ft.id = fth.id) WHERE 1=1 AND `student_id` = '".$result['student_id']."' AND `a_status` = '1' AND `fees_name` = '".$fvalue['fees_name']."' AND `status` = '1' AND `year` = '".$current_year."' LIMIT 1";
				$paid_data = $this->db->query($sql);
				if($paid_data->num_rows > 0){
					$paid_text = 'Paid';
				} else {
					$sql = "SELECT ft.`month`, fth.`fees_paid_value` FROM `sc_fees_transaction` ft RIGHT JOIN `sc_fees_transaction_history` fth ON (ft.id = fth.id) WHERE 1=1 AND `student_id` = '".$result['student_id']."' AND `a_status` = '1' AND `fees_name` = '".$fvalue['fees_name']."' AND `status` = '0' AND `year` = '".$current_year."' LIMIT 1";
					$unpaid_data = $this->db->query($sql);
					if($unpaid_data->num_rows > 0){
						$partial_paid_amount = $unpaid_data->row['fees_paid_value'];
						$partial_month = $months_name[$unpaid_data->row['month']];
						$paid_text = 'Partial Payment of Rs ' . $partial_paid_amount;
					} else {
						$paid_text = 'Not Paid';
					}
				}
				if($applicable == 1){
					$paid_datas[] = array(
						'FeeName' => $fvalue['fees_name'],
						'FeeText' => $paid_text,
					);
				}
			}

			//$this->log->write(print_r($paid_datas,true));

			$html = '';
			$html .= '<table class="list" style="width:50%;">';
				$html .= '<thead>';						
					$html .= '<tr>';
						$html .= '<td>';
							$html .= 'Fee Name';
						$html .= '</td>';
						$html .= '<td>';
							$html .= 'Payment Status';
						$html .= '</td>';
					$html .= '</tr>';							
				$html .= '</thead>';
				$html .= '<tbody>';
				foreach($paid_datas as $pkey => $pvalue){
					$html .= '<tr>';
						$html .= '<td>';
							$html .= $pvalue['FeeName'];
						$html .= '</td>';
						$html .= '<td>';
							$html .= $pvalue['FeeText'];
						$html .= '</td>';
					$html .= '</tr>';
				}
				$html .= '</tbody>';
			$html .= '</table>';
			$json = array(
				'paid_html' => $html
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}