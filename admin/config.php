<?php
// HTTP
 error_reporting(E_ALL);

define('HTTP_SERVER', 'http://localhost/ador/admin/');
define('TEST', 'http://5.161.114.135/hotel/hotelserver/');

define('HTTP_CATALOG', 'http://localhost/ador/');

define('HTTP_HOME', 'http://localhost/admin/index.php?route=common/login&user_id=1');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/ador/admin/');
define('HTTPS_CATALOG', 'http://localhost/ador/');

// DIR

define('DIR_APPLICATION', 'C:/xampp/htdocs/ador/admin/');
define('DIR',  'C:/xampp/htdocs/ador/');
define('DIR_SYSTEM',  'C:/xampp/htdocs/ador/system/');
define('DIR_IMAGE',  'C:/xampp/htdocs/ador/image/');
define('DIR_LANGUAGE',  'C:/xampp/htdocs/ador/admin/language/');
define('DIR_TEMPLATE',  'C:/xampp/htdocs/ador/admin/view/template/');
define('DIR_CONFIG',  'C:/xampp/htdocs/ador/system/config/');
define('DIR_CACHE',  'C:/xampp/htdocs/ador/system/storage/cache/');
define('DIR_DOWNLOAD',  'C:/xampp/htdocs/ador/system/storage/download/');
define('DIR_LOGS',  'C:/xampp/htdocs/ador/system/storage/logs/');
define('DIR_MODIFICATION',  'C:/xampp/htdocs/ador/system/storage/modification/');
define('DIR_UPLOAD',  'C:/xampp/htdocs/ador/system/storage/upload/');
define('DIR_CATALOG',  'C:/xampp/htdocs/ador/catalog/');
define('DIR_ACTIVATE',  'C:/xampp/htdocs/ador/system/library/escpos-php-developmentss/src/Mike42/Escpos/PrintBuffers/cache/core.php');

// for database backup it is compulsory to add drive name in which the xampp folder is present here my drive is D:/xampp/htdocs
define('DATABASE_BKP', 'C');



// DB ador
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'db_ador_2022');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
define('type', '1');

//DB HOTEL
define('DB_HOSTNAME2', 'localhost');
define('DB_USERNAME2', 'root');
define('DB_PASSWORD2', '');
define('DB_DATABASE2', 'db_hotel');

//DB 27
define('DB_HOSTNAME1', 'funshalla.in');
define('DB_USERNAME1', 'funshalla2018');
define('DB_PASSWORD1', 'funshalla2018');
define('DB_DATABASE1', 'db_hotel_fun');

define('FASTFOOD', '2');
define('WAITER','0');
define('CAPTAIN','0');
define('PERSONS','0');
define('SETTLEMENT','1');
define('SETTLEMENT_ON','0');
define('SERVICE_CHARGE_FOOD', '0');
define('SERVICE_CHARGE_LIQ', '0');
define('INCLUSIVE','1'); // 1 = Inclusive , 0 = Exclusive
define('NOCATEGORY','0'); // 1 = direct subcategory, 0 = category -> subcategory
define('CATEGORY','1'); // If direct subcategory is set to '1' this should also be set 

//For bill printing
define('PRINTER_NAME','192.168.1.152'); // for network 'ip address' & for windows 'name' // 192.168.1.152
define('PRINTER_TYPE','1'); // 1 for Network & 2 for Windows

define('BARCODE_PRINTER','XP-58C');

// Hotel Name and Address
define('HOTEL_NAME', 'ENATS');
define('HOTEL_ADD', "Shop No 5,Mansorovar\nOpp. Narayan E-School\nSwami Satyanand Marg\nBhayandar-W");
define('GST_NO', '123456');
define('TEXT1', '');
define('TEXT2', '');
define('TEXT3', 'Thank you visit again');
define('ONLINE_ORDER', 'Online Order');

//sms link
define('SMS_LINK_1', "http://bulksms.gfxbandits.com/api/sendmsg.php?user=funshalla&pass=xyz123@abc&sender=FUNSLA&priority=ndnd&stype=normal");
define('SMS_LINK_2', "http://bulksms.gfxbandits.com/api/sendmsg.php?user=funshalla1&pass=xyz123@abc&sender=FUNSLA&priority=ndnd&stype=normal");
define('SMS_LINK_3', "http://bulksms.gfxbandits.com/api/sendmsg.php?user=funshalla2&pass=xyz123@abc&sender=FUNSLA&priority=ndnd&stype=normal");
//define('BAR_NAME', 'YARRI');
//define('BAR_ADD', "Shop No 5,Mansorovar\nOpp. Narayan E-School\nSwami Satyanand Marg\nBhayandar-W");

//define('CONTACT_NUMBER','7977580214');
define('CONTACT_NUMBER','8850446788');

define('BAR_NAME', '');
define('BAR_ADD', "");
define('ADVANCE_NOTE',"Booking amount once paid\n will not be refunded");


define('SYNC_TO_SERVER', "http://113.193.26.178:81/ilc_1/services/sync_to/sync_to.php");

//for restorant Purpose Live Integration
// define('MERCHANT_ID', '1322');
// define('MERCHANT_ID_CANCEL', '1322');
// define('WERA_API_KEY', 'ec9efc82-0080-4d76-8e2c-3b29798733a5');

//for ador testing
define('MERCHANT_ID', '1169');
define('MERCHANT_ID_CANCEL', '1169');
define('WERA_API_KEY', 'd6cdb5fa-e694-49a3-8409-ee7564ce6ec5');
define('version', '0.0.0.1');

define('URBANPIPER_API_KEY', '');
define('API_DATABASE', '');
define('HOTEL_LOCATION_ID', '');


// define('CHECK_ONLINE_DATAS_URBANPIPER', "");
// define('FETCH_ONLINE_URBANPIPER_DATAS', "");
// define('FETCH_KOT_DATAS_URBANPIPER', "");
// define('FETCH_KOT_DATAS2_URBANPIPER', "");
// define('UPDATE_KOT_STATUS_URBANPIPER', "");

// define('CHECK_ONLINE_DATAS_URBANPIPER', "http://5.189.188.230/webhook_live_fordevloper/urban_piper_check_online_datas.php");
// define('FETCH_ONLINE_URBANPIPER_DATAS', "http://5.189.188.230/webhook_live_fordevloper/urban_piper_fetch_online_datas.php");
// define('FETCH_KOT_DATAS_URBANPIPER', "http://5.189.188.230/webhook_live_fordevloper/fetch_kot_datas.php");
// define('FETCH_KOT_DATAS2_URBANPIPER', "http://5.189.188.230/webhook_live_fordevloper/fetch_kot_datas2.php");
// define('UPDATE_KOT_STATUS_URBANPIPER', "http://5.189.188.230/webhook_live_fordevloper/urban_piper_kot_status.php");

define('CHECK_ONLINE_DATAS_URBANPIPER', "http://5.189.188.230/webhook_test_fordevloper/urban_piper_check_online_datas.php");
define('FETCH_ONLINE_URBANPIPER_DATAS', "http://5.189.188.230/webhook_test_fordevloper/urban_piper_fetch_online_datas.php");
define('FETCH_KOT_DATAS_URBANPIPER', "http://5.189.188.230/webhook_test_fordevloper/fetch_kot_datas.php");
define('FETCH_KOT_DATAS2_URBANPIPER', "http://5.189.188.230/webhook_test_fordevloper/fetch_kot_datas2.php");
define('UPDATE_KOT_STATUS_URBANPIPER', "http://5.189.188.230/webhook_test_fordevloper/urban_piper_kot_status.php");

// define('NEW_TRANSECTION_COUNT', "");

define('NEW_TRANSECTION_COUNT', "http://5.189.188.230/webhook_live_fordevloper/getnew_orders_count.php");

define('COUNT_NEW_ORDERS', "http://5.189.188.230/webhook_live_fordevloper/new_order_urbanpiper.php");//workable



// define('STOREAPI', "");

// define('ORDER_UPDATE_API', "");
// define('ITEM_ON_OFF', "");
// define('ITEM_API', "");
// define('PLATFORMON_OFF', "");
// define('CATEGORY_TIMING_WISE', "");

// define('Webhook_Api', "");
// define('Webhook_edit_Api', "");

// define('Callback_webhook', "");
// define('Callback_Store_LIVE', "");
// define('Callback_Platform_LIVE', "");
// define('Callback_Platform_ITEMS', "");

// define('Truncate_Basictable', "");


define('STOREAPI', "https://api.urbanpiper.com/external/api/v1/stores/");

define('ORDER_UPDATE_API', "https://api.urbanpiper.com/external/api/v1/orders/%s/status/");
define('ITEM_ON_OFF', "https://api.urbanpiper.com/hub/api/v1/items/");
define('ITEM_API', "https://api.urbanpiper.com/external/api/v1/inventory/locations/%s/");
define('PLATFORMON_OFF', "https://api.urbanpiper.com/hub/api/v1/location/");
define('CATEGORY_TIMING_WISE', "https://api.urbanpiper.com/external/api/v1/inventory/categories/timing-groups/");

define('Webhook_Api', "https://api.urbanpiper.com/external/api/v1/webhooks/");
define('Webhook_edit_Api', "https://api.urbanpiper.com/external/api/v1/webhooks/%u/");

define('Callback_webhook', "http://5.189.188.230/webhook_live_fordevloper/common_api.php");
define('Callback_Store_LIVE', "http://5.189.188.230/webhook_live_fordevloper/create_store_webhook_api.php");
define('Callback_Platform_LIVE', "http://5.189.188.230/webhook_live_fordevloper/platform_webhook_api.php");
define('Callback_Platform_ITEMS', "http://5.189.188.230/webhook_live_fordevloper/platform_item_webhook.php");

define('Truncate_Basictable', "http://5.189.188.230/webhook_live_fordevloper/truncate_query.php");



//define('UPDATE_KOT_STATUS_URBANPIPER', "");
// define('UPDATE_ENTRY_STATUS_RIDER_URBANPIPER', "");
// define('FETCH_RIDER_URBANPIPER', "");

define('UPDATE_ENTRY_STATUS_RIDER_URBANPIPER', "http://5.189.188.230/webhook_live_fordevloper/urbanpiper_update_rider_entry_status.php");
define('FETCH_RIDER_URBANPIPER', "http://5.189.188.230/webhook_live_fordevloper/rider_status_fetch_local.php");
define('FETCH_UPDATE_ORDER_STATUS', "http://5.189.188.230/webhook_live_fordevloper/common_api_order_fetch_update.php");


////////////////////////////////////////////////////////////////////

define('CHECK_ONLINE_DATAS', "http://5.189.188.230/webhook_test_fordevloper/urban_piper_check_online_datas.php");
//define('FETCH_ONLINE_DATAS', "http://5.189.188.230/webhook_test_fordevloper/urban_piper_fetch_online_datas.php");
define('FETCH_KOT_DATAS', "http://5.189.188.230/webhook_test_fordevloper/fetch_kot_datas.php");
define('FETCH_KOT_DATAS2', "http://5.189.188.230/webhook_test_fordevloper/fetch_kot_datas2.php");
define('UPDATE_KOT_STATUS', "http://5.189.188.230/webhook_test_fordevloper/update_kot_status.php");

///////////////////////// current api ///////////////////////////////////////////

define('FETCH_ONLINE_DATAS', "http://5.189.188.230/mangalore_app_api/fetch_online_datas.php");

// define('CHECK_ONLINE_DATAS', "https://towfixers.in/ador_app_api/check_online_datas.php");
// define('FETCH_ONLINE_DATAS', "https://towfixers.in/ador_app_api/fetch_online_datas.php");
// define('FETCH_KOT_DATAS', "https://towfixers.in/ador_app_api/fetch_kot_datas.php");
// define('FETCH_KOT_DATAS2', "https://towfixers.in/ador_app_api/fetch_kot_datas2.php");
// define('UPDATE_KOT_STATUS', "https://towfixers.in/ador_app_api/update_kot_status.php");


