<?php
class ModelCatalogVenue extends Model {
	public function addvenue($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX .
				   "venue SET venue_name = '" .$this->db->escape($data['name']). "', 
					rate = '" . $this->db->escape($data['rate']) . "',
					rate_child = '" . $this->db->escape($data['rate_child']) . "', 
					capacity = '" . $this->db->escape($data['capacity'])."' ");
		$ven_id = $this->db->getLastId();
		return $ven_id;
	}

	public function editvenue($id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "venue SET venue_name = '" . $this->db->escape($data['name']) . "',
					rate = '" . $this->db->escape($data['rate']) . "',
					rate_child = '" . $this->db->escape($data['rate_child']) . "', 
					capacity = '" . $this->db->escape($data['capacity'])."'
					WHERE venue_id = '" . (int)$id . "'");
		return $id;
	}

	public function deletevenue($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "venue WHERE venue_id = '" . (int)$id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getvenue($id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";

		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "venue WHERE venue_id = '" . (int)$id . "' ");
		return $query->row;
	}

	public function getvenues($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "venue WHERE 1=1 ";
		if (!empty($data['filter_name'])) {
			$sql .= " AND venue_name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'venue_name',
			'sport_type'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY venue_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalvenue($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "venue WHERE 1=1 ";
		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}