<?php
class ModelCatalogWaiter extends Model {
	public function addWaiter($data) {
		$doj =  '0000-00-00';
		if ($data['doj'] != '0000-00-00' && $data['doj'] !='' )  {
			$doj = date('Y-m-d',strtotime($data['doj']));
		}

		if (!isset($data['commission'] )) {
			$data['commission'] = 0;
		}
		if (!isset($data['activate'] )) {
			$data['activate'] = 0;
		}
		$this->db->query("INSERT INTO " . DB_PREFIX .
				   "waiter SET name = '" .$this->db->escape($data['name']). "',
					roll = '" . $this->db->escape($data['roll'])."',
					address = '" . $this->db->escape($data['address'])."',
					refer = '" . $this->db->escape($data['refer'])."',
					place = '" . $this->db->escape($data['place'])."',
					pin = '" . $this->db->escape($data['pin'])."',
					contact = '" . $this->db->escape($data['contact'])."',
					doj = '" . $this->db->escape($doj)."',
					commission = '" . $this->db->escape($data['commission'])."',
					activate = '" . $this->db->escape($data['activate'])."',
					change_status = '" . $this->db->escape($data['change_status'])."',
					code = '" . $this->db->escape($data['code'])."' ");
		
		$waiter_id = $this->db->getLastId();

		return $waiter_id;
	}

	public function editWaiter($waiter_id, $data) {
		$doj =  '0000-00-00';
		if ($data['doj'] != '0000-00-00' && $data['doj'] !='' )  {
			$doj = date('Y-m-d',strtotime($data['doj']));
		}

		if (!isset($data['commission'] )) {
			$data['commission'] = 0;
		}
		if (!isset($data['activate'] )) {
			$data['activate'] = 0;
		}

		$this->db->query("UPDATE " . DB_PREFIX . "waiter SET name = '" . $this->db->escape($data['name']) . "',
															roll = '" . $this->db->escape($data['roll'])."',
															address = '" . $this->db->escape($data['address'])."',
															refer = '" . $this->db->escape($data['refer'])."',
															place = '" . $this->db->escape($data['place'])."',
															pin = '" . $this->db->escape($data['pin'])."',
															contact = '" . $this->db->escape($data['contact'])."',
															doj = '" . $this->db->escape($doj)."',
															commission = '" . $this->db->escape($data['commission'])."',
															activate = '" . $this->db->escape($data['activate'])."',
															change_status = '" . $this->db->escape($data['change_status'])."',
															code = '" .$this->db->escape($data['code']). "' 
															WHERE waiter_id = '" . (int)$waiter_id . "'");
		return $waiter_id;
	}

	public function deleteWaiter($waiter_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "waiter WHERE waiter_id = '" . (int)$waiter_id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getWaiter($waiter_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "waiter WHERE waiter_id = '" . (int)$waiter_id . "' ");
		return $query->row;
	}

	public function getWaiters($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "waiter WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		// if (!empty($data['filter_waiter_id'])) {
		// 	$sql .= " AND waiter_id = '" . $this->db->escape($data['filter_waiter_id']) . "' ";
		// }

		$sort_data = array(
			'name',
			'code',
			'roll'

		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalWaiter($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "waiter WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_waiter_id'])) {
			$sql .= " AND sport_id = '" . $this->db->escape($data['filter_waiter_id']) . "' ";
		}

		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}