<?php
class ModelCatalogReception extends Model {

	public function addFeedback($data) {
		// echo "<pre>";
		// print_r($data);
		// exit();
		$dob = $data['year_dob'].'-'.$data['month_dob'].'-'.$data['date_dob'];
		$doa = $data['year_doa'].'-'.$data['month_doa'].'-'.$data['date_doa'];
		$datas = $this->db->query("SELECT * FROM oc_customerinfo WHERE contact = '".$data['contact']."'");
		if ($datas->num_rows > 0) {
			$contact = $datas->row['contact'];
			$dob = $datas->row['dob'];
			$doa = $datas->row['anniversary'];

			$this->db->query("UPDATE  `oc_customerinfo` SET `name` = '".$this->db->escape($data['name'])."',
						contact = '".$contact."',
						email = '".$this->db->escape($data['email'])."',
						address = '".$this->db->escape($data['address'])."',
						dob = '".$dob."',
						anniversary = '".$doa."'
					 	WHERE contact = '".$contact."'");

			$this->db->query("INSERT INTO `oc_feedback` SET `name` = '".$this->db->escape($data['name'])."',
						contact_no = '".$contact."',
						table_no = '".$data['table_no']."',
						email = '".$this->db->escape($data['email'])."',
						date_dob = '".$data['date_dob']."',
						month_dob = '".$data['month_dob']."',
						year_dob = '".$data['year_dob']."',
						date_doa = '".$data['date_doa']."',
						month_doa = '".$data['month_doa']."',
						year_doa = '".$data['year_doa']."',
						`date` = '".date('Y-m-d')."'"  );
		}else{

			$this->db->query("INSERT INTO `oc_feedback` SET 
						`name` = '".$this->db->escape($data['name'])."',
						table_no = '".$data['table_no']."',
						contact_no = '".$data['contact']."',
						email = '".$this->db->escape($data['email'])."',
						date_dob = '".$data['date_dob']."',
						month_dob = '".$data['month_dob']."',
						year_dob = '".$data['year_dob']."',
						date_doa = '".$data['date_doa']."',
						month_doa = '".$data['month_doa']."',
						year_doa = '".$data['year_doa']."',
						`date` = '".date('Y-m-d')."' ");
			$cust_id = $this->db->getLastId();

			$this->db->query("INSERT INTO `oc_customerinfo` SET `name` = '".$this->db->escape($data['name'])."',
							contact = '".$data['contact']."',
							email = '".$this->db->escape($data['email'])."',
							address = '".$this->db->escape($data['address'])."',
							dob = '".$dob."',
							anniversary = '".$doa."'
							 ");
			$cust_id = $this->db->getLastId();
			$posted = false;
			return $posted;

		}

		//$kotbot = $botno;
		$this->load->model('catalog/order');

		$HOTEL_NAME = $this->model_catalog_order->get_settings('HOTEL_NAME');
		$hote_name=str_replace(" ", "%20", $HOTEL_NAME);
		$text = 'Welcome%20to%20'.$hote_name.'%0aHave%20a%20pleasant%20dining.%0ahttps://www.facebook.com/skybarzeal/'; 
		//$link_1 ='http://kutility.in/app/smsapi/index.php?key=35C23751BC0A7F&routeid=415&type=text&senderid=KUTILY&contacts=8850446788&msg='.$text;
		$setting_value_link_1 = $this->model_catalog_order->get_settings('SMS_LINK_1');
		$link_1 = html_entity_decode($setting_value_link_1, ENT_QUOTES, 'UTF-8');
		//$setting_value_number = $this->model_catalog_order->get_settings('CONTACT_NUMBER');
		$link = $link_1."&contacts=".$data['contact']."&msg=".$text;
		$ret = file($link);
		
	}


	public function editFeedback($feedback_id,$data) {
		// echo "<pre>";
		// print_r($data);
		// exit();

		$this->db->query("UPDATE `oc_fb_cus_detail` SET
					name = '".$this->db->escape($data['name'])."',
					contact_no = '".$this->db->escape($data['contact_no'])."',
					email = '".$this->db->escape($data['email'])."', 
					location = '".$this->db->escape($data['location'])."',
					dob = '".date('Y-m-d', strtotime($data['dob']))."',
					doa = '".date('Y-m-d', strtotime($data['doa']))."' 
					WHERE id = '".$feedback_id."'");
		//$cust_id = $this->db->getLastId();

	}
		

	public function deleteFeedback($feedback_id) {
		$this->db->query("DELETE FROM oc_feedback1 WHERE feedback_id = '" . (int)$feedback_id . "'");
	}

	public function getFeedback($feedback_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM oc_fb_cus_detail fcd LEFT JOIN oc_feedback_transaction fbt ON (fcd.`id` = fbt.`cust_id`) WHERE fcd.`id` = '" . (int)$feedback_id . "'");
		return $query->row;
	}

	public function getFeedbacks($data = array()) {
		$sql = "SELECT * FROM oc_feedback1";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_teacher_name'])) {
			$sql .= " AND `teacher_name` LIKE '%" . $this->db->escape($data['filter_teacher_name']) . "%'";
		}

		if (!empty($data['filter_teacher_id'])) {
			$sql .= " AND `teacher_id` = '" . $this->db->escape($data['filter_teacher_id']) . "'";
		}

		if (!empty($data['filter_division_name'])) {
			$sql .= " AND `division_name` LIKE '%" . $this->db->escape($data['filter_division_name']) . "%'";
		}

		if (!empty($data['filter_division_id'])) {
			$sql .= " AND `division_id` = '" . $this->db->escape($data['filter_division_id']) . "'";
		}

		if (!empty($data['filter_teacher_id'])) {
			$sql .= " AND `teacher_id` = '" . $this->db->escape($data['filter_teacher_id']) . "'";
		}

		if (!empty($data['filter_student_name'])) {
			$sql .= " AND `student_name` LIKE '%" . $this->db->escape($data['filter_student_name']) . "%'";
		}

		// if (!empty($data['filter_student_id'])) {
		// 	$sql .= " AND `student_id` = '" . $this->db->escape($data['filter_student_id']) . "'";
		// }

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND `date_added` = '" . $this->db->escape($data['filter_date_added']) . "'";
		}

		$sort_data = array(
			'teacher_name',
			'teacher_id',
			'student_name',
			'student_id',
			'date_added',
			'total_mark',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY `date_added`";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalFeedbacks($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_fb_cus_detail WHERE 1=1";
		
		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getcontacts($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "fb_cus_detail WHERE 1=1 ";
		if (!empty($data['filter_name'])) {
			$sql .= " AND contact_no = '" . $this->db->escape($data['filter_name']) . "'";
		}
			$sql.= " ORDER BY id DESC LIMIT 1 ";

			// echo $sql;
			// exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

}