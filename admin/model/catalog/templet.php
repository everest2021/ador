<?php
class ModelCatalogTemplet extends Model {
	public function addTeacher($data) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$this->db->query("INSERT INTO oc_template SET 
							tem_name = '" . $this->db->escape($data['tem_name']) . "',
							tem_description = '" . $this->db->escape($data['tem_description']) . "',
							status = '" . $this->db->escape($data['status']) . "'
						");
		$tem_id = $this->db->getLastId();
		return $tem_id;
	}

	public function editTeacher($tem_id, $data) {
		$this->db->query("UPDATE oc_template SET 
							tem_name = '" . $this->db->escape($data['tem_name']) . "',
							tem_description = '" . $this->db->escape($data['tem_description']) . "',
							status = '" . $this->db->escape($data['status']) . "'
							WHERE tem_id = '" . (int)$tem_id . "'");
	}

	public function deleteTeacher($tem_id) {
		$this->db->query("DELETE FROM oc_template WHERE tem_id = '" . (int)$tem_id . "'");
	}

	public function getTeacher($tem_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM oc_template WHERE tem_id = '" . (int)$tem_id . "'");

		return $query->row;
	}

	public function getTeachers($data = array()) {

		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM oc_template";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_tem_name'])) {
			$sql .= " AND tem_name LIKE '%" . $this->db->escape($data['filter_tem_name']) . "%'";
		}

		if (!empty($data['filter_tem_id'])) {
			$sql .= " AND tem_id = '" . $this->db->escape($data['filter_tem_id']) . "'";
		}

		$sort_data = array(
			'tem_name',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY tem_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		//echo $sql;exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalTeachers($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_template";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_tem_id'])) {
			$sql .= " AND tem_id = '" . $this->db->escape($data['filter_tem_id']) . "'";
		}

		if (!empty($data['filter_tem_name'])) {
			$sql .= " AND tem_name LIKE '%" . $this->db->escape($data['filter_tem_name']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
}