<?php
class ModelCatalogCustomerCredit extends Model {
	public function addCredit($data) {
		$this->db->query("INSERT INTO oc_customercredit SET 
							c_id = '".$data['c_id']."',
							customer_name = '".$data['customername']."',
							contact = '".$data['contact']."',
							credit = '".$data['credit']."',
							msr_no = '".$data['msr_no']."',
							date = '".date('Y-m-d')."'
						");
		return $this->db->getLastId();
	}

	public function editCredit($item_id,$data) {
		$this->db->query("UPDATE oc_customercredit SET 
							c_id = '".$data['c_id']."',
							customer_name = '".$data['customername']."',
							contact = '".$data['contact']."',
							credit = '".$data['credit']."',
							msr_no = '".$data['msr_no']."',
							date = '".date('Y-m-d')."'
							WHERE id='".$item_id."'
						");
	}

	public function deleteCredit($item_id) {
		$this->db->query("DELETE FROM oc_customercredit WHERE id = '" . (int)$item_id . "'");
	}

	public function getCredit($item_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM oc_customercredit WHERE id = '" . (int)$item_id . "' ");
		return $query->row;
	}

	public function getCredits($data = array()) {
		$sql = "SELECT SUM(credit) as credit, id, customer_name, contact,msr_no FROM oc_customercredit WHERE 1=1 ";

		if (isset($data['filter_item_id']) && !empty($data['filter_item_id'])) {
			$sql .= " AND id = '" . $data['filter_item_id'] . "' ";
		}

		if (!empty($data['filter_customername'])) {
			$sql .= " AND customer_name LIKE '%" . $this->db->escape($data['filter_customername']) . "%'";
			$sql .= " OR contact LIKE '%" . $this->db->escape($data['filter_customername']) . "%'";
		}

		$sort_data = array(
			'customer_name',
			'contact',
			'credit'
		);

		$sql .= " GROUP BY contact";  

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY customer_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;

		//$this->log->write($sql);

		$query = $this->db->query($sql);

		// echo "<pre>";
		// print_r($query);
		// exit();

		return $query->rows;
	}

	public function getTotalCredits($data = array()) {
		$sql = "SELECT * FROM oc_customercredit WHERE 1=1 ";

		if (isset($data['filter_item_id']) && !empty($data['filter_item_id'])) {
			$sql .= " AND id = '" . $data['filter_item_id'] . "' ";
		}

		if (!empty($data['filter_customername'])) {
			$sql .= " AND customer_name LIKE '%" . $this->db->escape($data['filter_customername']) . "%'";
		}

		$sql .= " GROUP BY contact";  

		$query = $this->db->query($sql);
		return $query->num_rows;
	}
}