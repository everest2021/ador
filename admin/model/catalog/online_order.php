<?php
class ModelCatalogOnlineOrder extends Model {
	public function addOrder($order_id) {
		// echo "<pre>";
		// print_r($order_id);
		// exit;
		$results = $this->db->query("SELECT * FROM oc_orders_app WHERE live_order_id = '".$order_id."' ")->rows;

		$result1 = array();
		foreach ($results as $result) {

			$prev_details = $this->db->query("SELECT kot_no, table_id FROM oc_order_info ORDER BY order_id DESC LIMIT 1 ");
			if ($prev_details->num_rows > 0) {
				$kot_no = (int)$prev_details->row['kot_no'] + 1;
				$table_id = (int)$prev_details->row['table_id'] + 1;
			} else {
				$kot_no = 1;
				$table_id = 5000;
			}


			$cust_details = $this->db->query("SELECT local_customer_id, CONCAT(firstname, ' ', lastname) AS cust_name, CONCAT(address_1, ' ', address_2) AS address FROM oc_address_app WHERE local_customer_id = '".$result['cust_id']."' ");
			if ($cust_details->num_rows > 0) {
				$cust_name = $cust_details->row['cust_name'];
				$cust_id = $cust_details->row['local_customer_id'];
				$cust_address = $cust_details->row['address'];
			} else {
				$cust_name = '';
				$cust_id = 0;
				$cust_address = '';
			}
			$cust_details2 = $this->db->query("SELECT telephone, email FROM oc_customer_app WHERE local_customer_id = '".$result['cust_id']."' ");
			if ($cust_details2->num_rows > 0) {
				$cust_contact = $cust_details2->row['telephone'];
				$cust_email = $cust_details2->row['email'];
			} else {
				$cust_contact = '';
				$cust_email = '';
			}

			$other_details = $this->db->query("SELECT bill_date FROM oc_order_items ORDER BY id ASC LIMIT 1 ");
			if ($other_details->num_rows > 0) {
				$bill_date = $other_details->row['bill_date'];
			} else {
				$bill_date = date('Y-m-d');
			}

			$grand_totals = number_format($result['grand_tot'],2,'.','');
			$grand_total_explode = explode('.', $grand_totals);
			$roundtotal = 0;
			if(isset($grand_total_explode[1])){
				if($grand_total_explode[1] >= 50){
					$roundtotals = 100 - $grand_total_explode[1];
					$roundtotal = ($roundtotals / 100); 
				} else {
					$roundtotal = -($grand_total_explode[1] / 100);
				}
			}
			
			$other_details2 = $this->db->query("SELECT COUNT(*) AS total_items, SUM(qty) AS item_quantity FROM oc_order_item_app WHERE order_id = '".$result['order_id']."' ");
			if ($other_details2->num_rows > 0) {
				$total_items = $other_details2->row['total_items'];
				$item_quantity = $other_details2->row['item_quantity'];
			} else {
				$total_items = 0;
				$item_quantity = 0;
			}

			//order_items

			// echo "<pre>";
			// print_r($result['order_id']);
			// exit;
			$results2 = $this->db->query("SELECT * FROM oc_order_item_app WHERE order_id = '".$order_id."' ")->rows;

			//echo "<pre>";print_r("SELECT * FROM oc_order_item_app WHERE order_id = '".$order_id."'");exit;

			$online_orders_item = array();
			foreach ($results2 as $result2) {
				$subcats = $this->db->query("SELECT item_sub_category_id, vat FROM oc_item WHERE item_code = '".$result2['item_id']."' ");
				if ($subcats->num_rows > 0) {
					$subcat_id = $subcats->row['item_sub_category_id'];
					$gst = $subcats->row['vat'];
				} else {
					$subcat_id = 0;
					$gst = 0;
				}
				$gst_val = ($result2['total'] * $gst)/100;
				$online_orders_item[] = array(
					'item_id' => $result2['item_id'],
					'item'        => $result2['item'],
					'qty' => $result2['qty'],
					'price' => round($result2['price'], 2),
					'total' => round($result2['total'], 2),
					'subcat_id' => $subcat_id,
					'special_notes' => $result2['special_notes'],
					'gst' => $gst,
					'gst_val' => $gst_val,
				);

			}
			//order_items

			$result1['online_orders'][] = array(
				'kot_no' => $kot_no,
				'table_id' => $table_id,
				'total' => round($result['total'], 2),
				'grand_tot' => round($result['grand_tot'], 2),
				'gst_val' => $result['gst_val'],
				'cust_name' => $cust_name,
				'cust_id' => $cust_id,
				'cust_contact' => $cust_contact,
				'cust_address' => $result['address_1'].' '.$result['address_2'],
				'cust_email' => $cust_email,
				'bill_date' => $bill_date,
				'roundtotal' => $roundtotal,
				'total_items' => $total_items,
				'item_quantity' => $item_quantity,
				'online_orders_item' => $online_orders_item
			);
		}

		// $url = FETCH_KOT_DATAS.'?order_id='.$order_id;
		// $data_json = json_encode($order_id);
		// $ch = curl_init();
		// curl_setopt($ch, CURLOPT_URL, $url);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		// curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
		// $response  = curl_exec($ch);
		// $results = json_decode($response,true);
		// echo "<pre>";
		// print_r($results);
		// exit;

		foreach ($result1['online_orders'] as $o2key => $o2value) { //echo "<pre>";print_r($o2value);exit;

			$user_id = $this->user->getId();
			$user_name = $this->user->getUserName();

			$this->db->query("INSERT INTO oc_order_info SET
				app_order_id = '".$order_id."',
				online_order_id = '".$order_id."',
				kot_no = '".$o2value['kot_no']."',
				order_no = '0',
				merge_number = '0',
				location = 'Online',
				location_id = '10000',
				t_name = '".$o2value['table_id']."',
				table_id = '".$o2value['table_id']."',
				waiter_code = '',
				waiter = '',
				captain_code = '',
				captain = '',
				captain_id = '',
				person = '0',
				ftotal = '".$o2value['total']."',
				ftotal_discount = '0',
				gst = '".$o2value['gst_val']."',
				ltotal = '0',
				ltotal_discount = '0',
				vat = '0',
				date = '".date('Y-m-d')."',
				time = '".date('h:i:s')."',
				bill_status = '0',
				payment_status = '0',
				cust_name = '".$o2value['cust_name']."',
				cust_id = '".$o2value['cust_id']."',
				cust_contact = '".$o2value['cust_contact']."',
				cust_address = '".$o2value['cust_address']."',
				cust_email = '".$o2value['cust_email']."',
				day_close_status = '0',
				bill_date = '".$o2value['bill_date']."',
				rate_id = '',
				grand_total = '".$o2value['grand_tot']."',
				roundtotal = '".$o2value['roundtotal']."',
				total_items = '".$o2value['total_items']."',
				item_quantity = '".$o2value['item_quantity']."',
				date_added = '".date('Y-m-d')."',
				time_added = '".date('h:i:s')."',
				login_id = '".$user_id."',
				login_name = '".$user_name."'
			");

			//$order_id = $this->db->getLastId();

			foreach ($o2value['online_orders_item'] as $okey => $ovalue) {

				$this->db->query("INSERT INTO oc_order_items SET
					order_id = '".$order_id."',
					billno = '0',
					nc_kot_status = '0',
					nc_kot_reason = '',
					code = '".$ovalue['item_id']."',
					name = '".$ovalue['item']."',
					qty = '".$ovalue['qty']."',
					transfer_qty = '0',
					rate = '".$ovalue['price']."',
					ismodifier = '1',
					parent_id = '',
					parent = '',
					cancelmodifier = '',
					amt = '".$ovalue['total']."',
					new_amt = '0',
					new_qty = '0',
					stax = '0',
					new_rate = '0',
					new_discount_per = '0',
					new_discount_value = '0',
					subcategoryid = '".$ovalue['subcat_id']."',
					message = '".$ovalue['special_notes']."',
					is_liq = '0',
					kot_status = 1,
					pre_qty = '0',
					prefix = '+',
					is_new = '0',
					kot_no = '".$o2value['kot_no']."',
					reason = '',
					discount_per = '0',
					discount_value = '0',
					tax1 = '".$ovalue['gst']."',
					tax1_value = '".$ovalue['gst_val']."',
					tax2 = '0',
					tax2_value = '0',
					bk_status = '0',
					cancelstatus = '0',
					login_id = '".$user_id."',
					login_name = '".$user_name."',
					cancel_bill = '0',
					bill_modify = '0',
					time = '".date('h:i:s')."',
					date = '".date('Y-m-d')."',
					printstatus = '0',
					captain_id = '',
					captain_commission = '',
					waiter_id = '',
					pd_status = '0',
					complimentary_status = '0',
					complimentary_resion = '',
					bill_date = '".$o2value['bill_date']."',
					kitchen_dis_status = '0',
					kitchen_display = '0',
					cancel_kot_reason = '',
					packaging_amt = '0'
				");
			}
		}
		
		return $order_id;
	}

	public function editLocation($location_id, $data) {
		if (!isset($data['parcel_detail'] )) {
			$data['parcel_detail'] = 0;
		}
		if (!isset($data['dboy_detail'] )) {
			$data['dboy_detail'] = 0;
		}

		if (!isset($data['a_to_z'] )) {
			$data['a_to_z'] = 0;
		}
		if (!isset($data['direct_bill'] )) {
			$data['direct_bill'] = 0;
		}
		if (!isset($data['is_app'] )) {
			$data['is_app'] = 0;
		}

		if (!isset($data['is_advance'] )) {
			$data['is_advance'] = 0;
		}

		if (!isset($data['kot_different'] )) {
			$data['kot_different'] = 0;
		}

		if (!isset($data['service_charge'] )) {
			$data['service_charge'] = 0;
		}
		
		$this->db->query("UPDATE " . DB_PREFIX . "location SET 
						location = '" . $this->db->escape($data['location']) . "',
						table_to = '" .$this->db->escape($data['table_to']). "',
						table_from = '" .$this->db->escape($data['table_from']). "',
						bill_copy = '" .$this->db->escape($data['bill_copy']). "',
						master_kot = '" .$this->db->escape($data['master_kot']). "',
						kot_copy = '" .$this->db->escape($data['kot_copy']). "',
						bot_copy = '" .$this->db->escape($data['bot_copy']). "',
						
						store_name = '" .$this->db->escape($data['store_name']). "',
						parcel_detail = '" .$this->db->escape($data['parcel_detail']). "',
						dboy_detail = '" .$this->db->escape($data['dboy_detail']). "',
						a_to_z = '" .$this->db->escape($data['a_to_z']). "',
						direct_bill = '" .$this->db->escape($data['direct_bill']). "',
						is_app = '" .$this->db->escape($data['is_app']). "',
						is_advance = '" .$this->db->escape($data['is_advance']). "',
						kot_different = '" .$this->db->escape($data['kot_different']). "',
						service_charge = '" .$this->db->escape($data['service_charge']). "',
						bill_printer_name = '" .$this->db->escape($data['bill_printer_name']). "',
						bill_printer_type = '" .$this->db->escape($data['bill_printer_type']). "',
						rate_id = '" . $this->db->escape($data['rate_id']) . "'
						WHERE location_id = '" . (int)$location_id . "'");
		return $location_id;
	}

	public function deleteLocation($location_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$location_id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getLocation($Location_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$Location_id . "' ");
		return $query->row;
	}

	public function getOnlineorder($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "order_item_app WHERE 1=1 ";

		// if (!empty($data['filter_location'])) {
		// 	$sql .= " AND location LIKE '" . $this->db->escape($data['filter_location']) . "%'";
		// }

		// if (!empty($data['filter_location_id'])) {
		//  	$sql .= " AND location_id = '" . $this->db->escape($data['filter_location_id']) . "' ";
		// }

		// $sort_data = array(
		// 	'location',
		// 	'table_from',
		// 	'table_to'
		// 	);

		// if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
		// 	$sql .= " ORDER BY " . $data['sort'];
		// } else {
		// 	$sql .= " ORDER BY location";
		// }

		// if (isset($data['order']) && ($data['order'] == 'DESC')) {
		// 	$sql .= " DESC";
		// } else {
		// 	$sql .= " ASC";
		// }

		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}

		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalOrders($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_item_app WHERE 1=1 ";

		// if (!empty($data['filter_location'])) {
		// 	$sql .= " AND location LIKE '" . $this->db->escape($data['filter_location']) . "%'";
		// }

		// if (!empty($data['filter_location_id'])) {
		// 	$sql .= " AND location_id = '" . $this->db->escape($data['filter_location_id']) . "' ";
		// }

		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function get_settings($key) {
		$setting_status = $this->db->query("SELECT * FROM settings_ador WHERE `key` = '".$key."'");
		//$this->log->write($setting_status);
		if($setting_status->num_rows > 0){
			return $setting_status->row['value'];
		} else {
			return '';
		}
	}
}