<?php
class ModelCatalogOnlineOrder extends Model {
	public function addOrder($order_id) {

		$results = $this->db->query("SELECT SUM(total) AS total, SUM(qty) AS item_quantity, COUNT(*) AS total_items, SUM(total_value) AS grand_tot, SUM(gst_value) AS gst_val, cust_id, order_id, order_date AS bill_date, p_status FROM local_cart WHERE order_id = '".$order_id."' ")->rows;

		$result1 = array();
		foreach ($results as $result) { //echo "<pre>";print_r($results);exit;

			$roundtotal = 0;

			$prev_details = $this->db->query("SELECT kot_no, table_id FROM oc_order_info ORDER BY order_id DESC LIMIT 1 ");
			if ($prev_details->num_rows > 0) {
				$kot_no = (int)$prev_details->row['kot_no'] + 1;
				$table_id = (int)$prev_details->row['table_id'] + 1;
			} else {
				$kot_no = 1;
				$table_id = 5000;
			}

			$cust_id = $result['cust_id'];
			$bill_date = $result['bill_date'];
			$total_items = $result['total_items'];
			$item_quantity = $result['item_quantity'];
			$payment_status = $result['p_status'];
			
			if ($payment_status == '0') {
				$payment_type = 'Website cash';
			} elseif ($payment_status == '1') {
				$payment_type = 'Website Online';
			} else {
				$payment_type = 'Website Payment';
			}

			$result['address'] = '';
			$cust_name = '';
			$cust_contact = '';
			$cust_email = '';
			$cust_datas_sql = $this->db->query("SELECT cust_name, CONCAT(address, ' ', address2, ' ', address3, ' ', address4) AS address, telephone AS cust_contact, email AS cust_email FROM local_customer WHERE live_cust_id = '".$result['cust_id']."' ");
			if ($cust_datas_sql->num_rows > 0) {
				$result['address'] = $cust_datas_sql->row['address'];
				$cust_name = $cust_datas_sql->row['cust_name'];
				$cust_contact = $cust_datas_sql->row['cust_contact'];
				$cust_email = $cust_datas_sql->row['cust_email'];
			}

			$orders_item_sql = $this->db->query("SELECT p_id AS item_id, p_name AS item, qty, price, total, gst, gst_value, note AS special_notes FROM local_cart WHERE order_id = '".$order_id."' ")->rows;

			$online_orders_item = array();
			foreach ($orders_item_sql as $key => $result2) {

				$gst = 0;
				$gst_val = 0;
				$subcats = $this->db->query("SELECT item_sub_category_id AS subcat_id FROM oc_item WHERE item_id = '".$result2['item_id']."' ");
				if ($subcats->num_rows > 0) {
					$subcat_id = $subcats->row['subcat_id'];
				} else {
					$subcat_id = 0;
				}


				//order_items
				$online_orders_item[] = array(
					'item_id' => $result2['item_id'],
					'item'        => $result2['item'],
					'qty' => $result2['qty'],
					'price' => round($result2['price'], 2),
					'total' => round($result2['total'], 2),
					'subcat_id' => $subcat_id,
					'special_notes' => $result2['special_notes'],
					'gst' => round($result2['gst'], 2),
					'gst_val' => round($result2['gst_value'], 2),
				);
			}

			//orders
			$result1['online_orders'][] = array(
				'kot_no' => $kot_no,
				'table_id' => $table_id,
				'total' => round($result['total'], 2),
				'grand_tot' => round($result['grand_tot'], 2),
				'gst_val' => $result['gst_val'],
				'cust_name' => $cust_name,
				'cust_id' => $cust_id,
				'cust_contact' => $cust_contact,
				'cust_address' => $result['address'],
				'cust_email' => $cust_email,
				'bill_date' => $bill_date,
				'roundtotal' => $roundtotal,
				'total_items' => $total_items,
				'item_quantity' => $item_quantity,
				'online_orders_item' => $online_orders_item
			);
		}

		foreach ($result1['online_orders'] as $o2key => $o2value) { //echo "<pre>";print_r($result1['online_orders']);exit;

			$user_id = $this->user->getId();
			$user_name = $this->user->getUserName();

			$this->db->query("INSERT INTO oc_order_info SET
				app_order_id = '0',
				online_order_id = '".$order_id."',
				kot_no = '".$o2value['kot_no']."',
				order_no = '0',
				merge_number = '0',
				location = '".$payment_type."',
				payment_type = '".$payment_type."',
				location_id = '10000',
				t_name = '".$o2value['table_id']."',
				table_id = '".$o2value['table_id']."',
				waiter_code = '',
				waiter = '',
				captain_code = '',
				captain = '',
				captain_id = '',
				person = '0',
				ftotal = '".$o2value['total']."',
				ftotal_discount = '0',
				gst = '".$o2value['gst_val']."',
				ltotal = '0',
				ltotal_discount = '0',
				vat = '0',
				date = '".date('Y-m-d')."',
				time = '".date('h:i:s')."',
				bill_status = '0',
				payment_status = '0',
				cust_name = '".$o2value['cust_name']."',
				cust_id = '".$o2value['cust_id']."',
				cust_contact = '".$o2value['cust_contact']."',
				cust_address = '".$o2value['cust_address']."',
				cust_email = '".$o2value['cust_email']."',
				day_close_status = '0',
				bill_date = '".$o2value['bill_date']."',
				rate_id = '',
				grand_total = '".$o2value['grand_tot']."',
				roundtotal = '".$o2value['roundtotal']."',
				total_items = '".$o2value['total_items']."',
				item_quantity = '".$o2value['item_quantity']."',
				date_added = '".date('Y-m-d')."',
				time_added = '".date('h:i:s')."',
				login_id = '".$user_id."',
				login_name = '".$user_name."'
			");

			$order_id = $this->db->getLastId();

			foreach ($o2value['online_orders_item'] as $okey => $ovalue) { //echo "<pre>";print_r($o2value['online_orders_item']);exit;

				$this->db->query("INSERT INTO oc_order_items SET
					order_id = '".$order_id."',
					billno = '0',
					nc_kot_status = '0',
					nc_kot_reason = '',
					code = '".$ovalue['item_id']."',
					name = '".$ovalue['item']."',
					qty = '".$ovalue['qty']."',
					transfer_qty = '0',
					rate = '".$ovalue['price']."',
					ismodifier = '1',
					parent_id = '',
					parent = '',
					cancelmodifier = '',
					amt = '".$ovalue['total']."',
					new_amt = '0',
					new_qty = '0',
					stax = '0',
					new_rate = '0',
					new_discount_per = '0',
					new_discount_value = '0',
					subcategoryid = '".$ovalue['subcat_id']."',
					message = '".$ovalue['special_notes']."',
					is_liq = '0',
					kot_status = 1,
					pre_qty = '0',
					prefix = '+',
					is_new = '0',
					kot_no = '".$o2value['kot_no']."',
					reason = '',
					discount_per = '0',
					discount_value = '0',
					tax1 = '".$ovalue['gst']."',
					tax1_value = '".$ovalue['gst_val']."',
					tax2 = '0',
					tax2_value = '0',
					bk_status = '0',
					cancelstatus = '0',
					login_id = '".$user_id."',
					login_name = '".$user_name."',
					cancel_bill = '0',
					bill_modify = '0',
					time = '".date('h:i:s')."',
					date = '".date('Y-m-d')."',
					printstatus = '0',
					captain_id = '',
					captain_commission = '',
					waiter_id = '',
					pd_status = '0',
					complimentary_status = '0',
					complimentary_resion = '',
					bill_date = '".$o2value['bill_date']."',
					kitchen_dis_status = '0',
					kitchen_display = '0',
					cancel_kot_reason = '',
					packaging_amt = '0'
				");
			}
		}
		
		return $order_id;
	}

	public function editLocation($location_id, $data) {
		if (!isset($data['parcel_detail'] )) {
			$data['parcel_detail'] = 0;
		}
		if (!isset($data['dboy_detail'] )) {
			$data['dboy_detail'] = 0;
		}

		if (!isset($data['a_to_z'] )) {
			$data['a_to_z'] = 0;
		}
		if (!isset($data['direct_bill'] )) {
			$data['direct_bill'] = 0;
		}
		if (!isset($data['is_app'] )) {
			$data['is_app'] = 0;
		}

		if (!isset($data['is_advance'] )) {
			$data['is_advance'] = 0;
		}

		if (!isset($data['kot_different'] )) {
			$data['kot_different'] = 0;
		}

		if (!isset($data['service_charge'] )) {
			$data['service_charge'] = 0;
		}
		
		$this->db->query("UPDATE " . DB_PREFIX . "location SET 
						location = '" . $this->db->escape($data['location']) . "',
						table_to = '" .$this->db->escape($data['table_to']). "',
						table_from = '" .$this->db->escape($data['table_from']). "',
						bill_copy = '" .$this->db->escape($data['bill_copy']). "',
						master_kot = '" .$this->db->escape($data['master_kot']). "',
						kot_copy = '" .$this->db->escape($data['kot_copy']). "',
						bot_copy = '" .$this->db->escape($data['bot_copy']). "',
						
						store_name = '" .$this->db->escape($data['store_name']). "',
						parcel_detail = '" .$this->db->escape($data['parcel_detail']). "',
						dboy_detail = '" .$this->db->escape($data['dboy_detail']). "',
						a_to_z = '" .$this->db->escape($data['a_to_z']). "',
						direct_bill = '" .$this->db->escape($data['direct_bill']). "',
						is_app = '" .$this->db->escape($data['is_app']). "',
						is_advance = '" .$this->db->escape($data['is_advance']). "',
						kot_different = '" .$this->db->escape($data['kot_different']). "',
						service_charge = '" .$this->db->escape($data['service_charge']). "',
						bill_printer_name = '" .$this->db->escape($data['bill_printer_name']). "',
						bill_printer_type = '" .$this->db->escape($data['bill_printer_type']). "',
						rate_id = '" . $this->db->escape($data['rate_id']) . "'
						WHERE location_id = '" . (int)$location_id . "'");
		return $location_id;
	}

	public function deleteLocation($location_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$location_id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getLocation($Location_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$Location_id . "' ");
		return $query->row;
	}

	public function getOnlineorder($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "order_item_app WHERE 1=1 ";

		// if (!empty($data['filter_location'])) {
		// 	$sql .= " AND location LIKE '" . $this->db->escape($data['filter_location']) . "%'";
		// }

		// if (!empty($data['filter_location_id'])) {
		//  	$sql .= " AND location_id = '" . $this->db->escape($data['filter_location_id']) . "' ";
		// }

		// $sort_data = array(
		// 	'location',
		// 	'table_from',
		// 	'table_to'
		// 	);

		// if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
		// 	$sql .= " ORDER BY " . $data['sort'];
		// } else {
		// 	$sql .= " ORDER BY location";
		// }

		// if (isset($data['order']) && ($data['order'] == 'DESC')) {
		// 	$sql .= " DESC";
		// } else {
		// 	$sql .= " ASC";
		// }

		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}

		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalOrders($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_item_app WHERE 1=1 ";

		// if (!empty($data['filter_location'])) {
		// 	$sql .= " AND location LIKE '" . $this->db->escape($data['filter_location']) . "%'";
		// }

		// if (!empty($data['filter_location_id'])) {
		// 	$sql .= " AND location_id = '" . $this->db->escape($data['filter_location_id']) . "' ";
		// }

		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function get_settings($key) {
		$setting_status = $this->db->query("SELECT * FROM settings_ador WHERE `key` = '".$key."'");
		//$this->log->write($setting_status);
		if($setting_status->num_rows > 0){
			return $setting_status->row['value'];
		} else {
			return '';
		}
	}
}