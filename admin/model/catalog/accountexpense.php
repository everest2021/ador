<?php
class ModelCatalogAccountExpense extends Model {
	public function addAccexpense($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX ."expense_account SET 
				    exp_acc_name = '" .$this->db->escape($data['exp_acc_name']). "',
				    exp_acc_type = '" .$this->db->escape($data['exp_acc_type']). "',
				    contact_no = '" .$this->db->escape($data['contact_no']). "',
					address = '" .$this->db->escape($data['address']). "',
				    gst_no = '" .$this->db->escape($data['gst_no']). "' ");
		$expense_id = $this->db->getLastId();

		return $expense_id;
	}

	public function editAccexpense($expense_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "expense_account SET 
						exp_acc_name = '" . $this->db->escape($data['exp_acc_name']) . "',
						exp_acc_type = '" .$this->db->escape($data['exp_acc_type']). "',
						contact_no = '" .$this->db->escape($data['contact_no']). "',
						address = '" .$this->db->escape($data['address']). "',
						gst_no = '" . $this->db->escape($data['gst_no']) . "'
						WHERE expense_id = '" . (int)$expense_id . "'");
		return $expense_id;
	}

	public function deleteAccexpense($expense_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "expense_account WHERE expense_id = '" . (int)$expense_id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getAccexpense($expense_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "expense_account WHERE expense_id = '" . (int)$expense_id . "' ");
		return $query->row;
	}

	public function getCategory(){
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."category");
		//echo "SELECT * FROM ".DB_PREFIX."location WHERE location_id='".(int)$location_id."'";
		return $query->rows;
	}

	public function getAccexpenses($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "expense_account` WHERE 1=1 ";

		if (!empty($data['filter_expense_id'])) {
			$sql .= " AND `expense_id` = '" . $this->db->escape($data['filter_expense_id']) . "' ";
		}

		if (!empty($data['filter_exp_acc_name'])) {
			$sql .= " AND acc_exp_name LIKE '%" . $this->db->escape($data['filter_acc_exp_name']) . "%' ";
		}

		$sort_data = array(
			'acc_exp_name',
			'expense_id'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY exp_acc_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;
		// exit();
		//$this->log->write($sql);

		$query = $this->db->query($sql);



		return $query->rows;
	}

	public function getTotalAccexpense($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "expense_account WHERE 1=1 ";

		if (!empty($data['filter_exp_acc_name'])) {
			$sql .= " AND `exp_acc_name` LIKE '%" . $this->db->escape($data['filter_exp_acc_name']) . "%'";
		}

		if (!empty($data['filter_expense_id'])) {
			$sql .= " AND `expense_id` = '" . $this->db->escape($data['filter_expense_id']) . "' ";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}