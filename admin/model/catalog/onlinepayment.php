<?php
class ModelCatalogOnlinePayment extends Model {
	public function addonlinepayment($data) {
		$this->db->query("INSERT INTO 
				   `oc_onlinepayment` SET payment_type = '" .$this->db->escape($data['payment_type']). "'  
					 ");
		$id = $this->db->getLastId();
		return $id;
	}

	public function editonlinepayment($id, $data) {
		$this->db->query("UPDATE `oc_onlinepayment` SET payment_type = '" . $this->db->escape($data['payment_type']) . "',
					payment_type = '" . $this->db->escape($data['payment_type'])."'
					WHERE id = '" . (int)$id . "'");
		return $id;
	}

	public function deleteonlinepayment($id) {
		// echo $id;
		// exit;
		$this->db->query("DELETE FROM oc_onlinepayment  WHERE id = '" . (int)$id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

/*onlinepaymentonlinepayment*/
	/*public function getonlinepayments($id) {*/
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";

	/*	//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM   `oc_onlinepayment`   WHERE id = '" . (int)$id . "' ");
		return $query->row;
	}*/

	public function getonlinepayment ($data = array()) {
		// echo'<pre>';
		// print_r($data);
		// exit();
		$sql = "SELECT * FROM " . DB_PREFIX . "onlinepayment WHERE 1=1 ";
		if (!empty($data)) {
			$sql .= " AND id = '" . $this->db->escape($data) . "'";
		}

		// $sort_data = array(
		// 	'onlinepayment',
		// 	'payment_type',
		// );

		// if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
		// 	$sql .= " ORDER BY " . $data['sort'];
		// } else {
		// 	$sql .= " ORDER BY payment_type";
		// }

		// if (isset($data['order']) && ($data['order'] == 'DESC')) {
		// 	$sql .= " DESC";
		// } else {
		// 	$sql .= " ASC";
		// }

		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}

		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }

		$query = $this->db->query($sql);
		return $query->row;
	}
	

	public function getonlinepayments($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "onlinepayment WHERE 1=1 ";
		if (!empty($data['filter_name'])) {
			$sql .= " AND payment_type LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'onlinepayment',
			'payment_type',
			
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY payment_type";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalonlinepayment($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "onlinepayment WHERE 1=1 ";
		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}