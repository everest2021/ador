<?php
class ModelCatalogCategory extends Model {
	public function addCategory($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX ."category SET 
					category = '" .$this->db->escape($data['category']). "',
					colour = '" .$this->db->escape($data['colour']). "',
				    photo = '" .$this->db->escape($data['photo']). "',
				    change_status = '" . $this->db->escape($data['change_status'])."',
					photo_source = '" .$this->db->escape($data['photo_source']). "'");
		$category_id = $this->db->getLastId();

		return $category_id;
	}

	public function editCategory($category_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "category SET 
						category = '" . $this->db->escape($data['category']) . "',
						photo = '" .$this->db->escape($data['photo']). "',
						colour = '" .$this->db->escape($data['colour']). "',
						change_status = '" . $this->db->escape($data['change_status'])."',
						photo_source = '" .$this->db->escape($data['photo_source']). "'
						WHERE category_id = '" . (int)$category_id . "'");
		return $category_id;
	}

	public function deleteCategory($category_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getCategory($category_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "' ");
		return $query->row;
	}

	public function getLocation(){
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."category");
		//echo "SELECT * FROM ".DB_PREFIX."location WHERE location_id='".(int)$location_id."'";
		return $query->rows;
	}

	public function getCategorys($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "category` WHERE 1=1 ";

		if (!empty($data['filter_category'])) {
			$sql .= " AND `category` LIKE '%" . $this->db->escape($data['filter_category']) . "%'";
		}

		if (!empty($data['filter_category_id'])) {
			$sql .= " AND `category_id` = '" . $this->db->escape($data['filter_category_id']) . "' ";
		}

		$sort_data = array(
			'category'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY category";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);



		return $query->rows;
	}

	public function getTotalCategory($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category WHERE 1=1 ";

		if (!empty($data['filter_category'])) {
			$sql .= " AND `category` LIKE '%" . $this->db->escape($data['filter_category']) . "%'";
		}

		if (!empty($data['filter_category_id'])) {
			$sql .= " AND `category_id` = '" . $this->db->escape($data['filter_category_id']) . "' ";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}