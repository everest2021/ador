<?php
class ModelCatalogSupplier extends Model {
	public function addWaiter($data) {

		// if (!isset($data['show_in_purchase'])) {
		// 	$data['show_in_purchase'] = 0;
		// }
		$this->db->query("INSERT INTO " . DB_PREFIX .
				   "supplier SET supplier = '" .$this->db->escape($data['supplier']). "',
				   code = '" .$this->db->escape($data['code']). "',
				   address = '" .$this->db->escape($data['address']). "',
				   vat_no = '" .$this->db->escape($data['vat_no']). "',
				   gst_no = '" .$this->db->escape($data['gst_no']). "',
				   city = '" .$this->db->escape($data['city']). "',
				   pinno = '" .$this->db->escape($data['pinno']). "',
				   contactno = '" .$this->db->escape($data['contactno']). "',
				   mobileno = '" .$this->db->escape($data['mobileno']). "' ");
		$supplier_id= $this->db->getLastId();

		return $supplier_id;
	}

	public function editWaiter($supplier_id, $data) {

		// if (!isset($data['show_in_purchase'])) {
		// 	$data['show_in_purchase'] = 0;
		// }
		$this->db->query("UPDATE " . DB_PREFIX . "supplier SET  supplier = '" .$this->db->escape($data['supplier']). "',
												   code = '" .$this->db->escape($data['code']). "',
												   address = '" .$this->db->escape($data['address']). "',
												   vat_no = '" .$this->db->escape($data['vat_no']). "',
												   gst_no = '" .$this->db->escape($data['gst_no']). "',
												   city = '" .$this->db->escape($data['city']). "',
												   pinno = '" .$this->db->escape($data['pinno']). "',
												   contactno = '" .$this->db->escape($data['contactno']). "',
												   mobileno = '" .$this->db->escape($data['mobileno']). "' 
												   WHERE supplier_id = '" . (int)$supplier_id . "'");
		return $supplier_id;
	}

	public function deleteWaiter($supplier_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "supplier WHERE supplier_id = '" . (int)$supplier_id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getWaiter($supplier_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "supplier WHERE supplier_id = '" . (int)$supplier_id . "' ");
		return $query->row;
	}

	public function getWaiters($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "supplier WHERE 1=1 ";

		if (!empty($data['filter_supplier'])) {
			$sql .= " AND supplier LIKE '" . $this->db->escape($data['filter_supplier']) . "%'";
		}

		if (!empty($data['filter_supplier_id'])) {
			$sql .= " AND supplier_id = '" . $this->db->escape($data['filter_supplier_id']) . "' ";
		}

		

		$sort_data = array(
			'supplier',
			'code',
			'city'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY supplier";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalWaiter($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "supplier WHERE 1=1 ";

		if (!empty($data['filter_supplier'])) {
			$sql .= " AND supplier LIKE '" . $this->db->escape($data['filter_supplier']) . "%'";
		}

		if (!empty($data['filter_supplier_id'])) {
			$sql .= " AND supplier_id = '" . $this->db->escape($data['filter_supplier_id']) . "' ";
		}

		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}