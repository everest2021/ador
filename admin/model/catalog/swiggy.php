<?php
class ModelCatalogSwiggy extends Model {
	public function addOrder($data) {
		// echo "<pre>";print_r($data);exit;
		$order_info_report_id = $this->db->query("SELECT order_id from oc_order_info_report order by order_id DESC LIMIT 1");
		$order_info_id = $this->db->query("SELECT order_id from oc_order_info  order by order_id DESC LIMIT 1");

		if($order_info_id->num_rows == 0){
			if($order_info_report_id->num_rows > 0){
				$increment_order_info_report = $order_info_report_id->row['order_id'];
			} else {
				$increment_order_info_report = 0;
			}
			$neworder_id = $increment_order_info_report + 1;
			$this->db->query("ALTER TABLE oc_order_info AUTO_INCREMENT = ".$neworder_id." ");
		}

		$order_items_report_id = $this->db->query("SELECT id from oc_order_items_report order by id DESC LIMIT 1");
		$order_items_id = $this->db->query("SELECT id from oc_order_items  order by id DESC LIMIT 1");

		if($order_items_id->num_rows == 0){
			if($order_items_report_id->num_rows > 0){
				$increment_order_item_report = $order_items_report_id->row['id'];
			} else {
				$increment_order_item_report = 0;
			}
			$neworder_itemid = $increment_order_item_report + 1;
			$this->db->query("ALTER TABLE oc_order_items AUTO_INCREMENT = ".$neworder_itemid." ");
		}	

		date_default_timezone_set("Asia/Kolkata");
		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$kot_no_datas = $this->db->query("SELECT `kot_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' ORDER BY `kot_no` DESC LIMIT 1")->row;
		$o_kot_no = 1;
		if(isset($kot_no_datas['kot_no'])){
			$o_kot_no = $kot_no_datas['kot_no'] + 1;
		}

		$date_added = date('Y-m-d');
		$time_added = date('H:i:s');

		$kotno2 = $this->db->query("SELECT `kot_no` FROM `oc_order_items` WHERE `bill_date` = '".$last_open_date."' AND is_liq = '0' order by `kot_no` DESC LIMIT 1");
		//$this->log->write('queryy ' .print_r("SELECT `kot_no` FROM `oc_order_items` WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `kot_no` DESC LIMIT 1",true));
		if($kotno2->num_rows > 0){
			$kot_no2 = $kotno2->row['kot_no'];
			$kotno = $kot_no2 + 1;
		} else{
			$kotno = 1;
		}

		// echo "<pre>";print_r($last_open_date);exit;
		$ftotal = 0;
		$ftotal_discount = 0;
		$total_qty = 0;
		$gst = 0;
		$staxfood = 0;
		$vat = 0;
		$total_item = 0;
		$stax = 0;
		$order_otp = 0;

		foreach ($data as $dkey => $dvalue) {
			$item_details[] = $this->db->query("SELECT * FROM `oc_item` WHERE `item_name` ='".$dvalue['item_name']."' ")->row;
			$item_data = $this->db->query("SELECT * FROM `oc_item` WHERE `item_name` ='".$dvalue['item_name']."' ")->row;
			$location_name = ONLINE_ORDER;
			$locationss =  $this->db->query("SELECT  * FROM oc_location WHERE location ='".$location_name."' ")->row;
			// echo "<pre>";print_r($locationss);exit;
			$rates = array(
				'1' => 'rate_1',
				'2' => 'rate_2',
				'3' => 'rate_3',
				'4' => 'rate_4',
				'5' => 'rate_5',
				'6' => 'rate_6',
			);

			if(isset($locationss['rate_id'])){
				$rate_key = $locationss['rate_id'];
			} else {
				$rate_key = 'rate_1';
			}

			$tax1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$item_data['vat']."'");
			if($tax1->num_rows > 0){
				$taxvalue1 = $tax1->row['tax_value'];
			} else{
				$taxvalue1 = '0';
			}

			$tax2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$item_data['tax2']."'");
			if($tax2->num_rows > 0){
				$taxvalue2 = $tax2->row['tax_value'];
			} else{
				$taxvalue2 = '0';
			}
			$online_order_id = $dvalue['order_id'];
			$total_item = $total_item + count($dvalue['qty']);
			$total_qty = $total_qty + $dvalue['qty'];
			$amt = $item_data[$rates[$rate_key]] * $dvalue['qty'];
			$ftotal = $ftotal + $amt;
			$gst = $gst + $amt*($taxvalue1/100);
			$grand_total = $ftotal + $gst;
			$order_otp = $dvalue['otp'];
		} 

		$locations_datas = $this->db->query("SELECT * FROM `oc_order_info` WHERE `location` = '" . $this->db->escape($locationss['location']) . "' order by table_id desc");

		if ($locations_datas->num_rows > 0) {
			$tables_name = $locations_datas->row['table_id'] + 1;
			$tables_id = $locations_datas->row['table_id'] + 1;
		} else {
			$tables_name = $locationss['table_from'];
			$tables_id = $locationss['table_from'];
		}

		// echo "<pre>";print_r($total_item);exit;
		// echo "<pre>";print_r($dvalue);
		// echo "<pre>";print_r($online_order_id);
		// echo "<pre>";print_r($total_qty);
		// echo "<pre>";print_r($ftotal);
		// echo "<pre>";print_r($gst);
		// echo "<pre>";print_r($grand_total);exit;

		$this->db->query("INSERT INTO " . DB_PREFIX . "order_info SET  
						`online_order_id` = '" . $this->db->escape($online_order_id) . "', 
						`location` = '" . $this->db->escape($locationss['location']) . "', 
						`location_id` = '" . $this->db->escape($locationss['location_id']) ."',
						`t_name` = '" . $this->db->escape($tables_name) . "',
						`table_id` = '" . $this->db->escape($tables_id) . "',  
						`ftotal` = '" . $this->db->escape($ftotal) . "',
						`gst` = '" . $this->db->escape($gst) . "',
						`grand_total` = '" . $this->db->escape($grand_total) . "',
						`total_items` = '" . $this->db->escape($total_item) . "',
						`item_quantity` = '" . $this->db->escape($total_qty) . "',
						`rate_id` = '" . $this->db->escape($locationss['rate_id']) . "',
						`bill_date` = '".$last_open_date."',
						`date_added` = '".$date_added."',
						`time_added` = '".$time_added."',
						`kot_no` = '".$o_kot_no."',
						`date` = '" . $this->db->escape(DATE('Y-m-d')) . "',
						`time` = '" . $this->db->escape(DATE('H:i:s')) . "',
						`login_id` = '" . $this->db->escape($this->user->getId()) . "',
						`order_otp` = '" . $this->db->escape($order_otp) . "',
						`login_name` = '" . $this->db->escape($this->user->getUserName()) . "' 
		");

		$order_id = $this->db->getLastId();

		foreach ($item_details as $ikey => $ivalue) {
			// echo"<pre>";print_r($ivalue);exit;
			$qty = $data[$ikey]['qty'];

			$locationss =  $this->db->query("SELECT  * FROM oc_location WHERE location_id ='3' ")->row;
			$rates = array(
				'1' => 'rate_1',
				'2' => 'rate_2',
				'3' => 'rate_3',
				'4' => 'rate_4',
				'5' => 'rate_5',
				'6' => 'rate_6',
			);

			if(isset($locationss['rate_id'])){
				$rate_key = $locationss['rate_id'];
			} else {
				$rate_key = 'rate_1';
			}

			$tax1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$item_data['vat']."'");
			if($tax1->num_rows > 0){
				$taxvalue1 = $tax1->row['tax_value'];
			} else{
				$taxvalue1 = '0';
			}

			$tax2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$item_data['tax2']."'");
			if($tax2->num_rows > 0){
				$taxvalue2 = $tax2->row['tax_value'];
			} else{
				$taxvalue2 = '0';
			}

			$amt = $ivalue[$rates[$rate_key]] * $qty;
			$gst = $amt*($taxvalue1/100);

			$this->db->query("INSERT INTO " . DB_PREFIX . "order_items SET
					`order_id` = '" . $this->db->escape($order_id) . "',   
					`code` = '" . $this->db->escape($ivalue['item_code']) ."',
					`name` = '" . htmlspecialchars_decode($ivalue['item_name']) ."', 
					`qty` = '" . $this->db->escape($qty) . "', 
					`rate` = '" . $this->db->escape($ivalue[$rates[$rate_key]]) . "',  
					`purchase` = '" . $this->db->escape($ivalue['purchase_price']) . "',  
					`amt` = '" . $this->db->escape($amt) . "',
					`ismodifier` = '1',
					`subcategoryid` = '" . $this->db->escape($ivalue['item_sub_category_id']) . "',
					`kot_status` = '" . $this->db->escape('1') . "',
					`kot_no` = '" .$this->db->escape($kotno). "',
					`is_liq` = '" . $this->db->escape($ivalue['is_liq']) . "',
					`tax1` = '" . $this->db->escape($taxvalue1) . "',
					`tax2` = '" . $this->db->escape($taxvalue2) . "',
					`tax1_value` = '" . $this->db->escape($gst) . "',
					`kitchen_display` = '" . $this->db->escape($ivalue['kitchen_dispaly']) . "',
					`bill_date` = '".$last_open_date."',
					`date` = '".date('Y-m-d')."',
					`time` = '".date('H:i:s')."',
					`login_id` = '" . $this->db->escape($this->user->getId()) . "',
					`login_name` = '" . $this->db->escape($this->user->getUserName()) . "' "
			);
		}

		return $order_id;	
	}

	public function getorders($data = array()) {
		// echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM " . DB_PREFIX . "online_order WHERE 1=1 ";

		if (isset($data['filter_date']) && $data['filter_date'] !='' && $data['filter_date'] !='1970-01-01') {
			$sql .= "AND date LIKE '" . $this->db->escape($data['filter_date']) ."'";
		}

		if (isset($data['filter_partner']) && ($data['filter_partner']) !='' && $data['filter_partner'] !='0') {
			$sql .= " AND delivery_partner = '" . (int)$data['filter_partner'] . "'";
		}

		if (isset($data['order_status']) && ($data['order_status']) !='' && $data['order_status'] !='2') {
			$sql .= " AND panding_status = '" . (int)$data['order_status'] . "'";
		}
		// if (isset($data['order']) && ($data['order'] == 'DESC')) {
		$sql .= " ORDER BY id DESC";
		// } else {
		// 	$sql .= " ASC";
		// }

		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}

		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }

		$query = $this->db->query($sql);
		// echo "<pre>";print_r($sql);exit;
		return $query->rows;
	}
	
	public function getStatus($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "order_app` WHERE 1=1 ";

		// if (!empty($data['order_status'])) {
		// 	$sql .= " AND `brand` LIKE '%" . $this->db->escape($data['order_status']) . "%'";
		// }

		if (!empty($data['order_status'])) {
			$sql .= " AND `status` = '" . $this->db->escape($data['order_status']) . "' ";
		}

		

		$query = $this->db->query($sql);

		return $query->rows;
	}

	

	public function getTotalBrand($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "brand WHERE 1=1 ";

		if (!empty($data['filter_brand'])) {
			$sql .= " AND `brand` LIKE '%" . $this->db->escape($data['filter_brand']) . "%'";
		}

		if (!empty($data['filter_brand_id'])) {
			$sql .= " AND `brand_id` = '" . $this->db->escape($data['filter_brand_id']) . "' ";
		}

		if (!empty($data['filter_subcategory'])) {
			$sql .= " AND subcategory LIKE '%" . $this->db->escape($data['filter_subcategory']) . "%' ";
		}

		if (!empty($data['filter_subcategory_id'])) {
			$sql .= " AND subcategory_id = '" . $this->db->escape($data['filter_subcategory_id']) . "' ";
		}
		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}