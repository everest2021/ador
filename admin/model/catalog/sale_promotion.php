<?php
class ModelCatalogSalePromotion extends Model {
	public function addSalePromotion($data) {

		//$this->db->query("DELETE FROM " . DB_PREFIX . "customerinfo WHERE c_id = '" . (int)$data['c_id'] . "'");
		
		$from_date = '0000-00-00';
		$to_date =  '0000-00-00';

		if ($data['from_date'] != '0000-00-00' && $data['from_date'] != '' )  {
			$from_date = date('Y-m-d',strtotime($data['from_date']));
		}

		if ($data['to_date'] != '0000-00-00' && $data['to_date'] !='' )  {
			$to_date = date('Y-m-d',strtotime($data['to_date']));
		}

		$this->db->query("INSERT INTO " . DB_PREFIX ."sale_promotion SET name = '" .$this->db->escape($data['name']). "', to_date = '" .$this->db->escape($to_date). "', from_date = '" .$this->db->escape($from_date). "', from_time = '" .$this->db->escape($data['from_time']). "', to_time = '" .$this->db->escape($data['to_time']). "', free_sub_category_id = '" .(int)$data['free_subcategory']. "', sub_category_id = '" .(int)$data['subcategory']. "', discount_percentage = '" .(float)$data['discount_percentage']. "', discount_rupees = '" .(float)$data['discount_rupees']. "', amount = '" .(float)$data['amount']. "', point = '" .(float)$data['point']. "', per_point = '" .(float)$data['per_point']. "', point_rupees = '" .(float)$data['point_rupees']. "' ,days = '" . $this->db->escape(isset($data['days']) ? serialize($data['days']) : '')  . "' ");


		//echo"<pre>";print_r("INSERT INTO " . DB_PREFIX ."sale_promotion SET name = '" .$this->db->escape($data['name']). "', to_date = '" .$this->db->escape($to_date). "', from_date = '" .$this->db->escape($from_date). "', from_time = '" .$this->db->escape($data['from_time']). "', to_time = '" .$this->db->escape($data['to_time']). "', free_sub_category_id = '" .(int)$data['free_subcategory']. "', sub_category_id = '" .(int)$data['subcategory']. "', discount_percentage = '" .(float)$data['discount_percentage']. "', discount_rupees = '" .(float)$data['discount_rupees']. "', amount = '" .(float)$data['amount']. "', point = '" .(float)$data['point']. "', per_point = '" .(float)$data['per_point']. "', point_rupees = '" .(float)$data['point_rupees']. "' ,days = '" . $this->db->escape(isset($data['days']) ? serialize($data['days']) : '') . "' ");exit;
		$sale_promotion_id = $this->db->getLastId();

		if(isset($data['item_list'])){
			foreach ($data['item_list'] as $key => $value) {
				$item_quntity = 0;
				if(isset($data['item_quntity'][$value])){
					$item_quntity = $data['item_quntity'][$value];
				}
				$this->db->query("INSERT INTO `oc_sale_promotion_items` SET `sale_promotion_id` = '".$sale_promotion_id."', `sub_category_id` = '".(int)$data['subcategory']."', `item_id` = '".(int)$value."', `quantity` = '".(int)$item_quntity."', `free_sub_cat` = '0'  ");
			}
		}

		if(isset($data['free_itme_list'])){
			foreach ($data['free_itme_list'] as $key => $value) {
				$item_quntity = 0;
				if(isset($data['free_item_quntity'][$value])){
					$item_quntity = $data['free_item_quntity'][$value];
				}
				$this->db->query("INSERT INTO `oc_sale_promotion_items` SET `sale_promotion_id` = '".$sale_promotion_id."', `sub_category_id` = '".(int)$data['free_subcategory']."', `item_id` = '".(int)$value."', `quantity` = '".(int)$item_quntity."', `free_sub_cat` = '1'  ");
			}
		}

		return $sale_promotion_id;
	}

	public function editSalePromotion($sale_promotion_id, $data) {

		$from_date = '0000-00-00';
		$to_date =  '0000-00-00';

		if ($data['from_date'] != '0000-00-00' && $data['from_date'] != '' )  {
			$from_date = date('Y-m-d',strtotime($data['from_date']));
		}

		if ($data['to_date'] != '0000-00-00' && $data['to_date'] !='' )  {
			$to_date = date('Y-m-d',strtotime($data['to_date']));
		}

		$this->db->query("UPDATE " . DB_PREFIX ."sale_promotion SET name = '" .$this->db->escape($data['name']). "', to_date = '" .$this->db->escape($to_date). "', from_date = '" .$this->db->escape($from_date). "', from_time = '" .$this->db->escape($data['from_time']). "', to_time = '" .$this->db->escape($data['to_time']). "', free_sub_category_id = '" .(int)$data['free_subcategory']. "', sub_category_id = '" .(int)$data['subcategory']. "', discount_percentage = '" .(float)$data['discount_percentage']. "', discount_rupees = '" .(float)$data['discount_rupees']. "', amount = '" .(float)$data['amount']. "', point = '" .(float)$data['point']. "', per_point = '" .(float)$data['per_point']. "', point_rupees = '" .(float)$data['point_rupees']. "' ,days = '" . $this->db->escape(isset($data['days']) ? serialize($data['days']) : '') . "' WHERE id = '" . (int)$sale_promotion_id . "' ");

		$this->db->query("DELETE FROM " . DB_PREFIX . "sale_promotion_items WHERE sale_promotion_id = '" . (int)$sale_promotion_id . "'");

		if(isset($data['item_list'])){
			foreach ($data['item_list'] as $key => $value) {
				$item_quntity = 0;
				if(isset($data['item_quntity'][$value])){
					$item_quntity = $data['item_quntity'][$value];
				}
				$this->db->query("INSERT INTO `oc_sale_promotion_items` SET `sale_promotion_id` = '".$sale_promotion_id."', `sub_category_id` = '".(int)$data['subcategory']."', `item_id` = '".(int)$value."', `quantity` = '".(int)$item_quntity."', `free_sub_cat` = '0'  ");
			}
		}

		if(isset($data['free_itme_list'])){
			foreach ($data['free_itme_list'] as $key => $value) {
				$item_quntity = 0;
				if(isset($data['free_item_quntity'][$value])){
					$item_quntity = $data['free_item_quntity'][$value];
				}
				$this->db->query("INSERT INTO `oc_sale_promotion_items` SET `sale_promotion_id` = '".$sale_promotion_id."', `sub_category_id` = '".(int)$data['free_subcategory']."', `item_id` = '".(int)$value."', `quantity` = '".(int)$item_quntity."', `free_sub_cat` = '1'  ");
			}
		}
	}

	public function deleteSalePromotionss($sale_promotion_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "sale_promotion WHERE id = '" . (int)$sale_promotion_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "sale_promotion_items WHERE sale_promotion_id = '" . (int)$sale_promotion_id . "'");
	}

	public function getSalePromotionss($sale_promotion_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "sale_promotion WHERE id = '" . (int)$sale_promotion_id . "' ");
		return $query->row;
	}

	public function getSalePromotion($data = array()) {
		
		$sql = "SELECT * FROM " . DB_PREFIX . "sale_promotion WHERE 1=1 ";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;
		
		$this->log->write($sql);

		$query = $this->db->query($sql);
	
		return $query->rows;
	}

	public function getTotalSalePromotion($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "sale_promotion WHERE 1=1 ";

		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getCategory(){
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."subcategory WHERE 1=1 ORDER BY name");
		return $query->rows;
	}

	public function getitems($data){
		$sql = "SELECT `item_name`, `item_id` FROM ".DB_PREFIX."item WHERE 1=1 ";

		if(isset($data['subcategory_id']) && $data['subcategory_id'] > 0){
			$sql .= " AND `item_sub_category_id` = '".$data['subcategory_id']."' ";
		}

		$sql .= "ORDER BY item_name ";
		//echo '<pre>';print_r($sql);exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
}