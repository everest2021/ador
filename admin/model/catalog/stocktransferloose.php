<?php
class ModelCatalogstocktransferloose extends Model {
	public function addpurchaseentry($data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		if($data['invoice_date'] != '' && $data['invoice_date'] != '00-00-0000'){
			$data['invoice_date']  = date('Y-m-d', strtotime($data['invoice_date']));
		}
		$sql = "INSERT INTO `oc_stocktransferloose` SET 
				`invoice_no` = '".$this->db->escape($data['invoice_no'])."',
				`invoice_date` = '".$this->db->escape($data['invoice_date'])."',
				`from_store_id` = '".$this->db->escape($data['from_store_id'])."',
				`from_store_name` = '".$this->db->escape($data['from_store_name'])."',
				`to_store_id` = '".$this->db->escape($data['to_store_id'])."',
				`to_store_name` = '".$this->db->escape($data['to_store_name'])."',
				`narration` = '".$this->db->escape($data['narration'])."',
				`category` = 'Liquor',
				`total_qty` = '".$this->db->escape($data['total_qty'])."',
				`total_pay` = '".$this->db->escape($data['total_pay'])."'
				";
		$this->db->query($sql);
		$this->log->write($sql);
		$id = $this->db->getLastId();

		foreach($data['po_datas'] as $pkey => $pvalue){
			$sql = "INSERT INTO `oc_stocktransferloose_items` SET 
				`p_id` = '".$this->db->escape($id)."',
				`description` = '".$this->db->escape($pvalue['description'])."',
				`description_id` = '".$this->db->escape($pvalue['description_id'])."',
				`description_code` = '".$this->db->escape($pvalue['description_code'])."',
				`description_barcode` = '".$this->db->escape($pvalue['description_barcode'])."',
				`description_code_search` = '".$this->db->escape($pvalue['description_code_search'])."',
				`type` = '".$this->db->escape($pvalue['type_id'])."',
				`qty` = '".$this->db->escape($pvalue['qty'])."',
				`avqty` = '".$this->db->escape($pvalue['avq'])."',
				`unit` = '".$this->db->escape($pvalue['unit'])."',
				`unit_id` = '".$this->db->escape($pvalue['unit_id'])."',
				`rate` = '".$this->db->escape($pvalue['rate'])."',
				`amount` = '".$this->db->escape($pvalue['amount'])."'
				";
			$this->db->query($sql);
			$this->log->write($sql);
		}
		return $id;
	}

	public function editpurchaseentry($data, $id) {
		if($data['invoice_date'] != '' && $data['invoice_date'] != '00-00-0000'){
			$data['invoice_date']  = date('Y-m-d', strtotime($data['invoice_date']));
		}
		$sql = "UPDATE `oc_stocktransferloose` SET 
				`invoice_no` = '".$this->db->escape($data['invoice_no'])."',
				`invoice_date` = '".$this->db->escape($data['invoice_date'])."',
				`from_store_id` = '".$this->db->escape($data['from_store_id'])."',
				`from_store_name` = '".$this->db->escape($data['from_store_name'])."',
				`to_store_id` = '".$this->db->escape($data['to_store_id'])."',
				`to_store_name` = '".$this->db->escape($data['to_store_name'])."',
				`narration` = '".$this->db->escape($data['narration'])."',
				`category` = 'Liquor',
				`total_qty` = '".$this->db->escape($data['total_qty'])."',
				`total_pay` = '".$this->db->escape($data['total_pay'])."'
				WHERE `id` = '".$id."'
				";
		$this->db->query($sql);
		$this->log->write($sql);

		$this->db->query("DELETE FROM `oc_stocktransferloose_items` WHERE `p_id` = '".$id."' ");
		foreach($data['po_datas'] as $pkey => $pvalue){
			$sql = "INSERT INTO `oc_stocktransferloose_items` SET 
				`p_id` = '".$this->db->escape($id)."',
				`description` = '".$this->db->escape($pvalue['description'])."',
				`description_id` = '".$this->db->escape($pvalue['description_id'])."',
				`description_code` = '".$this->db->escape($pvalue['description_code'])."',
				`description_barcode` = '".$this->db->escape($pvalue['description_barcode'])."',
				`description_code_search` = '".$this->db->escape($pvalue['description_code_search'])."',
				`type` = '".$this->db->escape($pvalue['type_id'])."',
				`qty` = '".$this->db->escape($pvalue['qty'])."',
				`avqty` = '".$this->db->escape($pvalue['avq'])."',
				`unit` = '".$this->db->escape($pvalue['unit'])."',
				`unit_id` = '".$this->db->escape($pvalue['unit_id'])."',
				`rate` = '".$this->db->escape($pvalue['rate'])."',
				`amount` = '".$this->db->escape($pvalue['amount'])."'
				";
			$this->db->query($sql);
			$this->log->write($sql);
		}
		return $id;
	}

	public function deletepurchaseentry($id) {
		$this->db->query("UPDATE " . DB_PREFIX . "stocktransferloose SET delete_status = '1' WHERE id = '" . (int)$id . "' ");
	}

	public function getPurchaseentry($id) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "stocktransferloose` WHERE `id` = '" . (int)$id . "' ");
		return $query->rows;
	}

	public function getPurchaseentryby_supp_invoice($invoice_no) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "stocktransferloose` WHERE `invoice_no` = '".$invoice_no."'");
		return $query->rows;
	}
}