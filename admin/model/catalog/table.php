<?php
class ModelCatalogTable extends Model {
	public function addTable($data) {

		$this->db->query("INSERT INTO " . DB_PREFIX ."table SET 
					name = '" .$this->db->escape($data['name']). "',
				    location = '" .$this->db->escape($data['location']). "',
				    location_id = '" .$this->db->escape($data['location_id']). "',
				    colour = '" .$this->db->escape($data['colour']). "'");
		$table_id = $this->db->getLastId();

		return $table_id;
	}

	public function editTable($table_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "table SET 
						location = '" . $this->db->escape($data['location']) . "',
						location_id = '" . $this->db->escape($data['location_id']) . "',
						name = '" .$this->db->escape($data['name']). "',
						colour = '" .$this->db->escape($data['colour']). "'
						WHERE table_id = '" . (int)$table_id . "'");
		return $table_id;
	}

	public function deleteTable($table_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "table WHERE table_id = '" . (int)$table_id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getTable($table_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "table WHERE table_id = '" . (int)$table_id . "' ");
		return $query->row;
	}

	public function getLocation(){
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."location");
		//echo "SELECT * FROM ".DB_PREFIX."location WHERE location_id='".(int)$location_id."'";
		return $query->rows;
	}

	public function getTables($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "table` WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND `name` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `table_id` = '" . $this->db->escape($data['filter_name_id']) . "' ";
		}

		if (!empty($data['filter_location'])) {
			$sql .= " AND location LIKE '%" . $this->db->escape($data['filter_location']) . "%' ";
		}

		if (!empty($data['filter_location_id'])) {
			$sql .= " AND location_id = '" . $this->db->escape($data['filter_location_id']) . "' ";
		}

		$sort_data = array(
			'name',
			'location'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		// echo $sql;
		// exit();

		$query = $this->db->query($sql);



		return $query->rows;
	}

	public function getTotalTable($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "table WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND `name` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `table_id` = '" . $this->db->escape($data['filter_name_id']) . "' ";
		}

		if (!empty($data['filter_location'])) {
			$sql .= " AND location LIKE '%" . $this->db->escape($data['filter_location']) . "%' ";
		}

		if (!empty($data['filter_location_id'])) {
			$sql .= " AND location_id = '" . $this->db->escape($data['filter_location_id']) . "' ";
		}
		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}