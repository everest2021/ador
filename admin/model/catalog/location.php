<?php
class ModelCatalogLocation extends Model {
	public function addLocation($data) {

		if (!isset($data['parcel_detail'] )) {
			$data['parcel_detail'] = 0;
		}
		if (!isset($data['dboy_detail'] )) {
			$data['dboy_detail'] = 0;
		}

		if (!isset($data['a_to_z'] )) {
			$data['a_to_z'] = 0;
		}
		if (!isset($data['direct_bill'] )) {
			$data['direct_bill'] = 0;
		}

		if (!isset($data['is_app'] )) {
			$data['is_app'] = 0;
		}

		if (!isset($data['is_advance'] )) {
			$data['is_advance'] = 0;
		}

		if (!isset($data['kot_different'] )) {
			$data['kot_different'] = 0;
		}

		if (!isset($data['service_charge'] )) {
			$data['service_charge'] = 0;
		}
		

		$this->db->query("INSERT INTO " . DB_PREFIX ."location SET 
						location = '" .$this->db->escape($data['location']). "',
						table_to = '" .$this->db->escape($data['table_to']). "',
						table_from = '" .$this->db->escape($data['table_from']). "',
						bill_copy = '" .$this->db->escape($data['bill_copy']). "',
						master_kot = '" .$this->db->escape($data['master_kot']). "',
						store_name = '" .$this->db->escape($data['store_name']). "',
						kot_copy = '" .$this->db->escape($data['kot_copy']). "',
						bot_copy = '" .$this->db->escape($data['bot_copy']). "',

						parcel_detail = '" .$this->db->escape($data['parcel_detail']). "',
						dboy_detail = '" .$this->db->escape($data['dboy_detail']). "',
						a_to_z = '" .$this->db->escape($data['a_to_z']). "',
						direct_bill = '" .$this->db->escape($data['direct_bill']). "',
						is_app = '" .$this->db->escape($data['is_app']). "',
						is_advance = '" .$this->db->escape($data['is_advance']). "',
						kot_different = '" .$this->db->escape($data['kot_different']). "',
						service_charge = '" .$this->db->escape($data['service_charge']). "',
						bill_printer_name = '" .$this->db->escape($data['bill_printer_name']). "',
						bill_printer_type = '" .$this->db->escape($data['bill_printer_type']). "',
						rate_id = '" .$this->db->escape($data['rate_id']). "'
						");
		$location_id = $this->db->getLastId();

		
		return $location_id;
	}

	public function editLocation($location_id, $data) {
		if (!isset($data['parcel_detail'] )) {
			$data['parcel_detail'] = 0;
		}
		if (!isset($data['dboy_detail'] )) {
			$data['dboy_detail'] = 0;
		}

		if (!isset($data['a_to_z'] )) {
			$data['a_to_z'] = 0;
		}
		if (!isset($data['direct_bill'] )) {
			$data['direct_bill'] = 0;
		}
		if (!isset($data['is_app'] )) {
			$data['is_app'] = 0;
		}

		if (!isset($data['is_advance'] )) {
			$data['is_advance'] = 0;
		}

		if (!isset($data['kot_different'] )) {
			$data['kot_different'] = 0;
		}

		if (!isset($data['service_charge'] )) {
			$data['service_charge'] = 0;
		}
		
		$this->db->query("UPDATE " . DB_PREFIX . "location SET 
						location = '" . $this->db->escape($data['location']) . "',
						table_to = '" .$this->db->escape($data['table_to']). "',
						table_from = '" .$this->db->escape($data['table_from']). "',
						bill_copy = '" .$this->db->escape($data['bill_copy']). "',
						master_kot = '" .$this->db->escape($data['master_kot']). "',
						kot_copy = '" .$this->db->escape($data['kot_copy']). "',
						bot_copy = '" .$this->db->escape($data['bot_copy']). "',
						
						store_name = '" .$this->db->escape($data['store_name']). "',
						parcel_detail = '" .$this->db->escape($data['parcel_detail']). "',
						dboy_detail = '" .$this->db->escape($data['dboy_detail']). "',
						a_to_z = '" .$this->db->escape($data['a_to_z']). "',
						direct_bill = '" .$this->db->escape($data['direct_bill']). "',
						is_app = '" .$this->db->escape($data['is_app']). "',
						is_advance = '" .$this->db->escape($data['is_advance']). "',
						kot_different = '" .$this->db->escape($data['kot_different']). "',
						service_charge = '" .$this->db->escape($data['service_charge']). "',
						bill_printer_name = '" .$this->db->escape($data['bill_printer_name']). "',
						bill_printer_type = '" .$this->db->escape($data['bill_printer_type']). "',
						rate_id = '" . $this->db->escape($data['rate_id']) . "'
						WHERE location_id = '" . (int)$location_id . "'");
		return $location_id;
	}

	public function deleteLocation($location_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$location_id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getLocation($Location_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$Location_id . "' ");
		return $query->row;
	}

	public function getLocations($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "location WHERE 1=1 ";

		if (!empty($data['filter_location'])) {
			$sql .= " AND location LIKE '" . $this->db->escape($data['filter_location']) . "%'";
		}

		if (!empty($data['filter_location_id'])) {
		 	$sql .= " AND location_id = '" . $this->db->escape($data['filter_location_id']) . "' ";
		 }

		$sort_data = array(
			'location',
			'table_from',
			'table_to'
			);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY location";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalLocation($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "location WHERE 1=1 ";

		if (!empty($data['filter_location'])) {
			$sql .= " AND location LIKE '" . $this->db->escape($data['filter_location']) . "%'";
		}

		if (!empty($data['filter_location_id'])) {
			$sql .= " AND location_id = '" . $this->db->escape($data['filter_location_id']) . "' ";
		}

		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}