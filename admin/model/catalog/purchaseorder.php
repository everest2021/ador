<?php
class ModelCatalogPurchaseorder extends Model {
	public function addItem($data) {

		if(!empty($data['date'])){
			$data['date'] = date('Y-m-d', strtotime($data['date']));
		}

		$this->db->query("INSERT INTO oc_purchase_order SET 
							po_prefix = 'PO-',
							po_date = '".$data['date']."',
							outlet_id = '".$data['store']."'
						");

		$po_id = $this->db->getLastId();

		foreach ($data['po_datas'] as $pkey => $pvalues) {
			foreach($data['po_datas'] as $pkeys => $pvaluess){
				if($pkey == $pkeys) {

				} elseif($pkey > $pkeys && $pvalues['itemid'] == $pvaluess['itemid'] && $pvalues['msg'] == $pvaluess['msg']){
					$pvalues['itemid'] = '';
				} elseif ($pvalues['itemid'] == $pvaluess['itemid'] && $pvalues['msg'] == $pvaluess['msg']) {
					$pvalues['qty'] = $pvalues['qty'] + $pvaluess['qty'];
				}
			}
			if($pvalues['itemid'] != ''){
				$this->db->query("INSERT INTO oc_purchase_order_items SET 
								po_id = '".$po_id."',
								item_id = '".$pvalues['itemid']."',
								item_name = '".$pvalues['item']."',
								quantity = '".$pvalues['qty']."',
								notes = '".$pvalues['msg']."'
							");
			}
		}
		$podata = $this->db->query("SELECT * FROM oc_purchase_order WHERE 1=1");
		if($podata->num_rows > 0){
			$po_number = $this->db->query("SELECT po_number FROM oc_purchase_order ORDER BY po_number DESC LIMIT 1")->row;
			$po_number = $po_number['po_number'] + 1;
		} else{
			$po_number = 1;
		}
		$this->db->query("UPDATE oc_purchase_order SET po_number = '".$po_number."' WHERE id = '".$po_id."'");
		return $po_id;
	}

	public function editItem($item_id, $data) {

		if(!empty($data['date'])){
			$data['date'] = date('Y-m-d', strtotime($data['date']));
		}

		$this->db->query("DELETE FROM oc_purchase_order_items WHERE po_id = '".$item_id."'");
		$this->db->query("UPDATE oc_purchase_order SET 
							po_prefix = 'po',
							po_date = '".$data['delivery_date']."',
							outlet_id = '".$data['store']."'
							WHERE id='".$item_id."'
						");

		foreach ($data['po_datas'] as $key => $value) {
			$this->db->query("INSERT INTO oc_purchase_order_items SET 
								po_id = '".$item_id."',
								item_id = '".$value['itemid']."',
								item_name = '".$value['item']."',
								quantity = '".$value['qty']."',
								notes = '".$value['msg']."'
							");

			if($value['msg'] != ''){
				$id = $this->db->query("SELECT * FROM oc_kotmsg order by msg_code DESC LIMIT 0, 1")->row;
				$msg_code = $id['msg_code'] + 1 ;
				$is_exist = $this->db->query("SELECT * FROM oc_kotmsg WHERE `message` = '".$value['msg']."' ");
				
				$this->log->write("SELECT * FROM oc_kotmsg WHERE `message` = '".$value['msg']."' ");
				$this->log->write(print_r($is_exist, true));

				if($is_exist->num_rows == 0){
					$this->db->query("INSERT INTO oc_kotmsg SET message = '".$value['msg']."', msg_code = '".$msg_code."' ");
				}	
			}
		}
	}

	public function deleteItem($item_id) {
		$this->db->query("DELETE FROM oc_purchase_order WHERE id = '" . (int)$item_id . "'");
		$this->db->query("DELETE FROM oc_purchase_order_items WHERE po_id = '" . (int)$item_id . "'");
		//$this->db->query("DELETE FROM " . DB_PREFIX . "item WHERE item_id = '" . (int)$item_id . "'");
	}

	public function getItem($item_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "item WHERE item_id = '" . (int)$item_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM oc_purchase_order WHERE id = '" . (int)$item_id . "' ");
		return $query->row;
	}

	public function getItems($data = array()) {
		$sql = "SELECT * FROM oc_purchase_order WHERE 1=1 ";

		if (isset($data['filter_date']) && !empty($data['filter_date'])) {
			$sql .= " AND `po_date` = '" . date('Y-m-d', strtotime($data['filter_date'])) . "' ";
		}

		if ($data['filter_status'] >= 0 && $data['filter_status'] != '') {
			$sql .= " AND status = '" . $this->db->escape($data['filter_status']) . "'";
		}

		$sql .=" AND outlet_id = '".$this->user->getUserStore()."'";

		$sort_data = array(
			'po_date',
			'po_number',
			'status'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY po_date";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;

		//$this->log->write($sql);

		$query = $this->db->query($sql);

		// echo "<pre>";
		// print_r($query);
		// exit();

		return $query->rows;
	}

	public function getTotalItems($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_purchase_order WHERE 1=1 ";

		if (isset($data['filter_date']) && !empty($data['filter_date'])) {
			$sql .= " AND `po_date` = '" . date('Y-m-d', strtotime($data['filter_date'])) . "' ";
		}

		if ($data['filter_status'] >= 0 && $data['filter_status'] != '') {
			$sql .= " AND status = '" . $this->db->escape($data['filter_status']) . "'";
		}

		$sql .=" AND outlet_id = '".$this->user->getUserStore()."'";

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}