<?php
class ModelCatalogMeal extends Model {
	public function addmeal($data) {
		// echo "<pre>";
		// print_r($data);
		// exit;
		$meal_datas = $this->db->query("SELECT * FROM " . DB_PREFIX . "venue WHERE venue_id = '" . (int)$data['venue_id']. "'")->row;
		$this->db->query("INSERT INTO " . DB_PREFIX .
				   "meal SET name = '" .$this->db->escape($data['name']). "',
				   	venue_id = '" . $this->db->escape($data['venue_id']) . "',
				   	image = '" . $this->db->escape($data['image']) . "',
				   	image_source = '" . $this->db->escape($data['image_source']) . "',
				   	venue_name = '" .$this->db->escape($meal_datas['venue_name']). "', 
				   	rate_child = '" .$this->db->escape($data['rate_child']). "', 
					rate = '" . $this->db->escape($data['rate']) . "' 
					");
		$ven_id = $this->db->getLastId();
	//return $ven_id;

		foreach ($data['po_datas'] as $key => $value) {
			$this->db->query("INSERT INTO `oc_combo_items` SET 
								venue_id = '".$ven_id."',
								combo_item_id = '".$value['itemid']."',
								combo_item_code = '".$value['itemcode']."',
								combo_item = '".$value['item']."',
								default_item = '".$value['default_item']."'");
		}

	}

	public function editmeal($id, $data) {	
		//echo "<pre>";print_r($data);exit;
		$this->db->query("DELETE FROM `oc_combo_items` WHERE `venue_id` = '".$id."'");
		$meal_datas = $this->db->query("SELECT * FROM " . DB_PREFIX . "venue WHERE venue_id = '" . (int)$data['venue_id']. "'")->row;
		$this->db->query("UPDATE " . DB_PREFIX . "meal SET name = '" . $this->db->escape($data['name']) . "',
			        venue_id = '" . $this->db->escape($data['venue_id']) . "',
					venue_name = '" . $this->db->escape($meal_datas['venue_name']) . "',
					image = '" . $this->db->escape($data['image']) . "',
				   	image_source = '" . $this->db->escape($data['image_source']) . "',
					rate_child = '" .$this->db->escape($data['rate_child']). "', 									
					rate = '" . $this->db->escape($data['rate']) . "'															 
					WHERE id = '" . (int)$id . "'");
	//return $id;
		foreach ($data['po_datas'] as $key => $value) {
			$this->db->query("INSERT INTO `oc_combo_items` SET 
								venue_id = '".$id."',
								combo_item_id = '".$value['itemid']."',
								combo_item_code = '".$value['itemcode']."',
								combo_item = '".$value['item']."',
								default_item = '".$value['default_item']."'");
		}

	}

	public function deletemeal($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "meal WHERE id = '" . (int)$id . "'");
		$this->db->query("DELETE FROM `oc_combo_items` WHERE `venue_id` = '" . (int)$id . "'");

	}

	public function getmeal($id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "meal WHERE id = '" . (int)$id . "' ");
		return $query->row;
	}

	public function getmeals($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "meal WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'name',
			'venue_name',
			'venue_id',
			'sport_type'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalmeal($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "meal WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}