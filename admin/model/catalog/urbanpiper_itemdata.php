<?php
class ModelCatalogurbanpiperitemdata extends Model {
	public function addOrder($online_order_id) { 
		date_default_timezone_set("Asia/Kolkata");
		$results = $this->db->query("SELECT * FROM oc_orders_app WHERE online_order_id = '".$online_order_id."' ")->rows;
		$order_info_report_id = $this->db->query("SELECT order_id from oc_order_info_report order by order_id DESC LIMIT 1");
		//if($order_info_report_id->num_rows > 0){
			$order_info_id = $this->db->query("SELECT order_id from oc_order_info  order by order_id DESC LIMIT 1");

			if($order_info_id->num_rows == 0){
				if($order_info_report_id->num_rows > 0){
					$increment_order_info_report = $order_info_report_id->row['order_id'];
				} else {
					$increment_order_info_report = 0;
				}
				$neworder_id = $increment_order_info_report + 1;
				$this->db->query("ALTER TABLE oc_order_info AUTO_INCREMENT = ".$neworder_id." ");
			}
		//}

		$order_items_report_id = $this->db->query("SELECT id from oc_order_items_report order by id DESC LIMIT 1");
		//if($order_items_report_id->num_rows > 0){
			$order_items_id = $this->db->query("SELECT id from oc_order_items  order by id DESC LIMIT 1");

			if($order_items_id->num_rows == 0){
				if($order_items_report_id->num_rows > 0){
					$increment_order_item_report = $order_items_report_id->row['id'];
				} else {
					$increment_order_item_report = 0;
				}
				$neworder_itemid = $increment_order_item_report + 1;
				$this->db->query("ALTER TABLE oc_order_items AUTO_INCREMENT = ".$neworder_itemid." ");
			}
		//}

		$result1 = array();
		foreach ($results as $result) {

			$prev_details = $this->db->query("SELECT kot_no, table_id FROM oc_order_info ORDER BY order_id DESC LIMIT 1 ");
			if ($prev_details->num_rows > 0) {
				$kot_no = (int)$prev_details->row['kot_no'] + 1;
				$table_id = (int)$prev_details->row['table_id'] + 1;
			} else {
				$kot_no = 1;
				$table_id = 5000;
			}


			$cust_details = $this->db->query("SELECT CONCAT(sub_locality,' ',address_1, ' ', address_2, ' ',city , ' ',postcode, ' ',area) AS address FROM oc_address_app WHERE customer_id = '".$result['cust_id']."' AND online_order_id = '".$online_order_id."'");
			if ($cust_details->num_rows > 0) {
				$cust_address = $cust_details->row['address'];
			} else {
				$cust_address = '';
			}
			// $cust_details2 = $this->db->query("SELECT telephone, email, firstname ,customer_id FROM oc_customer_app WHERE customer_id = '".$result['cust_id']."' AND customer_type = 'online' AND online_order_id = '".$result['online_order_id']."'");
			// if ($cust_details2->num_rows > 0) {
			// 	$cust_contact = $cust_details2->row['telephone'];
			// 	$cust_email = $cust_details2->row['email'];
			// 	$cust_name = $cust_details2->row['firstname'];
			// 	$cust_id = $cust_details2->row['customer_id'];
			// } else {
			// 	$cust_contact = '';
			// 	$cust_email = '';
			// 	$cust_name = '';
			// 	$cust_id = '';
			// }

			$last_open_dates = $this->db->query("SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1");

			if($last_open_dates->num_rows > 0){
				$last_open_date = $last_open_dates->row['bill_date'];
			} else {
				$last_open_date = date('Y-m-d');
			}

			$other_details2 = $this->db->query("SELECT COUNT(*) AS total_items, SUM(qty) AS item_quantity FROM oc_order_item_app WHERE online_order_id = '".$result['online_order_id']."' ");
			if ($other_details2->num_rows > 0) {
				$total_items = $other_details2->row['total_items'];
				$item_quantity = $other_details2->row['item_quantity'];
			} else {
				$total_items = 0;
				$item_quantity = 0;
			}

			$this->db->query("INSERT INTO " . DB_PREFIX . "order_info SET  
								urbanpiper_order_id = '".$result['online_order_id']."',
								`packaging` = '".$result['total_charge']."',
								delivery_charges = '".$result['delivery_charge']."',
								kot_no = '".$kot_no."',
								order_no = '0',
								merge_number = '0',
								location = 'Online',
								location_id = '10000',
								t_name = '".$table_id."',
								table_id = '".$table_id."',
								waiter_code = '',
								waiter = '',
								captain_code = '',
								captain = '',
								captain_id = '',
								person = '0',
								ftotal = '".$result['total']."',
								gst = '".$result['gst_val']."',
								ltotal = '0',
								ltotal_discount = '0',
								vat = '0',
								bill_status = '0',
								`payment_status` = '1',
								`pay_method` = '1',
								cust_name = '".$result['firstname']."',
								cust_id = '".$result['cust_id']."',
								cust_contact = '".$result['telephone']."',
								cust_address = '".$cust_address."',
								cust_email = '".$result['email']."',
								day_close_status = '0',
								bill_date = '".$last_open_date."',
								rate_id = '',
								grand_total = '".$result['grand_tot']."',
								 total_payment = '".$result['grand_tot']."',
								total_items = '".$total_items."',
								item_quantity = '".$item_quantity."',
								`date` = '".$result['order_date']."',
								`order_from` = '".$result['online_channel']."',
								`time` = '".$result['order_time']."',
								`date_added` = '".date('Y-m-d')."',
								`time_added` = '".date('H:i:s')."',
								roundtotal = '".$result['roundtotal']."',
								pay_online = '".$result['pay_online']."',
								`ftotalvalue` = '".$this->db->escape($result['ftotalvalue'])."',
								`ftotal_discount` = '".$this->db->escape($result['ftotal_discount'])."',
								`discount` = '".$this->db->escape($result['discount'])."',
								`discount_reason` = '".$this->db->escape($result['discount_reason'])."',
								`login_id` = '" . $this->db->escape($this->user->getId()) . "',
								`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");
			$order_id = $this->db->getLastId();


			$item_datas = $this->db->query("SELECT * FROM oc_order_item_app WHERE online_order_id = '".$result['online_order_id']."'");

			if($item_datas->num_rows > 0){
				foreach ($item_datas->rows as $ikey => $ivalue) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_items SET
									`order_id` = '" . $this->db->escape($order_id) . "',   
									`code` = '" . $this->db->escape($ivalue['item_id']) ."',
									`name` = '" . htmlspecialchars_decode($ivalue['item']) ."', 
									`qty` = '" . $this->db->escape($ivalue['qty']) . "', 
									`rate` = '" . $this->db->escape($ivalue['price']) . "',  
									`amt` = '" . $this->db->escape($ivalue['total']) . "',
									`ismodifier` = '1',  
									`nc_kot_status` = '0',
									`nc_kot_reason` = '',
									`transfer_qty` = '0',
									`parent_id` = '0', 
									`parent` = '0',  
									`subcategoryid` = '1',
									`kot_status` = '1',
									`kot_no` = '" . $this->db->escape($kot_no) . "',
									`prefix` = '',
									`pre_qty` = '',
									`kitchen_display` = '',
									`is_liq` = '0',
									`is_new` = '0',
									`cancelstatus` = '0',
									`tax1` = '" . $this->db->escape( $ivalue['gst']/2) . "',
									`tax2` = '" . $this->db->escape($ivalue['gst']/2) . "',
									`tax1_value` = '" . $this->db->escape( $ivalue['gst_val']/2) . "',
									`tax2_value` = '" . $this->db->escape( $ivalue['gst_val']/2) . "',
									`stax` = '',
									`captain_id` = '0',
									`captain_commission` = '0',
									`waiter_id` = '0',
									`waiter_commission` = '0',
									`message` = '',
									`packaging_amt` = '',
									`bill_date` = '".$last_open_date."',
									`date` = '".$result['order_date']."',
									`time` = '".$result['order_time']."',
									`login_id` = '" . $this->db->escape($this->user->getId()) . "',
									cancel_kot_reason = '',
									`login_name` = '" . $this->db->escape($this->user->getUserName()) . "',
									`discount_per` = '" . $this->db->escape($ivalue['discount_per']) . "',  
									`discount_value` = '" . $this->db->escape($ivalue['discount_value']) . "'
								");
				}
			}
		}

		return $order_id;
	}

	public function editLocation($location_id, $data) {
		if (!isset($data['parcel_detail'] )) {
			$data['parcel_detail'] = 0;
		}
		if (!isset($data['dboy_detail'] )) {
			$data['dboy_detail'] = 0;
		}

		if (!isset($data['a_to_z'] )) {
			$data['a_to_z'] = 0;
		}
		if (!isset($data['direct_bill'] )) {
			$data['direct_bill'] = 0;
		}
		if (!isset($data['is_app'] )) {
			$data['is_app'] = 0;
		}

		if (!isset($data['is_advance'] )) {
			$data['is_advance'] = 0;
		}

		if (!isset($data['kot_different'] )) {
			$data['kot_different'] = 0;
		}

		if (!isset($data['service_charge'] )) {
			$data['service_charge'] = 0;
		}
		
		$this->db->query("UPDATE " . DB_PREFIX . "location SET 
						location = '" . $this->db->escape($data['location']) . "',
						table_to = '" .$this->db->escape($data['table_to']). "',
						table_from = '" .$this->db->escape($data['table_from']). "',
						bill_copy = '" .$this->db->escape($data['bill_copy']). "',
						master_kot = '" .$this->db->escape($data['master_kot']). "',
						kot_copy = '" .$this->db->escape($data['kot_copy']). "',
						bot_copy = '" .$this->db->escape($data['bot_copy']). "',
						
						store_name = '" .$this->db->escape($data['store_name']). "',
						parcel_detail = '" .$this->db->escape($data['parcel_detail']). "',
						dboy_detail = '" .$this->db->escape($data['dboy_detail']). "',
						a_to_z = '" .$this->db->escape($data['a_to_z']). "',
						direct_bill = '" .$this->db->escape($data['direct_bill']). "',
						is_app = '" .$this->db->escape($data['is_app']). "',
						is_advance = '" .$this->db->escape($data['is_advance']). "',
						kot_different = '" .$this->db->escape($data['kot_different']). "',
						service_charge = '" .$this->db->escape($data['service_charge']). "',
						bill_printer_name = '" .$this->db->escape($data['bill_printer_name']). "',
						bill_printer_type = '" .$this->db->escape($data['bill_printer_type']). "',
						rate_id = '" . $this->db->escape($data['rate_id']) . "'
						WHERE location_id = '" . (int)$location_id . "'");
		return $location_id;
	}

	public function deleteLocation($location_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$location_id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getLocation($Location_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$Location_id . "' ");
		return $query->row;
	}

	public function getOnlineorder($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "order_item_app WHERE 1=1 ";

		// if (!empty($data['filter_location'])) {
		// 	$sql .= " AND location LIKE '" . $this->db->escape($data['filter_location']) . "%'";
		// }

		// if (!empty($data['filter_location_id'])) {
		//  	$sql .= " AND location_id = '" . $this->db->escape($data['filter_location_id']) . "' ";
		// }

		// $sort_data = array(
		// 	'location',
		// 	'table_from',
		// 	'table_to'
		// 	);

		// if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
		// 	$sql .= " ORDER BY " . $data['sort'];
		// } else {
		// 	$sql .= " ORDER BY location";
		// }

		// if (isset($data['order']) && ($data['order'] == 'DESC')) {
		// 	$sql .= " DESC";
		// } else {
		// 	$sql .= " ASC";
		// }

		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}

		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalOrders($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_item_app WHERE 1=1 ";

		// if (!empty($data['filter_location'])) {
		// 	$sql .= " AND location LIKE '" . $this->db->escape($data['filter_location']) . "%'";
		// }

		// if (!empty($data['filter_location_id'])) {
		// 	$sql .= " AND location_id = '" . $this->db->escape($data['filter_location_id']) . "' ";
		// }

		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function get_settings($key) {
		$setting_status = $this->db->query("SELECT * FROM settings_ador WHERE `key` = '".$key."'");
		//$this->log->write($setting_status);
		if($setting_status->num_rows > 0){
			return $setting_status->row['value'];
		} else {
			return '';
		}
	}
}