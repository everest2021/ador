<?php
class ModelCatalogSaleStat extends Model {
	
	public function mostFamousFood()
	{
		return $this->db->query("SELECT name, code, count(*) as total_cnt FROM `oc_order_items_report` WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = 0 GROUP BY code ORDER BY total_cnt DESC LIMIT 5")->rows;
	}

	public function mostFamousLiq()
	{
		return $this->db->query("SELECT name, code, count(*) as total_cnt FROM `oc_order_items_report` WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = 1 GROUP BY code ORDER BY total_cnt DESC LIMIT 5")->rows;
	}

	public function weeklyFamousfood($start_date, $end_date)
	{
		
		return $this->db->query("SELECT name, code, count(*) as total_cnt FROM `oc_order_items_report` WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = 0 AND bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' GROUP BY code ORDER BY total_cnt DESC LIMIT 5")->rows;

	}

	public function weeklyFamousLiq($start_date, $end_date)
	{
		
		return $this->db->query("SELECT name, code, count(*) as total_cnt FROM `oc_order_items_report` WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = 1 AND bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' GROUP BY code ORDER BY total_cnt DESC LIMIT 5")->rows;

	}

	public function highestSale()
	{
		return $this->db->query("SELECT SUM(grand_total) as g_total , bill_date FROM `oc_order_info_report` WHERE cancel_status = 0 AND bill_status = 1  GROUP BY bill_date ORDER BY g_total DESC LIMIT 1")->rows;

	}
}