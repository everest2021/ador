ALTER TABLE `oc_order_info` ADD `report_status` INT(11) NOT NULL DEFAULT '0' AFTER `shift_user_id`;

ALTER TABLE `oc_item` ADD `item_onoff_status` INT(11) NOT NULL DEFAULT '0' AFTER `normal_point`;

ALTER TABLE `oc_order_info` ADD `wera_order_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `report_status`;

ALTER TABLE `oc_order_items_report` ADD `packaging_amt` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `kitchen_display`;


ALTER TABLE `oc_item` ADD `packaging_amt` INT(11) NOT NULL DEFAULT '0' AFTER `item_onoff_status`;


ALTER TABLE `oc_order_info_report`  ADD `shiftclose_status` INT(11) NOT NULL  AFTER `new_total_payment`,  ADD `shift_id` INT(11) NOT NULL  AFTER `shiftclose_status`,  ADD `shift_date` DATE NOT NULL  AFTER `shift_id`,  ADD `shift_time` VARCHAR(255) NOT NULL  AFTER `shift_date`,  ADD `payment_type` VARCHAR(255) NOT NULL  AFTER `shift_time`,  ADD `shift_username` VARCHAR(255) NOT NULL  AFTER `payment_type`,  ADD `shift_user_id` VARCHAR(255) NOT NULL  AFTER `shift_username`,  ADD `report_status` INT(11) NOT NULL  AFTER `shift_user_id`,  ADD `wera_order_id` VARCHAR(255) NOT NULL  AFTER `report_status`,  ADD `order_from` VARCHAR(255) NOT NULL  AFTER `wera_order_id`,  ADD `order_packaging` FLOAT(10,2) NOT NULL  AFTER `order_from`,  ADD `packaging_cgst` FLOAT(10,2) NOT NULL  AFTER `order_packaging`,  ADD `packaging_sgst` FLOAT(10,2) NOT NULL  AFTER `packaging_cgst`,  ADD `packaging_cgst_percent` FLOAT(10,2) NOT NULL  AFTER `packaging_sgst`,  ADD `packaging_sgst_percent` FLOAT(10,2) NOT NULL  AFTER `packaging_cgst_percent`,  ADD `packaging` FLOAT(10,2) NOT NULL  AFTER `packaging_sgst_percent`;

ALTER TABLE `oc_order_info` ADD `order_from` VARCHAR(255) NOT NULL DEFAULT '' AFTER `wera_order_id`;
ALTER TABLE `oc_order_info`  ADD `order_packaging` FLOAT(10,2) NOT NULL  AFTER `order_from`,  ADD `packaging_cgst` FLOAT(10,2) NOT NULL  AFTER `order_packaging`,  ADD `packaging_sgst` FLOAT(10,2) NOT NULL  AFTER `packaging_cgst`,  ADD `packaging_cgst_percent` FLOAT(10,2) NOT NULL  AFTER `packaging_sgst`,  ADD `packaging_sgst_percent` FLOAT(10,2) NOT NULL  AFTER `packaging_cgst_percent`,  ADD `packaging` FLOAT(10,2) NOT NULL  AFTER `packaging_sgst_percent`




ALTER TABLE `oc_order_items` ADD `packaging` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `kitchen_display`;

ALTER TABLE `oc_order_info_report` ADD `report_status` INT(11) NOT NULL DEFAULT '0' AFTER `shift_user_id`, ADD `wera_order_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `report_status`, ADD `order_from` VARCHAR(255) NOT NULL DEFAULT '' AFTER `wera_order_id`, ADD `order_packaging` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `order_from`, ADD `packaging_cgst` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `order_packaging`, ADD `packaging_sgst` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `packaging_cgst`, ADD `packaging_cgst_percent` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `packaging_sgst`, ADD `packaging_sgst_percent` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `packaging_cgst_percent`, ADD `packaging` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `packaging_sgst_percent`;
ALTER TABLE `oc_order_items_report` ADD `packaging_amt` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `kitchen_display`;
ALTER TABLE `oc_order_info` ADD `packaging_cgst` FLOAT(10,2) NOT NULL DEFAULT '0.00' AFTER `order_from` ;  
ALTER TABLE `oc_order_info` ADD `packaging_sgst` FLOAT(10,2) NOT NULL DEFAULT '0.00' AFTER `packaging_cgst` ;

CREATE TABLE `oc_sale_promotion` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `from_date` date NOT NULL DEFAULT '0000-00-00',
  `to_date` date NOT NULL DEFAULT '0000-00-00',
  `from_time` time NOT NULL DEFAULT '00:00:00',
  `to_time` time NOT NULL DEFAULT '00:00:00',
  `sub_category_id` int(11) NOT NULL DEFAULT '0',
  `free_sub_category_id` int(11) NOT NULL DEFAULT '0',
  `discount_percentage` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `discount_rupees` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `amount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `point` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `per_point` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `point_rupees` decimal(15,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `oc_sale_promotion`  ADD PRIMARY KEY (`id`);

ALTER TABLE `oc_sale_promotion`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `oc_sale_promotion` ADD `days` TEXT NOT NULL DEFAULT '' AFTER `point_rupees`;

CREATE TABLE `oc_sale_promotion_items` (
  `id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) NOT NULL DEFAULT '0',
  `free_sub_cat` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `oc_sale_promotion_items` ADD `sale_promotion_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `oc_sale_promotion_items`  ADD PRIMARY KEY (`id`);

ALTER TABLE `oc_sale_promotion_items` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `oc_sale_promotion_items` ADD `quantity` INT(11) NOT NULL DEFAULT '0' AFTER `item_id`;

ALTER TABLE `oc_purchase_transaction` ADD `day_close_status` INT(11) NOT NULL AFTER `barcode_number`;

ALTER TABLE `oc_purchase_items_transaction` ADD `day_close_status` INT(11) NOT NULL AFTER `type`;

