<?php
require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
class ControllerCatalogInvoice extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/invoice');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/invoice');
		$this->load->model('catalog/product');

		$this->getList();
	}

	public function add() {
		//echo 'innn';exit;
		$this->load->language('catalog/invoice');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/invoice');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			 // echo '<pre>';
			 // print_r($this->request->post);
			 // exit;
			$invoice_id = $this->model_catalog_invoice->addInvoice($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_client'])) {
				$url .= '&filter_client=' . urlencode(html_entity_decode($this->request->get['filter_client'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_client_id'])) {
				$url .= '&filter_client_id=' . urlencode(html_entity_decode($this->request->get['filter_client_id'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_customer'])) {
				$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_customer_id'])) {
				$url .= '&filter_customer_id=' . urlencode(html_entity_decode($this->request->get['filter_customer_id'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_category'])) {
				$url .= '&filter_category=' . urlencode(html_entity_decode($this->request->get['filter_category'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . urlencode(html_entity_decode($this->request->get['filter_category_id'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_ref_no'])) {
				$url .= '&filter_ref_no=' . urlencode(html_entity_decode($this->request->get['filter_ref_no'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_site_code'])) {
				$url .= '&filter_site_code=' . urlencode(html_entity_decode($this->request->get['filter_site_code'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_po_number'])) {
				$url .= '&filter_po_number=' . urlencode(html_entity_decode($this->request->get['filter_po_number'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			//$url .= '&invoice_id='.$invoice_id.'&category_id='.$this->request->post['category_id'].'&first=1';

			$this->response->redirect($this->url->link('catalog/invoice', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		//echo "in";exit;
		$this->load->language('catalog/invoice');
		$this->load->model('catalog/product');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/invoice');
		//echo $this->request->server['REQUEST_METHOD'];exit;
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//echo "in";exit;
			$this->model_catalog_invoice->editInvoice($this->request->get['project_id'], $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_client'])) {
				$url .= '&filter_client=' . urlencode(html_entity_decode($this->request->get['filter_client'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_client_id'])) {
				$url .= '&filter_client_id=' . urlencode(html_entity_decode($this->request->get['filter_client_id'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_customer'])) {
				$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_customer_id'])) {
				$url .= '&filter_customer_id=' . urlencode(html_entity_decode($this->request->get['filter_customer_id'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_category'])) {
				$url .= '&filter_category=' . urlencode(html_entity_decode($this->request->get['filter_category'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . urlencode(html_entity_decode($this->request->get['filter_category_id'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_ref_no'])) {
				$url .= '&filter_ref_no=' . urlencode(html_entity_decode($this->request->get['filter_ref_no'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_site_code'])) {
				$url .= '&filter_site_code=' . urlencode(html_entity_decode($this->request->get['filter_site_code'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_po_number'])) {
				$url .= '&filter_po_number=' . urlencode(html_entity_decode($this->request->get['filter_po_number'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['refer'])) {
				$url .= '&refer=' . $this->request->get['refer'];	
			}

			//$url .= '&invoice_id='.$this->request->get['invoice_id'].'&category_id='.$this->request->post['category_id'].'&second=1';
			
			$this->response->redirect($this->url->link('catalog/invoice', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		// echo "<pre>";print_r($this->request->post);exit;
		$this->load->language('catalog/invoice');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/invoice');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $invoice_id) {
				$this->model_catalog_invoice->deleteInvoice($invoice_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_client'])) {
				$url .= '&filter_client=' . urlencode(html_entity_decode($this->request->get['filter_client'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_client_id'])) {
				$url .= '&filter_client_id=' . urlencode(html_entity_decode($this->request->get['filter_client_id'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_customer'])) {
				$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_customer_id'])) {
				$url .= '&filter_customer_id=' . urlencode(html_entity_decode($this->request->get['filter_customer_id'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_category'])) {
				$url .= '&filter_category=' . urlencode(html_entity_decode($this->request->get['filter_category'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . urlencode(html_entity_decode($this->request->get['filter_category_id'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_ref_no'])) {
				$url .= '&filter_ref_no=' . urlencode(html_entity_decode($this->request->get['filter_ref_no'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_site_code'])) {
				$url .= '&filter_site_code=' . urlencode(html_entity_decode($this->request->get['filter_site_code'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_po_number'])) {
				$url .= '&filter_po_number=' . urlencode(html_entity_decode($this->request->get['filter_po_number'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_invoice_no'])) {
				$url .= '&filter_invoice_no=' . urlencode(html_entity_decode($this->request->get['filter_invoice_no'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/invoice', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_client'])) {
			$filter_client = $this->request->get['filter_client'];
		} else {
			$filter_client = null;
		}

		if (isset($this->request->get['filter_client_id'])) {
			$filter_client_id = $this->request->get['filter_client_id'];
		} else {
			$filter_client_id = null;
		}

		if (isset($this->request->get['filter_customer'])) {
			$filter_customer = $this->request->get['filter_customer'];
		} else {
			$filter_customer = null;
		}

		if (isset($this->request->get['filter_customer_id'])) {
			$filter_customer_id = $this->request->get['filter_customer_id'];
		} else {
			$filter_customer_id = null;
		}

		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = null;
		}

		if (isset($this->request->get['filter_category_id'])) {
			$filter_category_id = $this->request->get['filter_category_id'];
		} else {
			$filter_category_id = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['filter_ref_no'])) {
			$filter_ref_no = $this->request->get['filter_ref_no'];
		} else {
			$filter_ref_no = null;
		}

		if (isset($this->request->get['filter_site_code'])) {
			$filter_site_code = $this->request->get['filter_site_code'];
		} else {
			$filter_site_code = null;
		}
		if (isset($this->request->get['filter_po_number'])) {
			$filter_po_number = $this->request->get['filter_po_number'];
		} else {
			$filter_po_number = null;
		}
		if (isset($this->request->get['filter_invoice_no'])) {
			$filter_invoice_no = $this->request->get['filter_invoice_no'];
		} else {
			$filter_invoice_no = null;
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'ref_no';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['refer'])) {
			$data['refer'] = $this->request->get['refer'];
		} else {
			$data['refer'] = 0;
		}

		$url = '';

		if (isset($this->request->get['filter_client'])) {
			$url .= '&filter_client=' . urlencode(html_entity_decode($this->request->get['filter_client'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_client_id'])) {
			$url .= '&filter_client_id=' . urlencode(html_entity_decode($this->request->get['filter_client_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer_id'])) {
			$url .= '&filter_customer_id=' . urlencode(html_entity_decode($this->request->get['filter_customer_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . urlencode(html_entity_decode($this->request->get['filter_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_category_id'])) {
			$url .= '&filter_category_id=' . urlencode(html_entity_decode($this->request->get['filter_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_ref_no'])) {
			$url .= '&filter_ref_no=' . urlencode(html_entity_decode($this->request->get['filter_ref_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_site_code'])) {
			$url .= '&filter_site_code=' . urlencode(html_entity_decode($this->request->get['filter_site_code'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_po_number'])) {
			$url .= '&filter_po_number=' . urlencode(html_entity_decode($this->request->get['filter_po_number'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_invoice_no'])) {
			$url .= '&filter_invoice_no=' . urlencode(html_entity_decode($this->request->get['filter_invoice_no'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/invoice', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/invoice/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/invoice/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['invoices'] = array();

		$filter_data = array(
			'filter_client'	  => $filter_client,
			'filter_client_id'  => $filter_client_id,
			'filter_customer'	  => $filter_customer,
			'filter_customer_id'  => $filter_customer_id,
			'filter_category'	  => $filter_category,
			'filter_category_id'  => $filter_category_id,
			'filter_status'	  => $filter_status,
			'filter_ref_no'	  => $filter_ref_no,
			'filter_invoice_no'	  => $filter_invoice_no,
			'filter_site_code'	  => $filter_site_code,
			'filter_po_number' => $filter_po_number,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		// $data['statuses'] = array(
		// 	''  => 'All',
		// 	'1' => 'Not Started',
		// 	'2' => 'Stuck',
		// 	'3' => 'Work In Progress',
		// 	'4' => 'Completed',
		// 	'5' => 'Revoked',
		// 	'6' => 'Near Deadline',
		// 	'7' => 'Deadline Crossed'
		// );

		$data['statuses'] = array(
			''  => 'All',
			'1' => 'WTS',
			'2' => 'Held Up',
			'3' => 'WIP',
			'4' => 'Completed',
			'5' => 'Site Drop',
		);

		$invoice_total = $this->model_catalog_invoice->getTotalInvoices($filter_data);

		$results = $this->model_catalog_invoice->getInvoicesnew($filter_data);
		// echo "<pre>";print_r($results);exit;
		foreach ($results as $result) {
			$category_names = $this->db->query("SELECT `name` FROM `oc_category` WHERE `category_id` = '".$result['category_id']."'");
			$category_name = '';
			if($category_names->num_rows > 0){
				$category_name = $category_names->row['name'];
				if($category_name == 'Repairs & Mantenance') {
					$category_name = "Repairs & Mantenance";
				} else if($category_name == 'Branding') {
					$category_name = "Branding";
				} 
			}
			//$billing_address = $this->db->query("SELECT `billing_address` FROM `oc_client` WHERE `client_id` = '".$result['client_id']."'");

			// echo'<pre>';
			// print_r($category_name);
			// exit();

			// $anchor_styles = "color: #666666 !important; cursor:pointer;";
			$client_names = $this->db->query("SELECT `client_name` FROM `is_client` WHERE `client_id` = '".$result['client_id']."'");
			$client_name = '';
			if($client_names->num_rows > 0){
				$client_name = $client_names->row['client_name'];
			}
			$customer_names = $this->db->query("SELECT `customer_name` FROM `oc_customer` WHERE `customer_id` = '".$result['customer_id']."'");
			$customer_name = '';
			if($customer_names->num_rows > 0){
				$customer_name = $customer_names->row['customer_name'];
			}

			$ref_no = $result['ref_prefix'].'/'.$result['ref_no'];
			$edit = $this->url->link('catalog/invoice/edit', 'token=' . $this->session->data['token'] . '&project_id=' . $result['project_id'] . $url, true);
			
			$invoice_datas = $this->db->query("SELECT `invoice_print_type` FROM `is_invoice` WHERE `project_id` = '".$result['project_id']."' ");
			
			$transport_invoice = '';
			if($invoice_datas->num_rows > 0){
				$invoice_data = $invoice_datas->row;
				
				if($invoice_data['invoice_print_type'] == 'material_labour'){
					$print_material = '';
					$transport_invoice = $this->url->link('catalog/invoice/transport', 'token=' . $this->session->data['token'] . '&project_id=' . $result['project_id'] . '&type=material' . $url, true);
					$invoice_ids = $this->db->query("SELECT `invoice_id`, `material_description`, `labour_description`, `total_material_total` FROM `is_invoice` WHERE `project_id` = '".$result['project_id']."' AND `type` = 'material' ");
					if($invoice_ids->num_rows > 0){
						if($invoice_ids->row['total_material_total'] != ''){
							$print_material_labour = $this->url->link('catalog/invoice/prints_material_labour', 'token=' . $this->session->data['token'] . '&project_id=' . $result['project_id'] . '&type=material' . $url, true);
						} else {
							$print_material_labour = '';
						}
					} else {
						$print_material_labour = '';
					}
				} else {
					$print_material_labour = '';
					$invoice_ids = $this->db->query("SELECT `invoice_id`, `material_description`, `labour_description`, `total_material_total` FROM `is_invoice` WHERE `project_id` = '".$result['project_id']."' AND `type` = 'material' ");
					if($invoice_ids->num_rows > 0){
						if($invoice_ids->row['total_material_total'] != '0.00'){
							$print_material = $this->url->link('catalog/invoice/prints', 'token=' . $this->session->data['token'] . '&project_id=' . $result['project_id'] . '&type=material' . $url, true);
							$transport_invoice = $this->url->link('catalog/invoice/transport', 'token=' . $this->session->data['token'] . '&project_id=' . $result['project_id'] . '&type=material' . $url, true);
						} else {
							$print_material = '';
						}
					} else {
						$print_material = '';
					}
				}
				$invoice_ids = $this->db->query("SELECT `labour_description`, `total_labour_total` FROM `is_invoice` WHERE `project_id` = '".$result['project_id']."' AND `type` = 'labour' ");
				if($invoice_ids->num_rows > 0){
					if($invoice_ids->row['total_labour_total'] != '0.00'){
						$print_labour = $this->url->link('catalog/invoice/prints_labour', 'token=' . $this->session->data['token'] . '&project_id=' . $result['project_id'] . '&type=labour' . $url, true);
					} else {
						$print_labour = '';
					}
				} else {
					$print_labour = '';
				}
			} else {
				$print_labour = '';
				$print_material = '';
				$print_material_labour = '';
			}

			$projectboqs = $this->db->query("SELECT * FROM `is_projectboq` WHERE `project_id` = '".$result['project_id']."' ");
			if($projectboqs->num_rows > 0){
				$print_boq = $this->url->link('catalog/invoice/prints_boq', 'token=' . $this->session->data['token'] . '&project_id=' . $result['project_id'] . $url, true);
			} else {
				$print_boq = '';
			}
			if ($result['invoice_prefix'] != '') {
				$prefix = $result['invoice_prefix'].'/';
			}else{
				$prefix = '';
			}

			if (isset($result['cancel_status']) && $result['cancel_status'] == 1) {
				$cancel_status = $result['cancel_status'];
				$anchor_styles = "color: red !important; cursor:pointer;";
			} else {
				$cancel_status = 0;
				$anchor_styles = "color: #666666 !important; cursor:pointer;";
			}
			$data['invoices'][] = array(
				'invoice_id' => $result['invoice_id'],
				'inv_id' => $prefix.$result['inv_id'],
				'ref_no'     => $ref_no,
				'po_number'     => $result['po_number'],
				'site_code'     => $result['site_code'],
				'category' => $category_name,
				'anchor_styles' => $anchor_styles,
				'client' => $client_name,
				'customer' => $customer_name,
				'edit'     => $edit,
				'print_material' => $print_material,
				'transport_invoice' => $transport_invoice,
				'print_labour' => $print_labour,
				'print_material_labour' => $print_material_labour,
				'print_boq'     => $print_boq,
				'cancel_status'     => $cancel_status,
			);
		}

		$data['base_link'] = $this->url->link('catalog/invoice', 'token=' . $this->session->data['token']);
		$data['base_link_1'] = $this->url->link('catalog/invoice/edit', 'token=' . $this->session->data['token']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_client'])) {
			$url .= '&filter_client=' . urlencode(html_entity_decode($this->request->get['filter_client'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_client_id'])) {
			$url .= '&filter_client_id=' . urlencode(html_entity_decode($this->request->get['filter_client_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer_id'])) {
			$url .= '&filter_customer_id=' . urlencode(html_entity_decode($this->request->get['filter_customer_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . urlencode(html_entity_decode($this->request->get['filter_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_category_id'])) {
			$url .= '&filter_category_id=' . urlencode(html_entity_decode($this->request->get['filter_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_ref_no'])) {
			$url .= '&filter_ref_no=' . urlencode(html_entity_decode($this->request->get['filter_ref_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_site_code'])) {
			$url .= '&filter_site_code=' . urlencode(html_entity_decode($this->request->get['filter_site_code'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_po_number'])) {
			$url .= '&filter_po_number=' . urlencode(html_entity_decode($this->request->get['filter_po_number'], ENT_QUOTES, 'UTF-8'));
		}
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_ref_no'] = $this->url->link('catalog/invoice', 'token=' . $this->session->data['token'] . '&sort=ref_no' . $url, true);
		$data['sort_status'] = $this->url->link('catalog/invoice', 'token=' . $this->session->data['token'] . '&sort=status' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_client'])) {
			$url .= '&filter_client=' . urlencode(html_entity_decode($this->request->get['filter_client'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_client_id'])) {
			$url .= '&filter_client_id=' . urlencode(html_entity_decode($this->request->get['filter_client_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer_id'])) {
			$url .= '&filter_customer_id=' . urlencode(html_entity_decode($this->request->get['filter_customer_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . urlencode(html_entity_decode($this->request->get['filter_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_category_id'])) {
			$url .= '&filter_category_id=' . urlencode(html_entity_decode($this->request->get['filter_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_ref_no'])) {
			$url .= '&filter_ref_no=' . urlencode(html_entity_decode($this->request->get['filter_ref_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_site_code'])) {
			$url .= '&filter_site_code=' . urlencode(html_entity_decode($this->request->get['filter_site_code'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_po_number'])) {
			$url .= '&filter_po_number=' . urlencode(html_entity_decode($this->request->get['filter_po_number'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $invoice_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/invoice', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($invoice_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($invoice_total - $this->config->get('config_limit_admin'))) ? $invoice_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $invoice_total, ceil($invoice_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['filter_client'] = $filter_client;
		$data['filter_client_id'] = $filter_client_id;
		$data['filter_invoice_no'] = $filter_invoice_no;
		$data['filter_customer'] = $filter_customer;
		$data['filter_customer_id'] = $filter_customer_id;
		$data['filter_category'] = $filter_category;
		$data['filter_category_id'] = $filter_category_id;
		$data['filter_status'] = $filter_status;
		$data['filter_ref_no'] = $filter_ref_no;
		$data['filter_site_code'] = $filter_site_code;
		$data['filter_po_number'] =$filter_po_number;
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];

		$this->response->setOutput($this->load->view('catalog/invoice_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['invoice_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		if (isset($this->error['inv_number'])) {
			$data['error_inv_number'] = $this->error['inv_number'];
		} else {
			$data['error_inv_number'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_client'])) {
			$url .= '&filter_client=' . urlencode(html_entity_decode($this->request->get['filter_client'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_client_id'])) {
			$url .= '&filter_client_id=' . urlencode(html_entity_decode($this->request->get['filter_client_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer_id'])) {
			$url .= '&filter_customer_id=' . urlencode(html_entity_decode($this->request->get['filter_customer_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . urlencode(html_entity_decode($this->request->get['filter_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_category_id'])) {
			$url .= '&filter_category_id=' . urlencode(html_entity_decode($this->request->get['filter_category_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_ref_no'])) {
			$url .= '&filter_ref_no=' . urlencode(html_entity_decode($this->request->get['filter_ref_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_site_code'])) {
			$url .= '&filter_site_code=' . urlencode(html_entity_decode($this->request->get['filter_site_code'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_po_number'])) {
			$url .= '&filter_po_number=' . urlencode(html_entity_decode($this->request->get['filter_po_number'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['refer'])) {
			$url .= '&refer=' . $this->request->get['refer'];	
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/invoice', 'token=' . $this->session->data['token'] . $url, true)
		);

		$inv_ids = $this->db->query("SELECT * FROM is_invoice WHERE project_id = '".$this->request->get['project_id']."' ");
		if ($inv_ids->num_rows > 0) {
			$inv_id = $inv_ids->row['inv_id'];
		}
		// echo $inv_id;
		// exit;

		if ($inv_id != 0) {
			$data['action'] = $this->url->link('catalog/invoice/edit', 'token=' . $this->session->data['token'] . $url . '&project_id=' . $this->request->get['project_id'], true);
		} else {
			$data['action'] = $this->url->link('catalog/invoice/add', 'token=' . $this->session->data['token'] . $url . '&project_id=' . $this->request->get['project_id'], true);
		}

		$data['cancel'] = $this->url->link('catalog/invoice', 'token=' . $this->session->data['token'] . $url, true);

		if(isset($this->request->get['project_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')){
			$invoice_info = $this->model_catalog_invoice->getInvoice($this->request->get['project_id'], 'material');
			$invoice_info_labour = $this->model_catalog_invoice->getInvoice($this->request->get['project_id'], 'labour');
			$project_info = $this->model_catalog_invoice->getProject($this->request->get['project_id']);
		}

		//echo "<pre>";print_r($invoice_info);exit;

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['company_id'])) {
			$data['company_id'] = $this->request->post['company_id'];
		} elseif (!empty($invoice_info)) {
			$data['company_id'] = $invoice_info['company_id'];
		} elseif (!empty($invoice_info_labour)) {
			$data['company_id'] = $invoice_info_labour['company_id'];
		} else {
			$data['company_id'] = '0';
		}

		if (isset($this->request->post['category'])) {
			$data['category'] = $this->request->post['category'];
		} elseif (!empty($invoice_info)) {
			$data['category'] = $invoice_info['category'];
		} elseif (!empty($invoice_info_labour)) {
			$data['category'] = $invoice_info_labour['category'];
		} elseif (!empty($project_info)) {
			$category_names = $this->db->query("SELECT `name` FROM `oc_category` WHERE `category_id` = '".$project_info['category_id']."' ");
			$category_name = '';
			if($category_names->num_rows > 0){
				$category_name = $category_names->row['name'];
			}
			$data['category'] = $category_name;
		} else {
			$data['category'] = '';
		}

		if (isset($this->request->post['category_id'])) {
			$data['category_id'] = $this->request->post['category_id'];
		} elseif (!empty($invoice_info) && $invoice_info['category_id'] != '0') {
			$data['category_id'] = $invoice_info['category_id'];
		} elseif (!empty($invoice_info_labour) && $invoice_info_labour['category_id'] != '0') {
			$data['category_id'] = $invoice_info_labour['category_id'];
		} elseif (!empty($project_info)) {
			$data['category_id'] = $project_info['category_id'];
		} else {
			$data['category_id'] = '0';
		}

		if($data['company_id'] == '1'){
			$co_abv='ISB';
		} elseif($data['company_id'] == '2'){
			$co_abv='IS';
		} elseif($data['company_id'] == '3'){
			$co_abv='IGS';
		} else {
			$co_abv='ISB';
		}

		$data['company_branch_data'] = array();
		if($data['company_id'] != '0'){
			$company_branch_datas = $this->db->query("SELECT `id`, `branch_label`  ,`branch_address_state`  FROM `is_company_branch` WHERE `company_id` = '".$data['company_id']."' ")->rows;
			$data['client_address_data'][0] = array(
				'id' => '0',
				'branch_label' => 'Please Select',
				'state' => '0',
			);
			$cnt = 1;
			foreach($company_branch_datas as $ckeys => $cvalues){
				$data['company_branch_data'][$cnt] = array(
					'id' => $cvalues['id'],
					'branch_label' => $cvalues['branch_label'],
					'state' => $cvalues['branch_address_state'],
				);
				$cnt ++;
			}			
		}

		if (isset($this->request->post['site_address_state'])) {
			$data['site_address_state'] = $this->request->post['site_address_state'];
		} /*elseif (!empty($invoice_info)) {
			$data['site_address_state'] = $invoice_info['site_address_state'];
		} elseif (!empty($invoice_info_labour)) {
			$data['site_address_state'] = $invoice_info_labour['site_address_state'];
		}*/ elseif (!empty($project_info)) {
			if($project_info['state'] == 'Please Select'){
				$data['site_address_state'] = '';
			} else {
				$data['site_address_state'] = $project_info['state'];
			}
		} else {
			$data['site_address_state'] = '';
		}

		///$data['state']=array(
		//	'MH'=> 'Maharashtra',
		/*	'CH'=> 'Chhatisgarh',
			'KR'=> 'Kerala',
			'KA'=> 'Karnataka',
			'RJ'=> 'Rajasthan',
			'UP'=> 'UP',
			'MP'=> 'MP',
			'TN'=> 'Tamil Nadu',
			'GJ'=> 'Gujrat',
			'AP'=> 'Andhra Pradesh',
			'MH'=> 'Arunachal Pradesh',
			'AS'=> 'Assam', 
			'BH'=> 'Bihar',
			'CH'=> 'Chandigarh',
			'DN'=> 'Dadra and Nagar Haveli', 
			'DD'=> 'Daman and Diu',
			'GA'=> 'Goa',
			'HR'=> 'Haryana',
			'HP'=> 'Himachal Pradesh',
			'JK'=> 'Jammu and Kashmir',
			'JA'=> 'Jharkhand'
			 );
*/
		$state=array(
			'Maharashtra'=>'MH',
			'Chhatisgarh'=>'CH',
			'Kerala'=> 'KR',
			'Karnataka'=> 'KA',
			'Rajasthan'=> 'RJ',
			'UP'=> 'UP',
			'MP'=> 'MP',
			'Tamil Nadu'=> 'TN',
			'Gujrat'=> 'GJ',
			'Andhra Pradesh'=> 'AP',
			'Arunachal Pradesh'=> 'Arunachal Pradesh',
			'Assam'=> 'AS', 
			'Bihar'=> 'BH',
			'Chandigarh'=> 'CH',
			'Dadra and Nagar Haveli'=> 'DN', 
			'Daman and Diu'=> 'DD',
			'Goa'=> 'GA',
			'Haryana'=> 'HR',
			'Himachal Pradesh'=> 'HP',
			'Jammu and Kashmir'=> 'JK',
			'Jharkhand'=> 'JA',
			'Delhi'=> 'DH'
		);

		$state=array(
			'Maharashtra'=>'MH',
			'Karnataka'=> 'KA',
			//'Gujrat'=> 'GJ',
			//'Madhya Pradesh'=> 'MP',
			// 'Tamil Nadu'=> 'TN',
			// 'Chhatisgarh'=>'CH',
			// 'Kerala'=> 'KR',
			// 'Rajasthan'=> 'RJ',
			// 'UP'=> 'UP',
			// 'Andhra Pradesh'=> 'AP',
			// 'Arunachal Pradesh'=> 'Arunachal Pradesh',
			// 'Assam'=> 'AS', 
			// 'Bihar'=> 'BH',
			// 'Chandigarh'=> 'CH',
			// 'Dadra and Nagar Haveli'=> 'DN', 
			// 'Daman and Diu'=> 'DD',
			// 'Goa'=> 'GA',
			// 'Haryana'=> 'HR',
			// 'Himachal Pradesh'=> 'HP',
			// 'Jammu and Kashmir'=> 'JK',
			// 'Jharkhand'=> 'JA',
			// 'Delhi'=> 'DH'
		);
		//echo $data['site_address_state'];
		//exit();
		$st = $data['site_address_state'];
		if(isset($state[$st])){
			$shortname = $state[$st];
		} else {
			$shortname = 'MH';
		}

		$invoice_options = array(
			//'material_labour' => 'Material Labour', 
			'individual' => 'Individual'
		);
		$data['invoice_options'] = $invoice_options;

		if (isset($this->request->post['invoice_print_type'])) {
			$data['invoice_print_type'] = $this->request->post['invoice_print_type'];
		} elseif (!empty($invoice_info)) {
			$data['invoice_print_type'] = $invoice_info['invoice_print_type'];
		} elseif (!empty($invoice_info_labour)) {
			$data['invoice_print_type'] = $invoice_info_labour['invoice_print_type'];
		} else {
			$data['invoice_print_type'] = 0;
		}

		if (isset($this->request->post['company_address_type'])) {
			$data['company_address_type'] = $this->request->post['company_address_type'];
		} elseif (!empty($invoice_info)) {
			$data['company_address_type'] = $invoice_info['company_address_type'];
		} elseif (!empty($invoice_info_labour)) {
			$data['company_address_type'] = $invoice_info_labour['company_address_type'];
		} else {
			$data['company_address_type'] = '';
		}

		if (isset($this->request->post['company_address'])) {
			$data['company_address'] = $this->request->post['company_address'];
		} elseif (!empty($invoice_info)) {
			$data['company_address'] = $invoice_info['company_address'];
		} elseif (!empty($invoice_info_labour)) {
			$data['company_address'] = $invoice_info_labour['company_address'];
		} else {
			$data['company_address'] = '';
		}

		if (isset($this->request->post['branch_address_type'])) {
			$data['branch_address_type'] = $this->request->post['branch_address_type'];
		} elseif (!empty($invoice_info)) {
			$data['branch_address_type'] = $invoice_info['branch_address_type'];
		} elseif (!empty($invoice_info_labour)) {
			$data['branch_address_type'] = $invoice_info_labour['branch_address_type'];
		} else {
			$data['branch_address_type'] = '';
		}

		if (isset($this->request->post['branch_address'])) {
			$data['branch_address'] = $this->request->post['branch_address'];
		} elseif (!empty($invoice_info)) {
			$data['branch_address'] = $invoice_info['branch_address'];
		} elseif (!empty($invoice_info_labour)) {
			$data['branch_address'] = $invoice_info_labour['branch_address'];
		} else {
			$data['branch_address'] = '';
		}

		if (isset($this->request->post['company_branch_state'])) {
			$data['company_branch_state'] = $this->request->post['company_branch_state'];
		} elseif (!empty($invoice_info)) {
			$data['company_branch_state'] = $invoice_info['company_branch_state'];
		} elseif (!empty($invoice_info_labour)) {
			$data['company_branch_state'] = $invoice_info_labour['company_branch_state'];
		} else {
			$data['company_branch_state'] = '';
		}

		if (isset($this->request->post['inv_id'])) {
			$data['inv_id'] = $this->request->post['inv_id'];
		} elseif (!empty($invoice_info) && $invoice_info['inv_id'] != '0') {
			$data['inv_id'] = $invoice_info['inv_id'];
		} elseif (!empty($invoice_info_labour) && $invoice_info_labour['inv_id'] != '0') {
			$data['inv_id'] = $invoice_info_labour['inv_id'];
		} else {
			$data['inv_id'] = '';
		}

		if (isset($this->request->post['invoice_status'])) {
			$data['invoice_status'] = $this->request->post['invoice_status'];
		} elseif (!empty($invoice_info)) {
			$data['invoice_status'] = $invoice_info['invoice_status'];
		} elseif (!empty($invoice_info_labour)) {
			$data['invoice_status'] = $invoice_info_labour['invoice_status'];
		} else {
			$data['invoice_status'] = 0;
		}

		if($data['inv_id'] == 0){
			$data['edit'] = '0';
		} else {
			$data['edit'] = '1';
		}

		if (isset($this->request->post['inv_id_labour'])) {
			$data['inv_id_labour'] = $this->request->post['inv_id_labour'];
		} elseif (!empty($invoice_info_labour) && $invoice_info_labour['inv_id'] != '0') {
			$data['inv_id_labour'] = $invoice_info_labour['inv_id'];
		} else {
			$data['inv_id_labour'] = 0;
		}

		if (isset($this->request->post['inv_number'])) {
			// echo'innn 1';exit;
			$data['inv_number'] = $this->request->post['inv_number'];
		} elseif (!empty($invoice_info) && $invoice_info['invoice_prefix'] != '') {
			// echo "innn 2";exit;
			if ($data['branch_address_type'] == '94') {
				// echo "innn 2 1";exit;
				$site_address_state_comp = 'Karnataka';
				$inv_id = $this->db->query("SELECT `inv_id` FROM is_invoice  WHERE `company_address_type` = '".$site_address_state_comp."' AND company_id = '".$invoice_info['company_id']."' ORDER BY invoice_id DESC, inv_id DESC");
			} elseif ($data['branch_address_type'] == '99' || $data['branch_address_type'] == '15' || $data['branch_address_type'] == '92') {
				// echo "innn 2 2";exit;
				if (!empty($invoice_info['client_address_state'])) {
					$site_address_state_comp = $invoice_info['client_address_state'];
					$inv_id = $this->db->query("SELECT `inv_id` FROM is_invoice  WHERE `client_address_state` = '".$site_address_state_comp."' AND company_id = '".$invoice_info['company_id']."' ORDER BY invoice_id DESC, inv_id DESC");

				} else {
					$site_address_state_comp = $invoice_info_labour['client_address_state'];
					$inv_id = $this->db->query("SELECT `inv_id` FROM is_invoice  WHERE `client_address_state` = '".$site_address_state_comp."' AND company_id = '".$invoice_info_labour['company_id']."' ORDER BY invoice_id DESC, inv_id DESC");
				}
			}

			// $inv_ids = $this->db->query("SELECT `inv_id` FROM is_invoice WHERE `inv_id` <> '0' ORDER BY invoice_id DESC LIMIT 1 ");
			// if ($inv_ids->num_rows > 0) {
			// 	$inv_id = $inv_ids->row['inv_id'] + 1;
			// } else {
			// 	$inv_id = 1;
			// }
			
			$data['invoice_prefix'] = $invoice_info['invoice_prefix'];
			$data['inv_number'] = $data['invoice_prefix'].'/'.$data['inv_id'];
			/*echo "<pre>";
			echo "invoice_prefix  ";
			print_r($data['invoice_prefix']);
			echo "<pre>";
			echo "inv_number  ";
			print_r($data['inv_number']);
			echo "<pre>";
			echo "inv_id  ";
			print_r($data['inv_id']);
			exit;*/
		} elseif (!empty($invoice_info_labour) && $invoice_info['invoice_prefix'] != '') {
			// echo "innn 3";exit;
			$data['invoice_prefix'] = $invoice_info_labour['invoice_prefix'];
			$data['inv_number'] = $data['invoice_prefix'].'/'.$data['inv_id'];
		} else {//echo'inn';
			// echo "innn 4";exit;
			if(date('n') <= 3){
				// $prev_year = date('y',strtotime("-1 year"));
				// $current_year = date('y');
				// $yer_val = $prev_year.$current_year;
				$new_prev_year = date('y');
				$new_current_year = date('y',strtotime("+1 year"));
				$yer_val = $new_prev_year.$new_current_year;
				/*echo "<pre>";
				print_r($yer_val);
				exit;*/
			} else {
				$current_year = date('y');
				$next_year = date('y',strtotime("+1 year"));
				$yer_val = $current_year.$next_year;
			}
			
			if($invoice_info['company_id'] == 1 || $invoice_info['company_id'] == 2) {
				// $state=array(
				// 	'Maharashtra'=>'Maharashtra',
				// 	'Karnataka'=> 'Karnataka',
				// );
				// $st = $data['site_address_state'];
				
				// if(isset($state[$st])){
				// 	$site_address_state_comp = $state[$st];
				// } else {
				// 	$site_address_state_comp = 'Maharashtra';
				// }

				if ($invoice_info['company_address_type'] == '94') {
					$inv_prefix = 'ISB/2223/KA';
					$shortname = 'KA';
					$inv_id = $this->db->query("SELECT `inv_id` FROM is_invoice  WHERE `company_address_type` = '94' AND inv_id <> 0 AND company_id = '".$invoice_info['company_id']."' AND `invoice_prefix` = '".$inv_prefix."' ORDER BY invoice_id DESC LIMIT 1 ");
				} else {
					$inv_prefix = 'ISB/2223/MH';
					$inv_id = $this->db->query("SELECT `inv_id` FROM is_invoice  WHERE `company_address_type` <> '94' AND inv_id <> 0 AND company_id = '".$invoice_info['company_id']."' AND `invoice_prefix` = '".$inv_prefix."' ORDER BY invoice_id DESC LIMIT 1 ");
					
					$shortname = 'MH';
				}
				
				// $inv_id = $this->db->query("SELECT `inv_id` FROM is_invoice  WHERE `actual_site_address_state` = '".$site_address_state_comp."' AND company_id = '".$invoice_info['company_id']."' AND `invoice_date` >= '2020-04-01' ORDER BY inv_id DESC ");
				if ($inv_id->num_rows > 0) {
					$inv_id = $inv_id->row['inv_id'] + 1;
				} else {
					$inv_id = 1;
				}
			}else{

				$inv_prefix = 'IGS/2223/MH';
				$inv_id = $this->db->query("SELECT `inv_id` FROM is_invoice  WHERE company_id = '".$invoice_info['company_id']."' AND `inv_id` <> 0 AND company_id = '".$invoice_info['company_id']."' AND `invoice_prefix` = '".$inv_prefix."' ORDER BY invoice_id DESC LIMIT 1 ");

				if ($inv_id->num_rows > 0) {
					$inv_id = $inv_id->row['inv_id'] + 1;
				} else {
					$inv_id = 1;
				}
				$shortname = 'MH';		
			}

			//this is for plus one inv id for all Karnataka and Maharashtra
			// $inv_ids = $this->db->query("SELECT `inv_id` FROM is_invoice WHERE company_id = '".$invoice_info['company_id']."' AND `inv_id` <> '0' ORDER BY invoice_id DESC LIMIT 1 ");
			// if ($inv_ids->num_rows > 0) {
			// 	$inv_id = $inv_ids->row['inv_id'] + 1;
			// } else {
			// 	$inv_id = 1;
			// }
			//this is for plus one inv id for all Karnataka and Maharashtra
			
			$data['invoice_prefix'] = $co_abv.'/'.$yer_val;

			$data['inv_number'] = $data['invoice_prefix'].'/'.$shortname.'/'.$inv_id;

			
		}
		
		
		if (isset($this->request->post['invoice_date'])) {
			$data['invoice_date'] = $this->request->post['invoice_date'];
		} elseif (!empty($invoice_info)) {
			if($invoice_info['invoice_date'] != '1970-01-01' && $invoice_info['invoice_date'] != '0000-00-00'){
				$data['invoice_date'] = date('d-m-Y', strtotime($invoice_info['invoice_date']));
			} else {
				$data['invoice_date'] = date('d-m-Y');
			}
		} else {
			$data['invoice_date'] = date('d-m-Y');
		}

		if (isset($this->request->post['invoice_date_labour'])) {
			$data['invoice_date_labour'] = $this->request->post['invoice_date_labour'];
		} elseif (!empty($invoice_info_labour)) {
			if($invoice_info_labour['invoice_date'] != '1970-01-01' && $invoice_info_labour['invoice_date'] != '0000-00-00'){
				$data['invoice_date_labour'] = date('d-m-Y', strtotime($invoice_info_labour['invoice_date']));
			} else {
				$data['invoice_date_labour'] = date('d-m-Y');
			}
		} else {
			$data['invoice_date_labour'] = date('d-m-Y');
		}

		if (isset($this->request->post['invoice_submission_date'])) {
			$data['invoice_submission_date'] = $this->request->post['invoice_submission_date'];
		} elseif (!empty($invoice_info)) {
			if($invoice_info['invoice_submission_date'] != '1970-01-01' && $invoice_info['invoice_submission_date'] != '0000-00-00'){
				$data['invoice_submission_date'] = date('d-m-Y', strtotime($invoice_info['invoice_submission_date']));
			} else {
				$data['invoice_submission_date'] = date('d-m-Y');
			}
		} elseif (!empty($invoice_info)) {
			$data['invoice_submission_date'] = $invoice_info['invoice_submission_date'];
		} else {
			$data['invoice_submission_date'] = date('d-m-Y');
		}


		if (isset($this->request->post['invoice_datess'])) {
			$data['invoice_datess'] = $this->request->post['invoice_datess'];
		} elseif (!empty($invoice_info)) {
			if($invoice_info['invoice_date'] != '1970-01-01' && $invoice_info['invoice_date'] != '0000-00-00'){
				$data['invoice_datess'] = date('d-m-Y', strtotime($invoice_info['invoice_date']));
			} else {
				$data['invoice_datess'] = date('d-m-Y');
			}
		} elseif (!empty($invoice_info)) {
			$data['invoice_datess'] = $invoice_info['invoice_date'];
		} else {
			$data['invoice_datess'] = date('d-m-Y');
		}
         
  //        if (isset($this->request->post['invoice_submission_date'])) {
		// 	$data['invoice_submission_date'] = $this->request->post['invoice_submission_date'];
		// } elseif (!empty($invoice_submission_date)) {
		// 	if($invoice_submission_date['invoice_date'] != '0000-00-00'){
		// 		$data['invoice_submission_date'] = date('d-m-Y', strtotime($invoice_info['invoice_submission_date']));
		// 	} else {
		// 		$data['invoice_submission_date'] = '';
		// 	}
		// } else {
		// 	$data['invoice_submission_date'] = date('d-m-Y');
		// }

		//echo "<pre>";print_r($invoice_info);exit;


		if (isset($this->request->post['client'])) {
			$data['client'] = $this->request->post['client'];
			$data['client_id'] = $this->request->post['client_id'];
		} elseif (!empty($invoice_info)) {
			$data['client'] = $invoice_info['client'];
			$data['client_id'] = $invoice_info['client_id'];
		} elseif (!empty($invoice_info_labour)) {
			$data['client'] = $invoice_info_labour['client'];
			$data['client_id'] = $invoice_info_labour['client_id'];
		} elseif (!empty($project_info)) {
			$client_names = $this->db->query("SELECT `client_name` FROM `is_client` WHERE `client_id` = '".$project_info['client_id']."' ");
			$client_name = '';
			if($client_names->num_rows > 0){
				$client_name = $client_names->row['client_name'];
			}
			$data['client'] = $client_name;
			$data['client_id'] = $project_info['client_id'];
		} else {
			$data['client'] = '';
			$data['client_id'] = '0';
		}

		if (isset($this->request->post['customer'])) {
			$data['customer'] = $this->request->post['customer'];
		} elseif (!empty($invoice_info)) {
			$data['customer'] = $invoice_info['customer'];
			//echo "in";exit;
		} elseif (!empty($invoice_info_labour)) {
			$data['customer'] = $invoice_info_labour['customer'];
		} elseif (!empty($project_info)) {
			$customer_names = $this->db->query("SELECT `customer_name` FROM `oc_customer` WHERE `customer_id` = '".$project_info['customer_id']."'");
			$customer_name = '';
			if($customer_names->num_rows > 0){
				$customer_name = $customer_names->row['customer_name'];
			}
			$data['customer'] = $customer_name;
		} else {
			$data['customer'] = '';
		}

		if (isset($this->request->post['client_address_type'])) {
			$data['client_address_type'] = $this->request->post['client_address_type'];
		} elseif (!empty($invoice_info)) {
			$data['client_address_type'] = $invoice_info['client_address_type'];
		} elseif (!empty($invoice_info_labour)) {
			$data['client_address_type'] = $invoice_info_labour['client_address_type'];
		} else {
			$data['client_address_type'] = '';
		}
 

		$data['rev_types']=array(
			'1' => 'Yes',
			'2' => 'NO',
		);


		if (isset($this->request->post['rev_type'])) {
			$data['rev_type'] = $this->request->post['rev_type'];
		} elseif (!empty($invoice_info)) {
			$data['rev_type'] = $invoice_info['reverse_charges_id'];
		} elseif (!empty($invoice_info_labour)) {
			$data['rev_type'] = $invoice_info_labour['reverse_charges_id'];
		} else {
			$data['rev_type'] = '';
		}




		$data['client_address_data'] = array();
		if($data['client'] != ''){
			$client_address_datas = $this->db->query("SELECT `id`, `address_type` FROM `is_client_vat` WHERE `client_id` = '".$data['client_id']."' ")->rows;
			$data['client_address_data'][0] = array(
				'id' => '0',
				'address_type' => 'Please Select',
			);
			$cnt = 1;
			foreach($client_address_datas as $ckeys => $cvalues){
				$data['client_address_data'][$cnt] = array(
					'id' => $cvalues['id'],
					'address_type' => $cvalues['address_type']
				);
				$cnt ++;
			}
			$data['client_address_data'][$cnt] = array(
				'id' => 'other',
				'address_type' => 'Other',
			);
		}

		if (isset($this->request->post['client_address_heading'])) {
			$data['client_address_heading'] = $this->request->post['client_address_heading'];
		} elseif (!empty($invoice_info)) {
			$data['client_address_heading'] = $invoice_info['client_address_heading'];
		} elseif (!empty($invoice_info_labour)) {
			$data['client_address_heading'] = $invoice_info_labour['client_address_heading'];
		} else {
			$data['client_address_heading'] = '';
		}

		if (isset($this->request->post['client_address'])) {
			$data['client_address'] = $this->request->post['client_address'];
		} elseif (!empty($invoice_info)) {
			$data['client_address'] = $invoice_info['client_address'];
		} elseif (!empty($invoice_info_labour)) {
			$data['client_address'] = $invoice_info_labour['client_address'];
		} else {
			$data['client_address'] = '';
		}

		if (isset($this->request->post['client_address_state'])) {
			$data['client_address_state'] = $this->request->post['client_address_state'];
		} elseif (!empty($invoice_info)) {
			$data['client_address_state'] = $invoice_info['client_address_state'];
		} elseif (!empty($invoice_info_labour)) {
			$data['client_address_state'] = $invoice_info_labour['client_address_state'];
		} else {
			$data['client_address_state'] = '';
		}

		if (isset($this->request->post['client_address_tin'])) {
			$data['client_address_tin'] = $this->request->post['client_address_tin'];
		} elseif (!empty($invoice_info)) {
			$data['client_address_tin'] = $invoice_info['client_address_tin'];
		} elseif (!empty($invoice_info_labour)) {
			$data['client_address_tin'] = $invoice_info_labour['client_address_tin'];
		} else {
			$data['client_address_tin'] = '';
		}
		if (isset($this->request->post['client_address_gst'])) {
			$data['client_address_gst'] = $this->request->post['client_address_gst'];
		} elseif (!empty($invoice_info)) {
			$data['client_address_gst'] = $invoice_info['gst_no'];
		} elseif (!empty($invoice_info_labour)) {
			$data['client_address_gst'] = $invoice_info_labour['client_address_gst'];
		} else {
			$data['client_address_gst'] = '';
		}

		if (isset($this->request->post['site_code'])) {
			$data['site_code'] = $this->request->post['site_code'];
		} elseif (!empty($invoice_info)) {
			$data['site_code'] = $invoice_info['site_code'];
		} elseif (!empty($invoice_info_labour)) {
			$data['site_code'] = $invoice_info_labour['site_code'];
		} elseif (!empty($project_info)) {
			$data['site_code'] = $project_info['site_code'];
		} else {
			$data['site_code'] = '';
		}

		if (isset($this->request->post['approval_type'])) {
			$data['approval_type'] = $this->request->post['approval_type'];
		} elseif (!empty($invoice_info)) {
			$data['approval_type'] = $invoice_info['approval_type'];
		} elseif (!empty($invoice_info_labour)) {
			$data['approval_type'] = $invoice_info_labour['approval_type'];
		} else {
			$data['approval_type'] = '1';
		}

		$data['approval_types'] = array(
			'1' => 'PO',
			'2' => 'BOQ',
			'3' => 'Other',
		);		

		if (isset($this->request->post['approved_by'])) {
			$data['approved_by'] = $this->request->post['approved_by'];
		} elseif (!empty($invoice_info)) {
			$data['approved_by'] = $invoice_info['approved_by'];
		} elseif (!empty($invoice_info_labour)) {
			$data['approved_by'] = $invoice_info_labour['approved_by'];
		} else {
			$data['approved_by'] = '';
		}

		if (isset($this->request->post['email_subject'])) {
			$data['email_subject'] = $this->request->post['email_subject'];
		} elseif (!empty($invoice_info)) {
			$data['email_subject'] = $invoice_info['email_subject'];
		} elseif (!empty($invoice_info_labour)) {
			$data['email_subject'] = $invoice_info_labour['email_subject'];
		} else {
			$data['email_subject'] = '';
		}

		if (isset($this->request->post['email_amount'])) {
			$data['email_amount'] = $this->request->post['email_amount'];
		} elseif (!empty($invoice_info)) {
			$data['email_amount'] = $invoice_info['email_amount'];
		} elseif (!empty($invoice_info_labour)) {
			$data['email_amount'] = $invoice_info_labour['email_amount'];
		} else {
			$data['email_amount'] = '';
		}

		if (isset($this->request->post['po_number'])) {
			$data['po_number'] = $this->request->post['po_number'];
		} elseif (!empty($invoice_info)) {
			$data['po_number'] = $invoice_info['po_number'];
		} elseif (!empty($invoice_info_labour)) {
			$data['po_number'] = $invoice_info_labour['po_number'];
		} elseif (!empty($project_info)) {
			$data['po_number'] = $project_info['po_number'];
		} else {
			$data['po_number'] = '';
		}

		if (isset($this->request->post['po_date'])) {
			$data['po_date'] = $this->request->post['po_date'];
		} elseif (!empty($invoice_info)) {
			if($invoice_info['po_date'] != '1970-01-01' && $invoice_info['po_date'] != '0000-00-00'){
				$data['po_date'] = date('d-m-Y', strtotime($invoice_info['po_date']));
			} else {
				$data['po_date'] = date('d-m-Y');
			}
		} elseif (!empty($invoice_info_labour)) {
			if($invoice_info_labour['po_date'] != '1970-01-01' && $invoice_info_labour['po_date'] != '0000-00-00'){
				$data['po_date'] = date('d-m-Y', strtotime($invoice_info_labour['po_date']));
			} else {
				$data['po_date'] = date('d-m-Y');
			}
		} elseif (!empty($project_info)) {
			if($project_info['po_date'] != '1970-01-01' && $project_info['po_date'] != '0000-00-00'){
				$data['po_date'] = date('d-m-Y', strtotime($project_info['po_date']));
			} else {
				$data['po_date'] = date('d-m-Y');
			}
		} else {
			$data['po_date'] = date('d-m-Y');
		}

		if (isset($this->request->post['ref_no'])) {
			$data['ref_no'] = $this->request->post['ref_no'];
		} elseif (!empty($invoice_info)) {
			$data['ref_no'] = $invoice_info['ref_no'];
		} elseif (!empty($invoice_info_labour)) {
			$data['ref_no'] = $invoice_info_labour['ref_no'];
		} else {
			$data['ref_no'] = '';
		}

		if (isset($this->request->post['ref_date'])) {
			$data['ref_date'] = $this->request->post['ref_date'];
		} elseif (!empty($invoice_info)) {
			if($invoice_info['ref_date'] != '1970-01-01' && $invoice_info['ref_date'] != '0000-00-00'){
				$data['ref_date'] = date('d-m-Y', strtotime($invoice_info['ref_date']));
			} else {
				$data['ref_date'] = date('d-m-Y');
			}
		} elseif (!empty($invoice_info_labour)) {
			if($invoice_info_labour['ref_date'] != '1970-01-01' && $invoice_info_labour['ref_date'] != '0000-00-00'){
				$data['ref_date'] = date('d-m-Y', strtotime($invoice_info_labour['ref_date']));
			} else {
				$data['ref_date'] = date('d-m-Y');
			}
		} else {
			$data['ref_date'] = date('d-m-Y');
		}

		if (isset($this->request->post['atm_id'])) {
			$data['atm_id'] = $this->request->post['atm_id'];
		} elseif (!empty($invoice_info)) {
			$data['atm_id'] = $invoice_info['atm_id'];
		} elseif (!empty($invoice_info_labour)) {
			$data['atm_id'] = $invoice_info_labour['atm_id'];
		} else {
			$data['atm_id'] = '';
		}

		if (isset($this->request->post['site_address'])) {
			$data['site_address'] = $this->request->post['site_address'];
		} elseif (!empty($invoice_info)) {
			$data['site_address'] = $invoice_info['site_address'];
		} elseif (!empty($invoice_info_labour)) {
			$data['site_address'] = $invoice_info_labour['site_address'];
		} elseif (!empty($project_info)) {
			$data['site_address'] = $project_info['site_address'];
		} else {
			$data['site_address'] = '';
		}

		if (isset($this->request->post['project_id'])) {
			$data['project_id'] = $this->request->post['project_id'];
		} elseif (!empty($invoice_info)) {
			$data['project_id'] = $invoice_info['project_id'];
		} elseif (!empty($invoice_info_labour)) {
			$data['project_id'] = $invoice_info_labour['project_id'];
		} elseif (!empty($project_info)) {
			$data['project_id'] = $project_info['project_id'];
		} else {
			$data['project_id'] = '';
		}

		$companys = $this->model_catalog_invoice->getCompanyname();
		$data['companys'] = array();
		$data['companys'][0] = array(
			'company_id' => '0',
			'company_name' => 'Please Select',
		);
		$cnt = 1;
		foreach($companys as $ckeys => $cvalues){
			$data['companys'][$cnt] = array(
				'company_id' => $cvalues['company_id'],
				'company_name' => $cvalues['company_name'],
			);
			$cnt ++;
		
		}
		// echo "<pre>";print_r($this->request->post);exit;
		if (isset($this->request->post['material_datas'])) {
			$data['material_datas'] = $this->request->post['material_datas'];
		} elseif (!empty($invoice_info)) {
			$material_datas = array();
			for($i=1; $i<=10;$i++){
				$material_datass = $this->db->query("SELECT * FROM `is_invoice_material` WHERE `project_id` = '".$data['project_id']."' AND `invoice_id` = '".$data['inv_id']."' AND `inc_id` = '".$i."' ");
				// echo "<pre>";
				// print_r("SELECT * FROM `is_invoice_material` WHERE `project_id` = '".$data['project_id']."' AND `invoice_id` = '".$data['inv_id']."' AND `inc_id` = '".$i."'");
				// exit;
				if($material_datass->num_rows > 0){
					$material_data = $material_datass->row;
					$material_hsn_code = '';
					$material_taxable_value = 0.00;
					$material_tax_rate = 0.00;
					$material_tax_value = 0.00;
					$material_amount = 0.00;
					// echo '<pre>';
					// print_r($material_data);
					// exit;
					if($material_data['material_hsn_code'] == ''){
						$material_taxable_value = round($material_data['material_rate'] * $material_data['material_quantity']); 
						$product_gst_ids = $this->db->query("SELECT * FROM `is_productfinished` WHERE `productfinished_id` = '".$material_data['material_id']."' ");
						if($product_gst_ids->num_rows > 0){
							$gst_id = $product_gst_ids->row['gst_type_id'];
							$gsn_datas = $this->db->query("SELECT `hsn`, `value` FROM `is_gst` WHERE `gst_id` = '".$gst_id."' ");
							if($gsn_datas->num_rows > 0){
								$gsn_data = $gsn_datas->row;
								$material_hsn_code = $gsn_data['hsn'];
								$material_tax_rate = $gsn_data['value'];
								$material_tax_value = (($material_taxable_value * $material_tax_rate) / 100);
								//$material_tax_value = number_format($material_tax_value, 2, '.', '');
								$material_tax_value = round($material_tax_value);
							}	
						}
						$material_amount = $material_taxable_value + $material_tax_value;
					} else {
						$material_hsn_code = $material_data['material_hsn_code'];
						$material_taxable_value = $material_data['material_taxable_value'];
						$material_tax_rate = $material_data['material_tax_rate'];
						$material_tax_value = $material_data['material_tax_value'];
						$material_amount = $material_data['material_amount'];
					}

					$material_datas[] = array(
						'material_id' => $material_data['material_id'],
						'material_description' => $material_data['material_description'],
						'material_hsn_code' => $material_hsn_code,
						'material_rate' => $material_data['material_rate'],
						'material_quantity' => $material_data['material_quantity'],
						'material_taxable_value' => $material_taxable_value,
						'material_tax_rate' => $material_tax_rate,
						'material_tax_value' => $material_tax_value,
						'material_amount' => $material_amount,
						'new' => 0,
						'inc_id' => $i,
					);
				} else {
					$material_datas[] = array(
						'material_id' => '',
						'material_description' => '',
						'material_hsn_code' => '',
						'material_taxable_value' => '',
						'material_tax_rate' => '',
						'material_quantity' => '',
						'material_rate' => '',
						'material_tax_value' => '',
						'material_amount' => '',
						'new' => 1,
						'inc_id' => $i,
					);
				}
			}		
			$data['material_datas'] = $material_datas;
		} else {
			$material_datas = array();
			for($i=1; $i<=10;$i++){
				$material_datas[] = array(
					'material_description' => '',
					'material_hsn_code' => '',
					'material_taxable_value' => '',
					'material_tax_rate' => '',
					'material_id' => '',
					'material_quantity' => '',
					'material_rate' => '',
					'material_tax_value' => '',
					'material_amount' => '',
					'new' => 1,
					'inc_id' => $i,
				);
			}		
			$data['material_datas'] = $material_datas;
		}

		// echo '<pre>';
		// print_r($data['material_datas']);
		// exit;

		// if (isset($this->request->post['material_description'])) {
		// 	$data['material_description'] = $this->request->post['material_description'];
		// } elseif (!empty($invoice_info)) {
		// 	$data['material_description'] = $invoice_info['material_description'];
		// } else {
		// 	$data['material_description'] = '';
		// }

		// if (isset($this->request->post['material_quantity'])) {
		// 	$data['material_quantity'] = $this->request->post['material_quantity'];
		// } elseif (!empty($invoice_info)) {
		// 	if($invoice_info['material_quantity'] == ''){
		// 		$data['material_quantity'] = 0;
		// 	} else {
		// 		$data['material_quantity'] = $invoice_info['material_quantity'];
		// 	}
		// } else {
		// 	$data['material_quantity'] = 0;
		// }

		// if (isset($this->request->post['material_rate'])) {
		// 	$data['material_rate'] = $this->request->post['material_rate'];
		// } elseif (!empty($invoice_info)) {
		// 	if($invoice_info['material_rate'] == ''){
		// 		$data['material_rate'] = 0;
		// 	} else {
		// 		$data['material_rate'] = $invoice_info['material_rate'];
		// 	}
		// } else {
		// 	$data['material_rate'] = 0;
		// }

		// if (isset($this->request->post['material_amount'])) {
		// 	$data['material_amount'] = $this->request->post['material_amount'];
		// } elseif (!empty($invoice_info)) {
		// 	if($invoice_info['material_amount'] == ''){
		// 		$data['material_amount'] = 0;
		// 	} else {
		// 		$data['material_amount'] = $invoice_info['material_amount'];
		// 	}
		// } else {
		// 	$data['material_amount'] = 0;
		// }

		if (isset($this->request->post['material_vat'])) {
			$data['material_vat'] = $this->request->post['material_vat'];
		} elseif (!empty($invoice_info)) {
			$data['material_vat'] = $invoice_info['material_vat'];
		} else {
			$data['material_vat'] = '';
		}

		if (isset($this->request->post['material_vat_rate'])) {
			$data['material_vat_rate'] = $this->request->post['material_vat_rate'];
		} elseif (!empty($invoice_info)) {
			if($invoice_info['material_vat_rate'] == ''){
				$data['material_vat_rate'] = 0;
			} else {
				$data['material_vat_rate'] = $invoice_info['material_vat_rate'];	
			}
		} else {
			$data['material_vat_rate'] = 0;
		}

		if (isset($this->request->post['material_cst'])) {
			$data['material_cst'] = $this->request->post['material_cst'];
		} elseif (!empty($invoice_info)) {
			$data['material_cst'] = $invoice_info['material_cst'];
		} else {
			$data['material_cst'] = '0';
		}

		if (isset($this->request->post['material_cst_rate'])) {
			$data['material_cst_rate'] = $this->request->post['material_cst_rate'];
		} elseif (!empty($invoice_info)) {
			if($invoice_info['material_cst_rate'] == ''){
				$data['material_cst_rate'] = 0;
			} else {
				$data['material_cst_rate'] = $invoice_info['material_cst_rate'];
			}
		} else {
			$data['material_cst_rate'] = 0;
		}

		if (isset($this->request->post['material_octroi'])) {
			$data['material_octroi'] = $this->request->post['material_octroi'];
		} elseif (!empty($invoice_info)) {
			$data['material_octroi'] = $invoice_info['material_octroi'];
		} else {
			$data['material_octroi'] = '';
		}

		if (isset($this->request->post['material_octroi_rate'])) {
			$data['material_octroi_rate'] = $this->request->post['material_octroi_rate'];
		} elseif (!empty($invoice_info)) {
			if($invoice_info['material_octroi_rate'] == ''){
				$data['material_octroi_rate'] = 0;
			} else {
				$data['material_octroi_rate'] = $invoice_info['material_octroi_rate'];
			}
		} else {
			$data['material_octroi_rate'] = 0;
		}

		if (isset($this->request->post['material_advance'])) {
			$data['material_advance'] = $this->request->post['material_advance'];
		} elseif (!empty($invoice_info)) {
			if($invoice_info['material_advance'] == '' || $invoice_info['material_advance'] == '0.00'){
				$payment_datas = $this->db->query("SELECT * FROM `is_invoice_payment` WHERE `project_id` = '".$project_info['project_id']."' AND `invoice_id` = '0' ")->rows;
				$total_received = 0;
				foreach($payment_datas as $pkey => $pvalue){
					$total_received += $pvalue['amount_received'];
				}
				$data['material_advance'] = number_format($total_received, 2, '.', '');
			} else {
				$data['material_advance'] = $invoice_info['material_advance'];
			}
		} elseif (!empty($project_info)) {
			$payment_datas = $this->db->query("SELECT * FROM `is_invoice_payment` WHERE `project_id` = '".$project_info['project_id']."' AND `invoice_id` = '0' ")->rows;
			$total_received = 0;
			foreach($payment_datas as $pkey => $pvalue){
				$total_received += $pvalue['amount_received'];
			}
			$data['material_advance'] = number_format($total_received, 2, '.', '');
			// if($project_info['advance_type'] == 'material'){
			// 	$data['material_advance'] = $project_info['advance'];
			// } else {
			// 	$data['material_advance'] = 0;
			// }
		} else {
			$data['material_advance'] = 0;
		}

		if (isset($this->request->post['total_material_total'])) {
			$data['total_material_total'] = $this->request->post['total_material_total'];
		} elseif (!empty($invoice_info)) {
			if($invoice_info['total_material_total'] == ''){
				$data['total_material_total'] = 0;
			} else {
				$data['total_material_total'] = $invoice_info['total_material_total'];
			}
		} else {
			$data['total_material_total'] = 0;
		}

		if (isset($this->request->post['labour_datas'])) {
			$data['labour_datas'] = $this->request->post['labour_datas'];
		} elseif (!empty($invoice_info_labour)) {
			$labour_datas = array();
			for($i=1; $i<=10;$i++){
				$labour_datass = $this->db->query("SELECT * FROM `is_invoice_labour` WHERE `project_id` = '".$data['project_id']."' AND `invoice_id` = '".$data['inv_id_labour']."' AND `inc_id` = '".$i."' ");
				// echo '<pre>';
				// print_r($labour_datass);
				// exit;
				if($labour_datass->num_rows > 0){
					$labour_data = $labour_datass->row;
					$labour_datas[] = array(
						'labour_description' => $labour_data['labour_description'],
						'labour_quantity' => $labour_data['labour_quantity'],
						'labour_rate' => $labour_data['labour_rate'],
						'labour_amount' => $labour_data['labour_amount'],
						'inc_id' => $i,
					);
				} else {
					$labour_datas[] = array(
						'labour_description' => '',
						'labour_quantity' => '',
						'labour_rate' => '',
						'labour_amount' => '',
						'inc_id' => $i,
					);
				}
			}		
			$data['labour_datas'] = $labour_datas;
		} else {
			$labour_datas = array();
			for($i=1; $i<=10;$i++){
				$labour_datas[] = array(
					'labour_description' => '',
					'labour_quantity' => '',
					'labour_rate' => '',
					'labour_amount' => '',
					'inc_id' => $i,
				);
			}		
			$data['labour_datas'] = $labour_datas;
		}

		// if (isset($this->request->post['labour_description'])) {
		// 	$data['labour_description'] = $this->request->post['labour_description'];
		// } elseif (!empty($invoice_info_labour)) {
		// 	$data['labour_description'] = $invoice_info_labour['labour_description'];
		// } else {
		// 	$data['labour_description'] = '';
		// }

		// if (isset($this->request->post['labour_quantity'])) {
		// 	$data['labour_quantity'] = $this->request->post['labour_quantity'];
		// } elseif (!empty($invoice_info_labour)) {
		// 	if($invoice_info_labour['labour_quantity'] == ''){
		// 		$data['labour_quantity'] = 0;
		// 	} else {
		// 		$data['labour_quantity'] = $invoice_info_labour['labour_quantity'];
		// 	}
		// } else {
		// 	$data['labour_quantity'] = 0;
		// }


		// if (isset($this->request->post['labour_rate'])) {
		// 	$data['labour_rate'] = $this->request->post['labour_rate'];
		// } elseif (!empty($invoice_info_labour)) {
		// 	if($invoice_info_labour['labour_rate'] == ''){
		// 		$data['labour_rate'] = 0;
		// 	} else {
		// 		$data['labour_rate'] = $invoice_info_labour['labour_rate'];	
		// 	}
		// } else {
		// 	$data['labour_rate'] = 0;
		// }

		// if (isset($this->request->post['labour_amount'])) {
		// 	$data['labour_amount'] = $this->request->post['labour_amount'];
		// } elseif (!empty($invoice_info_labour)) {
		// 	if($invoice_info_labour['labour_amount'] == ''){
		// 		$data['labour_amount'] = 0;
		// 	} else { 
		// 		$data['labour_amount'] = $invoice_info_labour['labour_amount'];
		// 	}
		// } else {
		// 	$data['labour_amount'] = 0;
		// }

		if (isset($this->request->post['labour_service_tax'])) {
			$data['labour_service_tax'] = $this->request->post['labour_service_tax'];
		} elseif (!empty($invoice_info_labour)) {
			$data['labour_service_tax'] = $invoice_info_labour['labour_service_tax'];
		} else {
			$data['labour_service_tax'] = '';
		}

		if (isset($this->request->post['labour_service_tax_rate'])) {
			$data['labour_service_tax_rate'] = $this->request->post['labour_service_tax_rate'];
		} elseif (!empty($invoice_info_labour)) {
			if($invoice_info_labour['labour_service_tax_rate'] == ''){
				$data['labour_service_tax_rate'] = 0;
			} else {
				$data['labour_service_tax_rate'] = $invoice_info_labour['labour_service_tax_rate'];
			}
		} else {
			$data['labour_service_tax_rate'] = 0;
		}

		if (isset($this->request->post['labour_sbc'])) {
			$data['labour_sbc'] = $this->request->post['labour_sbc'];
		} elseif (!empty($invoice_info_labour)) {
			$data['labour_sbc'] = $invoice_info_labour['labour_sbc'];
		} else {
			$data['labour_sbc'] = '';
		}

		if (isset($this->request->post['labour_sbc_rate'])) {
			$data['labour_sbc_rate'] = $this->request->post['labour_sbc_rate'];
		} elseif (!empty($invoice_info_labour)) {
			if($invoice_info_labour['labour_sbc_rate'] == ''){
				$data['labour_sbc_rate'] = 0;
			} else {
				$data['labour_sbc_rate'] = $invoice_info_labour['labour_sbc_rate'];
			}
		} else {
			$data['labour_sbc_rate'] = 0;
		}

		if (isset($this->request->post['labour_kkc'])) {
			$data['labour_kkc'] = $this->request->post['labour_kkc'];
		} elseif (!empty($invoice_info_labour)) {
			$data['labour_kkc'] = $invoice_info_labour['labour_kkc'];
		} else {
			$data['labour_kkc'] = '';
		}

		if (isset($this->request->post['labour_kkc_rate'])) {
			$data['labour_kkc_rate'] = $this->request->post['labour_kkc_rate'];
		} elseif (!empty($invoice_info_labour)) {
			if($invoice_info_labour['labour_kkc_rate'] == ''){
				$data['labour_kkc_rate'] = 0;
			} else {
				$data['labour_kkc_rate'] = $invoice_info_labour['labour_kkc_rate'];
			}
		} else {
			$data['labour_kkc_rate'] = 0;
		}

		if (isset($this->request->post['labour_octroi'])) {
			$data['labour_octroi'] = $this->request->post['labour_octroi'];
		} elseif (!empty($invoice_info_labour)) {
			$data['labour_octroi'] = $invoice_info_labour['labour_octroi'];
		} else {
			$data['labour_octroi'] = '';
		}

		if (isset($this->request->post['labour_octroi_rate'])) {
			$data['labour_octroi_rate'] = $this->request->post['labour_octroi_rate'];
		} elseif (!empty($invoice_info_labour)) {
			if($invoice_info_labour['labour_octroi_rate'] == ''){
				$data['labour_octroi_rate'] = 0;
			} else {
				$data['labour_octroi_rate'] = $invoice_info_labour['labour_octroi_rate'];
			}
		} else {
			$data['labour_octroi_rate'] = '';
		}

		if (isset($this->request->post['labour_advance'])) {
			$data['labour_advance'] = $this->request->post['labour_advance'];
		} elseif (!empty($invoice_info_labour)) {
			if($invoice_info_labour['labour_advance'] == ''){
				$data['labour_advance'] = 0;
			} else {
				$data['labour_advance'] = $invoice_info_labour['labour_advance'];
			}
		} elseif (!empty($project_info)) {
			if($project_info['advance_type'] == 'labour'){
				$data['labour_advance'] = $project_info['advance'];
			} else {
				$data['labour_advance'] = 0;
			}
		} else {
			$data['labour_advance'] = 0;
		}

		if (isset($this->request->post['total_labour_total'])) {
			$data['total_labour_total'] = $this->request->post['total_labour_total'];
		} elseif (!empty($invoice_info_labour)) {
			if($invoice_info_labour['total_labour_total'] == ''){
				$data['total_labour_total'] = 0;
			} else {
				$data['total_labour_total'] = $invoice_info_labour['total_labour_total'];
			}
		} else {
			$data['total_labour_total'] = 0;
		}

		$project_task = array();
		$project_tasks = $this->db->query("SELECT * FROM `oc_project_task` WHERE `project_id` = '".$data['project_id']."' AND `task_id` = '0' ")->rows;
		foreach($project_tasks as $pkeys => $pvalues){
			$project_task[] = array(
				'pt_id' => $pvalues['pt_id'],
				'name' => $pvalues['name'],
				'start_date' => date('d-m-Y', strtotime($pvalues['start_date'])),
				'end_date' => date('d-m-Y', strtotime($pvalues['end_date'])),
				'daysrequired' => $pvalues['daysrequired'],
			);
		}
		$data['project_task'] = $project_task;

		if (isset($this->error['filename'])) {
			$data['error_filename'] = $this->error['filename'];
		} else {
			$data['error_filename'] = '';
		}

		$file_show_path = HTTP_CATALOG.'system/storage/download/project_upload_document/';
		if (isset($this->request->post['filename'])) {
			$data['filename'] = $this->request->post['filename'];
			if($data['filename'] != ''){
				$data['link_href'] = $file_show_path.$this->request->post['filename'];
			} else {
				$data['link_href'] = '';
			}
		} elseif (!empty($invoice_info)) {
			$data['filename'] = $invoice_info['filename'];
			if($data['filename'] != ''){
				$data['link_href'] = $file_show_path.$invoice_info['filename']; 
			} else {
				$data['link_href'] = '';
			}
		} elseif (!empty($invoice_info_labour)) {
			$data['filename'] = $invoice_info_labour['filename'];
			if($data['filename'] != ''){
				$data['link_href'] = $file_show_path.$invoice_info_labour['filename']; 
			} else {
				$data['link_href'] = '';
			}
		} else {
			$data['filename'] = '';
			$data['link_href'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/invoice_form', $data));
	}


	public function getCompanyState() {
		$json = array();
		if (isset($this->request->get['company'])) {
			$company = $this->request->get['company'];
			$result = $this->db->query("SELECT `registered_address_state` FROM `is_company` WHERE `company_id` = '".$company."' ")->rows;
			//echo("SELECT `bank_name` FROM `is_company_bank` WHERE `company_id` = '".$company."' ");
			//exit();
			//echo'<pre>';
			//print_r($result);
			//exit();
			$json = array(
				'state' => $result['registered_address_state']
			);
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
	}

	public function getCompanyAddress() {
		$json = array();
		if (isset($this->request->get['company'])) {
			$company = $this->request->get['company'];
			$company_branch_datas = $this->db->query("SELECT `id`, `branch_label` FROM `is_company_branch` WHERE `company_id` = '".$company."' ")->rows;
			$company_branch_data = array();
			$json[0] = array(
				'id' => '0',
				'branch_label' => 'Please Select',
				//'state' => '',
			);
			$cnt = 1;
			foreach($company_branch_datas as $ckeys => $cvalues){
				$json[$cnt] = array(
					'id' => $cvalues['id'],
					'branch_label' => $cvalues['branch_label'],
					//'state' => $cvalues['branch_address_state'],
				);
				$cnt ++;
			}
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
	}

	public function getAddress() {
		$json = array();
		if (isset($this->request->get['address_id'])) {
			$address_id = $this->request->get['address_id'];
			$company_branch_datas = $this->db->query("SELECT `branch_address`, `branch_address_state` FROM `is_company_branch` WHERE `id` = '".$address_id."' ")->row;
			if(isset($company_branch_datas['branch_address'])){
				$json['address'] = $company_branch_datas['branch_address'];
				$json['state'] = $company_branch_datas['branch_address_state'];
			} else {
				$json['address'] = '';
				$json['state'] = '';
			}
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
	}

	public function getclientAddress() {
		$json = array();
		if (isset($this->request->get['address_id'])) {
			$address_id = $this->request->get['address_id'];
			$client_address_datas = $this->db->query("SELECT `address`, `state`, `vat_no`, `gst_no`,`pincode` FROM `is_client_vat` WHERE `id` = '".$address_id."' ")->row;
			if(isset($client_address_datas['address'])){
				$json['address'] = $client_address_datas['address'].' - ' . $client_address_datas['pincode'];
				$json['state'] = $client_address_datas['state'];
				$json['vat_no'] = $client_address_datas['vat_no'];
				$json['gst_no'] = $client_address_datas['gst_no'];
			} else {
				$json['address'] = '';
				$json['state'] = '';
				$json['vat_no'] = '';
				$json['gst_no'] = '';
			}
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
	}

	protected function validateForm() {
		//echo 'in';
		if (!$this->user->hasPermission('modify', 'catalog/invoice')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		// echo "<pre>";
		// print_r($this->request->post);
		// exit;
		
		// $invoice = $this->request->post['inv_number'];
		// $invoicess = explode('/', $invoice);

		// if ($this->request->post['inv_number']){
		//  	if($this->request->post['company_id'] == '1' || $this->request->post['company_id'] == '2'){
		//  		$state=array(
		// 			'Maharashtra'=>'Maharashtra',
		// 			'Karnataka'=> 'Karnataka',
		// 		);
		// 		$st = $this->request->post['site_address_state'];
		// 		if(isset($state[$st])){
		// 			$site_address_state_comp = $state[$st];
		// 		} else {
		// 			$site_address_state_comp = 'Maharashtra';
		// 		}
		//  		$invoice = $this->db->query("SELECT * FROM `is_invoice` WHERE `inv_id` = '".$invoicess[3]."' AND `project_id` != '".$this->request->post['project_id']."' AND `company_id` = '".$this->request->post['company_id']."' AND `actual_site_address_state` = '".$site_address_state_comp."' ");
		// 	} else {
		// 		$invoice = $this->db->query("SELECT * FROM `is_invoice` WHERE `inv_id` = '".$invoicess[3]."' AND `project_id` != '".$this->request->post['project_id']."' AND `company_id` = '".$this->request->post['company_id']."' ");	
		// 	}
		// 	if($invoice->num_rows > 0){
		// 		$this->error['inv_number'] = 'Invoice Number Already Exist';
		// 	}	
		// }
		// echo '<pre>';
		// print_r($this->error);
		// exit;
 		// 	$invoice = $this->db->query("SELECT * FROM `is_invoice` WHERE `inv_id` = '".$invoicess[3]."' AND `project_id` = '".$this->request->post['project_id']."'")->row;
		// //echo "<pre>";print_r($invoice['inv_id']);exit;
		// //echo "<pre>";print_r($invoice['inv_id']);exit;
		// if (utf8_strlen($invoicess[3] == $invoice['inv_id'])){
		// 	$this->error['inv_number'] = $this->language->get('error_name');
		// }		

		// if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 64)) {
		// 	$this->error['name'] = $this->language->get('error_name');
		// }

		// if ((utf8_strlen($this->request->post['invoice_code']) < 1) || (utf8_strlen($this->request->post['invoice_code']) > 64)) {
		// 	$this->error['invoice_code'] = 'Please Enter Invoice Code!';
		// }

		// if($this->request->post['category_id'] == '0' || $this->request->post['category_id'] == ''){
		// 	$this->error['category'] = 'Please Select Valid Category!';
		// }

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/invoice')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// $this->load->model('catalog/product');

		// foreach ($this->request->post['selected'] as $invoice_id) {
		// 	$product_total = $this->model_catalog_product->getTotalProductsByInvoiceId($invoice_id);

		// 	if ($product_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_product'), $product_total);
		// 	}
		// }

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		return !$this->error;
	}

	public function prints_material_labour() {
		$this->load->language('catalog/invoice');
		$this->load->language('sale/order');

		$data['title'] = 'Sales Invoice';

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		$data['text_invoice'] = 'Sales Invoice';//$this->language->get('text_invoice');
		$data['text_order_detail'] = $this->language->get('text_order_detail');
		$data['text_order_id'] = $this->language->get('text_order_id');
		$data['text_invoice_no'] = 'Purchase Order Number';//$this->language->get('text_invoice_no');
		$data['text_invoice_date'] = $this->language->get('text_invoice_date');
		$data['text_date_added'] = $this->language->get('text_date_added');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_fax'] = $this->language->get('text_fax');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_website'] = $this->language->get('text_website');
		$data['text_payment_address'] = $this->language->get('text_payment_address');
		$data['text_shipping_address'] = $this->language->get('text_shipping_address');
		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comment'] = $this->language->get('text_comment');

		$data['column_product'] = $this->language->get('column_product');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total'] = $this->language->get('column_total');

		$this->load->model('catalog/invoice');
		$data['orders'] = array();
		$orders = array();

		if (isset($this->request->get['project_id'])) {
			$project_id = $this->request->get['project_id'];
		}
		if (isset($this->request->get['type'])) {
			$type = '';//$this->request->get['type'];
		} else {
			$type = '';
		}

		$invoice_infos = $this->model_catalog_invoice->getInvoice($project_id, 'material');
		$invoice_infos_labour = $this->model_catalog_invoice->getInvoice($project_id, 'labour');
		$project_infos = $this->model_catalog_invoice->getProject($project_id);

		$total = 0;
		setlocale(LC_MONETARY, 'en_IN');
		$material_data = array();
		$material_inner_data = array();
		$total_material_amount = 0.00;
		$total_taxable_value = 0.00;
		$total_sgst_value = 0.00;
		$total_cgst_value = 0.00;
		$total_igst_value = 0.00;
		$total_tax_value = 0.00;
		$inter_status = 0;
		$gst_reverge_charge = 0.00;
		$j = 1;
		if($invoice_infos['total_material_total'] != '0.00'){
			$inter_status = 0;
			if($invoice_infos['company_branch_state'] == $invoice_infos['site_address_state']){
				$inter_status = 1;
			}
			$material_datass = $this->db->query("SELECT * FROM `is_invoice_material` WHERE `project_id` = '".$project_id."' ");
			if($material_datass->num_rows > 0){
				$material_datas = $material_datass->rows;
				foreach($material_datas as $mkey => $mvalue){
					$material_taxable_value = $mvalue['material_rate'] * $mvalue['material_quantity'];
					
					$material_sgst_value = 0.00;
					$material_cgst_value = 0.00;
					$material_igst_value = 0.00;
					if($inter_status == 1){
						$tax_rate_divide = $mvalue['material_tax_rate'] / 2;
						
						$material_sgst_rate = $tax_rate_divide;
						$material_sgst_value = (($material_taxable_value * $material_sgst_rate) / 100);
						$material_sgst_value = number_format($material_sgst_value, 2, '.', '');

						$material_cgst_rate = $tax_rate_divide;
						$material_cgst_value = (($material_taxable_value * $material_cgst_rate) / 100);
						$material_cgst_value = number_format($material_cgst_value, 2, '.', '');
						
						$material_inner_data[] = array(	
							'inc_id' => $j,
							'material_description' => $mvalue['material_description'],
							'material_hsn_code' => $mvalue['material_hsn_code'],
							'material_rate' => $mvalue['material_rate'],
							'material_quantity'	       => $mvalue['material_quantity'],
							'material_taxable_value' => $material_taxable_value,
							'material_cgst_rate' => $material_cgst_rate . '%',
							'material_cgst_value' => $material_cgst_value,
							'material_sgst_rate' => $material_sgst_rate . '%',
							'material_sgst_value' => $material_sgst_value,
							'material_amount'    => $mvalue['material_amount'],	
						);
					} else {
						$material_igst_rate = $mvalue['material_tax_rate'];
						$material_igst_value = (($material_taxable_value * $material_igst_rate) / 100);
						$material_igst_value = number_format($material_igst_value, 2, '.', '');
						
						$material_inner_data[] = array(	
							'inc_id' => $j,
							'material_description' => $mvalue['material_description'],
							'material_hsn_code' => $mvalue['material_hsn_code'],
							'material_rate' => $mvalue['material_rate'],
							'material_quantity'	       => $mvalue['material_quantity'],
							'material_taxable_value' => $material_taxable_value,
							'material_igst_rate' => $material_igst_rate . '%',
							'material_igst_value' => $material_igst_value,
							'material_amount'    => $mvalue['material_amount'],	
						);
					}
					$j ++;
					$total_taxable_value = $total_taxable_value + $material_taxable_value;
					$total_sgst_value = $total_sgst_value + $material_sgst_value;
					$total_cgst_value = $total_cgst_value + $material_cgst_value;
					$total_igst_value = $total_igst_value + $material_igst_value;
					if($inter_status == 1){
						$total_tax_value = $total_tax_value + $material_cgst_value + $material_sgst_value;
					} else {
						$total_tax_value = $total_tax_value + $total_igst_value;
					}
					$total_material_amount = $total_material_amount + $mvalue['material_amount'];
				}
			}
		}

		if($invoice_infos_labour['total_labour_total'] != '0.00'){
			for($i=1;$i<=8;$i++){
				$labour_datass = $this->db->query("SELECT * FROM `is_invoice_labour` WHERE `project_id` = '".$project_id."' AND `inc_id` = '".$i."' ");
				if($labour_datass->num_rows > 0){
					$labour_datas = $labour_datass->row;
					$material_inner_data[] = array(	
						'inc_id' => $j,
						'material_description' => $labour_datas['labour_description'],
						'material_quantity'	       => $labour_datas['labour_quantity'],
						'material_rate' => $labour_datas['labour_rate'],
						'material_amount'    => $labour_datas['labour_amount'],	
					);
					$j ++;
					$total_material_amount = $total_material_amount + $labour_datas['labour_amount'];
				} else {
					$material_inner_data[] = array(	
						'inc_id' => '',
						'material_description' => '',
						'material_quantity'	       => '',
						'material_rate' => '',
						'material_amount'    => '',	
					);
				}
			}
			$mat_cnt = count($material_inner_data);
			if($mat_cnt > 10){
				foreach($material_inner_data as $mkey => $mvalue){
					if($mkey > 10){
						unset($material_inner_data[$mkey]);
					}
				}
				//$material_inner_data = array_filter(array_map('array_filter', $material_inner_data));
			}
		}

		$material_data[] = array(
			'total_taxable_value' => number_format($total_taxable_value, 2, '.', ''),
			'total_cgst_value' => number_format($total_cgst_value, 2, '.', ''),
			'total_sgst_value' => number_format($total_sgst_value, 2, '.', ''),
			'total_igst_value' => number_format($total_igst_value, 2, '.', ''),
			'total_tax_value' => number_format($total_tax_value, 2, '.', ''),
			'total_material_amount' => number_format($total_material_amount, 2, '.', ''),
			
			'material_inner_data' => $material_inner_data,
			'material_vat_rate'    => $invoice_infos['material_vat_rate'],
			'material_vat'    => $invoice_infos['material_vat'],
			'material_cst_rate'    => $invoice_infos['material_cst_rate'],
			'material_cst'    => $invoice_infos['material_cst'],
			'material_octroi_rate'    => number_format($invoice_infos['material_octroi_rate'], 2, '.', ''),
			'material_octroi'    => number_format($invoice_infos['material_octroi'] + $invoice_infos_labour['labour_octroi'], 2, '.', ''),
			
			'labour_service_tax_rate'    => $invoice_infos_labour['labour_service_tax_rate'],
			'labour_service_tax'    => $invoice_infos_labour['labour_service_tax'],
			'labour_sbc_rate'    => $invoice_infos_labour['labour_sbc_rate'],
			'labour_sbc'    => $invoice_infos_labour['labour_sbc'],
			'labour_kkc_rate'    => $invoice_infos_labour['labour_kkc_rate'],
			'labour_kkc'    => $invoice_infos_labour['labour_kkc'],

			'material_advance'    => number_format($invoice_infos['material_advance'] + $invoice_infos_labour['labour_advance'], 2, '.', ''),
			//'material_net_total' => number_format($invoice_infos['material_amount'] + $invoice_infos['material_vat'] + $invoice_infos['material_cst'] + $invoice_infos['material_octroi'] + $invoice_infos_labour['labour_amount'] + $invoice_infos_labour['labour_service_tax'] + $invoice_infos_labour['labour_sbc'] + $invoice_infos_labour['labour_kkc'] + $invoice_infos_labour['labour_octroi'], 2, '.', ''),
			'material_net_total' => number_format($invoice_infos['material_amount'] + $total_tax_value, 2, '.', ''),
			'total_material_total'    => $invoice_infos['total_material_total'] + $invoice_infos_labour['total_labour_total'],// '4002325.00',//
			
			'inter_status' => $inter_status,
			'gst_reverge_charge' => number_format($gst_reverge_charge, 2, '.', ''),
		);
		
		$invoice_date = '';
		if($invoice_infos['invoice_date'] != '0000-00-00'){
			$invoice_date = date('F j, Y', strtotime($invoice_infos['invoice_date']));
		}

		$po_date = '';
		if($invoice_infos['po_date'] != '0000-00-00'){
			$po_date = date('j-M-Y', strtotime($invoice_infos['po_date']));
		}

		$ref_date = '';
		if($invoice_infos['ref_date'] != '0000-00-00'){
			$ref_date = date('j-M-Y', strtotime($invoice_infos['ref_date']));
		}

		if($invoice_infos['client_address_type'] == 'other'){
			$client_address_heading = $invoice_infos['client_address_heading'].' (Lessor' . ')';
			$client_address = $invoice_infos['client_address'];	
			$client_address_state = $invoice_infos['client_address_state'];	
			$client_address_tin = $invoice_infos['client_address_tin'];	
			$client_address_gst = $invoice_infos['client_address_gst'];	

			$site_address_heading = $invoice_infos['client'].' (Lessor' . ')';
			$site_address = $invoice_infos['site_address'];
			$site_address_state = $invoice_infos['site_address_state'];
		} else {
			$client_address_heading = $invoice_infos['client'];
			$client_address = $invoice_infos['client_address'];	
			$client_address_state = $invoice_infos['client_address_state'];	
			$client_address_gst= $invoice_infos['client_address_gst'];	

			$site_address_heading = $invoice_infos['customer'];
			$site_address = $invoice_infos['site_address'];
			$site_address_state = $invoice_infos['site_address_state'];
		}

		$payment_string = '';
		if($project_infos['payment_type'] != '' && $project_infos['advance_type'] == 'material'){
			if($project_infos['payment_type'] == 'Cash'){
				$payment_string = 'Advance Payment of ' . $project_infos['advance'] . ' by Cash';
			} else if($project_infos['payment_type'] == 'Cheque'){
				$payment_string = 'Advance Payment of ' . $project_infos['advance'] . ' by Cheque, Bank Name : ' . $project_infos['bank_name'] . ' | Cheque Number : ' . $project_infos['cheque_number'] . ' | Cheque Date : ' . $project_infos['cheque_date'];
			} else if($project_infos['payment_type'] == 'Bank Transfer'){
				$payment_string = 'Advance Payment of ' . $project_infos['advance'] . ' by Bank Transfer, Bank Name : ' . $project_infos['bank_name'] . ' | Account Number : ' . $project_infos['account_number'] . ' | Transfer Date : ' . $project_infos['transfer_date'];
			}
		}
		
		$data['orders'][] = array(
			'company_name'  => $invoice_infos['company_name'],
			'company_address'  => nl2br($invoice_infos['company_address']),
			'invoice_number'   => $invoice_infos['invoice_prefix'].'/'.$invoice_infos['inv_id'],
			'invoice_date'	   => $invoice_date,
			'branch_address'   => nl2br($invoice_infos['branch_address']),
			'branch_pan'	   => $invoice_infos['branch_pan'],
			'branch_service_tax' => $invoice_infos['branch_service_tax'],
			'branch_vat'	   => $invoice_infos['branch_vat'],
			'branch_cst'	   => $invoice_infos['branch_cst'],
			'branch_gst'	   => $invoice_infos['branch_gst'],
			'invoice_print_type' => $invoice_infos['invoice_print_type'],
			'client_address_heading' => $client_address_heading,
			'client_address' => $client_address,
			'client_address_state' => $client_address_state,
			'client_address_tin' => $client_address_tin,
			'client_address_gst' => $client_address_gst,
			'site_address_heading' => $site_address_heading,
			'site_address' => $site_address,
			'site_address_state' => $site_address_state,
			'po_number' => $invoice_infos['po_number'],
			'po_date' => $po_date,
			'ref_no' => $invoice_infos['ref_no'],
			'ref_date' => $ref_date,
			'site_id' => $invoice_infos['site_code'],
			'material_data'          => $material_data,
			'payment_string'          => $payment_string,
		);
		// echo '<pre>';
		// print_r($data['orders']);
		// exit;
		//$data['sign_image'] = 'C:/xampp/htdocs/imagesolution/admin/view/image/image_sign.png';
		$data['login_name'] = $this->user->getEmail();
		$data['header_image'] = DIR_APPLICATION.'view/image/image_logo.png';
		// echo '<pre>';
		// print_r($data['orders']);
		// exit;
		$html = $this->load->view('sale/sales_invoice', $data);
		//echo $html;exit;
		$client_name = str_replace(array(' ', ',', '&', '.'), '_', $invoice_infos['client']);
		//$filename = 'Purchase_Order_' . $manufacturer_name . '_' . $po_number. '_' . $t_date .'.pdf';
		if($invoice_infos['invoice_print_type'] == 'material_labour'){
			$filename = $client_name.'-Material-Labour-Invoice';
		} else {
			$filename = $client_name.'-Material-Invoice';
		}
		//echo $filename;exit;		
		//require_once '/home/aaron/public_html/balaji/system/library/dompdf/autoload.inc.php';
		//require_once("/Library/WebServer/Documents/balaji/system/library/dompdf/dompdf_config.inc.php");
	    define("DOMPDF_ENABLE_REMOTE", false);
	    $dompdf = new Dompdf();
	    $dompdf->loadHtml($html);
	    $dompdf->setPaper('A4', 'portrait');
	    $dompdf->render();
	    $dompdf->stream($filename);

	 	//$filename = 'Purchase_Order_' . $po_number.'.html';
		//header('Content-type: text/html');
		////header('Set-Cookie: fileLoading=true');
		//header('Content-Disposition: attachment; filename='.$filename);
		//echo $html;
		//exit;
		//$this->response->setOutput($this->load->view('sale/order_invoice', $data));
	}

	public function prints() {
		$this->load->language('catalog/invoice');
		$this->load->language('sale/order');

		$data['title'] = 'Sales Invoice';

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		$data['text_invoice'] = 'Sales Invoice';//$this->language->get('text_invoice');
		$data['text_order_detail'] = $this->language->get('text_order_detail');
		$data['text_order_id'] = $this->language->get('text_order_id');
		$data['text_invoice_no'] = 'Purchase Order Number';//$this->language->get('text_invoice_no');
		$data['text_invoice_date'] = $this->language->get('text_invoice_date');
		$data['text_date_added'] = $this->language->get('text_date_added');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_fax'] = $this->language->get('text_fax');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_website'] = $this->language->get('text_website');
		$data['text_payment_address'] = $this->language->get('text_payment_address');
		$data['text_shipping_address'] = $this->language->get('text_shipping_address');
		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comment'] = $this->language->get('text_comment');

		$data['column_product'] = $this->language->get('column_product');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total'] = $this->language->get('column_total');

		$this->load->model('catalog/invoice');
		$data['orders'] = array();
		$orders = array();

		if (isset($this->request->get['project_id'])) {
			$project_id = $this->request->get['project_id'];
		}
		if (isset($this->request->get['type'])) {
			$type = $this->request->get['type'];
		}

		$invoice_infos = $this->model_catalog_invoice->getInvoice($project_id, $type);
		$project_infos = $this->model_catalog_invoice->getProject($project_id);

		$productraw_datas = array();
		// $project_datas = $this->db->query("SELECT * FROM `oc_project` WHERE `project_id` = '".$project_id."' "); 
		// if($project_datas->num_rows > 0){
		// 	$cnt = 1;
		// 	$project_data = $project_datas->row;
		// 	$category_id = $project_data['category_id'];
		// 	$projectboqs = $this->db->query("SELECT * FROM `is_projectboq` WHERE `productboq_category_id` = '".$category_id."' AND `project_id` = '".$project_id."' "); 
		// 	if($projectboqs->num_rows > 0){
		// 		$projectboq_info = $projectboqs->row;						
		// 		$product_raw_datass = $this->db->query("SELECT * FROM `is_projectboqraw` WHERE `productboq_category_id` = '".$projectboq_info['productboq_category_id']."' AND `project_id` = '".$project_id."' ")->rows;
		// 		foreach($product_raw_datass as $pkeys => $pvalues){
		// 			$productraw_datas[] = array(
		// 				'cnt' => $cnt,
		// 				'productboqraw_name' => $pvalues['productboqraw_name'],
		// 				'quantity' => $pvalues['quantity'],
		// 			); 
		// 			$cnt ++;			
		// 		}
		// 	}
		// }		

		// echo '<pre>';
		// print_r($productraw_datas);
		// exit;

		$total = 0;
		setlocale(LC_MONETARY, 'en_IN');
		$material_data = array();
		$material_inner_data = array();
		$total_material_amount = 0.00;
		$total_taxable_value = 0.00;
		$total_sgst_value = 0.00;
		$total_cgst_value = 0.00;
		$total_igst_value = 0.00;
		$total_tax_value = 0.00;
		$inter_status = 0;
		$gst_reverge_charge = 0.00;
		if($invoice_infos['total_material_total'] != '0.00'){
			if($invoice_infos['company_branch_state'] == $invoice_infos['site_address_state']){
				$inter_status = 1;
			}
			for($i=1;$i<=5;$i++){
				$material_datass = $this->db->query("SELECT * FROM `is_invoice_material` WHERE `project_id` = '".$project_id."' AND `inc_id` = '".$i."' ");
				if($material_datass->num_rows > 0){
					$mvalue = $material_datass->row;
					$material_taxable_value = round($mvalue['material_rate'] * $mvalue['material_quantity']);
					
					$material_sgst_value = 0.00;
					$material_cgst_value = 0.00;
					$material_igst_value = 0.00;
					if($inter_status == 1){
						$tax_rate_divide = $mvalue['material_tax_rate'] / 2;
						
						$material_sgst_rate = $tax_rate_divide;
						$material_sgst_value = (($material_taxable_value * $material_sgst_rate) / 100);
						//$material_sgst_value = number_format($material_sgst_value, 2, '.', '');
						$material_sgst_value = round($material_sgst_value);

						$material_cgst_rate = $tax_rate_divide;
						$material_cgst_value = (($material_taxable_value * $material_cgst_rate) / 100);
						//$material_cgst_value = number_format($material_cgst_value, 2, '.', '');
						$material_cgst_value = round($material_cgst_value);

						$material_inner_data[] = array(	
							'inc_id' => $mvalue['inc_id'],
							'material_description' => $mvalue['material_description'],
							'material_hsn_code' => $mvalue['material_hsn_code'],
							'material_rate' => $mvalue['material_rate'],
							'material_quantity'	       => $mvalue['material_quantity'],
							'material_taxable_value' => $material_taxable_value,
							'material_cgst_rate' => $material_cgst_rate . '%',
							'material_cgst_value' => $material_cgst_value,
							'material_sgst_rate' => $material_sgst_rate . '%',
							'material_sgst_value' => $material_sgst_value,
							'material_amount'    => round($mvalue['material_amount']),	
						);
					} else {
						$material_igst_rate = $mvalue['material_tax_rate'];
						$material_igst_value = (($material_taxable_value * $material_igst_rate) / 100);
						//$material_igst_value = number_format($material_igst_value, 2, '.', '');
						$material_igst_value = round($material_igst_value);

						$material_inner_data[] = array(	
							'inc_id' => $mvalue['inc_id'],
							'material_description' => $mvalue['material_description'],
							'material_hsn_code' => $mvalue['material_hsn_code'],
							'material_rate' => $mvalue['material_rate'],
							'material_quantity'	       => $mvalue['material_quantity'],
							'material_taxable_value' => $material_taxable_value,
							'material_igst_rate' => $material_igst_rate . '%',
							'material_igst_value' => $material_igst_value,
							'material_amount'    => round($mvalue['material_amount']),	
						);
					}
					$total_taxable_value = round($total_taxable_value + $material_taxable_value);
					$total_sgst_value = round($total_sgst_value + $material_sgst_value);
					$total_cgst_value = round($total_cgst_value + $material_cgst_value);
					$total_igst_value = round($total_igst_value + $material_igst_value);
					if($inter_status == 1){
						$total_tax_value = round($total_tax_value + $material_cgst_value + $material_sgst_value);
					} else {
						$total_tax_value = round($total_tax_value + $total_igst_value);
					}
					$total_material_amount = round($total_material_amount + $mvalue['material_amount']);
				} else {
					if($inter_status == 1){
						$material_inner_data[] = array(	
							'inc_id' => '&nbsp;',
							'material_description' => '&nbsp;',
							'material_hsn_code' => '&nbsp;',
							'material_quantity'	       => '&nbsp;',
							'material_rate' => '&nbsp;',
							'material_taxable_value' => '&nbsp;',
							'material_sgst_rate' => '&nbsp;',
							'material_sgst_value' => '&nbsp;',
							'material_cgst_rate' => '&nbsp;',
							'material_cgst_value' => '&nbsp;',
							'material_amount'    => '&nbsp;',	
						);
					} else {
						$material_inner_data[] = array(	
							'inc_id' => '&nbsp;',
							'material_description' => '&nbsp;',
							'material_hsn_code' => '&nbsp;',
							'material_quantity'	       => '&nbsp;',
							'material_rate' => '&nbsp;',
							'material_taxable_value' => '&nbsp;',
							'material_igst_rate' => '&nbsp;',
							'material_igst_value' => '&nbsp;',
							'material_amount'    => '&nbsp;',	
						);
					}
				}
			}
			
			$material_data[] = array(
				'total_taxable_value' => round($total_taxable_value),
				'total_cgst_value' => round($total_cgst_value),
				'total_sgst_value' => round($total_sgst_value),
				'total_igst_value' => round($total_igst_value),
				'total_tax_value' => round($total_tax_value),
				'total_material_amount' => round($total_material_amount),

				'material_inner_data' => $material_inner_data,
				'material_vat_rate'    => $invoice_infos['material_vat_rate'],
				'material_vat'    => $invoice_infos['material_vat'],
				'material_cst_rate'    => $invoice_infos['material_cst_rate'],
				'material_cst'    => $invoice_infos['material_cst'],
				'material_octroi_rate'    => $invoice_infos['material_octroi_rate'],
				'material_octroi'    => $invoice_infos['material_octroi'],
				'material_advance'    => round($invoice_infos['material_advance']),
				//'material_net_total' => number_format($invoice_infos['material_amount'] + $invoice_infos['material_vat'] + $invoice_infos['material_cst'] + $invoice_infos['material_octroi']),
				'material_net_total' => round($invoice_infos['material_amount'] + $total_tax_value),
				'total_material_total'    => round($invoice_infos['total_material_total']),// '4002325.00',//
				'gst_reverge_charge' => round($gst_reverge_charge),
				'inter_status' => $inter_status,
			);
		}
		
		$invoice_date = '';
		if($invoice_infos['invoice_date'] != '0000-00-00'){
			$invoice_date = date('F j, Y', strtotime($invoice_infos['invoice_date']));
		}

		$po_date = '';
		if($invoice_infos['po_date'] != '0000-00-00'){
			$po_date = date('j-M-Y', strtotime($invoice_infos['po_date']));
		}

		$ref_date = '';
		if($invoice_infos['ref_date'] != '0000-00-00'){
			$ref_date = date('j-M-Y', strtotime($invoice_infos['ref_date']));
		}

		if($invoice_infos['client_address_type'] == 'other'){
			$client_address_heading = $invoice_infos['client_address_heading'].' (Lessor' . ')';
			$client_address = $invoice_infos['client_address'];	
			$client_address_state = $invoice_infos['client_address_state'];	
			$client_address_tin = $invoice_infos['client_address_tin'];	

			$site_address_heading = $invoice_infos['client'].' (Lessor' . ')';
			$site_address = $invoice_infos['site_address'];
			$site_address_state = $invoice_infos['site_address_state'];
		} else {
			$client_address_heading = $invoice_infos['client'];
			$client_address = $invoice_infos['client_address'];	
			$client_address_state = $invoice_infos['client_address_state'];	
			$client_address_tin = $invoice_infos['client_address_tin'];	

			$site_address_heading = $invoice_infos['customer'];
			$site_address = $invoice_infos['site_address'];
			$site_address_state = $invoice_infos['site_address_state'];
		}

		$payment_string = '';
		if($project_infos['payment_type'] != '' && $project_infos['advance_type'] == 'material'){
			if($project_infos['payment_type'] == 'Cash'){
				$payment_string = 'Advance Payment of ' . $project_infos['advance'] . ' by Cash';
			} else if($project_infos['payment_type'] == 'Cheque'){
				$payment_string = 'Advance Payment of ' . $project_infos['advance'] . ' by Cheque, Bank Name : ' . $project_infos['bank_name'] . ' | Cheque Number : ' . $project_infos['cheque_number'] . ' | Cheque Date : ' . $project_infos['cheque_date'];
			} else if($project_infos['payment_type'] == 'Bank Transfer'){
				$payment_string = 'Advance Payment of ' . $project_infos['advance'] . ' by Bank Transfer, Bank Name : ' . $project_infos['bank_name'] . ' | Account Number : ' . $project_infos['account_number'] . ' | Transfer Date : ' . $project_infos['transfer_date'];
			}
		}
		/*
		echo '<pre>';
		print_r($invoice_infos);
		exit;*/
		$states= array(
			'' => '',
			'Andaman & Nicobar' => 'AN',
			'Andhra Pradesh' => 'AP',
			'Arunachal Pradesh' => 'AR',
			'Assam' => 'AS',
			'Bihar' => 'BH',
			'Chandigarh' => 'CH',
			'Chhattisgarh' => 'CT',
			'Dadar & Nagar Haveli' => 'DN',
			'Daman & Diu' => 'DD',
			'Delhi' => 'DL',
			'Goa' => 'GA',
			'Gujarat' => 'GJ',
			'Haryana' => 'HR',
			'Himachal Pradesh' => 'HP',
			'Jammu Kashmir' => 'JK',
			'Jharkhand' => 'JH',
			'Karnataka' => 'KA',
			'Kerala' => 'KL',
			'Lakshadeep' => 'LD',
			'Madhya Pradesh' => 'MP',
			'Maharashtra' => 'MH',
			'Manipur' => 'MN',
			'Meghalaya' => 'ME',
			'Mizoram' => 'MI',
			'Nagaland' => 'NL',
			'Orissa' => 'OR',
			'Pondicherry' => 'PY',
			'Punjab' => 'PB',
			'Rajasthan' => 'RJ',
			'Sikkim' => 'SK',
			'Tamil Nadu' => 'TN',
			'Telangana' => 'TS',
			'Tripura' => 'TR',
			'Uttaranchal' => 'UT',
			'Uttarakhand' => 'UK',
			'Uttar Pradesh' => 'UP',
			'West Bengal' => 'WB',
		);
		if(isset($states[$invoice_infos['site_address_state']])){
			$site_address_state_code = $states[$invoice_infos['site_address_state']];
		} else {
			$site_address_state_code = '';
		}

		if(isset($states[$invoice_infos['client_address_state']])){
			$client_address_state_code = $states[$invoice_infos['client_address_state']];
		} else {
			$client_address_state_code = '';
		}

		$atmid=$this->db->query("SELECT `atm_id` FROM oc_project WHERE project_id = '".$project_id."'")->row;
		//echo "SELECT `atm_id` FROM oc_project WHERE project_id = '".$project_id."'";exit;
		$data['orders'][] = array(
			'company_name'  => $invoice_infos['company_name'],
			'company_address'  => nl2br($invoice_infos['company_address']),
			'invoice_number'   => $invoice_infos['invoice_prefix'].'/'.$invoice_infos['inv_id'],
			'invoice_date'	   => $invoice_date,
			'branch_address'   => nl2br($invoice_infos['branch_address']),
			'branch_pan'	   => $invoice_infos['branch_pan'],
			'branch_service_tax' => $invoice_infos['branch_service_tax'],
			'branch_vat'	   => $invoice_infos['branch_vat'],
			'company_branch_state' => $invoice_infos['company_branch_state'],
			'state_code' => $states[$invoice_infos['company_branch_state']],
			'client_state_code' =>$client_address_state_code,
			'branch_cst'	   => $invoice_infos['branch_cst'],
			'branch_gst'	   => $invoice_infos['branch_gst'],
			'reverse_charges'	   => $invoice_infos['reverse_charges'],
			'gst_no' => $invoice_infos['gst_no'],
			'atmid'  => $atmid['atm_id'],
			'client_address_heading' => $client_address_heading,
			'client_address' => $client_address,
			'client_address_state' => $client_address_state,
			'client_address_tin' => $client_address_tin,
			'site_address_heading' => $site_address_heading,
			'site_address' => $site_address,
			'site_address_state' => $site_address_state,
			'site_address_state_code' => $site_address_state_code,
			'po_number' => $invoice_infos['po_number'],
			'po_date' => $po_date,
			'ref_no' => $invoice_infos['ref_no'],
			'ref_date' => $ref_date,
			'site_id' => $invoice_infos['site_code'],
			'material_data'          => $material_data,
			'payment_string'          => $payment_string,
			'productraw_datas' => $productraw_datas,
			'inter_status' => $inter_status,
			//'labour_data'          => $labour_data,
		);
		// echo '<pre>';
		// print_r($data['orders']);
		// exit;
		//$data['sign_image'] = 'C:/xampp/htdocs/imagesolution/admin/view/image/image_sign.png';
		$data['login_name'] = $this->user->getEmail();
		$data['header_image'] = DIR_APPLICATION.'view/image/image_logo.png';
		$data['igs_image'] = DIR_APPLICATION.'view/image/igs_image.jpg';
		$data['isb_image'] = DIR_APPLICATION.'view/image/isb_image.jpg';
		$data['user_name'] = $this->user->getFullname();
		// echo '<pre>';
		// print_r($invoice_infos['invoice_prefix'].'_'.$invoice_infos['inv_id']);
		// exit;
		$html = $this->load->view('sale/sales_invoice', $data);
		//echo $html;exit;
		$client_name = str_replace(array(' ', ',', '&', '.', '/'), '_', $invoice_infos['invoice_prefix'].'_'.$invoice_infos['inv_id']);
		//$filename = 'Purchase_Order_' . $manufacturer_name . '_' . $po_number. '_' . $t_date .'.pdf';
		$filename = $client_name;
		//echo $filename;exit;$orders['0']['invoice_number']		
		//require_once '/home/aaron/public_html/balaji/system/library/dompdf/autoload.inc.php';
		//require_once("/Library/WebServer/Documents/balaji/system/library/dompdf/dompdf_config.inc.php");
	    define("DOMPDF_ENABLE_REMOTE", false);
	    $dompdf = new Dompdf();
	    $dompdf->loadHtml($html);
	    $dompdf->setPaper('A4', 'portrait');
	    $dompdf->render();
	    $dompdf->stream($filename);

	 	//$filename = 'Purchase_Order_' . $po_number.'.html';
		//header('Content-type: text/html');
		////header('Set-Cookie: fileLoading=true');
		//header('Content-Disposition: attachment; filename='.$filename);
		//echo $html;
		//exit;
		//$this->response->setOutput($this->load->view('sale/order_invoice', $data));
	}

	public function prints_labour() {
		$this->load->language('catalog/invoice');
		$this->load->language('sale/order');

		$data['title'] = 'Sales Invoice';

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		$data['text_invoice'] = 'Sales Invoice';//$this->language->get('text_invoice');
		$data['text_order_detail'] = $this->language->get('text_order_detail');
		$data['text_order_id'] = $this->language->get('text_order_id');
		$data['text_invoice_no'] = 'Purchase Order Number';//$this->language->get('text_invoice_no');
		$data['text_invoice_date'] = $this->language->get('text_invoice_date');
		$data['text_date_added'] = $this->language->get('text_date_added');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_fax'] = $this->language->get('text_fax');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_website'] = $this->language->get('text_website');
		$data['text_payment_address'] = $this->language->get('text_payment_address');
		$data['text_shipping_address'] = $this->language->get('text_shipping_address');
		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comment'] = $this->language->get('text_comment');

		$data['column_product'] = $this->language->get('column_product');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total'] = $this->language->get('column_total');

		$this->load->model('catalog/invoice');
		$data['orders'] = array();
		$orders = array();

		if (isset($this->request->get['project_id'])) {
			$project_id = $this->request->get['project_id'];
		}

		if (isset($this->request->get['type'])) {
			$type = $this->request->get['type'];
		}

		$invoice_infos = $this->model_catalog_invoice->getInvoice($project_id, $type);
		$project_infos = $this->model_catalog_invoice->getProject($project_id);
		$total = 0;
		setlocale(LC_MONETARY, 'en_IN');
		// $material_data = array();
		// if($invoice_infos['material_description'] != ''){
		// 	$material_data[] = array(
		// 		'material_description' => $invoice_infos['material_description'],
		// 		'material_quantity'	       => $invoice_infos['material_quantity'],
		// 		'material_rate' => $invoice_infos['material_rate'],
		// 		'material_amount'    => $invoice_infos['material_amount'],
		// 		'material_vat_rate'    => $invoice_infos['material_vat_rate'],
		// 		'material_vat'    => $invoice_infos['material_vat'],
		// 		'material_cst_rate'    => $invoice_infos['material_cst_rate'],
		// 		'material_cst'    => $invoice_infos['material_cst'],
		// 		'material_octroi_rate'    => $invoice_infos['material_octroi_rate'],
		// 		'material_octroi'    => $invoice_infos['material_octroi'],
		// 		'material_advance'    => $invoice_infos['material_advance'],
		// 		'material_net_total' => number_format($invoice_infos['material_amount'] + $invoice_infos['material_vat'] + $invoice_infos['material_cst'] + $invoice_infos['material_octroi'], 2, '.', ''),
		// 		'total_material_total'    => $invoice_infos['total_material_total'],
		// 	);
		// }

		$productraw_datas = array();
		// $project_datas = $this->db->query("SELECT * FROM `oc_project` WHERE `project_id` = '".$project_id."' "); 
		// if($project_datas->num_rows > 0){
		// 	$cnt = 1;
		// 	$project_data = $project_datas->row;
		// 	$category_id = $project_data['category_id'];
		// 	$projectboqs = $this->db->query("SELECT * FROM `is_projectboq` WHERE `productboq_category_id` = '".$category_id."' AND `project_id` = '".$project_id."' "); 
		// 	if($projectboqs->num_rows > 0){
		// 		$projectboq_info = $projectboqs->row;						
		// 		$product_raw_datass = $this->db->query("SELECT * FROM `is_projectboqraw` WHERE `productboq_category_id` = '".$projectboq_info['productboq_category_id']."' AND `project_id` = '".$project_id."' ")->rows;
		// 		foreach($product_raw_datass as $pkeys => $pvalues){
		// 			$productraw_datas[] = array(
		// 				'cnt' => $cnt,
		// 				'productboqraw_name' => $pvalues['productboqraw_name'],
		// 				'quantity' => $pvalues['quantity'],
		// 			); 
		// 			$cnt ++;			
		// 		}
		// 	}
		// }

		$labour_data = array();
		$labour_inner_data = array();
		if($invoice_infos['total_labour_total'] != '0.00'){
			$total_labour_amount = 0;
			for($i=1;$i<=10;$i++){
				$labour_datass = $this->db->query("SELECT * FROM `is_invoice_labour` WHERE `project_id` = '".$project_id."' AND `inc_id` = '".$i."' ");
				if($labour_datass->num_rows > 0){
					$labour_datas = $labour_datass->row;
					$labour_inner_data[] = array(	
						'inc_id' => $labour_datas['inc_id'],
						'labour_description' => $labour_datas['labour_description'],
						'labour_quantity'	       => $labour_datas['labour_quantity'],
						'labour_rate' => $labour_datas['labour_rate'],
						'labour_amount'    => $labour_datas['labour_amount'],	
					);
					$total_labour_amount = $total_labour_amount + $labour_datas['labour_amount'];
				} else {
					$labour_inner_data[] = array(	
						'inc_id' => '&nbsp;',
						'labour_description' => '&nbsp;',
						'labour_quantity'	       => '&nbsp;',
						'labour_rate' => '&nbsp;',
						'labour_amount'    => '&nbsp;',	
					);
				}
			}

			// echo '<pre>';
			// print_r($labour_inner_data);
			// exit;

			$labour_data[] = array(
				// 'labour_description' => $invoice_infos['labour_description'],
				// 'labour_quantity'	       => $invoice_infos['labour_quantity'],
				// 'labour_rate' => $invoice_infos['labour_rate'],
				// 'labour_amount'    => $invoice_infos['labour_amount'],
				'total_labour_amount' => number_format($total_labour_amount, 2, '.', ''),
				'labour_inner_data' => $labour_inner_data,
				'labour_service_tax_rate'    => $invoice_infos['labour_service_tax_rate'],
				'labour_service_tax'    => $invoice_infos['labour_service_tax'],
				'labour_sbc_rate'    => $invoice_infos['labour_sbc_rate'],
				'labour_sbc'    => $invoice_infos['labour_sbc'],
				'labour_kkc_rate'    => $invoice_infos['labour_kkc_rate'],
				'labour_kkc'    => $invoice_infos['labour_kkc'],
				'labour_octroi_rate' => $invoice_infos['labour_octroi_rate'],
				'labour_octroi' => $invoice_infos['labour_octroi'],
				'labour_advance'    => $invoice_infos['labour_advance'],
				'labour_net_total' => number_format($invoice_infos['labour_amount'] + $invoice_infos['labour_service_tax'] + $invoice_infos['labour_sbc'] + $invoice_infos['labour_kkc'] + $invoice_infos['labour_octroi'], 2, '.', ''),
				'total_labour_total'    => $invoice_infos['total_labour_total'],
			);
		}
		
		//$total = number_format($total, 2, '.', '');
		
		$invoice_date = '';
		if($invoice_infos['invoice_date'] != '0000-00-00'){
			$invoice_date = date('F j, Y', strtotime($invoice_infos['invoice_date']));
		}

		$po_date = '';
		if($invoice_infos['po_date'] != '0000-00-00'){
			$po_date = date('j-M-Y', strtotime($invoice_infos['po_date']));
		}

		$ref_date = '';
		if($invoice_infos['ref_date'] != '0000-00-00'){
			$ref_date = date('j-M-Y', strtotime($invoice_infos['ref_date']));
		}

		if($invoice_infos['client_address_type'] == 'other'){
			$client_address_heading = $invoice_infos['client_address_heading'].' (Lessor' . ')';
			$client_address = $invoice_infos['client_address'];	
			$client_address_state = $invoice_infos['client_address_state'];	
			$client_address_tin = $invoice_infos['client_address_tin'];	

			$site_address_heading = $invoice_infos['client'].' (Lessor' . ')';
			$site_address = $invoice_infos['site_address'];
			$site_address_state = $invoice_infos['site_address_state'];
		} else {
			$client_address_heading = $invoice_infos['client'];
			$client_address = $invoice_infos['client_address'];	
			$client_address_state = $invoice_infos['client_address_state'];	
			$client_address_tin = $invoice_infos['client_address_tin'];	

			$site_address_heading = $invoice_infos['customer'];
			$site_address = $invoice_infos['site_address'];
			$site_address_state = $invoice_infos['site_address_state'];
		}

		$payment_string = '';
		if($project_infos['payment_type'] != '' && $project_infos['advance_type'] == 'labour'){
			if($project_infos['payment_type'] == 'Cash'){
				$payment_string = 'Advance Payment of ' . $project_infos['advance'] . ' by Cash';
			} else if($project_infos['payment_type'] == 'Cheque'){
				$payment_string = 'Advance Payment of ' . $project_infos['advance'] . ' by Cheque, Bank Name : ' . $project_infos['bank_name'] . ' | Cheque Number : ' . $project_infos['cheque_number'] . ' | Cheque Date : ' . $project_infos['cheque_date'];
			} else if($project_infos['payment_type'] == 'Bank Transfer'){
				$payment_string = 'Advance Payment of ' . $project_infos['advance'] . ' by Bank Transfer, Bank Name : ' . $project_infos['bank_name'] . ' | Account Number : ' . $project_infos['account_number'] . ' | Transfer Date : ' . $project_infos['transfer_date'];
			}
		}
		
		$data['orders'][] = array(
			'company_name'  => $invoice_infos['company_name'],
			'company_address'  => nl2br($invoice_infos['company_address']),
			'invoice_number'   => $invoice_infos['invoice_prefix'].'/'.$invoice_infos['inv_id'],
			'invoice_date'	   => $invoice_date,
			'branch_address'   => nl2br($invoice_infos['branch_address']),
			'branch_pan'	   => $invoice_infos['branch_pan'],
			'branch_service_tax' => $invoice_infos['branch_service_tax'],
			'branch_vat'	   => $invoice_infos['branch_vat'],
			'branch_cst'	   => $invoice_infos['branch_cst'],
			'branch_gst'	   => $invoice_infos['branch_gst'],
			'client_address_heading' => $client_address_heading,
			'client_address' => $client_address,
			'client_address_state' => $client_address_state,
			'client_address_tin' => $client_address_tin,
			'site_address_heading' => $site_address_heading,
			'site_address' => $site_address,
			'site_address_state' => $site_address_state,
			'po_number' => $invoice_infos['po_number'],
			'po_date' => $po_date,
			'ref_no' => $invoice_infos['ref_no'],
			'ref_date' => $ref_date,
			'payment_string' => $payment_string,
			'site_id' => $invoice_infos['site_code'],
			'productraw_datas' => $productraw_datas,
			//'material_data'          => $material_data,
			'labour_data'          => $labour_data,
		);
		// echo '<pre>';
		// print_r($data['orders']);
		// exit;
		//$data['sign_image'] = 'C:/xampp/htdocs/imagesolution/admin/view/image/image_sign.png';
		$data['login_name'] = $this->user->getEmail();
		$data['header_image'] = DIR_APPLICATION.'view/image/image_logo.png';
		// echo '<pre>';
		// print_r($data['orders']);
		// exit;
		$html = $this->load->view('sale/sales_invoice_labour', $data);
		//echo $html;exit;
		$client_name = str_replace(array(' ', ',', '&', '.'), '_', $invoice_infos['client']);
		//$filename = 'Purchase_Order_' . $manufacturer_name . '_' . $po_number. '_' . $t_date .'.pdf';
		$filename = $client_name.'-Labour-Invoice';
		//echo $filename;exit;		
		//require_once '/home/aaron/public_html/balaji/system/library/dompdf/autoload.inc.php';
		//require_once("/Library/WebServer/Documents/balaji/system/library/dompdf/dompdf_config.inc.php");
	    define("DOMPDF_ENABLE_REMOTE", false);
	    $dompdf = new Dompdf();
	    $dompdf->loadHtml($html);
	    $dompdf->setPaper('A4', 'portrait');
	    $dompdf->render();
	    $dompdf->stream($filename);

	 	//$filename = 'Purchase_Order_' . $po_number.'.html';
		//header('Content-type: text/html');
		////header('Set-Cookie: fileLoading=true');
		//header('Content-Disposition: attachment; filename='.$filename);
		//echo $html;
		//exit;
		//$this->response->setOutput($this->load->view('sale/order_invoice', $data));
	}

	public function prints_boq() {
		$this->load->language('catalog/invoice');
		$this->load->language('sale/order');

		$data['title'] = 'Sales Invoice';

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		$data['text_invoice'] = 'Sales Invoice';//$this->language->get('text_invoice');
		$data['text_order_detail'] = $this->language->get('text_order_detail');
		$data['text_order_id'] = $this->language->get('text_order_id');
		$data['text_invoice_no'] = 'Purchase Order Number';//$this->language->get('text_invoice_no');
		$data['text_invoice_date'] = $this->language->get('text_invoice_date');
		$data['text_date_added'] = $this->language->get('text_date_added');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_fax'] = $this->language->get('text_fax');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_website'] = $this->language->get('text_website');
		$data['text_payment_address'] = $this->language->get('text_payment_address');
		$data['text_shipping_address'] = $this->language->get('text_shipping_address');
		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comment'] = $this->language->get('text_comment');

		$data['column_product'] = $this->language->get('column_product');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total'] = $this->language->get('column_total');

		$this->load->model('catalog/invoice');
		$data['orders'] = array();
		$orders = array();

		if (isset($this->request->get['project_id'])) {
			$project_id = $this->request->get['project_id'];
		}
		if (isset($this->request->get['type'])) {
			$type = $this->request->get['type'];
		}

		//$invoice_infos = $this->model_catalog_invoice->getInvoice($project_id, 'material');

		$project_datas = $this->db->query("SELECT * FROM `oc_project` WHERE `project_id` = '".$project_id."' "); 
		$productraw_datas = array();
		$category_name = '';
		$site_code = '';
		$site_address = '';
		$image_ref_no = '';
		$project_code = '';
		if($project_datas->num_rows > 0){
			$cnt = 1;
			$project_data = $project_datas->row;
			$category_id = $project_data['category_id'];
			$category_names = $this->db->query("SELECT `name` FROM `oc_category` WHERE `category_id` = '".$category_id."' ");
			if($category_names->num_rows > 0){
				$category_name = $category_names->row['name'];
			}
			$projectboqs = $this->db->query("SELECT * FROM `is_projectboq` WHERE `productboq_category_id` = '".$category_id."' AND `project_id` = '".$project_id."' "); 
			if($projectboqs->num_rows > 0){
				$projectboq_info = $projectboqs->row;						
				$project_datas = $this->db->query("SELECT `ref_prefix`, `ref_no`, `site_code`, `site_address`, `project_code` FROM `oc_project` WHERE `project_id` = '".$project_id."' "); 
				if($project_datas->num_rows > 0){
					$project_data = $project_datas->row;
					$image_ref_no = $project_data['ref_prefix'].'/'.$project_data['ref_no'];
					$site_code = $project_data['site_code'];
					$site_address = $project_data['site_address'];
					$project_code = $project_data['project_code'];
				}
				$product_raw_group_datass = $this->db->query("SELECT * FROM `is_projectboqraw` WHERE `productboq_category_id` = '".$projectboq_info['productboq_category_id']."' AND `project_id` = '".$project_id."' GROUP BY `hsn_code`")->rows;
				$rate_total = 0;
				$total_total = 0;
				$quantity_total = 0;
				$cnt = 1;
				foreach($product_raw_group_datass as $pkeys => $pvalues){
					$product_raw_datass = $this->db->query("SELECT * FROM `is_projectboqraw` WHERE `productboq_category_id` = '".$projectboq_info['productboq_category_id']."' AND `project_id` = '".$project_id."' AND `hsn_code` = '".$pvalues['hsn_code']."' ")->rows;
					$cnt1 = 1;
					foreach($product_raw_datass as $pkey => $pvalue){
						$productraw_datas[$cnt][] = array(
							'cnt' => $cnt.'.'.$cnt1,
							'productboqraw_name' => $pvalue['productboqraw_name'],
							'productboqraw_description' => $pvalue['productboqraw_description'],
							'unit' => $pvalue['unit'],
							'productboqraw_rate' => $pvalue['productboqraw_rate'],
							'quantity' => $pvalue['quantity'],
							'productboqraw_total' => $pvalue['productboqraw_total'],
							'hsn_heading' => $pvalue['hsn_name'].' ( ' . $pvalue['hsn_code'] . ' )',
						);
						$rate_total = $rate_total + $pvalue['productboqraw_rate'];
						$total_total = $total_total + $pvalue['productboqraw_total'];
						$quantity_total = $quantity_total + $pvalue['quantity'];
						$cnt1 ++;			
					} 
					$cnt ++;
				}
			}
		}		
		
		$data['orders'][] = array(
			'category_name' => $category_name,
			'image_ref_no' => $image_ref_no,
			'site_code' => $site_code,
			'site_address' => $site_address,
			'rate_total' => sprintf("%0.2f",$rate_total),
			'quantity_total' => sprintf("%0.2f",$quantity_total),
			'total_total' => sprintf("%0.2f",$total_total),
			'productraw_datas' => $productraw_datas,
			'last_count' => $cnt - 1,
		);
		// echo '<pre>';
		// print_r($data['orders']);
		// exit;
		//$data['sign_image'] = 'C:/xampp/htdocs/imagesolution/admin/view/image/image_sign.png';
		$data['login_name'] = $this->user->getEmail();
		$data['header_image'] = DIR_APPLICATION.'view/image/image_logo.png';
		$data['user_name'] = $this->user->getFullname();
		// echo '<pre>';
		// print_r($data['orders']);
		// exit;
		$html = $this->load->view('sale/sales_boq', $data);
		$html = preg_replace('/>\s+</', "><", $html);
		//echo $html;exit;
		//$client_name = str_replace(array(' ', ',', '&', '.'), '_', $invoice_infos['client']);
		//$filename = 'Purchase_Order_' . $manufacturer_name . '_' . $po_number. '_' . $t_date .'.pdf';
		$filename = 'BOQ_'.$project_code;
		//echo $filename;exit;		
		//require_once '/home/aaron/public_html/balaji/system/library/dompdf/autoload.inc.php';
		//require_once("/Library/WebServer/Documents/balaji/system/library/dompdf/dompdf_config.inc.php");
	    define("DOMPDF_ENABLE_REMOTE", false);
	    $dompdf = new Dompdf();
	    $dompdf->loadHtml($html);
	    $dompdf->setPaper('A4', 'portrait');
	    $dompdf->render();
	    $dompdf->stream($filename);

	 	//$filename = 'Purchase_Order_' . $po_number.'.html';
		//header('Content-type: text/html');
		////header('Set-Cookie: fileLoading=true');
		//header('Content-Disposition: attachment; filename='.$filename);
		//echo $html;
		//exit;
		//$this->response->setOutput($this->load->view('sale/order_invoice', $data));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/invoice');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				//'start'       => 0,
				//'limit'       => 5
			);

			$results = $this->model_catalog_invoice->getInvoices($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'invoice_id' => $result['invoice_id'],
					'name'     => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getJobId() {
		$json = array();

		if (isset($this->request->get['category_id'])) {
			$this->load->model('catalog/invoice');
			$results = $this->model_catalog_invoice->getJobId($this->request->get['category_id']);
			
			echo $results;
			exit;	
			
		
		}

		
	}


	public function transport() {
		$this->load->language('catalog/invoice');
		$this->load->language('sale/order');

		$data['title'] = 'Transport Invoice';

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		$data['text_invoice'] = 'Transport Invoice';//$this->language->get('text_invoice');
		$data['text_order_detail'] = $this->language->get('text_order_detail');
		$data['text_order_id'] = $this->language->get('text_order_id');
		$data['text_invoice_no'] = 'Purchase Order Number';//$this->language->get('text_invoice_no');
		$data['text_invoice_date'] = $this->language->get('text_invoice_date');
		$data['text_date_added'] = $this->language->get('text_date_added');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_fax'] = $this->language->get('text_fax');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_website'] = $this->language->get('text_website');
		$data['text_payment_address'] = $this->language->get('text_payment_address');
		$data['text_shipping_address'] = $this->language->get('text_shipping_address');
		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comment'] = $this->language->get('text_comment');

		$data['column_product'] = $this->language->get('column_product');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total'] = $this->language->get('column_total');

		$this->load->model('catalog/invoice');
		$data['orders'] = array();
		$orders = array();

		if (isset($this->request->get['project_id'])) {
			$project_id = $this->request->get['project_id'];
		}
		if (isset($this->request->get['type'])) {
			$type = $this->request->get['type'];
		}

		$invoice_infos = $this->model_catalog_invoice->getInvoice($project_id, $type);
		$project_infos = $this->model_catalog_invoice->getProject($project_id);

		$productraw_datas = array();
		// $project_datas = $this->db->query("SELECT * FROM `oc_project` WHERE `project_id` = '".$project_id."' "); 
		// if($project_datas->num_rows > 0){
		// 	$cnt = 1;
		// 	$project_data = $project_datas->row;
		// 	$category_id = $project_data['category_id'];
		// 	$projectboqs = $this->db->query("SELECT * FROM `is_projectboq` WHERE `productboq_category_id` = '".$category_id."' AND `project_id` = '".$project_id."' "); 
		// 	if($projectboqs->num_rows > 0){
		// 		$projectboq_info = $projectboqs->row;						
		// 		$product_raw_datass = $this->db->query("SELECT * FROM `is_projectboqraw` WHERE `productboq_category_id` = '".$projectboq_info['productboq_category_id']."' AND `project_id` = '".$project_id."' ")->rows;
		// 		foreach($product_raw_datass as $pkeys => $pvalues){
		// 			$productraw_datas[] = array(
		// 				'cnt' => $cnt,
		// 				'productboqraw_name' => $pvalues['productboqraw_name'],
		// 				'quantity' => $pvalues['quantity'],
		// 			); 
		// 			$cnt ++;			
		// 		}
		// 	}
		// }		

		// echo '<pre>';
		// print_r($productraw_datas);
		// exit;

		$total = 0;
		setlocale(LC_MONETARY, 'en_IN');
		$material_data = array();
		$material_inner_data = array();
		$total_material_amount = 0.00;
		$total_taxable_value = 0.00;
		$total_sgst_value = 0.00;
		$total_cgst_value = 0.00;
		$total_igst_value = 0.00;
		$total_tax_value = 0.00;
		$inter_status = 0;
		$gst_reverge_charge = 0.00;
		if($invoice_infos['total_material_total'] != '0.00'){
			if($invoice_infos['company_branch_state'] == $invoice_infos['site_address_state']){
				$inter_status = 1;
			}
			for($i=1;$i<=5;$i++){
				$material_datass = $this->db->query("SELECT * FROM `is_invoice_material` WHERE `project_id` = '".$project_id."' AND `inc_id` = '".$i."' ");
				if($material_datass->num_rows > 0){
					$mvalue = $material_datass->row;
					$transport_prices = $this->db->query("SELECT transport_price FROM `is_productfinished` WHERE `productfinished_id` = '".$mvalue['material_id']."' ");
					if($transport_prices->num_rows > 0){
						$transport_price = $transport_prices->row['transport_price'];
					} else {
						$transport_price = '';
					}
					$material_taxable_value = round($transport_price * $mvalue['material_quantity']);
					
					$material_sgst_value = 0.00;
					$material_cgst_value = 0.00;
					$material_igst_value = 0.00;
					if($inter_status == 1){
						$tax_rate_divide = $mvalue['material_tax_rate'] / 2;
						
						$material_sgst_rate = $tax_rate_divide;
						$material_sgst_value = (($material_taxable_value * $material_sgst_rate) / 100);
						//$material_sgst_value = number_format($material_sgst_value, 2, '.', '');
						$material_sgst_value = round($material_sgst_value);

						$material_cgst_rate = $tax_rate_divide;
						$material_cgst_value = (($material_taxable_value * $material_cgst_rate) / 100);
						//$material_cgst_value = number_format($material_cgst_value, 2, '.', '');
						$material_cgst_value = round($material_cgst_value);

						$material_inner_data[] = array(	
							'inc_id' => $mvalue['inc_id'],
							'material_description' => $mvalue['material_description'],
							'material_hsn_code' => $mvalue['material_hsn_code'],
							'material_rate' => $transport_price,
							'material_quantity'	       => $mvalue['material_quantity'],
							'material_taxable_value' => $material_taxable_value,
							'material_cgst_rate' => $material_cgst_rate . '%',
							'material_cgst_value' => $material_cgst_value,
							'material_sgst_rate' => $material_sgst_rate . '%',
							'material_sgst_value' => $material_sgst_value,
							'material_amount'    => round($mvalue['material_amount']),	
						);
					} else {
						$material_igst_rate = $mvalue['material_tax_rate'];
						$material_igst_value = (($material_taxable_value * $material_igst_rate) / 100);
						//$material_igst_value = number_format($material_igst_value, 2, '.', '');
						$material_igst_value = round($material_igst_value);

						$material_inner_data[] = array(	
							'inc_id' => $mvalue['inc_id'],
							'material_description' => $mvalue['material_description'],
							'material_hsn_code' => $mvalue['material_hsn_code'],
							'material_rate' => $transport_price,
							'material_quantity'	       => $mvalue['material_quantity'],
							'material_taxable_value' => $material_taxable_value,
							'material_igst_rate' => $material_igst_rate . '%',
							'material_igst_value' => $material_igst_value,
							'material_amount'    => round($mvalue['material_amount']),	
						);
					}
					$total_taxable_value = round($total_taxable_value + $material_taxable_value);
					$total_sgst_value = round($total_sgst_value + $material_sgst_value);
					$total_cgst_value = round($total_cgst_value + $material_cgst_value);
					$total_igst_value = round($total_igst_value + $material_igst_value);
					if($inter_status == 1){
						$total_tax_value = round($total_tax_value + $material_cgst_value + $material_sgst_value);
					} else {
						$total_tax_value = round($total_tax_value + $total_igst_value);
					}
					$total_material_amount = round($total_material_amount + $mvalue['material_amount']);
				} else {
					if($inter_status == 1){
						$material_inner_data[] = array(	
							'inc_id' => '&nbsp;',
							'material_description' => '&nbsp;',
							'material_hsn_code' => '&nbsp;',
							'material_quantity'	       => '&nbsp;',
							'material_rate' => '&nbsp;',
							'material_taxable_value' => '&nbsp;',
							'material_sgst_rate' => '&nbsp;',
							'material_sgst_value' => '&nbsp;',
							'material_cgst_rate' => '&nbsp;',
							'material_cgst_value' => '&nbsp;',
							'material_amount'    => '&nbsp;',	
						);
					} else {
						$material_inner_data[] = array(	
							'inc_id' => '&nbsp;',
							'material_description' => '&nbsp;',
							'material_hsn_code' => '&nbsp;',
							'material_quantity'	       => '&nbsp;',
							'material_rate' => '&nbsp;',
							'material_taxable_value' => '&nbsp;',
							'material_igst_rate' => '&nbsp;',
							'material_igst_value' => '&nbsp;',
							'material_amount'    => '&nbsp;',	
						);
					}
				}
			}
			
			$material_data[] = array(
				'total_taxable_value' => round($total_taxable_value),
				'total_cgst_value' => round($total_cgst_value),
				'total_sgst_value' => round($total_sgst_value),
				'total_igst_value' => round($total_igst_value),
				'total_tax_value' => round($total_tax_value),
				'total_material_amount' => round($total_material_amount),

				'material_inner_data' => $material_inner_data,
				'material_vat_rate'    => $invoice_infos['material_vat_rate'],
				'material_vat'    => $invoice_infos['material_vat'],
				'material_cst_rate'    => $invoice_infos['material_cst_rate'],
				'material_cst'    => $invoice_infos['material_cst'],
				'material_octroi_rate'    => $invoice_infos['material_octroi_rate'],
				'material_octroi'    => $invoice_infos['material_octroi'],
				'material_advance'    => round($invoice_infos['material_advance']),
				//'material_net_total' => number_format($invoice_infos['material_amount'] + $invoice_infos['material_vat'] + $invoice_infos['material_cst'] + $invoice_infos['material_octroi']),
				'material_net_total' => round($invoice_infos['material_amount'] + $total_tax_value),
				'total_material_total'    => round($invoice_infos['total_material_total']),// '4002325.00',//
				'gst_reverge_charge' => round($gst_reverge_charge),
				'inter_status' => $inter_status,
			);
		}
		
		$invoice_date = '';
		if($invoice_infos['invoice_date'] != '0000-00-00'){
			$invoice_date = date('F j, Y', strtotime($invoice_infos['invoice_date']));
		}

		$po_date = '';
		if($invoice_infos['po_date'] != '0000-00-00'){
			$po_date = date('j-M-Y', strtotime($invoice_infos['po_date']));
		}

		$ref_date = '';
		if($invoice_infos['ref_date'] != '0000-00-00'){
			$ref_date = date('j-M-Y', strtotime($invoice_infos['ref_date']));
		}

		if($invoice_infos['client_address_type'] == 'other'){
			$client_address_heading = $invoice_infos['client_address_heading'].' (Lessor' . ')';
			$client_address = $invoice_infos['client_address'];	
			$client_address_state = $invoice_infos['client_address_state'];	
			$client_address_tin = $invoice_infos['client_address_tin'];	

			$site_address_heading = $invoice_infos['client'].' (Lessor' . ')';
			$site_address = $invoice_infos['site_address'];
			$site_address_state = $invoice_infos['site_address_state'];
		} else {
			$client_address_heading = $invoice_infos['client'];
			$client_address = $invoice_infos['client_address'];	
			$client_address_state = $invoice_infos['client_address_state'];	
			$client_address_tin = $invoice_infos['client_address_tin'];	

			$site_address_heading = $invoice_infos['customer'];
			$site_address = $invoice_infos['site_address'];
			$site_address_state = $invoice_infos['site_address_state'];
		}

		$payment_string = '';
		if($project_infos['payment_type'] != '' && $project_infos['advance_type'] == 'material'){
			if($project_infos['payment_type'] == 'Cash'){
				$payment_string = 'Advance Payment of ' . $project_infos['advance'] . ' by Cash';
			} else if($project_infos['payment_type'] == 'Cheque'){
				$payment_string = 'Advance Payment of ' . $project_infos['advance'] . ' by Cheque, Bank Name : ' . $project_infos['bank_name'] . ' | Cheque Number : ' . $project_infos['cheque_number'] . ' | Cheque Date : ' . $project_infos['cheque_date'];
			} else if($project_infos['payment_type'] == 'Bank Transfer'){
				$payment_string = 'Advance Payment of ' . $project_infos['advance'] . ' by Bank Transfer, Bank Name : ' . $project_infos['bank_name'] . ' | Account Number : ' . $project_infos['account_number'] . ' | Transfer Date : ' . $project_infos['transfer_date'];
			}
		}
		/*
		echo '<pre>';
		print_r($invoice_infos);
		exit;*/
		$states= array(
			'' => '',
			'Andaman & Nicobar' => 'AN',
			'Andhra Pradesh' => 'AP',
			'Arunachal Pradesh' => 'AR',
			'Assam' => 'AS',
			'Bihar' => 'BH',
			'Chandigarh' => 'CH',
			'Chhattisgarh' => 'CT',
			'Dadar & Nagar Haveli' => 'DN',
			'Daman & Diu' => 'DD',
			'Delhi' => 'DL',
			'Goa' => 'GA',
			'Gujarat' => 'GJ',
			'Haryana' => 'HR',
			'Himachal Pradesh' => 'HP',
			'Jammu Kashmir' => 'JK',
			'Jharkhand' => 'JH',
			'Karnataka' => 'KA',
			'Kerala' => 'KL',
			'Lakshadeep' => 'LD',
			'Madhya Pradesh' => 'MP',
			'Maharashtra' => 'MH',
			'Manipur' => 'MN',
			'Meghalaya' => 'ME',
			'Mizoram' => 'MI',
			'Nagaland' => 'NL',
			'Orissa' => 'OR',
			'Pondicherry' => 'PY',
			'Punjab' => 'PB',
			'Rajasthan' => 'RJ',
			'Sikkim' => 'SK',
			'Tamil Nadu' => 'TN',
			'Telangana' => 'TS',
			'Tripura' => 'TR',
			'Uttaranchal' => 'UT',
			'Uttarakhand' => 'UK',
			'Uttar Pradesh' => 'UP',
			'West Bengal' => 'WB',
		);
		if(isset($states[$invoice_infos['site_address_state']])){
			$site_address_state_code = $states[$invoice_infos['site_address_state']];
		} else {
			$site_address_state_code = '';
		}

		if(isset($states[$invoice_infos['client_address_state']])){
			$client_address_state_code = $states[$invoice_infos['client_address_state']];
		} else {
			$client_address_state_code = '';
		}

		$atmid=$this->db->query("SELECT `atm_id` FROM oc_project WHERE project_id = '".$project_id."'")->row;
		//echo "SELECT `atm_id` FROM oc_project WHERE project_id = '".$project_id."'";exit;
		// echo"<pre>";
		// print_r($invoice_infos);
		// exit;
		$ts_nos = $this->db->query("SELECT `ts_no` FROM is_invoice WHERE `project_id` = '".$project_id."' ");
		if (($ts_nos->num_rows > 0) && ($ts_nos->row['ts_no'] > 0)) {
			$ts_no = $ts_nos->row['ts_no'];
		} else {
			$ts_nos1 = $this->db->query("SELECT `ts_no` FROM is_invoice WHERE 1=1 ORDER BY `ts_no` DESC LIMIT 1 ");
			if ($ts_nos1->num_rows > 0) {
				$prev_ts_no = $ts_nos1->row['ts_no'];
			} else {
				$prev_ts_no = 0;
			}
			$ts_no = $prev_ts_no + 1;

		}
		$this->db->query("UPDATE `is_invoice` SET `ts_no` = '".$ts_no."' WHERE project_id = '".$project_id."' ");

		$data['orders'][] = array(
			'company_name'  => $invoice_infos['company_name'],
			'company_address'  => nl2br($invoice_infos['company_address']),
			'transport_number'   => $invoice_infos['invoice_prefix'].'/'.$ts_no,
			'invoice_date'	   => $invoice_date,
			'branch_address'   => nl2br($invoice_infos['branch_address']),
			'branch_pan'	   => $invoice_infos['branch_pan'],
			'branch_service_tax' => $invoice_infos['branch_service_tax'],
			'branch_vat'	   => $invoice_infos['branch_vat'],
			'company_branch_state' => $invoice_infos['company_branch_state'],
			'state_code' => $states[$invoice_infos['company_branch_state']],
			'client_state_code' =>$client_address_state_code,
			'branch_cst'	   => $invoice_infos['branch_cst'],
			'branch_gst'	   => $invoice_infos['branch_gst'],
			'reverse_charges'	   => $invoice_infos['reverse_charges'],
			'gst_no' => $invoice_infos['gst_no'],
			'atmid'  => $atmid['atm_id'],
			'client_address_heading' => $client_address_heading,
			'client_address' => $client_address,
			'client_address_state' => $client_address_state,
			'client_address_tin' => $client_address_tin,
			'site_address_heading' => $site_address_heading,
			'site_address' => $site_address,
			'site_address_state' => $site_address_state,
			'site_address_state_code' => $site_address_state_code,
			'po_number' => $invoice_infos['po_number'],
			'po_date' => $po_date,
			'ref_no' => $invoice_infos['ref_no'],
			'ref_date' => $ref_date,
			'site_id' => $invoice_infos['site_code'],
			'material_data'          => $material_data,
			'payment_string'          => $payment_string,
			'productraw_datas' => $productraw_datas,
			'inter_status' => $inter_status,
			//'labour_data'          => $labour_data,
		);
		// echo '<pre>';
		// print_r($data['orders']);
		// exit;
		//$data['sign_image'] = 'C:/xampp/htdocs/imagesolution/admin/view/image/image_sign.png';
		$data['login_name'] = $this->user->getEmail();
		$data['header_image'] = DIR_APPLICATION.'view/image/image_logo.png';
		$data['user_name'] = $this->user->getFullname();
		// echo '<pre>';
		// print_r($invoice_infos['invoice_prefix'].'_'.$invoice_infos['inv_id']);
		// exit;
		$html = $this->load->view('sale/transport_invoice', $data);
		//echo $html;exit;
		$invoice_number = $invoice_infos['invoice_prefix'];
		$invoice_numbers = substr($invoice_number,0,3);

		if (($invoice_numbers == 'IGS') || ($invoice_numbers == 'IG')) {
		   $resStr = str_replace('IGS', 'TR', $invoice_infos['invoice_prefix']);
		    //echo "Invoice No : ".$resStr;
		   } elseif (($invoice_numbers == 'ISB') || ($invoice_numbers == 'IS')) {
		   $resStr = str_replace('ISB', 'TR',$invoice_infos['invoice_prefix']);
		 	 //echo "Invoice No : ".$resStr;
		  }
		$client_name = str_replace(array(' ', ',', '&', '.', '/'), '_', $resStr.'_'.$invoice_infos['inv_id']);
		//$filename = 'Purchase_Order_' . $manufacturer_name . '_' . $po_number. '_' . $t_date .'.pdf';

		$filename = $client_name;
		// echo"<pre>";
		// print_r($client_name);
		// exit;
		//echo $filename;exit;$orders['0']['invoice_number']		
		//require_once '/home/aaron/public_html/balaji/system/library/dompdf/autoload.inc.php';
		//require_once("/Library/WebServer/Documents/balaji/system/library/dompdf/dompdf_config.inc.php");
	    define("DOMPDF_ENABLE_REMOTE", false);
	    $dompdf = new Dompdf();
	    $dompdf->loadHtml($html);
	    $dompdf->setPaper('A4', 'portrait');
	    $dompdf->render();
	    $dompdf->stream($filename);

	 	//$filename = 'Purchase_Order_' . $po_number.'.html';
		//header('Content-type: text/html');
		////header('Set-Cookie: fileLoading=true');
		//header('Content-Disposition: attachment; filename='.$filename);
		//echo $html;
		//exit;
		//$this->response->setOutput($this->load->view('sale/order_invoice', $data));
	}
}