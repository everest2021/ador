<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$file = '/var/www/html/taazitokari/webhook_test/rider.txt';
//$file = 'C:/xampp/htdocs/hotel/ador/services/query.txt';
$handle = fopen($file, 'a+'); 
$data = file_get_contents('php://input');
error_reporting(E_ALL);
ini_set("display_errors", 1);
$datas = json_decode($data,true);
fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($datas, true)  . "\n");
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas, $handle);
date_default_timezone_set("Asia/Kolkata");
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array(),$handle){

		if($data != NULL && !empty($data)){ 
				$this->query("INSERT INTO `oc_address_app` SET 
					`channel_name` = '".$this->escape($data['additional_info']['external_channel']['name'], $this->conn) ."', 
					`channel_order_id` = '".$this->escape($data['additional_info']['external_channel']['order_id'], $this->conn) ."', 
					`rider_name` = '".$this->escape($data['delivery_info']['delivery_person_details']['name'], $this->conn)."',
					`rider_alt_phone` = '".$this->escape($data['delivery_info']['delivery_person_details']['alt_phone'], $this->conn)."', 
					`rider_phone`= '".$this->escape($data['delivery_info']['delivery_person_details']['phone'], $this->conn)."',
					`rider_user_id`= '".$this->escape($data['delivery_info']['delivery_person_details']['user_id'], $this->conn)."', 
					`rider_status`= '".$this->escape($data['delivery_info']['current_state'], $this->conn)."', 
					`real_order_id` = '".$this->escape($data['delivery_info']['order_id'], $this->conn)."', 
					`entry_status`= '0'
					 "
				, $this->conn);

				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_address_app` SET 
					`channel_name` = '".$this->escape($data['additional_info']['external_channel']['name'], $this->conn) ."', 
					`channel_order_id` = '".$this->escape($data['additional_info']['external_channel']['order_id'], $this->conn) ."', 
					`rider_name` = '".$this->escape($data['delivery_info']['delivery_person_details']['name'], $this->conn)."',
					`rider_alt_phone` = '".$this->escape($data['delivery_info']['delivery_person_details']['alt_phone'], $this->conn)."', 
					`rider_phone`= '".$this->escape($data['delivery_info']['delivery_person_details']['phone'], $this->conn)."',
					`rider_user_id`= '".$this->escape($data['delivery_info']['delivery_person_details']['user_id'], $this->conn)."', 
					`rider_status`= '".$this->escape($data['delivery_info']['current_state'], $this->conn)."', 
					`real_order_id` = '".$this->escape($data['delivery_info']['order_id'], $this->conn)."', 
					`entry_status`= '0' ", true)  . "\n");
			
		}
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>