<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$file = FILE_PATH;
$handle = fopen($file, 'a+');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$categoty_api = new categoty_api();

fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('innn to cat', true)  . "\n");
$value = $categoty_api->getCategotydata($datas, $handle);
fclose($handle);

exit(json_encode($value));


class categoty_api {
	public $conn;
	public function __construct() {
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getCategotydata($data = array(),$handle){

		fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('cat', true)  . "\n");

		$result = array();
		$result['category'] = $this->query("SELECT * FROM  `oc_category` ",$this->conn)->rows;

		$running_sql = "SELECT `status` FROM `oc_running_table` WHERE `table_id` = '".$data['table_id']."' ";
		$running_datas = $this->query($running_sql,$this->conn);
		if($running_datas->num_rows > 0){
			$result['status'] = 2;
		} else {
		
		$SKIPTABLE = $this->query("SELECT `value` FROM settings_ador WHERE `key` = 'SKIPTABLE'",$this->conn)->row;

			if($SKIPTABLE['value'] == 0) {
				date_default_timezone_set("Asia/Kolkata");
				$sql = "INSERT INTO `oc_running_table` SET `table_id` = '".$data['table_id']."', `date_added` = '".date('Y-m-d')."', `time_added` = '".date('H:i:s')."' ";
				$this->query($sql,$this->conn);
				$result['status'] = 1;
			}	
		}
		$bill_datas = $this->query("SELECT `bill_status` FROM oc_order_info WHERE `table_id` = '".$data['table_id']."' AND bill_status = 1 AND payment_status ='0'",$this->conn);
		if($bill_datas->num_rows > 0){
			$result['bill_status'] = $bill_datas->row['bill_status'];
		} else{
			$result['bill_status'] = 0;
		}

		return $result;
	}
	
}

?>