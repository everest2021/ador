<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$file = 'C:/xampp/htdocs/hotel/ador/services/query.txt';
date_default_timezone_set("Asia/Kolkata");
//$file = 'C:/xampp/htdocs/hotel/ador/services/query.txt';
$handle = fopen($file, 'a+'); 
$data = file_get_contents('php://input');
error_reporting(E_ALL);
ini_set("display_errors", 1);
$datas = json_decode($data,true);
//fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($datas, true)  . "\n");
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas, $handle);

exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array(),$handle){
		$this->query("TRUNCATE oc_item_urbanpiper", $this->conn);
		$this->query("TRUNCATE oc_urbanpiper_category_time", $this->conn);
		$this->query("TRUNCATE oc_allwebhook_response", $this->conn);
		//$this->query("TRUNCATE oc_urbanpiper_platform", $this->conn);
		$this->query("TRUNCATE oc_urbanpiper_platform_items", $this->conn);
		$this->query("DELETE FROM `oc_pipedream_store` WHERE 1 = 1", $this->conn);
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>