<?php
if(isset($_GET['order_exist']) && $_GET['order_exist'] == 1){
	$order_exist = 1;
} else {
	$order_exist = 0;
}
?>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Order Now</title>
	<meta property="og:url" content="https://taazitokari.com/order.php" />
	<meta property="og:image" content="https://taazitokari.com/image/catalog/logo/logo3.png">
	<link type="text/css" href="order.css" rel="stylesheet" media="screen" />	
</head>
<body>
	<div class="mainDiv">
		<img src="https://taazitokari.com/image/catalog/logo/logo3.png" style="width:97%" />	
		<?php if($order_exist == 0){ ?>
			<h1>
				Order Placed !!!
			</h1>
			<h3 style="color: white;">You will get the delivery by 2-05-2020</h3>
			<h3 style="color: white;">Our team will get back to you.</h3>
			<h3 style="color: white;">Thank you</h3>
		<?php } else { ?>
			<h1>
				Your Order already Exist !!!
			</h1>
			<h3 style="color: white;">Thank you</h3>
		<?php } ?>
		<a class="formButton" href="https://taazitokari.com/order.php">Continue</a>
	</div>
</body>