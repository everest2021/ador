<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		if(!isset($data['user_id'])){
			$data['user_id'] = '';//'212';
		}
		if(!isset($data['address_id'])){
			$data['address_id'] = '';//'225';
		}
		if(!isset($data['address_1'])){
			$data['address_1'] = '';//'Test Address';
		}
		if(!isset($data['address_2'])){
			$data['address_2'] = '';//'Test Address 1';
		}
		if(!isset($data['city'])){
			$data['city'] = '';//'Gass';
		}
		if(!isset($data['postcode'])){
			$data['postcode'] = '';//'401203';
		}
		if(!isset($data['area'])){
			$data['area'] = '';//'Gass';
		}
		
		$result = array();
		$exist_status = 0;
		if($data['postcode'] != ''){
			$user_data = $this->query("SELECT * FROM oc_pincode WHERE pincode = '" .$data['postcode']."' AND `status` = '1' ",$this->conn);
			if($user_data->num_rows > 0){
				$exist_status = 1;
			}
		}

		if($exist_status == 1){
			if ($data['address_id'] == 0){
				$user_datas = $this->query("SELECT * FROM `oc_customer` WHERE customer_id = '".$data['user_id']."' ",$this->conn)->row;
				$address_data =  $this->query("INSERT INTO `oc_address` SET `customer_id` = '".$data['user_id']."', `address_1` = '".$this->escape($data['address_1'], $this->conn)."', `address_2` = '".$this->escape($data['address_2'], $this->conn)."', `city`= '".$this->escape($data['city'], $this->conn)."', `postcode`= '".$this->escape($data['postcode'], $this->conn)."', `area`= '".$this->escape($data['area'], $this->conn)."', `firstname` = '".$this->escape($user_datas['firstname'], $this->conn)."', `lastname` = '".$this->escape($user_datas['lastname'], $this->conn)."', `country_id` = 99 , `zone_id` = 1493 ", $this->conn);
				$result['success'] = 1;
				$result['city'] = $data['city'];
				$result['postcode'] = $data['postcode'];
				$result['area'] = $data['area'];
			} else{
				$address_data = $this->query("UPDATE `oc_address` SET `address_1` = '".$this->escape($data['address_1'], $this->conn)."', `address_2` = '".$this->escape($data['address_2'], $this->conn)."', `city`= '".$this->escape($data['city'], $this->conn)."', `postcode` = '".$this->escape($data['postcode'], $this->conn)."', `area`= '".$this->escape($data['area'], $this->conn)."' WHERE address_id ='".$data['address_id']."' ",$this->conn);
				$result['success'] = 2;
				$result['city'] = $data['city'];
				$result['postcode'] = $data['postcode'];
				$result['area'] = $data['area'];
			}
		} else {
			$result['success'] = 0;
			$result['city'] = '';
			$result['postcode'] = '';
			$result['area'] = '';
		}

		return $result;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>