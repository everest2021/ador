<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
		$this->conn->set_charset("utf8");
		$this->conn->query("SET SQL_MODE = ''");
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getProduct($product_id,$itemname_search) {
		$sql = "SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = 1 AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = 1 AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND pr.customer_group_id = 1) AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = 1) AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = 1) AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = 1) AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = 1 AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ";
		if($itemname_search != ''){
			$sql .= " AND pd.`name` LIKE '%".$itemname_search."%' ";
		}
		$query = $this->query($sql, $this->conn);	
		if ($query->num_rows) {
			if(!$query->row['special']){
				$price = ($query->row['discount'] ? $query->row['discount'] : $query->row['price']);
			} else {
				$price = $query->row['special'];
			}
			return array(
				'product_id'       => $query->row['product_id'],
				'name'             => html_entity_decode($query->row['name']),
				'description'      => $query->row['description'],
				'meta_title'       => $query->row['meta_title'],
				'meta_description' => $query->row['meta_description'],
				'meta_keyword'     => $query->row['meta_keyword'],
				'tag'              => $query->row['tag'],
				'model'            => $query->row['model'],
				'sku'              => $query->row['sku'],
				'upc'              => $query->row['upc'],
				'ean'              => $query->row['ean'],
				'jan'              => $query->row['jan'],
				'isbn'             => $query->row['isbn'],
				'mpn'              => $query->row['mpn'],
				'location'         => $query->row['location'],
				'quantity'         => $query->row['quantity'],
				'stock_status'     => $query->row['stock_status'],
				'stock_status_id'  => $query->row['stock_status_id'],
				'image'            => $query->row['image'],
				'manufacturer_id'  => $query->row['manufacturer_id'],
				'manufacturer'     => $query->row['manufacturer'],
				//'price'            => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
				'price'			=> $price,
				'unit'          => $query->row['unit'],
				'special'          => $query->row['special'],
				'reward'           => $query->row['reward'],
				'points'           => $query->row['points'],
				'tax_class_id'     => $query->row['tax_class_id'],
				'date_available'   => $query->row['date_available'],
				'weight'           => $query->row['weight'],
				'weight_class_id'  => $query->row['weight_class_id'],
				'length'           => $query->row['length'],
				'width'            => $query->row['width'],
				'height'           => $query->row['height'],
				'length_class_id'  => $query->row['length_class_id'],
				'subtract'         => $query->row['subtract'],
				'rating'           => round($query->row['rating']),
				'reviews'          => $query->row['reviews'] ? $query->row['reviews'] : 0,
				'minimum'          => $query->row['minimum'],
				'sort_order'       => $query->row['sort_order'],
				'status'           => $query->row['status'],
				'date_added'       => $query->row['date_added'],
				'date_modified'    => $query->row['date_modified'],
				'viewed'           => $query->row['viewed']
			);
		} else {
			return false;
		}
	}
	public function getBestSellerProducts($itemname_search) {
		$product_data = array();
		$query = $this->query("SELECT op.product_id, SUM(op.quantity) AS total FROM oc_order_product op LEFT JOIN `oc_order` o ON (op.order_id = o.order_id) LEFT JOIN `oc_product` p ON (op.product_id = p.product_id) LEFT JOIN oc_product_to_store p2s ON (p.product_id = p2s.product_id) WHERE o.order_status_id > '0' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '0' GROUP BY op.product_id ORDER BY total DESC LIMIT 8",$this->conn);
		foreach ($query->rows as $result) {
			$product_data[$result['product_id']] = $this->getProduct($result['product_id'],$itemname_search);
		}
		return $product_data;
	}
	public function getProductSpecials($itemname_search) {
		$sql = "SELECT DISTINCT ps.product_id, (SELECT AVG(rating) FROM oc_review r1 WHERE r1.product_id = ps.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating FROM oc_product_special ps LEFT JOIN oc_product p ON (ps.product_id = p.product_id) LEFT JOIN oc_product_description pd ON (p.product_id = pd.product_id) LEFT JOIN oc_product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 AND ps.customer_group_id = 1 AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id";
		$product_data = array();
		$query = $this->query($sql,$this->conn);
		foreach ($query->rows as $result) {
			$product_data[$result['product_id']] = $this->getProduct($result['product_id'],$itemname_search);
		}
		return $product_data;
	}
		
	public function getProductOptions($product_id) {
		$product_option_data = array();
		$product_option_query = $this->query("SELECT * FROM `oc_product_option` po LEFT JOIN `oc_option` o ON (po.option_id = o.option_id) LEFT JOIN oc_option_description od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = 1 ORDER BY o.sort_order ",$this->conn);
		
		foreach ($product_option_query->rows as $product_option) {
			$product_option_value_data = array();

			$product_option_value_query = $this->query("SELECT * FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int)$product_id . "' AND pov.product_option_id = '" . (int)$product_option['product_option_id'] . "' AND ovd.language_id = 1 ORDER BY ov.sort_order",$this->conn)->rows;

			foreach ($product_option_value_query as $product_option_value) {
				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'name'                    => $product_option_value['name'],
					'image'                   => $product_option_value['image'],
					'quantity'                => $product_option_value['quantity'],
					'subtract'                => $product_option_value['subtract'],
					'price'                   => $product_option_value['price'],
					'price_prefix'            => $product_option_value['price_prefix'],
					'weight'                  => $product_option_value['weight'],
					'weight_prefix'           => $product_option_value['weight_prefix']
				);
			}

			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'product_option_value' => $product_option_value_data,
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],
				'value'                => $product_option['value'],
				'required'             => $product_option['required'],
				'is_time'             => $product_option['is_time'],
				'is_date'             => $product_option['is_date']
			);
		}

		return $product_option_data;
	}

	public function getitem($data = array()){
		$data['featured'] = array();
		$data['bestseller'] = array();
		$data['category'] = array();
		$data['specialss'] = array();
		$data['item_datas'] = array();
		$result = array();
		$base = 'https://taazitokari.com/image/';
		$logo = 'https://taazitokari.com/image/catalog/logo/logo6.png';
		if(!isset($data['currentusercart'])){
			$data['currentusercart'] = array();
		}
		if(!isset($data['itemname_search'])){
			$data['itemname_search'] = '';
		}
		if(!isset($data['subcategory_id'])){
			$data['subcategory_id'] = '0';
		}
		if(!isset($data['user_id'])){
			$data['user_id'] = '';//'212';
		}

		$station_id = 0;
		if($data['user_id'] > 0){
			$address_data = $this->query("SELECT `postcode` FROM `oc_address` WHERE `customer_id` = '".$data['user_id']."' ",$this->conn);
			if($address_data->num_rows > 0){
				$pincode = $address_data->row['postcode'];
				$station_datas = $this->query("SELECT `station` FROM `oc_pincode` WHERE `pincode` = '".$pincode."' ",$this->conn);
				if($station_datas->num_rows > 0){
					$station_id = $station_datas->row['station'];
				}
			}
		}

		//echo "<pre>"; print_r($data);exit;
		$wholesaler_check = $this->query("SELECT is_wholesaler FROM oc_customer WHERE customer_id = '".$data['user_id']."' " ,$this->conn)->row;
		if(!isset($wholesaler_check['is_wholesaler'])){
			$wholesaler_check['is_wholesaler'] = 0;
		}
		$subcat = $this->query("SELECT * FROM `oc_category_path` WHERE path_id = '".$data['subcategory_id']."' AND category_id != '".$data['subcategory_id']."' ",$this->conn);
		if ($subcat->num_rows > 0) {
			foreach($subcat->rows as $skey => $svalue){
				$level = $this->query("SELECT * FROM `oc_category_path` WHERE `level` = 2 ",$this->conn);
				if($level->num_rows > 0){
					$levels = 2;
				} else {
					$levels = 1;
				}
				$subcategory = $this->query("SELECT * FROM `oc_category` c LEFT JOIN  `oc_category_description` cd  ON cd.`category_id`= c.`category_id` LEFT JOIN `oc_category_path`cp ON c.`category_id`= cp.`category_id`  WHERE  cp.`level`= '".$levels."' AND c.`category_id` = '".$svalue['category_id']."' AND c.`status` = '1' ",$this->conn);
				if ($subcategory->num_rows > 0) {
					foreach($subcategory->rows as $skey => $svalue){
						$data['category'][] = array(
							'subcategory_id' => $svalue['category_id'], 
							'name' => html_entity_decode($svalue['name']),
							'image' =>'https://taazitokari.com/'.'image/'.$svalue['image']
						);
					}	
				}
			}
			$data['result_type'] = 'sub_category';
			$total_items = 0;
			$total_price = 0;
			$item_price = 0;
			foreach($data['currentusercart'] as $key => $value){
				$item_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id`= p.`product_id` WHERE pd.`product_id`= '".$key."' AND p.`is_wholesaller`= '".$wholesaler_check['is_wholesaler']."'", $this->conn)->row;
				if(isset($item_datas['stock_status_id']) && $item_datas['stock_status_id'] == 9 || $item_datas['quantity'] == 0){
					$value = 0;
				}
				$special_datass = "SELECT product_special_id, price FROM oc_product_special  WHERE product_id = '".$key."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1" ;
				$special_data = $this->query($special_datass, $this->conn);
				if($special_data->num_rows > 0){
					$price = $special_data->row['price'];
					$product_special_id = $special_data->row['product_special_id'];
					if($station_id > 0){
						$special_datass = "SELECT price FROM oc_product_special_station_price  WHERE product_id = '".$key."' AND `product_special_id` = '".$product_special_id."' AND `station_id` = '".$station_id."' " ;
						$special_data = $this->query($special_datass, $this->conn);
						if($special_data->num_rows > 0){
							$price = $special_data->row['price'];
						}
					}
					$isSpecialPrice = 1;
				} else{
					$isSpecialPrice = 0;
					if (isset($wholesaler_check['is_wholesaler']) && $wholesaler_check['is_wholesaler'] == '1') {
						$price = $item_datas['wholeseller_price'];
					} else {
						$price = $item_datas['price'];
						if($station_id > 0){
							$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$key."' AND `station_id` = '".$station_id."' " ;
							$special_data = $this->query($special_datass, $this->conn);
							if($special_data->num_rows > 0){
								$price = $special_data->row['price'];
							}
						}
					}
				}
				if($item_datas['stock_status_id'] != 9 && $item_datas['quantity'] > 0){
					$total_items = $total_items + $value;
					$total_price = $total_price + ($value * $price);
				}
			}
			$data['total_items'] = (int)$total_items;
			$data['total_price'] = (int)$total_price;
		} else {
			$specials_product = $this->getProductSpecials($data['itemname_search']);
			if ($specials_product) {
				foreach ($specials_product as  $key => $result) {
					$spcl_name = (strlen($result['name']) > 12) ? substr($result['name'],0,10).'..' : $result['name'];
					$data['specialss'][] = array(
						'product_id'  => $result['product_id'],
						'image'       => $base.$result['image'],
						'name'        => $spcl_name,
						'stock_status_id' => $result['stock_status_id'],
					);
				}
			}
			
			if(!isset($data['currentusercart'])){
				$data['currentusercart'] = array();
			}
			if($data['subcategory_id'] == '0'){
				$item_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id`= p.`product_id` WHERE pd.`language_id`= 1 AND p.`status` = '1' AND p.`is_wholesaller`= '".$wholesaler_check['is_wholesaler']."' AND p.`date_available` <= NOW() ORDER BY p.price ASC" ,$this->conn);
			} else {
				$item_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN `oc_product_description` pd ON pd.`product_id`= p.`product_id` LEFT JOIN  `oc_product_to_category` pc ON pc.`product_id`= p.`product_id` WHERE pc.`category_id`= '".$data['subcategory_id']."' AND p.is_wholesaller='".$wholesaler_check['is_wholesaler']."' AND p.`status` = '1' AND p.`date_available` <= NOW() ORDER BY p.price ASC",$this->conn);
			}
			if ($item_datas->num_rows > 0) {
				foreach($item_datas->rows as $nkey => $nvalue){
					if(isset($data['currentusercart'][$nvalue['product_id']])){
						$quantity = $data['currentusercart'][$nvalue['product_id']];
					} else {
						$quantity = (int)0;
					}
					$regularPrice = 0;
					$special_datass = "SELECT price, product_special_id FROM oc_product_special  WHERE product_id = '".$nvalue['product_id']."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1" ;
					$special_data = $this->query($special_datass, $this->conn);
					if($special_data->num_rows > 0){
						$price = $special_data->row['price'];
						$product_special_id = $special_data->row['product_special_id'];
						if($station_id > 0){
							$special_datass = "SELECT price FROM oc_product_special_station_price  WHERE product_id = '".$nvalue['product_id']."' AND `product_special_id` = '".$product_special_id."' AND `station_id` = '".$station_id."' " ;
							$special_data = $this->query($special_datass, $this->conn);
							if($special_data->num_rows > 0){
								$price = $special_data->row['price'];
							}
						}
						$isSpecialPrice = 1;
						$regularPrice = $nvalue['price'];
						if($station_id > 0){
							$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$nvalue['product_id']."' AND `station_id` = '".$station_id."' " ;
							$special_data = $this->query($special_datass, $this->conn);
							if($special_data->num_rows > 0){
								$regularPrice = $special_data->row['price'];
							}
						}
					} else{
						$isSpecialPrice = 0;
						if (isset($wholesaler_check['is_wholesaler']) && $wholesaler_check['is_wholesaler'] == '1') {
							$price = $nvalue['wholeseller_price'];
						} else {
							$price = $nvalue['price'];
							if($station_id > 0){
								$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$nvalue['product_id']."' AND `station_id` = '".$station_id."' " ;
								$special_data = $this->query($special_datass, $this->conn);
								if($special_data->num_rows > 0){
									$price = $special_data->row['price'];
								}
							}
						}
					}
					if($nvalue['quantity'] <= 0){
						$nvalue['stock_status_id'] = 9;
					}
					$data['item_datas'][] = array(
						'product_id' => $nvalue['product_id'], 
						'stock_status_id' => $nvalue['stock_status_id'],
						'name' => html_entity_decode($nvalue['name']),
						'description' => html_entity_decode($nvalue['description']),
						'quantity' => $quantity,
						'is_wholesaler' => $wholesaler_check['is_wholesaler'],
						'wholesaler_quantity' => $nvalue['wholeseller_minimium_quantity'],
						'price' => (int)$price,
						'isSpecialPrice' => $isSpecialPrice,
						'regularPrice' => (int)$regularPrice,
						'image' => $base.$nvalue['image'],
						'logo_bg'=> $logo
					);

				}	
			}

			$total_items = 0;
			$total_price = 0;
			$item_price = 0;
			foreach($data['currentusercart'] as $key => $value){
				$item_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id`= p.`product_id` WHERE pd.`product_id`= '".$key."' AND p.`is_wholesaller`= '".$wholesaler_check['is_wholesaler']."'", $this->conn)->row;
				if(isset($item_datas['stock_status_id']) && $item_datas['stock_status_id'] == 9 || $item_datas['quantity'] == 0){
					$value = 0;
				}
				$special_datass = "SELECT product_special_id, price FROM oc_product_special  WHERE product_id = '".$key."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1" ;
				$special_data = $this->query($special_datass, $this->conn);
				if($special_data->num_rows > 0){
					$price = $special_data->row['price'];
					$product_special_id = $special_data->row['product_special_id'];
					if($station_id > 0){
						$special_datass = "SELECT price FROM oc_product_special_station_price  WHERE product_id = '".$key."' AND `product_special_id` = '".$product_special_id."' AND `station_id` = '".$station_id."' " ;
						$special_data = $this->query($special_datass, $this->conn);
						if($special_data->num_rows > 0){
							$price = $special_data->row['price'];
						}
					}
					$isSpecialPrice = 1;
				} else{
					$isSpecialPrice = 0;
					if (isset($wholesaler_check['is_wholesaler']) && $wholesaler_check['is_wholesaler'] == '1') {
						$price = $item_datas['wholeseller_price'];
					} else {
						$price = $item_datas['price'];
						if($station_id > 0){
							$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$key."' AND `station_id` = '".$station_id."' " ;
							$special_data = $this->query($special_datass, $this->conn);
							if($special_data->num_rows > 0){
								$price = $special_data->row['price'];
							}
						}
					}
				}
				if($item_datas['stock_status_id'] != 9 && $item_datas['quantity'] > 0){
					$total_items = $total_items + $value;
					$total_price = $total_price + ($value * $price);
				}
			}
			$data['total_items'] = (int)$total_items;
			$data['total_price'] = (int)$total_price;
			$level2 = $this->query("SELECT * FROM `oc_category_path` WHERE level = 2",$this->conn)->rows;
			$id = array();
			foreach($level2  as $pvalue) {
				$id[] = $pvalue['category_id'];
			}
			if($id !=array()){
				$subcategory = $this->query("SELECT * FROM `oc_category` c LEFT JOIN  `oc_category_description` cd  ON cd.`category_id`= c.`category_id` LEFT JOIN `oc_category_path`cp ON c.`category_id`= cp.`category_id` WHERE cp.`level`= 1 AND c.`status` = '1' AND c.`category_id` NOT IN ( ".implode(',', $id)." ) ORDER BY sort_order ASC",$this->conn);
			} else {
				$subcategory = $this->query("SELECT * FROM `oc_category` c LEFT JOIN  `oc_category_description` cd  ON cd.`category_id`= c.`category_id` LEFT JOIN `oc_category_path`cp ON c.`category_id`= cp.`category_id` WHERE cp.`level`= 1 AND c.`status` = '1' ORDER BY sort_order ASC",$this->conn);
			}
			if ($subcategory->num_rows > 0) {
				foreach($subcategory->rows as $skey => $svalue){
					$data['categorys'][$svalue['category_id']] = array(
						'subcategory_id' => $svalue['category_id'], 
						'name' => html_entity_decode($svalue['name']),
						'image' =>'https://taazitokari.com/'.'image/'.$svalue['image']
					);
				}

				$subcategory = $this->query("SELECT * FROM `oc_category` c LEFT JOIN  `oc_category_description` cd  ON cd.`category_id`= c.`category_id` LEFT JOIN `oc_category_path`cp ON c.`category_id`= cp.`category_id` WHERE cp.`level`= 2 AND c.`status` = '1' ",$this->conn);
				if ($subcategory->num_rows > 0) {
					foreach($subcategory->rows as $skey => $svalue){
						if(isset($data['categorys'][$svalue['parent_id']])){
							unset($data['categorys'][$svalue['parent_id']]);
						}
						$data['categorys'][$svalue['category_id']] = array(
							'subcategory_id' => $svalue['category_id'], 
							'name' => html_entity_decode($svalue['name']),
							'image' =>'https://taazitokari.com/'.'image/'.$svalue['image']
						);
					}
				}
				
				$data['category'] = array();
				foreach($data['categorys'] as $ckey => $cvalue){
					$data['category'][] = $cvalue;
				}					
			}
			$data['featured'] = array();
			$data['bestseller'] = array();
			$data['result_type'] = 'item';
		}
		$special_status = $this->query("SELECT * FROM `oc_setting` WHERE `key`= 'config_special_offer'",$this->conn)->row;
		$data['special_status'] = $special_status['value'];
		if(isset($data['item_datas']) || isset($data['category'])){
			$data['success'] = 1;
		} else {
			$data['success'] = 0;
		}
		$data['banner_image'] = BANNER_IMAGE;
		$data['latest_app_version'] = LATEST_APP_VERSION;
		$data['version_change_image'] = VERSION_CHANGE_IMAGE;
		$data['app_link'] = APP_LINK;
		// echo '<pre>';
		// print_r($data);
		// exit;
		return $data;
	}
	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>