<?php 
date_default_timezone_set("Asia/Kolkata");
require_once('config.php');

$referral = new referral();
$referral->main();

class referral {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function main(){
		$customers = $this->query("SELECT `customer_id` FROM `oc_customer` WHERE firstname != '' AND telephone != '' ",$this->conn)->rows;
		foreach ($customers as $customer) {
			$orders = $this->query("SELECT `order_id` FROM `oc_order` WHERE customer_id = '".$customer['customer_id']."' AND order_status_id = 5 AND DATE(date_added) >= '2020-08-23' ",$this->conn)->rows;
			foreach ($orders as $order) {
				$order_total = $this->query("SELECT `value` FROM `oc_order_total` WHERE order_id = '".$order['order_id']."' AND code = 'total' ",$this->conn)->row['value'];
				if ($order_total >= 1000 && $order_total < 2000) {
					$credit_point = 50;
					$this->query("UPDATE `oc_customer` SET referral_credits = (referral_credits + " . (float)$credit_point . ") WHERE `customer_id` = '".$customer['customer_id']."' ",$this->conn);
				}

				if ($order_total >= 2000 && $order_total < 3000) {
					$credit_point = 100;
					$this->query("UPDATE `oc_customer` SET referral_credits = (referral_credits + " . (float)$credit_point . ") WHERE `customer_id` = '".$customer['customer_id']."' ",$this->conn);
				}

				if ($order_total >= 3000 && $order_total < 4000) {
					$credit_point = 175;
					$this->query("UPDATE `oc_customer` SET referral_credits = (referral_credits + " . (float)$credit_point . ") WHERE `customer_id` = '".$customer['customer_id']."' ",$this->conn);
				}

				if ($order_total >= 4000 && $order_total < 5000) {
					$credit_point = 275;
					$this->query("UPDATE `oc_customer` SET referral_credits = (referral_credits + " . (float)$credit_point . ") WHERE `customer_id` = '".$customer['customer_id']."' ",$this->conn);
				}

				if ($order_total >= 5000) {
					$credit_point = 375;
					$this->query("UPDATE `oc_customer` SET referral_credits = (referral_credits + " . (float)$credit_point . ") WHERE `customer_id` = '".$customer['customer_id']."' ",$this->conn);
				}
			} 
		}
		echo "done";
	}
}
 ?>