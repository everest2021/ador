<?php
date_default_timezone_set("Asia/Kolkata");
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
//$value = $Itemapi->getitem($datas);
$value = $Itemapi->getcategory($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
		$this->conn->set_charset("utf8");
		$this->conn->query("SET SQL_MODE = ''");
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getcategory($data = array()){
		// echo'<pre>';
		// print_r($data);
		// exit;

		if(!isset($data['currentusercart'])){
			$data['currentusercart'] = array();
			$data['total_items'] = (int)0;
			$data['total_price'] = (int)0;
		}
		if(isset($data['currentusercart'])){
			$total_items = 0;
			$grand_totals = 0;
			$data['total_items'] = 0;
			$data['total_price'] = 0;
			foreach($data['currentusercart'] as $key => $value){
				$prices = $this->query("SELECT * FROM `oc_item` WHERE item_code = '".$key."' ",$this->conn);
				if ($prices->num_rows > 0) {
					$price = $prices->row['rate_1'];
					$gst_id = $prices->row['vat'];
				} else {
					$price = 0;
					$gst_id = 0;
				}

				$gst_tax = $this->query("SELECT tax_value FROM oc_tax WHERE id = '" . $gst_id . "'", $this->conn);
				if ($gst_tax->num_rows > 0) {
					$gst = $gst_tax->row['tax_value'];
				} else {
					$gst = 0;
				}

				$total_items += $value;
				$total_price = $value * $price;
				$gst_val = ($total_price * $gst)/100;
				$grand_total = $total_price + $gst_val;
				$grand_totals += $grand_total;
			}
			$data['total_items'] = $total_items;
			$data['total_price'] = $grand_totals;
		}

		$data['item_datas'] = array();
		$data['success'] = 0;
		
		$products = "SELECT * FROM `oc_item` WHERE 1=1 ";
		
		if (isset($data['keyword']) && $data['keyword'] != '') {
			$products .= " AND item_name LIKE '%" . $data['keyword'] . "%' ";
		}

		$results = $this->query($products,$this->conn)->rows;

		if ($results) {
			foreach($results as $ckey => $c3value){

				if(isset($data['currentusercart'][$c3value['item_code']])){
					$quantity = $data['currentusercart'][$c3value['item_code']];
				} else {
					$quantity = (int)0;
				}

				$item_datas = $this->query("SELECT vat FROM oc_item WHERE item_code = '".$c3value['item_code']."' ", $this->conn);
				if ($item_datas->num_rows > 0) {
					$gst_id = $item_datas->row['vat'];
				} else {
					$gst_id = 0;
				}

				$gst_tax = $this->query("SELECT tax_value FROM oc_tax WHERE id = '" . $gst_id . "'", $this->conn);
				if ($gst_tax->num_rows > 0) {
					$gst = $gst_tax->row['tax_value'];
				} else {
					$gst = 0;
				}

				$gst_val = ($c3value['rate_1'] * $gst)/100;
				$total = $c3value['rate_1'] + $gst_val;

				$data['item_datas'][] = array(
					'product_id' => $c3value['item_code'],
					'name' => html_entity_decode($c3value['item_name']),
					'name' => html_entity_decode($c3value['item_name']),
					'price' => (floatval($c3value['rate_1'])),
					'total' => (floatval($total)),
					'quantity' => $quantity,
					'image' => DIR_BASE,
				);
			}
			$data['success'] = 1;
		}

		return $data;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>