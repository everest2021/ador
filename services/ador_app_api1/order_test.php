<html>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<head>
		<title>Order Now</title>
		<meta property="og:url" content="https://taazitokari.com/order.php" />
		<meta property="og:image" content="https://taazitokari.com/image/catalog/logo/logo3.png">
		<link type="text/css" href="order.css" rel="stylesheet" media="screen" />	
		<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	</head>
	<body>
		<div class="mainDiv">
			<img src="https://taazitokari.com/image/catalog/logo/logo3.png" style="width:97%" />	
			
			<h1>
		  		Order Now
			</h1>
			<div style="text-align: center; margin-bottom: 12px;">
				<a style="padding: 10px;" class="formButton" href="https://taazitokari.com/view.php">Track Previous Order</a>
			</div>
		</div>
		<div class="mainDiv">
			<form action="order_test_insert.php" id="order_form" method="post">

				<div>
					<div style="width:80%; float:left; margin-left:20%; color:white; margin-bottom:20px">
						Delivery On <i>27th April 2020</i> tentatively
						<br><br>
						
					</div>
					<div style="display:none; width:80%; float:left; margin-left:5%; color:white; margin-bottom:20px">
						<input type="checkbox" id="express_charge" name="express_charge" value="1">
			  			Same day Express Delivery Rs 100. <br> Note: Same day delivery if ordered before 3 pm
					</div>
					
					<div style=" width:80%; float:left; margin-left:5%; color:white; margin-bottom:20px">
						<input onclick="click_function()" type="checkbox" id="gift_box" name="gift_box" value="1">
						<!-- <input type="checkbox" id="gift_box" name="gift_box" value="1"> -->
			  			Gift Box
			  			<div id="gift" style="display: none;">
			  				<p> please enter the details of the person you willing to send this order to </p>
			  			</div>
					</div>
				</div>

				<div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg16" src="https://taazitokari.com/fish/mango.jpeg" alt="Fruits" style="width:100%;max-width:150px" />
						<img id="myImgLarge16" src="https://taazitokari.com/fish/mango_description.png" style="display:none;" />
						<div id="myModal16" class="modal">
						  <span class="close16">&times;</span>
						  <img class="modal-content" id="img01_16">
						  <div id="caption16"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Devgad Alphonso<br>
							1 Dozen Pack
							<select id="mango_select" name="items[mango]" class="formStyle" style="width:60%; margin-top: 10px; font-size:14px;">
								<!--<option value='0'>No Stock</option>-->
								<option value='0'>Qty</option>
								<option value='2'>2 (Rs 1100)</option>
								<option value='3'>3 (Rs 1650)</option>
								<option value='4'>4 (Rs 2200)</option>
							</select>
						</div>
					</div>

					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg17" src="https://taazitokari.com/fish/halwa.png" alt="Halwa" style="width:100%;max-width:150px" />
						<img id="myImgLarge17" src="https://taazitokari.com/fish/halwa.png" style="display:none;" />
						<div id="myModal17" class="modal">
						  <span class="close17">&times;</span>
						  <img class="modal-content" id="img01_17">
						  <div id="caption17"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Black Pomfret (Halwa) <br/>2 Kg Pack<br/>
							<select id="halwa3_select" name="items[halwa3]" class="formStyle" style="width:60%; margin-top: 5px; font-size:14px;">
								<!--<option value='0'>No Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1 (Rs 1300)</option>
							</select>
						</div>
					</div>
				</div>


				<div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg18" src="https://taazitokari.com/fish/bangda.png" alt="Bangda" style="width:100%;max-width:150px" />
						<img id="myImgLarge18" src="https://taazitokari.com/fish/bangda.png" style="display:none;" />
						<div id="myModal18" class="modal">
						  <span class="close18">&times;</span>
						  <img class="modal-content" id="img01_18">
						  <div id="caption18"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Indian Mackerel Bangda<br>
							1 Kg (6-8 pcs)
							<select id="bangda_select" name="items[bangda]" class="formStyle" style="width:60%; margin-top: 10px; font-size:14px;">
								<!--<option value='0'>No Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1 (Rs 350)</option>
								<option value='2'>2 (Rs 700)</option>
								
							</select>
						</div>
					</div>

					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg19" src="https://taazitokari.com/fish/kapri.png" alt="Kapri" style="width:100%;max-width:150px" />
						<img id="myImgLarge19" src="https://taazitokari.com/fish/kapri.png" style="display:none;" />
						<div id="myModal19" class="modal">
						  <span class="close19">&times;</span>
						  <img class="modal-content" id="img01_19">
						  <div id="caption19"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Kapri Pomfret <br/>1 Kg (4-5 pcs)<br/>
							<select id="kapri_select" name="items[kapri]" class="formStyle" style="width:60%; margin-top: 5px; font-size:14px;">
								<!--<option value='0'>No Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1 (Rs 800)</option>
								<option value='2'>2 (Rs 1600)</option>
							</select>
						</div>
					</div>
				</div>


				<div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg" src="https://taazitokari.com/fish/pomfret.png" alt="Pomfret" style="width:100%;max-width:150px" />
						<img id="myImgLarge" src="https://taazitokari.com/fish/pomfret.png" style="display:none;" />
						<div id="myModal" class="modal">
						  <span class="close">&times;</span>
						  <img class="modal-content" id="img01">
						  <div id="caption"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Silver pomfret <br/>1 kg(10 to 15 Pcs)<br/>
							<select id="pomfret_select" name="items[pomfret]" class="formStyle" style="width:60%; margin-top: 5px; font-size:14px;">
								<!--<option value='0'>Out Of Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1 (Rs 450)</option>
								<option value='2'>2 (Rs 900)</option>
								<option value='3'>3 (Rs 1350)</option>
							</select>
						</div>
					</div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg2" src="https://taazitokari.com/fish/prawns.png" alt="Prawns" style="width:100%;max-width:150px" />
						<img id="myImgLarge2" src="https://taazitokari.com/fish/prawns.png" style="display:none;" />
						<div id="myModal2" class="modal">
						  <span class="close2">&times;</span>
						  <img class="modal-content" id="img01_2">
						  <div id="caption2"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Prawns Tray <br/>(44-48 pcs)<br/>
							<select id="prawns_select" name="items[prawns]" class="formStyle" style="width:60%; margin-top: 10px; font-size:14px;">
								<!--<option value='0'>Out Of Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1 (Rs 550)</option>
								<option value='2'>2 (Rs 1100)</option>
								<option value='3'>3 (Rs 1650)</option>
							</select>
						</div>
					</div>
				</div>


				
				<div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg9" src="https://taazitokari.com/fish/pomfret1.png" alt="Pomfret" style="width:100%;max-width:150px" />
						<img id="myImgLarge9" src="https://taazitokari.com/fish/pomfret1.png" style="display:none;" />
						<div id="myModal9" class="modal">
						  <span class="close9">&times;</span>
						  <img class="modal-content" id="img01_9">
						  <div id="caption9"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Pomfret No 3 <br/>1 Kg Pack<br/>4 - 5 pcs<br/>
							<select id="pomfret3_select" name="items[pomfret3]" class="formStyle" style="width:60%; margin-top: 5px; font-size:14px;">
								<!--<option value='0'>No Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1 (Rs 950)</option>
								<option value='2'>2 (Rs 1900)</option>
								<option value='3'>3 (Rs 2850)</option>
							</select>
						</div>
					</div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg10" src="https://taazitokari.com/fish/pomfret1.png" alt="Pomfret" style="width:100%;max-width:150px" />
						<img id="myImgLarge10" src="https://taazitokari.com/fish/pomfret1.png" style="display:none;" />
						<div id="myModal10" class="modal">
						  <span class="close10">&times;</span>
						  <img class="modal-content" id="img01_10">
						  <div id="caption10"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Pomfret No 4 <br/>1 Kg Pack<br/>6 - 7 pcs<br/>
							<select id="pomfret4_select" name="items[pomfret4]" class="formStyle" style="width:60%; margin-top: 5px; font-size:14px;">
								<!--<option value='0'>No Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1 (Rs 750)</option>
								<option value='2'>2 (Rs 1500)</option>
								<option value='3'>3 (Rs 2250)</option>
							</select>
						</div>
					</div>
				</div>


				<div>
					<div style="width:45%; float:left; margin-left:5%; display:none;">
						<img id="myImg3" src="https://taazitokari.com/fruitsbasket.png" alt="Fruits" style="width:100%;max-width:150px" />
						<img id="myImgLarge3" src="https://taazitokari.com/fruitboxdesc.png" style="display:none;" />
						<div id="myModal3" class="modal">
						  <span class="close3">&times;</span>
						  <img class="modal-content" id="img01_3">
						  <div id="caption3"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Fruit Box(6 type)<br>
							<select id="fruits_select" name="items[fruits]" class="formStyle" style="width:60%; margin-top: 10px; font-size:14px;">
								<option value='0'>No Stock</option>
								<!-- <option value='0'>Qty</option>
								<option value='1'>1 (Rs 425)</option>
								<option value='2'>2 (Rs 850)</option>
								<option value='3'>3 (Rs 1275)</option> -->
							</select>
						</div>
					</div>

					

					
				</div>

				<div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg13" src="https://taazitokari.com/fish/halwa.png" alt="Halwa" style="width:100%;max-width:150px" />
						<img id="myImgLarge13" src="https://taazitokari.com/fish/halwa.png" style="display:none;" />
						<div id="myModal13" class="modal">
						  <span class="close13">&times;</span>
						  <img class="modal-content" id="img01_13">
						  <div id="caption13"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Black Pomfret (Halwa) <br/>5 Kg Pack<br/>
							<select id="halwa1_select" name="items[halwa1]" class="formStyle" style="width:60%; margin-top: 5px; font-size:14px;">
								<!--<option value='0'>No Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1 (Rs 3250)</option>
							</select>
						</div>
					</div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg14" src="https://taazitokari.com/fish/halwa.png" alt="Halwa" style="width:100%;max-width:150px" />
						<img id="myImgLarge14" src="https://taazitokari.com/fish/halwa.png" style="display:none;" />
						<div id="myModal14" class="modal">
						  <span class="close14">&times;</span>
						  <img class="modal-content" id="img01_14">
						  <div id="caption14"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Black Pomfret (Halwa) <br/>10 Kg Pack<br/>
							<select id="halwa2_select" name="items[halwa2]" class="formStyle" style="width:60%; margin-top: 5px; font-size:14px;">
								<!--<option value='0'>No Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1 (Rs 6500)</option>
							</select>
						</div>
					</div>
				</div>



				



				<div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg5" src="https://taazitokari.com/fish/pomfret1.png" alt="Pomfret" style="width:100%;max-width:150px" />
						<img id="myImgLarge5" src="https://taazitokari.com/fish/pomfret1.png" style="display:none;" />
						<div id="myModal5" class="modal">
						  <span class="close5">&times;</span>
						  <img class="modal-content" id="img01_5">
						  <div id="caption5"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Pomfret No 1 <br/>10 Kg Bulk<br/>20 - 25 pcs<br/>
							<select id="pomfret1_select" name="items[pomfret1]" class="formStyle" style="width:60%; margin-top: 5px; font-size:14px;">
								<!--<option value='0'>No Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1 (Rs 14500)</option>
								<option value='2'>2 (Rs 29000)</option>
								<option value='3'>3 (Rs 43500)</option>
							</select>
						</div>
					</div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg6" src="https://taazitokari.com/fish/pomfret1.png" alt="Pomfret" style="width:100%;max-width:150px" />
						<img id="myImgLarge6" src="https://taazitokari.com/fish/pomfret1.png" style="display:none;" />
						<div id="myModal6" class="modal">
						  <span class="close6">&times;</span>
						  <img class="modal-content" id="img01_6">
						  <div id="caption6"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Pomfret No 2 <br/>10 Kg Bulk<br/>28 - 32 pcs<br/>
							<select id="pomfret2_select" name="items[pomfret2]" class="formStyle" style="width:60%; margin-top: 5px; font-size:14px;">
								<!--<option value='0'>No Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1 (Rs 12500)</option>
								<option value='2'>2 (Rs 25000)</option>
								<option value='3'>3 (Rs 37500)</option>
							</select>
						</div>
					</div>
				</div>
				


				<div style="display:none;">
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg8" src="https://taazitokari.com/appleorange.png" alt="Veg Tokari" style="width:100%;max-width:150px" />
						<img id="myImgLarge8" src="https://taazitokari.com/orangeappledesc.png" style="display:none;" />
						<div id="myModal8" class="modal">
						  <span class="close8">&times;</span>
						  <img class="modal-content" id="img01_8">
						  <div id="caption8"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Apples and Oranges<br/> 1 Kg Each<br/>
							<select id="orange_select" name="items[orange]" class="formStyle" style="width:60%; margin-top: 10px; font-size:14px;">
								<option value='0'>No Stock</option>
								<!-- <option value='0'>Qty</option>
								<option value='1'>1 (Rs 370)</option>
								<option value='2'>2 (Rs 740)</option>
								<option value='3'>3 (Rs 1110)</option> -->
							</select>
						</div>
					</div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg7" src="https://taazitokari.com/applesbasket.png" alt="Veg Tokari" style="width:100%;max-width:150px" />
						<img id="myImgLarge7" src="https://taazitokari.com/appledescription.png" style="display:none;" />
						<div id="myModal7" class="modal">
						  <span class="close7">&times;</span>
						  <img class="modal-content" id="img01_7">
						  <div id="caption7"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Turkish Apples<br/> 2 Kg Box<br/>
							<select id="apple_select" name="items[apple]" class="formStyle" style="width:60%; margin-top: 10px; font-size:14px;">
								<option value='0'>No Stock</option>
								<!-- <option value='0'>Qty</option>
								<option value='1'>1 (Rs 395)</option>
								<option value='2'>2 (Rs 790)</option>
								<option value='3'>3 (Rs 1185)</option> -->
							</select>
						</div>
					</div>
				</div>

				<div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg15" src="https://taazitokari.com/fish/chicken.png" alt="Chicken Curry Cut" style="width:100%;max-width:150px" />
						<img id="myImgLarge15" src="https://taazitokari.com/fish/chicken.png" style="display:none;" />
						<div id="myModal15" class="modal">
						  <span class="close15">&times;</span>
						  <img class="modal-content" id="img01_15">
						  <div id="caption15"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Chicken curry cut<br/> Godrej 1 Kg Pack<br/>
							<select id="chicken_select" name="items[chicken]" class="formStyle" style="width:60%; margin-top: 10px; font-size:14px;">
								<!--<option value='0'>No Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1 (Rs 285)</option>
								<option value='2'>2 (Rs 570)</option>
								<option value='3'>3 (Rs 855)</option>
							</select>
						</div>
					</div>

					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg1" src="https://taazitokari.com/fish/surmai.png" alt="Surmai" style="width:100%;max-width:150px" />
						<img id="myImgLarge1" src="https://taazitokari.com/fish/surmai.png" style="display:none;" />
						<div id="myModal1" class="modal">
						  <span class="close1">&times;</span>
						  <img class="modal-content" id="img01_1">
						  <div id="caption1"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Seer fish (Surmai) <br/>1 Kg Steak<br/>
							<select id="surmai_select" name="items[surmai]" class="formStyle" style="width:60%; margin-top: 5px; font-size:14px;">
								<option value='0'>No Stock</option>
								<!-- <option value='0'>Qty</option>
								<option value='1'>1 (Rs 650)</option>
								<option value='2'>2 (Rs 1300)</option>
								<option value='3'>3 (Rs 1950)</option> -->
							</select>
						</div>
					</div>

					<div style="display:none; width:45%; float:left; margin-left:5%;">
						<img id="myImg4" src="https://taazitokari.com/vegbasket.png" alt="Veg Tokari" style="width:100%;max-width:150px" />
						<img id="myImgLarge4" src="https://taazitokari.com/vegboxdesc.png" style="display:none;" />
						<div id="myModal4" class="modal">
						  <span class="close4">&times;</span>
						  <img class="modal-content" id="img01_4">
						  <div id="caption4"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Veg Tokari<br/>
							<select id="veg_select" name="items[veg]" class="formStyle" style="width:60%; margin-top: 10px; font-size:14px;">
								<option value='0'>No Stock</option>
								<!-- <option value='0'>Qty</option>
								<option value='1'>1 (Rs 420)</option>
								<option value='2'>2 (Rs 840)</option>
								<option value='3'>3 (Rs 1260)</option> -->
							</select>
						</div>
					</div>
				</div>

				


				<div>
					<div style="width:80%; float:left; margin-left:10%; color:white; margin-top:20px; margin-bottom:20px">
						Actual rate of Salmon (Rawas) will be calculated at Rs 650 / Kg during delivery
						<i>(Please select the option as per your requirement)</i>
					</div>
						
				</div>



				<div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg11" src="https://taazitokari.com/fish/rawas.png" alt="Rawas" style="width:100%;max-width:150px" />
						<img id="myImgLarge11" src="https://taazitokari.com/fish/rawas.png" style="display:none;" />
						<div id="myModal11" class="modal">
						  <span class="close11">&times;</span>
						  <img class="modal-content" id="img01_11">
						  <div id="caption11"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Salmon (Rawas) <br/>3 - 5 Kg Pack<br/>
							<select id="rawas1_select" name="items[rawas1]" class="formStyle" style="width:60%; margin-top: 5px; font-size:14px;">
								<!--<option value='0'>No Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1</option>
								
							</select>
						</div>
					</div>
					<div style="width:45%; float:left; margin-left:5%;">
						<img id="myImg12" src="https://taazitokari.com/fish/rawas.png" alt="Rawas" style="width:100%;max-width:150px" />
						<img id="myImgLarge12" src="https://taazitokari.com/fish/rawas.png" style="display:none;" />
						<div id="myModal12" class="modal">
						  <span class="close12">&times;</span>
						  <img class="modal-content" id="img01_12">
						  <div id="caption12"></div>
						</div>
						<div style="padding-top:10px; margin-bottom: 10px; color:white;">
							Salmon (Rawas) <br/>5 - 7 Kg Pack<br/>
							<select id="rawas2_select" name="items[rawas2]" class="formStyle" style="width:60%; margin-top: 5px; font-size:14px;">
								<!--<option value='0'>No Stock</option>-->
								<option value='0'>Qty</option>
								<option value='1'>1</option>
								
							</select>
						</div>
					</div>
				</div>



				<div>
					<div style="width:80%; float:left; margin-left:10%; color:white;">
			  			Delivery Charges Rs 30 Extra per Customer. <br>
			  			<!-- <input type="checkbox" id="express_charge" name="express_charge" value="1">
			  			Delivery Express Charges Rs 100 Extra per Customer. <br> -->
			  			<i>(Note : If ordering from same mobile number to same address delivery charges will be applied only once)</i>
			  		</div>
			  		<div style="width:80%; float:left; margin-left:10%; color:white;">
			  			For bulk box orders please call <a href="tel:7387421025">7387421025</a>
			  		</div>
			  	</div>
			    <input id="name" type="text" name="name" class="formStyle" placeholder="Name (required)" required />
			    <input id="mobile" type="number" name="mobile" class="formStyle" placeholder="Mobile (10 digits required)" maxlength="10" required /><br/>
			    <span style="display: none;color: red;" id="error_mobile" class="error"><?php echo 'Please Enter Valid Mobile Number'; ?></span>
			    <textarea id="address" name="address"class="formStyle" placeholder="Address (required)"></textarea>
			    <input type="hidden" id="building" name="building"class="formStyle" placeholder="Building Name (required)" required/>
			    <input type="hidden" id="latitude" name="latitude" />
			    <input type="hidden" id="longitude" name="longitude" />
			    <br/>
			    Please select your area correctly for quick delivery
			    <br/>
			    <br/>
			    <select id="payment_method" name="payment_method" class="formStyle" >
			    	<option value='cash on delivery'>Cash On Delivery</option>
					<option value='online payment'>Online Payment</option>
				</select>
			    <select id="city" name="city" class="formStyle" >
			    	<option value='0'>Please Select</option>
					<option value='100 ft road'>100 ft road</option>
					<option value='Achole Road'>Achole Road</option>
					<option value='Agashi'>Agashi</option>
					<option value='Ambadi Road'>Ambadi Road</option>
					<option value='Arnala'>Arnala</option>
					<option value='Bangli'>Bangli</option>
					<option value='Barampur'>Barampur</option>
					<option value='bhabola'>bhabola</option>
					<option value='Bhuigaon'>Bhuigaon</option>
					<option value='Bolinj'>Bolinj</option>
					<option value='Chulna'>Chulna</option>
					<option value='Dewanman'>Dewanman</option>
					<option value='Evershine city'>Evershine city</option>
					<option value='Gass'>Gass</option>
					<option value='Giriz'>Giriz</option>
					<option value='Global city'>Global city</option>
					<option value='Gokhivare'>Gokhivare</option>
					<option value='Gomes aali'>Gomes aali</option>
					<option value='Holi'>Holi</option>
					<option value='K T village, Vasai west'>K T village, Vasai west</option>
					<option value='Khochiwade '>Khochiwade </option>
					<option value='koliwada'>koliwada</option>
					<option value='Manickpur'>Manickpur</option>
					<option value='Manvelpada'>Manvelpada</option>
					<option value='Merces'>Merces</option>
					<option value='Mulgaon'>Mulgaon</option>
					<option value='Naigaon east'>Naigaon east</option>
					<option value='Naigaon west'>Naigaon west</option>
					<option value='Nalla'>Nalla</option>
					<option value='Nallasopara East'>Nallasopara East</option>
					<option value='Nallasopara west'>Nallasopara west</option>
					<option value='Nanbhat'>Nanbhat</option>
					<option value='Nandakhal'>Nandakhal</option>
					<option value='Nirmal'>Nirmal</option>
					<option value='Om Nagar'>Om Nagar</option>
					<option value='Papdy'>Papdy</option>
					<option value='Parnaka'>Parnaka</option>
					<option value='Phoolpada'>Phoolpada</option>
					<option value='Rajodi'>Rajodi</option>
					<option value='Ramedy'>Ramedy</option>
					<option value='Sagarshet'>Sagarshet</option>
					<option value='Sai nager'>Sai nager</option>
					<option value='Samel Pada'>Samel Pada</option>
					<option value='Sandor'>Sandor</option>
					<option value='satpala'>satpala</option>
					<option value='Shastri Nagar'>Shastri Nagar</option>
					<option value='Sopara'>Sopara</option>
					<option value='Sriprasta'>Sriprasta</option>
					<option value='Stella'>Stella</option>
					<option value='Suncity'>Suncity</option>
					<option value='Tamtalao'>Tamtalao</option>
					<option value='Tarkhad'>Tarkhad</option>
					<option value='Umelman'>Umelman</option>
					<option value='umralla'>umralla</option>
					<option value='Vasai West'>Vasai West</option>
					<option value='Vasaigaon'>Vasaigaon</option>
					<option value='Vasalai'>Vasalai</option>
					<option value='Vasant Nagari'>Vasant Nagari</option>
					<option value='Virar east'>Virar east</option>
					<option value='Virar West'>Virar West</option>
					<option value='Wagholi'>Wagholi</option>
					<option value='waliv'>waliv</option>
				</select>
			    <input type="button" id="submit_fun_id" onclick="submit_fun()" class="formButton" value="Confirm" />
		  	</form>
		</div>

		<script>

		function submit_fun(){
			pomfret_select = document.getElementById('pomfret_select').value;
			pomfret1_select = document.getElementById('pomfret1_select').value;
			pomfret2_select = document.getElementById('pomfret2_select').value;
			pomfret3_select = document.getElementById('pomfret3_select').value;
			pomfret4_select = document.getElementById('pomfret4_select').value;
			surmai_select = document.getElementById('surmai_select').value;
			//surmai_select = 0;
			prawns_select = document.getElementById('prawns_select').value;
			fruits_select = document.getElementById('fruits_select').value;
			//fruits_select = 0;
			veg_select = document.getElementById('veg_select').value;
			//veg_select = 0;
			apple_select = document.getElementById('apple_select').value;
			orange_select = document.getElementById('orange_select').value;
			rawas1_select = document.getElementById('rawas1_select').value;
			rawas2_select = document.getElementById('rawas2_select').value;
			halwa1_select = document.getElementById('halwa1_select').value;
			halwa2_select = document.getElementById('halwa2_select').value;

			chicken_select = document.getElementById('chicken_select').value;

			mango_select = document.getElementById('mango_select').value;

			halwa3_select = document.getElementById('halwa3_select').value;
			bangda_select = document.getElementById('bangda_select').value;
			kapri_select = document.getElementById('kapri_select').value;


			if(chicken_select > 0 && pomfret_select == 0 && pomfret1_select == 0 && pomfret2_select == 0 && pomfret3_select == 0 && pomfret4_select == 0 && surmai_select == 0 && prawns_select == 0 && fruits_select == 0 && veg_select == 0 && apple_select == 0 && orange_select == 0 && rawas1_select == 0 && rawas2_select == 0 && halwa1_select == 0 && halwa2_select == 0 && mango_select == 0 && halwa3_select == 0 && bangda_select == 0 && kapri_select == 0){
				alert("Please select one more item");
				return false;
			}

			name = document.getElementById('name').value;
			express_charge = document.getElementById('express_charge').value;
			mobile = document.getElementById('mobile').value;
			var n = mobile.length;
			if(n > 10) {
				alert('Please enter correct mobile number');
					return false;		
			}

			address = document.getElementById('address').value;
			payment_method = document.getElementById('payment_method').value;
			building = document.getElementById('building').value;
			city = document.getElementById('city').value;
			if(pomfret_select > 0 || pomfret2_select > 0 || pomfret1_select > 0 || pomfret3_select > 0 || pomfret4_select > 0 || surmai_select > 0 || prawns_select > 0 || fruits_select > 0 || veg_select > 0 || apple_select > 0 || orange_select > 0 || rawas1_select > 0 || rawas2_select > 0 || halwa1_select > 0 || halwa2_select > 0 || chicken_select > 0 || mango_select > 0 || halwa3_select > 0 || bangda_select > 0 || kapri_select > 0){
				if(name != '' && mobile != '' && address != '' && payment_method != '' && city != ''){
					document.getElementById("order_form").submit();
					var el = document.getElementById('submit_fun_id');
					el.removeAttribute('onclick');
				} else {
					alert('Please Enter the Required Data');
					return false;		
				}
			} else {
				alert('Please Select the Quantity for One of the Product');
				return false;
			}
		}


		// Get the modal
		var modal = document.getElementById("myModal");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img = document.getElementById("myImg");
		var imgLarge = document.getElementById("myImgLarge");
		var modalImg = document.getElementById("img01");
		var captionText = document.getElementById("caption");
		img.onclick = function(){
		  modal.style.display = "block";
		  modalImg.src = imgLarge.src;
		  captionText.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

		// When the user clicks on <span> (x), close the modal
		span.onclick = function() { 
		  modal.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal1 = document.getElementById("myModal1");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img1 = document.getElementById("myImg1");
		var imgLarge1 = document.getElementById("myImgLarge1");
		var modalImg1 = document.getElementById("img01_1");
		var captionText1 = document.getElementById("caption1");
		img1.onclick = function(){
		  modal1.style.display = "block";
		  modalImg1.src = imgLarge1.src;
		  captionText1.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span1 = document.getElementsByClassName("close1")[0];

		// When the user clicks on <span> (x), close the modal
		span1.onclick = function() { 
		  modal1.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal2 = document.getElementById("myModal2");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img2 = document.getElementById("myImg2");
		var imgLarge2 = document.getElementById("myImgLarge2");
		var modalImg2 = document.getElementById("img01_2");
		var captionText2 = document.getElementById("caption2");
		img2.onclick = function(){
		  modal2.style.display = "block";
		  modalImg2.src = imgLarge2.src;
		  captionText2.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span2 = document.getElementsByClassName("close2")[0];

		// When the user clicks on <span> (x), close the modal
		span2.onclick = function() { 
		  modal2.style.display = "none";
		}
		</script>


		<script>
		// Get the modal
		var modal3 = document.getElementById("myModal3");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img3 = document.getElementById("myImg3");
		var imgLarge3 = document.getElementById("myImgLarge3");
		var modalImg3 = document.getElementById("img01_3");
		var captionText3 = document.getElementById("caption3");
		img3.onclick = function(){
		  modal3.style.display = "block";
		  modalImg3.src = imgLarge3.src;
		  captionText3.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span3 = document.getElementsByClassName("close3")[0];

		// When the user clicks on <span> (x), close the modal
		span3.onclick = function() { 
		  modal3.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal4 = document.getElementById("myModal4");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img4 = document.getElementById("myImg4");
		var imgLarge4 = document.getElementById("myImgLarge4");
		var modalImg4 = document.getElementById("img01_4");
		var captionText4 = document.getElementById("caption4");
		img4.onclick = function(){
		  modal4.style.display = "block";
		  modalImg4.src = imgLarge4.src;
		  captionText4.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span4 = document.getElementsByClassName("close4")[0];

		// When the user clicks on <span> (x), close the modal
		span4.onclick = function() { 
		  modal4.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal5 = document.getElementById("myModal5");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img5 = document.getElementById("myImg5");
		var imgLarge5 = document.getElementById("myImgLarge5");
		var modalImg5 = document.getElementById("img01_5");
		var captionText5 = document.getElementById("caption5");
		img5.onclick = function(){
		  modal5.style.display = "block";
		  modalImg5.src = imgLarge5.src;
		  captionText5.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span5 = document.getElementsByClassName("close5")[0];

		// When the user clicks on <span> (x), close the modal
		span5.onclick = function() { 
		  modal5.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal6 = document.getElementById("myModal6");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img6 = document.getElementById("myImg6");
		var imgLarge6 = document.getElementById("myImgLarge6");
		var modalImg6 = document.getElementById("img01_6");
		var captionText6 = document.getElementById("caption6");
		img6.onclick = function(){
		  modal6.style.display = "block";
		  modalImg6.src = imgLarge6.src;
		  captionText6.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span6 = document.getElementsByClassName("close6")[0];

		// When the user clicks on <span> (x), close the modal
		span6.onclick = function() { 
		  modal6.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal7 = document.getElementById("myModal7");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img7 = document.getElementById("myImg7");
		var imgLarge7 = document.getElementById("myImgLarge7");
		var modalImg7 = document.getElementById("img01_7");
		var captionText7 = document.getElementById("caption7");
		img7.onclick = function(){
		  modal7.style.display = "block";
		  modalImg7.src = imgLarge7.src;
		  captionText7.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span7 = document.getElementsByClassName("close7")[0];

		// When the user clicks on <span> (x), close the modal
		span7.onclick = function() { 
		  modal7.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal8 = document.getElementById("myModal8");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img8 = document.getElementById("myImg8");
		var imgLarge8 = document.getElementById("myImgLarge8");
		var modalImg8 = document.getElementById("img01_8");
		var captionText8 = document.getElementById("caption8");
		img8.onclick = function(){
		  modal8.style.display = "block";
		  modalImg8.src = imgLarge8.src;
		  captionText8.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span8 = document.getElementsByClassName("close8")[0];

		// When the user clicks on <span> (x), close the modal
		span8.onclick = function() { 
		  modal8.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal9 = document.getElementById("myModal9");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img9 = document.getElementById("myImg9");
		var imgLarge9 = document.getElementById("myImgLarge9");
		var modalImg9 = document.getElementById("img01_9");
		var captionText9 = document.getElementById("caption9");
		img9.onclick = function(){
		  modal9.style.display = "block";
		  modalImg9.src = imgLarge9.src;
		  captionText9.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span9 = document.getElementsByClassName("close9")[0];

		// When the user clicks on <span> (x), close the modal
		span9.onclick = function() { 
		  modal9.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal10 = document.getElementById("myModal10");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img10 = document.getElementById("myImg10");
		var imgLarge10 = document.getElementById("myImgLarge10");
		var modalImg10 = document.getElementById("img01_10");
		var captionText10 = document.getElementById("caption10");
		img10.onclick = function(){
		  modal10.style.display = "block";
		  modalImg10.src = imgLarge10.src;
		  captionText10.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span10= document.getElementsByClassName("close10")[0];

		// When the user clicks on <span> (x), close the modal
		span10.onclick = function() { 
		  modal10.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal11 = document.getElementById("myModal11");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img11 = document.getElementById("myImg11");
		var imgLarge11 = document.getElementById("myImgLarge11");
		var modalImg11 = document.getElementById("img01_11");
		var captionText11 = document.getElementById("caption11");
		img11.onclick = function(){
		  modal11.style.display = "block";
		  modalImg11.src = imgLarge11.src;
		  captionText11.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span11= document.getElementsByClassName("close11")[0];

		// When the user clicks on <span> (x), close the modal
		span11.onclick = function() { 
		  modal11.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal12 = document.getElementById("myModal12");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img12 = document.getElementById("myImg12");
		var imgLarge12 = document.getElementById("myImgLarge12");
		var modalImg12 = document.getElementById("img01_12");
		var captionText12 = document.getElementById("caption12");
		img12.onclick = function(){
		  modal12.style.display = "block";
		  modalImg12.src = imgLarge12.src;
		  captionText12.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span12= document.getElementsByClassName("close12")[0];

		// When the user clicks on <span> (x), close the modal
		span12.onclick = function() { 
		  modal12.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal13 = document.getElementById("myModal13");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img13 = document.getElementById("myImg13");
		var imgLarge13 = document.getElementById("myImgLarge13");
		var modalImg13 = document.getElementById("img01_13");
		var captionText13 = document.getElementById("caption13");
		img13.onclick = function(){
		  modal13.style.display = "block";
		  modalImg13.src = imgLarge13.src;
		  captionText13.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span13= document.getElementsByClassName("close13")[0];

		// When the user clicks on <span> (x), close the modal
		span13.onclick = function() { 
		  modal13.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal14 = document.getElementById("myModal14");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img14 = document.getElementById("myImg14");
		var imgLarge14 = document.getElementById("myImgLarge14");
		var modalImg14 = document.getElementById("img01_14");
		var captionText14 = document.getElementById("caption14");
		img14.onclick = function(){
		  modal14.style.display = "block";
		  modalImg14.src = imgLarge14.src;
		  captionText14.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span14= document.getElementsByClassName("close14")[0];

		// When the user clicks on <span> (x), close the modal
		span14.onclick = function() { 
		  modal14.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal15 = document.getElementById("myModal15");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img15 = document.getElementById("myImg15");
		var imgLarge15 = document.getElementById("myImgLarge15");
		var modalImg15 = document.getElementById("img01_15");
		var captionText15 = document.getElementById("caption15");
		img15.onclick = function(){
		  modal15.style.display = "block";
		  modalImg15.src = imgLarge15.src;
		  captionText15.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span15= document.getElementsByClassName("close15")[0];

		// When the user clicks on <span> (x), close the modal
		span15.onclick = function() { 
		  modal15.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal16 = document.getElementById("myModal16");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img16 = document.getElementById("myImg16");
		var imgLarge16 = document.getElementById("myImgLarge16");
		var modalImg16 = document.getElementById("img01_16");
		var captionText16 = document.getElementById("caption16");
		img16.onclick = function(){
		  modal16.style.display = "block";
		  modalImg16.src = imgLarge16.src;
		  captionText16.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span16= document.getElementsByClassName("close16")[0];

		// When the user clicks on <span> (x), close the modal
		span16.onclick = function() { 
		  modal16.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal17 = document.getElementById("myModal17");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img17 = document.getElementById("myImg17");
		var imgLarge17 = document.getElementById("myImgLarge17");
		var modalImg17 = document.getElementById("img01_17");
		var captionText17 = document.getElementById("caption17");
		img17.onclick = function(){
		  modal17.style.display = "block";
		  modalImg17.src = imgLarge17.src;
		  captionText17.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span17= document.getElementsByClassName("close17")[0];

		// When the user clicks on <span> (x), close the modal
		span17.onclick = function() { 
		  modal17.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal18 = document.getElementById("myModal18");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img18 = document.getElementById("myImg18");
		var imgLarge18 = document.getElementById("myImgLarge18");
		var modalImg18 = document.getElementById("img01_18");
		var captionText18 = document.getElementById("caption18");
		img18.onclick = function(){
		  modal18.style.display = "block";
		  modalImg18.src = imgLarge18.src;
		  captionText18.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span18= document.getElementsByClassName("close18")[0];

		// When the user clicks on <span> (x), close the modal
		span18.onclick = function() { 
		  modal18.style.display = "none";
		}
		</script>

		<script>
		// Get the modal
		var modal19 = document.getElementById("myModal19");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img19 = document.getElementById("myImg19");
		var imgLarge19 = document.getElementById("myImgLarge19");
		var modalImg19 = document.getElementById("img01_19");
		var captionText19 = document.getElementById("caption19");
		img19.onclick = function(){
		  modal19.style.display = "block";
		  modalImg19.src = imgLarge19.src;
		  captionText19.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span19= document.getElementsByClassName("close19")[0];

		// When the user clicks on <span> (x), close the modal
		span19.onclick = function() { 
		  modal19.style.display = "none";
		}
		</script>


		<script>
			(function() {
			   // your page initialization code here
			   // the DOM will be available here
			   getLocation();
			})();
			function getLocation() {
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(showPosition);
				}
			}
			function showPosition(position) {
				latitude = position.coords.latitude;				
				longitude = position.coords.longitude;				
				document.getElementById('latitude').value = latitude;
				document.getElementById('longitude').value = longitude;
			}
		</script>

		<script type="text/javascript">
		  $("#mobile").keyup(function()  {
		            if(this.value.length ==10){
		                $('#error_mobile').hide();
		                
		            } else {
		                $('#error_mobile').show();
		            }
		        });
		  
		</script>
		<!-- <script>
		function click_function(){
			$('#gift').show();			
;			var selectobject = document.getElementById("payment_method");
			for (var i=0; i<selectobject.length; i++) {
   				if (selectobject.options[i].value == 'cash on delivery') {
       				selectobject.remove(i);
  				}
			}
		}
		</script> -->
		<script type="text/javascript">
		function click_function(){
			var chkexpress = document.getElementById("gift_box");
    		if (chkexpress.checked) {
    			$('#gift').show();
    			var selectobject = document.getElementById("payment_method");
				for (var i=0; i<selectobject.length; i++) {
		    		if (selectobject.options[i].value == 'cash on delivery') {
		        		selectobject.remove(i);
		    		}
				}
				let element = document.getElementById('payment_method');
				element.value = '0';
    		} else {
    			$('#gift').hide();
    			var x = document.getElementById("payment_method");
				var option = document.createElement("option");
				option.text = "Cash On Delivery";
				x.add(option);
   			}
		}
		</script>
	</body>
</html>