<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		if(!isset($data['postcode'])){
			$data['postcode'] = '401203';
		}
		
		$area_datas = $this->query("SELECT `station`, `station_n` FROM `oc_pincode` WHERE `pincode` = '".$this->escape($data['postcode'], $this->conn)."' ", $this->conn)->rows;
		foreach($area_datas as $akey => $avalue) {
			$result['area_datas'][] = array(
				'key' => $avalue['station'],
				'value' => $avalue['station_n'],
			);
		}			
			
		if(isset($result['area_datas']) && !empty($result['area_datas'])){
			$result['success'] = 1;
		} else {
			$result['success'] = 0;
		}
		
		return $result;
	}
	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>