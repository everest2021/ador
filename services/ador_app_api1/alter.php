CREATE TABLE `db_restaurant`.`oc_customer_app` ( `customer_id` INT(11) NOT NULL AUTO_INCREMENT , `telephone` VARCHAR(32) NOT NULL , `salt` VARCHAR(9) NOT NULL , `customer_group_id` INT(11) NOT NULL , `status` INT(11) NOT NULL , `approved` INT(11) NOT NULL , `date_added` DATE NOT NULL DEFAULT '0000-00-00' , `otp` INT(11) NOT NULL , PRIMARY KEY (`customer_id`)) ENGINE = InnoDB;

CREATE TABLE `db_restaurant`.`oc_address_app` ( `address_id` INT(11) NOT NULL AUTO_INCREMENT , `customer_id` INT(11) NOT NULL , `city` VARCHAR(255) NOT NULL , `postcode` INT(255) NOT NULL , `area` INT(255) NOT NULL , PRIMARY KEY (`address_id`)) ENGINE = InnoDB;

ALTER TABLE `oc_customer_app` ADD `is_logged_in` INT(11) NOT NULL AFTER `otp`;

ALTER TABLE `oc_customer_app`  ADD `firstname` VARCHAR(255) NOT NULL  AFTER `customer_id`,  ADD `lastname` VARCHAR(255) NOT NULL  AFTER `firstname`,  ADD `email` VARCHAR(255) NOT NULL  AFTER `lastname`;

ALTER TABLE `oc_address_app` ADD `address_1` VARCHAR(11) NOT NULL AFTER `customer_id`, ADD `address_2` VARCHAR(11) NOT NULL AFTER `address_1`;

ALTER TABLE `oc_customer_app` ADD `information_field` INT(11) NOT NULL AFTER `date_added`;

ALTER TABLE `oc_address_app` ADD `firstname` VARCHAR(255) NOT NULL AFTER `area`, ADD `lastname` VARCHAR(255) NOT NULL AFTER `firstname`, ADD `country_id` INT(11) NOT NULL AFTER `lastname`, ADD `zone_id` INT(11) NOT NULL AFTER `country_id`;

ALTER TABLE `oc_address_app` CHANGE `area` `area` VARCHAR(255) NOT NULL;

CREATE TABLE `db_restaurant`.`oc_orders_app` ( `order_id` INT(11) NOT NULL AUTO_INCREMENT , `cust_id` INT(11) NOT NULL , `tot_qty` DECIMAL(20,6) NOT NULL , `grand_tot` DECIMAL(20,6) NOT NULL , `order_time` TIME NOT NULL DEFAULT '00:00:00' , `order_date` DATE NOT NULL DEFAULT '0000-00-00' , `address_1` VARCHAR(255) NOT NULL , `address_2` VARCHAR(255) NOT NULL , `city` VARCHAR(50) NOT NULL , `postcode` INT(20) NOT NULL , `area` VARCHAR(255) NOT NULL , PRIMARY KEY (`order_id`)) ENGINE = InnoDB;

CREATE TABLE `db_restaurant`.`oc_order_item_app` ( `item_app_id` INT(11) NOT NULL AUTO_INCREMENT , `order_id` INT(11) NOT NULL , `item_id` INT(11) NOT NULL , `item` VARCHAR(255) NOT NULL , `qty` INT(11) NOT NULL , `price` DECIMAL(20,6) NOT NULL , PRIMARY KEY (`item_app_id`)) ENGINE = InnoDB;

ALTER TABLE `oc_orders_app` CHANGE `tot_qty` `tot_qty` INT(11) NOT NULL;

ALTER TABLE `oc_orders_app` ADD `total` INT(11) NOT NULL AFTER `tot_qty`;

ALTER TABLE `oc_order_item_app` ADD `special_notes` VARCHAR(255) NOT NULL AFTER `item`;

ALTER TABLE `oc_order_item_app` ADD `total` DECIMAL(20,6) NOT NULL AFTER `price`;

ALTER TABLE `oc_order_item_app` ADD `accept_status` INT(11) NOT NULL AFTER `item`;

