<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
		$this->conn->set_charset("utf8");
		$this->conn->query("SET SQL_MODE = ''");
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getProduct($product_id, $itemname_search, $search_product_id) {
		$sql = "SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = 1 AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = 1 AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND pr.customer_group_id = 1) AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = 1) AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = 1) AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = 1) AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = 1 AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ";
		if($itemname_search != ''){
			$sql .= " AND pd.`name` LIKE '%".$itemname_search."%' ";
		}
		if($search_product_id > 0){
			$sql .= " AND p.`product_id` = '".$search_product_id."' ";
		}
		$query = $this->query($sql, $this->conn);	
		if ($query->num_rows) {
			if(!$query->row['special']){
				$price = ($query->row['discount'] ? $query->row['discount'] : $query->row['price']);
			} else {
				$price = $query->row['special'];
			}
			return array(
				'product_id'       => $query->row['product_id'],
				'name'             => html_entity_decode($query->row['name']),
				'description'      => $query->row['description'],
				'meta_title'       => $query->row['meta_title'],
				'meta_description' => $query->row['meta_description'],
				'meta_keyword'     => $query->row['meta_keyword'],
				'tag'              => $query->row['tag'],
				'model'            => $query->row['model'],
				'sku'              => $query->row['sku'],
				'upc'              => $query->row['upc'],
				'ean'              => $query->row['ean'],
				'jan'              => $query->row['jan'],
				'isbn'             => $query->row['isbn'],
				'mpn'              => $query->row['mpn'],
				'location'         => $query->row['location'],
				'quantity'         => $query->row['quantity'],
				'stock_status'     => $query->row['stock_status'],
				'stock_status_id'  => $query->row['stock_status_id'],
				'image'            => $query->row['image'],
				'manufacturer_id'  => $query->row['manufacturer_id'],
				'manufacturer'     => $query->row['manufacturer'],
				//'price'            => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
				'price'			=> $price,
				'unit'          => $query->row['unit'],
				'special'          => $query->row['special'],
				'reward'           => $query->row['reward'],
				'points'           => $query->row['points'],
				'tax_class_id'     => $query->row['tax_class_id'],
				'date_available'   => $query->row['date_available'],
				'weight'           => $query->row['weight'],
				'weight_class_id'  => $query->row['weight_class_id'],
				'length'           => $query->row['length'],
				'width'            => $query->row['width'],
				'height'           => $query->row['height'],
				'length_class_id'  => $query->row['length_class_id'],
				'subtract'         => $query->row['subtract'],
				'rating'           => round($query->row['rating']),
				'reviews'          => $query->row['reviews'] ? $query->row['reviews'] : 0,
				'minimum'          => $query->row['minimum'],
				'sort_order'       => $query->row['sort_order'],
				'status'           => $query->row['status'],
				'date_added'       => $query->row['date_added'],
				'date_modified'    => $query->row['date_modified'],
				'viewed'           => $query->row['viewed']
			);
		} else {
			return false;
		}
	}

	public function getProductSpecials($itemname_search, $search_product_id) {
		$sql = "SELECT DISTINCT ps.product_special_id,ps.product_id,p.price, (SELECT AVG(rating) FROM oc_review r1 WHERE r1.product_id = ps.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating FROM oc_product_special ps LEFT JOIN oc_product p ON (ps.product_id = p.product_id) LEFT JOIN oc_product_description pd ON (p.product_id = pd.product_id) LEFT JOIN oc_product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 AND ps.customer_group_id = 1 AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id";
 		$product_data = array();
		$query = $this->query($sql,$this->conn);
		foreach ($query->rows as $result) {
			$product_return = $this->getProduct($result['product_id'], $itemname_search, $search_product_id);
			if($product_return){
				$product_data[$result['product_id']] = $product_return;
				$product_data[$result['product_id']]['product_special_id'] = $result['product_special_id'];
			}
		}
		return $product_data;
	}

	public function getitem($data = array()){ 
		$data['item_datas'] = array();
		if(!isset($data['itemname_search'])){
			$data['itemname_search'] = '';
		}
		if(!isset($data['product_id'])){
			$data['product_id'] = '';//'209';
		}
		if(!isset($data['user_id'])){
			$data['user_id'] = '';//'212';
		}
		if(!isset($data['currentusercart'])){
			$data['currentusercart'] = array();
		}

		$station_id = 0;
		if($data['user_id'] > 0){
			$address_data = $this->query("SELECT `postcode` FROM `oc_address` WHERE `customer_id` = '".$data['user_id']."' ",$this->conn);
			if($address_data->num_rows > 0){
				$pincode = $address_data->row['postcode'];
				$station_datas = $this->query("SELECT `station` FROM `oc_pincode` WHERE `pincode` = '".$pincode."' ",$this->conn);
				if($station_datas->num_rows > 0){
					$station_id = $station_datas->row['station'];
				}
			}
		}

		$base = 'https://taazitokari.com/image/';
		$logo = 'https://taazitokari.com/image/catalog/logo/logo6.png';
		$specials_product = $this->getProductSpecials($data['itemname_search'], $data['product_id']);
		$total_items = 0;
		$total_price = 0;
		$query_wholesaler_check = $this->query("SELECT is_wholesaler FROM oc_customer WHERE customer_id = '".$data['user_id']."' " ,$this->conn)->row;
		$wholesaler_check = 0;
		if ($query_wholesaler_check) {
			$wholesaler_check = $query_wholesaler_check['is_wholesaler'];
		}
		if($wholesaler_check == 0){
			if ($specials_product) {
				foreach ($specials_product as  $key => $result) {
					$sql = "SELECT `price` FROM oc_product WHERE `product_id` = '".$result['product_id']."' ";
					$real_price = $this->query($sql, $this->conn);
					$price = $result['price'];
					$product_special_id = $result['product_special_id'];
					if($station_id > 0){
						$special_datass = "SELECT price FROM oc_product_special_station_price  WHERE product_id = '".$result['product_id']."' AND `product_special_id` = '".$product_special_id."' AND `station_id` = '".$station_id."' " ;
						$special_data = $this->query($special_datass, $this->conn);
						if($special_data->num_rows > 0){
							$price = $special_data->row['price'];
						}
					}
					$isSpecialPrice = 1;
					$regularPrice = $real_price->row['price'];
					if($station_id > 0){
						$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$result['product_id']."' AND `station_id` = '".$station_id."' " ;
						$special_data = $this->query($special_datass, $this->conn);
						if($special_data->num_rows > 0){
							$regularPrice = $special_data->row['price'];
						}
					}
					if(isset($data['currentusercart'][ $result['product_id']])){
						$quantity = $data['currentusercart'][ $result['product_id']];
					} else {
						$quantity = (int)0;
					}
					// if($result['quantity'] <= 0){
					// 	$result['stock_status_id'] = 9;
					// }
					$stock = $this->stock($result['product_id']);
					$stock_status_id = $result['stock_status_id'];
					if ($stock['available_quantity'] <= 0) {
						$stock_status_id = 9;
					}
					$options = array();
					$data['item_datas'][] = array(
						'product_id'  => $result['product_id'],
						'image'       => $base.$result['image'],
						'name'        => $result['name'],
						'description' => html_entity_decode($result['description']),
						'price'       => (int)$price,
						'regularPrice' => (int)$regularPrice,
						'stock_status_id'  => $stock_status_id,
						'quantity'    => (int)$quantity,
					);
				}
			}
		}	
		$total_items = 0;
		$total_price = 0;
		$item_price = 0;
		foreach($data['currentusercart'] as $key => $value){
			$item_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id`= p.`product_id` WHERE pd.`product_id`= '".$key."' AND p.`is_wholesaller`= '".$wholesaler_check."'", $this->conn)->row;
			if(isset($item_datas['stock_status_id']) && $item_datas['stock_status_id'] == 9 || $item_datas['quantity'] == 0){
				$value = 0;
			}
			$special_datass = "SELECT product_special_id, price FROM oc_product_special  WHERE product_id = '".$key."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1" ;
			$special_data = $this->query($special_datass, $this->conn);
			if($special_data->num_rows > 0){
				$price = $special_data->row['price'];
				$product_special_id = $special_data->row['product_special_id'];
				if($station_id > 0){
					$special_datass = "SELECT price FROM oc_product_special_station_price  WHERE product_id = '".$key."' AND `product_special_id` = '".$product_special_id."' AND `station_id` = '".$station_id."' " ;
					$special_data = $this->query($special_datass, $this->conn);
					if($special_data->num_rows > 0){
						$price = $special_data->row['price'];
					}
				}
				$isSpecialPrice = 1;
			} else{
				$isSpecialPrice = 0;
				if (isset($wholesaler_check) && $wholesaler_check == '1') {
					$price = $item_datas['wholeseller_price'];
				} else {
					$price = $item_datas['price'];
					if($station_id > 0){
						$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$key."' AND `station_id` = '".$station_id."' " ;
						$special_data = $this->query($special_datass, $this->conn);
						if($special_data->num_rows > 0){
							$price = $special_data->row['price'];
						}
					}
				}
			}
			if($item_datas['stock_status_id'] != 9 && $item_datas['quantity'] > 0){
				$total_items = $total_items + $value;
				$total_price = $total_price + ($value * $price);
			}
		}
		$data['total_items'] = (int)$total_items;
		$data['total_price'] = (int)$total_price;	
		if(isset($data['item_datas'])){
			$data['success'] = 1;
		} else {
			$data['success'] = 0;
		}
		$data['result_type'] = 'item';
		return $data;
	}

	public function token($length = 32) {
		// Create random token
		$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		$max = strlen($string) - 1;
		$token = '';
		for ($i = 0; $i < $length; $i++) {
			$token .= $string[mt_rand(0, $max)];
		}	
		return $token;
	}

	public function getDayCloseProduct($product_id) {
		$query = $this->query("SELECT * from `oc_day_close` having  `date` = (SELECT max(`date`) from `oc_day_close` WHERE 1=1 ) AND product_id = '".$product_id."' ",$this->conn)->row;

		return $query;
	}

	public function stock($product_id) {
		$result = $this->getDayCloseProduct($product_id);

		$product = array(
			'category_name' 		=> 0,
			'product_name' 			=> '',
			'opening_quantity'     	=> 0,
			'sold_quantity'			=> 0,
			'sold_quantity_by_order'=> 0,
			'purchased_quantity'	=> 0,
			'purchased_return_quantity'	=> 0,
			'available_quantity'	=> 0,
		);

		if ($result) {
			$next_date = date('Y-m-d', strtotime("+1 day", strtotime($result['date'])));
			$delivery_date = date('Y-m-d', strtotime("+2 day", strtotime($result['date'])));
			$query_category =  $this->query("SELECT category_id FROM `oc_product_to_category` WHERE product_id= '".$result['product_id']."' ",$this->conn);
			$categoryId = 0;
			if ($query_category->num_rows > 0) {
				$categoryId = $query_category->row['category_id'];
			}
			$query_category_name =  $this->query("SELECT name FROM `oc_category_description` WHERE category_id= '".$categoryId."' ",$this->conn);
			$category_name = '';
			if($query_category_name->num_rows > 0) {
				$category_name = $query_category_name->row['name'];
			}
			$product_name_sql = "SELECT name FROM `oc_product_description` WHERE product_id= '".$result['product_id']."' ";
			$query_sold_quantity_from_outward = $this->query("SELECT quantity FROM oc_outward WHERE `date` = '".$next_date."' AND product_id = '".$result['product_id']."' ",$this->conn)->row;
			$query_sold_quantity = $this->query("SELECT op.product_id, SUM(op.quantity) AS sold_quantity FROM oc_order o LEFT JOIN oc_order_product op ON (o.order_id= op.order_id) WHERE DATE(o.delivery_date) = '".$delivery_date."' AND (o.order_status_id = '2' OR o.order_status_id = '5') AND op.product_id = '".$result['product_id']."' GROUP BY op.product_id ",$this->conn)->row;
			$query_inward_quantity = $this->query("SELECT ip.product_id, SUM(ip.quantity) AS purchased_quantity FROM oc_inward i LEFT JOIN oc_inward_product ip ON (i.inward_id = ip.inward_id) WHERE DATE(i.date_added) = '".$next_date."' AND i.cancel_status = '1' AND ip.product_id = '".$result['product_id']."' AND i.is_return = '0' GROUP BY ip.product_id ",$this->conn)->row;
			$query_return_inward_quantity = $this->query("SELECT ip.product_id, SUM(ip.quantity) AS purchased_return_quantity FROM oc_inward i LEFT JOIN oc_inward_product ip ON (i.inward_id = ip.inward_id) WHERE DATE(i.date_added) = '".$next_date."' AND i.cancel_status = '1' AND ip.product_id = '".$result['product_id']."' AND i.is_return = '1'  GROUP BY ip.product_id ",$this->conn)->row;


			$sold_quantity = 0;
			$sold_quantity_by_order = 0;
			$return_inward_quantity = 0;

			if ($query_return_inward_quantity) {
				$return_inward_quantity = $query_return_inward_quantity['purchased_return_quantity'];
			}

			if ($query_sold_quantity) {
				$sold_quantity_by_order = $query_sold_quantity['sold_quantity'];
			}

			if ($query_sold_quantity_from_outward) {
				$sold_quantity = $query_sold_quantity_from_outward['quantity'];
			} else {
				if ($query_sold_quantity) {
					$sold_quantity = $query_sold_quantity['sold_quantity'];
				}
			}

			$inward_quantity = 0;
			if ($query_inward_quantity) {
				$inward_quantity = $query_inward_quantity['purchased_quantity'];
			}

			$product_name = $this->query($product_name_sql,$this->conn);
			if($product_name->num_rows > 0){
				$final_product_name = $product_name->row['name'];			
			} else {
				$final_product_name = '';
			}

			$available_quantity = ($result['quantity'] + $inward_quantity) - $sold_quantity - $return_inward_quantity;

			$product = array(
				'category_name' 		=> $category_name ,
				'product_name' 			=> $final_product_name ,
				'opening_quantity'     	=> $result['quantity'],
				'sold_quantity'			=> $sold_quantity,
				'sold_quantity_by_order'=> $sold_quantity_by_order,
				'purchased_quantity'	=> $inward_quantity,
				'purchased_return_quantity'	=> $return_inward_quantity,
				'available_quantity'	=> $available_quantity,
			);

		}

		return $product;
	}
}
?>