<?php 
error_reporting(E_ALL);
ini_set("display_errors", 1);

$con = mysqli_connect("localhost","root","adorenats","db_taazitokari");

// Check connection
if (!$con) {
	echo "connection failed..";exit;
}

$delivery_date = '2020-04-27';
$apple_price = 395;
$orange_price = 370;
$veg_price = 420;
$surmai_price = 650;
$pomfret_price = 450;
$prawns_price = 550;
$fruits_price = 425;
$pomfret1_price = 14500;
$pomfret2_price = 12500;
$pomfret3_price = 950;
$pomfret4_price = 750;
$rawas1_price = 0;
$rawas2_price = 0;
$halwa1_price = 3250;
$halwa2_price = 6500;

$chicken_price = 285;

$mango_price = 550;
$halwa3_price = 1300;
$bangda_price = 350;
$kapri_price = 800;

$breast_boneless_price = 270;
$kheema_price = 240;
$lollipop_price = 160;

$liver_sealed_price = 75;

$mango2_price = 750;
$mango3_price = 950;

$dates = date("Y-m-d") ;
$pomfret_qty = $_POST['items']['pomfret'];
$pomfret1_qty = $_POST['items']['pomfret1'];
$pomfret2_qty = $_POST['items']['pomfret2'];
$pomfret3_qty = $_POST['items']['pomfret3'];
$pomfret4_qty = $_POST['items']['pomfret4'];
$surmai_qty = $_POST['items']['surmai'];
$prawns_qty = $_POST['items']['prawns'];
$fruits_qty = $_POST['items']['fruits'];
$veg_qty = $_POST['items']['veg'];
$apple_qty = $_POST['items']['apple'];
$orange_qty = $_POST['items']['orange'];

$rawas1_qty = $_POST['items']['rawas1'];
$rawas2_qty = $_POST['items']['rawas2'];

$halwa1_qty = $_POST['items']['halwa1'];
$halwa2_qty = $_POST['items']['halwa2'];

$chicken_qty = $_POST['items']['chicken'];

$mango_qty = $_POST['items']['mango'];

$halwa3_qty = $_POST['items']['halwa3'];
$bangda_qty = $_POST['items']['bangda'];
$kapri_qty = $_POST['items']['kapri'];

$breast_boneless_qty = $_POST['items']['breast_boneless'];
$kheema_qty = $_POST['items']['kheema'];
$lollipop_qty = $_POST['items']['lollipop'];

$liver_sealed_qty = $_POST['items']['liver_sealed'];

$mango2_qty = $_POST['items']['mango2'];
$mango3_qty = $_POST['items']['mango3'];

$total_qty =  $pomfret_qty + $pomfret1_qty + $pomfret2_qty + $pomfret3_qty + $pomfret4_qty + $surmai_qty + $prawns_qty + $fruits_qty + $veg_qty + $apple_qty + $orange_qty + $rawas1_qty + $rawas2_qty + $halwa1_qty + $halwa2_qty + $chicken_qty + $mango_qty + $halwa3_qty + $bangda_qty + $kapri_qty + $breast_boneless_qty + $kheema_qty + $lollipop_qty + $liver_sealed_qty + $mango2_qty + $mango3_qty;

$pomfret_total = $pomfret_qty * $pomfret_price;
$pomfret1_total = $pomfret1_qty * $pomfret1_price;
$pomfret2_total = $pomfret2_qty * $pomfret2_price;
$pomfret3_total = $pomfret3_qty * $pomfret3_price;
$pomfret4_total = $pomfret4_qty * $pomfret4_price;
$surmai_total = $surmai_qty * $surmai_price;
$prawns_total = $prawns_qty * $prawns_price;
$fruits_total = $fruits_qty * $fruits_price;
$veg_total = $veg_qty * $veg_price;
$apple_total = $apple_qty * $apple_price;
$orange_total = $orange_qty * $orange_price;

$rawas1_total = $rawas1_qty * $rawas1_price;
$rawas2_total = $rawas2_qty * $rawas2_price;

$halwa1_total = $halwa1_qty * $halwa1_price;
$halwa2_total = $halwa2_qty * $halwa2_price;

$chicken_total = $chicken_qty * $chicken_price;

$mango_total = $mango_qty * $mango_price;

$halwa3_total = $halwa3_qty * $halwa3_price;
$bangda_total = $bangda_qty * $bangda_price;
$kapri_total = $kapri_qty * $kapri_price;

$breast_boneless_total = $breast_boneless_qty * $breast_boneless_price;
$kheema_total = $kheema_qty * $kheema_price;
$lollipop_total = $lollipop_qty * $lollipop_price;

$liver_sealed_total = $liver_sealed_qty * $liver_sealed_price;

$mango2_total = $mango2_qty * $mango2_price;
$mango3_total = $mango3_qty * $mango3_price;

$express = 0;
if(isset($_POST['express_charge'])){
	if($_POST['express_charge']  == 1){ 
		$delivery = 100;
		$express = 1;

		

	} else {
		$delivery = 30;
	}

}else{
	$delivery = 30;
}
$total = $pomfret_total + $pomfret1_total + $pomfret2_total + $pomfret3_total + $pomfret4_total + $surmai_total + $prawns_total + $fruits_total + $veg_total + $apple_total +$orange_total +$rawas1_total+$rawas2_total+$halwa1_total+ $halwa2_total+ $chicken_total+ $mango_total +$halwa3_total+ $bangda_total +$kapri_total+ $breast_boneless_total + $kheema_total + $lollipop_total + $liver_sealed_total + $mango2_total + $mango3_total +$delivery;

$sub_total = $pomfret_total + $pomfret1_total + $pomfret2_total + $pomfret3_total + $pomfret4_total + $surmai_total + $prawns_total + $fruits_total + $veg_total + $orange_total + $apple_total +$rawas1_total+$rawas2_total+$halwa1_total+$halwa2_total+$chicken_total+$mango_total + $halwa3_total + $bangda_total + $kapri_total+ $breast_boneless_total + $kheema_total + $lollipop_total + $liver_sealed_total + $mango2_total + $mango3_total;
	
$order_exist_sql = "SELECT `order_id` FROM `oc_order` WHERE `store_url` LIKE 'Whatsapp' AND `telephone` = '".$_POST['mobile']."' AND `total` = '".$total."' AND `delivery_date` = '".$delivery_date."' AND order_status_id = '2' ";
$query = $con->query($order_exist_sql);
$order_exist = 0;
if (isset($query->num_rows) && $query->num_rows > 0) {
	$order_exist = 1;
}

if($order_exist == 0){
	// echo $veg_qty;
	// echo '<br />';
	// echo $apple_qty;
	// echo '<br />';
	// echo $orange_qty;
	// echo '<br />';
	// echo $total_qty;
	// echo '<br />';
	// echo '<br />';

	// echo $veg_total;
	// echo '<br />';
	// echo $apple_total;
	// echo '<br />';
	// echo $orange_total;
	// echo '<br />';
	// echo $total;
	// echo '<br />';
	// echo '<br />';
	// exit;

	$customer_sql = "SELECT `customer_id` FROM `oc_customer` WHERE `telephone` = '".$_POST['mobile']."' ";
	$query = $con->query($customer_sql);
	$customer_id = 0;
	if (isset($query->num_rows) && $query->num_rows > 0) {
		while ($row = $query->fetch_assoc()) {
			$customer_id = $row['customer_id'];
		}
	}

	$sql = "INSERT INTO oc_order SET 
		`store_name` = 'Taazi Tokari',
		`store_url` = 'Whatsapp',
		`customer_id` = '".$customer_id."',
		`customer_group_id` = '1',
		`firstname` = '".$con->real_escape_string($_POST['name'])."',
		`lastname` = '',
		`telephone` = '".$con->real_escape_string($_POST['mobile'])."',
		`payment_firstname` = '".$con->real_escape_string($_POST['name'])."',
		`payment_lastname` = '',
		`payment_address_1` = '".$con->real_escape_string($_POST['address'])."',
		`building` = '".$con->real_escape_string($_POST['building'])."',
		`payment_address_2` = '',
		`payment_city` = '".$con->real_escape_string($_POST['city'])."',
		`payment_country` = 'India',
		`payment_country_id` = '99',
		`payment_zone` = 'Maharashtra',
		`payment_zone_id` = '1493',
		`payment_method` = '".$con->real_escape_string($_POST['payment_method'])."',
		`payment_code` = 'COD',
		`shipping_firstname` = '".$con->real_escape_string($_POST['name'])."',
		`shipping_lastname` = '',
		`shipping_address_1` = '".$con->real_escape_string($_POST['address'])."',
		`shipping_address_2` = '',
		`shipping_city` = 'Mumbai',
		`shipping_country` = 'India',
		`shipping_country_id` = '99',
		`shipping_zone` = 'Maharashtra',
		`shipping_zone_id` = '1493',
		`shipping_method` = 'Flat Shipping Rate',
		`shipping_code` = 'flat',
		`total` = '".$total."',
		`express_charge` = '".$con->real_escape_string($express)."',
		`order_status_id` = '2',
		`language_id` = '1',
		`currency_id` = '4',
		`currency_code` = 'INR',
		`currency_value` = '1',
		`date_added` = '".$con->real_escape_string($dates)."',
		`date_modified` = '".$con->real_escape_string($dates)."',
		`location_id` = '1',
		`delivery_date` = '".$delivery_date."',
		`delivery_time` = '1',
		`custom_field` = '', 
		`payment_address_format` = '', 
		`payment_custom_field` = '', 
		`shipping_address_format` = '', 
		`shipping_custom_field` = '',
		`latitude` = '".$con->real_escape_string($_POST['latitude']) . "',
		`longitude` = '".$con->real_escape_string($_POST['longitude']) . "', 
		`comment` = '',
		`gift_box`='".$con->real_escape_string($_POST['gift_box'])."',
		`delivered_status` = '0'
		";
	
	if ($con->query($sql) === TRUE) {
	//if (1) {
		$last_id = $con->insert_id;
		
		if($pomfret_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '672', 
		    `name` = 'Pomfret', 
		    quantity = '".$pomfret_qty."',
		    price = '".$pomfret_total."',
		    total = '".$pomfret_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($pomfret1_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '675', 
		    `name` = 'Pomfret 1', 
		    quantity = '".$pomfret1_qty."',
		    price = '".$pomfret1_total."',
		    total = '".$pomfret1_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($pomfret2_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '676', 
		    `name` = 'Pomfret 2', 
		    quantity = '".$pomfret2_qty."',
		    price = '".$pomfret2_total."',
		    total = '".$pomfret2_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($pomfret3_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '677', 
		    `name` = 'Pomfret 3', 
		    quantity = '".$pomfret3_qty."',
		    price = '".$pomfret3_total."',
		    total = '".$pomfret3_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($pomfret4_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '678', 
		    `name` = 'Pomfret 4', 
		    quantity = '".$pomfret4_qty."',
		    price = '".$pomfret4_total."',
		    total = '".$pomfret4_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($surmai_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '673', 
		    `name` = 'Surmai', 
		    quantity = '".$surmai_qty."',
		    price = '".$surmai_total."',
		    total = '".$surmai_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($prawns_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '674', 
		    `name` = 'Prawns', 
		    quantity = '".$prawns_qty."',
		    price = '".$prawns_total."',
		    total = '".$prawns_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($fruits_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '671', 
		    `name` = 'Fruit basket', 
		    quantity = '".$fruits_qty."',
		    price = '".$fruits_total."',
		    total = '".$fruits_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($veg_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '668', 
		    `name` = 'Veg Tokari', 
		    quantity = '".$veg_qty."',
		    price = '".$veg_total."',
		    total = '".$veg_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($apple_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '669', 
		    `name` = 'Apples', 
		    quantity = '".$apple_qty."',
		    price = '".$apple_total."',
		    total = '".$apple_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($orange_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '670', 
		    `name` = 'Orange', 
		    quantity = '".$orange_qty."',
		    price = '".$orange_total."',
		    total = '".$orange_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($rawas1_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '679', 
		    `name` = 'Rawas1', 
		    quantity = '".$rawas1_qty."',
		    price = '".$rawas1_total."',
		    total = '".$rawas1_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($rawas2_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '680', 
		    `name` = 'Rawas2', 
		    quantity = '".$rawas2_qty."',
		    price = '".$rawas2_total."',
		    total = '".$rawas2_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($halwa1_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '681', 
		    `name` = 'Halwa1', 
		    quantity = '".$halwa1_qty."',
		    price = '".$halwa1_total."',
		    total = '".$halwa1_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($halwa2_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '682', 
		    `name` = 'Halwa2', 
		    quantity = '".$halwa2_qty."',
		    price = '".$halwa2_total."',
		    total = '".$halwa2_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($chicken_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '683', 
		    `name` = 'Chicken', 
		    quantity = '".$chicken_qty."',
		    price = '".$chicken_total."',
		    total = '".$chicken_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($mango_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '684', 
		    `name` = 'Mango', 
		    quantity = '".$mango_qty."',
		    price = '".$mango_total."',
		    total = '".$mango_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($halwa3_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '685', 
		    `name` = 'Halwa3', 
		    quantity = '".$halwa3_qty."',
		    price = '".$halwa3_total."',
		    total = '".$halwa3_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($bangda_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '686', 
		    `name` = 'Bangda', 
		    quantity = '".$bangda_qty."',
		    price = '".$bangda_total."',
		    total = '".$bangda_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($kapri_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '687', 
		    `name` = 'Kapri', 
		    quantity = '".$kapri_qty."',
		    price = '".$kapri_total."',
		    total = '".$kapri_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($breast_boneless_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '204', 
		    `name` = 'breast_boneless', 
		    quantity = '".$breast_boneless_qty."',
		    price = '".$breast_boneless_total."',
		    total = '".$breast_boneless_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($kheema_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '209', 
		    `name` = 'kheema', 
		    quantity = '".$kheema_qty."',
		    price = '".$kheema_total."',
		    total = '".$kheema_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($lollipop_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '690', 
		    `name` = 'lollipop', 
		    quantity = '".$lollipop_qty."',
		    price = '".$lollipop_total."',
		    total = '".$lollipop_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($liver_sealed_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '691', 
		    `name` = 'liver_sealed', 
		    quantity = '".$liver_sealed_qty."',
		    price = '".$liver_sealed_total."',
		    total = '".$liver_sealed_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($mango2_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '692', 
		    `name` = 'mango2', 
		    quantity = '".$mango2_qty."',
		    price = '".$mango2_total."',
		    total = '".$mango2_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

		if($mango3_qty>0) {    
		    $sql1 = "INSERT INTO oc_order_product SET 
		    order_id = '".$last_id."', 
		    product_id = '693', 
		    `name` = 'mango3', 
		    quantity = '".$mango3_qty."',
		    price = '".$mango3_total."',
		    total = '".$mango3_total."',
		    tax = '1',
		    reward = '1',
		    location_id = '1'
		    ";

		    $con->query($sql1);
		}

	    $sql2 = "INSERT INTO oc_order_total SET 
	    order_id = '".$last_id."', 
	    `code` = 'total', 
	    `title` = 'Total', 
	    value = '".$total."',
	    sort_order = '9'
	    ";

	    $con->query($sql2);
	    

	    $sql3 = "INSERT INTO oc_order_total SET 
	    order_id = '".$last_id."', 
	    `code` = 'sub_total', 
	    `title` = 'Sub-Total', 
	    value = '".$sub_total."',
	    sort_order = '1'
	    ";

	    $con->query($sql3);

	    $sql5 = "INSERT INTO oc_order_total SET 
	    order_id = '".$last_id."', 
	    `code` = 'shippinng', 
	    `title` = 'Flat shipping rate', 
	    value = '".$delivery."',
	    sort_order = '3'
	    ";

	    $con->query($sql5);

	    

	    $sql4 = "INSERT INTO oc_order_history SET 
	    order_id = '".$last_id."', 
	    `order_status_id` = '2', 
	    `notify` = '1', 
	    comment = '',
	    date_added = '".$dates."'
	    ";

	    $con->query($sql4);

		$mainurl = 'http://sms.360marketings.in/vendorsms/pushsms.aspx?user=Taazitokari&password=Taazitokari@65789&sid=TTOKRI&fl=0&gwid=2&msisdn='.$_POST['mobile'].'&msg=';
		$messagecustomer = "Thank you for ordering with us. Your order is confirm and tentative delivery Date is :".$delivery_date."%0aTotal :Rs ".$total."%0aWith Regards%0aTEAM TAAZI TOKARI";
		$messagecustomer = str_replace(' ', '%20', $messagecustomer);
		$link = $mainurl.$messagecustomer;
	   	file($link);


	   	if($express == 1) {
		   	$smsurl = 'http://sms.360marketings.in/vendorsms/pushsms.aspx?user=Taazitokari&password=Taazitokari@65789&sid=TTOKRI&fl=0&gwid=2&msisdn=9730941488,9320218272&msg=';
			$messagesms = "New express order no ".$last_id;
			$messagesms = str_replace(' ', '%20', $messagesms);
			$link = $smsurl.$messagesms;
		   	file($link);
		}


	   	//http://sms.360marketings.in/vendorsms/pushsms.aspx?user=Taazitokari&password=Taazitokari@65789&sid=TTOKRI&fl=0&gwid=2&msisdn=9987760244&msg=Thank%20you%20for%20ordering%20with%20us.%20Your%20order%20is%20in%20Process.%0aWith%20Regards%0aTEAM%20TAAZI%20TOKARI
	   	// DELETE FROM `oc_order` WHERE `order_id` = '2082';
	   	// DELETE FROM `oc_order_product` WHERE `order_id` = '2082';
	   	// DELETE FROM `oc_order_total` WHERE `order_id` = '2082';
	   	// DELETE FROM `oc_order_history` WHERE `order_id` = '2082';
	   	// echo $link;
	   	// echo '<br />';
	    // echo "Successfully Inserted";
	    // exit;
		header("Location: https://www.taazitokari.com/order_success.php?order_exist=0");
	    //header("Location: http://localhost:8012/rewillax/order_success.php?order_exist=0");
	} else {
		echo "Error: " . $sql . "<br>" . $con->error;
	}
} else {
	header("Location: https://www.taazitokari.com/order_success.php?order_exist=1");
	//header("Location: http://localhost:8012/rewillax/order_success.php?order_exist=1");
}
?>