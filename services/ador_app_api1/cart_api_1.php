<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		$result['item_datas'] = array();
		$total_items = 0;
		$total_prices = 0;
		$gst_vals = 0;
		$grand_totals = 0;
		$result['total_items'] = 0;
		$result['total_price'] = 0;
		$result['gst_val'] = 0;
		$result['grand_total'] = 0;
		$total_qty = 0;
		foreach($data['currentusercart'] as $key => $value){
			$item_datas = $this->query("SELECT * FROM oc_item WHERE item_code = '".$key."' ", $this->conn)->row;
			if ($item_datas) {
				$gst_tax = $this->query("SELECT tax_value FROM oc_tax WHERE id = '" . $item_datas['vat'] . "'", $this->conn);
				if ($gst_tax->num_rows > 0) {
					$gst = $gst_tax->row['tax_value'];
				} else {
					$gst = 0;
				}

				$price_gst_val = ($item_datas['rate_1'] * $gst)/100;
				// $abc = explode('.', $price_gst_val);
				// if (isset($abc[1])) {
				// 	if ($abc[1] >= 5) {
				// 		$bcd = $abc[0] + 1;
				// 		echo $bcd;
				// 	} else {
				// 		$bcd = $price_gst_val;
				// 	}
				// } else {
				// 	$bcd = $price_gst_val;
				// }




				$tots = $item_datas['rate_1'] + $price_gst_val;

				$result['item_datas'][] = array(
					'product_id' => $key,
					'name' => html_entity_decode($item_datas['item_name']),
					'special_notes' => '',
					'quantity' => (int)$value,
					'price' => (floatval($item_datas['rate_1'])),
					'gst_val' => (floatval($price_gst_val)),
					'total' => (floatval($tots)),
					'image' =>	DIR_BASE
				);
				$total_items += $value;
				$total_price = $item_datas['rate_1'] * $value;
				$gst_val = ($total_price * $gst)/100;

				// $abc = explode('.', $gst_val);
				// if (isset($abc[1])) {
				// 	if ($abc[1] >= 5) {
				// 		$bcd = $abc[0] + 1;
				// 		// echo $bcd;
				// 	} else {
				// 		$bcd = $gst_val;
				// 	}
				// } else {
				// 	$bcd = $gst_val;
				// }

				
				$grand_total = $total_price + $gst_val;
				$total_prices += $total_price;
				$gst_vals += $gst_val;
				$grand_totals += $grand_total;
			}
		}
		$result['total_items'] = $total_items;
		$result['total_price'] = $total_prices;
		$result['gst_val'] = $gst_vals;
		$result['grand_total'] = $grand_totals;

		$address_data = $this->query("SELECT * FROM `oc_address_app` WHERE `customer_id` = '".$data['user_id']."' ",$this->conn);

		$result['address_data'] = array();
		$result['address_data_normal'] = array();
		if ($address_data->num_rows > 0) {
			foreach($address_data->rows as $nkey => $nvalue){
				if($nkey == 0){
					$result['address_id'] = $nvalue['address_id'];	
					$result['city'] = $nvalue['city'];
					$result['postcode'] = $nvalue['postcode'];	
					$result['area'] = $nvalue['area'];	
				}
				$result['address_data'][] = array(
					'address_id' =>  $nvalue['address_id'], 
					'address_1' => $nvalue['address_1'].$nvalue['address_2'].$nvalue['city']
				);
				$result['address_data_normal'][$nvalue['address_id']] = array(
					'address_id' =>  $nvalue['address_id'], 
					'postcode' =>  $nvalue['postcode'], 
					'city' =>  $nvalue['city'], 
					'area' =>  $nvalue['area'], 
					'address_1' => $nvalue['address_1'].$nvalue['address_2'].$nvalue['city']
				);
			}
		}

		// $result['delivery_date'] = date('d-m-Y');

		return $result;
	}
	public function getitems($data = array()){
		// echo'<pre>';
		// print_r($data);
		// exit;
		if(!isset($data['user_id'])){
			$data['user_id'] = '';//'212';
		}
		if(!isset($data['referral_code_used'])){
			$data['referral_code_used'] = '';//'212';
		}

		$station_id = 0;
		if($data['user_id'] > 0){
			$address_data = $this->query("SELECT `postcode` FROM `oc_address_app` WHERE `customer_id` = '".$data['user_id']."' ",$this->conn);
		}

		$existing_date = $this->query("SELECT `date` FROM `oc_day_close_status`",$this->conn)->row;
		//echo "<pre>";print_r($existing_date);exit;

		date_default_timezone_set("Asia/Kolkata");
		$result = array();
		$current_date = $existing_date['date'];
		
		$current_date = date('d-m-Y');

		$todays_time = date('H:m:s');
		$noon_12 = date('12:00:00');
		if ($todays_time <= $noon_12) {
			$current_dates = date('d-m-Y', strtotime($current_date));
		} else {
			$current_dates = date('d-m-Y', strtotime($current_date . ' +1 day'));
		}
		$next_day0 = date('d-m-Y', strtotime($current_dates . ' +0 day'));
		$next_day1 = date('d-m-Y', strtotime($current_dates . ' +1 day'));
		$next_day2 = date('d-m-Y', strtotime($current_dates . ' +2 day'));
		$next_day3 = date('d-m-Y', strtotime($current_dates . ' +3 day'));
		$next_day4 = date('d-m-Y', strtotime($current_dates . ' +4 day'));
		$next_day5 = date('d-m-Y', strtotime($current_dates . ' +5 day'));
		$next_day6 = date('d-m-Y', strtotime($current_dates . ' +6 day'));
		$next_day7 = date('d-m-Y', strtotime($current_dates . ' +7 day'));
		$next_day8 = date('d-m-Y', strtotime($current_dates . ' +8 day'));
		$next_day9 = date('d-m-Y', strtotime($current_dates . ' +9 day'));
		$next_day10 = date('d-m-Y', strtotime($current_dates . ' +10 day'));

		$result['delivery_datess'] = array(
			$next_day0  => $next_day0,
			$next_day1  => $next_day1,
			$next_day2  => $next_day2,
			$next_day3  => $next_day3,
			$next_day4  => $next_day4,
			$next_day5  => $next_day5,
			$next_day6  => $next_day6,
			$next_day7  => $next_day7,
			$next_day8  => $next_day8,
			$next_day9  => $next_day9,
			$next_day10  => $next_day10
		);

		// $result['delivery_datess'] = array(
		// 	'14-05-2020' => '14-05-2020',
		// );

		$result['delivery_timess'] = array(
			'1'  => '08 am to 12 pm',
			'2'  => '12 pm to 04 pm',
			'3'  => '04 pm to 08 pm'
		);

		$current_date_time = date('Y-m-d H:i:s');
		$compare_hour = date('H', strtotime($current_date_time . ' +0 Hours'));
		$in1 = 0;
		foreach($result['delivery_timess'] as $dkey => $dvalue){
			$result['delivery_time'] = $dkey;
			$in = 0;
			if($dkey == '1'){
				if($compare_hour >= 0 && $compare_hour < 8){
					$in = 1;
				}
				// if($compare_hour < 12){
				// $in = 1;
				// }
			} else if($dkey == '2'){
				if($compare_hour >= 0 && $compare_hour < 12){
					$in = 1;
				}
				// if($compare_hour < 16){
				// $in = 1;
				// }
			} else if($dkey == '3'){
				if($compare_hour >= 0 && $compare_hour < 16){
					$in = 1;
				}
				// if($compare_hour < 20){
				// $in = 1;
				// }
			}

			if($in == 0){
				//unset($result['delivery_timess'][$dkey]);
			} 
		}
	
		$result['delivery_date'] = $next_day0;
		//$result['delivery_date'] = '14-05-2020';
		$result['delivery_time'] = 1;
		if(count($result['delivery_timess']) == 0){
			unset($result['delivery_datess'][$next_day0]);
			$result['delivery_date'] = $next_day0;
			$result['delivery_timess'] = array(
				'1'  => '08 am to 12 pm',
				'2'  => '12 pm to 04 pm',
				'3'  => '04 pm to 08 pm'
			);
		}

		$address_data = $this->query("SELECT * FROM `oc_address` WHERE `customer_id` = '".$data['user_id']."' ",$this->conn);
		$customer_data = $this->query("SELECT firstname,lastname,telephone,referral_code,referral_credits FROM `oc_customer` WHERE `customer_id` = '".$data['user_id']."' ",$this->conn)->row;
		$result['fname'] = '';
		$result['lname'] = '';
		$result['phoneno'] = '';
		$result['referral_credits'] = 0;
		if ($customer_data) {
			$result['fname'] = $customer_data['firstname'];
			$result['lname'] = $customer_data['lastname'];
			$result['phoneno'] = $customer_data['telephone'];
			$result['referral_credits'] = $customer_data['referral_credits'];
		}
		//echo "<pre>";print_r($customer_data);exit;
		$result['referral_code'] = '';
		$sql = "SELECT `order_id` FROM `oc_order` WHERE `customer_id` = '".$data['user_id']."' AND `order_status_id` <> '0' AND `order_status_id` <> '7' ";
		if(isset($data['edit_order_id'])){
			$sql .= "AND order_id = '".$data['edit_order_id']."' ";//'212';
		}
		$order_done = $this->query($sql,$this->conn);
		if($order_done->num_rows == 0){
			if(strlen($customer_data['referral_code']) == 10){
				$result['referral_code'] = $customer_data['referral_code'];
				$sql1 = "SELECT `order_id` FROM `oc_order` WHERE `referral_code` = '".$result['referral_code']."' AND `customer_id` = '".$data['user_id']."' AND `order_status_id` <> '0' AND `order_status_id` <> '7' ";
				if(isset($data['edit_order_id'])){
					$sql1 .= "AND order_id = '".$data['edit_order_id']."' ";//'212';
				}
				$order_done = $this->query($sql1,$this->conn);
				if($order_done->num_rows == 0){
					$result['referral_code_used'] = $customer_data['referral_code'];
				} else {
					$result['referral_code_used'] = '';
				}
			} else {
				$result['referral_code_used'] = $data['referral_code_used'];
			}
		} else {
			$result['referral_code'] = '99999999999';
			$result['referral_code_used'] = '';
		}		

		//$result['app_discount_percent'] = 0;
		//$result['referral_discount_self'] = DISCOUNT_MOBILE_SELF;
		$result['app_discount_percent'] = DISCOUNT_APP;
		$result['referral_discount_self'] = 0;

		$result['address_id'] = 0;
		$result['city'] = '';
		$result['postcode'] = '';
		$result['area'] = '';
		$result['address_data'] = array();
		$result['address_data_normal'] = array();
		if ($address_data->num_rows > 0) {
			foreach($address_data->rows as $nkey => $nvalue){
				/*$user_data = $this->query("SELECT * FROM oc_pincode WHERE pincode = '" .$nvalue['postcode']."' AND `status` = '1' ",$this->conn);*/
				/*if($user_data->num_rows > 0){
					$exist_status = 1;
				}*/
				$exist_status = 1;
				if($nkey == 0){
					$result['address_id'] = $nvalue['address_id'];	
					$result['city'] = $nvalue['city'];
					if (POSTCODE_MANDATORY == 0 && $nvalue['postcode'] == '') {
						$result['postcode'] = '123';	
					} else {
						$result['postcode'] = $nvalue['postcode'];	
					}

					if (AREA_MANDATORY == 0 && $nvalue['area'] == '') {
						$result['area'] = '123';	
					} else {
						$result['area'] = $nvalue['area'];	
					}

					if ($result['postcode'] == '' || $result['area'] == '') {
						$result['exist_status'] = 0;	
					} else {
						$result['exist_status'] = 1;	
					}

				}
				$result['address_data'][] = array(
					'address_id' =>  $nvalue['address_id'], 
					'address_1' => $nvalue['address_1'].$nvalue['address_2'].$nvalue['city']
				);
				$result['address_data_normal'][$nvalue['address_id']] = array(
					'address_id' =>  $nvalue['address_id'], 
					'postcode' =>  $nvalue['postcode'], 
					'city' =>  $nvalue['city'], 
					'area' =>  $nvalue['area'], 
					'exist_status' =>  $exist_status, 
					'address_1' => $nvalue['address_1'].$nvalue['address_2'].$nvalue['city']
				);
			}
		}

		foreach($result['delivery_datess'] as $dkey => $dvalue){
			$result['delivery_dates'][] = array(
				'key' => $dkey,
				'value' => $dvalue,
			);
		}

		foreach($result['delivery_timess'] as $dkey => $dvalue){
			$result['delivery_times'][] = array(
				'key' => $dkey,
				'value' => $dvalue,
			);
		}

		$base = 'https://taazitokari.com/image/';
		$total_items = 0;
		$total_price = 0;
		if(!isset($data['currentusercart'])){
			$data['currentusercart'] = array();
		}
		$query_wholesaler_check = $this->query("SELECT is_wholesaler FROM oc_customer WHERE customer_id = '".$data['user_id']."' " ,$this->conn)->row;
		$wholesaler_check = 0;
		if ($query_wholesaler_check) {
			$wholesaler_check = $query_wholesaler_check['is_wholesaler'];
		}
		if (isset($data['edit_order_id'])) {
			$total_items = 0;
			$total_price = 0;
			$data['currentusercart'] = $this->query("SELECT `product_id`, `quantity` FROM `oc_order_product` WHERE `order_id` = '".$data['edit_order_id']."' ", $this->conn)->rows;
			foreach($data['currentusercart'] as $key => $value){
				$item_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id`= p.`product_id` WHERE pd.`product_id`= '".$value['product_id']."' AND p.`is_wholesaller`= '".$wholesaler_check."'", $this->conn)->row;
				if(isset($item_datas['product_id'])){
					if(isset($item_datas['stock_status_id']) && $item_datas['stock_status_id'] == 9 || $item_datas['quantity'] == 0){
						$edit_quantity = 0;
					} else {
						$edit_quantity = (int)$value['quantity'];
					}
					$special_datass = "SELECT product_special_id, price FROM oc_product_special  WHERE product_id = '".$value['product_id']."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1" ;
					$special_data = $this->query($special_datass, $this->conn);
					$regularPrice = 0;
					if($special_data->num_rows > 0){
						$price = $special_data->row['price'];
						$product_special_id = $special_data->row['product_special_id'];
						if($station_id > 0){
							$special_datass = "SELECT price FROM oc_product_special_station_price  WHERE product_id = '".$value['product_id']."' AND `product_special_id` = '".$product_special_id."' AND `station_id` = '".$station_id."' " ;
							$special_data = $this->query($special_datass, $this->conn);
							if($special_data->num_rows > 0){
								$price = $special_data->row['price'];
							}
						}
						$isSpecialPrice = 1;
						$regularPrice = $item_datas['price'];
						if($station_id > 0){
							$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$value['product_id']."' AND `station_id` = '".$station_id."' " ;
							$special_data = $this->query($special_datass, $this->conn);
							if($special_data->num_rows > 0){
								$regularPrice = $special_data->row['price'];
							}
						}
					} else{
						$isSpecialPrice = 0;
						if (isset($wholesaler_check) && $wholesaler_check == '1') {
							$price = $item_datas['wholeseller_price'];
						} else {
							$price = $item_datas['price'];
							if($station_id > 0){
								$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$value['product_id']."' AND `station_id` = '".$station_id."' " ;
								$special_data = $this->query($special_datass, $this->conn);
								if($special_data->num_rows > 0){
									$price = $special_data->row['price'];
								}
							}
						}
					}
					$result['item_datas'][] = array(
						'product_id' => $item_datas['product_id'],
						'ng_product_id' => 'ng_'.$item_datas['product_id'],
						'ph_product_id' => 'ph_'.$item_datas['product_id'],
						'name' => html_entity_decode($item_datas['name']),
						'description' => html_entity_decode($item_datas['description']),
						'quantity' => $edit_quantity,
						'price' => (int)$price,
						'isSpecialPrice' => $isSpecialPrice,
						'regularPrice' => (int)$regularPrice,
						'is_wholesaler' => $wholesaler_check,
						'wholesaler_quantity' => $item_datas['wholeseller_minimium_quantity'],
						'stock_status_id' => $item_datas['stock_status_id'],
						'image' =>$base.$item_datas['image']
					);

					if($item_datas['stock_status_id'] != 9 && $item_datas['quantity'] > 0){
						$total_items = $total_items + $edit_quantity;
						$total_price = $total_price + ($edit_quantity * $price);
					}
				}
			}

			// $order_edit_products = $this->query("SELECT `product_id`, `quantity` FROM `oc_order_product` WHERE `order_id` = '".$data['edit_order_id']."' ", $this->conn)->rows;
			// foreach ($order_edit_products as $oepkey => $oepvalue) {
			// 	$result['currentusercart'] = array(
			// 		$oepvalue['product_id'] => $oepvalue['product_id'] 
			// 	);
			// }
		}
		foreach($data['currentusercart'] as $key => $value){
			$item_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id`= p.`product_id` WHERE pd.`product_id`= '".$key."' AND p.`is_wholesaller`= '".$wholesaler_check."'", $this->conn)->row;
			if(isset($item_datas['product_id'])){
				if(isset($item_datas['stock_status_id']) && $item_datas['stock_status_id'] == 9 || $item_datas['quantity'] == 0){
					$value = 0;
				}
				$special_datass = "SELECT product_special_id, price FROM oc_product_special  WHERE product_id = '".$key."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1" ;
				$special_data = $this->query($special_datass, $this->conn);
				$regularPrice = 0;
				if($special_data->num_rows > 0){
					$price = $special_data->row['price'];
					$product_special_id = $special_data->row['product_special_id'];
					if($station_id > 0){
						$special_datass = "SELECT price FROM oc_product_special_station_price  WHERE product_id = '".$key."' AND `product_special_id` = '".$product_special_id."' AND `station_id` = '".$station_id."' " ;
						$special_data = $this->query($special_datass, $this->conn);
						if($special_data->num_rows > 0){
							$price = $special_data->row['price'];
						}
					}
					$isSpecialPrice = 1;
					$regularPrice = $item_datas['price'];
					if($station_id > 0){
						$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$key."' AND `station_id` = '".$station_id."' " ;
						$special_data = $this->query($special_datass, $this->conn);
						if($special_data->num_rows > 0){
							$regularPrice = $special_data->row['price'];
						}
					}
				} else{
					$isSpecialPrice = 0;
					if (isset($wholesaler_check) && $wholesaler_check == '1') {
						$price = $item_datas['wholeseller_price'];
					} else {
						$price = $item_datas['price'];
						if($station_id > 0){
							$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$key."' AND `station_id` = '".$station_id."' " ;
							$special_data = $this->query($special_datass, $this->conn);
							if($special_data->num_rows > 0){
								$price = $special_data->row['price'];
							}
						}
					}
				}
				$result['item_datas'][] = array(
					'product_id' => $item_datas['product_id'],
					'ng_product_id' => 'ng_'.$item_datas['product_id'],
					'ph_product_id' => 'ph_'.$item_datas['product_id'],
					'name' => html_entity_decode($item_datas['name']),
					'description' => html_entity_decode($item_datas['description']),
					'quantity' => (int)$value,
					'price' => (int)$price,
					'isSpecialPrice' => $isSpecialPrice,
					'regularPrice' => (int)$regularPrice,
					'is_wholesaler' => $wholesaler_check,
					'wholesaler_quantity' => $item_datas['wholeseller_minimium_quantity'],
					'stock_status_id' => $item_datas['stock_status_id'],
					'image' =>$base.$item_datas['image']
				);

				if($item_datas['stock_status_id'] != 9 && $item_datas['quantity'] > 0){
					$total_items = $total_items + $value;
					$total_price = $total_price + ($value * $price);
				}
			}
		}
		$result['total_items'] = (int)$total_items;
		$result['total_price'] = (int)$total_price;

		$discount_received = 0;
		if($data['user_id'] > 0){
			$sql4 = "SELECT `order_id` FROM `oc_order` WHERE `customer_id` = '".$data['user_id']."' AND `order_status_id` <> '0' AND `order_status_id` <> '7' ";
			if(isset($data['edit_order_id'])){
				$sql4 .= "AND order_id = '".$data['edit_order_id']."' ";//'212';
			}
			$order_exist = $this->query($sql4, $this->conn)->rows;
			foreach($order_exist as $okey => $ovalue){
				$discount = $this->query("SELECT `order_total_id` FROM oc_order_total WHERE order_id = '".$ovalue['order_id']."' AND code = 'appdiscount' ", $this->conn);
				if ($discount->num_rows > 0) {
					$discount_received = 1;
				}
			}
		}

		$sql5 = "SELECT count(*) as total_order FROM oc_order WHERE (order_status_id = 5 OR order_status_id = 2) AND `customer_id` = '".$data['user_id']."' ";
		if(isset($data['edit_order_id'])){
			$sql5 .= "AND order_id = '".$data['edit_order_id']."' ";//'212';
		}
		$result['total_orders'] = $this->query($sql5,$this->conn)->row['total_order'];

		$result['discount_received'] = $discount_received;
		$result['minimum_order_amount'] = MINIMUM_ORDER_AMOUNT;
		$result['postcode_mandatory'] = POSTCODE_MANDATORY;
		$result['area_mandatory'] = AREA_MANDATORY;

		if(isset($result['item_datas'])){
			$result['success'] = 1;
		} else {
			$result['success'] = 0;
		}

		if (isset($data['edit_order_id'])) {
			$ordered_products = $this->query("SELECT `product_id`, `quantity` FROM `oc_order_product` WHERE `order_id` = '".$data['edit_order_id']."' ", $this->conn)->rows;
			//echo "<pre>"; print_r($ordered_products);exit;
			foreach ($ordered_products as $opkey => $opvalue) {
				$result['edit_products'][] = array(
					$opvalue['product_id']	=>	$opvalue['quantity'],
				);
			}
		}
		return $result;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>