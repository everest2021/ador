<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
		$this->conn->set_charset("utf8");
		$this->conn->query("SET SQL_MODE = ''");
	}
	
	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		if(!isset($data['itemname_search'])){
			$data['itemname_search'] = '';
		}
		if(!isset($data['subcategory_id'])){
			$data['subcategory_id'] = '';
		}
		if(!isset($data['currentusercart'])){
			$data['currentusercart'] = array();
		}
		if(!isset($data['user_id'])){
			$data['user_id'] = '';//'212';
		}

		$station_id = 0;
		if($data['user_id'] > 0){
			$address_data = $this->query("SELECT `postcode` FROM `oc_address` WHERE `customer_id` = '".$data['user_id']."' ",$this->conn);
			if($address_data->num_rows > 0){
				$pincode = $address_data->row['postcode'];
				$station_datas = $this->query("SELECT `station` FROM `oc_pincode` WHERE `pincode` = '".$pincode."' ",$this->conn);
				if($station_datas->num_rows > 0){
					$station_id = $station_datas->row['station'];
				}
			}
		}
		
		$wholesaler_check = $this->query("SELECT is_wholesaler FROM oc_customer WHERE customer_id = '".$data['user_id']."' " ,$this->conn)->row;
		if(!isset($wholesaler_check['is_wholesaler'])){
			$wholesaler_check['is_wholesaler'] = 0;
		}

		$sql = "SELECT * FROM `oc_product` p LEFT JOIN `oc_product_description` pd ON pd.`product_id`= p.`product_id` LEFT JOIN  `oc_product_to_category` pc ON pc.`product_id`= p.`product_id` WHERE pc.`category_id`= '".$data['subcategory_id']."' AND p.`is_wholesaller`= '".$wholesaler_check['is_wholesaler']."' AND p.`status` = '1' AND p.`date_available` <= NOW() ";
		if($data['itemname_search'] != ''){
			$sql .= " AND pd.`name` LIKE '%".$data['itemname_search']."%' ";
		}
		$sql .= " ORDER BY `price` ASC";	
		$item_datas = $this->query($sql, $this->conn);
		$base = 'http://taazitokari.com/image/';
		$result = array();
		if ($item_datas->num_rows > 0) {
			foreach($item_datas->rows as $nkey => $nvalue){
				if(isset($data['currentusercart'][$nvalue['product_id']])){
					$quantity = $data['currentusercart'][$nvalue['product_id']];
				} else {
					$quantity = (int)0;
				}
				$regularPrice = 0;
				$special_datass = "SELECT price, product_special_id FROM oc_product_special  WHERE product_id = '".$nvalue['product_id']."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1" ;
				$special_data = $this->query($special_datass, $this->conn);
				if($special_data->num_rows > 0){
					$price = $special_data->row['price'];
					$product_special_id = $special_data->row['product_special_id'];
					if($station_id > 0){
						$special_datass = "SELECT price FROM oc_product_special_station_price  WHERE product_id = '".$nvalue['product_id']."' AND `product_special_id` = '".$product_special_id."' AND `station_id` = '".$station_id."' " ;
						$special_data = $this->query($special_datass, $this->conn);
						if($special_data->num_rows > 0){
							$price = $special_data->row['price'];
						}
					}
					$isSpecialPrice = 1;
					$regularPrice = $nvalue['price'];
					if($station_id > 0){
						$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$nvalue['product_id']."' AND `station_id` = '".$station_id."' " ;
						$special_data = $this->query($special_datass, $this->conn);
						if($special_data->num_rows > 0){
							$regularPrice = $special_data->row['price'];
						}
					}
				} else{
					$isSpecialPrice = 0;
					if (isset($wholesaler_check['is_wholesaler']) && $wholesaler_check['is_wholesaler'] == '1') {
						$price = $nvalue['wholeseller_price'];
					} else {
						$price = $nvalue['price'];
						if($station_id > 0){
							$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$nvalue['product_id']."' AND `station_id` = '".$station_id."' " ;
							$special_data = $this->query($special_datass, $this->conn);
							if($special_data->num_rows > 0){
								$price = $special_data->row['price'];
							}
						}
					}
				}
				// if($nvalue['quantity'] <= 0){
				// 	$nvalue['stock_status_id'] = 9;
				// }

				$stock = $this->stock($nvalue['product_id']);
				$stock_status_id = $nvalue['stock_status_id'];
				if ($stock['available_quantity'] <= 0) {
					$stock_status_id = 9;
				}
				$result['item_datas'][] = array(
					'product_id' => $nvalue['product_id'], 
					'stock_status_id' => $stock_status_id,
					'name' => html_entity_decode($nvalue['name']),
					'description' => html_entity_decode($nvalue['description']),
					'is_wholesaler' => $wholesaler_check['is_wholesaler'],
					'wholesaler_quantity' => $nvalue['wholeseller_minimium_quantity'],
					'quantity' => $quantity,
					'price' => (int)$price,
					'isSpecialPrice' => $isSpecialPrice,
					'regularPrice' => (int)$regularPrice,
					'image' =>$base.$nvalue['image'],
				);
			}	
		}

		$total_items = 0;
		$total_price = 0;
		$item_price = 0;
		foreach($data['currentusercart'] as $key => $value){
			$item_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id`= p.`product_id` WHERE pd.`product_id`= '".$key."' AND p.`is_wholesaller`= '".$wholesaler_check['is_wholesaler']."'", $this->conn)->row;
			if(isset($item_datas['stock_status_id']) && $item_datas['stock_status_id'] == 9 || $item_datas['quantity'] == 0){
				$value = 0;
			}
			$special_datass = "SELECT product_special_id, price FROM oc_product_special  WHERE product_id = '".$key."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1" ;
			$special_data = $this->query($special_datass, $this->conn);
			if($special_data->num_rows > 0){
				$price = $special_data->row['price'];
				$product_special_id = $special_data->row['product_special_id'];
				if($station_id > 0){
					$special_datass = "SELECT price FROM oc_product_special_station_price  WHERE product_id = '".$key."' AND `product_special_id` = '".$product_special_id."' AND `station_id` = '".$station_id."' " ;
					$special_data = $this->query($special_datass, $this->conn);
					if($special_data->num_rows > 0){
						$price = $special_data->row['price'];
					}
				}
				$isSpecialPrice = 1;
			} else{
				$isSpecialPrice = 0;
				if (isset($wholesaler_check['is_wholesaler']) && $wholesaler_check['is_wholesaler'] == '1') {
					$price = $item_datas['wholeseller_price'];
				} else {
					$price = $item_datas['price'];
					if($station_id > 0){
						$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$key."' AND `station_id` = '".$station_id."' " ;
						$special_data = $this->query($special_datass, $this->conn);
						if($special_data->num_rows > 0){
							$price = $special_data->row['price'];
						}
					}
				}
			}
			if($item_datas['stock_status_id'] != 9 && $item_datas['quantity'] > 0){
				$total_items = $total_items + $value;
				$total_price = $total_price + ($value * $price);
			}
		}
		$result['total_items'] = (int)$total_items;
		$result['total_price'] = (int)$total_price;
		if(isset($result['item_datas'])){
			$result['success'] = 1;
		} else {
			$result['success'] = 0;
		}
		return $result;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}

	public function getDayCloseProduct($product_id) {
		$query = $this->query("SELECT * from `oc_day_close` having  `date` = (SELECT max(`date`) from `oc_day_close` WHERE 1=1 ) AND product_id = '".$product_id."' ",$this->conn)->row;

		return $query;
	}

	public function stock($product_id) {
		$result = $this->getDayCloseProduct($product_id);

		$product = array(
			'category_name' 		=> 0,
			'product_name' 			=> '',
			'opening_quantity'     	=> 0,
			'sold_quantity'			=> 0,
			'sold_quantity_by_order'=> 0,
			'purchased_quantity'	=> 0,
			'purchased_return_quantity'	=> 0,
			'available_quantity'	=> 0,
		);

		if ($result) {
			$next_date = date('Y-m-d', strtotime("+1 day", strtotime($result['date'])));
			$delivery_date = date('Y-m-d', strtotime("+2 day", strtotime($result['date'])));
			$query_category =  $this->query("SELECT category_id FROM `oc_product_to_category` WHERE product_id= '".$result['product_id']."' ",$this->conn);
			$categoryId = 0;
			if ($query_category->num_rows > 0) {
				$categoryId = $query_category->row['category_id'];
			}
			$query_category_name =  $this->query("SELECT name FROM `oc_category_description` WHERE category_id= '".$categoryId."' ",$this->conn);
			$category_name = '';
			if($query_category_name->num_rows > 0) {
				$category_name = $query_category_name->row['name'];
			}
			$product_name_sql = "SELECT name FROM `oc_product_description` WHERE product_id= '".$result['product_id']."' ";
			$query_sold_quantity_from_outward = $this->query("SELECT quantity FROM oc_outward WHERE `date` = '".$next_date."' AND product_id = '".$result['product_id']."' ",$this->conn)->row;
			$query_sold_quantity = $this->query("SELECT op.product_id, SUM(op.quantity) AS sold_quantity FROM oc_order o LEFT JOIN oc_order_product op ON (o.order_id= op.order_id) WHERE DATE(o.delivery_date) = '".$delivery_date."' AND (o.order_status_id = '2' OR o.order_status_id = '5') AND op.product_id = '".$result['product_id']."' GROUP BY op.product_id ",$this->conn)->row;
			$query_inward_quantity = $this->query("SELECT ip.product_id, SUM(ip.quantity) AS purchased_quantity FROM oc_inward i LEFT JOIN oc_inward_product ip ON (i.inward_id = ip.inward_id) WHERE DATE(i.date_added) = '".$next_date."' AND i.cancel_status = '1' AND ip.product_id = '".$result['product_id']."' AND i.is_return = '0' GROUP BY ip.product_id ",$this->conn)->row;
			$query_return_inward_quantity = $this->query("SELECT ip.product_id, SUM(ip.quantity) AS purchased_return_quantity FROM oc_inward i LEFT JOIN oc_inward_product ip ON (i.inward_id = ip.inward_id) WHERE DATE(i.date_added) = '".$next_date."' AND i.cancel_status = '1' AND ip.product_id = '".$result['product_id']."' AND i.is_return = '1'  GROUP BY ip.product_id ",$this->conn)->row;


			$sold_quantity = 0;
			$sold_quantity_by_order = 0;
			$return_inward_quantity = 0;

			if ($query_return_inward_quantity) {
				$return_inward_quantity = $query_return_inward_quantity['purchased_return_quantity'];
			}

			if ($query_sold_quantity) {
				$sold_quantity_by_order = $query_sold_quantity['sold_quantity'];
			}

			if ($query_sold_quantity_from_outward) {
				$sold_quantity = $query_sold_quantity_from_outward['quantity'];
			} else {
				if ($query_sold_quantity) {
					$sold_quantity = $query_sold_quantity['sold_quantity'];
				}
			}

			$inward_quantity = 0;
			if ($query_inward_quantity) {
				$inward_quantity = $query_inward_quantity['purchased_quantity'];
			}

			$product_name = $this->query($product_name_sql,$this->conn);
			if($product_name->num_rows > 0){
				$final_product_name = $product_name->row['name'];			
			} else {
				$final_product_name = '';
			}

			$available_quantity = ($result['quantity'] + $inward_quantity) - $sold_quantity - $return_inward_quantity;

			$product = array(
				'category_name' 		=> $category_name ,
				'product_name' 			=> $final_product_name ,
				'opening_quantity'     	=> $result['quantity'],
				'sold_quantity'			=> $sold_quantity,
				'sold_quantity_by_order'=> $sold_quantity_by_order,
				'purchased_quantity'	=> $inward_quantity,
				'purchased_return_quantity'	=> $return_inward_quantity,
				'available_quantity'	=> $available_quantity,
			);

		}

		return $product;
	}
}
?>