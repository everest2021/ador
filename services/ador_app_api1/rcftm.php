<?php 
date_default_timezone_set("Asia/Kolkata");
require_once('config.php');

$rcftm = new rcftm();
$rcftm->main();

class rcftm {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function main(){
		$customer_data = $this->query("SELECT `customer_id` FROM `oc_customer` WHERE firstname != '' AND telephone != '' LIMIT 550 ",$this->conn)->rows;
		$i = 1;
		foreach ($customer_data as $customer ) {
			$orders = $this->query("SELECT count(*) as total_order, customer_id, telemarketing_id FROM oc_order WHERE (order_status_id = 5 OR order_status_id = 2) AND customer_id = '".$customer['customer_id']."' ",$this->conn)->row;
			if ($orders['total_order'] >= 5) {
				echo "<pre>"; print_r($i++.'Customer_id :'.$orders['customer_id'].', Total_orders :'.$orders['total_order'].' , Telemarketing_id :'.$orders['telemarketing_id']);
				//$this->query("UPDATE `oc_customer` SET `telemarketing_id` = '888' WHERE customer_id = '".$orders['customer_id']."' ",$this->conn);
			}
		}
		exit;
		echo 'Done.';
	}
}
 ?>