<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		if(!isset($data['user_id'])){
			$data['user_id'] = '';//'212';
		}

		$station_id = 0;
		if($data['user_id'] > 0){
			$address_data = $this->query("SELECT `postcode` FROM `oc_address` WHERE `customer_id` = '".$data['user_id']."' ",$this->conn);
			if($address_data->num_rows > 0){
				$pincode = $address_data->row['postcode'];
				$station_datas = $this->query("SELECT `station` FROM `oc_pincode` WHERE `pincode` = '".$pincode."' ",$this->conn);
				if($station_datas->num_rows > 0){
					$station_id = $station_datas->row['station'];
				}
			}
		}

		$existing_date = $this->query("SELECT `date` FROM `oc_day_close_status`",$this->conn)->row;
		date_default_timezone_set("Asia/Kolkata");
		$result = array();
		$current_date = $existing_date['date'];//date('d-m-Y');
		$next_day0 = date('d-m-Y', strtotime($current_date . ' +0 day'));
		$next_day1 = date('d-m-Y', strtotime($current_date . ' +1 day'));
		$next_day2 = date('d-m-Y', strtotime($current_date . ' +2 day'));
		$next_day3 = date('d-m-Y', strtotime($current_date . ' +3 day'));
		$next_day4 = date('d-m-Y', strtotime($current_date . ' +4 day'));
		$next_day5 = date('d-m-Y', strtotime($current_date . ' +5 day'));
		$next_day6 = date('d-m-Y', strtotime($current_date . ' +6 day'));
		$next_day7 = date('d-m-Y', strtotime($current_date . ' +7 day'));
		$next_day8 = date('d-m-Y', strtotime($current_date . ' +8 day'));
		$next_day9 = date('d-m-Y', strtotime($current_date . ' +9 day'));
		$next_day10 = date('d-m-Y', strtotime($current_date . ' +10 day'));

		$result['delivery_datess'] = array(
			$next_day0  => $next_day0,
			$next_day1  => $next_day1,
			$next_day2  => $next_day2,
			$next_day3  => $next_day3,
			$next_day4  => $next_day4,
			$next_day5  => $next_day5,
			$next_day6  => $next_day6,
			$next_day7  => $next_day7,
			$next_day8  => $next_day8,
			$next_day9  => $next_day9,
			$next_day10  => $next_day10
		);

		// $result['delivery_datess'] = array(
		// 	'20-05-2020' => '20-05-2020',
		// );

		$result['delivery_timess'] = array(
			'1'  => '08 am to 12 pm',
			'2'  => '12 pm to 04 pm',
			'3'  => '04 pm to 08 pm'
		);

		$current_date_time = date('Y-m-d H:i:s');
		$compare_hour = date('H', strtotime($current_date_time . ' +0 Hours'));
		$in1 = 0;
		foreach($result['delivery_timess'] as $dkey => $dvalue){
			$result['delivery_time'] = $dkey;
			$in = 0;
			if($dkey == '1'){
				if($compare_hour >= 0 && $compare_hour < 8){
					$in = 1;
				}
				// if($compare_hour < 12){
				// $in = 1;
				// }
			} else if($dkey == '2'){
				if($compare_hour >= 0 && $compare_hour < 12){
					$in = 1;
				}
				// if($compare_hour < 16){
				// $in = 1;
				// }
			} else if($dkey == '3'){
				if($compare_hour >= 0 && $compare_hour < 16){
					$in = 1;
				}
				// if($compare_hour < 20){
				// $in = 1;
				// }
			}

			if($in == 0){
				//unset($result['delivery_timess'][$dkey]);
			} 
		}
	
		$result['delivery_date'] = $next_day0;
		//$result['delivery_date'] = '20-05-2020';
		$result['delivery_time'] = 1;
		if(count($result['delivery_timess']) == 0){
			unset($result['delivery_datess'][$next_day0]);
			$result['delivery_date'] = $next_day1;
			$result['delivery_timess'] = array(
				'1'  => '08 am to 12 pm',
				'2'  => '12 pm to 04 pm',
				'3'  => '04 pm to 08 pm'
			);
		}

		$address_data = $this->query("SELECT * FROM `oc_address` WHERE `customer_id` = '".$data['user_id']."' ",$this->conn);
		$customer_data = $this->query("SELECT firstname,lastname,telephone,referral_code,referral_credits FROM `oc_customer` WHERE `customer_id` = '".$data['user_id']."' ",$this->conn)->row;
		//echo "<pre>";print_r($customer_data);exit;
		$result['fname'] = $customer_data['firstname'];
		$result['lname'] = $customer_data['lastname'];
		$result['phoneno'] = $customer_data['telephone'];
		$result['referral_code'] = $customer_data['referral_code'];
		$result['referral_credits'] = $customer_data['referral_credits'];

		$result['address_id'] = 0;
		$result['city'] = '';
		$result['postcode'] = '';
		$result['address_data'] = array();
		$result['address_data_normal'] = array();
		if ($address_data->num_rows > 0) {
			foreach($address_data->rows as $nkey => $nvalue){
				$user_data = $this->query("SELECT * FROM oc_pincode WHERE pincode = '" .$nvalue['postcode']."' AND `status` = '1' ",$this->conn);
				$exist_status = 0;
				if($user_data->num_rows > 0){
					$exist_status = 1;
				}
				if($nkey == 0){
					$result['address_id'] = $nvalue['address_id'];	
					$result['city'] = $nvalue['city'];	
					$result['postcode'] = $nvalue['postcode'];	
					$result['exist_status'] = $exist_status;	
				}
				$result['address_data'][] = array(
					'address_id' =>  $nvalue['address_id'], 
					'address_1' => $nvalue['address_1'].$nvalue['address_2'].$nvalue['city']
				);
				$result['address_data_normal'][$nvalue['address_id']] = array(
					'address_id' =>  $nvalue['address_id'], 
					'postcode' =>  $nvalue['postcode'], 
					'city' =>  $nvalue['city'], 
					'exist_status' =>  $exist_status, 
					'address_1' => $nvalue['address_1'].$nvalue['address_2'].$nvalue['city']
				);
			}
		}

		foreach($result['delivery_datess'] as $dkey => $dvalue){
			$result['delivery_dates'][] = array(
				'key' => $dkey,
				'value' => $dvalue,
			);
		}

		foreach($result['delivery_timess'] as $dkey => $dvalue){
			$result['delivery_times'][] = array(
				'key' => $dkey,
				'value' => $dvalue,
			);
		}

		$base = 'https://taazitokari.com/image/';
		$total_items = 0;
		$total_price = 0;
		if(!isset($data['currentusercart'])){
			$data['currentusercart'] = array();
		}
		$wholesaler_check = $this->query("SELECT is_wholesaler FROM oc_customer WHERE customer_id = '".$data['user_id']."' " ,$this->conn)->row;
		foreach($data['currentusercart'] as $key => $value){
			$item_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id`= p.`product_id` WHERE pd.`product_id`= '".$key."' AND p.`is_wholesaller`= '".$wholesaler_check['is_wholesaler']."'", $this->conn)->row;
			if(isset($item_datas['product_id'])){
				$special_datass = "SELECT product_special_id, price FROM oc_product_special  WHERE product_id = '".$key."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1" ;
				$special_data = $this->query($special_datass, $this->conn);
				if($special_data->num_rows > 0){
					$price = $special_data->row['price'];
					$isSpecialPrice = 1;
				} else{
					$isSpecialPrice = 0;
					if (isset($wholesaler_check['is_wholesaler']) && $wholesaler_check['is_wholesaler'] == '1') {
						$price = $item_datas['wholeseller_price'];
					} else {
						$price = $item_datas['price'];
					}
				}
				$result['item_datas'][] = array(
					'product_id' => $item_datas['product_id'],
					'ng_product_id' => 'ng_'.$item_datas['product_id'],
					'ph_product_id' => 'ph_'.$item_datas['product_id'],
					'name' => html_entity_decode($item_datas['name']),
					'description' => html_entity_decode($item_datas['description']),
					'quantity' => (int)$value,
					'price' => (int)$price,
					'isSpecialPrice' => $isSpecialPrice,
					'regularPrice' => (int)$item_datas['price'],
					'is_wholesaler' => $wholesaler_check['is_wholesaler'],
					'wholesaler_quantity' => $item_datas['wholeseller_minimium_quantity'],
					'stock_status_id' => $item_datas['stock_status_id'],
					'image' =>$base.$item_datas['image']
				);
				$total_items = $total_items + $value;
				$total_price = $total_price + ($value * $price);
			}
		}
		$result['total_items'] = (int)$total_items;
		$result['total_price'] = (int)$total_price;

		$discount_received = 0;
		if($data['user_id'] > 0){
			$order_exist = $this->query("SELECT `order_id` FROM `oc_order` WHERE `customer_id` = '".$data['user_id']."' AND `order_status_id` <> '0' AND `order_status_id` <> '7' ", $this->conn)->rows;
			foreach($order_exist as $okey => $ovalue){
				$discount = $this->query("SELECT `order_total_id` FROM oc_order_total WHERE order_id = '".$ovalue['order_id']."' AND code = 'appdiscount' ", $this->conn);
				if ($discount->num_rows > 0) {
					$discount_received = 1;
				}
			}
		}
		$result['discount_received'] = $discount_received;

		if(isset($result['item_datas'])){
			$result['success'] = 1;
		} else {
			$result['success'] = 0;
		}
		return $result;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>