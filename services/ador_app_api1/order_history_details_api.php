<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		// echo'<pre>';
		// print_r($data);
		// exit;
		if(!isset($data['order_id'])){
			$data['order_id'] = '';
		}
		$result['product'] = array();
		$order = $this->query("SELECT * FROM  `oc_orders_app`  WHERE `order_id` = '".$data['order_id']."'  ",$this->conn);
		if ($order->num_rows > 0) {
			$order_ids = $order->row['order_id'];
			$result['total_qty'] = $order->row['tot_qty'];
			$result['total'] = round($order->row['total'],2);
			$result['total_gst'] = round($order->row['gst_val'],2);
			if ($order->row['order_status'] == 1) {
				$result['order_status'] = 'Ordered';
			} elseif ($order->row['order_status'] == 2) {
				$result['order_status'] = 'Processing';
			} elseif ($order->row['order_status'] == 3) {
				$result['order_status'] = 'Delivered';
			} else {
				$result['order_status'] = 'Canceled';
			}
		} else {
			$order_ids = '';
			$result['total_qty'] = 0;
			$result['total'] = 0;
			$result['total_gst'] = 0;
			$result['order_status'] = 0;
		}
		
		if(isset($data['product_id']) && $data['product_id'] != ''){
			$order_products = $this->query(" SELECT * FROM `oc_order_item_app` WHERE order_id = '".$order_ids."' AND item_id = '".$data['product_id']."' ",$this->conn)->rows;
		} else {
			$order_products = $this->query(" SELECT * FROM `oc_order_item_app` WHERE order_id = '".$order_ids."' ",$this->conn)->rows;
		}


		$tots = 0;
		foreach ($order_products as $pkey => $pvalue) {
			$tots = $tots + $pvalue['total'];

			$result['product'][]= array(
				'order_product_id' => $pvalue['item_id'],
				'order_id' => $data['order_id'],
				'name' => $pvalue['item'],
				'quantity' => $pvalue['qty'],
				'price' => round($pvalue['price'],2),
				'image' => DIR_BASE,
				'total' => round($pvalue['total'],2),
				'grand_tot' => round($pvalue['grand_tot'],2),
				'gst_val' => round($pvalue['gst_val'],2),
			);
		}
		$tot_items = count($result['product']);
		$result['tot_items'] = $tot_items;
		
		return $result;
	}
	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>