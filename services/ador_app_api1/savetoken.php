<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$file = '/var/www/html/taazitokari/service/service.txt';
$handle = fopen($file, 'a+'); 
// $message = 'tdcsfas';
// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true)  . "\n");
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Loginapi = new Loginapi();
$value = $Loginapi->getuserdata($datas, $handle);
fclose($handle);
exit(json_encode($value));
class Loginapi {
  	public $conn;

  	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
		$this->conn->set_charset("utf8");
		$this->conn->query("SET SQL_MODE = ''");
  	}

  	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function escape($value) {
		return $this->conn->real_escape_string($value);
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

  	public function getuserdata($data = array(), $handle){
	    if(!isset($data['token'])){
	    	$data['token'] = '';
	    }
	    fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($data, true)  . "\n");
		$result = array();
	    $t=$data['token'];
		if($t != ''){
			$this->query("DELETE FROM `oc_token` WHERE `token_value` = '".$t."' ", $this->conn);
			$this->query("INSERT INTO `oc_token` SET `token_value` = '".$t."' ", $this->conn);
			$result['user_datas']['status'] = 1;
		} else {
			$result['user_datas']['status'] = 0;
		}
		return $result;
  	}

  	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>