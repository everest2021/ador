<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		// echo'<pre>';
		// print_r($data);
		// exit;
		if(!isset($data['user_id'])){
		 	$data['user_id'] = '';//'212';
		}
		$user_datas = $this->query("SELECT * FROM `oc_customer_app` WHERE customer_id = '".$data['user_id']."' ",$this->conn);
		$result = array();
		$address_data = array();
		$result['user_datas']['exist_status'] = 0;
		$address_present = 0;
		if ($user_datas->num_rows > 0) {
			foreach($user_datas->rows as $nkey => $nvalue){
				$address_data = $this->query("SELECT * FROM `oc_address_app` WHERE customer_id ='".$data['user_id']."' ",$this->conn)->rows;
				$address_id = '';
				$address_1 	= '';
				$address_2 	= '';
				$city 		= '';
				$postcode 	= '';
				$area 		= '';
				$area_name 	= '';
				if(isset($address_data[0]['address_id'])){
					$address_id = $address_data[0]['address_id'];
					$address_1 	= $address_data[0]['address_1'];
					$address_2 	= $address_data[0]['address_2'];
					$city 		= $address_data[0]['city'];
					$postcode 	= $address_data[0]['postcode'];
					$area 		= $address_data[0]['area'];

					/*$area_names = $this->query("SELECT `name` FROM `oc_station` WHERE station_id = '".$address_data[0]['area']."' ",$this->conn); 
					if($area_names->num_rows > 0){
						$area_name = $area_names->row['name'];	
					}
					$address_data[0]['area_name'] = $area_name;*/

					/*$area_datas = $this->query("SELECT `station`, `station_n` FROM `oc_pincode` WHERE `pincode` = '".$address_data[0]['postcode']."' ", $this->conn)->rows;
					foreach($area_datas as $aakey => $aavalue) {
						$result['area_datas'][] = array(
							'key' => $aavalue['station'],
							'value' => $aavalue['station_n'],
						);
					}*/
					$address_present = 1;
				}
				$result['user_datas'] = array(
					'user_id' => $nvalue['customer_id'], 
					'firstname' => $nvalue['firstname'],
				    'lastname'  => $nvalue['lastname'],
					'phonenumber' => $nvalue['telephone'],
					'email' => $nvalue['email'],
					'exist_status' => 1,
					/*'address' => $address_data,*/
					'address_id' => $address_id,
					'address_1' => $address_1,
					'address_2' => $address_2,
					'city' => $city,
					'postcode' => $postcode,
					'area' => $area,
					/*'area_name' => $area_name,*/
					'state' => 'MAHARASHTRA',
					'country' => 'INDIA',
				);
			}	
		} 

		$city_datass = array(
			''  => 'Please Select',
			'100 ft road'  => '100 ft road',
			'Achole Road' => 'Achole Road',
			'Agashi' => 'Agashi',	
			'Ambadi Road' => 'Ambadi Road',
			'Arnala' => 'Arnala',
			'Bangli' => 'Bangli',
			'Barampur' => 'Barampur',
			'bhabola' => 'bhabola',
			'Bhuigaon' => 'Bhuigaon',
			'Bolinj' => 'Bolinj',
			'Chulna' => 'Chulna',
			'Dewanman' => 'Dewanman',
			'Evershine city' => 'Evershine city',
			'Gass' => 'Gass',
			'Giriz' => 'Giriz',
			'Global city' => 'Global city',
			'Gokhivare' => 'Gokhivare',
			'Gomes aali' => 'Gomes aali',
			'Holi' => 'Holi',
			'K T village, Vasai west' => 'K T village, Vasai west',
			'Khochiwade ' => 'Khochiwade ',
			'koliwada' => 'koliwada',
			'Manickpur' => 'Manickpur',
			'Manvelpada' => 'Manvelpada',
			'Merces' => 'Merces',
			'Mulgaon' => 'Mulgaon',
			'Naigaon east' => 'Naigaon east',
			'Naigaon west' => 'Naigaon west',
			'Nalla' => 'Nalla',
			'Nallasopara East' => 'Nallasopara East',
			'Nallasopara west' => 'Nallasopara west',
			'Nanbhat' => 'Nanbhat',
			'Nandakhal' => 'Nandakhal',
			'Nirmal' => 'Nirmal',
			'Om Nagar' => 'Om Nagar',
			'Papdy' => 'Papdy',
			'Parnaka' => 'Parnaka',
			'Phoolpada' => 'Phoolpada',
			'Rajodi' => 'Rajodi',
			'Ramedy' => 'Ramedy',
			'Sagarshet' => 'Sagarshet',
			'Sai nager' => 'Sai nager',
			'Samel Pada' => 'Samel Pada',
			'Sandor' => 'Sandor',
			'satpala' => 'satpala',
			'Shastri Nagar' => 'Shastri Nagar',
			'Sopara' => 'Sopara',
			'Sriprasta' => 'Sriprasta',
			'Stella' => 'Stella',
			'Suncity' => 'Suncity',
			'Tamtalao' => 'Tamtalao',
			'Tarkhad' => 'Tarkhad',
			'Umelman' => 'Umelman',
			'umralla' => 'umralla',
			'Vasai West' => 'Vasai West',
			'Vasaigaon' => 'Vasaigaon',
			'Vasalai' => 'Vasalai',
			'Vasant Nagari' => 'Vasant Nagari',
			'Virar east' => 'Virar east',
			'Virar West' => 'Virar West',
			'Wagholi' => 'Wagholi',
			'waliv' => 'waliv',
		);

		foreach($city_datass as $dkey => $dvalue){
			$result['city_datas'][] = array(
				'key' => $dkey,
				'value' => $dvalue,
			);
		}

		$result['address_present'] = $address_present;

		if(isset($result['user_datas'])){
			$result['exist_status'] = 1;
		} else {
			$result['exist_status'] = 0;
		}

		return $result;
	}

	public function token($length = 32) {
		// Create random token
		$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		
		$max = strlen($string) - 1;
		
		$token = '';
		
		for ($i = 0; $i < $length; $i++) {
			$token .= $string[mt_rand(0, $max)];
		}	
		
		return $token;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>