<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$file = '/var/www/html/taazitokari/service/service.txt';
//$file = 'C:\xampp\htdocs\taazitokari\service\service.txt';

$handle = fopen($file, 'a+'); 
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas, $handle);
fclose($handle);

exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array(), $handle){
		fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('-----Start------', true)  . "\n");
		if(!isset($data['user_id'])){
			$data['user_id'] = '';
		}
		if(!isset($data['address_id'])){
			$data['address_id'] = '';
		}
		if(!isset($data['currentusercart'])){
			$data['currentusercart'] = array();
		}
		if(!isset($data['delivery_date'])){
			$data['delivery_date'] = '';
		}
		if(!isset($data['delivery_time'])){
			$data['delivery_time'] = '';
		}
		if(!isset($data['ref_discount_per'])){
			$data['ref_discount_per'] = '';
		}
		if(!isset($data['discount_amt'])){
			$data['discount_amt'] = '';
		}
		$ref_code = '';
		if(!isset($data['referral_code'])){
			$data['referral_code'] = '';
		} else {
			$ref_code = $data['referral_code'];
		}
		if(!isset($data['special_notes'])){
			$data['special_notes'] = '';
		}
		if(!isset($data['app_discount'])){
			$data['app_discount'] = 0;
		} 

		$station_id = 0;
		if($data['user_id'] > 0){
			$address_data = $this->query("SELECT `postcode` FROM `oc_address` WHERE `customer_id` = '".$data['user_id']."' ",$this->conn);
			if($address_data->num_rows > 0){
				$pincode = $address_data->row['postcode'];
				$station_datas = $this->query("SELECT `station` FROM `oc_pincode` WHERE `pincode` = '".$pincode."' ",$this->conn);
				if($station_datas->num_rows > 0){
					$station_id = $station_datas->row['station'];
				}
			}
		}

		fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($data, true)  . "\n");
		
		$result = array();
		$date = date('Y-m-d', strtotime($data['delivery_date']));
		if($data['currentusercart']){
			if($data['referral_code'] != ''){
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('in referral_code', true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($data['referral_code'], true)  . "\n");
				$this->query("UPDATE `oc_customer` SET referral_code = '".$data['referral_code']."' WHERE customer_id = '".$data['user_id']."'", $this->conn);
			}
			if ($data['referral_code'] == '') {
				$r_code = $this->query("SELECT referral_code FROM `oc_customer` WHERE customer_id = '".$data['user_id']."'",$this->conn)->row;
				$ref_code = $r_code['referral_code'];
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r("SELECT referral_code FROM `oc_customer` WHERE customer_id = '".$data['user_id']."'", true)  . "\n");
			}
			$address_datas = $this->query("SELECT * FROM `oc_address`  LEFT JOIN `oc_customer`  ON (oc_address.customer_id = oc_customer.customer_id) WHERE oc_address.`address_id` = '".$data['address_id']."' ",$this->conn);
			if($address_datas->num_rows > 0){
				$address_data = $address_datas->row;
				$area_names = $this->query("SELECT `name` FROM `oc_station` WHERE station_id = '".$address_data['area']."' ",$this->conn); 
				$area_name = '';
				if($area_names->num_rows > 0){
					$area_name = $area_names->row['name'];	
				}
				$store_url = 'https://taazitokari.com/service/';
				$this->query("INSERT INTO `oc_order` SET 
						ismobile = '1' ,
						invoice_no = '0' , 
						invoice_prefix = '',
						store_name = 'Taazi Tokari', 
						store_url = '".$store_url."', 
						store_id = '0', 
						customer_id = '" . $address_data['customer_id'] . "', 
						firstname = '" . $this->escape($address_data['firstname'], $this->conn). "', 
						lastname = '" .$this->escape($address_data['lastname'], $this->conn) . "', 
						email = '" .$address_data['email'] . "',
						telephone = '" . $address_data['telephone']. "',
						fax = '',
						custom_field = '',
						payment_address_format = '',
						payment_custom_field = '',
						payment_firstname = '" . $this->escape($address_data['firstname'], $this->conn) . "',
						payment_lastname = '" .$this->escape($address_data['lastname'], $this->conn) . "', 
						payment_address_1 = '" . $this->escape($address_data['address_1'], $this->conn) . "',
						payment_address_2 = '" . $this->escape($address_data['address_2'], $this->conn) . "',
						payment_city = '" . $this->escape($address_data['city'], $this->conn) . "', 
						payment_postcode = '" . $address_data['postcode'] . "', 
						payment_area = '" . $area_name . "', 
						payment_country = 'INDIA', 
						payment_country_id = '99',
						payment_zone_id = '1492',
						shipping_address_format = '',
						shipping_custom_field = '',
						referral_code = '".$this->escape($ref_code, $this->conn)."',
						referral_dis_per = '".$this->escape($data['ref_discount_per'], $this->conn)."',
						comment = '".$this->escape($data['special_notes'], $this->conn)."',
						payment_method = 'Cash on delivery', 
						shipping_firstname = '" . $this->escape($address_data['firstname'], $this->conn) . "', 
						shipping_lastname = '" . $this->escape($address_data['lastname'], $this->conn) . "', 
						shipping_address_1 = '" . $this->escape($address_data['address_1'], $this->conn) . "', 
						shipping_address_2 = '" . $this->escape($address_data['address_2'], $this->conn) . "', 
						shipping_city = '" .$this->escape($address_data['city'], $this->conn) . "', 
						shipping_postcode = '" . $address_data['postcode'] . "', 
						shipping_country = 'INDIA', shipping_country_id = '99',
						shipping_zone_id = '1492', 
						shipping_method = 'Flat shippinng Rate',  
						total = '0.00',
						language_id = '1', 
						currency_id = '4', 
						order_status_id = 2,
						currency_code = 'INR',
						date_added = NOW(), 
						date_modified = NOW(),
						location_id='1' , 
						delivery_date = '".$date."' , 
						delivery_time = '".$data['delivery_time']."',
						telemarketing_id = '".$address_data['telemarketing_id']."' ",$this->conn);

				
				$order_id = $this->getLastId($this->conn);

				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Id : ' . $order_id, true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($data['currentusercart'], true)  . "\n");

				$subtotal = 0;
				$base = 'https://taazitokari.com/image/';
				//echo "<pre>"; print_r($data);exit;
				$wholesaler_check = $this->query("SELECT is_wholesaler FROM oc_customer WHERE customer_id = '".$data['user_id']."' " ,$this->conn)->row;
				foreach($data['currentusercart']  as $ckey => $cvalue){
					$product_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id` = p.`product_id` WHERE pd.`product_id`= '".$ckey."' " ,$this->conn)->row;
					if($wholesaler_check['is_wholesaler'] == '1') {
						$price = $product_datas['wholeseller_price'];
					} else {
						$special_datass = "SELECT product_special_id, price FROM oc_product_special  WHERE product_id = '".$ckey."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1" ;
						$special_data = $this->query($special_datass, $this->conn);
						if($special_data->num_rows > 0){
							$price = $special_data->row['price'];
							$product_special_id = $special_data->row['product_special_id'];
							if($station_id > 0){
								$special_datass = "SELECT price FROM oc_product_special_station_price  WHERE product_id = '".$ckey."' AND `product_special_id` = '".$product_special_id."' AND `station_id` = '".$station_id."' " ;
								$special_data = $this->query($special_datass, $this->conn);
								if($special_data->num_rows > 0){
									$price = $special_data->row['price'];
								}
							}
							$isSpecialPrice = 1;
						} else{
							$isSpecialPrice = 0;
							if (isset($wholesaler_check['is_wholesaler']) && $wholesaler_check['is_wholesaler'] == '1') {
								$price = $product_datas['wholeseller_price'];
							} else {
								$price = $product_datas['price'];
								if($station_id > 0){
									$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$ckey."' AND `station_id` = '".$station_id."' " ;
									$special_data = $this->query($special_datass, $this->conn);
									if($special_data->num_rows > 0){
										$price = $special_data->row['price'];
									}
								}
							}
						}
					}
					$subtotals = (int)$cvalue * $price;
					$subtotal += $subtotals;
					$this->query("INSERT INTO `oc_order_product` SET order_id = '" .$order_id. "', product_id = '" . $product_datas['product_id'] . "', name = '" . $this->escape($product_datas['name'], $this->conn) . "', model = 'Taazi Tokari', quantity = '" .(int)$cvalue. "', price = '" . $price . "', total = '" .$subtotals. "', tax = '0', reward = '0', location_id = '1' ",$this->conn);
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_product` SET order_id = '" .$order_id. "', product_id = '" . $product_datas['product_id'] . "', name = '" . $this->escape($product_datas['name'], $this->conn) . "', model = 'Taazi Tokari', quantity = '" .(int)$cvalue. "', price = '" . $price . "', total = '" .$subtotals. "', tax = '0', reward = '0', location_id = '1' ", true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('order Product Response', true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
				}

				$discount_amt = 0;
				if($data['discount_amt'] != '' && $data['discount_amt'] != '0'){
					$discount_amt = '-'.$data['discount_amt'];
					$title = 'Referral Discount('.$data['ref_discount_per'].'%)';
					$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'referral_discount', title = '".$title."', value='".$discount_amt."', sort_order = 2",$this->conn);
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'referral_discount', title = '".$title."', value='".$discount_amt."', sort_order = 2", true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('referral_discount_used', true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
					$discount_amt = $data['discount_amt'];
				}


				$app_discount = 0;
				if ($data['app_discount'] > 0){
					$app_discounts = '-'.$data['app_discount'];
					$title = 'App Discount('.DISCOUNT_APP.'%)';
					$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'appdiscount', title = '".$title."', value = '".$app_discounts."', sort_order = 4 ",$this->conn);
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'appdiscount', title = '".$title."', value='".$app_discounts."', sort_order = 4 ", true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total App Discount Response', true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
					$app_discount = $data['app_discount'];
				}
				
				$flat_cost = 30;
				if($subtotal >= 200){
					$flat_cost = 30;
				}

				$used_credit = 0;
				$total_amount = ($subtotal) - ($discount_amt + $app_discount);
				if($address_data['referral_credits'] > 0){
					if($address_data['referral_credits'] > $total_amount){
						$after_used_credit = $address_data['referral_credits'] - $total_amount;
						$used_credit = $total_amount;
						$amtsubtotal = 0;
						$flat_cost = 30;
						if($subtotal >= 200){
							$flat_cost = 30;
						}
						if($flat_cost > 0){
							if($after_used_credit > $flat_cost){
								$used_credit += $flat_cost;
								$flat_costs = 0;
								$after_used_credit = $after_used_credit - $flat_cost;
							} else {
								$flat_costs = $flat_cost - $after_used_credit;
								$used_credit += $after_used_credit;
								$after_used_credit = 0;
							}
						} else {
							$flat_costs = $flat_cost;
						}
						$total = $flat_costs;
					} else {
						$after_used_credit = 0;
						$used_credit = $address_data['referral_credits'];
						$amtsubtotal = $total_amount - $address_data['referral_credits'];
						$flat_cost = 30;
						if($amtsubtotal >= 200){
							$flat_cost = 30;
						}
						$total = $amtsubtotal + $flat_cost;
					}
					$referral_credits = '-'.$used_credit;
					$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'referral_credits_used', title = 'Referral Credits Used', value='".$referral_credits."', sort_order = 7",$this->conn);
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'referral_credits_used', title = 'Referral Credits Used', value = '".$referral_credits."', sort_order = 7", true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total Referral Credit Use', true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
					$this->query("UPDATE `oc_customer` SET referral_credits = '".$after_used_credit."' WHERE customer_id = '".$data['user_id']."'", $this->conn);
				} else {
					$total = $total_amount + $flat_cost;
				}
				
				if($data['referral_code'] != ''){
					$referral_creditss = ($total * DISCOUNT_MOBILE) / 100; 
					$this->query("UPDATE `oc_customer` SET referral_credits = (referral_credits + " . (float)$referral_creditss . ") WHERE `telephone` = '".$data['referral_code']."' ",$this->conn);
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("UPDATE `oc_customer` SET referral_credits = (referral_credits + " . (float)$referral_creditss . ") WHERE `telephone` = '".$data['referral_code']."' ", true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('referral_discount_credits_gave', true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
				}

				if ($flat_cost > 0){
					$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'shipping', title = 'Flat shipping rate', value = '".$flat_cost."', sort_order = 6 ",$this->conn);
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'shipping', title = 'Flat shipping rate', value='".$flat_cost."', sort_order = 3 ", true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total Shipping Response', true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
				}
				
				$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'sub_total' ,title = 'Sub-Total' ,value='".$subtotal."', sort_order = 1 ",$this->conn);
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'sub_total' ,title = 'Sub-Total' ,value='".$subtotal."', sort_order = 1 ", true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total Sub Total Response', true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");

				$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'total' ,title = 'Total' ,value='".$total."' ,sort_order = 9 ",$this->conn);
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'total' ,title = 'Total' ,value='".$total."' ,sort_order = 9 ", true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total Total Response', true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");

				$this->query("UPDATE `oc_order` SET total = '".$total."' WHERE `order_id` = '".$order_id."' ",$this->conn);
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("UPDATE `oc_order` SET total = '".$total."' WHERE `order_id` = '".$order_id."' ", true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Update Total Response', true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");

				$this->query("INSERT INTO `oc_order_history` SET order_id = '" .$order_id. "', order_status_id = 2, notify = 1, comment= '".$this->escape($data['special_notes'], $this->conn)."', date_added = NOW() ",$this->conn);
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_history` SET order_id = '" .$order_id. "', order_status_id = 2, notify = 1, comment= '".$this->escape($data['special_notes'], $this->conn)."', date_added = NOW() ", true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order History Insert Response', true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");

				$order_product_query = $this->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'", $this->conn);
				foreach ($order_product_query->rows as $order_product) {
					$this->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'", $this->conn);
					$order_option_query = $this->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product['order_product_id'] . "'", $this->conn);
					foreach ($order_option_query->rows as $option) {
						$this->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'", $this->conn);
					}
				}
				//UPDATE `oc_product` SET `quantity` = '5000' WHERE `quantity` < '0';

				$result['success'] = 1;
			} else {
				$result['success'] = 2;
			}
		} else {
			$result['success'] = 0;
		}
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($result, true)  . "\n");
		fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('-----End------', true)  . "\n");
		return $result;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>