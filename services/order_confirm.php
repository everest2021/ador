<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
//$file = '/var/www/html/taazitokari/webhook_test/query.txt';
$file = 'C:/xampp/htdocs/hotel/ador/services/query.txt';
$handle = fopen($file, 'a+'); 
$data = file_get_contents('php://input');
error_reporting(E_ALL);
ini_set("display_errors", 1);
$datas = json_decode($data,true);
fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($datas, true)  . "\n");
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas, $handle);

exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array(),$handle){

		$my_array = array(
			'phone' => +919284261826,
            'address' => array
                (
                    'city' => 'NA',
                    'pin' => 'NA',
                    'locality' => '',
                    'longitude' => 0,
                    'line_1' => 'null',
                    'latitude' => 0,
                    'line_2' => '',
                    'sub_locality' => 'NA',
                ),

           'email' => '',
           'name' => +919284261826 ,

		);

		$order = array(
			 'next_states' => Array
                (
                     0=> 'Acknowledged',
                    1 => 'Food Ready',
                    2 => 'Dispatched',
                    3 => 'Completed',
                    4 => 'Cancelled',
                ),

            'items' => Array
                (
                    '0' => Array
                        (
                            'image_url' => '',
                            'total' => '200',
                            'title' => 'Pav Bhaji Fondue',
                            'charges' => Array
                                (
                                ),

                            'total_with_tax' => 210,
                            'price' => 200,
                            'translations' => '',
                            'unit_weight' => 0,
                            'discount' => 0,
                            'image_landscape_url' => '',
                            'taxes' => Array
                                (
                                    '0' => Array
                                        (
                                            'rate' => 2.5,
                                            'value' => 5,
                                            'title' => 'SGST',
                                        ),

                                    '1' => Array
                                        (
                                            'rate' => 2.5,
                                            'value' => 5,
                                            'title' => 'CGST',
                                        ),

                                ),

                            'options_to_remove' => Array
                                (
                                ),

                            'food_type' => 1,
                            'merchant_id' => 4,
                            'options_to_add' => Array
                                (
                                ),

                            'id' => 654205,
                            'quantity' => 1,
                        )

                ),

            'details' => Array
                (
                    'coupon' => '',
                    'total_taxes' => 10,
                    'merchant_ref_id' => '',
                    'order_level_total_charges' => 0,
                    'id' => 60313,
                    'payable_amount' => 210,
                    'total_external_discount' => 0,
                    'order_total' => 210,
                    'order_type' => 'pickup',
                    'state' => 'Placed',
                    'channel' => 'satellite',
                    'delivery_datetime' => '1614684665000',
                    'item_level_total_charges' => 0,
                    'item_taxes' => 10,
                    'discount' => 0,
                    'biz_id' => '88635274',
                    'order_state' => 'Placed',
                    'instructions' => '',
                    'total_charges' => 0,
                    'item_level_total_taxes' => 10,
                    'charges' => Array
                        (
                        ),

                    'ext_platforms' => Array
                        (
                        ),

                    'created' => 1614682871979,
                    'taxes' => Array
                        (
                        ),

                    'order_level_total_taxes' => 0,
                    'order_subtotal' => 200,
                ),

            'payment' => Array
                (
                    '0' => Array
                        (
                            'amount' => 210,
                            'option' => 'cash',
                            'srvr_trx_id' => '',
                        )

                ),

            'store' => Array
                (
                    'name' => 'Salt Pan Restaurant And Bar - Test',
                    'longitude' => 0,
                    'merchant_ref_id' => '888-AdorHOTEL_test',
                    'address' => 'Opp. Bhuigaon-Dongri Talav Giriz, Nirmal Rd, Vasai-Virar, Maharashtra 401201',
                    'latitude' => 0,
                    'id' => 32925,
                ),

            'next_state' => 'Acknowledged',

		);

		
		$data = array(
			'customer' => $my_array,
			'order' => $order 


  

   

		);

		if($data != NULL && !empty($data)){ //echo "<pre>";print_r($data);exit;
			//fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($data, true)  . "\n");
			if(!isset($data['customer']['phone'])){
				$data['customer']['phone']= '';
			}
			if(!isset($data['customer']['name'])){
				$data['customer']['name']= '';
			}

			if(!isset($data['customer']['address']['city']) ){
				$data['customer']['address']['city'] = '';
			} else if($data['customer']['address']['city'] == NA || $data['customer']['address']['city'] == 'NA' || $data['customer']['address']['city'] == null || $data['customer']['address']['city'] == 'null'){
				$data['customer']['address']['city'] = '';
			}

			if(!isset($data['customer']['address']['landmark']) ){
				$data['customer']['address']['landmark'] = '';
			} else if($data['customer']['address']['landmark'] == NA || $data['customer']['address']['landmark'] == null || $data['customer']['address']['landmark'] == 'NA'|| $data['customer']['address']['landmark'] == 'null'){
				$data['customer']['address']['landmark'] = '';
			}

			if(!isset($data['customer']['address']['pin']) ){
				$data['customer']['address']['pin'] = '';
			} else if($data['customer']['address']['pin'] == NA || $data['customer']['address']['pin'] == null || $data['customer']['address']['pin'] == 'NA' || $data['customer']['address']['pin'] == 'null'){
				$data['customer']['address']['pin'] = '';
			}

			if(!isset($data['customer']['address']['longitude']) ){
				$data['customer']['address']['longitude'] = '';
			} else if($data['customer']['address']['longitude'] == NA || $data['customer']['address']['longitude'] == null || $data['customer']['address']['longitude'] == 'NA' || $data['customer']['address']['longitude'] == 'null'){
				$data['customer']['address']['longitude'] = '';
			}

			if(!isset($data['customer']['address']['latitude']) ){
				$data['customer']['address']['latitude'] = '';
			} else if($data['customer']['address']['latitude'] == NA || $data['customer']['address']['latitude'] == null || $data['customer']['address']['longitude'] == 'NA' || $data['customer']['address']['longitude'] == 'null'){
				$data['customer']['address']['latitude'] = '';
			}

			if(!isset($data['customer']['address']['line_1']) ){
				$data['customer']['address']['line_1'] = '';
			} else if($data['customer']['address']['line_1'] == NA || $data['customer']['address']['line_1'] == 'null' || $data['customer']['address']['line_1'] == 'NA' || $data['customer']['address']['line_1'] == 'null'){
				$data['customer']['address']['line_1'] = '';
			}

			if(!isset($data['customer']['address']['line_2']) ){
				$data['customer']['address']['line_2'] = '';
			} else if($data['customer']['address']['line_2'] == NA || $data['customer']['address']['line_2'] == null || $data['customer']['address']['line_2'] == 'NA' || $data['customer']['address']['line_2'] == 'null'){
				$data['customer']['address']['line_2'] = '';
			}

			if(!isset($data['customer']['address']['sub_locality']) ){
				$data['customer']['address']['sub_locality'] = '';
			} else if($data['customer']['address']['sub_locality'] == NA || $data['customer']['address']['sub_locality'] == null || $data['customer']['address']['line_2'] == 'NA' || $data['customer']['address']['line_2'] == 'null'){
				$data['customer']['address']['sub_locality'] = '';
			}

			if(isset($data['order']['payment']['0']['option']) ){
				$payment_method = $data['order']['payment']['0']['option'];
			} else {
				$payment_method = '';
			}
			
			//echo "<pre>";print_r($data);exit;

			$user_datas = $this->query("SELECT customer_id FROM `oc_customer_app` WHERE telephone = '".$data['customer']['phone']."' AND customer_type = 'online'  ",$this->conn);
			if ($user_datas->num_rows == 0) {
				$this->query("INSERT INTO `oc_customer_app` SET `firstname` = '".$this->escape($data['customer']['name'], $this->conn)."', `email`= '".$this->escape($data['customer']['email'], $this->conn)."', `telephone`= '".$this->escape($data['customer']['phone'], $this->conn)."', `information_field` = '1' ,customer_type = 'online' , date_added = '".date('Y-m-d')."'",$this->conn);
				$customer_id = $this->getLastId($this->conn);
				 $this->query("INSERT INTO `oc_address_app` SET `customer_id` = '".$customer_id ."', `address_1` = '".$this->escape($data['customer']['address']['line_1'], $this->conn)."', `address_2` = '".$this->escape($data['customer']['address']['line_2'], $this->conn)."', `city`= '".$this->escape($data['customer']['address']['city'], $this->conn)."', `postcode`= '".$this->escape($data['customer']['address']['pin'], $this->conn)."', `area`= '".$this->escape($data['customer']['address']['landmark'], $this->conn)."', `firstname` = '".$this->escape($data['customer']['name'], $this->conn)."', `sub_locality`= '".$this->escape($data['customer']['address']['sub_locality'], $this->conn)."',`country_id` = 99 , `zone_id` = 1493 ", $this->conn);


				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_customer_app` SET `firstname` = '".$this->escape($data['customer']['name'], $this->conn)."', `email`= '".$this->escape($data['customer']['email'], $this->conn)."', `telephone`= '".$this->escape($data['customer']['phone'], $this->conn)."', `information_field` = '1' ,customer_type = 'online'", true)  . "\n");
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_address_app` SET `customer_id` = '".$customer_id ."', `address_1` = '".$this->escape($data['customer']['address']['line_1'], $this->conn)."', `address_2` = '".$this->escape($data['customer']['address']['line_2'], $this->conn)."', `city`= '".$this->escape($data['customer']['address']['city'], $this->conn)."', `postcode`= '".$this->escape($data['customer']['address']['pin'], $this->conn)."', `area`= '".$this->escape($data['customer']['address']['landmark'], $this->conn)."', `firstname` = '".$this->escape($data['customer']['name'], $this->conn)."', `sub_locality`= '".$this->escape($data['customer']['address']['sub_locality'], $this->conn)."',`country_id` = 99 , `zone_id` = 1493 ", true)  . "\n");
			} else {
				$this->query("UPDATE `oc_customer_app` SET `firstname` = '".$this->escape($data['customer']['name'], $this->conn)."', `email`= '".$this->escape($data['customer']['email'], $this->conn)."', `telephone`= '".$this->escape($data['customer']['phone'], $this->conn)."', `information_field` = '1' ,customer_type = 'online'  WHERE customer_id = '".$user_datas->row['customer_id']."' ",$this->conn);
				if($user_datas->num_rows > 0){
					$customer_id = $user_datas->row['customer_id'];
				}
				$this->query("UPDATE `oc_address_app` SET  `address_1` = '".$this->escape($data['customer']['address']['line_1'], $this->conn)."', `address_2` = '".$this->escape($data['customer']['address']['line_2'], $this->conn)."', `city`= '".$this->escape($data['customer']['address']['city'], $this->conn)."', `postcode`= '".$this->escape($data['customer']['address']['pin'], $this->conn)."', `area`= '".$this->escape($data['customer']['address']['landmark'], $this->conn)."', `firstname` = '".$this->escape($data['customer']['name'], $this->conn)."', `sub_locality`= '".$this->escape($data['customer']['address']['sub_locality'], $this->conn)."',`country_id` = 99 , `zone_id` = 1493 WHERE customer_id = '".$customer_id."'", $this->conn);

				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("UPDATE `oc_customer_app` SET `firstname` = '".$this->escape($data['customer']['name'], $this->conn)."', `email`= '".$this->escape($data['customer']['email'], $this->conn)."', `telephone`= '".$this->escape($data['customer']['phone'], $this->conn)."', `information_field` = '1' ,customer_type = 'online'  WHERE customer_id = '".$customer_id."'", true)  . "\n");
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("UPDATE `oc_address_app` SET  `address_1` = '".$this->escape($data['customer']['address']['line_1'], $this->conn)."', `address_2` = '".$this->escape($data['customer']['address']['line_2'], $this->conn)."', `city`= '".$this->escape($data['customer']['address']['city'], $this->conn)."', `postcode`= '".$this->escape($data['customer']['address']['pin'], $this->conn)."', `area`= '".$this->escape($data['customer']['address']['landmark'], $this->conn)."', `firstname` = '".$this->escape($data['customer']['name'], $this->conn)."', `sub_locality`= '".$this->escape($data['customer']['address']['sub_locality'], $this->conn)."',`country_id` = 99 , `zone_id` = 1493 WHERE customer_id = '".$customer_id."'", true)  . "\n");
			}

			$quantity = 0;
			$tax_rate = 0;

			foreach ($data['order']['items'] as $cckey => $ccvalue) {
				$quantity += $ccvalue['quantity'];
				if(!empty($ccvalue['taxes'])){
					foreach ($ccvalue['taxes'] as $tkey => $tvalue) {
						$tax_rate += $tvalue['rate'];
					}
				}
			}

			if(!isset($data['order']['details']['id']) ){
				$data['order']['details']['id'] = '';
			} else if($data['order']['details']['id'] == 'NA' || $data['order']['details']['id'] == 'null'){
				$data['order']['details']['id'] = '';
			}


			if(!empty($data['order']['details'])){
				$this->query("INSERT INTO `oc_orders_app` SET cust_id = '".$customer_id."', 
					tot_qty = '".$quantity."', 
					total = '".$data['order']['details']['order_subtotal']."', 
					grand_tot  = '".$data['order']['details']['order_total']."', 
					tot_gst = '".$this->escape($tax_rate,$this->conn)."',
					gst_val = '".$this->escape($data['order']['details']['total_taxes'],$this->conn)."',
					order_time = '".date('H:i:s')."', 
					order_date = '".date('Y-m-d')."', 
					address_1 = '".$this->escape($data['customer']['address']['line_1'], $this->conn)."', 
					address_2 = '".$this->escape($data['customer']['address']['line_2'], $this->conn)."', 
					city = '".$this->escape($data['customer']['address']['city'], $this->conn)."',
					postcode = '".$this->escape($data['customer']['address']['pin'], $this->conn)."',
					order_status = '1', area = '".$this->escape($data['customer']['address']['sub_locality'],$this->conn)."' ,
					online_order_status = '".$this->escape($data['order']['next_state'], $this->conn)."',
					online_order_type_placed_delie = '".$this->escape($data['order']['details']['order_state'], $this->conn)."',
					`online_order_id` = '".$this->escape($data['order']['details']['id'],$this->conn)."',
					`online_channel` = '".$this->escape($data['order']['details']['channel'],$this->conn)."',
					`online_instruction` = '".$this->escape($data['order']['details']['instructions'],$this->conn)."',
					`online_deliverydatetime` = '".$this->escape($data['order']['details']['delivery_datetime'],$this->conn)."',
					`online_created_time` = '".$this->escape($data['order']['details']['created'],$this->conn)."',
					`online_store_id` = '".$this->escape($data['order']['store']['id'],$this->conn)."',
					`online_biz_id` = '".$this->escape($data['order']['details']['biz_id'],$this->conn)."',
					`online_payment_method` = '".$this->escape($payment_method,$this->conn)."'

					 ", $this->conn);
				$order_id = $this->getLastId($this->conn);

				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_orders_app` SET cust_id = '".$customer_id."', 
					tot_qty = '".$quantity."', 
					total = '".$data['order']['details']['order_total']."', 
					grand_tot  = '".$data['order']['details']['order_total']."', 
					tot_gst = '".$this->escape($tax_rate,$this->conn)."',
					gst_val = '".$this->escape($data['order']['details']['total_taxes'],$this->conn)."',
					order_time = '".date('H:i:s')."', 
					order_date = '".date('Y-m-d')."', 
					address_1 = '".$this->escape($data['customer']['address']['line_1'], $this->conn)."', 
					address_2 = '".$this->escape($data['customer']['address']['line_2'], $this->conn)."', 
					city = '".$this->escape($data['customer']['address']['city'], $this->conn)."',
					postcode = '".$this->escape($data['customer']['address']['pin'], $this->conn)."',
					order_status = '1', area = '".$this->escape($data['customer']['address']['sub_locality'],$this->conn)."' ,
					online_order_status = '".$this->escape($data['order']['next_state'], $this->conn)."',
					online_order_type_placed_delie = '".$this->escape($data['order']['details']['order_state'], $this->conn)."',
					`online_payment_method` = '".$this->escape($payment_method,$this->conn)."',
					`online_channel` = '".$this->escape($data['order']['details']['channel'],$this->conn)."',
					`online_instruction` = '".$this->escape($data['order']['details']['instructions'],$this->conn)."',
					`online_deliverydatetime` = '".$this->escape($data['order']['details']['delivery_datetime'],$this->conn)."',
					`online_created_time` = '".$this->escape($data['order']['details']['created'],$this->conn)."',
					`online_store_id` = '".$this->escape($data['order']['store']['id'],$this->conn)."',
					`online_biz_id` = '".$this->escape($data['order']['details']['biz_id'],$this->conn)."',
					`online_order_id` = '".$this->escape($data['order']['details']['id'],$this->conn)."'  ", true)  . "\n");

			}



			foreach ($data['order']['items'] as $ikey => $ivalue) {


				$item_datas = $this->query("SELECT `item_name`,`item_code` FROM `oc_item` WHERE item_name = '".$ivalue['title']."' ",$this->conn);

				if($item_datas->num_rows > 0){
					$item_codes = $item_datas->row['item_code'];
					$item_name = $item_datas->row['item_name'];
				} else {
					$item_codes = '';
					$item_name = '';
				}
				$tax_rate = 0;
				$tax_val = 0;
				foreach ($ivalue['taxes'] as $tkey => $tvalue) {
					$tax_rate += $tvalue['rate'];
					$tax_val += $tvalue['value'];
				}

				$total = $ivalue['quantity'] * $ivalue['price'];

				$this->query("INSERT INTO `oc_order_item_app` SET order_id = '".$order_id."', `online_order_id` = '".$this->escape($data['order']['details']['id'],$this->conn)."' ,online_order_status = '".$this->escape($data['order']['next_state'], $this->conn)."',item_id = '".$this->escape($item_codes,$this->conn)."', item = '".$this->escape($item_name, $this->conn)."', qty = '".$this->escape($ivalue['quantity'], $this->conn)."', price = '".$this->escape($ivalue['price'], $this->conn)."', total = '".$this->escape($total, $this->conn)."', gst = '".$tax_rate."', gst_val = '".$tax_val."', grand_tot = '".$this->escape($ivalue['total_with_tax'], $this->conn)."' ,	online_order_type_placed_delie = '".$this->escape($data['order']['details']['order_state'], $this->conn)."'", $this->conn);
				
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_item_app` SET order_id = '".$order_id."', `online_order_id` = '".$this->escape($data['order']['details']['id'],$this->conn)."' ,online_order_status = '".$this->escape($data['order']['next_state'], $this->conn)."',item_id = '".$this->escape($item_codes,$this->conn)."', item = '".$this->escape($item_name, $this->conn)."', qty = '".$this->escape($ivalue['quantity'], $this->conn)."', price = '".$this->escape($ivalue['price'], $this->conn)."', total = '".$this->escape($total, $this->conn)."', gst = '".$tax_rate."', gst_val = '".$tax_val."', grand_tot = '".$this->escape($ivalue['total_with_tax'], $this->conn)."' ,	online_order_type_placed_delie = '".$this->escape($data['order']['details']['order_state'], $this->conn)."'", true)  . "\n");
			}
		}
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>