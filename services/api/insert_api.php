<?php 
date_default_timezone_set('Asia/Kolkata');
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$file = 'C:/xampp/htdocs/ador/services/api/service.txt';
$handle = fopen($file, 'a+');
$hourlysms = new Hourlysms($handle);
$hourlysms->putorder($handle);

class Hourlysms {
  	public $conn;
  	public $log;

  	public function __construct($handle) {
		$this->conn = new mysqli('localhost', 'root', '', 'db_ador_2022');
		if ($this->conn->connect_error) {
			fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r($this->conn->connect_error, true)  . "\n");
			die("Connection failed: " . $this->conn->connect_error);
		}
  	}

  	public function getLastId($conn){
		return $conn->insert_id;
		
	}

	public function query($sql, $conn) {
		$query = $this->conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function get_settings ($key,$conn) {
		$setting_status = $this->query("SELECT * FROM settings_ador WHERE `key` = '".$key."'",$conn);
		
		if($setting_status->num_rows > 0){
			return $setting_status->row['value'];
		} else {
			return '';
		}
	}

  	public function putorder($handle){
  		$sub_total_sale = 0;
 		$sub_total_cash = 0;;
 		$sub_total_card = 0;
 		$sub_total_online = 0;
 		$sub_total_onac = 0;
 		$sub_total_status = 0;

 		$sub_total_c_kot_amt = 0;
 		$sub_total_c_kot_count = 0;

 		$sub_total_c_bill_amt = 0;
 		$sub_total_c_bill_count = 0;
 		$sub_total_fdiscount = 0;
	 	$sub_total_ldiscount = 0;
  		

  		$finaldata = array();
  		$date = date("d/m/Y");
		$time =  date("h:i:sa");
  		
  		$total_sale = $this->query("SELECT SUM(grand_total) as t_sale, SUM(pay_cash) as t_cash, SUM(pay_card) as t_card, SUM(pay_online) as t_online, SUM(onac) as t_onac, SUM(cancel_status) as t_status FROM oc_order_info WHERE cancel_status = '0' ",$this->conn);

  		$table_infos =  $this->query("SELECT SUM(grand_total) as panding_sale FROM oc_order_info WHERE day_close_status = '0' AND payment_status = '0' AND cancel_status = '0'",$this->conn);

  		$cancel_kot = $this->query("SELECT count(*) as kot_count, SUM(amt) as c_kot_amt FROM oc_order_items WHERE cancelstatus ='1' ",$this->conn);

  		$cancel_bill = $this->query("SELECT count(*) as bill_count, SUM(grand_total) as c_bill_amt FROM oc_order_info WHERE cancel_status = '1' ",$this->conn);

  		$sub_total_sale = 0;
 		$sub_total_cash = 0;
 		$sub_total_card = 0;
 		$sub_total_online = 0;
 		$sub_total_onac = 0;
 		$sub_total_status = 0;
  		if($total_sale->num_rows > 0) {
	 		$sub_total_sale = $sub_total_sale + $total_sale->row['t_sale'];
	 		$sub_total_cash = $sub_total_cash + $total_sale->row['t_cash'];
	 		$sub_total_card = $sub_total_card + $total_sale->row['t_card'];
	 		$sub_total_online = $sub_total_online + $total_sale->row['t_online'];
	 		$sub_total_onac = $sub_total_onac + $total_sale->row['t_onac'];
	 		$sub_total_status = $sub_total_status + $total_sale->row['t_status'];


  		}

 		$panding_tables_amt = 0;
  		if ($table_infos->num_rows > 0) {
	 		$panding_tables_amt = $panding_tables_amt + $table_infos->row['panding_sale'] ;
  		}
  		// echo "<pre>";print_r($panding_tables_amt);exit;

  		$sub_total_c_kot_amt = 0;
  		$sub_total_c_kot_count = 0;
  		if($cancel_kot->num_rows > 0) {
	 		$sub_total_c_kot_amt = $sub_total_c_kot_amt + $cancel_kot->row['c_kot_amt'];
	 		$sub_total_c_kot_count = $sub_total_c_kot_count + $cancel_kot->row['kot_count'];
	 	}

	 	$sub_total_c_bill_amt = 0;
	 	$sub_total_c_bill_count = 0;
  		if($cancel_bill->num_rows > 0) {
	 		$sub_total_c_bill_amt = $sub_total_c_bill_amt + $cancel_bill->row['c_bill_amt'];
	 		$sub_total_c_bill_count = $sub_total_c_bill_count + $cancel_bill->row['bill_count'];
	 	}

	  	$setting_value_link_1 = $this->get_settings('SMS_LINK_1',$this->conn);
	  	$setting_value_link_2 = $this->get_settings('SMS_LINK_2',$this->conn);
	  	$setting_value_link_3 = $this->get_settings('SMS_LINK_3',$this->conn);
	  	
		
  		$send_link = $setting_value_link_1;
		$link_1 = html_entity_decode($send_link, ENT_QUOTES, 'UTF-8');
		$setting_value_number = $this->get_settings('CONTACT_NUMBER',$this->conn);
		
		if ($send_link != '' && $setting_value_number != '') {
			$text =$time.'%20TSale%20'.$sub_total_sale.',%20Pending%20Tbl%20=%20'.$panding_tables_amt.',%20Cancel%20KOT/BOT%20=%20'.$sub_total_c_kot_amt.'%20ADOR%20INFOTECH%20Sah%20Taiyav.';
			$link = $link_1.'&Message='.$text.'&MobileNumbers='.$setting_value_number;
			$ret = file($link);
			// echo"<pre>";print_r($link);exit;
			//comment This two lines in production
			fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Message Status', true)  . "\n");
			fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($ret, true)  . "\n");
		}

  	}	
  	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}

	
}
?>