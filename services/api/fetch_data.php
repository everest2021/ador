<?php 
date_default_timezone_set("Asia/Kolkata");
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
//echo "inn";exit;
require_once('config.php');
// $file = BASE_LINK;
// $file = '/var/www/html/webhook_live/query.txt';
// $file = '/var/www/html/webhook_live_fordevloper/query.txt';
$file = 'C:/xampp/htdocs/ador_1/services/api/query.txt';
$handle = fopen($file, 'a+');
$data = file_get_contents('php://input');
error_reporting(E_ALL);
ini_set("display_errors", 1);
$datas = json_decode($data,true);
// $handle = fopen($file, 'a+'); 
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas, $handle);
// fclose($handle);

exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($datas, $handle){
		fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r('----------inn getitem--------', true)  . "\n");
		$result1 = array();
		$results = $this->query("SELECT * FROM oc_orders_app WHERE entry_status = 0 AND online_order_id > 0 ", $this->conn)->rows;
		
		fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($results, true)  . "\n");
		$customer_details = array();
		$customer_add_details = array();
		foreach ($results as $result) {
			fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r('----------inn getitem--------', true)  . "\n");
			$cust_details = $this->query("SELECT * FROM oc_customer_app WHERE customer_id = '".$result['cust_id']."' ", $this->conn);
			
			fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($cust_details, true)  . "\n");
			
			$add_details = $this->query("SELECT * FROM oc_address_app WHERE customer_id = '".$result['cust_id']."' AND online_order_id = '".$result['online_order_id']."' ", $this->conn);
			
			fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($add_details, true)  . "\n");

			$customer_details = array(
				'customer_id'        => $cust_details->row['customer_id'],
				'firstname'        => $cust_details->row['firstname'],
				'email'        => $cust_details->row['email'],
				'telephone'        => $cust_details->row['telephone'],
				'date_added'  => $cust_details->row['date_added'],
			);
			fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($customer_details, true)  . "\n");
			$customer_add_details = array(
				'address_id'        => $add_details->row['address_id'],
				'customer_id'        => $add_details->row['customer_id'],
				'address_1'        => $add_details->row['address_1'],
				'address_2'        => $add_details->row['address_2'],
				'city'        => $add_details->row['city'],
				'postcode'        => $add_details->row['postcode'],
				'area'        => $add_details->row['area'],
				'sub_locality'        => $add_details->row['sub_locality'],
				'country_id'        => $add_details->row['country_id'],
				'zone_id'        => $add_details->row['zone_id'],
				'firstname'        => $add_details->row['firstname'],
			);
			fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($customer_add_details, true)  . "\n");
			$online_order_item = array();
			$order_details = $this->query("SELECT * FROM oc_order_item_app WHERE order_id = '".$result['order_id']."' ", $this->conn)->rows;
			foreach ($order_details as $okey => $ovalue) {
				$online_order_item[] = array(
					'item_app_id'        => $ovalue['item_app_id'],
					'online_order_id'        => $ovalue['online_order_id'],
					'order_id'        => $ovalue['order_id'],
					'online_order_status'        => $ovalue['online_order_status'],
					'item_id'        => $ovalue['item_id'],
					'item' => $ovalue['item'],
					'qty' => $ovalue['qty'],
					'price' => $ovalue['price'],
					'total' => $ovalue['total'],
					'gst' => $ovalue['gst'],
					'gst_val' => $ovalue['gst_val'],
					'grand_tot' => $ovalue['grand_tot'],
					'online_order_type_placed_delie' => $ovalue['online_order_type_placed_delie'],
					'discount_per' => $ovalue['discount_per'],
					'discount_value' => $ovalue['discount_value'],

				);
			}
			$result1['online_orders'][] = array(
				'order_id' => $result['order_id'],
				'packaging' => $result['packaging'],
				'delivery_charge' => $result['delivery_charge'],
				'total_charge' => $result['total_charge'],
				'cust_id' => $result['cust_id'],
				'tot_qty' =>  $result['tot_qty'], 
				'total' =>  $result['total'], 
				'grand_tot'  =>  $result['grand_tot'], 
				'tot_gst' =>  $result['tot_gst'],
				'gst_val' =>  $result['gst_val'],
				'order_time' =>  $result['order_time'], 
				'order_date' =>  $result['order_date'], 
				'address_1' =>  $result['address_1'], 
				'address_2' =>  $result['address_2'], 
				'city' =>  $result['city'],
				'postcode' =>  $result['postcode'],
				'order_status' =>  $result['order_status'], 
				'area' =>  $result['area'] ,
				'online_order_status' => $result['online_order_status'],
				'online_order_type_placed_delie' =>  $result['online_order_type_placed_delie'],
				'online_order_id' =>  $result['online_order_id'],
				'online_channel' =>  $result['online_channel'],
				'online_instruction' =>  $result['online_instruction'],
				'online_deliverydatetime' =>  $result['online_deliverydatetime'],
				'online_created_time' =>  $result['online_created_time'],
				'online_store_id' =>  $result['online_store_id'],
				'online_biz_id' =>  $result['online_biz_id'],
				'online_payment_method' =>  $result['online_payment_method'],
				'kot_status' => $result['kot_status'],
				'online_order_item' => $online_order_item,
				'customer_details' => $customer_details,
				'customer_add_details' => $customer_add_details,
				'roundtotal' => $result['roundtotal'],
				'pay_online' => $result['pay_online'],
				'ftotal_discount' => $result['ftotal_discount'],
				'ftotalvalue' => $result['ftotalvalue'],
				'discount' => $result['discount'],
				'discount_reason' => $result['discount_reason'],
				'online_deliery_type' => $result['online_deliery_type'],
				'zomato_order_type' => $result['zomato_order_type'],
				'zomato_order_id' => $result['zomato_order_id'],
				'zomato_discount' => $result['zomato_discount'],
				'zomato_rider_otp' => $result['zomato_rider_otp'],
				'firstname' => $result['firstname'],
				'email' => $result['email'],
				'telephone' => $result['telephone'],
			);
		} //echo "<pre>";print_r($result1);exit;
		fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($result1, true)  . "\n");
		return $result1;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>