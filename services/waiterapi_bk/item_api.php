<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);

$item_api = new item_api();
$value = $item_api->getItemdata($datas);
exit(json_encode($value));

class item_api {
	public $conn;
	public function __construct() {
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getItemdata($data = array()){
		$result = array();
		
		// echo'<pre>';
		// print_r($data);
		// exit;

		if(isset($data['rate_id'])){
			$data['rate_id'] = $data['rate_id'];
		} else {
			$data['rate_id'] = '1';
		}

		$rates = array(
			'1' => 'rate_1',
			'2' => 'rate_2',
			'3' => 'rate_3',
			'4' => 'rate_4',
			'5' => 'rate_5',
			'6' => 'rate_6',
		);


		$sql = "SELECT * FROM  `oc_item` WHERE 1=1";
		if(isset($data['sub_category_id'])){
			$sql .=" AND item_sub_category_id ='".$data['sub_category_id']."'"; 	
		}

		if(isset($data['item_code'])){
			$sql .=" AND item_code ='".$data['item_code']."'"; 	
		}
		
		if(isset($data['item_name']) && $data['item_name'] !=''){
			if(!is_numeric($data['item_name'])){
				$sql .=" AND `item_name` LIKE '%" .$data['item_name']. "%'";
				$sql .= " OR short_name LIKE '%" . ($data['item_name']) . "%'";
			}
		}
		if(isset($data['item_name']) && $data['item_name'] !=''){
			if(is_numeric($data['item_name'])){
				$sql .=" AND item_code ='".$data['item_name']."'";
			}
		}
		if(isset($data['item_code']) || isset($data['item_name']) && $data['item_name'] !=''){
			$sql .=" ORDER BY `item_name` LIMIT 0,10";
		
		}
		$result['item'] = $this->query($sql,$this->conn)->rows;
		foreach ($result['item'] as $key => $value) {
			if(isset($rates[$data['rate_id']])){
				$rate_key = $rates[$data['rate_id']];
			} else {
				$rate_key = 'rate_1';
			}


			//echo '<pre>';
			//print_r($value);
			$rate = $value[$rate_key];
			$result['item'][$key]['rate_1'] = $rate;
			$result['item'][$key]['tax1_value'] = 0;
			$result['item'][$key]['tax2_value'] = 0;
			if($value['vat'] > 0){
				$vat = $this->query("SELECT `tax_value` FROM `oc_tax` WHERE `id`='".$value['vat']."' ",$this->conn);
				if($vat->num_rows > 0){
					$result['item'][$key]['tax1_value'] =$vat->row['tax_value'];
				}
			}
			if($value['tax2'] > 0){
				$tax2 = $this->query("SELECT `tax_value` FROM `oc_tax` WHERE `id`='".$value['vat']."' ",$this->conn);
				if($tax2->num_rows > 0){
					$result['item'][$key]['tax2_value'] =$tax2->row['tax_value'];
				}
			}
			
		}
		$result['rate_change'] = $this->query("SELECT `value` FROM settings_ador WHERE `key` = 'RATE_CHANGE'",$this->conn)->row['value']; 
		
		// echo '<pre>';
		// print_r($sql);
		// exit;
		return $result;
	}
	
}

?>