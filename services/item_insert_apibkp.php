<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);

$file = '/var/www/html/ador/services/waiterapi/service.txt';
//$file = FILE_PATH;
$handle = fopen($file, 'a+'); 
fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r($datas, true)  . "\n");
fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r("datss innn insert api", true)  . "\n");

$item_insert_api = new item_insert_api();

$value = $item_insert_api->getItemInsertdata($datas,$handle);

exit(json_encode($value));
fclose($handle); 

class item_insert_api {
	public $conn;
	public function __construct() {
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}


	public function getItemInsertdata($data = array(), $handle){
		
		$json = array();
		$result = array();
		$data = $data[0];
		//echo '<pre>';
		//print_r($data);
		//exit;
		// fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r($data, true)  . "\n");

		$order_info_report_id = $this->query("SELECT order_id from oc_order_info_report order by order_id DESC LIMIT 1",$this->conn);
		//if($order_info_report_id->num_rows > 0){
			$order_info_id = $this->query("SELECT order_id from oc_order_info  order by order_id DESC LIMIT 1",$this->conn);

			if($order_info_id->num_rows == 0){
				$neworder_id = $order_info_report_id->row['order_id'] + 1;
				$this->query("ALTER TABLE oc_order_info AUTO_INCREMENT = ".$neworder_id." ",$this->conn);
			}
		//}

		$order_items_report_id = $this->query("SELECT id from oc_order_items_report order by id DESC LIMIT 1",$this->conn);
		//if($order_items_report_id->num_rows > 0){
			$order_items_id = $this->query("SELECT id from oc_order_items  order by id DESC LIMIT 1",$this->conn);

			if($order_items_id->num_rows == 0){
				$neworder_itemid = $order_items_report_id->row['id'] + 1;
				$this->query("ALTER TABLE oc_order_items AUTO_INCREMENT = ".$neworder_itemid." ",$this->conn);
			}
		//}
		$ftotal = 0;
		$ftotal_discount = 0;
		$ftotalvalue = 0;
		$gst = 0;
		$staxfood = 0;
		$ltotal = 0;
		$ltotal_discount = 0;
		$ltotalvalue = 0;
		$vat = 0;
		$staxliq = 0;
		$stax = 0;
		foreach($data['item_datas'] as $pkeys => $pvalues){
			$pvalues['item_amount_original'] = $pvalues['item_amount_original'];

			if(!isset($pvalues['is_liq'])){
				$pvalues['is_liq'] = 0;
			}

			if($pvalues['cancelstatus'] == 0 && $pvalues['nc_kot_status'] == 0){
				if($pvalues['is_liq'] == '0' || $pvalues['is_liq'] == 0){
					$ftotal = ($ftotal) + $pvalues['item_amount_original'];
				} else if($pvalues['is_liq'] == '1' || $pvalues['is_liq'] == 1) {
					$ltotal = $ltotal + $pvalues['item_amount_original'];
				}
			}
		}
		
		$data['fdiscountper'] = 0;
		$data['ldiscountper'] = 0;
		$data['ldiscount'] = 0;
		$data['fdiscount'] = 0;

		$data['discount'] = 0;
		$data['service_charge'] = 0;
		$servicechargefood =  $this->query("SELECT `value` FROM settings_ador WHERE `key` = 'SERVICE_CHARGE_FOOD'",$this->conn)->row['value'];
		$servicechargeliq = $this->query("SELECT `value` FROM settings_ador WHERE `key` = 'SERVICE_CHARGE_LIQ'",$this->conn)->row['value']; 
		
		foreach($data['item_datas'] as $pkeys => $pvalues){
		
			$pvalues['item_amount_original'] = $pvalues['item_amount_original'];
			
			if($pvalues['cancelstatus'] == 0 && $pvalues['nc_kot_status'] == 0){
				if($pvalues['is_liq'] == 0){
					if($data['fdiscountper'] > 0){
						$discount_value = $pvalues['item_amount_original'] * ($data['fdiscountper']/100);
						$afterdiscountamt = ($pvalues['item_amount_original'] - $discount_value);
					} elseif($data['discount'] > 0){
						$discount_per = (($data['discount'])/$ftotal)*100;
						$discount_value = $pvalues['item_amount_original'] * ($discount_per/100);
						$afterdiscountamt = ($pvalues['item_amount_original'] - $discount_value);
					} else {
						$afterdiscountamt = ($pvalues['item_amount_original']);
						$discount_value = 0;
					}
					$staxfood_1 = 0;
					if($servicechargefood > 0){
						if($data['service_charge'] == 1){
							$staxfood_1 = ($afterdiscountamt*($servicechargefood/100));		
						}else{
							$staxfood_1 = 0;
						}
					}
					$staxfood = $staxfood + $staxfood_1;
					$tax1_value = (($afterdiscountamt) + ($staxfood_1)) * (($pvalues['tax1_value']) / 100);
			  		$tax2_value = (($afterdiscountamt) + ($staxfood_1)) * (($pvalues['tax2_value']) / 100);
			  		
			  		$data['item_datas'][$pkeys]['tax1_amount'] = $tax1_value;
			  		$data['item_datas'][$pkeys]['tax2_amount'] = $tax2_value;
			  		$data['item_datas'][$pkeys]['stax'] = $staxfood_1;

			  		$gst = $gst + $tax1_value + $tax2_value;
			  		$ftotalvalue = $ftotalvalue + $discount_value;
			  		$ftotal_discount = $ftotal_discount + $afterdiscountamt; 
				} else {
					if($data['ldiscountper'] > 0){
						$discount_value = $pvalues['item_amount_original'] * ($data['ldiscountper']/100);
						$afterdiscountamt = $pvalues['item_amount_original'] - $discount_value;
					} elseif($data['ldiscount'] > 0){
						$discount_per = (($data['ldiscount'])/$ltotal)*100;
						$discount_value = $pvalues['item_amount_original'] * ($discount_per/100);
						$afterdiscountamt = $pvalues['item_amount_original'] - $discount_value;
					} else {
						$afterdiscountamt = $pvalues['item_amount_original'];
						$discount_value = 0;
					}
					$staxliq_1 = 0;
					if($servicechargeliq > 0){
						if ($data['service_charge'] == 1) {
							$staxliq_1 = $afterdiscountamt*($servicechargeliq/100);		
						}else{
							$staxliq_1 = 0;
						}
					}
					$staxliq = $staxliq + $staxliq_1;
					$tax1_value = ($afterdiscountamt + $staxliq_1) * ($pvalues['tax1_value'] / 100);
			  		$tax2_value = ($afterdiscountamt + $staxliq_1) * ($pvalues['tax2_value'] / 100);
			  		
			  		$data['item_datas'][$pkeys]['tax1_amount'] = $tax1_value;
			  		$data['item_datas'][$pkeys]['tax2_amount'] = $tax2_value;
			  		$data['item_datas'][$pkeys]['stax'] = $staxliq_1;

			  		$vat = $vat + $tax1_value + $tax2_value;
			  		$ltotalvalue = $ltotalvalue + $discount_value;
			  		$ltotal_discount = $ltotal_discount + $afterdiscountamt; 
				}
			} else {
				$data['item_datas'][$pkeys]['stax'] = 0;
			}
			$data['nc_kot_status'] = $pvalues['nc_kot_status'];
		}
		$stax = $staxfood + $staxliq;
		$INCLUSIVE=$this->query("SELECT `value` FROM settings_ador WHERE `key` = 'INCLUSIVE'",$this->conn)->row['value'];
		if($INCLUSIVE == 1){
			$grand_total = ($ftotal + $ltotal + $staxfood + $staxliq) - ($ftotalvalue + $ltotalvalue);
		} else{
			$grand_total = ($ftotal + $ltotal + $gst + $vat + $staxfood + $staxliq) - ($ftotalvalue + $ltotalvalue);
		}


		$grand_totals = number_format($grand_total,2,'.','');
		$grand_total_explode = explode('.', $grand_totals);
		$roundtotal = 0;
		if(isset($grand_total_explode[1])){
			if($grand_total_explode[1] >= 50){
				$roundtotals = 100 - $grand_total_explode[1];
				$roundtotal = ($roundtotals / 100);
			} else {
				$roundtotal = -($grand_total_explode[1] / 100);
			}	
		}

		
		$data['dchargeper'] = 0;
		$data['dcharge'] = 0;
		$dtotalvalue = 0;
		if($data['dchargeper'] > 0){
			$dtotalvalue = $grand_total * ($data['dchargeper'] / 100);
		} elseif($data['dcharge'] > 0){
			$dtotalvalue = $data['dcharge'];
		}

		$data['ftotal'] = $ftotal;
		$data['ftotal_discount'] = $ftotal_discount;
		$data['ftotalvalue'] = $ftotalvalue;
		$data['staxfood'] = $staxfood;
		$data['ltotal'] = $ltotal;
		$data['ltotal_discount'] = $ltotal_discount;
		$data['ltotalvalue'] = $ltotalvalue;
		$data['staxliq'] = $staxliq;
		$data['stax'] = $stax;
		$data['grand_total'] = $grand_total;
		$data['roundtotal'] = $roundtotal;
		$data['dtotalvalue'] = $dtotalvalue;

		

		date_default_timezone_set("Asia/Kolkata");
		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->query($last_open_date_sql,$this->conn);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		if($data['nc_kot_status'] == 1){
			$data['nc_kot_status'] = 1;
		} else {
			$data['nc_kot_status'] = 0;
		}
		
		// //********************** Start : To Make KOT number different for liquior day wise ******************************* //
		$kotno2 = $this->query("SELECT oit.`kot_no` FROM `oc_order_items` oit  WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by oit.`kot_no` DESC LIMIT 1",$this->conn);
		if($kotno2->num_rows > 0){
			$kot_no2 = $kotno2->row['kot_no'];
			$kotno = $kot_no2 + 1;
		} else{
			$kotno = 1;
		}
		
		
		$kotno1 = $this->query("SELECT oit.`kot_no` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 1 order by oit.`kot_no` DESC LIMIT 1",$this->conn);
		if($kotno1->num_rows > 0){
			$kot_no1 = $kotno1->row['kot_no'];
			$botno = $kot_no1 + 1;
		} else{
			$botno = 1;
		}


		$kot_no_datas = $this->query("SELECT `kot_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' ORDER BY `kot_no` DESC LIMIT 1",$this->conn)->row;
		$o_kot_no = 1;
		if(isset($kot_no_datas['kot_no'])){
			$o_kot_no = $kot_no_datas['kot_no'] + 1;
		}
		$date_added = date('Y-m-d');
		$time_added = date('H:i:s');


		$testdata['name'] = '';
		$testdata['c_id'] = '';
		$testdata['contact'] = '';
		$testdata['address'] = '';
		$testdata['email'] = '';
		$testdata['gst_no'] = '';


		if(!isset($data['waiter'])){
			$data['waiter'] = '';
		}

		if(!isset($data['waiter_id'])){
			$data['waiter_id'] = '';
		}

		if(!isset($data['waiterid'])){
			$data['waiterid'] = '';
		}

		if(!isset($data['captain'])){
			$data['captain'] = '';
		}

		if(!isset($data['captainid'])){
			$data['captainid'] = '';
		}

		if(!isset($data['captain_id'])){
			$data['captain_id'] = '';
		}

		if(!isset($data['person'])){
			$data['person'] = '';
		}

		if(!isset($data['type'])){
			$data['type'] = '';
		}

		if($data['type'] == 'Captain'){
			$data['captain'] = $data['waiter'];
			$data['captain_id'] = $data['waiter_id'];
			$data['captain_code'] = $data['waiter_code'];

			$data['waiter'] = '';
			$data['waiter_id'] = 0;
			$data['waiter_code'] = 0;

		} else {
			$data['waiter'] = $data['waiter'];
			$data['waiter_id'] = $data['waiter_id'];
			$data['waiter_code'] = $data['waiter_code'];

			$data['captain'] = '';
			$data['captain_id'] = 0;
			$data['captain_code'] = 0;
		}


		// echo'<pre>';
		// print_r($data);
		// exit;
		$data['cess'] = '';
		$data['ldiscount'] = 0;
		$data['advance_id'] = 0;
		$data['advance_amount'] = 0;
		if($data['order_id'] != '' || $data['order_id'] != '0' || $data['order_id'] != NULL){

			$this->query("UPDATE " . DB_PREFIX . "order_info SET  
				`app_order_id` = '" . ($data['apporderid']) . "',
				`location` = '" . ($data['location']) . "',
				`location_id` = '" . ($data['location_id']) ."',
				`parcel_status` = '" . ($data['parcel_status']) ."',
				`cust_name` = '" . ($testdata['name']) ."',
				`cust_id` = '" . ($testdata['c_id']) ."',
				`cust_contact` = '" . ($testdata['contact']) ."',
				`cust_address` = '" . ($testdata['address']) ."',
				`cust_email` = '" . ($testdata['email']) ."',
				`gst_no` = '" . ($testdata['gst_no']) ."',
				`t_name` = '" . ($data['table']) . "',
				`table_id` = '" . ($data['table_id']) . "',  
				`waiter_code` = '" . ($data['waiter_code']) . "',
				`waiter` = '" . ($data['waiter']) . "',  
				`waiter_id` = '" . ($data['waiter_id']) . "',
				`captain_code` = '" . ($data['captain_code']) . "',  
				`captain` = '" . ($data['captain']) . "',  
				`captain_id` = '" . ($data['captain_id']) . "',
				`person` = '" . ($data['person']) . "',
				`ftotal` = '" . ($data['ftotal']) . "',
				`ftotal_discount` = '" . ($data['ftotal_discount']) . "',
				`gst` = '" . ($data['gst']) . "',
				`ltotal` = '" . ($data['ltotal']) . "',
				`ltotal_discount` = '" . ($data['ltotal_discount']) . "',
				`vat` = '" . ($data['vat']) . "',
				`cess` = '" . ($data['cess']) . "',
				`staxfood` = '" . ($data['staxfood']) . "',
				`staxliq` = '" . ($data['staxliq']) . "',
				`stax` = '" . ($data['stax']) . "',
				`ftotalvalue` = '" . ($data['ftotalvalue']) . "',
				`fdiscountper` = '" . ($data['fdiscountper']) . "',
				`discount` = '" . ($data['discount']) . "',
				`ldiscount` = '" . ($data['ldiscount']) . "',
				`ldiscountper` = '" . ($data['ldiscountper']) . "',
				`ltotalvalue` = '" . ($data['ltotalvalue']) . "',
				`dtotalvalue` = '" . ($data['dtotalvalue']) . "',
				`dchargeper` = '" . ($data['dchargeper']) . "',
				`dcharge` = '" . ($data['dcharge']) . "',
				`advance_billno` = '" . ($data['advance_id']) . "',
				`advance_amount` = '" . ($data['advance_amount']) . "',
				`grand_total` = '" . ($data['grand_total']) . "',
				`roundtotal` = '" . ($data['roundtotal']) . "',
				`total_items` = '" . ($data['total_items']) . "',
				`item_quantity` = '" . ($data['item_quantity']) . "',
				`nc_kot_status` = '" . ($data['nc_kot_status']) . "',
				`rate_id` = '" . ($data['rate_id']) . "',
				`bill_date` = '".$last_open_date."',
				`date_added` = '".$date_added."',
				`time_added` = '".$time_added."',
				`kot_no` = '".$o_kot_no."',
				`date` = '" . (DATE('Y-m-d')) . "',
				`time` = '" . (DATE('H:i:s')) . "',
				`login_id` = '".$data['login_id']."',
				`login_name` = '".$data['login_name']."'
				WHERE order_id = '".$data['order_id']."'
				",$this->conn);
				$order_ids = $data['order_id'];
		} else {
		 	$this->query("INSERT INTO " . DB_PREFIX . "order_info SET  
				`app_order_id` = '" . ($data['apporderid']) . "',
				`location` = '" . ($data['location']) . "',
				`location_id` = '" . ($data['location_id']) ."',
				`parcel_status` = '" . ($data['parcel_status']) ."',
				`cust_name` = '" . ($testdata['name']) ."',
				`cust_id` = '" . ($testdata['c_id']) ."',
				`cust_contact` = '" . ($testdata['contact']) ."',
				`cust_address` = '" . ($testdata['address']) ."',
				`cust_email` = '" . ($testdata['email']) ."',
				`gst_no` = '" . ($testdata['gst_no']) ."',
				`t_name` = '" . ($data['table']) . "',
				`table_id` = '" . ($data['table_id']) . "',  
				`waiter_code` = '" . ($data['waiter_code']) . "',
				`waiter` = '" . ($data['waiter']) . "',  
				`waiter_id` = '" . ($data['waiter_id']) . "',
				`captain_code` = '" . ($data['captain_code']) . "',  
				`captain` = '" . ($data['captain']) . "',  
				`captain_id` = '" . ($data['captain_id']) . "',
				`person` = '" . ($data['person']) . "',
				`ftotal` = '" . ($data['ftotal']) . "',
				`ftotal_discount` = '" . ($data['ftotal_discount']) . "',
				`gst` = '" . ($data['gst']) . "',
				`ltotal` = '" . ($data['ltotal']) . "',
				`ltotal_discount` = '" . ($data['ltotal_discount']) . "',
				`vat` = '" . ($data['vat']) . "',
				`cess` = '" . ($data['cess']) . "',
				`staxfood` = '" . ($data['staxfood']) . "',
				`staxliq` = '" . ($data['staxliq']) . "',
				`stax` = '" . ($data['stax']) . "',
				`ftotalvalue` = '" . ($data['ftotalvalue']) . "',
				`fdiscountper` = '" . ($data['fdiscountper']) . "',
				`discount` = '" . ($data['discount']) . "',
				`ldiscount` = '" . ($data['ldiscount']) . "',
				`ldiscountper` = '" . ($data['ldiscountper']) . "',
				`ltotalvalue` = '" . ($data['ltotalvalue']) . "',
				`dtotalvalue` = '" . ($data['dtotalvalue']) . "',
				`dchargeper` = '" . ($data['dchargeper']) . "',
				`dcharge` = '" . ($data['dcharge']) . "',
				`advance_billno` = '" . ($data['advance_id']) . "',
				`advance_amount` = '" . ($data['advance_amount']) . "',
				`grand_total` = '" . ($data['grand_total']) . "',
				`roundtotal` = '" . ($data['roundtotal']) . "',
				`total_items` = '" . ($data['total_items']) . "',
				`item_quantity` = '" . ($data['item_quantity']) . "',
				`nc_kot_status` = '" . ($data['nc_kot_status']) . "',
				`rate_id` = '" . ($data['rate_id']) . "',
				`bill_date` = '".$last_open_date."',
				`date_added` = '".$date_added."',
				`time_added` = '".$time_added."',
				`kot_no` = '".$o_kot_no."',
				`date` = '" . (DATE('Y-m-d')) . "',
				`time` = '" . (DATE('H:i:s')) . "',
				`login_id` = '".$data['login_id']."',
				`login_name` = '".$data['login_name']."'",$this->conn);
			$order_ids = $this->getLastId($this->conn);

		} 
		

		// echo'<pre>';
		// print_r($order_ids);
		// exit;
		$itemdata = '';

		
		foreach($data['item_datas'] as $pkey => $pvalues){

			if(!isset($pvalues['nc_kot_status'])){
				$pvalues['nc_kot_status'] = 0;
			}
			if(!isset($pvalues['is_liq'])){
				$pvalues['is_liq'] = 0;
			}
			if(!isset($pvalues['kitchen_display'])){
				$pvalues['kitchen_display'] = 0;
			}
			if(!isset($pvalues['nc_kot_reason'])){
				$pvalues['nc_kot_reason'] = '';
			}
			if(!isset($pvalues['transfer_qty'])){
				$pvalues['transfer_qty'] = 0;
			}

			// if($pvalues['ismodifier'] == '0'){
			// 	$pvalues['parent_id'] = $autoincrementparentid;
			// }

			if(isset($pvalues['item_code']) && $pvalues['item_code'] != '') {
				if(isset($pvalues['kot_status'])) {
		            $kot_status = $pvalues['kot_status'];
		        } else {
		            $kot_status = 0;
		        }
	        
		        if(isset($pvalues['pre_qty'])){
		          	if($pvalues['item_quantity'] > $pvalues['pre_qty']){
						$prefix = '+';
						$pre_qty =$pvalues['pre_qty'];
					} else {
						$prefix = '-';
						$pre_qty =$pvalues['pre_qty'];
					}
				} else {
					$prefix = ' ';
					$pre_qty =0;
				}
					
				if($pvalues['is_liq'] == '1'){
					if($pvalues['kot_no'] != '0'){
						$c_no = $pvalues['kot_no'];
					} else {
						$c_no = $botno;
					}
				} else {
					if($pvalues['kot_no'] != '0'){
						$c_no = $pvalues['kot_no'];
					} else {
						$c_no = $kotno;
					}
				}
			}

			if ($data['captain_id'] != '') {
				$itemdata = $this->query("SELECT * FROM oc_item WHERE item_code = '".$pvalues['item_code']."'",$this->conn)->row;
				if ($itemdata['captain_rate_value'] == 1 ) {
					$caption_commition = $pvalues['item_quantity'] * $itemdata['captain_rate_per'];
				} else {
					$caption_commition = $pvalues['item_quantity'] *$pvalues['item_rate_original']/100 * $itemdata['captain_rate_per'];
				}
			} else {
				$caption_commition = '0';
			}

			if ($data['waiter_id'] != '') {
				$itemdata = $this->query("SELECT * FROM oc_item WHERE item_code = '".$pvalues['item_code']."'",$this->conn)->row;
				if ($itemdata['stweward_rate_value'] == 1) {
					$stweward_commition = $pvalues['item_quantity'] * $itemdata['stweward_rate_per'];
				}else{
					$stweward_commition = $pvalues['item_quantity'] * $pvalues['item_rate_original']/100 * $itemdata['stweward_rate_per'];
				}
			} else {
				$stweward_commition = '0';
			}

			$nc_kot_reason = '';
			$transfer_qty = 0;
			if(isset($pvalues['message'])){
				$message = $pvalues['message'];
			}else{
				$message = '';	
			}
			$discount_per = 0;
			$discount_value = 0;
			$parent_id = 0;
			$parent = 0;
		// 	echo "<pre>";
		// print_r($pvalues);
		// exit;
		
			if($pvalues['is_new'] == 0 ){
					
				$this->query("INSERT INTO " . DB_PREFIX . "order_items SET
				`order_id` = '" . ($order_ids) . "',  
				`code` = '" . ($pvalues['item_code']) ."',
				`name` = '" . htmlspecialchars_decode($pvalues['item_name']) ."',
				`qty` = '" . ($pvalues['item_quantity']) . "',
				`rate` = '" . ($pvalues['item_rate_original']) . "',  
				`amt` = '" . ($pvalues['item_amount_original']) . "',
				`ismodifier` = '1',
				`nc_kot_status` = '" . ($pvalues['nc_kot_status']) . "',
				`nc_kot_reason` = '" . ($nc_kot_reason) . "',
				`transfer_qty` = '" . ($transfer_qty) . "',
				`parent_id` = '" . ($parent_id) . "',
				`parent` = '" . ($parent) . "',  
				`subcategoryid` = '" . ($pvalues['item_sub_category_id']) . "',
				`kot_status` = '" .($kot_status). "',
				`kot_no` = '" .($c_no). "',
				`pre_qty` = '" .($pvalues['pre_qty']). "',
				`is_liq` = '" . ($pvalues['is_liq']) . "',
				`message` = '" . ($message) . "',
				`discount_per` = '" . ($discount_per) . "',
				`discount_value` = '" . ($discount_value) . "',
				`tax1` = '" . ($pvalues['tax1_value']) . "',
				`tax2` = '" . ($pvalues['tax2_value']) . "',
				`tax1_value` = '" . ($pvalues['tax1_amount']) . "',
				`tax2_value` = '" . ($pvalues['tax2_amount']) . "',
				`stax` = '" . ($pvalues['stax']) . "',
				`captain_id` = '" . ($data['captain_id']) . "',
				`captain_commission` = '" . ($caption_commition) . "',
				`waiter_id` = '" . ($data['waiter_id']) . "',
				`waiter_commission` = '" . ($stweward_commition) . "',
				`cancelstatus` = '" . ('0') . "',
				`new_qty` = '" . ($pvalues['item_quantity']) . "',
				`new_rate` = '" . ($pvalues['item_rate_original']) . "',
				`new_amt` = '" . ($pvalues['item_amount_original']) . "',
				`new_discount_per` = '0',
				`new_discount_value` = '0',
				`kitchen_display` = '" . ($pvalues['kitchen_display']) . "',
				`bill_date` = '".$last_open_date."',
				`date` = '".date('Y-m-d')."',
				`time` = '".date('H:i:s')."',
				`login_id` = '".$data['login_id']."',
				`login_name` = '".$data['login_name']."'",$this->conn);
			} 
		}
		$json['status'] = 1;
		if($order_ids != 0){
			$json['order_id'] = $order_ids;
		}
		fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r('innnnnnnnnnnnnnnnnnn', true)  . "\n");

		fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r($json, true)  . "\n");

		return $json;
		
	}
}

?>