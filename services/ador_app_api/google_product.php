<?php 
date_default_timezone_set("Asia/Kolkata");
require_once('config.php');

$googleProduct = new googleProduct();
$googleProduct->main();

class googleProduct {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function main(){
		$products = $this->query("SELECT * FROM oc_product p LEFT JOIN oc_product_description pd ON(p.product_id = pd.product_id)  WHERE p.status = 1 ",$this->conn)->rows;
		$xml = '<?xml version="1.0"?>
<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
	<channel>
		<title>Taazitokari - Online Store</title>
		<link>https://www.taazitokari.com/</link>
		<description>This is an online grocery store.</description>';
		foreach ($products as $product) {
			$query_special_price = $this->query("SELECT product_special_id, price FROM oc_product_special  WHERE product_id = '".$product["product_id"]."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1", $this->conn);
			if ($query_special_price->num_rows > 0) {
				$price = $query_special_price->row['price'];
			} else {
				$price = $product["price"];
			}
			$xml .= '
			<item>
				<g:id>'.$product["product_id"].'</g:id>
				<g:title>'.$product["meta_title"].'</g:title>
				<g:description>'.$product["meta_description"].'</g:description>
				<g:link>'.htmlspecialchars('https://taazitokari.com/index.php?route=product/product&product_id=').$product["product_id"].'</g:link>
				<g:image_link>https://taazitokari.com/image/'.$product["image"].'</g:image_link>
				<g:condition>Fresh</g:condition>';
				if ($product["stock_status_id"] == 7) {
					$xml .= '
				<g:availability>in stock</g:availability>';
				} elseif ($product["stock_status_id"] == 9) {
					$xml .= '
				<g:availability>out of stock</g:availability>';
				}
				$xml .= '
				<g:price>'.number_format($price,2).' INR</g:price>
				<g:shipping>
					<g:country>India</g:country>
					<g:service>Standard</g:service>
					<g:price>30.00 INR</g:price>
				</g:shipping>
				<g:brand>Taazi Tokari</g:brand>
			</item>';
		}
	$xml .= '
	</channel>
</rss>';
		$dom = new DOMDocument;
		$dom->preserveWhiteSpace = TRUE;
		$dom->loadXML($xml);
		$dom->save('../service/product.xml');
		echo "<script>window.close();</script>";
	}
}
 ?>