ALTER TABLE `oc_affiliate` ADD `referral_discount` VARCHAR(255) NOT NULL DEFAULT '' AFTER `date_added`;

ALTER TABLE `oc_customer` ADD `referral_code` VARCHAR(255) NOT NULL AFTER `otp`;

ALTER TABLE `oc_order` ADD `referral_code` VARCHAR(255) NOT NULL AFTER `delivery_time`;

ALTER TABLE `oc_order` CHANGE `referral_code` `referral_code` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';

ALTER TABLE `oc_customer` CHANGE `referral_code` `referral_code` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';

ALTER TABLE `oc_order_history` CHANGE `comment` `comment` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';

ALTER TABLE `oc_order` CHANGE `comment` `comment` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';

ALTER TABLE `oc_order` ADD `referral_dis_per` VARCHAR(255) NOT NULL DEFAULT '' AFTER `referral_code`;