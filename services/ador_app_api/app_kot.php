<?php
	require_once('config.php');
	$accept_link = ACCEPT_LINK;
	$con = mysqli_connect("localhost","root","","db_restaurant");

	// Check connection
	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
		exit();
	}
	$ordered_items = mysqli_query($con, "SELECT * FROM oc_order_item_app WHERE 1=1");
	$success = 0;
	if (isset($_GET['success'])) {
		$success = 1;
		unset($_GET['success']);
	}
?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

td, th {
	border: 1px solid #dddddd;
	text-align: left;
	padding: 8px;
}

tr:nth-child(even) {
	background-color: #dddddd;
}

.button {
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}

.button1 {background-color: #008CBA;} /* Green */
</style>
</head>
<body>

<h2 style="text-align: center;">Online Orders</h2>
<?php if ($success == 1) { ?>
	<h2 style="margin-bottom: 5px;">You had accepted order successfully!</h2>
<?php } ?>

<table>
	<tr>
		<th style="text-align: center;">Order Id</th>
		<th style="text-align: center;">Item</th>
		<th style="text-align: center;">Qty</th>
		<th style="text-align: center;">Price</th>
		<th style="text-align: center;">Special Notes</th>
		<th style="text-align: center;">Action</th>
	</tr>
	<?php $inn = 0; $order_id = ''; ?>
	<?php if ($ordered_items->num_rows > 0) { ?>
		<?php while($row = $ordered_items->fetch_assoc()) { ?>
			<tr>
				<td>
					<?php if (($order_id != $row['order_id']) || ($order_id != $row['order_id'])) { ?>
						<?php echo $row['order_id'] ?>
					<?php } ?>
				</td>
				<td><?php echo $row['item'] ?></td>
				<td style="text-align: right;"><?php echo $row['qty'] ?></td>
				<td style="text-align: right;"><?php echo round($row['total'], 2) ?></td>
				<td><?php echo $row['special_notes'] ?></td>
				<td><a class="button button1" href="<?php echo $accept_link.'&order_id='.$row['order_id'].'&item_id='.$row['item_id']; ?>">Accept</a></td>
			</tr>
			<?php $order_id = $row['order_id']; ?>
		<?php } ?>
	<?php } ?>
</table>

</body>
</html>