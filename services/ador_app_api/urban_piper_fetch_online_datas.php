<?php 
date_default_timezone_set("Asia/Kolkata");
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
//echo "inn";exit;
require_once('config.php');
// $file = BASE_LINK;
//$file = 'C:\xampp\htdocs\taazitokari_theme\service\service.txt';
error_reporting(E_ALL);
ini_set("display_errors", 1);

// $handle = fopen($file, 'a+'); 
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem();
// fclose($handle);

exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem(){
		$results = $this->query("SELECT * FROM oc_orders_app WHERE entry_status = 0 AND online_order_id > 0 ", $this->conn)->rows;
		$customer_details = array();
		$customer_add_details = array();
		foreach ($results as $result) {

			$cust_details = $this->query("SELECT * FROM oc_customer_app WHERE customer_id = '".$result['cust_id']."' ", $this->conn);
			$add_details = $this->query("SELECT * FROM oc_address_app WHERE customer_id = '".$result['cust_id']."' ", $this->conn);
			
			$customer_details = array(
				'customer_id'        => $cust_details->row['customer_id'],
				'firstname'        => $cust_details->row['firstname'],
				'email'        => $cust_details->row['email'],
				'telephone'        => $cust_details->row['telephone'],
			);

			$customer_add_details = array(
				'address_id'        => $add_details->row['address_id'],
				'customer_id'        => $add_details->row['customer_id'],
				'address_1'        => $add_details->row['address_1'],
				'address_2'        => $add_details->row['address_2'],
				'city'        => $add_details->row['city'],
				'postcode'        => $add_details->row['postcode'],
				'area'        => $add_details->row['area'],
				'sub_locality'        => $add_details->row['sub_locality'],
				'country_id'        => $add_details->row['country_id'],
				'zone_id'        => $add_details->row['zone_id'],
			);
			$online_order_item = array();
			$order_details = $this->query("SELECT * FROM oc_order_item_app WHERE order_id = '".$result['order_id']."' ", $this->conn)->rows;
			foreach ($order_details as $okey => $ovalue) {
				$online_order_item[] = array(
					'online_order_id'        => $ovalue['online_order_id'],
					'order_id'        => $ovalue['order_id'],
					'online_order_status'        => $ovalue['item_id'],
					'item_id'        => $ovalue['item'],
					'item' => $ovalue['special_notes'],
					'qty' => $ovalue['qty'],
					'price' => $ovalue['price'],
					'total' => $ovalue['total'],
					'gst' => $ovalue['gst'],
					'gst_val' => $ovalue['gst_val'],
					'grand_tot' => $ovalue['grand_tot'],
					'online_order_type_placed_delie' => $ovalue['online_order_type_placed_delie'],

				);
			}

			$result1['online_orders'][] = array(
				'order_id' => $result['order_id'],
				'cust_id' => $result['cust_id'],
				'tot_qty' =>  $result['tot_qty'], 
				'total' =>  $result['total'], 
				'grand_tot'  =>  $result['grand_tot'], 
				'tot_gst' =>  $result['tot_gst'],
				'gst_val' =>  $result['gst_val'],
				'order_time' =>  $result['order_time'], 
				'order_date' =>  $result['order_date'], 
				'address_1' =>  $result['address_1'], 
				'address_2' =>  $result['address_2'], 
				'city' =>  $result['city'],
				'postcode' =>  $result['postcode'],
				'order_status' =>  $result['order_status'], 
				'area' =>  $result['area'] ,
				'online_order_status' => $result['online_order_status'],
				'online_order_type_placed_delie' =>  $result['online_order_type_placed_delie'],
				'online_order_id' =>  $result['online_order_id'],
				'online_channel' =>  $result['online_channel'],
				'online_instruction' =>  $result['online_instruction'],
				'online_deliverydatetime' =>  $result['online_deliverydatetime'],
				'online_created_time' =>  $result['online_created_time'],
				'online_store_id' =>  $result['online_store_id'],
				'online_biz_id' =>  $result['online_biz_id'],
				'online_payment_method' =>  $result['online_payment_method'],
				'kot_status' => $result['kot_status'],
				'online_order_item' => $online_order_item,
				'customer_details' => $customer_details,
				'customer_add_details' => $customer_add_details,
			);
		} echo "<pre>";print_r($result1);exit;
		return $result1;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>