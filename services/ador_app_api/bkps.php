<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
//$file = '/var/www/html/taazitokari/service/service.txt';
$file = 'D:\xampp\htdocs\rewillax\service\service.txt';

$handle = fopen($file, 'a+'); 
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas, $handle);
fclose($handle);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array(), $handle){
		fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('-----Start------', true)  . "\n");
		fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($data, true)  . "\n");

		if(!isset($data['user_id'])){
			$data['user_id'] = '257';
		}
		if(!isset($data['address_id'])){
			$data['address_id'] = '262';
		}
		if(!isset($data['currentusercart'])){
			$data['currentusercart'] = array();
		}
		if(!isset($data['delivery_date'])){
			$data['delivery_date'] = '2019-09-02';
		}
		if(!isset($data['delivery_time'])){
			$data['delivery_time'] = '3';
		}

		if(!isset($data['ref_discount_per'])){
			$data['ref_discount_per'] = '5';
		}

		if(!isset($data['discount_amt'])){
			$data['discount_amt'] = '1.3';
		}

		if(!isset($data['referral_code'])){
			$data['referral_code'] = '5d2c406630214';
		}

		if(!isset($data['special_notes'])){
			$data['special_notes'] = 'test';
		}

		$result = array();
		$date = date('Y-m-d', strtotime($data['delivery_date']));
		if($data['currentusercart']){
			if($data['referral_code'] != ''){
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($data['referral_code'], true)  . "\n");

				$this->query("UPDATE `oc_customer` SET referral_code = '".$data['referral_code']."' WHERE customer_id = '".$data['user_id']."'", $this->conn);
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r("after update", true)  . "\n");

			}

			$address_datas = $this->query("SELECT * FROM `oc_address`  LEFT JOIN `oc_customer`  ON (oc_address.customer_id = oc_customer.customer_id) WHERE oc_address.`address_id` = '".$data['address_id']."' ",$this->conn);
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($address_datas, true)  . "\n");
			
			if($address_datas->num_rows > 0){
				$address_data = $address_datas->row;
				$store_url = 'https://taazitokari.com/service/';
				$this->query("INSERT INTO `oc_order` SET 
						ismobile = '1' ,
						invoice_no = '0' , 
						invoice_prefix = '',
						store_name = 'Taazi Tokari', 
						store_url = '".$store_url."', 
						store_id = '0', 
						customer_id = '" . $address_data['customer_id'] . "', 
						firstname = '" . $this->escape($address_data['firstname'], $this->conn). "', 
						lastname = '" .$this->escape($address_data['lastname'], $this->conn) . "', 
						email = '" .$address_data['email'] . "',
						telephone = '" . $address_data['telephone']. "',
						fax = '',
						custom_field = '',
						payment_address_format = '',
						payment_custom_field = '',
						payment_firstname = '" . $this->escape($address_data['firstname'], $this->conn) . "',
						payment_lastname = '" .$this->escape($address_data['lastname'], $this->conn) . "', 
						payment_address_1 = '" . $this->escape($address_data['address_1'], $this->conn) . "',
						payment_address_2 = '" . $this->escape($address_data['address_2'], $this->conn) . "',
						payment_city = '" . $this->escape($address_data['city'], $this->conn) . "', 
						payment_postcode = '" . $address_data['postcode'] . "', 
						payment_country = 'INDIA', 
						payment_country_id = '99',
						payment_zone_id = '1492',
						shipping_address_format = '',
						shipping_custom_field = '',
						referral_code = '".$this->escape($data['referral_code'], $this->conn)."',
						comment = '".$this->escape($address_data['special_notes'], $this->conn)."',
						payment_method = 'Cash on delivery', 
						shipping_firstname = '" . $this->escape($address_data['firstname'], $this->conn) . "', 
						shipping_lastname = '" . $this->escape($address_data['lastname'], $this->conn) . "', 
						shipping_address_1 = '" . $this->escape($address_data['address_1'], $this->conn) . "', 
						shipping_address_2 = '" . $this->escape($address_data['address_2'], $this->conn) . "', 
						shipping_city = '" .$this->escape($address_data['city'], $this->conn) . "', 
						shipping_postcode = '" . $address_data['postcode'] . "', 
						shipping_country = 'INDIA', shipping_country_id = '99',
						shipping_zone_id = '1492', 
						shipping_method = 'Flat shippinng Rate',  
						total = '0.00',
						language_id = '1', 
						currency_id = '4', 
						order_status_id = 2,
						currency_code = 'INR',
						date_added = NOW(), 
						date_modified = NOW(),
						location_id='1' , 
						delivery_date = '".$date."' , 
						delivery_time = '".$data['delivery_time']."' ",$this->conn);
				$order_id = $this->getLastId($this->conn);

				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Id : ' . $order_id, true)  . "\n");

				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($data['currentusercart'], true)  . "\n");

				$subtotal = 0;
				$base = 'https://taazitokari.com/image/';
				foreach($data['currentusercart']  as $ckey => $cvalue){
					$product_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id` = p.`product_id` WHERE pd.`product_id`= '".$ckey."' " ,$this->conn)->row;
					$subtotals = (int)$cvalue * $product_datas['price'];
					$subtotal += $subtotals;
					$this->query("INSERT INTO `oc_order_product` SET order_id = '" .$order_id. "', product_id = '" . $product_datas['product_id'] . "', name = '" . $this->escape($product_datas['name'], $this->conn) . "', model = 'Taazi Tokari', quantity = '" .(int)$cvalue. "', price = '" . $product_datas['price'] . "', total = '" .$subtotals. "', tax = '1', reward = '1', location_id = '1' ",$this->conn);
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_product` SET order_id = '" .$order_id. "', product_id = '" . $product_datas['product_id'] . "', name = '" . $this->escape($product_datas['name'], $this->conn) . "', model = 'Taazi Tokari', quantity = '" .(int)$cvalue. "', price = '" . $product_datas['price'] . "', total = '" .$subtotals. "', tax = '1', reward = '1', location_id = '1' ", true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('order Product Response', true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
				}

				$flat_cost = 30;
				if($subtotal > 200){
					$flat_cost = 0;
				}
				if($data['discount_amt'] != ''){
					$discount_amt = '-'.$data['discount_amt'];
					$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'referral_discount' ,title = 'Referral Discount' ,value='".$discount_amt."', sort_order = 1 ",$this->conn);
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'referral_discount' ,title = 'Referral Discount' ,value='".$discount_amt."', sort_order = 1  ", true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total Sub Total Response', true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");

				} else {
					$discount_amt = 0;
				}
				$total = $subtotal + $flat_cost - $discount_amt;

				if ($flat_cost > 0){
					$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'shippinng' ,title = 'Flat shipping rate' ,value='".$flat_cost."', sort_order = 3 ",$this->conn);
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'shippinng' ,title = 'Flat shipping rate' ,value='".$flat_cost."', sort_order = 3 ", true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total Shipping Response', true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
				}
				
				$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'sub_total' ,title = 'Sub-Total' ,value='".$subtotal."', sort_order = 1 ",$this->conn);
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'sub_total' ,title = 'Sub-Total' ,value='".$subtotal."', sort_order = 1 ", true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total Sub Total Response', true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");

				$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'total' ,title = 'total' ,value='".$total."' ,sort_order = 9 ",$this->conn);
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'total' ,title = 'total' ,value='".$total."' ,sort_order = 9 ", true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total Total Response', true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");

				$this->query("UPDATE `oc_order` SET total = '".$total."' WHERE `order_id` = '".$order_id."' ",$this->conn);
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("UPDATE `oc_order` SET total = '".$total."' WHERE `order_id` = '".$order_id."' ", true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Update Total Response', true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");

				$this->query("INSERT INTO `oc_order_history` SET order_id = '" .$order_id. "', order_status_id = 2, notify = 1, comment= '".$data['special_notes']."', date_added = NOW() ",$this->conn);
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_history` SET order_id = '" .$order_id. "', order_status_id = 2, notify = 1, comment=1, date_added = NOW() ", true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order History Insert Response', true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");

				$result['success'] = 1;
			} else {
				$result['success'] = 2;
			}
		} else {
			$result['success'] = 0;
		}
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($result, true)  . "\n");
		fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('-----End------', true)  . "\n");
		return $result;
	}
	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>