<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
		$this->conn->set_charset("utf8");
		$this->conn->query("SET SQL_MODE = ''");
	}
	
	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		if(!isset($data['itemname_search'])){
			$data['itemname_search'] = '';
		}
		if(!isset($data['subcategory_id'])){
			$data['subcategory_id'] = '';
		}
		if(!isset($data['currentusercart'])){
			$data['currentusercart'] = array();
		}
		if(!isset($data['user_id'])){
			$data['user_id'] = '';
		}
		
		$wholesaler_check = $this->query("SELECT is_wholesaler FROM oc_customer WHERE customer_id = '".$data['user_id']."' " ,$this->conn)->row;
		if(!isset($wholesaler_check['is_wholesaler'])){
			$wholesaler_check['is_wholesaler'] = 0;
		}

		$sql = "SELECT * FROM `oc_product` p LEFT JOIN `oc_product_description` pd ON pd.`product_id`= p.`product_id` LEFT JOIN  `oc_product_to_category` pc ON pc.`product_id`= p.`product_id` WHERE pc.`category_id`= '".$data['subcategory_id']."' AND p.`status` = '1' AND p.stock_status_id <> '9' AND p.`date_available` <= NOW() " ;
		if($data['itemname_search'] != ''){
			$sql .= " AND pd.`name` LIKE '%".$data['itemname_search']."%' ";
		}
		$sql .= " ORDER BY p.price ASC";	
		$item_datas = $this->query($sql, $this->conn);
		$base = 'http://taazitokari.com/image/';
		$result = array();
		if ($item_datas->num_rows > 0) {
			foreach($item_datas->rows as $nkey => $nvalue){
				if(isset($data['currentusercart'][$nvalue['product_id']])){
					$quantity = $data['currentusercart'][$nvalue['product_id']];
				} else {
					$quantity = (int)0;
				}
				$special_datass = "SELECT price FROM oc_product_special  WHERE product_id = '".$nvalue['product_id']."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1" ;
				$special_data = $this->query($special_datass, $this->conn);
				if($special_data->num_rows > 0){
					$isSpecialPrice = 1;
					$price = $special_data->row['price'];
				} else{
					$isSpecialPrice = 0;	
					$price = $nvalue['price'];
				}
				$result['item_datas'][] = array(
					'product_id' => $nvalue['product_id'], 
					'stock_status_id' => $nvalue['stock_status_id'],
					'name' => html_entity_decode($nvalue['name']),
					'description' => html_entity_decode($nvalue['description']),
					'is_wholesaler' => $wholesaler_check['is_wholesaler'],
					'wholesaler_quantity' => $nvalue['wholeseller_minimium_quantity'],
					'quantity' =>(int)$quantity,
					'price' => (int)$price,
					'isSpecialPrice' => $isSpecialPrice,
					'regularPrice' => (int)$nvalue['price'],
					'image' =>$base.$nvalue['image'],
				);
			}	
		}

		$total_items = 0;
		$total_price = 0;
		$item_price = 0;
		foreach($data['currentusercart'] as $key => $value){
			$item_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id`= p.`product_id` WHERE pd.`product_id`= '".$key."' AND p.`is_wholesaller`= '".$wholesaler_check['is_wholesaler']."'", $this->conn)->row;
			if($item_datas['stock_status_id'] == 9){
				$value = 0;
			}
			if($item_datas != array()){
				if ($wholesaler_check['is_wholesaler'] == '1') {
					$item_price = $item_datas['wholeseller_price'];
				} else {
					$item_price = $item_datas['price'];
				}
			}
			$special_datass = "SELECT price FROM oc_product_special  WHERE product_id = '".$key."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1" ;
			$special_data = $this->query($special_datass, $this->conn);
			if($special_data->num_rows > 0){
				$item_price = $special_data->row['price'];
				$isSpecialPrice = 1;
			} else{
				$isSpecialPrice = 0;
				if (isset($wholesaler_check['is_wholesaler']) && $wholesaler_check['is_wholesaler'] == '1') {
					$item_price = $item_datas['wholeseller_price'];
				} else {
					$item_price = $item_datas['price'];
				}
			}
			if($item_datas['stock_status_id'] != 9){
				$total_items = $total_items + $value;
				$total_price = $total_price + ($value * $item_price);
			}
		}
		$result['total_items'] = (int)$total_items;
		$result['total_price'] = (int)$total_price;
		
		if(isset($result['item_datas'])){
			$result['success'] = 1;
		} else {
			$result['success'] = 0;
		}
		
		return $result;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>