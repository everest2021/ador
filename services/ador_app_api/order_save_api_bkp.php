<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		
		if(!isset($data['user_id'])){
			$data['user_id'] = '';
		}
		if(!isset($data['address_id'])){
			$data['address_id'] = '';
		}
		if(!isset($data['currentusercart'])){
		
			$data['currentusercart'] = array();
		}
		if(!isset($data['delivery_date'])){
			$data['delivery_date'] = '';
		}
		if(!isset($data['delivery_time'])){
			$data['delivery_time'] = '';
		}

		$result = array();
		$date = date('Y-m-d', strtotime($data['delivery_date']));
		$address_data = $this->query("SELECT * FROM `oc_address` a LEFT JOIN `oc_customer` c ON c.`customer_id` = a.`customer_id` WHERE a.`address_id` = '".$data['address_id']."' ",$this->conn)->row;
		$store_url = 'https://taazitokari.com/';
		$this->query("INSERT INTO `oc_order` SET ismobile = '1' , store_name = 'Taazi Tokari', store_url = '".$store_url."', customer_id = '" . $address_data['customer_id'] . "', firstname = '" . $address_data['firstname']. "', lastname = '" .$address_data['lastname'] . "', email = '" .$address_data['email'] . "', telephone = '" . $address_data['telephone']. "', payment_firstname = '" . $address_data['firstname'] . "', payment_lastname = '" .$address_data['lastname'] . "', payment_address_1 = '" . $address_data['address_1'] . "', payment_address_2 = '" . $address_data['address_2'] . "', payment_city = '" . $address_data['city'] . "', payment_postcode = '" . $address_data['postcode'] . "', payment_country = 'INDIA', payment_country_id = '99', payment_zone_id = '1492',payment_method = 'Cash on delivery', shipping_firstname = '" . $address_data['firstname'] . "', shipping_lastname = '" . $address_data['lastname'] . "', shipping_address_1 = '" . $address_data['address_1'] . "', shipping_address_2 = '" . $address_data['address_2'] . "', shipping_city = '" .$address_data['city'] . "', shipping_postcode = '" . $address_data['postcode'] . "', shipping_country = 'INDIA', shipping_country_id = '99',shipping_zone_id = '1492', shipping_method = 'Flat shippinng Rate',  total = '0',language_id = '1', currency_id = '4', currency_code = 'INR', date_added = NOW(), date_modified = NOW(),location_id='1' , delivery_date = '".$date."' , delivery_time = '".$data['delivery_time']."', comment = '',shipping_custom_field= '',shipping_address_format = '' ,payment_custom_field ='',payment_address_format = '',custom_field ='',order_status_id = 2 ",$this->conn);
		$order_id = $this->getLastId($this->conn);

		$subtotal = 0;
		$base = 'https://taazitokari.com/image/';
		foreach($data['currentusercart']  as $ckey => $cvalue){
			$product_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id` = p.`product_id` WHERE pd.`product_id`= '".$ckey."' " ,$this->conn)->row;
			$subtotals = (int)$cvalue * $product_datas['price'];
			$subtotal += $subtotals;
			$this->query("INSERT INTO `oc_order_product` SET order_id = '" .$order_id. "', product_id = '" . $product_datas['product_id'] . "', name = '" . $product_datas['name'] . "', model = 'Taazi Tokari', quantity = '" .(int)$cvalue. "', price = '" . $product_datas['price'] . "', total = '" .$subtotals. "', tax = '1', reward = '1', location_id = '1' ",$this->conn);
		}
		$flat_cost=30;
		if($subtotal > 200){
			$flat_cost = 0;
		}
		$total = $subtotal + $flat_cost;
		if ($flat_cost > 0){
			$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'shippinng' ,title = 'Flat shipping rate' ,value='".$flat_cost."', sort_order = 3 ",$this->conn);
		}
		$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'sub_total' ,title = 'Sub-Total' ,value='".$subtotal."', sort_order = 1 ",$this->conn);
		$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'total' ,title = 'total' ,value='".$total."' ,sort_order = 9 ",$this->conn);
		
		$this->query("UPDATE `oc_order` SET total = '".$total."' WHERE `order_id` = '".$order_id."' ",$this->conn);
		$this->query("INSERT INTO `oc_order_history` SET order_id = '" .$order_id. "', order_status_id = 2, notify = 0, comment = '', date_added = NOW() ",$this->conn);
		$result['success'] = 1;
		return $result;
		

	}
	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>