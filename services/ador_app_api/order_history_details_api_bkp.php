<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		if(!isset($data['order_id'])){
			$data['order_id'] = '0';
		}
		$result = array();
		$order = $this->query("SELECT * FROM  `oc_order`  WHERE `order_id` = '".$data['order_id']."'  ",$this->conn)->row;
		
		$order_products = $this->query(" SELECT * FROM `oc_order_product` WHERE order_id = '".$order['order_id']."' ",$this->conn)->rows;

		$sub_total = $this->query("SELECT * FROM oc_order_total WHERE order_id = '".$order['order_id']."' AND code = 'sub_total'  ",$this->conn)->rows;
		$sub_totals = 0;
		foreach ($sub_total as $sub) {
			$sub_totals = $sub['value'];
		}

		$app_discount_total = $this->query("SELECT * FROM oc_order_total WHERE order_id = '".$order['order_id']."' AND code = 'appdiscount'  ",$this->conn)->rows;
		$app_discounts = 0;
		foreach ($app_discount_total as $sub) {
			$app_discounts = $sub['value'];
		}

		$flat = $this->query("SELECT * FROM oc_order_total WHERE order_id = '".$order['order_id']."' AND code = 'shipping'  ",$this->conn);
		if ($flat->num_rows > 0) {
			foreach ($flat->rows as $flatcost) {
				$flatcosts =  $flatcost['value'];
			}
		} else {
			$flatcosts = 0;
		}

		$ref_dis_datas = $this->query("SELECT * FROM oc_order_total WHERE order_id = '".$order['order_id']."' AND code = 'referral_discount'  ",$this->conn);
		if ($ref_dis_datas->num_rows > 0) {
			foreach ($ref_dis_datas->rows as $ref_dis) {
				$ref_discount =  $ref_dis['value'];
			}
		} else {
			$ref_discount = 0;
		}

		$ref_credit_datas = $this->query("SELECT * FROM oc_order_total WHERE order_id = '".$order['order_id']."' AND code = 'referral_credit_use'  ",$this->conn);
		if ($ref_credit_datas->num_rows > 0) {
			foreach ($ref_credit_datas->rows as $ref_credit) {
				$ref_credit = $ref_credit['value'];
			}
		} else {
			$ref_credit = 0;
		}

		$total = $this->query("SELECT * FROM oc_order_total WHERE order_id = '".$order['order_id']."' AND code = 'total'  ",$this->conn)->rows;
		$totals = '';
		foreach ($total as $tot) {
			$totals =  $tot['value'];
		}
		$total_qty = 0;
		$total_price = 0;

		foreach ($order_products as $pkey => $pvalue) {
			$total_qty = $total_qty + $pvalue['quantity'];
			$total_price = $total_price + $pvalue['price'];

			$result['product'][]= array(
				'order_product_id' => $pvalue['order_product_id'],
				'name' => $pvalue['name'],
				'quantity' => $pvalue['quantity'],
				'price' => number_format($pvalue['price'],2),
				'total' => number_format($pvalue['total'],2),
			);
		} 
		$result['status'] = $order['order_status_id'];
		$result['total_price'] = number_format($total_price,2);

		$result['flatcosts'] = number_format($flatcosts,2);
		$result['ref_discount'] = number_format($ref_discount,2);
		$result['ref_credit'] = number_format($ref_credit,2);
		$result['sub_totals'] = number_format($sub_totals,2);
		$result['app_discount'] = number_format($app_discounts,2);
		$result['total_qty'] = $total_qty;
		$result['grand_total'] = number_format($totals,2);
		
		return $result;
	}
	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>