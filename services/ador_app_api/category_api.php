<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		if(!isset($data['item_id'])){
			$data['item_id'] = '';

		}
		$result = array();
		$category = $this->query("SELECT * FROM `oc_category` c LEFT JOIN  `oc_category_description` cd  ON cd.`category_id`= c.`category_id` LEFT JOIN `oc_category_path` cp ON c.`category_id`= cp.`category_id` WHERE cp.`level`= 0 ORDER BY sort_order ASC",$this->conn);
		//$subcategory = $this->query("SELECT * FROM `oc_category` c LEFT JOIN  `oc_category_description` cd  ON cd.`category_id`= c.`category_id` LEFT JOIN `oc_category_path`cp ON cd.`category_id`= cp.`category_id` WHERE cp.`level`= 1",$this->conn);

		/*echo "<pre>";
		print_r($subcategory);
		exit;*/
		if ($category->num_rows > 0) {
			foreach($category->rows as $nkey => $nvalue){
				$result['category'][] = array(
					'category_id' => $nvalue['category_id'], 
					'name' => html_entity_decode($nvalue['name']),
				);
			}	
		}
		/*echo "<pre>";
		print_r($result);
		exit;*/
		return $result;
	}
	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>