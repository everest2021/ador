<?php 
date_default_timezone_set("Asia/Kolkata");
require_once('config.php');
$date = date('Y-m-d');
if(date('H') == '09'){
	$date_1 = date('Y_m_d');
    $database_name = DB_DATABASE.'_'.$date_1.'.sql';
	$database_name_zip = DB_DATABASE.'_'.$date_1.'.zip';
    echo 'mysqldump --user='.DB_USERNAME.' --password='.DB_PASSWORD.' --host='.DB_HOSTNAME.' '.DB_DATABASE.' > "'.DIR_BASE.'db_backup/'.$database_name.'" ';
    echo '<br />';
    echo 'zip -r '.DIR_BASE.'db_backup/'.$database_name_zip.' '.DIR_BASE.'db_backup/'.$database_name;
    echo '<br />';
    exit;
    exec('mysqldump --user='.DB_USERNAME.' --password='.DB_PASSWORD.' --host='.DB_HOSTNAME.' '.DB_DATABASE.' > "'.DIR_BASE.'db_backup/'.$database_name.'" ');
    exec('zip -r '.DIR_BASE.'db_backup/'.$database_name_zip.' '.DIR_BASE.'db_backup/'.$database_name);
} else {
	$date_1 = date('Y_m_d_H_i_s');
	$datebase_name = DB_DATABASE.'_'.$date_1.'.sql';
	exec('mysqldump --user='.DB_USERNAME.' --password='.DB_PASSWORD.' --host='.DB_HOSTNAME.' '.DB_DATABASE.' > "'.DIR_BASE.'db_backup/'.$datebase_name.'" ');
}
$srcPath = DIR_BASE.'db_backup/*';
$todaysdate = date("Y-m-d H:i:s");
foreach(glob($srcPath) as $key=>$object){
    $creation_date = date("Y-m-d H:i:s", filemtime($object));
    $datetime1 = new DateTime($todaysdate);
    $datetime2 = new DateTime($creation_date);
    $interval = $datetime1->diff($datetime2);
    $days = $interval->days;
    if($days > 10){
        delete_file($object);   
    }
}
echo 'Done';exit;
function delete_file($filename){
    exec('rm '.$filename.' ');
}
?>