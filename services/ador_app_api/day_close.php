<?php 
date_default_timezone_set("Asia/Kolkata");
require_once('config.php');

$dayClose = new dayClose();
$dayClose->main();

class dayClose {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function main(){
		//$current_date = '2021-01-17';
		$current_date = date('Y-m-d');
		$date = date('Y-m-d',strtotime(' -1 day', strtotime($current_date)));
		$yesterday_date = date('Y-m-d',strtotime(' -1 day', strtotime($date)));
		$delivery_date = date('Y-m-d',strtotime(' +1 day', strtotime($date)));
		$day_close_check = $this->query("SELECT `date` FROM `oc_day_close` WHERE `date` = '".$date."' ",$this->conn);
		if ( $day_close_check->num_rows == 0 ) {
			$product_ids = $this->query("SELECT `product_id` FROM `oc_product` WHERE status = 1 ",$this->conn)->rows;
			foreach ($product_ids as $product_id) {
				$query_day_close_quantity = $this->query("SELECT `product_id`, `quantity` FROM `oc_day_close` WHERE `date` = '".$yesterday_date."' AND `product_id` = '".$product_id['product_id']."' ",$this->conn)->row;
				$opening_quantity = 0;
				if ($query_day_close_quantity) {
					$opening_quantity = $query_day_close_quantity['quantity'];
				}
				$query_inward_quantity = $this->query("SELECT ip.product_id, SUM(ip.quantity) AS quantity FROM oc_inward i LEFT JOIN oc_inward_product ip ON (i.inward_id = ip.inward_id) WHERE DATE(i.date_added) = '".$date."' AND i.cancel_status = '1' AND ip.product_id = '".$product_id['product_id']."' AND i.is_return = '0' GROUP BY ip.product_id ",$this->conn)->row;

				$query_return_inward_quantity = $this->query("SELECT ip.product_id, SUM(ip.quantity) AS quantity FROM oc_inward i LEFT JOIN oc_inward_product ip ON (i.inward_id = ip.inward_id) WHERE DATE(i.date_added) = '".$date."' AND i.cancel_status = '1' AND ip.product_id = '".$product_id['product_id']."' AND i.is_return = '1' GROUP BY ip.product_id ",$this->conn)->row;

				//$query_sold_quantity = $this->query("SELECT op.product_id, SUM(op.quantity) AS quantity FROM oc_order o LEFT JOIN oc_order_product op ON (o.order_id= op.order_id) WHERE DATE(o.date_added) = '".$date."' AND (o.order_status_id = '2' OR o.order_status_id = '5') AND op.product_id = '".$product_id['product_id']."' GROUP BY op.product_id ",$this->conn)->row;
				$query_sold_quantity_from_outward = $this->query("SELECT quantity FROM oc_outward WHERE `date` = '".$date."' AND product_id = '".$product_id['product_id']."' ", $this->conn)->row;
				$query_sold_quantity = $this->query("SELECT op.product_id, SUM(op.quantity) AS quantity FROM oc_order o LEFT JOIN oc_order_product op ON (o.order_id= op.order_id) WHERE DATE(o.delivery_date) = '".$delivery_date."' AND (o.order_status_id = '2' OR o.order_status_id = '5') AND op.product_id = '".$product_id['product_id']."' GROUP BY op.product_id ",$this->conn)->row;
				$inward_quantity = 0;
				$sold_quantity = 0;
				$return_inward_quantity = 0;

				if ($query_return_inward_quantity) {
					$return_inward_quantity = $query_return_inward_quantity['quantity'];
				}

				if ($query_inward_quantity) {
					$inward_quantity = $query_inward_quantity['quantity'];
				}

				if ($query_sold_quantity_from_outward) {
					$sold_quantity = $query_sold_quantity_from_outward['quantity'];
				} else {
					if ($query_sold_quantity) {
						$sold_quantity = $query_sold_quantity['quantity'];
					}
				}

				$total_quantity = $opening_quantity + $inward_quantity;
				$closing_quantity = $total_quantity - $sold_quantity - $return_inward_quantity;
				$this->query("INSERT INTO `oc_day_close` SET `product_id` = '".$product_id['product_id']."', `date` = '".$date."', `quantity` = '".$closing_quantity."' ",$this->conn);
			}
			echo "<script>window.close();</script>";
		} else {
			echo "<script>alert('Day Already Closed.');window.close();</script>";
		}
	}
}
 ?>