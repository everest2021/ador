<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){ 
		date_default_timezone_set("Asia/Kolkata");
		//
		if(!isset($data['timess'])){
			$data['timess'] = '';
		}

		$result['delivery_timesss'] = array(
			'1'  => '08 am to 12 pm',
			'2'  => '12 pm to 04 pm',
			'3'  => '04 pm to 08 pm'
		);
		$delivery_date = date('Y-m-d', strtotime($data['timess']));
		$current_date = date('Y-m-d');
		$status = 1;
		if(strtotime($delivery_date) < strtotime($current_date)){
			$status = 2;
		}
		$next_day = date('Y-m-d', strtotime($current_date . ' +0 day'));
		if(strtotime($delivery_date) == strtotime($next_day) || $status == 2){
			$current_date_time = date('Y-m-d H:i:s');
			$compare_hour = date('H', strtotime($current_date_time . ' +0 Hours'));
			//echo "<pre>";print_r($compare_hour);exit;

			foreach($result['delivery_timesss'] as $dkey => $dvalue){
				$in = 0;
				if($dkey == '1'){
						if($compare_hour >= 0 && $compare_hour < 8){
							$in = 1;
						}
					
						} else if($dkey == '2'){
							if($compare_hour >= 0 && $compare_hour < 12){
								$in = 1;
							}
						} else if($dkey == '3'){
							if($compare_hour >= 0 && $compare_hour < 16){
								$in = 1;
							}
					
					}
				if($in == 0){
					unset($result['delivery_timesss'][$dkey]);
				}
			}
			//echo "<pre>";print_r($result['delivery_timesss']);exit;
		}
		if(count($result['delivery_timesss']) == 0) {
			$status = 2;
			$result['delivery_timesss'] = array(
				'1'  => '08 am to 12 pm',
				'2'  => '12 pm to 04 pm',
				'3'  => '04 pm to 08 pm'
			);
			$next_day = date('Y-m-d', strtotime($next_day . ' +1 day'));
		}

		foreach($result['delivery_timesss'] as $dkey => $dvalue){
			$result['delivery_times'][] = array(
				'key' => $dkey,
				'value' => $dvalue,
			);
		}
		$result['status'] = $status;
		$result['next_day'] = date('d-m-Y', strtotime($next_day));
		return $result;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>