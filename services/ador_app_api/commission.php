<?php 
date_default_timezone_set("Asia/Kolkata");
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');

$commission = new commission();
$commission->main();

class commission {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function main(){
		$order_info = $this->query("SELECT order_id FROM oc_order WHERE `order_status_id` = 5 ",$this->conn)->rows;
		foreach ($order_info as $value) {
			$tel_commission = 0;
			$cart_products = $this->query("SELECT product_id,name,price,quantity FROM oc_order_product WHERE order_id = '".$value['order_id']."' ", $this->conn)->rows;
			foreach ($cart_products as $cart) {
				$product_info = $this->query("SELECT commission_percent FROM oc_product WHERE product_id = '".$cart['product_id']."' ", $this->conn)->row;
				$product_commission = 0;
				if (isset($product_info['commission_percent'])) {
					$product_commission = $product_info['commission_percent'];
				}
				$tel_commission += (( $cart['price'] / 100 ) * $product_commission) * $cart['quantity'];
			}
			$this->query("UPDATE oc_order SET `tel_commission` = '".$tel_commission."' WHERE order_id = '".$value['order_id']."' ",$this->conn);
		}
		echo 'Done assigning telemarketer commission';exit;
	}
}
 ?>