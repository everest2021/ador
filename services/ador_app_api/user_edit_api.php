<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		
		if(!isset($data['user_id'])){
			$data['user_id'] = '';
		}
		if(!isset($data['firstname'])){
		 	$data['firstname'] = '';
		}
		if(!isset($data['lastname'])){
		 	$data['lastname'] = '';
		}
		if(!isset($data['email'])){
		 	$data['email'] = '';
		}
		if(!isset($data['phonenumber'])){
		 	$data['phonenumber'] = '';
		}
		/*if(!isset($data['check_ws'])){
		 	$data['check_ws'] = '';
		}*/
		if(!isset($data['address_id'])){
			$data['address_id'] = '';//'225';
		}
		if(!isset($data['address_1'])){
			$data['address_1'] = '';//'Test Address';
		}
		if(!isset($data['address_2'])){
			$data['address_2'] = '';//'Test Address 1';
		}
		if(!isset($data['city'])){
			$data['city'] = '';//'Gass';
		}
		if(!isset($data['postcode'])){
			$data['postcode'] = '';//'401203';
		}
		if(!isset($data['area'])){
			$data['area'] = '';//'Gass';
		}

		$result = array();

		$this->query("UPDATE `oc_customer_app` SET `local_customer_id` = '".$data['user_id']."', `firstname` = '".$this->escape($data['firstname'], $this->conn)."', `lastname`='".$this->escape($data['lastname'], $this->conn)."', `email`= '".$this->escape($data['email'], $this->conn)."', `telephone`= '".$this->escape($data['phonenumber'], $this->conn)."', `information_field` = '1' WHERE `customer_id` = '".$data['user_id']."' ",$this->conn);
		$result['success'] = 1;

		if (isset($data['address_present'])) {
			if ($data['address_id'] == 0){
				$address_data =  $this->query("INSERT INTO `oc_address_app` SET `customer_id` = '".$data['user_id']."', `local_customer_id` = '".$data['user_id']."', `address_1` = '".$this->escape($data['address_1'], $this->conn)."', `address_2` = '".$this->escape($data['address_2'], $this->conn)."', `city`= '".$this->escape($data['city'], $this->conn)."', `postcode`= '".$this->escape($data['postcode'], $this->conn)."', `area`= '".$this->escape($data['area'], $this->conn)."', `firstname` = '".$this->escape($data['firstname'], $this->conn)."', `lastname` = '".$this->escape($data['lastname'], $this->conn)."', `country_id` = 99 , `zone_id` = 1493 ", $this->conn);

				$result['success'] 		= 1;
				$result['firstname'] 	= $data['firstname'];
				$result['lastname'] 	= $data['lastname'];
				$result['phonenumber']	= $data['phonenumber'];
				$result['email'] 		= $data['email'];
				$result['city'] 		= $data['city'];
				$result['postcode'] 	= $data['postcode'];
				$result['area'] 		= $data['area'];
			} else {
				$address_data = $this->query("UPDATE `oc_address_app` SET `local_customer_id` = '".$data['user_id']."', `address_1` = '".$this->escape($data['address_1'], $this->conn)."', `address_2` = '".$this->escape($data['address_2'], $this->conn)."', `city`= '".$this->escape($data['city'], $this->conn)."', `postcode` = '".$this->escape($data['postcode'], $this->conn)."', `area`= '".$this->escape($data['area'], $this->conn)."', `firstname` = '".$this->escape($data['firstname'], $this->conn)."', `lastname` = '".$this->escape($data['lastname'], $this->conn)."' WHERE address_id ='".$data['address_id']."' ",$this->conn);

				$result['success'] 		= 1;
				$result['firstname'] 	= $data['firstname'];
				$result['lastname'] 	= $data['lastname'];
				$result['phonenumber']	= $data['phonenumber'];
				$result['email'] 		= $data['email'];
				$result['city'] 		= $data['city'];
				$result['postcode'] 	= $data['postcode'];
				$result['area'] 		= $data['area'];
			}
		}
		return $result;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>