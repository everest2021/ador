<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		if(!isset($data['user_id'])){
			$data['user_id'] = '';//'212';
		}
		$data['order_id'] = '18565';

		$get_payment_method = $this->query("SELECT payment_method FROM oc_order WHERE `order_id` = '".$data['order_id']."' ",$this->conn)->row['payment_method'];

		$newArray = explode(',', $get_payment_method);
		
		$payment_methods = array();
		foreach ($newArray as $key => $str) {
			$value = str_replace(' ', '', $str);
		// 	echo '<pre>';
		// print_r($value);
		// exit;
			if ($value == 'COD') {
				$payment_methods['COD'] = 1;
			}
			if ($value == 'Gpay/Paytm/BHIM/etc') {
				$payment_methods['BHIM'] = 1;
			}
			if ($value == 'Sodexo') {
				$payment_methods['SODEXO'] = 1;
			}
			if ($value == 'OnlinePayment') {
				$payment_methods['OP'] = 1;
			}
		}
		$result['payment_methods'] = $payment_methods;

		$result['total_orders'] = $this->query("SELECT count(*) as total_order FROM oc_order WHERE (order_status_id = 5 OR order_status_id = 2) AND `customer_id` = '".$data['user_id']."' ",$this->conn)->row['total_order'];

		return $result;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>