<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){ 
		if(!isset($data['referral_code'])){
			$data['referral_code'] = '';//'9730941488';
		}
		if($data['referral_code'] != ''){
			// $referral_data = $this->query("SELECT * FROM oc_affiliate WHERE code = '" .$data['referral_code']."' ",$this->conn);
			// $result = array();
			// if ($referral_data->num_rows > 0) {
			// 	$result['referral_discount'] = $referral_data->row['referral_discount'];	
			// }
			$referral_creddits = $this->query("SELECT `telephone` FROM oc_customer WHERE telephone = '".$data['referral_code']."' ",$this->conn);
			if ($referral_creddits->num_rows > 0){
				//$result['referral_code'] = $referral_creddits->row['telephone'];
				//$result['referral_discount_self'] = DISCOUNT_MOBILE_SELF;
				$result['referral_code'] = '';
				$result['referral_discount_self'] = 0;
			} else {
				$result['referral_code'] = '';
				$result['referral_discount_self'] = 0;
			}			 
		} 
		if(isset($result['referral_discount_self']) && $result['referral_discount_self'] > 0){
			$result['status'] = 1;
		} else {
			$result['status'] = 1;
		}
		return $result;
	}
	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>