<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		if(!isset($data['itemname_search'])){
			$data['itemname_search'] = '';
		}
		$sql = "SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id`= p.`product_id` WHERE 1=1 ";
		if($data['itemname_search'] != ''){
			$sql .= " AND pd.`name` LIKE '%".$data['itemname_search']."%' ";
		}		
		$item_datas = $this->query($sql, $this->conn);
		$base = 'https://taazitokari.com/image/';
		$result = array();
		if ($item_datas->num_rows > 0) {
			foreach($item_datas->rows as $nkey => $nvalue){
				$result['item_datas'][] = array(
					'product_id' => $nvalue['product_id'], 
					'name' => html_entity_decode($nvalue['name']),
					'quantity' =>(int) 0,
					'price' => (int)$nvalue['price'],
					'image' =>$base.$nvalue['image'],
				);
			}	
		}
		if(isset($result['item_datas'])){
			$result['success'] = 1;
		} else {
			$result['success'] = 0;
		}
		// echo "<pre>";
		// print_r($result);
		// exit;
		return $result;
	}
	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>