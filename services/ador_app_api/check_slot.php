<?php
date_default_timezone_set("Asia/Kolkata");
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
		$this->conn->set_charset("utf8");
		$this->conn->query("SET SQL_MODE = ''");
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data){
		$data['success'] = 0;
		$time_slots = $this->query("SELECT * FROM `oc_time_slot` WHERE 1=1 ",$this->conn);
		if ($time_slots->num_rows > 0) {
			$m_order_from_time = date('H:i', strtotime($time_slots->row['m_from_time']));
			$m_order_to_time = date('H:i', strtotime($time_slots->row['m_to_time']));
			$a_order_from_time = date('H:i', strtotime($time_slots->row['a_from_time']));
			$a_order_to_time = date('H:i', strtotime($time_slots->row['a_to_time']));
			$current_time = date('H:i');
			// echo $m_order_from_time;
			// echo'<br>';
			// echo $m_order_to_time;
			// echo'<br>';
			// echo $a_order_from_time;
			// echo'<br>';
			// echo $a_order_to_time;


			if ($current_time >= $m_order_from_time && $current_time <= $m_order_to_time) {
				// echo "inn";
				$data['success'] = 1;
			}
			if ($current_time >= $a_order_from_time && $current_time <= $a_order_to_time) {
				// echo "inn2";
				$data['success'] = 1;
			}
			if ($current_time > $m_order_to_time) {
				$data['time'] = array(
					'from_time' => date('h:i', strtotime($time_slots->row['m_from_time'])),
					'am_pm' => 'PM',
				);
			}
			if ($current_time > $a_order_to_time && $current_time < $m_order_to_time) {
				$data['time'] = array(
					'from_time' => date('h:i', strtotime($time_slots->row['a_from_time'])),
					'am_pm' => 'AM',
				);
			}
		}

		return $data;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>