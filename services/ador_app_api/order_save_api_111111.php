<?php 
date_default_timezone_set("Asia/Kolkata");
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
//echo "inn";exit;
require_once('config.php');
// $file = BASE_LINK;
//$file = 'C:\xampp\htdocs\taazitokari_theme\service\service.txt';
error_reporting(E_ALL);
ini_set("display_errors", 1);

// $handle = fopen($file, 'a+'); 
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
// fclose($handle);

exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitemsss($data = array()){
		// echo'<pre>';
		// print_r($_GET);
		// exit;
	}

	public function getitem($data = array()){
		// echo'<pre>';
		// print_r($data);
		// exit;

		$result['success'] = 0;
		$result['order_id'] = 0;
		if ($data['firstname'] != '' && $data['firstname'] != 'null' && $data['firstname'] != null && $data['lastname'] != '' && $data['lastname'] != 'null' && $data['lastname'] != null && $data['phoneno'] != '' && $data['phoneno'] != 'null' && $data['phoneno'] != null && $data['postcode'] != '' && $data['postcode'] != 'null' && $data['postcode'] != null && $data['area'] != '' && $data['area'] != 'null' && $data['area'] != null && $data['address_1'] != '' && $data['address_1'] != 'null' && $data['address_1'] != null) {
			if(isset($data['currentusercart'])){
				$this->query("INSERT INTO `oc_orders_app` SET cust_id = '".$data['user_id']."', tot_qty = '".$data['total_items']."', total = '".$data['total_price']."', grand_tot = '0', order_time = '".date('H:i:s')."', order_date = '".date('Y-m-d')."', address_1 = '".$this->escape($data['address_1'], $this->conn)."', address_2 = '".$this->escape($data['address_2'], $this->conn)."', city = '".$this->escape($data['city'], $this->conn)."', postcode = '".$data['postcode']."', area = '".$this->escape($data['area'],$this->conn)."' ", $this->conn);
				$order_id = $this->getLastId($this->conn);
				foreach ($data['currentusercart'] as $cckey => $ccvalue) {
					$items = $this->query("SELECT item_name, rate_1 FROM oc_item WHERE item_id = '" . (int)$cckey . "'", $this->conn);
					if ($items->num_rows > 0) {
						$item = $items->row['item_name'];
						$price = $items->row['rate_1'];
					} else {
						$item = '';
						$price = 0;
					}
					$total = $price*$ccvalue;
					$this->query("INSERT INTO `oc_order_item_app` SET order_id = '".$order_id."', item_id = '".$cckey."', item = '".$this->escape($item, $this->conn)."', qty = '".$ccvalue."', price = '".$price."', total = '".$total."' ", $this->conn);
					$result['success'] = 1;
					$result['order_id'] = $order_id;
				}
			}
			if(isset($data['special_notes_array'])){
				foreach ($data['special_notes_array'] as $skey => $svalue) {
					$this->query("UPDATE `oc_order_item_app` SET special_notes = '".$svalue."' WHERE order_id = '".$order_id."' AND item_id = '".$skey."' ", $this->conn);
				}
			}
		} else {
			$result['success'] = 2;
		}
		return $result;
	}
	public function getitems($data = array()){
		$qty_empty = 0;
		$products = '';
		if(isset($data['currentusercart'])){
			foreach ($data['currentusercart'] as $cckey => $ccvalue) {
				$stock = $this->stock($cckey);
				$avail_qtys = $stock['available_quantity'];
				if ($avail_qtys >= 0) {
					$avail_qty = $avail_qtys;
					if ((int)$ccvalue > (int)$avail_qty) {
						unset($data['currentusercart'][$cckey]);
						$qty_empty = 1;
						$product_names = $this->query("SELECT `name` FROM `oc_product_description` WHERE `product_id` = '".$cckey."' ",$this->conn);
						if ($product_names->num_rows > 0) {
							$product_name = $product_names->row['name'];
						} else {
							$product_name = '';
						}
						$products .= rtrim($product_name.',', ',');
					}
				}
			}
		}
		
		// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('-----Start------', true)  . "\n");
		if(!isset($data['user_id'])){
			$data['user_id'] = '';
		}
		if(!isset($data['address_id'])){
			$data['address_id'] = '';
		}
		if(!isset($data['currentusercart'])){
			$data['currentusercart'] = array();//array('1370' => '2');
		}
		/*if(!isset($data['delivery_date'])){
			$data['delivery_date'] = '';
		} */
		if(!isset($data['delivery_time'])){
			$data['delivery_time'] = "";//'2020-10-14';
		}
		if(!isset($data['ref_discount_per'])){
			$data['ref_discount_per'] = "";//'0';
		}
		if(!isset($data['discount_amt'])){
			$data['discount_amt'] = "";//'0';
		}
		$ref_code = '';
		if(!isset($data['referral_code'])){
			$data['referral_code'] = '';
		} else {
			$ref_code = $data['referral_code'];
		}
		if(!isset($data['special_notes'])){
			$data['special_notes'] = '';
		}
		if(!isset($data['app_discount'])){
			$data['app_discount'] = 0;
		} 
		if(!isset($data['payment_method'])){
			$payment_method = 'Cash on delivery';
		}

		if (isset($data['payment_method'])) {
			$str = '';
			foreach ($data['payment_method'] as $key => $value) {
				$str .= $value['name'].', ';
			}
			$payment_method = rtrim($str,", ");
		}


		$station_id = 0;
		if($data['user_id'] > 0){
			$address_data = $this->query("SELECT `postcode` FROM `oc_address` WHERE `customer_id` = '".$data['user_id']."' ",$this->conn);
			if($address_data->num_rows > 0){
				$pincode = $address_data->row['postcode'];
				$station_datas = $this->query("SELECT `station` FROM `oc_pincode` WHERE `pincode` = '".$pincode."' ",$this->conn);
				if($station_datas->num_rows > 0){
					$station_id = $station_datas->row['station'];
				}
			}
		}

		// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($data, true)  . "\n");
		
		$result = array();
		
		// $delivery_dates = $this->query("SELECT `date` FROM `oc_day_close_status` WHERE 1 = 1",$this->conn);
		// if($delivery_dates->num_rows > 0){
		// 	$data['delivery_date'] = $delivery_dates->row['date'];
		// } else {
		// 	$data['delivery_date'] = date('Y-m-d');
		// }


		$date = date('Y-m-d', strtotime($data['delivery_date']));
		
		if($data['currentusercart']){
			if($data['referral_code'] != ''){
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('in referral_code', true)  . "\n");
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($data['referral_code'], true)  . "\n");
				$this->query("UPDATE `oc_customer` SET referral_code = '".$data['referral_code']."' WHERE customer_id = '".$data['user_id']."'", $this->conn);
			}
			if ($data['referral_code'] == '') {
				$r_code = $this->query("SELECT referral_code FROM `oc_customer` WHERE customer_id = '".$data['user_id']."'",$this->conn)->row;
				$ref_code = $r_code['referral_code'];
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r("SELECT referral_code FROM `oc_customer` WHERE customer_id = '".$data['user_id']."'", true)  . "\n");
			}
			$address_datas = $this->query("SELECT * FROM `oc_address`  LEFT JOIN `oc_customer`  ON (oc_address.customer_id = oc_customer.customer_id) WHERE oc_address.`address_id` = '".$data['address_id']."' ",$this->conn);
			if($address_datas->num_rows > 0){
				$address_data = $address_datas->row;
				$area_names = $this->query("SELECT `name` FROM `oc_station` WHERE station_id = '".$address_data['area']."' ",$this->conn); 
				$area_name = '';
				if($area_names->num_rows > 0){
					$area_name = $area_names->row['name'];	
				}
				$store_url = 'https://taazitokari.com/service/';
				$this->query("INSERT INTO `oc_order` SET 
						ismobile = '1' ,
						invoice_no = '0' , 
						invoice_prefix = '',
						store_name = 'Taazi Tokari', 
						store_url = '".$store_url."', 
						store_id = '0', 
						customer_id = '" . $address_data['customer_id'] . "', 
						firstname = '" . $this->escape($address_data['firstname'], $this->conn). "', 
						lastname = '" .$this->escape($address_data['lastname'], $this->conn) . "', 
						email = '" .$address_data['email'] . "',
						telephone = '" . $address_data['telephone']. "',
						fax = '',
						custom_field = '',
						payment_address_format = '',
						payment_custom_field = '',
						payment_firstname = '" . $this->escape($address_data['firstname'], $this->conn) . "',
						payment_lastname = '" .$this->escape($address_data['lastname'], $this->conn) . "', 
						payment_address_1 = '" . $this->escape($address_data['address_1'], $this->conn) . "',
						payment_address_2 = '" . $this->escape($address_data['address_2'], $this->conn) . "',
						payment_city = '" . $this->escape($address_data['city'], $this->conn) . "', 
						payment_postcode = '" . $address_data['postcode'] . "', 
						payment_area = '" . $area_name . "', 
						payment_country = 'INDIA', 
						payment_country_id = '99',
						payment_zone_id = '1492',
						shipping_address_format = '',
						shipping_custom_field = '',
						referral_code = '".$this->escape($ref_code, $this->conn)."',
						referral_dis_per = '".$this->escape($data['ref_discount_per'], $this->conn)."',
						comment = '".$this->escape($data['special_notes'], $this->conn)."',
						payment_method = '".$this->escape($payment_method, $this->conn)."', 
						shipping_firstname = '" . $this->escape($address_data['firstname'], $this->conn) . "', 
						shipping_lastname = '" . $this->escape($address_data['lastname'], $this->conn) . "', 
						shipping_address_1 = '" . $this->escape($address_data['address_1'], $this->conn) . "', 
						shipping_address_2 = '" . $this->escape($address_data['address_2'], $this->conn) . "', 
						shipping_city = '" .$this->escape($address_data['city'], $this->conn) . "', 
						shipping_postcode = '" . $address_data['postcode'] . "', 
						shipping_country = 'INDIA', shipping_country_id = '99',
						shipping_zone_id = '1492', 
						shipping_method = 'Flat shippinng Rate',  
						total = '0.00',
						language_id = '1', 
						currency_id = '4', 
						order_status_id = 2,
						currency_code = 'INR',
						date_added = NOW(), 
						date_modified = NOW(),
						location_id='1' , 
						delivery_date = '".$date."' , 
						delivery_time = '".$data['delivery_time']."',
						telemarketing_id = '".$address_data['telemarketing_id']."' ",$this->conn);

				
				$order_id = $this->getLastId($this->conn);
				$result['order_id'] = $order_id;

				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Id : ' . $order_id, true)  . "\n");
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($data['currentusercart'], true)  . "\n");

				$subtotal = 0;
				$base = 'https://taazitokari.com/image/';
				//echo "<pre>"; print_r($data);exit;
				$wholesaler_check = $this->query("SELECT is_wholesaler FROM oc_customer WHERE customer_id = '".$data['user_id']."' " ,$this->conn)->row;
				foreach($data['currentusercart']  as $ckey => $cvalue){
					$product_datas = $this->query("SELECT * FROM `oc_product` p LEFT JOIN  `oc_product_description` pd ON pd.`product_id` = p.`product_id` WHERE pd.`product_id`= '".$ckey."' " ,$this->conn)->row;
					
					$stock = $this->stock($ckey);
					if ($cvalue > $stock['available_quantity']) {
						$cvalue = $stock['available_quantity'];
					}
					
					// if ($cvalue > $product_datas['quantity']) {
					// 	$cvalue = $product_datas['quantity'];
					// }

					if($wholesaler_check['is_wholesaler'] == '1') {
						$price = $product_datas['wholeseller_price'];
					} else {
						$special_datass = "SELECT product_special_id, price FROM oc_product_special  WHERE product_id = '".$ckey."' AND `customer_group_id` = 1 AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) ORDER BY `priority` ASC LIMIT 1" ;
						$special_data = $this->query($special_datass, $this->conn);
						if($special_data->num_rows > 0){
							$price = $special_data->row['price'];
							$product_special_id = $special_data->row['product_special_id'];
							if($station_id > 0){
								$special_datass = "SELECT price FROM oc_product_special_station_price  WHERE product_id = '".$ckey."' AND `product_special_id` = '".$product_special_id."' AND `station_id` = '".$station_id."' " ;
								$special_data = $this->query($special_datass, $this->conn);
								if($special_data->num_rows > 0){
									$price = $special_data->row['price'];
								}
							}
							$isSpecialPrice = 1;
						} else{
							$isSpecialPrice = 0;
							if (isset($wholesaler_check['is_wholesaler']) && $wholesaler_check['is_wholesaler'] == '1') {
								$price = $product_datas['wholeseller_price'];
							} else {
								$price = $product_datas['price'];
								if($station_id > 0){
									$special_datass = "SELECT price FROM oc_product_station_price  WHERE product_id = '".$ckey."' AND `station_id` = '".$station_id."' " ;
									$special_data = $this->query($special_datass, $this->conn);
									if($special_data->num_rows > 0){
										$price = $special_data->row['price'];
									}
								}
							}
						}
					}
					$subtotals = (int)$cvalue * $price;
					$subtotal += $subtotals;
					$this->query("INSERT INTO `oc_order_product` SET order_id = '" .$order_id. "', product_id = '" . $product_datas['product_id'] . "', name = '" . $this->escape($product_datas['name'], $this->conn) . "', model = 'Taazi Tokari', quantity = '" .(int)$cvalue. "', price = '" . $price . "', total = '" .$subtotals. "', tax = '0', reward = '0', location_id = '1' ",$this->conn);
					// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_product` SET order_id = '" .$order_id. "', product_id = '" . $product_datas['product_id'] . "', name = '" . $this->escape($product_datas['name'], $this->conn) . "', model = 'Taazi Tokari', quantity = '" .(int)$cvalue. "', price = '" . $price . "', total = '" .$subtotals. "', tax = '0', reward = '0', location_id = '1' ", true)  . "\n");
					// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('order Product Response', true)  . "\n");
					// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
				}

				$discount_amt = 0;
				if($data['discount_amt'] != '' && $data['discount_amt'] != '0'){
					$discount_amt = '-'.$data['discount_amt'];
					$title = 'Referral Discount('.$data['ref_discount_per'].'%)';
					$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'referral_discount', title = '".$title."', value='".$discount_amt."', sort_order = 2",$this->conn);
					// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'referral_discount', title = '".$title."', value='".$discount_amt."', sort_order = 2", true)  . "\n");
					// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('referral_discount_used', true)  . "\n");
					// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
					$discount_amt = $data['discount_amt'];
				}


				$app_discount = 0;
				if ($data['app_discount'] > 0){
					$app_discounts = '-'.$data['app_discount'];
					$title = 'App Discount('.DISCOUNT_APP.'%)';
					$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'appdiscount', title = '".$title."', value = '".$app_discounts."', sort_order = 4 ",$this->conn);
					// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'appdiscount', title = '".$title."', value='".$app_discounts."', sort_order = 4 ", true)  . "\n");
					// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total App Discount Response', true)  . "\n");
					// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
					$app_discount = $data['app_discount'];
				}

				$mainurl = MESSAGE.$address_data['telephone'].'&msg=';
				$messagecustomer = "Thank you for ordering with us. Your order is in Process.%0aWith Regards%0aTEAM TAAZI TOKARI";
				$messagecustomer = str_replace(' ', '%20', $messagecustomer);
				$link = $mainurl.$messagecustomer;
				//echo "<pre>";print_r($link);exit;
				file($link);

				$total_order_count = $this->query("SELECT count(*) as total_order FROM oc_order WHERE (order_status_id = 5 OR order_status_id = 2) AND telephone = '".$address_data['telephone']."' AND order_id <= '".$order_id."' ",$this->conn);
				//return $total_order_count->row['total_order'];
				$flat_cost = 30;
				if($total_order_count->num_rows > 0){
					$flat_cost = ($total_order_count->row['total_order'] > 5 || $subtotal >= 500) ? 0 :  30 ;
				}
				
				
				// $flat_cost = 30;
				// if($subtotal >= 200){
				// 	$flat_cost = 30;
				// }

				$used_credit = 0;
				$total_amount = ($subtotal) - ($discount_amt + $app_discount);
				if($address_data['referral_credits'] > 0){
					if($address_data['referral_credits'] > $total_amount){
						$after_used_credit = $address_data['referral_credits'] - $total_amount;
						$used_credit = $total_amount;
						$amtsubtotal = 0;
						// $flat_cost = 30;
						// if($subtotal >= 200){
						// 	$flat_cost = 30;
						// }
						$flat_cost = ($total_order_count->row['total_order'] > 5 || $subtotal >= 500) ? 0 :  30;

						if($flat_cost > 0){
							if($after_used_credit > $flat_cost){
								$used_credit += $flat_cost;
								$flat_costs = 0;
								$after_used_credit = $after_used_credit - $flat_cost;
							} else {
								$flat_costs = $flat_cost - $after_used_credit;
								$used_credit += $after_used_credit;
								$after_used_credit = 0;
							}
						} else {
							$flat_costs = $flat_cost;
						}
						$total = $flat_costs;

					} else {
						$after_used_credit = 0;
						$used_credit = $address_data['referral_credits'];
						$amtsubtotal = $total_amount - $address_data['referral_credits'];
						// $flat_cost = 30;
						// if($amtsubtotal >= 200){
						// 	$flat_cost = 30;
						// }
						$flat_cost = ($total_order_count->row['total_order'] > 5 || $subtotal >= 500) ? 0 :  30;

						$total = $amtsubtotal + $flat_cost;
						//echo "<pre>";print_r($total);exit;
					}


					

					$referral_credits = '-'.$used_credit;
					$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'referral_credits_used', title = 'Referral Credits Used', value='".$referral_credits."', sort_order = 7",$this->conn);
					// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'referral_credits_used', title = 'Referral Credits Used', value = '".$referral_credits."', sort_order = 7", true)  . "\n");
					// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total Referral Credit Use', true)  . "\n");
					// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
					$this->query("UPDATE `oc_customer` SET referral_credits = '".$after_used_credit."' WHERE customer_id = '".$data['user_id']."'", $this->conn);
				} else {
					$total = $total_amount + $flat_cost;
				}
				
				if($data['referral_code'] != ''){
					//$referral_creditss = ($total * DISCOUNT_MOBILE) / 100; 
					$referral_creditss = 50; 
					$this->query("UPDATE `oc_customer` SET referral_credits = (referral_credits + " . (float)$referral_creditss . ") WHERE `telephone` = '".$data['referral_code']."' ",$this->conn);
					// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("UPDATE `oc_customer` SET referral_credits = (referral_credits + " . (float)$referral_creditss . ") WHERE `telephone` = '".$data['referral_code']."' ", true)  . "\n");
					// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('referral_discount_credits_gave', true)  . "\n");
					// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
				}

				$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'shipping', title = 'Flat shipping rate', value = '".$flat_cost."', sort_order = 6 ",$this->conn);
				// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'shipping', title = 'Flat shipping rate', value='0', sort_order = 3 ", true)  . "\n");
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total Shipping Response', true)  . "\n");
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
				
				
				$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'sub_total' ,title = 'Sub-Total' ,value='".$subtotal."', sort_order = 1 ",$this->conn);
				// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'sub_total' ,title = 'Sub-Total' ,value='".$subtotal."', sort_order = 1 ", true)  . "\n");
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total Sub Total Response', true)  . "\n");
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");

				$this->query("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'total' ,title = 'Total' ,value='".$total."' ,sort_order = 9 ",$this->conn);
				// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_total` SET order_id = '" .$order_id. "', code = 'total' ,title = 'Total' ,value='".$total."' ,sort_order = 9 ", true)  . "\n");
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Total Total Response', true)  . "\n");
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");

				$this->query("UPDATE `oc_order` SET total = '".$total."' WHERE `order_id` = '".$order_id."' ",$this->conn);
				// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("UPDATE `oc_order` SET total = '".$total."' WHERE `order_id` = '".$order_id."' ", true)  . "\n");
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order Update Total Response', true)  . "\n");
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");

				$this->query("INSERT INTO `oc_order_history` SET order_id = '" .$order_id. "', order_status_id = 2, notify = 1, comment= '".$this->escape($data['special_notes'], $this->conn)."', date_added = NOW() ",$this->conn);
				// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("INSERT INTO `oc_order_history` SET order_id = '" .$order_id. "', order_status_id = 2, notify = 1, comment= '".$this->escape($data['special_notes'], $this->conn)."', date_added = NOW() ", true)  . "\n");
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('Order History Insert Response', true)  . "\n");
				// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");

				$order_product_query = $this->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'", $this->conn);
				foreach ($order_product_query->rows as $order_product) {
					$this->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'", $this->conn);
					$order_option_query = $this->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product['order_product_id'] . "'", $this->conn);
					foreach ($order_option_query->rows as $option) {
						$this->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'", $this->conn);
					}
				}
				//UPDATE `oc_product` SET `quantity` = '5000' WHERE `quantity` < '0';

				$result['success'] = 1;
			} else {
				if ($qty_empty == 1) {
					$result['success'] = 3;
					$result['products'] = $products;
				} else {
					$result['success'] = 0;
				}
			}
		} else {
			$result['success'] = 0;
		}
		// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($result, true)  . "\n");
		// fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('-----End------', true)  . "\n");
		return $result;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}

	public function getDayCloseProduct($product_id) {
		$query = $this->query("SELECT * from `oc_day_close` having  `date` = (SELECT max(`date`) from `oc_day_close` WHERE 1=1 ) AND product_id = '".$product_id."' ",$this->conn)->row;

		return $query;
	}

	public function stock($product_id) {
		$result = $this->getDayCloseProduct($product_id);

		$product = array(
			'category_name' 		=> 0,
			'product_name' 			=> '',
			'opening_quantity'     	=> 0,
			'sold_quantity'			=> 0,
			'sold_quantity_by_order'=> 0,
			'purchased_quantity'	=> 0,
			'purchased_return_quantity'	=> 0,
			'available_quantity'	=> 0,
		);

		if ($result) {
			$next_date = date('Y-m-d', strtotime("+1 day", strtotime($result['date'])));
			$delivery_date = date('Y-m-d', strtotime("+2 day", strtotime($result['date'])));
			$query_category =  $this->query("SELECT category_id FROM `oc_product_to_category` WHERE product_id= '".$result['product_id']."' ",$this->conn);
			$categoryId = 0;
			if ($query_category->num_rows > 0) {
				$categoryId = $query_category->row['category_id'];
			}
			$query_category_name =  $this->query("SELECT name FROM `oc_category_description` WHERE category_id= '".$categoryId."' ",$this->conn);
			$category_name = '';
			if($query_category_name->num_rows > 0) {
				$category_name = $query_category_name->row['name'];
			}
			$product_name_sql = "SELECT name FROM `oc_product_description` WHERE product_id= '".$result['product_id']."' ";
			$query_sold_quantity_from_outward = $this->query("SELECT quantity FROM oc_outward WHERE `date` = '".$next_date."' AND product_id = '".$result['product_id']."' ",$this->conn)->row;
			$query_sold_quantity = $this->query("SELECT op.product_id, SUM(op.quantity) AS sold_quantity FROM oc_order o LEFT JOIN oc_order_product op ON (o.order_id= op.order_id) WHERE DATE(o.delivery_date) = '".$delivery_date."' AND (o.order_status_id = '2' OR o.order_status_id = '5') AND op.product_id = '".$result['product_id']."' GROUP BY op.product_id ",$this->conn)->row;
			$query_inward_quantity = $this->query("SELECT ip.product_id, SUM(ip.quantity) AS purchased_quantity FROM oc_inward i LEFT JOIN oc_inward_product ip ON (i.inward_id = ip.inward_id) WHERE DATE(i.date_added) = '".$next_date."' AND i.cancel_status = '1' AND ip.product_id = '".$result['product_id']."' AND i.is_return = '0' GROUP BY ip.product_id ",$this->conn)->row;
			$query_return_inward_quantity = $this->query("SELECT ip.product_id, SUM(ip.quantity) AS purchased_return_quantity FROM oc_inward i LEFT JOIN oc_inward_product ip ON (i.inward_id = ip.inward_id) WHERE DATE(i.date_added) = '".$next_date."' AND i.cancel_status = '1' AND ip.product_id = '".$result['product_id']."' AND i.is_return = '1'  GROUP BY ip.product_id ",$this->conn)->row;


			$sold_quantity = 0;
			$sold_quantity_by_order = 0;
			$return_inward_quantity = 0;

			if ($query_return_inward_quantity) {
				$return_inward_quantity = $query_return_inward_quantity['purchased_return_quantity'];
			}

			if ($query_sold_quantity) {
				$sold_quantity_by_order = $query_sold_quantity['sold_quantity'];
			}

			if ($query_sold_quantity_from_outward) {
				$sold_quantity = $query_sold_quantity_from_outward['quantity'];
			} else {
				if ($query_sold_quantity) {
					$sold_quantity = $query_sold_quantity['sold_quantity'];
				}
			}

			$inward_quantity = 0;
			if ($query_inward_quantity) {
				$inward_quantity = $query_inward_quantity['purchased_quantity'];
			}

			$product_name = $this->query($product_name_sql,$this->conn);
			if($product_name->num_rows > 0){
				$final_product_name = $product_name->row['name'];			
			} else {
				$final_product_name = '';
			}

			$available_quantity = ($result['quantity'] + $inward_quantity) - $sold_quantity - $return_inward_quantity;

			$product = array(
				'category_name' 		=> $category_name ,
				'product_name' 			=> $final_product_name ,
				'opening_quantity'     	=> $result['quantity'],
				'sold_quantity'			=> $sold_quantity,
				'sold_quantity_by_order'=> $sold_quantity_by_order,
				'purchased_quantity'	=> $inward_quantity,
				'purchased_return_quantity'	=> $return_inward_quantity,
				'available_quantity'	=> $available_quantity,
			);

		}

		return $product;
	}
}
?>