<?php 
date_default_timezone_set("Asia/Kolkata");
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
//echo "inn";exit;
require_once('config.php');
// $file = BASE_LINK;
//$file = 'C:\xampp\htdocs\taazitokari_theme\service\service.txt';
error_reporting(E_ALL);
ini_set("display_errors", 1);

// $handle = fopen($file, 'a+'); 
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($_GET['order_id']);

exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($order_id){
		// echo $order_id;
		// exit;

		$results = $this->query("SELECT * FROM oc_orders_app WHERE order_id = '".$order_id."' ", $this->conn)->rows;

		$result1 = array();
		foreach ($results as $result) {

			$prev_details = $this->query("SELECT kot_no, table_id FROM oc_order_info ORDER BY order_id DESC LIMIT 1 ", $this->conn);
			if ($prev_details->num_rows > 0) {
				$kot_no = (int)$prev_details->row['kot_no'] + 1;
				$table_id = (int)$prev_details->row['table_id'] + 1;
			} else {
				$kot_no = 1;
				$table_id = 5000;
			}


			$cust_details = $this->query("SELECT customer_id, CONCAT(firstname, ' ', lastname) AS cust_name, CONCAT(address_1, ' ', address_2) AS address FROM oc_address_app WHERE customer_id = '".$result['cust_id']."' ", $this->conn);
			if ($cust_details->num_rows > 0) {
				$cust_name = $cust_details->row['cust_name'];
				$cust_id = $cust_details->row['customer_id'];
				$cust_address = $cust_details->row['address'];
			} else {
				$cust_name = '';
				$cust_id = 0;
				$cust_address = '';
			}
			$cust_details2 = $this->query("SELECT telephone, email FROM oc_customer_app WHERE customer_id = '".$result['cust_id']."' ", $this->conn);
			if ($cust_details2->num_rows > 0) {
				$cust_contact = $cust_details2->row['telephone'];
				$cust_email = $cust_details2->row['email'];
			} else {
				$cust_contact = '';
				$cust_email = '';
			}

			$other_details = $this->query("SELECT bill_date FROM oc_order_items ORDER BY id ASC LIMIT 1 ", $this->conn);
			if ($other_details->num_rows > 0) {
				$bill_date = $other_details->row['bill_date'];
			} else {
				$bill_date = date('Y-m-d');
			}

			$grand_totals = number_format($result['grand_tot'],2,'.','');
			$grand_total_explode = explode('.', $grand_totals);
			$roundtotal = 0;
			if(isset($grand_total_explode[1])){
				if($grand_total_explode[1] >= 50){
					$roundtotals = 100 - $grand_total_explode[1];
					$roundtotal = ($roundtotals / 100); 
				} else {
					$roundtotal = -($grand_total_explode[1] / 100);
				}
			}
			
			$other_details2 = $this->query("SELECT COUNT(*) AS total_items, SUM(qty) AS item_quantity FROM oc_order_item_app WHERE order_id = '".$result['order_id']."' ", $this->conn);
			if ($other_details2->num_rows > 0) {
				$total_items = $other_details2->row['total_items'];
				$item_quantity = $other_details2->row['item_quantity'];
			} else {
				$total_items = 0;
				$item_quantity = 0;
			}

			//order_items

			// echo "<pre>";
			// print_r($result['order_id']);
			// exit;
			$results2 = $this->query("SELECT * FROM oc_order_item_app WHERE order_id = '".$order_id."' ", $this->conn)->rows;

			$online_orders_item = array();
			foreach ($results2 as $result2) {
				$subcats = $this->query("SELECT item_sub_category_id, vat FROM oc_item WHERE item_code = '".$result2['item_id']."' ", $this->conn);
				if ($subcats->num_rows > 0) {
					$subcat_id = $subcats->row['item_sub_category_id'];
					$gst = $subcats->row['vat'];
				} else {
					$subcat_id = 0;
					$gst = 0;
				}
				$gst_val = ($result2['total'] * $gst)/100;
				$online_orders_item[] = array(
					'item_id' => $result2['item_id'],
					'item'        => $result2['item'],
					'qty' => $result2['qty'],
					'price' => round($result2['price'], 2),
					'total' => round($result2['total'], 2),
					'subcat_id' => $subcat_id,
					'special_notes' => $result2['special_notes'],
					'gst' => $gst,
					'gst_val' => $gst_val,
				);

			}
			//order_items

			$result1['online_orders'][] = array(
				'kot_no' => $kot_no,
				'table_id' => $table_id,
				'total' => round($result['total'], 2),
				'grand_tot' => round($result['grand_tot'], 2),
				'gst_val' => $result['gst_val'],
				'cust_name' => $cust_name,
				'cust_id' => $cust_id,
				'cust_contact' => $cust_contact,
				'cust_address' => $result['address_1'].' '.$result['address_2'],
				'cust_email' => $cust_email,
				'bill_date' => $bill_date,
				'roundtotal' => $roundtotal,
				'total_items' => $total_items,
				'item_quantity' => $item_quantity,
				'online_orders_item' => $online_orders_item
			);
		}
		return $result1;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>