<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){ 
		if(!isset($data['username'])){
			$data['username'] = '';
		}
		if($data['username'] != ''){
			$user_data = $this->query("SELECT * FROM oc_customer WHERE otp = '" .$data['username']."' ",$this->conn);
			$result = array();
			$result['user_data']['exist_status'] = 0;
			if ($user_data->num_rows > 0) {
				foreach($user_data->rows as $nkey => $nvalue){
					$this->query("UPDATE `oc_customer` SET `status`=1,`approved`=1,`is_logged_in` = 1 WHERE `customer_id`='".$nvalue['customer_id']."' ",$this->conn);
					$result['user_data'] = array(
						'user_id' => $nvalue['customer_id'], 
						'username' => $nvalue['firstname'].' '.$nvalue['lastname'],
						'displayName'=> $nvalue['firstname'].' '.$nvalue['lastname'],
						'photo' => '',
						'email' => $nvalue['email'],
						'exist_status' => 1,
					);
				}	
			} 
		} else {
			$result['user_data'] = array();
		}
		if(isset($result['user_data'])){
			$result['exist_status'] = 1;
		} else {
			$result['exist_status'] = 0;
		}
		return $result;
	}
	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>