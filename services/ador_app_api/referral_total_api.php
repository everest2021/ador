<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){ 
		if(!isset($data['customer_id'])){
			$data['customer_id'] = '';
			//$data['username'] = '';
		}
		if(!isset($data['total'])){
			$data['total'] = '';
			//$data['username'] = '';
		}
		
		$referral_creddits = $this->query("SELECT * FROM oc_customer WHERE customer_id = '".$data['customer_id']."'",$this->conn);
		if ($referral_creddits->num_rows > 0 ){
				$referral_credditss	= $referral_creddits->row['referral_credits'];
			}else{
				$referral_credditss = '0';
			}

		$final_totals = $data['total'] - $referral_credditss;
		if ($final_totals < 0) {
			$result = 0;
		} else {
			$result = $final_totals;
		}

		// if(isset($result['order_query'])){
		// 	$result['status'] = 1;
		// } else {
		// 	$result['status'] = 0;
		// }
		return $result;
	}

	public function token($length = 32) {
		// Create random token
		$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		
		$max = strlen($string) - 1;
		
		$token = '';
		
		for ($i = 0; $i < $length; $i++) {
			$token .= $string[mt_rand(0, $max)];
		}	
		
		return $token;
	}

}

?>