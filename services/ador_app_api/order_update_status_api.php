<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		/*echo "<pre>";
		print_r($data);
		exit;*/
		if(!isset($data['orderid'])){
			$data['orderid'] = '';
		}

		/*echo "UPDATE `oc_order` SET `order_status_id` = '7' WHERE `order_id` = '".$data['orderid']."'";
		echo "INSERT INTO `oc_order_history` SET `order_id` = '".$data['orderid']."', `order_status_id`= '7' , notify = '1', date_added = NOW()";
		echo "SELECT * FROM `oc_order_product` WHERE order_id = '".$data['orderid']."'";
		exit;*/
		
		$result = array();
			
		$orderitem_delete = $this->query("UPDATE `oc_order` SET `order_status_id` = '7' WHERE `order_id` = '".$data['orderid']."' ",$this->conn);

		$orderitem_delete = $this->query("INSERT INTO `oc_order_history` SET `order_id` = '".$data['orderid']."', `order_status_id`= '7' , notify = '1', comment = '', date_added = NOW() ",$this->conn);

		$product_query = $this->query("SELECT * FROM `oc_order_product` WHERE order_id = '".$data['orderid']."' ",$this->conn);

		foreach($product_query->rows as $product) {
			/*echo '<pre>';
			print_r($product);*/
			$this->query("UPDATE `oc_product` SET quantity = (quantity + " .(int)$product['quantity']. ") WHERE product_id = '" . (int)$product['product_id'] . "' AND subtract = '1' ",$this->conn);

			$option_query = $this->query("SELECT * FROM `oc_order_option` WHERE order_id = '" .$data['orderid']. "' AND order_product_id = '" . (int)$product['order_product_id'] . "' ",$this->conn);

			foreach ($option_query->rows as $option) {
				$this->query("UPDATE `oc_product_option_value` SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1' ",$this->conn);
			}
		}//exit;
		$telno = $this->query("SELECT telephone, customer_id FROM oc_order WHERE order_id = '".$data['orderid']."' ",$this->conn)->row;

		//return credit points. start
		$order_totals = $this->query("SELECT `value` FROM `oc_order_total` WHERE `code` = 'referral_credits_used' AND `order_id` = '".$data['orderid']."' ",$this->conn)->row;
		$credit_used = 0;
		if ($order_totals) {
			$credit_used = abs($order_totals['value']);
		}
		$this->query("UPDATE `oc_customer` SET referral_credits = (referral_credits + " . (float)$credit_used . ") WHERE `customer_id` = '".$telno['customer_id']."' ",$this->conn);
		//return credit points. ends

		$mainurl = MESSAGE.$telno['telephone'].'&msg=';
		$messagecustomer = "Dear customer your order has been canceled..%0aTaazitokari";
		$messagecustomer = str_replace(' ', '%20', $messagecustomer);
		$link = $mainurl.$messagecustomer;
		//echo "<pre>";print_r($link);exit;
		file($link);

		$result['success'] = 1;
	
		return $result;
	}
	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>