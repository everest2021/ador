<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();
$value = $Itemapi->getitem($datas);
exit(json_encode($value));

class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array()){
		if(!isset($data['address_id'])){
			$data['address_id'] = '0';//'225';
		}
		
		$result = array();
		$address_data = $this->query("SELECT * FROM `oc_address` WHERE address_id ='".$data['address_id']."' ", $this->conn);
		$state = 'MAHARASHTRA';
		$country = 'INDIA';
		if ($address_data->num_rows > 0) {
			foreach($address_data->rows as $nkey => $nvalue){
				$result['address_data'] = array(
					'address_id' =>  $nvalue['address_id'], 
                    'address_1' => $nvalue['address_1'], 
                    'address_2' => $nvalue['address_2'], 
                    'city' => $nvalue['city'],
                    'postcode' => $nvalue['postcode'],
                    'area' => $nvalue['area'],
                    'state' =>$state,
                    'country' =>$country
				);

				$area_datas = $this->query("SELECT `station`, `station_n` FROM `oc_pincode` WHERE `pincode` = '".$nvalue['postcode']."' ", $this->conn)->rows;
				foreach($area_datas as $akey => $avalue) {
					$result['area_datas'][] = array(
						'key' => $avalue['station'],
						'value' => $avalue['station_n'],
					);
				}			
			}	
		}

		$city_datass = array(
			''  => 'Please Select',
			'100 ft road'  => '100 ft road',
			'Achole Road' => 'Achole Road',
			'Agashi' => 'Agashi',
			'Ambadi Road' => 'Ambadi Road',
			'Arnala' => 'Arnala',
			'Bangli' => 'Bangli',
			'Barampur' => 'Barampur',
			'bhabola' => 'bhabola',
			'Bhuigaon' => 'Bhuigaon',
			'Bolinj' => 'Bolinj',
			'Chulna' => 'Chulna',
			'Dewanman' => 'Dewanman',
			'Evershine city' => 'Evershine city',
			'Gass' => 'Gass',
			'Giriz' => 'Giriz',
			'Global city' => 'Global city',
			'Gokhivare' => 'Gokhivare',
			'Gomes aali' => 'Gomes aali',
			'Holi' => 'Holi',
			'K T village, Vasai west' => 'K T village, Vasai west',
			'Khochiwade ' => 'Khochiwade ',
			'koliwada' => 'koliwada',
			'Manickpur' => 'Manickpur',
			'Manvelpada' => 'Manvelpada',
			'Merces' => 'Merces',
			'Mulgaon' => 'Mulgaon',
			'Naigaon east' => 'Naigaon east',
			'Naigaon west' => 'Naigaon west',
			'Nalla' => 'Nalla',
			'Nallasopara East' => 'Nallasopara East',
			'Nallasopara west' => 'Nallasopara west',
			'Nanbhat' => 'Nanbhat',
			'Nandakhal' => 'Nandakhal',
			'Nirmal' => 'Nirmal',
			'Om Nagar' => 'Om Nagar',
			'Papdy' => 'Papdy',
			'Parnaka' => 'Parnaka',
			'Phoolpada' => 'Phoolpada',
			'Rajodi' => 'Rajodi',
			'Ramedy' => 'Ramedy',
			'Sagarshet' => 'Sagarshet',
			'Sai nager' => 'Sai nager',
			'Samel Pada' => 'Samel Pada',
			'Sandor' => 'Sandor',
			'satpala' => 'satpala',
			'Shastri Nagar' => 'Shastri Nagar',
			'Sopara' => 'Sopara',
			'Sriprasta' => 'Sriprasta',
			'Stella' => 'Stella',
			'Suncity' => 'Suncity',
			'Tamtalao' => 'Tamtalao',
			'Tarkhad' => 'Tarkhad',
			'Umelman' => 'Umelman',
			'umralla' => 'umralla',
			'Vasai West' => 'Vasai West',
			'Vasaigaon' => 'Vasaigaon',
			'Vasalai' => 'Vasalai',
			'Vasant Nagari' => 'Vasant Nagari',
			'Virar east' => 'Virar east',
			'Virar West' => 'Virar West',
			'Wagholi' => 'Wagholi',
			'waliv' => 'waliv',
		);

		foreach($city_datass as $dkey => $dvalue){
			$result['city_datas'][] = array(
				'key' => $dkey,
				'value' => $dvalue,
			);
		}

		if(isset($result['address_data'])){
			$result['success'] = 1;
		} else {
			$result['success'] = 0;
		}

		// echo '<pre>';
		// print_r($result);
		// exit;
		
		return $result;
	}
	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>