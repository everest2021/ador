<?php
	session_start();
	include '../../admin/config.php';
	
	$file = 'db_branch_bk.zip';
	$path = pathinfo(realpath($file), PATHINFO_DIRNAME);
	$zip = new ZipArchive;
	if ($zip->open($file) === TRUE) {
	    $zip->extractTo($path);
	    $zip->close();
	    echo 'ok';
	} else {
	    echo 'failed';
	    exit;
	}
	
	$mysqli = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	/* check connection */
	if (mysqli_connect_errno()) {
	  printf("Connect failed: %s\n", mysqli_connect_error());
	  exit();
	}

	$mysqli->set_charset("utf8");
	$mysqli->multi_query(file_get_contents('server/db_branch_bk.sql'));
	if(isset($_SESSION['default']['token'])){
            $token = $_SESSION['default']['token'];
        } elseif (isset($_SESSION['token'])) {
             $token = $_SESSION['token'];
        }
        header('Location: '.HTTP_SERVER.'index.php?route=catalog/item&token='.$token);
?>