<?php

session_start();
include '../../admin/config.php';
$backup = new Backup();
$backup->backupDatabaseTables();
$backup->importDatabaseTables();
if(isset($_SESSION['default']['token'])){
    $token = $_SESSION['default']['token'];
} elseif (isset($_SESSION['token'])) {
     $token = $_SESSION['token'];
}
header('Location: '.HTTP_SERVER.'index.php?route=catalog/item&token='.$token);
class Backup
{
    public $conn;
    
    function __construct()
    {
        $dbname = DB_DATABASE;
        $host = DB_HOSTNAME;
        $user = DB_USERNAME;
        $pass = DB_PASSWORD;

        $this->conn = new mysqli($host,$user,$pass,$dbname);

        if($this->conn->connect_error){
            die("connection failed :".$this->conn->connect_error);
        }
    }

    function backupDatabaseTables(){
        $this->conn->query("SET NAMES 'utf8'");
        $queryTables    = $this->conn->query('SHOW TABLES'); 
        while($row = $queryTables->fetch_row()) { 
            if(
                $row[0] == 'oc_category' || 
                $row[0] == 'oc_department' ||
                $row[0] == 'oc_subcategory' || 
                $row[0] == 'oc_item' || 
                $row[0] == 'oc_kotgroup' || 
                $row[0] == 'oc_location' || 
                $row[0] == 'oc_tax' ||
                $row[0] == 'oc_waiter'
                ){
                $target_tables[] = $row[0]; 
            }
        }

        foreach($target_tables as $table) {
            $result         =   $this->conn->query("SELECT * FROM $table");  
            $fields_amount  =   $result->field_count;  
            $rows_num       =   $this->conn->affected_rows;    
            $res            =   $this->conn->query('SHOW CREATE TABLE '.$table); 
            $TableMLine     =   $res->fetch_row();
            $content        = (!isset($content) ?  '' : $content);

            for ($i = 0, $st_counter = 0; $i < $fields_amount;   $i++, $st_counter=0) {
                while($row = $result->fetch_row()) { //when started (and every after 100 command cycle):
                    if ($st_counter%100 == 0 || $st_counter == 0 )  {
                            $content .= "\nINSERT INTO ".$table." VALUES";
                    }
                    $content .= "\n(";
                    for($j=0; $j<$fields_amount; $j++)  { 
                        $row[$j] = str_replace("\n","\\n", addslashes($row[$j]) ); 
                        if (isset($row[$j])){
                            $content .= '"'.$row[$j].'"' ; 
                        } else {   
                            $content .= '""';
                        }     
                        if ($j<($fields_amount-1)) {
                            $content.= ',';
                        }      
                    }
                    $content .=")";
                    //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
                    if ( (($st_counter+1)%100==0 && $st_counter!=0) || $st_counter+1==$rows_num) {   
                        $content .= ";";
                    } else {
                        $content .= ",";
                    } 
                    $st_counter=$st_counter+1;
                }
            } $content .="\n\n\n";
        }

        $backup_name = '../branch/'.'db_server_bk.sql';
        file_put_contents($backup_name, $content);
    }

    function importDatabaseTables(){
        $file = 'db_branch_bk.zip';
        $path = pathinfo(realpath($file), PATHINFO_DIRNAME);
        $zip = new ZipArchive;
        if ($zip->open($file) === TRUE) {
            $zip->extractTo($path);
            $this->conn->set_charset("utf8");
            $this->conn->multi_query(file_get_contents('server/db_branch_bk.sql'));
            $this->conn->query("UPDATE oc_order_items SET ismodifier = '1'"); 
            $zip->close();
            echo 'ok';
        } else {
            echo 'failed';
            exit;
        }
    }

}

?>